﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using System.Windows.Media.Animation;

namespace RPN.ST.DisplayControl
{
    /// <summary>
    /// UserControl1.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class UserControl1 : UserControl
    {
        TextBlock text1, text2;


        public UserControl1()
        {
            InitializeComponent();

            this.Loaded += OnLoaded;

            this.Height = SystemParameters.MaximizedPrimaryScreenHeight;
            this.Width = SystemParameters.MaximizedPrimaryScreenWidth;

      ////      this.MouseLeftButtonDown += OnMouseLeftButtonDown;
            this.MouseRightButtonDown += OnMouseRightButtonDown;
        }

        public void OnLoaded(Object sender, RoutedEventArgs args)
        {
            CreatePanel();
        }

        public void OnMouseLeftButtonDown(Object sender, MouseButtonEventArgs args)
        {
            if (MouseButtonState.Pressed == args.LeftButton)
            {
           //     this.DragMove();
            }
        }

        public void OnMouseRightButtonDown(Object sender, RoutedEventArgs args)
        {

            TranslateAnimation(text1, 10);
            TranslateAnimation(text2, 10);
        }


        private void CreatePanel()
        {
            DockPanel dcPanel = new DockPanel();

            Canvas canvas = new Canvas();
            canvas.Background = new SolidColorBrush(Colors.LightBlue);
            canvas.Height = 132;

            text1 = new TextBlock();
            text2 = new TextBlock();

            text1.Name = "text1";
            text1.Text = "Canvas";
            text1.FontSize = 25;

            text2.Name = "text2";
            text2.Text = "왼쪽에서 오른 쪽으로 흐르는 텍스트!!";
            text2.FontSize = 20;

            Canvas.SetLeft(text1, 155);
            Canvas.SetTop(text2, 103.4);

            canvas.Children.Add(text1);
            canvas.Children.Add(text2);

            DockPanel.SetDock(canvas, Dock.Bottom);
            dcPanel.Children.Add(canvas);

            Rectangle redRectangle = new Rectangle();
            redRectangle.Width = 50;
            redRectangle.Height = 50;
            redRectangle.Stroke = new SolidColorBrush(Colors.Black);
            redRectangle.StrokeThickness = 10;
            redRectangle.Fill = new SolidColorBrush(Colors.Red);
            DockPanel.SetDock(redRectangle, Dock.Top);
            dcPanel.Children.Add(redRectangle);
            this.Content = dcPanel;



        }

        private void TranslateAnimation(TextBlock text, double time)
        {
            double right = 0;
            FrameworkElement parent = text.Parent as FrameworkElement;

            // 텍스트의 길이가 부모 패널을 넘어갈 때 
            if (text.ActualWidth > parent.ActualWidth)
            {
                right = text.ActualWidth - parent.ActualWidth;
            }

            // 부모 패널이 Grid라면 Margin 값 변경
            // 텍스트가 잘리는 현상 방지
            if (parent is Grid)
            {
                text.Margin = new Thickness(0, 0, -right, 0);
            }

            // TranslateTransform을 생성해야 애니메이션 적용
            text.RenderTransform = new TranslateTransform();
            // text.RenderTransform = new RotateTransform(0.0);

            Storyboard story = new Storyboard();

            DoubleAnimation animation = new DoubleAnimation();


            animation.From = parent.ActualWidth;
            animation.To = -(parent.ActualWidth + right);
            animation.Duration = TimeSpan.FromSeconds(time);
            animation.RepeatBehavior = RepeatBehavior.Forever;

            // TranslateTransform.XProperty 값 설정
            DependencyProperty[] Dproperty = new DependencyProperty[]
            {
                TextBlock.RenderTransformProperty,
                TranslateTransform.YProperty
            };

            string path = "(0).(1)";

            Storyboard.SetTargetProperty(animation, new PropertyPath(path, Dproperty));
            story.Children.Add(animation);
            story.Begin(text);
        }
    }
}
