﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using RPN.Common.Core.ViewModel;
using System.Windows.Media;


namespace RPN.ST.CommandControl.ViewModels
{
    partial class ST_CommandViewModel
    {
        // 글
        private string scrollingText_;
        public string ScrollingText
        {
            get
            {
                return scrollingText_;
            }

            set
            {
                scrollingText_ = value;
                RaisePropertyChanged(() => ScrollingText);
            }
        }


        // 배경색
        private Color bgColor_;
        public Color BgColor
        {
            get { return bgColor_; }
            set
            {
                bgColor_ = value;
                RaisePropertyChanged(() => BgColor);
            }
        }


        // 글색
        private Color fgColor_;
        public Color FgColor
        {
            get { return fgColor_; }
            set
            {
                fgColor_ = value;
                RaisePropertyChanged(() => FgColor);
            }
        }

        // 글꼴
        private FontFamily selectedFontFamily_;
        public FontFamily SelectedFontFamily
        {
            get { return selectedFontFamily_; }
            set
            {
                selectedFontFamily_ = value;
                RaisePropertyChanged(() => SelectedFontFamily);
            }
        }

        //글자크기
        private double fontSize_;
        public double FontSize
        {
            get { return fontSize_; }
            set
            {
                fontSize_ = value;
                RaisePropertyChanged(() => FontSize);
            }
        }

    }
}
