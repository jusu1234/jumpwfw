﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;
using System.Collections.ObjectModel;

using RPN.Common.Core.ViewModel;
using RPN.ST.CommandControl.Views;

namespace RPN.ST.CommandControl.ViewModels
{

    partial class ST_CommandViewModel : ViewModelBase
    {
        private static readonly double[] CommonlyUsedFontSizes = new double[]
        {
            6.0 , 8.0, 9.0 , 10.0, 11.0 , 12.0, 14.0, 16.0, 18.0

        };


        ICollection<FontFamily> fontFamilyList_;
        public ICollection<FontFamily> FontFamilyCollection
        {
            get
            {
                if (fontFamilyList_ == null)
                {
                    fontFamilyList_ = Fonts.SystemFontFamilies;
                }

                return fontFamilyList_;
            }

            set
            {
                if (fontFamilyList_ != value)
                {
                    fontFamilyList_ = value;
                    RaisePropertyChanged(() => FontFamilyCollection);
                }
            }
        }



        public ST_CommandViewModel() :
            base("ST_CommandViewModel", false, false)
        {

            ScrollingText = "Test Data";


            BgColor = Color.FromRgb(0x00, 0x00, 0xFF);
            FgColor = Color.FromRgb(0xFF, 0xFF, 0xFF);
        }





    }
}
