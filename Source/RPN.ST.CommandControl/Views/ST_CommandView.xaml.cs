﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using RPN.Common.Logging;
using RPN.ST.CommandControl.ViewModels;

namespace RPN.ST.CommandControl.Views
{
    /// <summary>
    /// UserControl1.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class ST_CommandView : UserControl
    {
        

        private bool fontFamilyListValid_ = false; 


        public ST_CommandView()
        {
            InitializeComponent();

            
        }


        protected override void OnInitialized(EventArgs e)
        {
           base.OnInitialized(e);


           if (!fontFamilyListValid_)
           {
               InitializeFontFamilyList();
               fontFamilyListValid_ = true;
           }
            
        }

        private void InitializeFontFamilyList()
        {
            ICollection<FontFamily> familyCollection = ((ST_CommandViewModel)this.DataContext).FontFamilyCollection;

            if (familyCollection != null)
            {
                FontFamilyItem[] items = new FontFamilyItem[familyCollection.Count];

                int i = 0;

                foreach (FontFamily family in familyCollection)
                {
                    items[i++] = new FontFamilyItem(family);
                }

                Array.Sort<FontFamilyItem>(items);

                foreach (FontFamilyItem item in items)
                {
                    this.FontFamilyCombo.Items.Add(item);
                }

            }

        }

        
    }
}
