﻿using System;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Controls;
using System.Windows.Media;
using System.Globalization;

namespace RPN.ST.CommandControl.Views
{
    internal class FontFamilyItem : TextBlock, IComparable
    {
        private string displayName_;

        public FontFamilyItem(FontFamily family)
        {
            displayName_ = FontFamilyHelper.GetDisplayName(family);

            this.FontFamily = family;
            this.Text = displayName_;
            this.ToolTip = displayName_;
        }

        public override string ToString()
        {
            return displayName_;
        }

        int IComparable.CompareTo(object other)
        {
            return -string.Compare(displayName_, other.ToString(), true, CultureInfo.CurrentCulture);
        }



    }
}
