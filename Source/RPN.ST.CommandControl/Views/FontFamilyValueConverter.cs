﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Windows.Data;
using System.Windows.Media;

namespace RPN.ST.CommandControl.Views
{
    
    class FontFamilyValueConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {

            FontFamily family = value as FontFamily;

            if (family == null)
            {
                throw new ArgumentNullException();
            }

            return FontFamilyHelper.GetDisplayName(family);
        }


        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotSupportedException();
        }
    }
     
}
