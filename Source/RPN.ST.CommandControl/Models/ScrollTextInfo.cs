﻿using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace RPN.ST.CommandControl.Models
{
    /// <summary>
    /// 글꼴 정보
    /// </summary>
    public class FontInfo
    {
        public FontFamily Family { get; set; }
        public double Size { get; set; }
        public FontStyle Style { get; set; }
        public FontStretch Stretch { get; set; }
        public FontWeight Weight { get; set; }
        public SolidColorBrush BrushColor { get; set; }

        public FamilyTypeface Typeface
        {
            get
            {
                FamilyTypeface ftf = new FamilyTypeface();
                ftf.Stretch = this.Stretch;
                ftf.Weight = this.Weight;
                ftf.Style = this.Style;
                return ftf;
            }
        }
    }

    /// <summary>
    /// 스크롤 정보
    /// </summary>
    public class ScrollInfo
    {
        // (수평/수직) 스크롤 
        public bool Horizontal { get; set; }

        // 스크롤 방향 
        public bool Direction { get; set; }

        // 스코를 속도
        public double Speed { get; set; }

        public ScrollInfo()
        {
        }

        public ScrollInfo(bool horizontal, bool direction, double speed)
        {
            Horizontal = horizontal;
            Direction = direction;
            Speed = speed;
        }
    }


    public class ScrollTextInfo
    {
        // 배경 색
        public SolidColorBrush BackgroundBrush { get; set; }

        // 글꼴 정보
        public FontInfo Font { get; set; }

        // 스크롤 정보
        public ScrollInfo Scroll { get; set; }

        // 표시 문자
        public string Text { get; set; }

    }

   


}
