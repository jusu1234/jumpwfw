﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using System.Collections.ObjectModel;
using System.ComponentModel;
using GMapIcon.ViewModel;
using GMapIcon.Model;
using N3N.WPF.Command;
using N3N.WPF.ViewModel;

namespace GMapIcon.ViewModel
{
    public class PointViewModel :ObservableObject
    {
        ObservableCollection<PointAndInfo> infos;
        //readonly PropertyObserver<RecordInfo> _observer;
        ICommand _command;

        public PointViewModel()
        {
            /*
            PersonsInfo = new ObservableCollection<RecordInfo>();

            PersonsInfo.Add(new RecordInfo { Name = "AA", Age = 24, DateOfBirth = new DateTime(1987, 4, 29),Address="XXX XXX XXXX" });
            PersonsInfo.Add(new RecordInfo { Name = "BB", Age = 23, DateOfBirth = new DateTime(1988, 3, 4),Address="XXX XXXXX XXX" });
            PersonsInfo.Add(new RecordInfo { Name = "CC", Age = 26, DateOfBirth = new DateTime(1985, 10, 2),Address="XXX XXX X" });
            PersonsInfo.Add(new RecordInfo { Name = "DD", Age = 28, DateOfBirth = new DateTime(1983, 5, 7),Address="XX XXXXX XX" });
            PersonsInfo.Add(new RecordInfo { Name = "EE", Age = 26, DateOfBirth = new DateTime(1985, 11, 20),Address="XXXX XXXXX XXX" });
            PersonsInfo.Add(new RecordInfo { Name = "FF", Age = 22, DateOfBirth = new DateTime(1989, 1, 27),Address="XXXXXXXXXXX XX" });
            PersonsInfo.Add(new RecordInfo { Name = "GG", Age = 21, DateOfBirth = new DateTime(1990, 8, 6),Address="XXX XXXX XXXXXX" });
            PersonsInfo.Add(new RecordInfo { Name = "HH", Age = 23, DateOfBirth = new DateTime(1988, 9, 9),Address="XX XXXXXXXXX XXX" });
            PersonsInfo.Add(new RecordInfo { Name = "II", Age = 24, DateOfBirth = new DateTime(1987, 10, 18),Address="XXXXXXXXX XXXX" });
            PersonsInfo.Add(new RecordInfo { Name = "JJ", Age = 21, DateOfBirth = new DateTime(1990, 11, 27),Address="XXXXX XXXXXXXX XX" });
           */
        }



        public ObservableCollection<PointAndInfo> PointInfo
        {
            get
            {
                return infos;
            }
            set
            {
                //System.Diagnostics.Debug.WriteLine("========>");
                infos = value;
                RaisePropertyChanged("PointInfo");
            }
        }



        
        public ICommand RemoveCommand
        {
            get
            {
                if (_command == null)
                {
                    //_command = new DelegateCommand(CanExecute, Execute);
                    _command = new DelegateCommand<object>((P) => Execute(P), CanExecute);
                }
                return _command;
            }
        }

        private void Execute(object parameter)
        {
            int index = PointInfo.IndexOf(parameter as PointAndInfo);
            if (index > -1 && index < PointInfo.Count)
            {
                PointInfo.RemoveAt(index);
            }
        }

        private bool CanExecute(object parameter)
        {
            return true;
        }
        
    }
}
