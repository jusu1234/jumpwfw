﻿using System;
using System.Collections.ObjectModel;
using N3N.WPF.ViewModel;
using N3N.WPF;

namespace GMapIcon.ViewModel
{
    public class NumberChangeLogViewModel
    {
        public NumberChangeLogViewModel()
        {
            this.Number = new NumberViewModel();
            this.ChangeLog = new ObservableCollection<string>();

            /*
            _observer =
                new PropertyObserver2<NumberViewModel>(this.Number)
                   .RegisterHandler(n => n.Value, n => Log("Value: " + n.Value))
                   .RegisterHandler(n => n.IsNegative, this.AppendIsNegative)
                   .RegisterHandler(n => n.IsEven, this.AppendIsEven);
            */

            _observer =
                new PropertyObserver<NumberViewModel>(this.Number)
                   .AddHandler(n => n.IsNegative, this.AppendIsNegative)
                   .AddHandler(n => n.IsEven, this.AppendIsEven)
                   .AddHandler(n => n.Value, (n,e) => Log("Value: " + n.Value));
        }

        void AppendIsNegative(NumberViewModel number, EventArgs args )
        {
            
            //Console.Out.WriteLine("=>" + number.IsNegative);
            System.Diagnostics.Debug.WriteLine("=>" + number.IsNegative);

            if (number.IsNegative)
                this.Log("\tNumber is now negative");
            else
                this.Log("\tNumber is now positive");
        }

        void AppendIsEven(NumberViewModel number, EventArgs args)
        {
            if (number.IsEven)
                this.Log("\tNumber is now even");
            else
                this.Log("\tNumber is now odd");
        }

        
        void Log(string item)
        {
            this.ChangeLog.Add(item);
            //App.Messenger.NotifyColleagues(App.MSG_LOG_APPENDED);
        }
         

        public ObservableCollection<string> ChangeLog { get; private set; }
        public NumberViewModel Number { get; private set; }

        readonly PropertyObserver<NumberViewModel> _observer;
    }
}