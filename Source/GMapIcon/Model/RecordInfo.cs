﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using System.ComponentModel;
using GMapIcon.ViewModel;
using N3N.WPF.ViewModel;

namespace GMapIcon.Model
{
    public class RecordInfo// :ObservableObject
    {
        //string _name = string.Empty;
        //Int32 _age = 0;
        //DateTime _dob;
        //string _address = string.Empty;

        public RecordInfo()
        {
            
        }

        public string Name { get; set; }
        public Int32 Age { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string Address { get; set; }

        /*
        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
                //OnPropertyChanged("Name");
                RaisePropertyChanged("Name");
            }
        }

        public Int32 Age
        {
            get
            {
                return _age;
            }
            set
            {
                _age = value;
                RaisePropertyChanged("Age");
            }
        }
        

        
        public DateTime DateOfBirth
        {
            get
            {
                return _dob;
            }
            set
            {
                _dob = value;
                RaisePropertyChanged("DateOfBirth");
            }
        }
        
        public string Address
        {
            get
            {
                return _address;
            }
            set
            {
                _address = value;
                RaisePropertyChanged("Address");
            }
        }
         * */
    }

    /*
    public class DelegateCommand : ICommand
    {

        Predicate<object> canExecute;
        Action<object> execute;

        public DelegateCommand(Predicate<object> _canexecute,Action<object> _execute)
            :this()
        {
            canExecute = _canexecute;
            execute = _execute;
        }

        public DelegateCommand()
        {

        }

        public bool CanExecute(object parameter)
        {
            return canExecute == null ? true : canExecute(parameter);
        }

        public event EventHandler CanExecuteChanged;

        public void Execute(object parameter)
        {
            execute(parameter);
        }
    }
     */



}
