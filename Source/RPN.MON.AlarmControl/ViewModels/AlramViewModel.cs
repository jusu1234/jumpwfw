﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.Composition;
using System.Collections.ObjectModel;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Controls;

using RPN.Common.Core.ViewModel;
using RPN.Common.Core.Command;
using RPN.Common.Core.Message;
using RPN.Common.Core.Service;
using RPN.Common.Core.Tasks;
using RPN.Common.Core.Collection;

using RPN.MON.Data;
using RPN.MON.Data.Models;
using System.Windows;
using System.Xml;
using System.IO;
using System.Xml.Serialization;
using RPN.Common.Util;
using System.Text.RegularExpressions;
using System.Xml.Linq;



namespace RPN.MON.AlarmControl.ViewModels
{
    [Export(typeof(IViewModel))]
    [View(typeof(RPN.MON.AlarmControl.Views.AlramView))]
    public class AlramViewModel : ViewModelBase , IViewModel
    {
        #region IView member
        static string viewName_;
        public string ViewName
        {
            get { return viewName_; }
            set
            {
                viewName_ = value;
                RaisePropertyChanged(() => ViewName);
            }
        }
        #endregion

        #region field

        ICommand _searchRuleCommand;
        ICommand _searchRuleMappingCommand;
        ICommand _addRuleCommand;
        ICommand _deleteRuleCommand;
        AlarmRule _selectedItem;

        ObservableCollection<AlarmSource> _alarmSourceCollection;
        ObservableCollection<AlarmRule> _alarmRuleCollection;
        ObservableCollection<AlarmRuleMapping> _alarmRuleMappingCollection;

        #endregion

        #region CLR member


        public AlarmRule SelectedItem
        {
            get { return this._selectedItem; }
            set
            {
                this._selectedItem = value;
                RaisePropertyChanged(() => SelectedItem);
            }
        }


        public ObservableCollection<AlarmSource> AlarmSourceList
        {
            get { return _alarmSourceCollection; }

            set
            {
                _alarmSourceCollection = value;
                RaisePropertyChanged(() => AlarmSourceList);
            }
        }

        public ObservableCollection<AlarmRule> AlarmRuleList
        {
            get { return _alarmRuleCollection; }

            set
            {
                _alarmRuleCollection = value;
                RaisePropertyChanged(() => AlarmRuleList);
            }
        }

        public ObservableCollection<AlarmRuleMapping> AlarmRuleMappingList
        {
            get { return _alarmRuleMappingCollection; }

            set
            {
                _alarmRuleMappingCollection = value;
                RaisePropertyChanged(() => AlarmRuleMappingList);
            }
        }


        #endregion

        #region Command

        public ICommand SearchRuleCommand
        {
            get
            {
                if (_searchRuleCommand == null)
                {
                    _searchRuleCommand = new DelegateCommand<object>((P) => SearchAlarmRuleExecute(P), CanExecute);
                }
                return _searchRuleCommand;
            }
        }

        public ICommand SearchRuleMappingCommand
        {
            get
            {
                if (_searchRuleMappingCommand == null)
                {
                    _searchRuleMappingCommand = new DelegateCommand<object>((P) => SearchAlarmRuleMappingExecute(P), CanExecute);
                }
                return _searchRuleMappingCommand;
            }
        }


        public ICommand AddRuleCommand
        {
            get
            {
                if (_addRuleCommand == null)
                {
                    _addRuleCommand = new DelegateCommand<object>((P) => AddRuleExecute(P), CanExecute);
                }
                return _addRuleCommand;
            }
        }

        public ICommand DeleteRuleCommand
        {
            get
            {
                if (_deleteRuleCommand == null)
                {
                    _deleteRuleCommand = new DelegateCommand<object>((P) => DeleteRuleExecute(P), CanExecute);
                }
                return _deleteRuleCommand;
            }
        }

        private void SearchAlarmRuleExecute(object parameter)
        {

            this.ViewRef.DataContext = this;

            try
            {
                string xml = RestServiceHelper.Request(RestServiceHelper.DATA_SERVICE_URL + "/GetAlarmRuleList", false);

                if (!string.IsNullOrWhiteSpace(xml))
                {
                    System.Diagnostics.Debug.WriteLine(xml);
                    
                    Regex.Split(xml, "\0");
                    XDocument doc = XDocument.Parse(xml);

                    IEnumerable<AlarmRule> lst =
                        from el in doc.Descendants("Table")
                        select new AlarmRule()
                        {
                            RuleId = (string)el.Element("AlarmRuleId").Value,
                            RuleName = (string)el.Element("AlarmRuleName").Value,
                            RuleActions = (string)el.Element("AlarmRuleObj").Value                            
                        };

                    _alarmRuleCollection = new ObservableCollection<AlarmRule>();
                    foreach (AlarmRule rule in lst)
                    {
                        _alarmRuleCollection.Add(rule);
                    }
                }
                
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("===========" + e.Message);
            }

        }

        private void SearchAlarmRuleMappingExecute(object parameter)
        {

            // TODO : 서버리스트와 MAPPING RULE 조인해서 가져오기
            this.ViewRef.DataContext = this;

            try
            {
                string xml = RestServiceHelper.Request(RestServiceHelper.DATA_SERVICE_URL + "/GetAlarmRuleMapping", false);

                if (!string.IsNullOrWhiteSpace(xml))
                {
                    System.Diagnostics.Debug.WriteLine(xml);

                    Regex.Split(xml, "\0");
                    XDocument doc = XDocument.Parse(xml);

                    IEnumerable<AlarmRuleMapping> lst =
                        from el in doc.Descendants("Table")
                        select new AlarmRuleMapping()
                        {

                            SystemId = (string)el.Element("SystemId").Value,
                            Description = (string)el.Element("Description").Value,
                            SystemType = (string)el.Element("Type").Value,
                            IP = (string)el.Element("IP").Value,
                            AlarmRuleId = el.Element("AlarmRuleId")==null?-1:Convert.ToInt32(el.Element("AlarmRuleId").Value)
                        };

                    _alarmRuleMappingCollection = new ObservableCollection<AlarmRuleMapping>();

                    foreach (AlarmRuleMapping rule in lst)
                    {
                        _alarmRuleMappingCollection.Add(rule);
                    }
                }

            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("===========" + e.Message);
            }

        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="parameter"></param>
        private void AddRuleExecute(object parameter)
        {
            //Mediator.GetInstance.BroadCast(ServiceMessage.GetCameraStatus);
            //MessageBox.Show("Clicked AddRule", "AddRule", MessageBoxButton.OK, MessageBoxImage.Information);

            ObservableCollection<AlarmSource> sourceList = new ObservableCollection<AlarmSource>();
            foreach (AlarmSource src in _alarmSourceCollection)
            {
                if (!src.IsChecked) continue;

                sourceList.Add(src);
            }

            AlarmRule newRule = new AlarmRule()
            {
                RuleName = "Undefined",
                AlarmSources = sourceList
            };

            try
            {
                string postMessage = newRule.Serialize();

                newRule.RuleId = string.Empty;
                newRule.RuleId = RestServiceHelper.RequestByPost("AddAlarmRule", postMessage);

                _alarmRuleCollection.Add(newRule);

            }
            catch (Exception e)
            {
                MessageBox.Show("Failed to Add Rule", "AddRule", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            finally
            {
                //TODO : Search Rule, Search RuleMapping
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="parameter"></param>
        private void DeleteRuleExecute(object parameter)
        {

            if (this.SelectedItem != null)
            {
                try
                {
                    //string ruleId = this.SelectedItem.RuleId;

                    RestServiceHelper.RequestByPost("DeleteAlarmRule", SelectedItem.Serialize());

                    int index = AlarmRuleList.IndexOf(this._selectedItem);
                    if (index > -1 && index < AlarmRuleList.Count)
                    {
                        AlarmRuleList.RemoveAt(index);
                    }

                }
                catch (Exception e)
                {
                    MessageBox.Show("Failed to Add Rule", "AddRule", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                finally
                {
                    //TOTO : Search Rule, Search RuleMapping
                }
            }
            else
            {
                MessageBox.Show("Please select an Item from the list", "AddRule", MessageBoxButton.OK, MessageBoxImage.Error);
            }

        }

        private bool CanExecute(object parameter)
        {
            return true;
        }



        #endregion

        #region Constructor
        public AlramViewModel() : base("AlramView", true, false)
        {
            ViewName = "Alram";

            initData();
        }


        private void initData()
        {
            //
            _alarmSourceCollection = new ObservableCollection<AlarmSource>()
            {
                new AlarmSource{ Id = AlarmSource.Src.HDD, Name = "Disk Usage", LimitedValue=90, IsChecked = false, IsEnabled=true},
                new AlarmSource{ Id = AlarmSource.Src.CPU, Name = "CPU Usage",  LimitedValue=90, IsChecked = true, IsEnabled=true},
                new AlarmSource{ Id = AlarmSource.Src.SERVER_PING, Name = "Ping",  LimitedValue=90, IsChecked = false, IsEnabled=true},
                new AlarmSource{ Id = AlarmSource.Src.MEMORY, Name = "Memory",  LimitedValue=90, IsChecked = false, IsEnabled=true},
                new AlarmSource{ Id = AlarmSource.Src.CAMERA_PING, Name = "Camera Ping",  LimitedValue=100, IsChecked = false, IsEnabled=false},
                new AlarmSource{ Id = AlarmSource.Src.CAMERA_RECORDSTATUS, Name = "Camera RecordStatus",  LimitedValue=0, IsChecked = false, IsEnabled=false}

            };

            _alarmRuleCollection = new ObservableCollection<AlarmRule>();


            //
            SearchAlarmRuleExecute(string.Empty);

            //
            SearchAlarmRuleMappingExecute(string.Empty);
        }

        #endregion


    }

    [Serializable]
    [XmlRoot(Namespace = "", IsNullable = false)]
    public class AlarmRule
    {

        public AlarmRule() { }

        public AlarmRule(string ruleId)
        {
            this.RuleId = ruleId;
        }

        /// <remarks/>
        [XmlElement(ElementName = "RuleId")]
        public string RuleId { get; set; }

        [XmlElement(ElementName = "RuleName")]
        public string RuleName { get; set; }

        [XmlElement(ElementName = "AlarmSource")]
        public ObservableCollection<AlarmSource> AlarmSources { get; set; }

        [XmlElement(ElementName = "RuleAction")]
        public string RuleActions { get; set; }

        public string Serialize()
        {
            XmlSerializer s = new XmlSerializer(this.GetType());
            StringBuilder sb = new StringBuilder();
            TextWriter w = new StringWriter(sb);
            try
            {
                s.Serialize(w, this);
                w.Flush();
                return sb.ToString();
            }
            catch
            {
                throw;
            }
            finally
            {
                w.Close();
                w.Dispose();
            }
                
        }

        public object DeSerialize(string xmlOfAnObject)
        {
            AlarmRule myObject = new AlarmRule();

            StringReader read = new StringReader(xmlOfAnObject);
            XmlSerializer serializer = new XmlSerializer(myObject.GetType());
            XmlReader reader = new XmlTextReader(read);

            try
            {
                myObject = (AlarmRule)serializer.Deserialize(reader);
                return myObject;
            }
            catch
            {
                throw;
            }
            finally
            {
                reader.Close();
                read.Close();
                read.Dispose();
            }
        }
    }
    public class AlarmSource
    {
        public enum Src { CPU, MEMORY, HDD, SERVER_PING, CAMERA_PING, CAMERA_RECORDSTATUS }
        public enum SrcType { SERVER, CAMERA }

        [XmlAttribute("id")]
        public Src Id { get; set; }

        [XmlAttribute("type")]
        public SrcType Type { get; set; }

        public string Name { get; set; }

        public int LimitedValue { get; set; }

        public bool IsChecked { get; set; }

        public bool IsEnabled { get; set; }


    }

    public class BaseAction
    {
        public enum Act {Email, SMS, Innowatch}

        [XmlAttribute("type")]
        public Act ActionType { get; set; }

        public string Name { get; set; }

        public string ActionValue { get; set; }
    }


    public class AlarmRuleMapping
    {
        public string SystemId { get; set; }
        public string Description { get; set; }
        public string SystemType { get; set; }
        public string IP { get; set; }
        public int AlarmRuleId { get; set; }

    }

}
