﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RPN.Common.Core.Data
{
    public class EntityItem
    {
        public object Value { get; private set; }
        public object OldValue { get; private set; }
        public Type type { get; private set; }

        public EntityItem(object value)
        {
            Value = value;
            type = value.GetType();

        }

        public EntityItem(object value, object oldValue)
        {
            Value = value;
            OldValue = oldValue;
            type = value.GetType();
        }
    }
}
