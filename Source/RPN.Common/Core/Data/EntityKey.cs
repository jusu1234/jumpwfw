﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RPN.Common.Core.Data
{
    public class EntityKey
    {
        public string Name { get; private set; }
        public object Value { get; private set; }

        public EntityKey(string name, object value)
        {
            Name = name;
            Value = value;
        }

        
    }
}
