﻿using System.ComponentModel;

namespace RPN.Common.Core.Data
{
    public sealed class EntityCustomTypeDescriptor : CustomTypeDescriptor
    {
        private readonly PropertyDescriptorCollection descriptors_;

        public EntityCustomTypeDescriptor(PropertyDescriptorCollection descriptors)
        {
            descriptors_ = descriptors;
        }

        public override PropertyDescriptorCollection GetProperties()
        {
            return descriptors_;
        }
    }
}
