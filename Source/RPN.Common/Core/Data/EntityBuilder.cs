﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.ComponentModel;
using System.Configuration;
using System.Xml.Linq;


namespace RPN.Common.Core.Data
{
    public static class EntityBuilder
    {

        internal static Dictionary<string, EntityPropertyDescriptor[]> metadatas_ =
            new Dictionary<string, EntityPropertyDescriptor[]>();

        static EntityBuilder()
        {
            var mf = ConfigurationManager.AppSettings["METADATA_FILE"];
            XElement mdFile = XElement.Load(mf);

            var allDescriptors = new List<EntityPropertyDescriptor>();
            foreach (XElement node in mdFile.Elements())
            {
                var descriptors = new List<EntityPropertyDescriptor>();
                foreach (var e in node.Elements())
                {
                    var pd = new EntityPropertyDescriptor(
                            e.Attribute("Name").Value,
                            Type.GetType(e.Attribute("Type").Value), null);
                    descriptors.Add(pd);
                    if (!allDescriptors.Contains(pd))
                    {
                        allDescriptors.Add(pd);
                    }

                    string metakey = node.Name.ToString();
                    metadatas_.Add(metakey, descriptors.ToArray());
                }
            }

             var customProvider = new EntityTypeDescriptionProvider(TypeDescriptor.GetProvider(typeof(Entity)),
                new EntityCustomTypeDescriptor(new PropertyDescriptorCollection(allDescriptors.ToArray(), false)));
            TypeDescriptor.AddProvider(customProvider, typeof(Entity));  
        }

        public static PropertyDescriptorCollection LoadMetaData(string name)
        {
             var descriptors = new List<PropertyDescriptor>();
     
             foreach (var pd in metadatas_[name])
             {
                  if (!descriptors.Contains(pd)) 
                      descriptors.Add(pd);
             }
        

            return new PropertyDescriptorCollection(descriptors.ToArray(), false);
        }
        
        public static ulong GetHash(params string[] sources)
        {
            const ulong fnvOffsetBasis = 14695981039346656037;
            const ulong fnvPrime = 1099511628211;
            ulong hash = fnvOffsetBasis;
            foreach (var src in sources)
            {
                foreach (var c in src.ToCharArray())
                {
                    hash = hash * fnvPrime;
                    hash = hash ^ c;
                }
            }
            return hash;

        }

        

    
        public static T Build<T>(string key)
        {
            return (T)Activator.CreateInstance(typeof(T), Activator.CreateInstance(typeof(EntityKey), key));
        }
    }
}
