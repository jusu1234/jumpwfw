﻿using System;
using System.ComponentModel;

namespace RPN.Common.Core.Data
{
    public class EntityPropertyDescriptor : PropertyDescriptor
    {
        private readonly Type type_;

        public EntityPropertyDescriptor(string name, Type type, Attribute[] attributes)
            : base(name, attributes)
        {
            type_ = type;
        }

        public override object GetValue(object component)
        {
            var entity = (Entity)component;
            EntityItem value;
            entity.Values.TryGetValue(Name, out value);
            return value;
        }

        public override void SetValue(object component, object value)
        {
            var entity = (Entity)component;
            entity.Values[Name] = (EntityItem)value;
        }

        public override void ResetValue(object component)
        {
        }

        public override bool CanResetValue(object component)
        {
            return false;
        }

        public override bool ShouldSerializeValue(object component)
        {
            return false;
        }

        public override Type PropertyType
        {
            get { return type_; }
        }

        public override bool IsReadOnly
        {
            get { return false; }
        }

        public override Type ComponentType
        {
            get { return typeof(Entity); }
        }
    }
}
