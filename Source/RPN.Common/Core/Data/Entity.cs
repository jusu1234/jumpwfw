﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;

using RPN.Common.Core;

namespace RPN.Common.Core.Data
{
    public class Entity : NotificationObject
    {
        private readonly EntityKey key_;
        private readonly ConcurrentDictionary<string, EntityItem> values_ = new ConcurrentDictionary<string, EntityItem>();

        public ConcurrentDictionary<string, EntityItem> Values
        {
            get { return values_; }
        }

        public EntityKey Key
        {
            get { return key_; }
        }

        public Entity(EntityKey key)
        {
            if (key == null) throw new ArgumentNullException("key");
            key_ = key;
            values_.TryAdd(key.Name,  new EntityItem(key.Value));
        }

        public IEnumerable<string> GetPropertyNames()
        {
            return values_.Keys;
        }

        public IEnumerable<object> GetPropertyValues()
        {
            return values_.Values;
        }

    }
}
