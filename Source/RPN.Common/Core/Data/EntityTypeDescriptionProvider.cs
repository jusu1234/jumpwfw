﻿using System;
using System.ComponentModel;

namespace RPN.Common.Core.Data
{
    public class EntityTypeDescriptionProvider : TypeDescriptionProvider
    {
        private readonly ICustomTypeDescriptor defaultDescriptor_;

        public EntityTypeDescriptionProvider(TypeDescriptionProvider parent, ICustomTypeDescriptor defaultDescriptor)
            : base(parent)
        {
            defaultDescriptor_ = defaultDescriptor;
        }

        public override ICustomTypeDescriptor GetTypeDescriptor(Type objectType, object instance)
        {
            return defaultDescriptor_;
        }
    }
}
