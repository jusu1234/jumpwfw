﻿using System;

namespace RPN.Common.Core
{
    [AttributeUsage(AttributeTargets.Property)]
    public class NotifyAttribute : Attribute
    {
    }
}
