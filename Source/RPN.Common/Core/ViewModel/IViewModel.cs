﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RPN.Common.Core.ViewModel
{
    public interface IViewModel
    {
        string ViewName { get; set; }
    }
}
