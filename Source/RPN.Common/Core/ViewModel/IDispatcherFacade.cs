﻿using System;
using System.Windows.Threading;

namespace RPN.Common.Core.ViewModel
{
    public interface IDispatcherFacade
    {
        void BeginInvoke( Delegate workItem);
    }
}
