﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RPN.Common.Core.ViewModel
{
    [AttributeUsage(AttributeTargets.Class)]
    public sealed class ViewAttribute : Attribute
    {
        public Type ViewType { get; private set; }

        public ViewAttribute(Type viewType)
        {
            ViewType = viewType;
        }
    }
}
