﻿using System;
using System.Collections.Generic;

using System.Text;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Diagnostics;
using System.Globalization;
using System.ComponentModel;
using System.Windows;
using System.Windows.Threading;
using System.Threading;

using System.Reactive.Linq;

using RPN.Common.Logging;
using RPN.Common.Core.Util;

using System.Runtime;
using System.Runtime.CompilerServices;

namespace RPN.Common.Core.ViewModel
{
    /// <summary>
    /// <summary>
    /// ViewModel 추상 클래스 
    /// 
    /// viewResolve가 true이면 view를 직접 생성한다. 
    ///     주의 :  1. 생성자에서 View 객체의  DataContext에 ViewModel을 주입한다. xaml이나 비하인드코드에서 viewModel를 주입하면
    ///                 무한재귀로 StackOverFlowException 발생
    ///                 
    ///             2. xaml 비하인드 코드에서 InitializeComponent로 객체를 생성한다면 에러발생
    /// </summary>
    public abstract class ViewModelBase : DispatcherObject , INotifyPropertyChanged
    {

      //  private static RecursionPoint s_RecursionPoint = new RecursionPoint();
        protected Logger log_;  

        public FrameworkElement ViewRef { get; private set; }

        protected IDispatcherFacade dispatcherFacade_;

        /// <summary>
        ///  생성자
        /// </summary>
        /// <param name="viewName"></param>
        /// <param name="viewResolve"></param>
        /// <param name="dynamicUpdate"></param>
        protected ViewModelBase(string viewName, bool viewResolve, bool dynamicUpdate)
        {
            //log_ = LoggerFactory.GetLogger(viewName);

            try
            {
                if (viewResolve)
                {
                    var attr = (ViewAttribute)GetType().GetCustomAttributes(typeof(ViewAttribute), true).FirstOrDefault();

                    if (attr == null)
                        throw new ApplicationException("ViweAttribute Missing");


                    ViewRef = (FrameworkElement)Activator.CreateInstance(attr.ViewType);


                    Uri resourceLocator;

                    //  InitializeComponent 함수가 수행하는 작업과 동일
                    resourceLocator = new Uri("/" + attr.ViewType.Module.ScopeName.Replace(".dll", "") + ";component/Views/" +
                            attr.ViewType.Name + ".xaml", UriKind.Relative);


                    Application.LoadComponent(ViewRef, resourceLocator);


                    log_.Info(string.Format("{0} View Create Located : {1}",
                        attr.ViewType.Name, resourceLocator.OriginalString));

                    // DataContext에 View 모델 주입
                    ViewRef.DataContext = this;

                }


                if (dynamicUpdate)
                {
                    dispatcherFacade_ = new MaintenanceView(viewName, log_, Dispatcher);
                }
            }
            catch (Exception ex)
            {
            }
        }
            
        /// <summary>
        ///   View에 Element를 읽어옴..
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="name"></param>
        /// <returns></returns>
        protected T GetElement<T>(string name)
        {
         
            return (T)ViewRef.FindName(name);
            
        }

        #region INotifyPropertyChanged Member
        public PropertyChangedEventHandler propertyChanged_;

        public event PropertyChangedEventHandler PropertyChanged
        {
            add { propertyChanged_ += value; }
            remove { propertyChanged_ -= value; }
        }

        protected void RaisePropertyChanged(string propertyName)
        {
            CheckPropertyName(propertyName);

            if (propertyChanged_ != null)
            {
                propertyChanged_(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        protected void RaisePropertyChanged<T>(Expression<Func<T>> expression)
        {
            string propertyName = PropertyHelper.ExtractPropertyName<T>(expression);
            RaisePropertyChanged(propertyName);
        }

        [Conditional("DEBUG")]
        private void CheckPropertyName(string propertyName)
        {
            PropertyDescriptor propertyDescriptor = TypeDescriptor.GetProperties(this)[propertyName];
            if (propertyDescriptor == null)
            {
                throw new InvalidOperationException(string.Format(CultureInfo.InvariantCulture,
                    "The property with the propertyName '{0}' doesn't exist.", propertyName));
            }
        }

        private PropertyInfo GetProperty(string propertyName)
        {
            return this.GetType().GetProperties().FirstOrDefault(property => property.Name == propertyName);
        }

        #endregion

        #region ProcessAsync
        protected static void ProcessAsync(Action method)
        {
            ThreadPool.QueueUserWorkItem((o) => method());
        }

        protected static void ProcessAsync<T>(Action<T> method, T parameter)
        {
            ThreadPool.QueueUserWorkItem((o) => method(parameter));
        }
       
        protected void ProcessOnDispatcherThread(Action method)
        {
            Dispatcher.BeginInvoke(method, null);
        }

        protected void ProcessOnDispatcherThread(Action method, DispatcherPriority priority)
        {
            Dispatcher.BeginInvoke(priority, method);
        }

        protected void ProcessOnDispatcherThread<T>(Action<T> method, T parameter)
        {
            Dispatcher.BeginInvoke(method, parameter);
        }

        protected void ProcessOnDispatcherThread<T1, T2>(Action<T1, T2> method, T1 parameter, T2 parameter2)
        {
            Dispatcher.BeginInvoke(method, parameter, parameter2);
        }

        protected void ProcessOnDispatcherThread<T>(Action<T> method, T parameter, DispatcherPriority priority)
        {
            Dispatcher.BeginInvoke(method, priority, parameter);
        }
        #endregion


        protected static void WithUpdateProgress(Func<Window> createUpdateWindow, Action progressOperation)
        {
            var dispatcherStart = new object();
            Window updateWindow = null;
            var thread = new Thread(() =>
            {
                lock (dispatcherStart)
                {
                    updateWindow = createUpdateWindow();
                    updateWindow.Closed += (sender, e) => updateWindow.Dispatcher.InvokeShutdown();
                    updateWindow.Show();
                }
                Dispatcher.Run();
            });

            thread.SetApartmentState(ApartmentState.STA);
            thread.Start();

            try
            {
                progressOperation();
            }
            finally
            {
                lock (dispatcherStart)
                {
                    updateWindow.Dispatcher.BeginInvoke((Action)(() => updateWindow.Close()));
                }
            }
        }
        

    }

    

}

