﻿using System;
using System.Collections.Concurrent;
using System.Configuration;
using System.Reactive.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Threading;

using RPN.Common.Core.Message;
using RPN.Common.Core.Util;
using RPN.Common.Logging;

namespace RPN.Common.Core.ViewModel
{
    public sealed class MaintenanceView : IDispatcherFacade
    {
        readonly Logger logger_;

        readonly string viewName_;
        readonly Dispatcher dispatcher_;
      
        readonly int heartBeat_rate_ = 1000;
        readonly int monitor_rate_ = 2000;
        readonly int queue_size_ = 250;

        private long operationQueueCount_ = 0;
        readonly ConcurrentDictionary<DispatcherOperation, object> operationQueue_ = new  ConcurrentDictionary<DispatcherOperation, object>();

        public MaintenanceView(string viewName, Logger logger, Dispatcher dispatcher)
        {
            
            viewName_ = viewName;
            logger_ = logger;
            dispatcher_ = dispatcher;

            Observable.Interval(TimeSpan.FromMilliseconds(monitor_rate_)).Subscribe(MonitorDispatcherQueue);
            Observable.Interval(TimeSpan.FromMilliseconds(heartBeat_rate_)).Subscribe(ViewStateUpdate);
        }

        private void ViewStateUpdate(long l)
        {
            DateTime dt = DateTime.Now;
          //  Action w = () => Mediator.GetInstance.BroadCast(Messages.StateUpdate, dt);
           // BeginInvoke(w);
            Mediator.GetInstance.BroadCast(Messages.StateUpdate, dt);

     //       logger_.Debug(string.Format("{0} Updated", viewName_));
        }

        private void MonitorDispatcherQueue(long l)
        {
            if (Interlocked.Read(ref operationQueueCount_) != 0)
            {
                logger_.Info(string.Format("Dispathcer Operation in Queue {0}", Interlocked.Read(ref operationQueueCount_)));
            }

            if (Interlocked.Read(ref operationQueueCount_) > queue_size_)
            {
                logger_.Info("Pushing all Dispathcer Operations");
                Application.Current.DoEvents();
                operationQueue_.Clear();
                Interlocked.Exchange(ref operationQueueCount_, 0);
            }
        }

        public void BeginInvoke( Delegate workItem)
        {
            var operation = dispatcher_.BeginInvoke(DispatcherPriority.Background, workItem);

            operation.Completed += (s, o) =>
                {
                    Interlocked.Decrement(ref operationQueueCount_);
                    object t;
                    operationQueue_.TryRemove((DispatcherOperation)s, out t);
                };

            operationQueue_.TryAdd(operation, null);
            Interlocked.Increment(ref operationQueueCount_);
        }
        
    }
}
