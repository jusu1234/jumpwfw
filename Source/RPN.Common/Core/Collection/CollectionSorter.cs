﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace RPN.Common.Core.Collection
{
    /// <summary>
    /// 
    /// ObserableCollection Sorting
    /// </summary>
    public static class CollectionSorter
    {
        public static void Sort<T>(this ObservableCollection<T> collection) where T : IComparable
        {
            List<T> sorted = collection.OrderBy(x => x).ToList();

			for (int i = 0; i < sorted.Count(); i++)
				collection.Move(collection.IndexOf(sorted[i]), i);
		}

        public static void Sort<T>(this ObservableCollection<T> collection, Comparison<T> comparison, bool bDescending = false)
        {
            IComparer<T> Comparator;
            
            if(!bDescending)
                Comparator = new Comparator<T>(comparison);
            else
                Comparator = new ReverseComparator<T>(comparison);


            List<T> sorted = collection.OrderBy(x => x, Comparator).ToList();


            for (int i = 0; i < sorted.Count(); i++)
                collection.Move(collection.IndexOf(sorted[i]), i);
        }

    }

    internal class Comparator<T> : IComparer<T>
    {
        private readonly Comparison<T> comp_;

        public Comparator(Comparison<T> comparison)
        {
            comp_ = comparison;
        }

        public int Compare(T x, T y)
        {
            return comp_.Invoke(x, y);
        }
    }

    internal class ReverseComparator<T> : IComparer<T>
    {
        private readonly Comparison<T> comp_;

        public ReverseComparator(Comparison<T> comparison)
        {
            comp_ = comparison;
        }

        public int Compare(T x, T y)
        {
            return -comp_.Invoke(x, y);
        }
    }
}
