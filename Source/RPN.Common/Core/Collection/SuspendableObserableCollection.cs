﻿
﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows.Threading;

namespace RPN.Common.Core.Collection
{
    /// <summary>
    ///   다량의 데이터가 한번에 업데이트 될 경우 
    ///   임시적으로 CollectionNotifyChanged가 호출되는것을 막아 수많은 Notification 일어나는것을 방지한다.
    ///   (AddorUpdate사용) 1000개이상 item 업데이트시 유용함. 
    /// </summary>
    /// 
    /// TODO : Contains , SetItem 호출 비용이 매우 큼 개선필요
    /// 
    public class SuspendableObserableCollection<T> : ObservableCollection<T>
    {

        private bool suspendCollectionNotifyChanged_;

        private int maxCountNonSuspend_ = 15000;

        public override event NotifyCollectionChangedEventHandler CollectionChanged;


        public SuspendableObserableCollection()
            : base()
        {
            suspendCollectionNotifyChanged_ = false;
        }

        public void SuspendCollectionNotifyChanged()
        {
            suspendCollectionNotifyChanged_ = true;
        }

        public void ResumCollectionNotifyChaned()
        {
            suspendCollectionNotifyChanged_ = false;

            OnCollectionChanged(
                new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
        }

        // Contains item 매칭 IEqutable<T>  (Non ThreadSafe)
        public void AddOrUpdate(IEnumerable<T> items, bool isReplace)
        {
            if (items == null)
            {
                return;
                //throw new ArgumentNullException("items null");
            }

            List<T> itemList = items.ToList();

            bool bSuspended = itemList.Count > maxCountNonSuspend_;

            if (bSuspended)
            {
                SuspendCollectionNotifyChanged();
            }

          
            for (var index = 0; index < itemList.Count; index++)
            {
                var item = itemList[index];

                if (!Contains(item))
                {
                    Add(item);

                    /*
                    if (!bSuspended)
                    {
                         OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, item));
                    }
                     * */
                }
                else
                {
                    if (isReplace)
                    {
                        int idx = this.IndexOf(item);
                        SetItem(idx, item);

                        if (!bSuspended)
                        {
                            OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Replace, item, item));
                        }
                    }
                }
            }


            if(bSuspended)
            {
                ResumCollectionNotifyChaned();
            }
        }


        /// <summary>
        ///  OnCollectionChanged 함수 재정의
        /// </summary>
        /// <param name="e"></param>
        protected override void OnCollectionChanged(NotifyCollectionChangedEventArgs e)
        {
            //////////////
            // 재진입 방지
            ///////////////
            using (BlockReentrancy())
            {
                // Suspend 상태에서는 Notify 이벤트를 호출하지 않는다.
                if (!suspendCollectionNotifyChanged_)
                {
                    NotifyCollectionChangedEventHandler eventHandler =
                          this.CollectionChanged;
                    if (eventHandler == null)
                    {
                        return;
                    }

                    Delegate[] delegates = eventHandler.GetInvocationList();

                    foreach
                    (NotifyCollectionChangedEventHandler handler in delegates)
                    {

                       handler(this, e);
                       
                    }
                }
            }
        }



    }
}

