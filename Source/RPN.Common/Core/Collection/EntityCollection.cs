﻿using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;

using RPN.Common.Core.Data;

namespace RPN.Common.Core.Collection
{
    class EntityCollecton : Collection<Entity>, ITypedList, INotifyCollectionChanged 
    {
        private readonly PropertyDescriptorCollection descriptors_;

        public EntityCollecton(PropertyDescriptorCollection descripters)
        {
            descriptors_ = descripters; 
        }

        public void AddOrUpdate(IEnumerable<Entity> items, bool isReplace)
        {
            foreach (var item in items)
            {
                if (!Contains(item))
                {
                    Add(item);
                    OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, item));
                }
                else
                {
                    if (isReplace)
                    {
                        OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Replace, item, item));
                    }
                    else
                    {
                        foreach (var property in item.GetPropertyNames())
                        {
                            item.RaisePropertyChanged(property);
                        }
                    }
                }
            }
        }


        #region INotifyCollectionChanged Members
        public event NotifyCollectionChangedEventHandler CollectionChanged;
        protected void OnCollectionChanged(NotifyCollectionChangedEventArgs e)
        {
            if (CollectionChanged != null)
            {
                CollectionChanged(this, e);
            }
        }
        #endregion

        #region ITypedList Members

        string ITypedList.GetListName(PropertyDescriptor[] listAccessors)
        {
            return typeof(Entity).Name;
        }

        PropertyDescriptorCollection ITypedList.GetItemProperties(PropertyDescriptor[] listAccessors)
        {
            return descriptors_;
        }

        #endregion
    }
}
