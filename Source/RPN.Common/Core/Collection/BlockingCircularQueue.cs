﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace RPN.Common.Core.Collection
{
    public class BlockingCircularQueue<T>
    {
        readonly T[] buffer_;
        readonly int size_ = 1024;
        int readerIndex_ = 0;
        int writerIndex_ = 0;
        bool writingSuspended_;
        readonly object lock_ = new object();

        public BlockingCircularQueue(int size)
        {
            size_ = size;
            buffer_ = new T[size_];
        }

        private void Clear()
        {
            readerIndex_ = 0;
            writerIndex_ = 0;

            Array.Clear(buffer_, 0, size_);
        }

        public int Count()
        {
            int count;
            lock (lock_)
            {
                count = ((writerIndex_ % size_) - readerIndex_);
            }

            return (count < 0) ? (-1) * count : count;
        }


        public void Enqueue(T item)
        {
            lock (lock_)
            {
                while (IsFull() || writingSuspended_)
                {
                    Monitor.Wait(lock_);
                }
                buffer_[writerIndex_] = item;
                writerIndex_++;
                writerIndex_ %= size_;
                Monitor.Pulse(lock_);
            }
        }

        public T Peek()
        {
            T item;
            lock (lock_)
            {
                if (IsEmpty())
                {
                    return default(T);
                }
                item = buffer_[readerIndex_];
            }
            return item;
        }

        public T Dequeue()
        {
            T item;
            lock (lock_)
            {
                while (IsEmpty())
                {
                    Monitor.Wait(lock_);
                }
                item = buffer_[readerIndex_];
                readerIndex_++;
                readerIndex_ %= size_;
                Monitor.Pulse(lock_);
            }
            return item;
        }

        public void SuspendEnqueue()
        {
            if (writingSuspended_) return;
            Task.Factory.StartNew(() =>
            {
                writingSuspended_ = true;
                while (true)
                {
                    lock (lock_)
                    {
                        if (!IsEmpty()) continue;
                        writingSuspended_ = false;
                        Monitor.PulseAll(lock_);
                    }
                    return;
                }
            });
        }

        private bool IsEmpty()
        {
            return (readerIndex_ == writerIndex_);
        }

        private bool IsFull()
        {
            return (((writerIndex_ + 1) % size_) == readerIndex_);
        }

    }
}
