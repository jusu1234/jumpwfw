﻿using System;
using System.Threading.Tasks;


namespace RPN.Common.Core.Tasks
{
    class ImmediaticTask : Task
    {
        private readonly double budget_;     
        private readonly Action action_;    
        private readonly int priority_;      
        private readonly DateTime timeCreated_;
      
        public double EstimatedBudget { get { return budget_; } }
        public Action Action { get { return action_; } }
        public int Priority { get { return priority_; } }
        public DateTime TimeCreated { get { return timeCreated_; } }

        public ImmediaticTask(Action action, double budget)
            : base(action)
        {
            budget_ = budget;
            priority_ = 0;
            action_ = action;
            timeCreated_ = DateTime.UtcNow;
        }
    }
}
