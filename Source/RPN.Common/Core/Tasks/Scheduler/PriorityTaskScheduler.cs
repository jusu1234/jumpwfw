﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;


using RPN.Common.Logging;
using RPN.Common.Core.Collection;
using RPN.Common.Core.Tasks;

namespace RPN.Common.Core.Tasks.Scheduler
{
    public sealed class PriorityTaskScheduler : TaskScheduler
    {
        #region Private Members Only

        private Logger log_ = LoggerFactory.GetLogger("PriorityTaskScheduler");

        private readonly int bufferSize_;
        private readonly double threshold_;
        private readonly ConcurrentQueue<Task> waitingQueue_;
        private readonly BlockingCircularQueue<PeriodTask> readyToRunQueue_;
     
       // private readonly PerformanceCounter messageCounter_;
        private readonly TaskFactory backgroundTaskFactory_ =
            new TaskFactory(TaskCreationOptions.LongRunning, TaskContinuationOptions.None);
        private readonly int tsp_;
        #endregion

        public PriorityTaskScheduler()
        {
          
            tsp_ = 50;
            bufferSize_ = 1024;
            threshold_ = 3000;
            readyToRunQueue_ = new BlockingCircularQueue<PeriodTask>(bufferSize_);
            waitingQueue_ = new ConcurrentQueue<Task>();

            var executor = new System.Timers.Timer(tsp_);
            executor.Elapsed += WaitingQueueToReadyToRun;
            executor.Start();

            for (int i = 0; i < Environment.ProcessorCount; i++)
            {
                backgroundTaskFactory_.StartNew(() =>
                {
                    while (true)
                    {
                        var task = readyToRunQueue_.Dequeue() as PeriodTask;
#if DEBUG
                    //    log_.Debug("Message Dequeued");
                        //_messageCounter.Decrement();
#endif
                        if (TryExecuteTask(task))
                        {
                            TimeSpan span = (DateTime.UtcNow - task.CreatedTime);

                            if (DateTime.UtcNow > task.Deadline)
                            {
                            //    log_.Warn(String.Format("Real-time Deadline exceeded : {0}", span.TotalMilliseconds));
                                //throw new ApplicationException("Real-time Deadline exceeded");
                            }
#if DEBUG
                    //          log_.Debug("Message Done");
#endif
                        }
                    }
                });
            }
        }

        private void WaitingQueueToReadyToRun(object sender, System.Timers.ElapsedEventArgs e)
        {
            Task task;
            while (waitingQueue_.TryDequeue(out task))
            {
                // check budget and invoke a context switch
                PeriodTask headTask;
                if (task is PeriodTask)
                {
                    headTask = (PeriodTask)task;
                    var nextToRun = readyToRunQueue_.Peek();

                    if ((nextToRun != null) && (nextToRun.Status == TaskStatus.WaitingToRun)
                        && headTask.Deadline < nextToRun.Deadline)
                    {
                        log_.Info("Task Change : " + DateTime.UtcNow);
                        var dequeuedTask = readyToRunQueue_.Dequeue();
                        readyToRunQueue_.Enqueue(headTask);
                        readyToRunQueue_.Enqueue(dequeuedTask);
                    }

                    readyToRunQueue_.Enqueue(headTask);
                }
            }
        }

        protected override void QueueTask(Task task)
        {
#if DEBUG
        //    log_.Debug("Message Enqueued");
           
#endif
            
      
            if (task is ImmediaticTask)
            {
                TryExecuteTask(task);
                log_.Info("ImmediaticTask To Queue " + DateTime.UtcNow);
                return;
            }
             

            waitingQueue_.Enqueue(task);
        }

        protected override bool TryExecuteTaskInline(Task task, bool taskWasPreviouslyQueued)
        {
            return TryExecuteTask(task);
        }

        protected override IEnumerable<Task> GetScheduledTasks()
        {
            return waitingQueue_.ToArray();
        }
    }
}
