﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;

namespace RPN.Common.Core.Tasks.Scheduler
{
    public class LongRunningTaskScheduler : TaskScheduler
    {
        readonly ApartmentState state_;
        readonly ConcurrentQueue<Thread> threadQueue_;
        readonly ConcurrentQueue<Task> taskQueue_;

        public LongRunningTaskScheduler(ApartmentState state)
        {
            state_ = state;
            threadQueue_ = new ConcurrentQueue<Thread>();
            taskQueue_ = new ConcurrentQueue<Task>();
        }

        protected override void QueueTask(Task task)
        {
            Thread thread = new Thread(() => TryExecuteTask(task));
            thread.SetApartmentState(state_);
            thread.Name = "GUI Thread";
            thread.Start();

            threadQueue_.Enqueue(thread);
            taskQueue_.Enqueue(task);
        }

        protected override IEnumerable<Task> GetScheduledTasks()
        {
            return taskQueue_;
        }

        protected override bool TryExecuteTaskInline(Task task, bool taskWasPreviouslyQueued)
        {
            Thread thread = new Thread(() => TryExecuteTask(task));
            thread.SetApartmentState(state_);
            thread.Name = "GUI Thread";
            thread.Start();

            threadQueue_.Enqueue(thread);
            taskQueue_.Enqueue(task);

            return true;

        }
    }
}
