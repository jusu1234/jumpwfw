﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RPN.Common.Core.Tasks
{
    public enum TaskType
    {
        CurrentTask,

        ImmediaticTask,

        BackgroundTask,

        DelayedTask,

        PeriodicTask,
        
        LongRunning
    }
}
