﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace RPN.Common.Core.Tasks
{
    class PeriodTask : Task
    {
        readonly long timeToRun_;
        readonly DateTime deadline_;
        readonly double budget_;
        readonly Action action_;
        readonly int priority_;
        readonly DateTime createdTime_;

        public long TimeToRun { get { return timeToRun_; } }
        public DateTime Deadline { get { return deadline_; } }
        public double Budget { get { return budget_; } }
        public Action Action { get { return action_; } }
        public int Priority { get { return priority_; } }

        public DateTime CreatedTime { get { return createdTime_; } }



        public PeriodTask(Action action, double budget)
            : base(action)
        {
            timeToRun_ = 0;
            deadline_ = DateTime.UtcNow.AddMilliseconds(budget);
            budget_ = budget;
            priority_ = 0;
            action_ = action;
            createdTime_ = DateTime.UtcNow;

            
        }
    }
}
