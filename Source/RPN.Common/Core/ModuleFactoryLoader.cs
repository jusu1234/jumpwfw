﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Configuration;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;

using RPN.Common.Core.ViewModel;
using RPN.Common.Core.Service;
using RPN.Common.Core.Message;

namespace RPN.Common.Core
{
    /// <summary>
    /// 동적으로 모듈 로드 (MEF) 
   
    /// </summary>
 
 
    public sealed class ModuleFactoryLoader
    {
        private CompositionContainer container_;

        private ConcurrentDictionary<string, Window> runningWindows_; 

        [ImportMany(typeof(IViewModel))]
        private IEnumerable<Lazy<IViewModel>> viewModels_;
        public IEnumerable<Lazy<IViewModel>> ViewModels
        {
            get { return viewModels_; }
        }

        [ImportMany(typeof(ServiceObserver))]
        private IEnumerable<Lazy<ServiceObserver>> serviceObservers_;
    
        private IEnumerable<Lazy<IService>> services_;
       
        public ModuleFactoryLoader()
        {
            runningWindows_ = new ConcurrentDictionary<string, Window>();

            // Windows만 UI Thread를 새로 생성할수 있음.

       //     Mediator.GetInstance.RegisterTask<Lazy<IViewModel>>(Messages.LoadViews, LoadView, Tasks.TaskType.LongRunning);
       //     Mediator.GetInstance.RegisterTask<Lazy<IViewModel>>(Messages.LoadViews, UnloadView, Tasks.TaskType.LongRunning);
        }

    
        public void Load(string filePath)
        {
            DynamicModuleLoad(filePath);
            InjectViewAndService(); 
        }

        private void DynamicModuleLoad(string filePath)
        {
            var catalog = new AggregateCatalog();

            try
            {
                catalog.Catalogs.Add(new DirectoryCatalog(filePath, "*Control.dll"));
       
                container_ = new CompositionContainer(catalog);
                container_.ComposeParts(this);

            }
            catch (Exception ex)
            {
                throw new ApplicationException(
                    string.Format("DynamicModuel Load Failed {0}", ex.Message));
            }

        }


        private void InjectViewAndService()
        {
            services_ = container_.GetExports<IService>();

            // Service 인스턴스 생성 및 Mediator 등록
            foreach (var lazy in services_)
            {
                var service = lazy.Value;
                Mediator.GetInstance.Register(service);
            }

            // ServiceObserer에 Service 등록 
            foreach (var lazy in serviceObservers_)
            {
                /*
                var d = ( from s in services_
                          where s.Value.ServiceName.Equals(lazy.Value.ServiceName)
                          select s).ToList();
                */

                // 이름을 비교하여 같은것은 등록한다.
                lazy.Value.RegisterService(
         
                    services_.Select(s => s.Value)
                    .Where(o => o.ServiceName.Equals(lazy.Value.ServiceName)).ToList()
                    );
            }

            // View 모델 인스턴스 생성 및 Mediator등록
            foreach (var lazy in viewModels_)
            {
                LoadView(lazy);
            }
        }


        private void LoadView(Lazy<IViewModel> lazy)
        {
            var vm = lazy.Value;
  

         //   runningWindows_.TryAdd(vm.ViewName,   ((ViewModelBase)vm).ViewRef);
            Mediator.GetInstance.Register(vm);
            
              
        }
    }
}
