﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace RPN.Common.Core.Util
{
    /// <summary>
    ///  재귀호출 체크..
    ///  CallStack 참조하는게 더 나을지도..
    /// </summary>
    public sealed class RecursionCheck
    {
        private long execute_ = 0;


        public long Execute
        {
            get { return execute_; }
        }

        public long Enter()
        {
            return Interlocked.Increment(ref execute_);
        }


        public long Exit()
        {
            return Interlocked.Decrement(ref execute_);
        }
    }
}
