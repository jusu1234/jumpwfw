﻿using System.Windows;
using System.Windows.Threading;


namespace RPN.Common.Core.Util
{

    
    

    /// <summary>
    ///   <para> UI 강제 업데이트 </para>
    /// </summary>
    /// 
    /// <remarks>
    /// <para> 참고 사이트 </para>
    /// 
    /// http://www.codeproject.com/Articles/152137/DispatcherFrame-Look-in-Depth
    /// http://kentb.blogspot.kr/2008/04/dispatcher-frames.html
    /// </remarks>
    /// 
    /// 
    ///
    public static class ApplicationExtensions
    {
   
       


        public static void DoEvents(this Application application)
        {
            DispatcherFrame frame = new DispatcherFrame();
            Dispatcher.CurrentDispatcher.BeginInvoke(DispatcherPriority.Background, new ExitFrameHandler(frm => frm.Continue = false), frame);
            Dispatcher.PushFrame(frame);
        }

        private delegate void ExitFrameHandler(DispatcherFrame frame);
    }
}
