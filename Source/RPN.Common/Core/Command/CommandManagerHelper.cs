﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Windows;
using System.Windows.Threading;

namespace RPN.Common.Core.Command
{
    internal class CommandManagerHelper
    {
        /// <summary>
        /// Invoke Handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="weakHandlers"></param>
        public static void CallWeakReferenceHandlers(object sender, List<WeakReference> weakHandlers)
        {

            if (weakHandlers != null)
            {
                EventHandler[] handlers = new EventHandler[weakHandlers.Count];

                int count = CleanupOldHandlers(weakHandlers, handlers, 0);

                for (int i = 0; i < count; i++)
                {
                    CallHandler(sender, handlers[i]);
                }
            }
        }

        ///<summary>
        /// Adds a handler to the supplied list in a weak way.
        ///</summary>
        ///<param name="handlers">Existing handler list.  It will be created if null.</param>
        ///<param name="handler">Handler to add.</param>
        ///<param name="defaultListSize">Default list size.</param>
        public static void AddWeakReferenceHandler(ref List<WeakReference> handlers, EventHandler handler, int defaultListSize)
        {
            if (handlers == null)
            {
                handlers = (defaultListSize > 0 ? new List<WeakReference>(defaultListSize) : new List<WeakReference>());
            }

            handlers.Add(new WeakReference(handler));
        }

        ///<summary>
        /// Removes an event handler from the reference list.
        ///</summary>
        ///<param name="handlers">Handler list to remove reference from.</param>
        ///<param name="handler">Handler to remove.</param>
        public static void RemoveWeakReferenceHandler(List<WeakReference> handlers, EventHandler handler)
        {
            if (handlers != null)
            {
                for (int i = handlers.Count - 1; i >= 0; i--)
                {
                    WeakReference reference = handlers[i];
                    EventHandler existingHandler = reference.Target as EventHandler;
                    if ((existingHandler == null) || (existingHandler == handler))
                    {
                        // Clean up old handlers that have been collected
                        // in addition to the handler that is to be removed.
                        handlers.RemoveAt(i);
                    }
                }
            }
        }


        private static void CallHandler(object sender, EventHandler handler)
        {
            DispatcherProxy dispatcher = DispatcherProxy.CreateDispatcher();




            if (dispatcher != null && !dispatcher.CheckAccess())
            {
                dispatcher.BeginInvoke((Action<object, EventHandler>)CallHandler, sender, handler);
            }
            else
            {
                handler(sender, EventArgs.Empty);
            }
        }

        private static int CleanupOldHandlers(List<WeakReference> weakHandlers, EventHandler[] handlers, int count)
        {
            for (int i = weakHandlers.Count - 1; i >= 0; i--)
            {
                WeakReference reference = weakHandlers[i];
                EventHandler handler = reference.Target as EventHandler;
                if (handler == null)
                {
                    // Clean up old handlers that have been collected
                    weakHandlers.RemoveAt(i);
                }
                else
                {
                    handlers[count] = handler;
                    count++;
                }
            }
            return count;
        }

        private class DispatcherProxy
        {
            Dispatcher dispatcher_;

            private DispatcherProxy(Dispatcher dispatcher)
            {
                dispatcher_ = dispatcher;
            }

            public static DispatcherProxy CreateDispatcher()
            {
                DispatcherProxy proxy = null;

                if (Application.Current == null)
                    return null;

                proxy = new DispatcherProxy(Application.Current.Dispatcher);

                return proxy;
            }

            public bool CheckAccess()
            {
                return dispatcher_.CheckAccess();
            }

            public DispatcherOperation BeginInvoke(Delegate method, params Object[] args)
            {
                return dispatcher_.BeginInvoke(method, DispatcherPriority.Normal, args);
            }
        }
    }
}
