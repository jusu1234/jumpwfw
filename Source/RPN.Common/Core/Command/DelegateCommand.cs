﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPN.Common.Core.Command
{
    /// <summary>
    /// Command
    /// </summary>
    public class DelegateCommand : DelegateCommandBase
    {
        public DelegateCommand(Action executeMethod)
            : this(executeMethod, null)
        {
        }

        public DelegateCommand(Action executeMethod, Func<bool> canExecuteMethod)
            : base(executeMethod != null ? (p) => executeMethod() : (Action<object>)null,
                canExecuteMethod != null ? (p) => canExecuteMethod() : (Func<object, bool>)null)
        {
        }

        public bool CanExecute()
        {
            return base.CanExecute(null);
        }

        public void Execute()
        {
            base.Execute(null);
        }
    }

    /// <summary>
    /// 인자있는 Command
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class DelegateCommand<T> : DelegateCommandBase
    {
        public DelegateCommand(Action<T> executeMethod)
            : this(executeMethod, null)
        {
        }

        public DelegateCommand(Action<T> executeMethod, Func<T, bool> canExecuteMethod)
            : base(executeMethod != null ? (p) => executeMethod((T)p) : (Action<object>)null,
                canExecuteMethod != null ? (p) => canExecuteMethod((T)p) : (Func<object, bool>)null)
        {
        }

        public bool CanExecute(T parameter)
        {
            return base.CanExecute(parameter);
        }

        public void Execute(T parameter)
        {
            base.Execute(parameter);
        }
    }
}
