﻿using System;
using System.Collections.Generic;
using System.Windows.Input;

namespace RPN.Common.Core.Command
{
    public abstract class DelegateCommandBase : ICommand
    {
        private readonly Action<object> executeMethod_;
        private readonly Func<object, bool> canExeuteMethod_;

        private List<WeakReference> weakHandlers_;

        protected DelegateCommandBase(Action<object> executeMethod, Func<object, bool> canExecuteMethod)
        {
            if (executeMethod == null)
                throw new ArgumentNullException("executeMethod");

            this.executeMethod_ = executeMethod;
            this.canExeuteMethod_ = canExecuteMethod;
        }

        protected virtual void DoCanExecuteChanged()
        {
            CommandManagerHelper.CallWeakReferenceHandlers(this, weakHandlers_);
        }

        void ICommand.Execute(object parameter)
        {
            Execute(parameter);
        }

        bool ICommand.CanExecute(object parameter)
        {
            return CanExecute(parameter);
        }

        protected void Execute(object parameter)
        {
            executeMethod_(parameter);
        }

        protected bool CanExecute(object parameter)
        {
            return canExeuteMethod_ == null || canExeuteMethod_(parameter);
        }

        public event EventHandler CanExecuteChanged
        {
            add
            {
                CommandManagerHelper.AddWeakReferenceHandler(ref weakHandlers_, value, 2);
            }

            remove
            {
                CommandManagerHelper.RemoveWeakReferenceHandler(weakHandlers_, value);
            }
        }
    }
}
