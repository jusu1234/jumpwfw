﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Reflection;

namespace RPN.Common.Core
{
    public class PropertyHelper
    {
        /// <summary>
        /// Property 람다식에서 Proerty 이름 추출
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="propertyExpression"></param>
        /// <returns></returns>
        public static string ExtractPropertyName<T>(Expression<Func<T>> propertyExpression)
        {

            if (propertyExpression == null)
            {
                throw new ArgumentNullException("propertyExpression");
            }


            MemberExpression memberExpression = propertyExpression.Body as MemberExpression;

            if (memberExpression == null)
            {
                throw new ArgumentException("NotMemberAccessExpression");
            }

            PropertyInfo propInfo = memberExpression.Member as PropertyInfo;

            if (propInfo == null)
            {
                throw new ArgumentException("ExpressionNotProperty");
            }

            MethodInfo getMethod = propInfo.GetGetMethod(true);

            if (getMethod.IsStatic)
            {
                throw new ArgumentException("StaticExpression");
            }


            return memberExpression.Member.Name;
        }

        public static string ExtractPropertyName<T>(Expression<Func<T, object>> propertyExpression)
        {
            if (propertyExpression == null)
            {
                throw new ArgumentNullException("propertyExpression");
            }

            LambdaExpression lambda = propertyExpression as LambdaExpression;
            MemberExpression memberExpression;

            if (lambda.Body is UnaryExpression)
            {
                UnaryExpression unaryExpression = lambda.Body as UnaryExpression;
                memberExpression = unaryExpression.Operand as MemberExpression;
            }
            else
            {
                memberExpression = lambda.Body as MemberExpression;
            }

            if (memberExpression == null)
            {
                throw new ArgumentException("NotMemberAccessExpression");
            }

            PropertyInfo propInfo = memberExpression.Member as PropertyInfo;


            if (propInfo == null)
            {
                throw new ArgumentException("ExpressionNotProperty");
            }

            MethodInfo getMethod = propInfo.GetGetMethod(true);

            if (getMethod.IsStatic)
            {
                throw new ArgumentException("StaticExpression");
            }

            return propInfo.Name;


        }
    }
}
