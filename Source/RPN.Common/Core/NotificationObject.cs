﻿using System;
using System.ComponentModel;

using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Diagnostics;

using System.Reactive.Concurrency;
using System.Reactive.Linq;
using RPN.Common.Core.Util;


namespace RPN.Common.Core
{
    /// <summary>
    /// NotificationObject  
    /// </summary>
    [Serializable]
    public abstract class NotificationObject : INotifyPropertyChanged 
    {
       
        [NonSerialized]
        public PropertyChangedEventHandler propertyChanged_;


        public event PropertyChangedEventHandler PropertyChanged
        {
            add { propertyChanged_ += value; }
            remove { propertyChanged_ -= value; }
        }

        public void RaisePropertyChanged(string propertyName)
        {
            CheckPropertyName(propertyName);

            if (propertyChanged_ != null)
            {
                propertyChanged_(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public void RaisePropertyChanged<T>(Expression<Func<T>> expression)
        {
            string propertyName = PropertyHelper.ExtractPropertyName<T>(expression);
            RaisePropertyChanged(propertyName);
        }

        [Conditional("DEBUG")]
        private void CheckPropertyName(string propertyName)
        {
            PropertyDescriptor propertyDescriptor = TypeDescriptor.GetProperties(this)[propertyName];
            if (propertyDescriptor == null)
            {
                throw new InvalidOperationException(string.Format(CultureInfo.InvariantCulture,
                    "The property with the propertyName '{0}' doesn't exist.", propertyName));
            }
        }

        private PropertyInfo GetProperty(string propertyName)
        {
            return this.GetType().GetProperties().FirstOrDefault(property => property.Name == propertyName);
        }
      

       
         
    }
}
