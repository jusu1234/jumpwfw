﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using RPN.Common.Core.Tasks;
using RPN.Common.Core.Tasks.Scheduler;

using System.Reactive;
using System.Reactive.Linq;

namespace RPN.Common.Core.Message
{
    /// <summary>
    ///  ViewModel과 Service간의 중재자. (Mediator Pattern)
    ///  
    /// Object에 RegisterTaskAttribute로 정의된 매소드를 찾아 등록하며
    ///  BroadCast로 메세지 전달시 해당 매소드를 AsynCall 한다. (Task)
    /// 
    /// TODO : Async 실행에 대한 취소 기능.. 
    /// 
    /// TODO : Reactive Extension 스타일로 Async를 구현 
    /// 
    /// </summary>
    public sealed class Mediator
    {
        readonly Dictionary<string, List<WeakAction>> registeredHandlers_;
        readonly double budget_;
        readonly Dictionary<object, Task<object>> taskQueue_;

        readonly TaskFactory backgroundTaskFactory_;
        readonly TaskFactory staTaskFactory_;
        readonly PriorityTaskScheduler priorityScheduler_;

        private static readonly Mediator Instance = new Mediator();
        private Mediator()
        {
            registeredHandlers_ = new Dictionary<string, List<WeakAction>>();
            taskQueue_ = new Dictionary<object, Task<object>>();
            budget_ = 50.0;
            backgroundTaskFactory_ = new TaskFactory(TaskCreationOptions.LongRunning, TaskContinuationOptions.None);
            staTaskFactory_ = new TaskFactory(new LongRunningTaskScheduler(ApartmentState.STA));
            priorityScheduler_ = new PriorityTaskScheduler();

        }

        public static Mediator GetInstance
        {
            get { return Instance; }
        }

        public bool BroadCast(string key, params object[] message)
        {
            List<WeakAction> waList;
            lock (registeredHandlers_)
            {
                if (!registeredHandlers_.TryGetValue(key, out waList))
                {
                    return false;
                }
            }

            foreach (WeakAction wa in waList)
            {
                Delegate action = wa.GetMethod();
                if (action == null)
                    continue;

                switch (wa.TaskType)
                {
                    case TaskType.BackgroundTask:
                        {
              
                            if (!taskQueue_.ContainsKey(wa.Target.Target))
                            {
                                var bgTask = backgroundTaskFactory_.StartNew(() => action.DynamicInvoke(message));
                                taskQueue_.Add(wa.Target.Target, bgTask);
                            }
                        }
                        break;
                    case TaskType.ImmediaticTask:
                        {
                            var immediaticTask = new ImmediaticTask(() => action.DynamicInvoke(message), budget_);
                            immediaticTask.Start(priorityScheduler_);
                        }
                        break;
                    case TaskType.PeriodicTask:
                        {
                            var periodTask = new PeriodTask(() => action.DynamicInvoke(message), budget_);
                            periodTask.Start(priorityScheduler_);
                        }
                        break;
                    case TaskType.LongRunning:
                        {
                            staTaskFactory_.StartNew(() => action.DynamicInvoke(message));
                        }
                        break;
                }
            }


            lock (registeredHandlers_)
            {
                waList.RemoveAll((wa) => wa.HasBeenCollected);
            }

            return true;
        }

        public void RegisterTask(string key, Action handler, TaskType taskType)
        {
            RegisterHandler(key, handler.GetType(), handler, taskType);
        }

        public void UnregisterTask(string key, Action handler)
        {
            UnregisterHandler(key, handler.GetType(), handler);
        }

        public void RegisterTask<T>(string key, Action<T> handler, TaskType taskType)
        {
            RegisterHandler(key, handler.GetType(), handler, taskType);
        }

        public void UnregisterTask<T>(string key, Action<T> handler)
        {
            UnregisterHandler(key, handler.GetType(), handler);
        }


        public void Register(object obj)
        {
            
            // Look at all instance/static methods on this object type.
            foreach (var mi in obj.GetType().GetMethods(BindingFlags.Instance | BindingFlags.Static | BindingFlags.NonPublic | BindingFlags.Public))
            {
                // See if we have a target attribute - if so, register the method as a handler.
                foreach (var att in mi.GetCustomAttributes(typeof(RegisterTaskAttribute), true))
                {
                    var ria = (RegisterTaskAttribute)att;
                    var pi = mi.GetParameters();
                    Type actionType = (pi.Length != 0) ?
                               GetType(pi.Length).MakeGenericType(pi.Select(p => p.ParameterType).ToArray()) : typeof(Action);

                    RegisterHandler(ria.Message, actionType,
                                    mi.IsStatic
                                        ? Delegate.CreateDelegate(actionType, mi)
                                        : Delegate.CreateDelegate(actionType, obj, mi.Name), ria.TaskType);
                }
            }
        }

        public void Unregister(object obj)
        {
            foreach (var mi in obj.GetType().GetMethods(BindingFlags.Instance | BindingFlags.Static | BindingFlags.NonPublic | BindingFlags.Public))
            {
                foreach (var att in mi.GetCustomAttributes(typeof(RegisterTaskAttribute), true))
                {
                    var ria = (RegisterTaskAttribute)att;
                    var pi = mi.GetParameters();
                    Type actionType = (pi.Length != 0) ?
                               GetType(pi.Length).MakeGenericType(pi.Select(p => p.ParameterType).ToArray()) : typeof(Action);

                    UnregisterHandler(ria.Message, actionType,
                                      mi.IsStatic
                                          ? Delegate.CreateDelegate(actionType, mi)
                                          : Delegate.CreateDelegate(actionType, obj, mi.Name));
                }
            }
        }

        public bool IsRegister(object obj, TaskType type)
        {

            foreach (List<WeakAction> waList in registeredHandlers_.Values)
            {
                foreach (WeakAction wa in waList)
                {
                    if (wa.Target.Target == obj && wa.TaskType == type)
                    {
                        return true;
                    }
                }
            }


            return false;
        }

        #region Private Methods
        private void RegisterHandler(string key, Type actionType, Delegate handler, TaskType taskType)
        {
            var action = new WeakAction(handler, actionType, taskType);

            lock (registeredHandlers_)
            {
                List<WeakAction> waList;
                if (registeredHandlers_.TryGetValue(key, out waList))
                {
                    if (waList.Count > 0)
                    {
                        WeakAction wa = waList[0];
                        if (wa.ActionType != actionType &&
                            !wa.ActionType.IsAssignableFrom(actionType))
                            throw new ArgumentException("Invalid key passed to RegisterHandler - existing handler has incompatible parameter type");
                    }

                    waList.Add(action);
                }
                else
                {
                    waList = new List<WeakAction> { action };
                    registeredHandlers_.Add(key, waList);
                }
            }
        }

        private void UnregisterHandler(string key, Type actionType, Delegate handler)
        {
            lock (registeredHandlers_)
            {
                List<WeakAction> waList;
                if (registeredHandlers_.TryGetValue(key, out waList))
                {
/*
                    foreach (WeakAction wa in waList)
                    {
                        if (taskQueue_.ContainsKey(wa.Target.Target))
                        {
                            taskQueue_[wa.Target.Target].Dispose
                        }
                    }
 * */
  
                    waList.RemoveAll(wa => handler == wa.GetMethod() && actionType == wa.ActionType);

                    if (waList.Count == 0)
                    {
                       

                        registeredHandlers_.Remove(key);
                    }
                }
            }
        }

        private static Type GetType(int n)
        {
            switch (n)
            {
                case 1: return typeof(Action<>);
                case 2: return typeof(Action<,>);
                case 3: return typeof(Action<,,>);
                case 4: return typeof(Action<,,,>);
                case 5: return typeof(Action<,,,,>);
                case 6: return typeof(Action<,,,,,>);
                case 7: return typeof(Action<,,,,,,>);
                case 8: return typeof(Action<,,,,,,,>);
                case 9: return typeof(Action<,,,,,,,,>);
                case 10: return typeof(Action<,,,,,,,,,>);
                case 11: return typeof(Action<,,,,,,,,,,>);
                case 12: return typeof(Action<,,,,,,,,,,,>);
                case 13: return typeof(Action<,,,,,,,,,,,,>);
                case 14: return typeof(Action<,,,,,,,,,,,,,>);
                case 15: return typeof(Action<,,,,,,,,,,,,,,>);
                case 16: return typeof(Action<,,,,,,,,,,,,,,,>);
                default: throw new ApplicationException("??");
            }
        }
        #endregion
    }
}
