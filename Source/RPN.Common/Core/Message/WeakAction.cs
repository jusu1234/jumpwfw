﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using RPN.Common.Core.Tasks;


namespace RPN.Common.Core.Message
{
    public sealed class WeakAction
    {
        readonly WeakReference target_;
        readonly Type ownerType_;
        readonly Type actionType_;
        readonly string methodName_;
        readonly TaskType taskType_;

        public WeakAction(Delegate handler, Type actionType, TaskType taskType)
        {
            if (handler.Method.IsStatic)
                ownerType_ = handler.Method.DeclaringType;
            else
                target_ = new WeakReference(handler.Target);

            methodName_ = handler.Method.Name;
            actionType_ = actionType;
            taskType_ = taskType;
        }

        public WeakReference Target
        {
            get { return target_; }
        }

        public TaskType TaskType
        {
            get { return taskType_; }
        }

        public Type ActionType
        {
            get { return actionType_; }
        }

        public bool HasBeenCollected
        {
            get
            {
                return (ownerType_ == null && (target_ == null || !target_.IsAlive));
            }
        }

        public Delegate GetMethod()
        {
            if (ownerType_ != null)
            {
                return Delegate.CreateDelegate(actionType_, ownerType_, methodName_);
            }

            if (target_ != null && target_.IsAlive)
            {
                object target = target_.Target;
                if (target != null)
                {
                    return Delegate.CreateDelegate(actionType_, target, methodName_);
                }
            }

            return null;
        }

    }
}
