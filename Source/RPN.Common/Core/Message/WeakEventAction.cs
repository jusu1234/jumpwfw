﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Reflection;

namespace RPN.Common.Core.Message
{
    public class WeakEventAction
    {
        readonly MethodInfo method_;
        readonly Type delegateType_;

        public WeakReference TargetObject { get; private set; }

        public WeakEventAction(object target, MethodInfo method, Type parameterType)
        {
            TargetObject = new WeakReference(target);

            method_ = method;
            delegateType_ = parameterType == null ? typeof(Action) : typeof(Action<>).MakeGenericType(parameterType);
        }

        internal Delegate CreateAction()
        {
            var target = TargetObject.Target;
            if (target != null)
            {
                return Delegate.CreateDelegate(delegateType_, TargetObject.Target, method_);
            }

            return null;
        }
    }


    public class WeakEventAction<T>
    {
        readonly List<WeakEventAction> delegates_ = new List<WeakEventAction>();


        public static WeakEventAction<T> operator +(WeakEventAction<T> wre, Action<T> handler)
        {
            wre.Add(handler);
            return wre;
        }

        public static WeakEventAction<T> operator -(WeakEventAction<T> wre, Action<T> handler)
        {
            wre.Remove(handler);
            return wre;
        }

        public void Add(Action<T> handler)
        {
            var parameters = handler.Method.GetParameters();

            if (parameters != null && parameters.Length > 1)
            {
                throw new InvalidOperationException("Action Should have 0 or 1 Parameter");
            }

            if (delegates_.Any((we) => we.TargetObject.Target == handler.Target))
            {
                return;
            }

            var parameterType = (parameters == null || parameters.Length == 0) ? null : parameters[0].ParameterType;

            delegates_.Add(new WeakEventAction(handler.Target, handler.Method, parameterType));
        }

        public void Remove(Action<T> handler)
        {
            foreach (WeakEventAction we in delegates_)
            {
                if (we.TargetObject.Target == handler.Target)
                {
                    delegates_.Remove(we);
                    return;
                }
            }
        }

        internal void Invoke(T arg)
        {
            for (var i = delegates_.Count; i > -1; --i)
            {
                var weakAction = delegates_[i];
                if (!weakAction.TargetObject.IsAlive)
                {
                    delegates_.RemoveAt(i);
                }
                else
                {
                    var action = weakAction.CreateAction();
                    action.DynamicInvoke(arg);
                }
            }
        }
    }
}
