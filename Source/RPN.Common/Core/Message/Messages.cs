﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RPN.Common.Core.Message
{
    public class Messages
    {

        public const string LoadViews = "LoadViews";
        public const string UnloadViews = "UnloadViews";

        public const string StateUpdate = "StateUpdate";
    }
}
