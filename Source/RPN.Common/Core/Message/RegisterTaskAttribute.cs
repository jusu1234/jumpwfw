﻿using System;
using RPN.Common.Core.Tasks;

namespace RPN.Common.Core.Message
{
    [AttributeUsage(AttributeTargets.Method)]
    public sealed class RegisterTaskAttribute : Attribute
    {
        public string Message { get; private set; }

        public TaskType TaskType { get; private set; }

        public TimeSpan DelayTime { get; private set; }

        public TimeSpan PeriodTime { get; private set; }

        public RegisterTaskAttribute(string message, TaskType taskType)
        {
            Message = message;
            TaskType = taskType;

            DelayTime = TimeSpan.Zero;
            PeriodTime = TimeSpan.Zero;
        }
    }
}
