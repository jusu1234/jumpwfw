﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

using RPN.Common.Core.Message;


namespace RPN.Common.Core.Service
{
    
    // TODO : 서비스 종료여부 통보..
    public abstract class StatusService
    {
        public const int NONE = 0;
        public const int START = 1;
        public const int STOP = 2;
        public const int PAUSE = 3;
        public const int RESUME = 4;

        public int Control { get; private set; }

        protected ManualResetEvent startEvent_;
        protected ManualResetEvent pauseEvent_;
        protected bool shutDown_;

        protected StatusService()
        {
            Control = NONE;
            startEvent_ = new ManualResetEvent(false);
            pauseEvent_ = new ManualResetEvent(true);
            shutDown_ = false;
        }

        protected virtual bool ControlService(int ctl)
        {
            bool res = true;
            if (Control == ctl)
            {
                res = false;
            }
            else if(Control ==  NONE && ctl == START  )
            {  
                // 시작
                Control = START;
                startEvent_.Set();
            }
            else if ( (Control == START || Control == RESUME) &&
                ctl == PAUSE)
            {
                // 일시정시
                pauseEvent_.Reset();
                Control = PAUSE;
            }
            else if ( (Control != NONE && Control != STOP ) &&
                ctl == STOP)
            {
                // 종료
                shutDown_ = true;
                pauseEvent_.Set();

                Control = STOP;
            }
            else if (Control == PAUSE && ctl == RESUME)
            {
                // 재시작
                pauseEvent_.Set();
                Control = RESUME;
            }
            else
            {

                res = false;
            }

            return res;
        }


    }
}
