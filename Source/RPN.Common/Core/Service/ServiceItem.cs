﻿using System;


namespace RPN.Common.Core.Service
{
    public class ServiceItem
    {
        public string Key       { get; private set; }
        public object Value     { get; private set; }


        public ServiceItem(string key, object value, int flushCount = 0)
        {
            Key = key;
            Value = value;

        }

       
    }
}
