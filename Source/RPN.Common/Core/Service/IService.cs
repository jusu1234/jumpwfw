﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using RPN.Common.Core.Message;

namespace RPN.Common.Core.Service
{
    

    public interface IService
    {
        string ServiceName { get; set; }
        
        event EventHandler<ServiceEventArgs> DataReceived;
    }
}
