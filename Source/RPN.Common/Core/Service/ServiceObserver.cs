﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.Text;

using System.Reactive;
using System.Reactive.Linq;

using RPN.Common.Logging;
using RPN.Common.Core.Message;
using RPN.Common.Core.Data;
using RPN.Common.Core.RX;

namespace RPN.Common.Core.Service
{

    /// <summary>
    ///  Service에서 발생시키는 데이터 이벤트를 확인하여 (RX)
    ///  내부적으로 호출한다.
    /// </summary>
    public abstract class ServiceObserver
    {

        public string ServiceName { get; set; }

        readonly int observer_period_ = 2000;
        Action<IEnumerable<object>> onEvent_;

 

        protected ServiceObserver()
        {
        }

        protected void RegisterEvent(Action<IEnumerable<object>> OnEvent)
        {
            onEvent_ = OnEvent;
        }

        public void RegisterService(IService service)
        {
         

            /*
             * ServiceEvent로 받아온 데이터를 버퍼링하여
             * 지정된 개수 이상이 버퍼에 차면 버퍼를 flush시킨다. 
             */

            var observer = 
                Observable.FromEventPattern<ServiceEventArgs>((h) => service.DataReceived += h, (h) => service.DataReceived -= h);

            
            observer.Select(x => x.EventArgs.Value)
                  .Where(x => x.Key != null)
                 .BufferWithTimeout(TimeSpan.FromMilliseconds(observer_period_), 20)
                 .Where(x => x.Count > 0)
                 .Subscribe(DataRecevied, OnError);

        }

        public void RegisterService(IEnumerable<IService> services)
        {
            var observer = services.Select((s) =>
                Observable.FromEventPattern<ServiceEventArgs>((h) => s.DataReceived += h, (h) => s.DataReceived -= h))
                .ToList().Merge();

            observer.Select(x => x.EventArgs.Value)
                .Where(x => x.Key != null)
                .Buffer(TimeSpan.FromMilliseconds(observer_period_))
                .Subscribe(DataRecevied, OnError);
        }

      

        protected virtual void OnError(Exception ex)
        {
        }

        protected virtual void DataRecevied(IList<ServiceItem> serviceItems)
        {
            /*
            int flushCount = 0;

            foreach(ServiceItem item in serviceItems)
            {
                EntityKey key = new EntityKey(item.Key);
                var value = item.Value;
                flushCount = item.FlushCount;
                Entity entity;

                bool isExsiting = buffer_.TryGetValue(key, out entity);

                if (!isExsiting)
                {
                    entity = new Entity(key);
                    buffer_.TryAdd(key, entity);
                }

                if(entity.Value == null)
                {
                    entity.Value = new EntityItem(value);
                }
                else{
                    entity.Value = new EntityItem(value, entity.Value.OldValue);
                }

            //    bufferQueue_.Enqueue(entity);
                buffer_.AddOrUpdate(entity.Key, entity, (n, oldValue) => entity); 
            }


           // if (bufferQueue_.Count == 40)
            {

                onEvent_(
                           buffer_.Values);

            //    Entity result;
            //    while (bufferQueue_.TryDequeue(out result)) ;
            }
             */

            onEvent_(
                      serviceItems.Select(s => s.Value).ToList());
          
        }


    }
}
