﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RPN.Common.Core.Service
{
    public sealed class ServiceEventArgs : EventArgs
    {
        readonly ServiceItem value_;

        public ServiceEventArgs(ServiceItem value)
        {
            value_ = value;
        }

        public ServiceItem Value
        {
            get { return value_; }
        }
    }
}
