﻿using System;
using System.Linq.Expressions;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;

using RPN.Common.Core.RX;
using System.Reactive;

namespace RPN.Common.Core
{
    public class PropertyObserver<TPropertySource>  where TPropertySource : INotifyPropertyChanged
    {
        readonly Dictionary<string, IDisposable> propertyNameToObserverMap_;
        readonly WeakReference propertySourceRef_;


       
        public PropertyObserver(TPropertySource source)
        {
            if (source == null)
            {
                throw new ArgumentNullException("source");
            }

           propertySourceRef_ = new WeakReference(source);


           propertyNameToObserverMap_ = new Dictionary<string, IDisposable>();
        }

        TPropertySource GetPropertySource()
        {
            
            try
            {
                return (TPropertySource)propertySourceRef_.Target;
                
            }
            catch
            {
                return default(TPropertySource);
            }
             

            
        }

        /*

        public PropertyObserver<TPropertySource> AddHandler(Expression<Func<TPropertySource>> expression, Action<TPropertySource, EventArgs> handler)
        {
            var propertyName = PropertyHelper.ExtractPropertyName<TPropertySource>(expression);
            return AddHandler(propertyName, handler);
        }
         */

        public PropertyObserver<TPropertySource> AddHandler(Expression<Func<TPropertySource, object>> expression, Action<TPropertySource, EventArgs> handler)
        {
            var propertyName = PropertyHelper.ExtractPropertyName<TPropertySource>(expression);

            return AddHandler(propertyName, handler);
        }



        public PropertyObserver<TPropertySource> AddHandler(string propertyName, Action<TPropertySource, EventArgs> handler)
        {
            TPropertySource source = this.GetPropertySource();

            if (source != null)
            {
                var subscriber = source.WhenPropertyChanged(propertyName).Subscribe(e => { handler(source, e.EventArgs); });
                propertyNameToObserverMap_.Add(propertyName, subscriber); 
            }

            return this;
        }

        
        public PropertyObserver<TPropertySource> RemoveHandler(string propertyName, Action<TPropertySource, EventArgs> handler)
        {
            TPropertySource source = this.GetPropertySource();

            if (source != null)
            {
                if (propertyNameToObserverMap_.ContainsKey(propertyName))
                {
                    propertyNameToObserverMap_[propertyName].Dispose();
                    propertyNameToObserverMap_.Remove(propertyName);
                }
            }

            return this;
        }


        
        
    }
}
