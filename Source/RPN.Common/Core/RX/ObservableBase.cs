﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RPN.Common.Core
{
    /// <summary>
    /// <para> Observerable Pattern </para>
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// 
    /// <remarks> 
    /// 이것을 구현하기 위해 다음을 참고 하였다.
    /// <list type="bullet">  
    /// <listheader>참고 사이트</listheader>  
    /// <item>  
    /// <description> http://www.remondo.net/observer-pattern-example-csharp-iobservable </description>  
    /// </item>  
    /// </list>  
    /// </remarks> 
    /// </description>
    /// <seealso cref="PropertyObserver"/> 
    /// 
    public abstract class ObservableBase<T> : IObservable<T>, IDisposable
    {
        private readonly HashSet<IObserver<T>> subscribers_ = new HashSet<IObserver<T>>();
        private readonly object lock_ = new object();
        private int key_;
        private bool isDisposed_;

        public void Dispose()
        {
            Dispose(true);
        }


        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                OnCompleted();
                isDisposed_ = true;
            }
        }

        protected void OnNext(T value)
        {
            if (isDisposed_)
            {
                throw new ObjectDisposedException("Observable<T>");
            }

            foreach (IObserver<T> observer in subscribers_)
            {
                observer.OnNext(value);
            }
        }

        protected void OnError(Exception exception)
        {
            if (isDisposed_)
            {
                throw new ObjectDisposedException("Observable<T>");
            }

            if (exception == null)
            {
                throw new ArgumentNullException("exception");
            }

            foreach (IObserver<T> observer in subscribers_)
            {
                observer.OnError(exception);
            }
        }

        protected void OnCompleted()
        {
            if (isDisposed_)
            {
                throw new ObjectDisposedException("Observable<T>");
            }

            foreach (IObserver<T> observer in subscribers_)
            {
                observer.OnCompleted();
            }
        }

        public IDisposable Subscribe(IObserver<T> observer)
        {
            if (!subscribers_.Contains(observer))
            {
                subscribers_.Add(observer);
            }

           
            lock(lock_)
            {
                return new Unsubsriber<IObserver<T>>((o) =>
                    {
                        lock (lock_)
                        {
                            subscribers_.Remove(o);
                        }
                    }
                    , observer
                );
            }

        }
    }


    public class Unsubsriber<T> : IDisposable
    {
        private Action<T> action_;
        private T obj_;

        public Unsubsriber(Action<T> action, T obj)
        {
            action_ = action;
            obj_ = obj;
        }

        public void Dispose()
        {
            action_(obj_);
        }

    }
}
