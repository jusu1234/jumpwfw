﻿using System;
using System.Collections.Generic;
using System.Linq;

using System.Reactive;
using System.Reactive.Linq;
using System.Reactive.Concurrency;
using System.Reactive.Disposables;

using RPN.Common.Logging;

namespace RPN.Common.Core.RX
{
    /// <summary>
    /// <para> RX Obervable 확장 </para>
    /// </summary>
    public static class ObservableExtension
    {

        public static Logger log = LoggerFactory.GetLogger(typeof(ObservableExtension));

        /**
         * 
         * 주기적으로 버퍼를 flush 작업중.. 
         * Null 값 체크
         * 
         * TODO : 주기적으로 데이터가 있든 없든 Observable.subscribe의 action을 실행시키는 기능 구현
         *     UI 업데이트 및 주기 체크용..
         */

        public static IObservable<IList<TSource>> BufferWithTime<TSource>(
            this IObservable<TSource> source, TimeSpan period)
        {
            return source.BufferWithTime(period,  SchedulerService.TaskPool);
        }


        public static IObservable<IList<TSource>> BufferWithTime<TSource>(
          this IObservable<TSource> source, TimeSpan period,  IScheduler scheduler)
        {
            return Observable.Create<IList<TSource>>(observer =>
            {
                object lockObj = new object();
                List<TSource> buffer = new List<TSource>();

                List<TSource> def = new List<TSource>(); 
                
                SerialDisposable periodDisposable = new SerialDisposable();

                ulong seq = 0;

                Action createTimer = () =>
                   {

                       ulong startSeq = seq;
          

                       periodDisposable.Disposable =  scheduler.Schedule(period,
                           (Action<TimeSpan> self) =>
                           {

                               bool noChanged;
                               IList<TSource> values = null;

                               lock (lockObj)
                               {
                                   noChanged = seq == startSeq;

                                   if (noChanged)
                                   {
                                       values = buffer.ToList();
                                       buffer.Clear();
                                   }
                                   else
                                   {
                                       

                                       
                                   }
                               }

                               if (noChanged)
                               {
                                   log.Debug("NoChanged");
                                   observer.OnNext(def);
                                   self(period);
                               }
                               else
                               {
                                   log.Debug("Changed");
                                   observer.OnNext(values);
                               }

                           }
                       );
                   };

                createTimer();

                var Subscription = source.Subscribe(
                    value =>
                    {
                        lock (lockObj)
                        {
                            seq += 1UL;
                            buffer.Add(value);
                    
                        }

                        createTimer();
                        
                    },
                    observer.OnError,
                    () =>
                    {
                        observer.OnNext(buffer);
                        observer.OnCompleted();
                    }
                );

                return new CompositeDisposable(periodDisposable, Subscription );

            });
        
        }


        /**
        * 
        * 버퍼에 데이터가 들어오면 일정시간 후에 강제로 flush
         * */
        public static IObservable<IList<TSource>> BufferWithTimeout<TSource>(
            this IObservable<TSource> source, TimeSpan timeout, int count = 0)
        {
            return source.BufferWithTimeout(timeout, count,  SchedulerService.TaskPool);
        }


        public static IObservable<IList<TSource>> BufferWithTimeout<TSource>(
            this IObservable<TSource> source, TimeSpan timeout, int count, IScheduler scheduler)
        {
            return Observable.Create<IList<TSource>>(observer =>
            {
                object lockObj = new object();
                List<TSource> buffer = new List<TSource>();

                MultipleAssignmentDisposable timeoutDisposable = new MultipleAssignmentDisposable();

                Action flushBuffer = () =>
                {
                    IList<TSource> values;

                    lock (lockObj)
                    {
                        if (count > 0 && buffer.Count >= count
                              || count == 0)
                        {
                            values = buffer.ToList();
                            buffer.Clear();
                        }
                        else
                        {
                            return;
                        }
                      
                    }

                     observer.OnNext(values);
          
                };

                var Subscription = source.Subscribe(
                    value =>
                    {
                        lock (lockObj)
                        {
                            buffer.Add(value);
                        }

                        timeoutDisposable.Disposable = scheduler.Schedule(timeout, flushBuffer);
                    },
                    observer.OnError,
                    () =>
                    {
                        observer.OnNext(buffer);
                        observer.OnCompleted();
                    }
                );

                return new CompositeDisposable(Subscription, timeoutDisposable);

            });
        }
    }
}
