﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Reactive.Linq;
using System.Threading.Tasks;

namespace RPN.Common.Core.RX
{
    public class Pararell
    {       

        // Foreach 
        public static void ForEach<T>(IEnumerable<T> sources, Action<T> body)
        {
            
            sources.ToObservable()
                .SelectMany(x => Observable.Start(() => body(x), SchedulerService.TaskPool))
                .ToList()
                .First();
            
        }

    }
}
