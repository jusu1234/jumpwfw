﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;


using System.Reactive.Concurrency;
using System.Reactive.Linq;
using System.Reactive.PlatformServices;

namespace RPN.Common.Core.RX
{
   
    internal class SchedulerService
    {
        public static IScheduler CurrentThread
        {
            get { return (IScheduler)CurrentThreadScheduler.Instance; }
        }

        public static IScheduler ImmeadiateThread
        {
            get { return (IScheduler)ImmediateScheduler.Instance; }
        }

        public static IScheduler Dispatcher
        {
            get { return (IScheduler) DispatcherScheduler.Current; }
        }

        public static IScheduler NewThread
        {
            get { return (IScheduler) NewThreadScheduler.Default; } 
        }

        public static IScheduler ThreadPool
        {
            get { return (IScheduler)ThreadPoolScheduler.Instance; }
        }

        public static IScheduler TaskPool
        {
            get { return (IScheduler)TaskPoolScheduler.Default; }
        }

        
    }
}
