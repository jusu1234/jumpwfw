﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Windows;


using System.Reactive;
using System.Reactive.Linq;

using RPN.Common.Core;

namespace RPN.Common.Core.RX
{
    /// <summary>
    ///   PropertyChangedEvent Observable
    /// </summary>
    /// <remarks>
    ///   참고할만한 사이트
    ///   http://stackoverflow.com/questions/1747235/weak-event-handler-model-for-use-with-lambdas
    /// </remarks>

    public static class PropertyChangeExtension
    {

        

        public static IObservable<EventPattern<PropertyChangedEventArgs>> WhenPropertyChanged<TProperty>(this TProperty property, string propertyName)
            where TProperty : INotifyPropertyChanged
        {
            return property.FromPropertyChanged().Where(p => p.EventArgs.PropertyName.Equals(propertyName));
        }

        
        public static IObservable<EventPattern<PropertyChangedEventArgs>> WhenPropertyChanged<TProperty>(this TProperty property, Expression<Func<TProperty>> propertyExpression) 
            where TProperty : INotifyPropertyChanged
        {
            return property.FromPropertyChanged().Where(p => p.EventArgs.PropertyName == PropertyHelper.ExtractPropertyName(propertyExpression));
        }
        

        public static IObservable<EventPattern<PropertyChangedEventArgs>> WhenPropertyChanged<TProperty>(this TProperty property, Expression<Func<TProperty, object>> propertyExpression)
            where TProperty : INotifyPropertyChanged
        {
            return property.FromPropertyChanged().Where(p => p.EventArgs.PropertyName == PropertyHelper.ExtractPropertyName(propertyExpression));
        }


        private static IObservable<EventPattern<PropertyChangedEventArgs>> FromPropertyChanged(this INotifyPropertyChanged property)
        {
            return Observable.FromEventPattern<PropertyChangedEventHandler, PropertyChangedEventArgs>(
                h => new PropertyChangedEventHandler(h),
                h => property.PropertyChanged += h,
                h => property.PropertyChanged -= h
                    );
        }
    }
}
