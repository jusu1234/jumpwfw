﻿using System;
using System.Collections.Generic;

namespace RPN.Common.Util
{
    public abstract class LazySingleton<T> where T : new()
    {
        private static Lazy<T> instance;

        private LazySingleton() { }

        static LazySingleton()
        {
            instance = new Lazy<T>(true);
        }

        public static Lazy<T> Instance
        {
            get { return instance; }
        }
    }
}
