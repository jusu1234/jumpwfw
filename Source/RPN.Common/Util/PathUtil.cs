﻿using System;
using System.Text;
using System.IO;
using System.Reflection;


namespace RPN.Common.Util
{
    public class PathUtil
    {

        public static readonly string USER_DATA_DIR = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData),
            Assembly.GetExecutingAssembly().GetName().Name);

        /// <summary>
        /// 파일경로에서 파일명 부분을 추출
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        static public string getFileName(string filePath)
        {

            if (string.IsNullOrEmpty(filePath))
                return string.Empty;


            int idx = filePath.LastIndexOf(Path.DirectorySeparatorChar);

            if (idx != -1)
            {
                if (idx < filePath.Length)
                    return filePath.Substring(filePath.LastIndexOf(Path.DirectorySeparatorChar) + 1);
                else
                    return string.Empty;
            }
            else
                return filePath;
        }

        /// <summary>
        /// 파일경로에서 확장자를 제외한 파일명 부분을 추출
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        static public string getNotExtentionFileName(string filePath)
        {
            filePath = getFileName(filePath);

            int idx = filePath.LastIndexOf('.');

            if (idx != -1)
            {
                if (idx < filePath.Length)
                    return filePath.Substring(0,idx);
                else
                    return string.Empty;
            }
            else
                return filePath;

        }
    }
}
