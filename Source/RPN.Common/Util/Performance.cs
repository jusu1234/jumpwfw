﻿using System;
using System.Threading;
using System.Diagnostics;

namespace RPN.Common.Util
{
    public class Performance
    {
        interface IStopWatch
        {
            bool IsRunning { get; }
            TimeSpan Elapsed { get; }

            void Start();
            void Stop();
            void Reset();
        }


        class TimeWatch : IStopWatch
        {
            Stopwatch stopwatch = new Stopwatch();

            public TimeSpan Elapsed
            {
                get { return stopwatch.Elapsed; }
            }

            public bool IsRunning
            {
                get { return stopwatch.IsRunning; }
            }



            public TimeWatch()
            {
                if (!Stopwatch.IsHighResolution)
                    throw new NotSupportedException("Your hardware doesn't support high resolution counter");

                //prevent the JIT Compiler from optimizing Fkt calls away
                long seed = Environment.TickCount;

                //use the second Core/Processor for the test
                Process.GetCurrentProcess().ProcessorAffinity = new IntPtr(2);

                //prevent "Normal" Processes from interrupting Threads
                Process.GetCurrentProcess().PriorityClass = ProcessPriorityClass.High;

                //prevent "Normal" Threads from interrupting this thread
                Thread.CurrentThread.Priority = ThreadPriority.Highest;
            }

            public void Start()
            {
                stopwatch.Start();
            }

            public void Stop()
            {
                stopwatch.Stop();
            }

            public void Reset()
            {
                stopwatch.Reset();
            }

        }


        class CpuWatch : IStopWatch
        {
            TimeSpan startTime;
            TimeSpan endTime;
            bool isRunning;

            public TimeSpan Elapsed
            {
                get
                {
                    if (IsRunning)
                        throw new NotImplementedException("Getting elapsed span while watch is running is not implemented");

                    return endTime - startTime;
                }
            }

            public bool IsRunning
            {
                get { return isRunning; }
            }



            public void Start()
            {
                startTime = Process.GetCurrentProcess().TotalProcessorTime;
                isRunning = true;
            }

            public void Stop()
            {
                endTime = Process.GetCurrentProcess().TotalProcessorTime;
                isRunning = false;
            }

            public void Reset()
            {
                startTime = TimeSpan.Zero;
                endTime = TimeSpan.Zero;
            }
        }

        TimeWatch timeWatch_;
        CpuWatch cpuWatch_;

        public Performance()
        {
            timeWatch_ = new TimeWatch();
            cpuWatch_ = new CpuWatch();
        }

        public void BeginTimeWatch()
        {
            timeWatch_.Start();
        }

        public void EndTimeWatch()
        {
            timeWatch_.Stop();

        }

        public TimeSpan GetTimeWatchDuration()
        {
            return timeWatch_.Elapsed;
        }

        public void BegionCpuWatch()
        {
            cpuWatch_.Start();
        }

        public void EndCpuWatch()
        {
            cpuWatch_.Stop();
        }

        public TimeSpan GetCpuWatchDuration()
        {
            return cpuWatch_.Elapsed;
        }

        public static TimeSpan CheckExceuteTime(Action action)
        {
            var stopwatch = new TimeWatch();
            stopwatch.Start();
            action();
            stopwatch.Stop();

            return stopwatch.Elapsed;

        }

        public static TimeSpan checkExecuteCPU(Action action)
        {
            var stopwatch = new CpuWatch();
            stopwatch.Start();
            action();
            stopwatch.Stop();

            return stopwatch.Elapsed;
        }


       


    }

}
