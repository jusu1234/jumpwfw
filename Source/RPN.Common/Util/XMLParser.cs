﻿using System;
using System.Collections.Generic;
using System.Xml;
using System.IO;

namespace RPN.Common.Util
{
    /// <summary>
    ///  XML SAX Parser
    /// </summary>
    public class XMLParser
    {
        private IXMLHandler handler_;

        public XMLParser(IXMLHandler handler)
        {
            handler_ = handler;
        }


        public bool parseString(string xmlStr)
        {
            XmlReader xr = XmlReader.Create(new StringReader(xmlStr));

            return parse(xr);
        }

        private bool parseFile(string fileName)
        {
            XmlTextReader xtr = new XmlTextReader(fileName);

            return parse(xtr);
        }

        private bool parse(XmlReader xr)
        {
            try
            {
                
                String localName, name, baseuri;

                handler_.startDocument();

                while (xr.Read())
                {
                    switch (xr.NodeType)
                    {
                        case XmlNodeType.Element:
                            {
                                Attributes attr = new Attributes();


                                baseuri = xr.BaseURI;
                                localName = xr.LocalName;
                                name = xr.Name;

                                if (xr.HasAttributes)
                                {
                                    while (xr.MoveToNextAttribute())
                                    {
                                        attr.addValue(xr.Name, xr.Value);
                                    }
                                }

                                handler_.startElement(baseuri, localName, name, attr);

                            }
                            break;

                        case XmlNodeType.EndElement:
                            {
                                handler_.endElement(xr.BaseURI, xr.LocalName, xr.Name);
                            }
                            break;
                        case XmlNodeType.Text:
                            {
                                char[] ch = xr.Value.ToCharArray();

                                handler_.characters(ch, 0, ch.Length);
                            }
                            break;
                    }
                }

                handler_.endDocument();

                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

    }

    public class Attributes
    {
        private Dictionary<String, String> dic_;

        public Attributes()
        {
            dic_ = new Dictionary<string, string>();
        }

        public void addValue(String key, String value)
        {
            dic_.Add(key, value);
        }

        public String getValue(String key)
        {
            return dic_[key];
        }
    }

    public interface IXMLHandler
    {
        void startDocument();

        void endDocument();

        void startElement(String uri, String localName, String name, Attributes attributes);

        void endElement(String uri, String localName, String name);

        void characters(char[] ch, int start, int length);
    }
}
