﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using System.IO;

using InnowatchDataHandler.Commons.DataService;

namespace RPN.Common.Util
{
    public class RestServiceHelper
    {
        static public string DATA_SERVICE_URL = "http://localhost:25000/rest/data/";

        static private RequestParameter.CharacterSetEncodingOption URLEncoding =
            RequestParameter.CharacterSetEncodingOption.UTF8;


        static public string Request(string cmd, bool isPost = false)
        {
            string result = string.Empty;

            RequestParameter req = new RequestParameter();

            req.EncodingOption = URLEncoding.ToString();
            req.Url = cmd;

            return result = DataServiceHandler.Request(req, isPost);
        }

        static public DataTable RequestToDataTable(string cmd, bool isPost = false)
        {

            string result = Request(cmd, isPost);

            var encodingOpt = GetEncodingOption("UTF8");
            DataSet ds = null;

            try
            {
                ds = new DataSet();

                var stream = new MemoryStream(encodingOpt.GetBytes(result));
                ds.ReadXml(stream);


                return ds.Tables.Count > 0 ? ds.Tables[0] : null;
            }
            catch (Exception ex)
            {

                return null;
            }
        }

        static public string RequestByPost(string method, string _PostMessage)
        {
            string result = string.Empty;
            RequestParameter req = new RequestParameter()
            {
                Url = DATA_SERVICE_URL + "/" + method,
                EncodingOption = URLEncoding.ToString(),
                PostMessage = _PostMessage

            };

            return result = DataServiceHandler.Request(req, true);
        }


        private static Encoding GetEncodingOption(string encondingOption)
        {
            Encoding encodingOption;

            // Contents에 지정된 EncodingOption을 가져와서 설정한다.
            switch (encondingOption)
            {
                case "ASCII":
                    encodingOption = Encoding.ASCII;
                    break;
                case "BigEndianUnicode":
                    encodingOption = Encoding.BigEndianUnicode;
                    break;
                case "Unicode":
                    encodingOption = Encoding.Unicode;
                    break;
                case "UTF32":
                    encodingOption = Encoding.UTF32;
                    break;
                case "UTF7":
                    encodingOption = Encoding.UTF7;
                    break;
                case "UTF8":
                    encodingOption = Encoding.UTF8;
                    break;
                case "Default":
                    encodingOption = Encoding.Default;
                    break;
                default:
                    encodingOption = Encoding.UTF8;
                    break;
            }

            return encodingOption;
        }


    }
}
