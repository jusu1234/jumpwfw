﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;

using Common.Logging;
using Common.Logging.NLog;


namespace RPN.Common.Logging
{
    /// <summary>
    /// <para> Common Logging Wrapper  </para>
    /// </summary>
    public class Logger 
    {
        private ILog log_;


        public Logger(string name)
        {
            log_ = LogManager.GetLogger(name);
        }
     

        public void Trace(string msg)
        {
            log_.Trace(msg);
        }

        public void Trace(string msg, Exception ex)
        {
            log_.Trace(msg, ex);
        }

        public void Debug(string msg)
        {
            log_.Debug(msg);
        }

        public void Debug(string msg, Exception ex)
        {
            log_.Debug(msg, ex);
        }

        public void Info(string msg)
        {
            log_.Info(msg);
        }

        public void Info(string msg, Exception ex)
        {
            log_.Info(msg, ex);
        }

        public void Warn(string msg)
        {
            log_.Warn(msg);
        }

        public void Warn(string msg, Exception ex)
        {
            log_.Warn(msg, ex);
        }

        public void Error(string msg)
        {
            log_.Error(msg);
        }

        public void Error(string msg, Exception ex)
        {
            log_.Error(msg, ex);
        }

        public void Fatal(string msg)
        {
            log_.Fatal(msg);
        }


        public void Fatal(string msg, Exception ex)
        {
            log_.Fatal(msg, ex);
        }


       

    }
}
