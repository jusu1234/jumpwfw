﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;

using Common.Logging;
using Common.Logging.NLog;
using System.IO;

namespace RPN.Common.Logging
{
    public  class LoggerFactory
    {
        private static Dictionary<string, Logger> s_Loggers;

        static LoggerFactory()
        {
            s_Loggers = new Dictionary<string, Logger>();
            string LogFile = "~/Logging/NLog.config";

            if (!File.Exists("~/Logging/NLog.config"))
            {
                File.Create("~/Logging/NLog.config");
            }

            NameValueCollection properties = new NameValueCollection();
            properties["configType"] = "FILE";
            properties["configFile"] = LogFile;

            LogManager.Adapter = new NLogLoggerFactoryAdapter(properties);
            
        }


        public static Logger GetLogger(Type tp)
        {
            return GetLogger(tp.Name);
        }

        public static Logger GetLogger(string name)
        {
            
            if(!s_Loggers.ContainsKey(name))
            {
                s_Loggers.Add(name, new Logger(name));
            }

            return s_Loggers[name];

             
        }
    }
}
