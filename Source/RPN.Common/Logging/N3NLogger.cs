﻿using System;
using System.Text;
using System.IO;
using System.Reflection;
using System.Threading;

using RPN.Common.Util;

namespace RPN.Common.Logging
{
    public class N3NLogger
    {

        public enum LogOutput
        {
            LOG_NONE = 0,
            LOG_CONSOLE_OUTPUT = 1,
            LOG_DEBUG_OUTPUT = 1 << 1,
            LOG_FILE_OUTPUT = 1 << 2
        };

        public enum LogLevel
        {
            LOG_DEBUG = 0,
            LOG_INFO = 1,
            LOG_WARN = 2,
            LOG_ERROR = 3,
            LOG_FATAL = 4
        };


        static private N3NLogger sInstance_ = null;

        static private object lock_object_ = new object();

        private LogLevel lvl_;
        private LogOutput output_;

        private String prefix_;
        private FileStream log_file_;

        /// <summary>
        /// 로그 클래스 초기화 (기본값)
        /// </summary>
        static public void initLogger()
        {
            sInstance_ = new N3NLogger();
        }
        /// <summary>
        ///  로그 클래스 초기화
        /// </summary>
        /// <param name="fileName">로그 파일명</param>
        /// <param name="lvl"> 로그 레벨</param>
        /// <param name="output">로그 출력</param>
        static public void InitLogger(String fileName, LogLevel lvl, LogOutput output)
        {
            sInstance_ = new N3NLogger(fileName, lvl, output);
        }


        static public void Debug(String msg)
        {
            sInstance_.Write(LogLevel.LOG_DEBUG, msg);
        }

        static public void Info(String msg)
        {
            sInstance_.Write(LogLevel.LOG_INFO, msg);
        }

        static public void Warn(String msg)
        {
            sInstance_.Write(LogLevel.LOG_WARN, msg);
        }

        static public void Error(String msg)
        {
            sInstance_.Write(LogLevel.LOG_ERROR, msg);
        }

        static public void Fatal(String msg)
        {
            sInstance_.Write(LogLevel.LOG_FATAL, msg);
        }


        private void Write(LogLevel lvl, String msg)
        {

            if (lvl < lvl_)
                return;

            if (initLogFile() && output_ != LogOutput.LOG_NONE)
            {


                if (lvl_ < LogLevel.LOG_WARN)  // Call Stack Logging
                {

                }


                StringBuilder sb = new StringBuilder();

                writeHeader(sb);
                sb.Append(msg);

                if ((output_ & LogOutput.LOG_CONSOLE_OUTPUT) == LogOutput.LOG_CONSOLE_OUTPUT)
                {
                    Console.WriteLine(sb.ToString());
                }

                if ((output_ & LogOutput.LOG_DEBUG_OUTPUT) == LogOutput.LOG_DEBUG_OUTPUT)
                {
                    System.Diagnostics.Debug.WriteLine(sb.ToString());
                }

                if ((output_ & LogOutput.LOG_FILE_OUTPUT) == LogOutput.LOG_FILE_OUTPUT)
                {

                    //파일에 쓸때 동기화

                    lock (lock_object_)
                    {
                        StreamWriter sw = new StreamWriter(log_file_);
                        sw.WriteLine(sb.ToString());

                        sw.Flush();
                        //  sw.Dispose();
                        //  sw.Close();
                    }
                }


                if (lvl_ == LogLevel.LOG_FATAL)
                {
                    // 프로그램 종료
                }


            }

        }

        private N3NLogger() :
            this("debug", LogLevel.LOG_DEBUG, LogOutput.LOG_DEBUG_OUTPUT | LogOutput.LOG_FILE_OUTPUT)
        {

        }



        private N3NLogger(String prefix, LogLevel lvl, LogOutput output)
        {
            log_file_ = null;

            prefix_ = prefix;
            lvl_ = lvl;
            output_ = output;


        }


        ~N3NLogger()
        {
            if (log_file_ != null)
            {
                log_file_.Close();
                log_file_.Dispose();


            }

            log_file_ = null;
        }


        private bool initLogFile()
        {
            if (log_file_ != null)
            {

                return true;
            }
            try
            {
                DateTime DT = DateTime.Now;

                String sFileName = String.Format("{0}_{1:0000}{2:00}{3:00}.log", prefix_, DT.Year, DT.Month, DT.Day);
                log_file_ = new FileStream(sFileName, FileMode.Append);

                return true;

            }
            catch (FileNotFoundException ex)
            {
                return false;
            }
        }



        private void writeHeader(StringBuilder sb)
        {

            Assembly assem = Assembly.GetExecutingAssembly();
            AssemblyName assemName = assem.GetName();

            System.Diagnostics.StackTrace trace = new System.Diagnostics.StackTrace(true);
            System.Diagnostics.StackFrame frame = trace.GetFrame(3); // logger.xxx(msg) 호출자 스택


            sb.Append(String.Format("[{0}][{1}]", assemName.Name, Thread.CurrentThread.ManagedThreadId));

            DateTime DT = DateTime.Now;
            /*
                      sb.Append(
                          String.Format("[{0:0000}/{1:00}/{2:00} {3:00}:{4:00}:{5:00}]",
                          DT.Year, DT.Month, DT.Day,
                          DT.Hour, DT.Minute, DT.Second));

                      */
            sb.Append(String.Format("[{0}:{1}][{2}] ", PathUtil.getFileName(frame.GetFileName()), frame.GetFileLineNumber(), frame.GetMethod().Name));


        }
    }
}
