﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Map
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class Demo2 : Window
    {
        public Demo2()
        {
            InitializeComponent();
            this.xShowMapSettingButton.Click += xShowMapSettingButton_Click;
        }
        private void xShowMapSettingButton_Click(object sender, RoutedEventArgs e)
        {
            //var dlg = new Innotive.InnoWatch.Console.UIs.LocationSettingPopupWindows.LocationSettingPopupWindow();
            //dlg.Show();
        }

        private void xShowMapSettingButton_Click_1(object sender, RoutedEventArgs e)
        {
            var dlg = new Innotive.InnoWatch.FileImport.View.ImportFrmView();
            dlg.Show();
        }
    }
}
