﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using System.Collections.ObjectModel;
using System.ComponentModel;
using Innotive.InnoWatch.FileImport;
using RPN.Common.Core;
using RPN.Common.Core.Command;
using RPN.Common.Core.ViewModel;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Controls;
using GMap.NET;
using System.IO;
using System.Windows;
using System.Xml;
using System.Xml.Linq;
using Map.Util;

using Innotive.InnoWatch.CommonUtils.Log;
using Innotive.InnoWatch.Commons.Configs;
using Innotive.InnoWatch.Commons.Map;
using Innotive.InnoWatch.Commons.Utils;
using Innotive.InnoWatch.Commons.Utils.Http;
using Innotive.InnoWatch.FileImport.Model;

namespace Innotive.InnoWatch.FileImport.ViewModel
{
    public class KMLImportViewModel : ViewModelBase
    {
        #region Member variable
        //private PointAndInfoList _pointNinfoList = new PointAndInfoList();
        private PointAndInfo _pointNinfo = new PointAndInfo();

        private XamlInfoList _xamlInfoList = new XamlInfoList();

        protected ICommand _KmlImportCommand;
        protected ICommand _KmlSaveCommand;
        protected ICommand _KmlDeleteCommand;

        private ICommand _XamlImportCommand;
        private ICommand _XamlSaveCommand;
        private ICommand _XamlDeleteCommand;

        #endregion

        #region Property
        private bool _isSelected;
        public bool IsSelected
        {
            get { return _isSelected; }
            set
            {
                if (_isSelected != value)
                {
                    _isSelected = value;
                    RaisePropertyChanged(() => IsSelected);
                }
            }
        }

        //public PointAndInfoList PointNinfoList
        //{
        //    get { return _pointNinfoList; }
        //    set { _pointNinfoList = value; }
        //}

        public XamlInfoList XamlInfoList
        {
            get { return _xamlInfoList; }
            set { _xamlInfoList = value; }
        }

        public PointAndInfo PointNinfo
        {
            get { return _pointNinfo; }
            set { _pointNinfo = value; }
        }

        #endregion

        #region Constructor
        public KMLImportViewModel()
            : base(string.Empty, false, false)
        {
            this.PropertyChanged += KMLImportViewModel_PropertyChanged;
        }

        public KMLImportViewModel(string sViewName, bool bRes, bool bDu)
            : base(sViewName, false, bDu)
        {
            this.PropertyChanged += KMLImportViewModel_PropertyChanged;
        }
        #endregion

        #region Command
        public ICommand KmlImportCommand
        {
            get
            {
                if (_KmlImportCommand == null)
                {
                    _KmlImportCommand = new DelegateCommand<object>((P) => KmlExecuteImport(P), (P) => KmlCanExecuteImport(P));
                }

                return _KmlImportCommand;
            }
        }

        public ICommand KmlDeleteCommand
        {
            get
            {
                if (_KmlDeleteCommand == null)
                {
                    _KmlDeleteCommand = new DelegateCommand<object>((P) => KmlExecuteDelete(P), (P) => KmlCanExecuteDelete(P));
                }

                return _KmlDeleteCommand;
            }
        }

        public ICommand KmlSaveCommand
        {
            get
            {
                if (_KmlSaveCommand == null)
                {
                    _KmlSaveCommand = new DelegateCommand<object>((P) => KmlExecuteSave(P), (P) => KmlCanExecuteSave(P));
                }

                return _KmlSaveCommand;
            }
        }

        public ICommand XamlImportCommand
        {
            get
            {
                if (_XamlImportCommand == null)
                {
                    _XamlImportCommand = new DelegateCommand<object>((P) => XamlExecuteImport(P), (P) => XamlCanExecuteImport(P));
                }

                return _XamlImportCommand;
            }
        }

        public ICommand XamlDeleteCommand
        {
            get
            {
                if (_XamlDeleteCommand == null)
                {
                    _XamlDeleteCommand = new DelegateCommand<object>((P) => XamlExecuteDelete(P), (P) => XamlCanExecuteDelete(P));
                }

                return _XamlDeleteCommand;
            }
        }

        public ICommand XamlSaveCommand
        {
            get
            {
                if (_XamlSaveCommand == null)
                {
                    _XamlSaveCommand = new DelegateCommand<object>((P) => XamlExecuteSave(P), (P) => XamlCanExecuteSave(P));
                }

                return _XamlSaveCommand;
            }
        }
        #endregion

        #region Command Execute
        private void KmlExecuteImport(object p)
        {
            Stream stream = null;
            Microsoft.Win32.OpenFileDialog openFileDialog = new Microsoft.Win32.OpenFileDialog();
            openFileDialog.Multiselect = false;

            openFileDialog.Filter = "kml files(*.kml)|*.kml|kmz files(*.kmz)|*.kmz";

            //PointAndInfo info = new PointAndInfo();

            if ((bool)openFileDialog.ShowDialog())
            {
                try
                {
                    if ((stream = openFileDialog.OpenFile()) != null)
                    {
                        _pointNinfo.Items.Clear();

                        XmlDocument doc = new XmlDocument();

                        if (Path.GetExtension(openFileDialog.FileName).ToUpper().Equals(".KMZ"))
                        {
                            KmzFile kmzFile = KmzFile.Open(stream);

                            string xml = kmzFile.ReadKml();

                            doc.LoadXml(xml);
                        }
                        else
                        {
                            doc.Load(stream);
                        }

                        XmlNamespaceManager nsMgr = new XmlNamespaceManager(doc.NameTable);
                        nsMgr.AddNamespace("kml", "http://www.opengis.net/kml/2.2");

                        XmlNodeList nodeCoordinates = doc.SelectNodes("//kml:Placemark/kml:Point/kml:coordinates", nsMgr);
                        XmlNodeList allfolderList = doc.SelectNodes("//kml:Folder", nsMgr);
                        List<XmlNode>  folderList = new List<XmlNode>();

                        for (int i = 0; i < allfolderList.Count; i++)
                        {
                            if (allfolderList[i].SelectNodes("descendant::kml:Placemark/kml:Point/kml:coordinates", nsMgr).Count > 0)
                            {
                                folderList.Add(allfolderList[i]);
                            }
                        }

                        for (int i = 0; i < folderList.Count; i++)
                        {
                            SetPointNinfoList(_pointNinfo, folderList[i], nsMgr);
                        }
                    }
                }
                catch (Exception ex)
                {
                    string sMessage = ex.Message;
                    if (ex.InnerException != null)
                    {
                        sMessage += Environment.NewLine;
                        sMessage += ex.InnerException.Message;
                    }
                    MessageBox.Show(sMessage);
                }
            }
        }

        private void KmlExecuteDelete(object P)
        {
            if( MessageBox.Show("Delete Ok?", string.Empty, MessageBoxButton.YesNo).Equals(MessageBoxResult.Yes))
            {
                PointAndInfo item = P as PointAndInfo;

                InnowatchService.DataService svc = new InnowatchService.DataService();

                svc.SetKmlPlaceMakerDelete(item.Id, string.Empty, string.Empty, string.Empty);

                item.Parent.Items.Remove(item);

                MessageBox.Show("Delete Complate");
            }
        }

        private void KmlExecuteSave(object P)
        {
            try
            {
                var xmlDocument = new XmlDocument();

                var xmlDeclaration = xmlDocument.CreateXmlDeclaration("1.0", "utf-8", null);
                xmlDocument.AppendChild(xmlDeclaration);

                var root = xmlDocument.CreateElement("Document");
                xmlDocument.AppendChild(root);

                KMLImportViewModel.PointInfoToXml(_pointNinfo, xmlDocument, root);

                var url = UrlFactory.GetUrlC(
                    Map.Util.Envirenment.DataServiceUrl,
                    "SetDataKmlPlaceMaker?",
                    Map.Util.Envirenment.ProjectID,
                    "USER",
                    "command");

                //var data = RestService.PostMessage(url, xmlDocument.InnerXml);


                InnowatchService.DataService svc = new InnowatchService.DataService();

                svc.SetKmlPlaceMaker(xmlDocument, string.Empty, string.Empty, string.Empty);
                //var result = new LocationBookmark();

                //if (!string.IsNullOrWhiteSpace(data))
                //{
                //    var read = new StringReader(data);
                //    var serializer = new XmlSerializer(typeof(LocationBookmark));

                //    XmlReader reader = new XmlTextReader(read);
                //    result = (LocationBookmark) serializer.Deserialize(reader);
                //}

                MessageBox.Show("Save Complate");
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        private void XamlExecuteImport(object P)
        {
            Stream[] streams = null;

            StreamReader reader = null;
            Microsoft.Win32.OpenFileDialog openFileDialog = new Microsoft.Win32.OpenFileDialog();
            openFileDialog.Multiselect = true;

            openFileDialog.Filter = "xaml files(*.xaml)|*.xaml";

            //PointAndInfo info = new PointAndInfo();

            if ((bool)openFileDialog.ShowDialog())
            {
                try
                {
                    streams = openFileDialog.OpenFiles();

                    for (int i = 0; i < streams.Length; i++)
                    {
                        //_xamlInfoList.Clear();

                        XamlInfo xmlInfo = new XamlInfo();

                        xmlInfo.Name = RPN.Common.Util.PathUtil.getFileName(openFileDialog.FileNames[i]);
                        xmlInfo.Id = Guid.NewGuid().ToString();

                        reader = new StreamReader(streams[i]);

                        xmlInfo.Xaml = reader.ReadToEnd();

                        _xamlInfoList.Add(xmlInfo);

                        reader.Close();
                        streams[i].Close();
                    }
                }
                catch (Exception ex)
                {
                    string sMessage = ex.Message;
                    if (ex.InnerException != null)
                    {
                        sMessage += Environment.NewLine;
                        sMessage += ex.InnerException.Message;
                    }

                    MessageBox.Show(sMessage);
                }
                finally
                {
                    reader.Close();
                }
            }
        }

        private void XamlExecuteDelete(object P)
        {
            //foreach (XamlInfo info in _xamlInfoList)
            //{
            //    if (info.Id.Equals(P))
            //    {
            //        _xamlInfoList.Remove(info);

            //        break;
            //    }
            //}
            if (MessageBox.Show("Delete Ok?", string.Empty, MessageBoxButton.YesNo).Equals(MessageBoxResult.Yes))
            {
                XamlInfo item = P as XamlInfo;

                InnowatchService.DataService svc = new InnowatchService.DataService();

                svc.SetMapXamlDelete(item.Id, string.Empty, string.Empty, string.Empty);

                _xamlInfoList.Remove(item);

                MessageBox.Show("Delete Complate");
            }
        }

        private void XamlExecuteSave(object P)
        {
            try
            {
                var xmlDocument = new XmlDocument();

                var xmlDeclaration = xmlDocument.CreateXmlDeclaration("1.0", "utf-8", null);
                xmlDocument.AppendChild(xmlDeclaration);

                var root = xmlDocument.CreateElement("Document");
                xmlDocument.AppendChild(root);

                KMLImportViewModel.MapXamlToXml(_xamlInfoList, xmlDocument, root);

                var url = UrlFactory.GetUrlC(
                    Map.Util.Envirenment.DataServiceUrl,
                    "SetMapXaml?",
                    Map.Util.Envirenment.ProjectID,
                    "USER",
                    "command");

                //var data = RestService.PostMessage(url, xmlDocument.InnerXml);

                InnowatchService.DataService svc = new InnowatchService.DataService();

                svc.SetMapXaml(xmlDocument, string.Empty, string.Empty, string.Empty);
                //var result = new LocationBookmark();

                //if (!string.IsNullOrWhiteSpace(data))
                //{
                //    var read = new StringReader(data);
                //    var serializer = new XmlSerializer(typeof(LocationBookmark));

                //    XmlReader reader = new XmlTextReader(read);
                //    result = (LocationBookmark) serializer.Deserialize(reader);
                //}

                MessageBox.Show("Save Complate");
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        #endregion

        #region Command Can Execute

        private bool KmlCanExecuteImport(object P)
        {
            bool bReturn = true;

            return bReturn;
        }

        private bool KmlCanExecuteDelete(object parameter)
        {
            return true;
        }

        private bool KmlCanExecuteSave(object parameter)
        {
            return true;
        }

        private bool XamlCanExecuteDelete(object P)
        {
            return true;
        }

        private bool XamlCanExecuteImport(object P)
        {
            return true;
        }

        private bool XamlCanExecuteSave(object P)
        {
            return true;
        }
        #endregion

        #region Event Handler
        void KMLImportViewModel_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                case "test":
                    break;
            }
        }
        #endregion

        #region General Function
        private static void PointInfoToXml(PointAndInfo info, XmlDocument xmlDocument, XmlNode root)
        {
            
            foreach (var item in info.Items)
            {
                var ePlaceMaker = xmlDocument.CreateElement("Data");

                root.AppendChild(ePlaceMaker);

                var attKpmId = xmlDocument.CreateAttribute("KpmId");
                var attKpmNm = xmlDocument.CreateAttribute("KpmNm");
                var attKpmParentId = xmlDocument.CreateAttribute("KpmParentId");
                var attKpmLat = xmlDocument.CreateAttribute("KpmLat");
                var attKpmLon = xmlDocument.CreateAttribute("KpmLon");
                var attKpmAlt = xmlDocument.CreateAttribute("KpmAlt");
                var attKpmDesc = xmlDocument.CreateAttribute("KpmDesc");
                var attKpmIsFolder = xmlDocument.CreateAttribute("KpmIsFolder");
                var attKpmIStyleUrl = xmlDocument.CreateAttribute("KpmStyleUrl");

                attKpmId.Value = item.Id;
                attKpmNm.Value = item.Name;
                attKpmParentId.Value = item.Parent.Id == null?string.Empty:item.Parent.Id ;
                attKpmLat.Value = item.Lat.ToString();
                attKpmLon.Value = item.Lon.ToString();
                attKpmAlt.Value = item.Alt.ToString();
                attKpmDesc.Value = item.Desc;
                attKpmIsFolder.Value = item.IsFolder ? "1" : "0";
                attKpmIStyleUrl.Value = item.StyleUrl;

                ePlaceMaker.Attributes.Append(attKpmId);
                ePlaceMaker.Attributes.Append(attKpmNm);
                ePlaceMaker.Attributes.Append(attKpmParentId);
                ePlaceMaker.Attributes.Append(attKpmLat);
                ePlaceMaker.Attributes.Append(attKpmLon);
                ePlaceMaker.Attributes.Append(attKpmAlt);
                ePlaceMaker.Attributes.Append(attKpmDesc);
                ePlaceMaker.Attributes.Append(attKpmIsFolder);
                ePlaceMaker.Attributes.Append(attKpmIStyleUrl);

                PointInfoToXml(item, xmlDocument, root);
            }
        }

        private static void MapXamlToXml(XamlInfoList list, XmlDocument xmlDocument, XmlNode root)
        {
            foreach (var item in list)
            {
                var ePlaceMaker = xmlDocument.CreateElement("Data");

                root.AppendChild(ePlaceMaker);

                var attMxlId = xmlDocument.CreateAttribute("MxlId");
                var attMxlNm = xmlDocument.CreateAttribute("MxlNm");
                var attMxlData = xmlDocument.CreateAttribute("MxlData");

                attMxlId.Value = item.Id;
                attMxlNm.Value = item.Name;
                attMxlData.Value = item.Xaml;

                ePlaceMaker.Attributes.Append(attMxlId);
                ePlaceMaker.Attributes.Append(attMxlNm);
                ePlaceMaker.Attributes.Append(attMxlData);
            }
        }

        private void SetPointNinfoList(PointAndInfo parentInfo, XmlNode parentNode, XmlNamespaceManager nsMgr)
        {
            PointAndInfo info;

            switch (parentNode.Name)
            {
                case "Folder":

                    if (parentNode.SelectNodes("descendant::kml:Placemark/kml:Point/kml:coordinates", nsMgr).Count < 1)
                    {
                        return;
                    }

                    info = new PointAndInfo();
                    info.Name = parentNode["name"].InnerText;
                    info.IsFolder = true;
                    info.Parent = parentInfo;
                    info.Id = Guid.NewGuid().ToString();
                    info.Desc = parentNode["description"].InnerText;
                    info.StyleUrl = string.Empty;

                    parentInfo.Items.Add(info);

                    foreach (XmlNode cNode in parentNode.ChildNodes)
                    {
                        SetPointNinfoList(info, cNode, nsMgr);
                    }

                    break;

                case "Placemark":
                    info = new PointAndInfo();

                    XmlNode coordinates = parentNode.SelectSingleNode("descendant::kml:Point/kml:coordinates", nsMgr);

                    if (coordinates == null)
                    {
                        return;
                    }

                    string sCoordinates = coordinates.InnerText;
                    string[] tokens = sCoordinates.Split(",".ToCharArray());
                    float fLat = float.Parse(tokens[1]);
                    float fLon = float.Parse(tokens[0]);
                    int iAlt = int.Parse(tokens[2]);

                    info.Name = coordinates.ParentNode.ParentNode["name"].InnerText;
                    info.IsFolder = false;
                    info.Parent = parentInfo;
                    info.Id = Guid.NewGuid().ToString();
                    info.Lat = fLat;
                    info.Lon = fLon;
                    info.Alt = iAlt;
                    info.Desc = coordinates.ParentNode.ParentNode["description"].InnerText;

                    info.StyleUrl = string.Empty;

                    if (parentNode["styleUrl"] != null)
                        info.StyleUrl = parentNode["styleUrl"].InnerText;
                   


                    parentInfo.Items.Add(info);
                    break;
            }
        }

        #endregion
    }
}
