﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Map.Model
{
    public class KmlInfo
    {
        public string Name { get; set; }
        public int Level { get; set; }
        public string ParentID { get; set; }
        public List<PointAndInfo> PointAndInfoList { get; set; }
    }
}
