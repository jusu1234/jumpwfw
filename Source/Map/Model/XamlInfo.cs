﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using System.ComponentModel;
using Innotive.InnoWatch.FileImport.ViewModel;
using RPN.Common.Core.ViewModel;
using GMap.NET;
using Innotive.InnoWatch.Commons.Models;
using System.Windows.Media;
using System.Xml;

namespace Innotive.InnoWatch.FileImport.Model
{
    public class XamlInfo : BaseModel//, IEquatable<PointAndInfo>//, IHasDepth
    {
        #region Member Variable

        //private string _name;
        private string _xaml;

        #endregion

        #region Constructors and Destructors

        public XamlInfo() { }

        public XamlInfo(string name, string xaml)
        {
            Name = name;
            _xaml = xaml;
        }

        #endregion

        #region Properties

        //public String Name
        //{
        //    get
        //    {
        //        return this._name;
        //    }

        //    set
        //    {
        //        this._name = value;
        //    }
        //}

        public string Xaml
        {
            get { return _xaml; }
            set { _xaml = value; }
        }


        #endregion


    }

}
