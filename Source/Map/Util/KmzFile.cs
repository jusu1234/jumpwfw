﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Ionic.Zip;

namespace Map.Util
{
    /// <summary>
    /// Represents a Kmz archive, containing Kml data and associated files.
    /// </summary>
    /// <remarks>
    /// The entire Kmz archive (in its compressed state) will be held in memory
    /// until a call to <see cref="KmzFile.Dispose"/> is made.
    /// </remarks>
    public sealed class KmzFile : IDisposable
    {
        // This is the default name for writing a KML file to a new archive,
        // however, the default file for reading from an archive is the first
        // file in the table of contents that ends with ".kml".
        private const string DefaultKmlFilename = "doc.kml";
        private static Encoding defaultEncoding = Encoding.UTF8;
        private ZipFile _zip;

        // The whole ZipFile will be saved to our stream so that we can check
        // the zip when it's loaded so we're no creating random exceptions when
        // accessing the ZipFile.
        // Also, ZipExntry.Extract will throw an exception if we try to call it
        // and the ZipFile has been modified (e.g. ZipFile.UpdateEntry)
        private MemoryStream _zipStream;

        private KmzFile(MemoryStream stream)
        {
            _zipStream = stream;
        }

        /// <summary>
        /// Gets or sets the default string encoding to use when extracting
        /// the Kml from a Kmz archive. Defaults to UTF8.
        /// </summary>
        public static Encoding DefaultEncoding
        {
            get { return defaultEncoding; }
            set { defaultEncoding = value; }
        }

        /// <summary>
        /// Gets the filenames for the entries contained in the archive.
        /// </summary>
        /// <exception cref="ObjectDisposedException">
        /// <see cref="Dispose"/> has been called on this instance or the
        /// stream was closed.
        /// </exception>
        public IEnumerable<string> Files
        {
            get
            {
                this.ThrowIfDisposed();
                return _zip.EntryFileNames;
            }
        }


        /// <summary>Opens a KmzFile from the specified stream.</summary>
        /// <param name="stream">The stream to read the data from.</param>
        /// <returns>A KmzFile representing the specified stream.</returns>
        /// <exception cref="ArgumentNullException">stream is null.</exception>
        /// <exception cref="IOException">
        /// The Kmz archive is not in the expected format.
        /// </exception>
        /// <exception cref="IOException">An I/O error occurred.</exception>
        /// <exception cref="NotSupportedException">
        /// The stream does not support reading.
        /// </exception>
        /// <exception cref="ObjectDisposedException">
        /// The stream was closed.
        /// </exception>
        public static KmzFile Open(Stream stream)
        {
            if (stream == null)
            {
                throw new ArgumentNullException("stream");
            }

            var memory = new MemoryStream();
            try
            {
                stream.CopyTo(memory);
            }
            catch
            {
                memory.Dispose();
                throw;
            }

            // Check if it's a valid Zip file
            memory.Position = 0;
            if (!ZipFile.IsZipFile(memory, true))
            {
                memory.Dispose();
#if SILVERLIGHT
                throw new IOException("The Kmz archive is not in the expected format.");
#else
                throw new InvalidDataException("The Kmz archive is not in the expected format.");
#endif
            }

            // Everything's ok
            var instance = new KmzFile(memory);
            instance.ResetZip(false);
            return instance;
        }


        /// <summary>Releases all resources used by this instance.</summary>
        public void Dispose()
        {
            if (_zip != null)
            {
                _zip.Dispose();
                _zip = null;
            }

            if (_zipStream != null)
            {
                _zipStream.Dispose();
                _zipStream = null;
            }
        }

        /// <summary>Extracts the specified file from the Kmz archive.</summary>
        /// <param name="path">
        /// The file, including directory information, to locate in the archive.
        /// </param>
        /// <returns>
        /// A byte array if the specified value parameter was found in the
        /// archive; otherwise, null.
        /// </returns>
        /// <exception cref="ObjectDisposedException">
        /// <see cref="Dispose"/> has been called on this instance.
        /// </exception>
        public byte[] ReadFile(string path)
        {
            this.ThrowIfDisposed();

            if (!string.IsNullOrEmpty(path))
            {
                // This will return null if the path is not found
                ZipEntry file = _zip[path];
                if (file != null)
                {
                    return this.ExtractResource(file);
                }
            }
            return null;
        }

        /// <summary>Extracts the default Kml file from the archive.</summary>
        /// <returns>
        /// A string containing the Kml content if a suitable file was found in
        /// the Kmz archive; otherwise, null.
        /// </returns>
        /// <remarks>
        /// This returns the first file in the Kmz archive table of contents
        /// which has a ".kml" extension. Note that the file found may not
        /// necessarily be in the root directory.
        /// </remarks>
        /// <exception cref="ObjectDisposedException">
        /// <see cref="Dispose"/> has been called on this instance.
        /// </exception>
        public string ReadKml()
        {
            this.ThrowIfDisposed();

            ZipEntry kml = _zip.FirstOrDefault(
                e => string.Equals(".kml", Path.GetExtension(e.FileName), StringComparison.OrdinalIgnoreCase));

            if (kml != null)
            {
                byte[] resource = this.ExtractResource(kml);
                return defaultEncoding.GetString(resource, 0, resource.Length);
            }
            return null;
        }


        /// <summary>Saves this instance to the specified stream.</summary>
        /// <param name="stream">The stream to write to.</param>
        /// <exception cref="ArgumentException">stream is not writable.</exception>
        /// <exception cref="ArgumentNullException">stream is null.</exception>
        /// <exception cref="IOException">An I/O error occurred.</exception>
        /// <exception cref="NotSupportedException">
        /// The stream does not support writing.
        /// </exception>
        /// <exception cref="ObjectDisposedException">
        /// <see cref="Dispose"/> has been called on this instance or the
        /// stream was closed.
        /// </exception>
        public void Save(Stream stream)
        {
            this.ThrowIfDisposed();
            if (stream == null)
            {
                throw new ArgumentNullException("stream");
            }

            // ZipFile will hold on to the passed in stream, so we need to save
            // to our private stream and then copy that to the stream.
            this.ResetZip(true);
            _zipStream.Position = 0;
            _zipStream.CopyTo(stream);
        }

        /// <summary>Saves this instance to the specified path.</summary>
        /// <param name="path">The complete file path to write to.</param>
        /// <remarks>
        /// If the specified file exists in the specified path then it will be
        /// overwritten; otherwise, a new file will be created.
        /// </remarks>
        /// <exception cref="ArgumentException">
        /// path is a zero-length string, contains only white space, or contains
        /// one or more invalid characters as defined by
        /// <see cref="Path.GetInvalidPathChars"/>.
        /// </exception>
        /// <exception cref="ArgumentNullException">path is null.</exception>
        /// <exception cref="DirectoryNotFoundException">
        /// The specified path is invalid.
        /// </exception>
        /// <exception cref="IOException">An I/O error occurred.</exception>
        /// <exception cref="NotSupportedException">
        /// path is in an invalid format.
        /// </exception>
        /// <exception cref="ObjectDisposedException">
        /// <see cref="Dispose"/> has been called on this instance.
        /// </exception>
        /// <exception cref="PathTooLongException">
        /// The specified path, file name, or both exceed the system-defined
        /// maximum length.
        /// </exception>
        /// <exception cref="UnauthorizedAccessException">
        /// The caller does not have the required permission or path specified a
        /// file that is read-only.
        /// </exception>
        public void Save(string path)
        {
            this.ThrowIfDisposed();
            using (var file = File.Create(path))
            {
                this.Save(file);
            }
        }

        private byte[] ExtractResource(ZipEntry entry)
        {
            // ZipEntry.Extract will throw an exception if the ZipFile has been
            // modified. Check the VersionNeeded - if it's zero then the entry
            // needs to be saved before we can extract it.
            if (entry.VersionNeeded == 0)
            {
                this.ResetZip(true);
                entry = _zip[entry.FileName];
            }

            using (var stream = new MemoryStream())
            {
                entry.Extract(stream);
                return stream.ToArray();
            }
        }

        private void ResetZip(bool save)
        {
            if (save)
            {
                var copy = new MemoryStream();
                _zip.Save(copy);
                _zip.Dispose();

                _zipStream.Dispose();
                _zipStream = copy;
            }

            _zipStream.Position = 0;
            _zip = ZipFile.Read(_zipStream);
        }

        private void ThrowIfDisposed()
        {
            if (_zipStream == null) // _zipStream is set to null only in Dispose
            {
                throw new ObjectDisposedException(this.GetType().FullName);
            }
        }
    }
}