﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Map.Util
{
    internal class Envirenment
    {
        private static string _dataServiceUrl = "http://localhost:25000/rest/data/";
        private static string _projectID = "Map";

        public static string DataServiceUrl
        { 
            get{ return _dataServiceUrl; }
        }

        public static string ProjectID
        {
            get { return _projectID; }
        }
        
    }
}
