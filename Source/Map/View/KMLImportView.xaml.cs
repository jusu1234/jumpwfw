﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Innotive.InnoWatch.FileImport.ViewModel;

namespace Innotive.InnoWatch.FileImport.View
{
    public partial class KMLImportView : UserControl
    {
        private KMLImportViewModel vm = new KMLImportViewModel("KMLImportView", false, false);

        public event EventHandler CloseHandler;

        public KMLImportView()
        {
            this.InitializeComponent();

            this.DataContext = vm;
        }

        private void xBtnKmlClose_Click(object sender, RoutedEventArgs e)
        {
            this.CloseHandler(sender, e);
        }

        private void xBtnXamlClose_Click(object sender, RoutedEventArgs e)
        {
            this.CloseHandler(sender, e);
        }
    }
}