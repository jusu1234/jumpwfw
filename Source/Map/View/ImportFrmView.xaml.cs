﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Innotive.InnoWatch.FileImport.View
{
    /// <summary>
    /// ImportView.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class ImportFrmView : Window
    {
        public ImportFrmView()
        {
            InitializeComponent();

            this.xKmlInpourtView.CloseHandler += xKmlInpourtView_CloseHandler;
        }

        void xKmlInpourtView_CloseHandler(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
