USE [InnowatchData];
GO
SET ANSI_NULLS ON;
GO
SET QUOTED_IDENTIFIER ON;
GO
ALTER PROCEDURE [dbo].[USP_SetData_Map_Point]
(
    @KpmId          VARCHAR(50)
  , @KpmNm          VARCHAR(50)
  , @KpmParentId    VARCHAR(50)
  , @KpmLat         DECIMAL
  , @KpmLon         DECIMAL
  , @KpmAlt         INT
  , @KpmDsc         VARCHAR(1000)
  , @userid         VARCHAR(50)
)
AS

 
  UPDATE KmlPlaceMaker
    SET 
        KpmNm = @KpmNm
      , KpmParentId = @KpmParentId
      , KpmLat   = @KpmLat
      , KpmLon   = @KpmLon
      , KpmAlt   = @KpmAlt
      , KpmDsc   = @KpmDsc
      , UpdateUid = @userid
      , UpdateDt = GetDate()
      
 WHERE 
    KpmId = @KpmId
   
 IF @@ROWCOUNT < 1 
 BEGIN
    INSERT INTO KmlPlaceMaker
    (
        KpmId 
      , KpmNm 
      , KpmParentId
      , KpmLat 
      , KpmLon 
      , KpmAlt
      , KpmDsc 
    )VALUES
    (
        KpmId
      , KpmNm
      , KpmParentId
      , KpmLat
      , KpmLon
      , KpmAlt
      , KpmDsc
     )
 END
