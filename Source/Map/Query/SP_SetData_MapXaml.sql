USE [InnowatchData];
GO
SET ANSI_NULLS ON;
GO
SET QUOTED_IDENTIFIER ON;
GO
ALTER PROCEDURE [dbo].[SP_SetData_MapXaml]
(
      @MxlId VARCHAR(20)
    , @MxlNm VARCHAR(50)
    , @MxlData TEXT
    , @userid VARCHAR(50)
)
AS

UPDATE MapXaml
SET
         MxlNm = @MxlNm
       , MxlData = @MxlData
       , UpdateUid = @userid
       , UpdateDt = GETDATE()
WHERE
    MxlId = @MxlId

IF @@ROWCOUNT < 1 
  BEGIN
    INSERT INTO MapXaml
    (
         MxlId
       , MxlNm
       , MxlData
       , UpdateUid
       , UpdateDt
       , InsertUid
       , InsertDt
    )VALUES
    (
         @MxlId
       , @MxlNm
       , @MxlData 
       , @userid
       , GETDATE()
       , @userid
       , GETDATE()
     )
   END
