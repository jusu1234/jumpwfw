USE [InnowatchData];
GO
SET ANSI_NULLS ON;
GO
SET QUOTED_IDENTIFIER ON;
GO
ALTER PROCEDURE [dbo].[SP_SetData_MapXaml_Delete]
(
      @MxlId VARCHAR(20)
)
AS

  DELETE FROM MapXaml
  WHERE
      MxlId = @MxlId

