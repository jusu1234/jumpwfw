USE [InnowatchData];
GO
SET ANSI_NULLS ON;
GO
SET QUOTED_IDENTIFIER ON;
GO
ALTER PROCEDURE [dbo].[USP_SetData_MapXaml]
(
      @mxl_id VARCHAR(20)
    , @mxl_nm VARCHAR(50)
    , @mxl_data TEXT
    , @uid VARCHAR(50)
)
AS

UPDATE MapXaml
SET
         mxl_nm = @mxl_nm
       , mxl_data = @mxl_data
       , UpdateUid = @uid
       , UpdateDt = GETDATE()
WHERE
    mxl_id = @mxl_id

IF @@ROWCOUNT < 1 
  BEGIN
    INSERT INTO MapXaml
    (
         mxl_id
       , mxl_nm
       , mxl_data
       , UpdateUid
       , UpdateDt
       , InsertUid
       , InsertDt
    )VALUES
    (
         @mxl_id
       , @mxl_nm
       , @mxl_data 
       , @uid
       , GETDATE()
       , @uid
       , GETDATE()
     )
   END
