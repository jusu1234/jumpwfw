USE [InnowatchData];
GO
SET ANSI_NULLS ON;
GO
SET QUOTED_IDENTIFIER ON;
GO
ALTER PROCEDURE [dbo].[USP_SetData_Map_Point]
(
    @point_id   VARCHAR(50)
  , @name       VARCHAR(50)
  , @lat        DECIMAL
  , @lon        DECIMAL
  , @alt        INT
  , @desc       VARCHAR(1000)
  , @userid     VARCHAR(50)
)
AS

  UPDATE PlaceMarkForKML
    SET 
        PointNM = @name
      , Lat   = @lat
      , Lon   = @lon
      , Alt   = @alt
      , Dsc   = @desc
      , UpdateUid = @userid
      , UpdateDt = GetDate()
      
 WHERE 
    PointId = @point_id
   
 IF @@ROWCOUNT < 1 
 BEGIN
    INSERT INTO PlaceMarkForKML
  (
      PointId 
    , PointNM 
    , lat 
    , lon 
    , alt
    , dsc 
  )VALUES
  (
      @point_id
    , @name
    , @lat
    , @lon
    , @alt
    , @desc
   )
 END
