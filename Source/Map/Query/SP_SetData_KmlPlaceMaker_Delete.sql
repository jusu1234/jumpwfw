USE [InnowatchData];
GO
SET ANSI_NULLS ON;
GO
SET QUOTED_IDENTIFIER ON;
GO
ALTER PROCEDURE [dbo].[SP_SetData_KmlPlaceMaker_Delete]
(
    @KpmId          VARCHAR(50)
)
AS
  DELETE FROM KmlPlaceMaker
  WHERE 
    KpmId = @KpmId
   
