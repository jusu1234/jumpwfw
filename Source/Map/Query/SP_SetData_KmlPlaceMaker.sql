USE [InnowatchData];
GO
SET ANSI_NULLS ON;
GO
SET QUOTED_IDENTIFIER ON;
GO
ALTER PROCEDURE [dbo].[SP_SetData_KmlPlaceMaker]
(
    @KpmId          VARCHAR(50)
  , @KpmNm          VARCHAR(50)
  , @KpmParentId    VARCHAR(50)
  , @KpmLat         DECIMAL
  , @KpmLon         DECIMAL
  , @KpmAlt         INT
  , @KpmDesc        VARCHAR(1000)
  , @KpmIsFolder    CHAR(1)
  , @userid         VARCHAR(50)
  , @KpmStyleUrl    VARCHAR(255)
)
AS
  UPDATE KmlPlaceMaker
    SET 
        KpmNm = @KpmNm
      , KpmParentId = @KpmParentId
      , KpmLat   = @KpmLat
      , KpmLon   = @KpmLon
      , KpmAlt   = @KpmAlt
      , KpmDesc   = @KpmDesc
      , KpmIsFolder = @KpmIsFolder
      , UpdateUid = @userid
      , UpdateDt = GetDate()
      , KpmStyleUrl = @KpmStyleUrl
      
 WHERE 
    KpmId = @KpmId
   
 IF @@ROWCOUNT < 1 
 BEGIN
    INSERT INTO KmlPlaceMaker
    (
        KpmId 
      , KpmNm 
      , KpmParentId
      , KpmLat 
      , KpmLon 
      , KpmAlt
      , KpmDesc 
      , KpmIsFolder
      , UpdateUid
      , UpdateDt
      , InsertUid
      , InsertDt
      , KpmStyleUrl
    )VALUES
    (
        @KpmId
      , @KpmNm
      , @KpmParentId
      , @KpmLat
      , @KpmLon
      , @KpmAlt
      , @KpmDesc
      , @KpmIsFolder
      , @userid
      , GetDate()
      , @userid
      , GetDate()
      , @KpmStyleUrl
     )
 END
