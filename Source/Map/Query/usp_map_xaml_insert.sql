USE [InnowatchData];
GO
SET ANSI_NULLS ON;
GO
SET QUOTED_IDENTIFIER ON;
GO
ALTER PROCEDURE [dbo].[usp_map_xaml_insert]
(
      @mxl_id VARCHAR(20)
    , @mxl_nm VARCHAR(50)
    , @mxl_data TEXT
    , @uid VARCHAR(50)
)
AS

  INSERT INTO MapXaml
  (
       mxl_id
     , mxl_nm
     , mxl_data
     , UpdateUid
     , UpdateDt
     , InsertUid
     , InsertDt
  )VALUES
  (
       @mxl_id
     , @mxl_nm
     , @mxl_data 
     , @uid
     , GETDATE()
     , @uid
     , GETDATE()
   )
