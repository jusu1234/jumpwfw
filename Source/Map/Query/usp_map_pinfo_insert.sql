USE [InnowatchData];
GO
SET ANSI_NULLS ON;
GO
SET QUOTED_IDENTIFIER ON;
GO
ALTER PROCEDURE [dbo].[usp_map_pinfo_insert]
(
    @point_id   VARCHAR(50)
  , @name       VARCHAR(50)
  , @lat        DECIMAL
  , @lon        DECIMAL
  , @alt        INT
  , @desc       VARCHAR(1000)
)
AS

  INSERT INTO PlaceMarkForKML
  (
      PointId 
    , PointNM 
    , lat 
    , lon 
    , alt
    , dsc 
  )VALUES
  (
      @point_id
    , @name
    , @lat
    , @lon
    , @alt
    , @desc
   )
