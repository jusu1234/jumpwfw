﻿using RPN.MON.Data;
using RPN.MON.Data.Models;

﻿using System;
using System.Text;
using System.Linq;
using System.Threading;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel.Composition;

using RPN.Common.Core.RX;
using RPN.Common.Core.Message;
using RPN.Common.Core.Service;
using RPN.Common.Core.Tasks;
using RPN.Common.Logging;
using RPN.Common.Util;
using System.Xml.Linq;
using System.Text.RegularExpressions;
using System.Collections.ObjectModel;


namespace RPN.MON.CameraControl.Services
{
    [Export(typeof(IService))]
    public class CameraStatusService : StatusService, IService
    {
        Random rnd = new Random();

        public string ServiceName { get; set; }

        Logger log_ = LoggerFactory.GetLogger("CameraStatusService");

        public event EventHandler<ServiceEventArgs> DataReceived;

        public CameraStatusService()
        {
            ServiceName = "CameraStatusService";

        }



        #region ImmediaticTask
        [RegisterTask(ServiceMessage.ControlCameraStatusService, TaskType.ImmediaticTask)]
        private void ControlCameraStatusService(int ctl)
        {

            if (!ControlService(ctl))
            {
                log_.Warn("Wrong Command Ignored");
            }
            else
            {
                log_.Info(string.Format("StatusService Control Command {0}", ctl));

            }

        }

        [RegisterTask(ServiceMessage.DoRefreshCameraStatus, TaskType.ImmediaticTask)]
        private void RefreshCameraStatusData()
        {
            log_.Debug("CameraStatus Refreshing Request Start");

            ConcurrentDictionary<string, CameraMonitor> monitors =
                    new ConcurrentDictionary<string, CameraMonitor>();

            Performance pf = new Performance();

            pf.BeginTimeWatch();

            Pararell.ForEach(CameraDataModelContainer.GetInstance.GetCameraDataList().Values,
                camera =>
                {
                    CameraMonitor monitor;
                    GetCameraStatusData(camera, out monitor);

                    // ServiceWatcher 결과를 콜렉션에 저장..
                    if (monitor != null && !monitors.ContainsKey(monitor.CameraID))
                    {
                        //monitors.TryAdd(monitor.CameraID, monitor);
                        monitors.AddOrUpdate(monitor.CameraID, monitor, (n, oldValue) => monitor);
                    }
                }
            );

            pf.EndTimeWatch();
            TimeSpan ts = pf.GetTimeWatchDuration();
            log_.Debug(string.Format("CameraStatus Refreshing Complete Elapsed {0}ms", ts.TotalMilliseconds));

            // Refreshing 결과 전송
            Mediator.GetInstance.BroadCast(ServiceMessage.GetRefreshCameraStatus, monitors.Values);

        }
        #endregion

        #region BackgroundTask
        /// <summary>
        /// ServiceWatcher로 데이터 요청하는 함수
        /// </summary>
        [RegisterTask(ServiceMessage.GetCameraStatus, TaskType.BackgroundTask)]
        private void GetCameraStatusData()
        {
            // 시작시 멈춤
            startEvent_.WaitOne();

            log_.Debug("CameraStatus Data Fetching Start");

            while (!shutDown_)
            {
                /*
                ConcurrentDictionary<string, CameraMonitor> monitors =
                    new ConcurrentDictionary<string, CameraMonitor>();
                */



                // 병렬 처리

                Performance pf = new Performance();

                pf.BeginTimeWatch();

                /*
                Pararell.ForEach(CameraDataModelContainer.GetInstance.GetCameraDataList().Values,
                    camera =>
                    {
                        CameraMonitor monitor;
                        GetCameraStatusData(camera, out monitor);

                        // ServiceWatcher 결과를 콜렉션에 저장..
                        if (monitor != null && !monitors.ContainsKey(monitor.CameraID))
                        {
                            monitors.AddOrUpdate(monitor.CameraID, monitor, (n, oldValue) => monitor);
                        }
                    }
                );
                */

                try
                {
                    string xml = RestServiceHelper.Request(RestServiceHelper.DATA_SERVICE_URL + "GetCameraStatus", false);

                    if (!string.IsNullOrWhiteSpace(xml))
                    {
                 //       System.Diagnostics.Debug.WriteLine(xml);

                        Regex.Split(xml, "\0");
                        XDocument doc = XDocument.Parse(xml);
                         
                        IEnumerable<CameraMonitor> lst =
                            from el in doc.Descendants("Table")
                            select new CameraMonitor()
                            {
                                CameraID = (string)el.Element("CameraId"),
                                CheckTime = (DateTime)el.Element("CheckTime"),
                                Ping = (int)el.Element("NET_CAM"),
                                RecordStatus = (int)el.Element("REC_STAT"),
                                Md = (string)el.Element("MD"),
                                RecordNo = (int?)el.Element("RecordNo"),
                                CameraIP = (string)el.Element("CameraIP"),
                                Rc = (string)el.Element("RC")
                            };
                     
                        /*
                        foreach (CameraMonitor cm in lst)
                        {
                           // _cameraDataList.Add(cm);

                        }
                         */
                        
                        EventHandler<ServiceEventArgs> handler = DataReceived;

                        if (handler != null)
                        {
                            handler(this,
                                    new ServiceEventArgs(new ServiceItem("update", lst)));
                        }
                    }
                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.WriteLine("===========" + e.Message);
                }

                pf.EndTimeWatch();
                TimeSpan ts = pf.GetTimeWatchDuration();
                log_.Debug(string.Format("CameraStatus Data Fetching Completed Elapsed {0}ms", ts.TotalMilliseconds));

                pauseEvent_.WaitOne();

                Thread.Sleep(1000);
            }

            log_.Debug("CameraStatus Data Fetching Stop");
        }
        #endregion

        #region private Func
        private void GetCameraStatusData(CameraDataModel camera, out CameraMonitor monitor)
        {
            DateTime dt = DateTime.Now;

            monitor = new CameraMonitor();

            //  CameraInfo cameraInfo;

            ///////////////////////////////////////
            // ServiceWatcher 호출
            //////////////////////////////////

            monitor.CameraID = camera.CameraID;
            monitor.RecordNo = camera.RecordCameraNumber;
            monitor.Md = camera.MediaServerID;
            monitor.Ping = rnd.Next(0, 100);
            monitor.RecordStatus = monitor.Ping == 0 ? 0 : 1;
            monitor.CheckTime = dt;

            //    cameraInfo = new CameraInfo(monitor.CameraID + "//" + monitor.Md + "//" + rnd.Next(0, 10000));

            //   monitor.RecordStatus = (string)cameraInfo.RecordStatus;

            //
            // XML Parsing
        }

        #endregion
    }
}
