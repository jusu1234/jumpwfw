﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.Composition;
using System.Collections.ObjectModel;
using System.Windows.Input;
using System.Windows.Controls;

using RPN.Common.Core.ViewModel;
using RPN.Common.Core.Command;
using RPN.Common.Core.Message;
using RPN.Common.Core.Service;
using RPN.Common.Core.Tasks;
using RPN.Common.Core.Collection;

using RPN.MON.Data;
using RPN.MON.Data.Models;
using InnowatchMonitorView.Services;
using RPN.Common.Util;
using System.Text.RegularExpressions;
using System.Xml.Linq;

namespace RPN.MON.CameraControl.ViewModels
{
    [Export(typeof(IViewModel))]
    [View(typeof(RPN.MON.CameraControl.Views.CameraView))]
    class CameraViewModel : ViewModelBase, IViewModel
    {
        #region IView member

        static string viewName_;
        public string ViewName
        {
            get
            {
                return viewName_;
            }

            set
            {
                viewName_ = value;
                RaisePropertyChanged(() => ViewName);
            }
        }

        #endregion

        #region field

        CameraMonitorCollection _cameraDataList;
        CameraMonitor selectedCameraData_;

        ICommand refreshCommand_;

        #endregion

        #region CLR member

        public CameraMonitorCollection CameraDataList
        {
            get
            {
                return _cameraDataList;
            }

            set
            {
                _cameraDataList = value;
                RaisePropertyChanged(() => CameraDataList);
            }
        }

        public CameraMonitor SelectedCameraData
        {
            get
            {
                return selectedCameraData_;
            }

            set
            {
                selectedCameraData_ = value;
                RaisePropertyChanged(() => SelectedCameraData);
            }
        }

        #endregion

        #region Command

        public ICommand RefreshCommand
        {
            get
            {
                if (refreshCommand_ == null)
                {
                    refreshCommand_ = new DelegateCommand(DoRefresh);
                }

                return refreshCommand_;
            }
        }

        public void DoRefresh()
        {
            Mediator.GetInstance.BroadCast(ServiceMessage.GetCameraStatus);
        }

        #endregion

        private readonly DataGrid dg_;

        public CameraViewModel()
            : base("CameraView", true, false)
        {
            ViewName = "Camera";

            dg_ = this.GetElement<DataGrid>("CameraDataGrid_Simple");
            dg_.SelectionMode = DataGridSelectionMode.Single;

            _cameraDataList = new CameraMonitorCollection();

            Mediator.GetInstance.BroadCast(ServiceMessage.GetCameraStatus);
            Mediator.GetInstance.BroadCast(ServiceMessage.ControlCameraStatusService, StatusService.START);
        }

        [RegisterTask(ServiceMessage.ReceviedCameraStatus, TaskType.PeriodicTask)]
        public void ReceviedData(IEnumerable<CameraMonitor> monitors)
        {
            if (monitors == null)
                return;

            Action e = () =>
            {
                CameraMonitor monitor = this.SelectedCameraData;

                _cameraDataList.AddOrUpdate(monitors, true);

                //     CollectionSorter.Sort<CameraMonitor>(cameraDataList_,
                //         (x, y) => x.CameraID.CompareTo(y.CameraID));

                this.SelectedCameraData = monitor;

                if (SelectedCameraData != null)
                {
                    dg_.SelectedItem = SelectedCameraData;
                    DataGridRow row = (DataGridRow)dg_.ItemContainerGenerator.ContainerFromIndex(dg_.SelectedIndex);
                    //   dg_.MoveFocus(new TraversalRequest(FocusNavigationDirection.Next));
                    //row.IsSelected = true;
                }

            };

            ProcessOnDispatcherThread(e);
        }

        [RegisterTask(ServiceMessage.GetRefreshCameraStatus, TaskType.ImmediaticTask)]
        private void GetRefreshData(IEnumerable<CameraMonitor> monitors)
        {
            if (monitors == null)
                return;

            Action e = () =>
            {
                CameraMonitor monitor = this.SelectedCameraData;

                _cameraDataList.AddOrUpdate(monitors, true);

                //     CollectionSorter.Sort<CameraMonitor>(cameraDataList_,
                //         (x, y) => x.CameraID.CompareTo(y.CameraID));

                this.SelectedCameraData = monitor;

                if (SelectedCameraData != null)
                {
                    dg_.SelectedItem = SelectedCameraData;
                    DataGridRow row = (DataGridRow)dg_.ItemContainerGenerator.ContainerFromIndex(dg_.SelectedIndex);
                    //   dg_.MoveFocus(new TraversalRequest(FocusNavigationDirection.Next));
                    row.IsSelected = true;
                }
            };

            ProcessOnDispatcherThread(e);
        }
    }

    public class CameraMonitorCollection : SuspendableObserableCollection<CameraMonitor>
    {

    }
}
