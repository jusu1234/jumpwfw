﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;


using InnowatchMonitorView.ViewModels;

namespace InnowatchMonitorView.Views
{
    /// <summary>
    /// CUM_Systme.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class CUM_SystemView : UserControl
    {
        public CUM_SystemView()
        {
        //    InitializeComponent();
        }


        private void ChkAll_Checked(object sender, RoutedEventArgs e)
        {
            CUM_SystemViewModel vm = (CUM_SystemViewModel)DataContext;

            vm.DoCheckAll(true);
        }

        private void ChkAll_Unchecked(object sender, RoutedEventArgs e)
        {
            CUM_SystemViewModel vm = (CUM_SystemViewModel)DataContext;

            vm.DoCheckAll(false);
        }


        private void DataGrid_Sorting(object sender, DataGridSortingEventArgs e)
        {
            DataGridColumn column = e.Column;

            bool bEnableSorted = column != null && column.CanUserSort && column.DisplayIndex != 7; // Action 제외

            if (bEnableSorted)
            {

                IComparer comparer = null;

                ListSortDirection direction = (column.SortDirection != ListSortDirection.Ascending) ? ListSortDirection.Ascending : ListSortDirection.Descending;

                column.SortDirection = direction;

                ListCollectionView lcv = (ListCollectionView)CollectionViewSource.GetDefaultView(SystemDataGrid_Simple.ItemsSource);


                if (column.DisplayIndex == 4) // MemoryInfo
                {
                    comparer = new MemoryInfoSorter(direction == ListSortDirection.Descending);

                    lcv.CustomSort = comparer;
                }
                else if (column.DisplayIndex == 5) // HddInfo
                {
                    comparer = new HddInfoSorter(direction == ListSortDirection.Descending);

                    lcv.CustomSort = comparer;
                }
                else
                {

                }

                e.Handled = true;


            }
            else
            {
                e.Handled = false;
            }
        }
    }
}
