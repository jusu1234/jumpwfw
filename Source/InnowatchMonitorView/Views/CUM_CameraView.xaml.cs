﻿using System.Collections;
using System.ComponentModel;
using System.Windows.Controls;
using System.Windows.Data;

namespace InnowatchMonitorView.Views
{
    /// <summary>
    /// CameraMonitorView.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class CUM_CameraView : UserControl
    {
        public CUM_CameraView()
        {
            //   InitializeComponent();
        }

        private void DataGrid_Sorting(object sender, DataGridSortingEventArgs e)
        {
            DataGridColumn column = e.Column;

            bool bEnableSorted = column != null && column.CanUserSort && column.DisplayIndex != 7; // Action 제외

            if (bEnableSorted)
            {
                IComparer comparer = null;

                ListSortDirection direction = (column.SortDirection != ListSortDirection.Ascending) ? ListSortDirection.Ascending : ListSortDirection.Descending;

                column.SortDirection = direction;

                ListCollectionView lcv = (ListCollectionView)CollectionViewSource.GetDefaultView(CameraDataGrid_Simple.ItemsSource);

                /*
                if (column.DisplayIndex == 4) // MemoryInfo
                {
                    comparer = new MemoryInfoSorter(direction == ListSortDirection.Descending);

                    lcv.CustomSort = comparer;
                }
                else if (column.DisplayIndex == 5) // HddInfo
                {
                    comparer = new HddInfoSorter(direction == ListSortDirection.Descending);

                    lcv.CustomSort = comparer;
                }
                else
                {

                }
                */
                e.Handled = true;


            }
            else
            {
                e.Handled = false;
            }
        }
    }
}
