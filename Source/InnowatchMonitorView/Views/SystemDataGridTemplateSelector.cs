﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;

using InnowatchMonitorView.ViewModels;
using InnowatchMonitorData.Models;

namespace InnowatchMonitorView.Views
{
    public class SystemDataGridTemplateSelector : DataTemplateSelector
    {

        public DataTemplate DefaultServerTemplate { get; set; }
        public DataTemplate MediaServerTemplate { get; set; }

        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            /**
             *  DataGridColumn에 대한 DataTemplate
             *  
             *  item이 항상 null값이 넘어온다.. Bug??
             *  직접 control element에 접근해서 값을 가져옴 (trick)
             */

            ContentPresenter presenter = container as ContentPresenter;
            DataGridCell cell = presenter.Parent as DataGridCell;
            SystemMonitor monitor = cell.DataContext as SystemMonitor;


            if (cell != null && monitor != null)
            {
               
                if (monitor.IsCamera)
                {
                    return MediaServerTemplate;
                }
                else
                {
                    return DefaultServerTemplate;
                }

            }




            return null;


        }
    }
}
