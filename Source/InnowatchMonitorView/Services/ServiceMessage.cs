﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using RPN.Common.Core.Message;

namespace InnowatchMonitorView.Services
{
    public class ServiceMessage : Messages
    {
        // DoRefresh SystemStatus
        public const string DoRefreshSystemStatus = "DoRefreshSystemStatus";
        public const string GetRefreshSystemStatus = "GetRefreshSystemStatus";

        // BackGround Data Fetching
        public const string GetSystemStatus = "GetSystemStatus";
        public const string ReceviedSystemStatus = "ReceviedSystemStatus";

        // SystemStatusService Control
        public const string ControlSystemStatusService = "ControlSystemStatusService";

        ///// Camera

        // DoRefresh CameraStatus
        public const string DoRefreshCameraStatus = "DoRefreshCameraStatus";
        public const string GetRefreshCameraStatus = "GetRefreshCameraStatus";

        // BackGround Data Fetching
        public const string GetCameraStatus = "GetCameraStatus";
        public const string ReceviedCameraStatus = "ReceviedCameraStatus";

        // CameraStatusService Control
        public const string ControlCameraStatusService = "ControlCameraStatusService";

        //Test
        public const string TestService = "TestService";


    }
}
