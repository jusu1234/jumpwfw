﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.Text;
using System.Threading;
using System.ComponentModel.Composition;

using RPN.Common.Logging;
using RPN.Common.Util;
using RPN.Common.Core.Tasks;
using RPN.Common.Core.Service;
using RPN.Common.Core.Message;
using RPN.Common.Core.RX;

using InnowatchMonitorData.Models;

namespace InnowatchMonitorView.Services
{
    [Export(typeof(IService))]
    public class ServerStatusService : StatusService, IService
    {

        Random rnd = new Random();
        Logger log_ = LoggerFactory.GetLogger(typeof(ServerStatusService));

        public string ServiceName { get; set; }
   
        public event EventHandler<ServiceEventArgs> DataReceived;
        
        public ServerStatusService()
        {
            this.ServiceName = "ServerStatusService";
            
        }

        #region ImmediaticTask
        [RegisterTask(ServiceMessage.ControlSystemStatusService, TaskType.ImmediaticTask)]
        private void ControlSystemStatusService(int ctl)
        {

            if (!ControlService(ctl))
            {
                log_.Warn("Wrong Command Ignored");
            }
            else
            {
                log_.Info( string.Format("StatusService Control Command {0}", ctl));

            }

        }

        [RegisterTask(ServiceMessage.DoRefreshSystemStatus, TaskType.ImmediaticTask)]
        private void RefreshSystemStatusData()
        {
            log_.Debug("SystemStatus Refreshing Request Start");

            ConcurrentDictionary<string, SystemMonitor> monitors =
                new ConcurrentDictionary<string, SystemMonitor>();

             Performance pf = new Performance();

            pf.BeginTimeWatch();


            Pararell.ForEach(ServerDataModelContainer.GetInstance.GetServerDataList().Values,
                server =>
                {
                    SystemMonitor monitor;
                    GetSystemStatusData(server, out monitor);

                    // ServiceWatcher 결과를 콜렉션에 저장..
                    if (monitor != null)
                    {
                        monitors.AddOrUpdate(monitor.ServerID, monitor, (n, oldValue) => monitor);
                       // monitors_.TryAdd(monitor.ServerID, monitor);
                    }

                }
            );

            pf.EndTimeWatch();
            TimeSpan ts = pf.GetTimeWatchDuration();
            log_.Debug(string.Format("SystemStatus Refreshing Complete Elapsed {0}ms", ts.TotalMilliseconds));

            // Refreshing 결과 전송
            Mediator.GetInstance.BroadCast(ServiceMessage.GetRefreshSystemStatus, monitors.Values);

        }
        #endregion

        #region BackgroundTask
        /// <summary>
        /// ServiceWatcher로 데이터 요청하는 함수
        /// </summary>
        [RegisterTask(ServiceMessage.GetSystemStatus , TaskType.BackgroundTask)]
        private void GetSystemStatusData()
        {
            // 시작시 멈춤
            startEvent_.WaitOne();

            log_.Debug("SystemStatus Data Fetching Start");

            ConcurrentDictionary<string, SystemMonitor> monitors =
               new ConcurrentDictionary<string, SystemMonitor>();

            while (!shutDown_ )
            {
                TimeSpan timeStamp = TimeSpan.FromTicks(DateTime.UtcNow.Ticks);


                // 병렬 처리

                Performance pf = new Performance();

                pf.BeginTimeWatch();

            //    foreach(ServerDataModelBase server in ServerDataModelContainer.GetInstance.GetServerDataList().Values)
                Pararell.ForEach(ServerDataModelContainer.GetInstance.GetServerDataList().Values,
                    server => 
                    {
                        SystemMonitor monitor;
                        GetSystemStatusData(server, out monitor);

                    
                        // ServiceWatcher 결과를 콜렉션에 저장..
                        if (monitor != null )
                        {
                            monitors.AddOrUpdate(monitor.ServerID, monitor, (n, oldValue) => monitor);
                        }
                    
                    }

                );

                pf.EndTimeWatch();
                TimeSpan ts = pf.GetTimeWatchDuration();
                log_.Debug(string.Format("SystemStatus Data Fetching Completed Elapsed {0}ms", ts.TotalMilliseconds));

                EventHandler<ServiceEventArgs> handler = DataReceived;

                if (handler != null)
                {
                    handler(this,
                            new ServiceEventArgs(new ServiceItem("update", monitors.Values)));
                }

                pauseEvent_.WaitOne();
                Thread.Sleep(1000);
            }

            log_.Debug("SystemStatus Data Fetching Stop");
            
        }
        #endregion

        #region private Func
        private void GetSystemStatusData(ServerDataModelBase server, out SystemMonitor monitor)
        {

            //  Performance pf1 = new Performance();

            DateTime dt = DateTime.Now;
            
            monitor = new SystemMonitor();


            //   pf1.BeginTimeWatch();

            MemoryInfo memInfo;
            IEnumerable<HddInfo> hddInfos;

            /*
            string pingInfoStr;
            string cpuInfoStr;
            string hddInfoStr;
            string memInfoStr;

           

            ///////////////////////////////////////
            // ServiceWatcher 호출
            //////////////////////////////////

            // string serviceUrl = server.ServiceURL;

            string serviceUrl = "http://localhost:5000/rest/watcher/";
            
            pingInfoStr = RestServiceHelper.Request(serviceUrl + "pingtime?host=" + server.ServerIP);
            cpuInfoStr = RestServiceHelper.Request(serviceUrl + "cpuinfo");
            hddInfoStr = RestServiceHelper.Request(serviceUrl + "hddinfo");
            memInfoStr = RestServiceHelper.Request(serviceUrl + "meminfo");
            */
            monitor.ServerID = server.ServerID;
            monitor.IsCamera = server.IsCamera;

            /*
            //
            // XML Parsing
            ServiceWatcherHelper.GetMemoryInfo(memInfoStr, out memInfo);
            ServiceWatcherHelper.GetHddInfo(hddInfoStr, out hddInfos);


                    
            monitor.Ping = Convert.ToInt32(pingInfoStr);  rnd.Next(0, 100); ; 
            monitor.Cpu = Convert.ToInt32(cpuInfoStr);  rnd.Next(0, 100);        

            monitor.Memory = memInfo != null ? memInfo : null;
            monitor.Hdds = hddInfos != null ? hddInfos : null;
            */

            monitor.Ping = rnd.Next(0, 100);
            monitor.Cpu = rnd.Next(0, 100);

            int totalMemory = 8192;
            int availableMemory = rnd.Next(0, 8192);
            memInfo = new MemoryInfo(totalMemory, availableMemory,
                Convert.ToInt32(Convert.ToDouble(availableMemory) / Convert.ToDouble(totalMemory) * 100));

            monitor.Memory = memInfo;
            List<HddInfo> hddInfoList = new List<HddInfo>();


            hddInfoList.Add(new HddInfo("Total", rnd.Next(0, 194830), 194830));

            hddInfos = hddInfoList;
            monitor.Hdds = hddInfos;
            monitor.Check_Time = dt;


            //     pf1.EndTimeWatch();

            //      log_.Debug(string.Format("Loop Service Time Execute Elapsed {0} ms", pf1.GetTimeWatchDuration().TotalMilliseconds));

        }
        #endregion
    }
}
