﻿using InnowatchMonitorData.Models;
using RPN.Common.Core.Message;
using RPN.Common.Core.Service;
using RPN.Common.Core.Tasks;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;

namespace InnowatchMonitorView.Services
{
    [Export(typeof(ServiceObserver))]
    public class CameraStatusServiceObserver : ServiceObserver
    {
        public CameraStatusServiceObserver()
        {
            this.ServiceName = "CameraStatusService";
            this.RegisterEvent(OnEvent);
        }

        public void OnEvent(IEnumerable<object> serviceItems)
        {
            IEnumerable<CameraMonitor> monitors = null;
            foreach (object o in serviceItems)
            {
                bool bUpdate = o is IEnumerable<CameraMonitor>;

                if (bUpdate == false)
                    return;

                monitors = (IEnumerable<CameraMonitor>)o;
            }

            // ViewModel에 받은 데이터 전송
            Mediator.GetInstance.BroadCast(ServiceMessage.ReceviedCameraStatus,
               monitors);
        }
    }
}
