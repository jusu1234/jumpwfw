﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.Composition;
using System.Collections.ObjectModel;
using System.Windows.Input;

using RPN.Common.Core.ViewModel;
using RPN.Common.Core.Command;
using RPN.Common.Core.Message;
using RPN.Common.Core.Service;
using RPN.Common.Core.Tasks;
using RPN.Common.Core.Collection;

using InnowatchMonitorData.Models;
using InnowatchMonitorView.Services;
using RPN.Common.Util;
using System.Xml.Linq;
using System.Xml;
using System.Text.RegularExpressions;
using System.Collections;
using System.Collections.Specialized;
using System.Windows;
using RPN.Common.Core;


namespace InnowatchMonitorView.ViewModels
{

    [Export(typeof(IViewModel))]
    [View(typeof(InnowatchMonitorView.Views.CUM_Alarm))]
    class CUM_AlarmViewModel : ViewModelBase , IViewModel
    {
        #region IView member
        static string viewName_;
        public string ViewName
        {
            get { return viewName_; }
            set
            {
                viewName_ = value;
                RaisePropertyChanged(() => ViewName);
            }
        }
        #endregion

        #region field

        ICommand _searchCommand;
        ICommand _addRuleCommand;
        ICommand _deleteRuleCommand;

        ObservableCollection<AlarmSource> _commonCodeCollection;
        ObservableCollection<AlarmRule> _alarmRuleCollection;

        
        #endregion

        #region CLR member

        public ObservableCollection<AlarmSource> CommonCodeList
        {
            get { return _commonCodeCollection; }

            set
            {
                _commonCodeCollection = value;
                RaisePropertyChanged(() => CommonCodeList);
            }
        }

        public ObservableCollection<AlarmRule> AlarmRuleList
        {
            get { return _alarmRuleCollection; }

            set
            {
                _alarmRuleCollection = value;
                RaisePropertyChanged(() => AlarmRuleList);
            }
        }


        #endregion

        #region Command

        public ICommand SearchCommand
        {
            get
            {
                if (_searchCommand == null)
                {
                    _searchCommand = new DelegateCommand<object>((P) => SearchExecute(P), CanExecute);
                }
                return _searchCommand;
            }
        }

        public ICommand AddRuleCommand
        {
            get
            {
                if (_addRuleCommand == null)
                {
                    _addRuleCommand = new DelegateCommand<object>((P) => AddRuleExecute(P), CanExecute);
                }
                return _addRuleCommand;
            }
        }

        public ICommand DeleteRuleCommand
        {
            get
            {
                if (_deleteRuleCommand == null)
                {
                    _deleteRuleCommand = new DelegateCommand<object>((P) => DeleteRuleExecute(P), CanExecute);
                }
                return _deleteRuleCommand;
            }
        }



        private void SearchExecute(object parameter)
        {
            //Mediator.GetInstance.BroadCast(ServiceMessage.GetCameraStatus);

            // TODO : 서버리스트와 MAPPING RULE 조인해서 가져오기

            this.ViewRef.DataContext = this;
        }

        private void AddRuleExecute(object parameter)
        {
            //Mediator.GetInstance.BroadCast(ServiceMessage.GetCameraStatus);
            //MessageBox.Show("Clicked AddRule", "AddRule", MessageBoxButton.OK, MessageBoxImage.Information);

            _alarmRuleCollection.Add(new AlarmRule { RuleId = "1", RuleName = "Ping response slow", RuleAction = "" });

        }

        private void DeleteRuleExecute(object parameter)
        {
            //Mediator.GetInstance.BroadCast(ServiceMessage.GetCameraStatus);
            int index = AlarmRuleList.IndexOf(parameter as AlarmRule);
            if (index > -1 && index < AlarmRuleList.Count)
            {
                AlarmRuleList.RemoveAt(index);
            }

        }

        private bool CanExecute(object parameter)
        {
            return true;
        }



        #endregion

        public CUM_AlarmViewModel() : base("AlarmView", true, true)
        {
            ViewName = "Alarm";

            //Mediator.GetInstance.BroadCast(ServiceMessage.TestService);

            _commonCodeCollection = new ObservableCollection<AlarmSource>()
            {
                new AlarmSource{ Id = "0", Name = "Disk Usage", limitValue="100", IsChecked = false, IsEnabled=true},
                new AlarmSource{ Id = "1", Name = "CPU Usage",  limitValue="90",IsChecked = true, IsEnabled=true},
                new AlarmSource{ Id = "2", Name = "Ping",  limitValue="200",IsChecked = false, IsEnabled=true},
                new AlarmSource{ Id = "3", Name = "Memory",  limitValue="100",IsChecked = false, IsEnabled=true},
                new AlarmSource{ Id = "4", Name = "Camera Ping",  limitValue="200",IsChecked = false, IsEnabled=false},
                new AlarmSource{ Id = "5", Name = "Camera RecordStatus",  limitValue="OFF",IsChecked = false, IsEnabled=false}

            };

            _alarmRuleCollection = new ObservableCollection<AlarmRule>()
            {
               // new AlarmRule{ RuleId = "0", RuleName = "Disk Quotes Exceeded", RuleAction = ""},
               // new AlarmRule{ RuleId = "1", RuleName = "High CPU Usage", RuleAction = ""},
               // new AlarmRule{ RuleId = "2", RuleName = "Ping response slow", RuleAction = ""},

            };


        }
    }

    class AlarmSource
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string limitValue { get; set; }
        public bool IsEnabled { get; set; }
        public bool IsChecked { get; set; }
    }

    class AlarmRule
    {
        public string RuleId { get; set; }
        public string RuleName { get; set; }
        public List<AlarmSource> RuleSources { get; set; }
        public string RuleAction { get; set; }
        
    }

    class AlarmRuleMapping
    {
        public string SystemId { get; set; }
        public string SystemType { get; set; }
        public AlarmRule MappingedRule { get; set; }
    }

    

    /*
    class ObservableDictionary<TKey, TValue> : IDictionary, INotifyCollectionChanged
    {

    }
     * */


}
