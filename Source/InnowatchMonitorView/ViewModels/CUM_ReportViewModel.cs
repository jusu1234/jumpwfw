﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.Composition;
using System.Collections.ObjectModel;
using System.Windows.Input;

using RPN.Common.Core.ViewModel;
using RPN.Common.Core.Command;
using RPN.Common.Core.Message;
using RPN.Common.Core.Service;
using RPN.Common.Core.Tasks;
using RPN.Common.Core.Collection;

using InnowatchMonitorData.Models;
using InnowatchMonitorView.Services;
using RPN.Common.Util;
using System.Xml.Linq;
using System.Xml;
using System.Text.RegularExpressions;


namespace InnowatchMonitorView.ViewModels
{

    [Export(typeof(IViewModel))]
    [View(typeof(InnowatchMonitorView.Views.CUM_Report))]
    class CUM_ReportViewModel : ViewModelBase , IViewModel
    {
        #region IView member
        static string viewName_;
        public string ViewName
        {
            get { return viewName_; }
            set
            {
                viewName_ = value;
                RaisePropertyChanged(() => ViewName);
            }
        }
        #endregion

        #region field

        ICommand _searchCommand;
        ObservableCollection<ReportType> _commonCodeCollection;
        
        #endregion

        #region CLR member

        public ObservableCollection<ReportType> CommonCodeList
        {
            get { return _commonCodeCollection; }

            set
            {
                _commonCodeCollection = value;
                RaisePropertyChanged(() => CommonCodeList);
            }
        }

        #endregion

        #region Command

        public ICommand SearchCommand
        {
            get
            {
                if (_searchCommand == null)
                {
                    _searchCommand = new DelegateCommand<object>((P) => Execute(P), CanExecute);
                }
                return _searchCommand;
            }
        }

        private void Execute(object parameter)
        {
            //Mediator.GetInstance.BroadCast(ServiceMessage.GetCameraStatus);

        }

        private bool CanExecute(object parameter)
        {
            return true;
        }

        #endregion

        public CUM_ReportViewModel() : base("ReportView", true, true)
        {
            ViewName = "Report";

            //Mediator.GetInstance.BroadCast(ServiceMessage.TestService);
            try
            {
                string xml = RestServiceHelper.Request(RestServiceHelper.DATA_SERVICE_URL + "GetCommonCode?code=RPT", false);

                if (!string.IsNullOrWhiteSpace(xml))
                {
                    System.Diagnostics.Debug.WriteLine(xml);
                    
                    Regex.Split(xml, "\0");
                    XDocument doc = XDocument.Parse(xml);

                    /*
                    var list = doc.Descendants("CodeDescription")
                                       .Select(el => el.Value)
                                       .ToList();

                    _commonCodeCollection = new ObservableCollection<ReportType>();
                    foreach (string desc in list)
                    {
                        _commonCodeCollection.Add(new ReportType { Name=desc});
                    }
                    */

                    
                    IEnumerable<ReportType> lst =
                        from el in doc.Descendants("Table")
                        select new ReportType()
                        {
                            Id = (string)el.Element("Code").Value,
                            Name = (string)el.Element("CodeDescription").Value
                        };
                    _commonCodeCollection = new ObservableCollection<ReportType>();
                    foreach (ReportType rpt in lst)
                    {
                        _commonCodeCollection.Add(rpt);
                    }
                    
                     
                }
                
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("===========" + e.Message);
            }
            
        }
    }

    public class ReportType
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }

}
