﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.Composition;
using System.Collections.ObjectModel;
using System.Windows.Input;
using System.Windows.Controls;

using RPN.Common.Core.ViewModel;
using RPN.Common.Core.Command;
using RPN.Common.Core.Message;
using RPN.Common.Core.Service;
using RPN.Common.Core.Tasks;
using RPN.Common.Core.Collection;

using InnowatchMonitorData.Models;
using InnowatchMonitorView.Services;

namespace InnowatchMonitorView.ViewModels
{
    [Export(typeof(IViewModel))]
    [View(typeof(InnowatchMonitorView.Views.CUM_CameraView))]
    class CUM_CameraViewModel : ViewModelBase , IViewModel
    {
        #region IView member
        
        static string viewName_;
        public string ViewName
        {
            get 
            {
                return viewName_; 
            }

            set
            {
                viewName_ = value;
                RaisePropertyChanged(() => ViewName);
            }
        }

        #endregion

        #region field

        CameraMonitorCollection cameraDataList_;
        CameraMonitor selectedCameraData_;

        ICommand refreshCommand_;

        #endregion

        #region CLR member

        public CameraMonitorCollection CameraDataList
        {
            get
            {
                return cameraDataList_;
            }

            set
            {
                cameraDataList_ = value;
                RaisePropertyChanged(() => CameraDataList);
            }
        }

        public CameraMonitor SelectedCameraData
        {
            get
            {
                return selectedCameraData_;
            }

            set
            {
                selectedCameraData_ = value;
                RaisePropertyChanged(() => SelectedCameraData);
            }
        }

        #endregion

        #region Command

        public ICommand RefreshCommand
        {
            get
            {
                if (refreshCommand_ == null)
                {
                    refreshCommand_ = new DelegateCommand(DoRefresh);
                }

                return refreshCommand_;
            }
        }

        public void DoRefresh()
        {
            Mediator.GetInstance.BroadCast(ServiceMessage.DoRefreshCameraStatus);
        }

        #endregion

        private readonly DataGrid dg_;

        public CUM_CameraViewModel()
            : base("CameraMonitorView", true, false)
        {
            ViewName = "Camera";

            cameraDataList_ = new CameraMonitorCollection();

            dg_ = this.GetElement<DataGrid>("CameraDataGrid_Simple");
            dg_.SelectionMode = DataGridSelectionMode.Single;



           Mediator.GetInstance.BroadCast(ServiceMessage.GetCameraStatus);
        }

        [RegisterTask(ServiceMessage.ReceviedCameraStatus, TaskType.PeriodicTask)]
        private void ReceviedData(IEnumerable<CameraMonitor> monitors)
        {
           
            Action e = () =>
            {
                CameraMonitor monitor = this.SelectedCameraData;

                cameraDataList_.AddOrUpdate(monitors, true);

               // CollectionSorter.Sort<CameraMonitor>(cameraDataList_,
               //      (x, y) => x.CameraID.CompareTo(y.CameraID));

                this.SelectedCameraData = monitor;

                if (SelectedCameraData != null)
                {
                    dg_.SelectedItem = SelectedCameraData;
                    DataGridRow row = (DataGridRow)dg_.ItemContainerGenerator.ContainerFromIndex(dg_.SelectedIndex);
                    //   dg_.MoveFocus(new TraversalRequest(FocusNavigationDirection.Next));
                    row.IsSelected = true;
                }

            };

            ProcessOnDispatcherThread(e);
        }

        [RegisterTask(ServiceMessage.GetRefreshCameraStatus, TaskType.ImmediaticTask)]
        private void GetRefreshData(IEnumerable<CameraMonitor> monitors)
        {
            Action e = () =>
            {
                CameraMonitor monitor = this.SelectedCameraData;

                cameraDataList_.AddOrUpdate(monitors, true);

                // CollectionSorter.Sort<CameraMonitor>(cameraDataList_,
                //      (x, y) => x.CameraID.CompareTo(y.CameraID));

                this.SelectedCameraData = monitor;

                if (SelectedCameraData != null)
                {
                    dg_.SelectedItem = SelectedCameraData;
                    DataGridRow row = (DataGridRow)dg_.ItemContainerGenerator.ContainerFromIndex(dg_.SelectedIndex);
                    //   dg_.MoveFocus(new TraversalRequest(FocusNavigationDirection.Next));
                    row.IsSelected = true;
                }
            };

            ProcessOnDispatcherThread(e);
        }
    }

    public class CameraMonitorCollection : SuspendableObserableCollection<CameraMonitor>
    {

    }
}
