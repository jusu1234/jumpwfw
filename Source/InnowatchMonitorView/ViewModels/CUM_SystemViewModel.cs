﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.Composition;
using System.Collections.ObjectModel;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Controls;

using RPN.Common.Core.ViewModel;
using RPN.Common.Core.Command;
using RPN.Common.Core.Message;
using RPN.Common.Core.Service;
using RPN.Common.Core.Tasks;
using RPN.Common.Core.Collection;

using InnowatchMonitorData.Models;
using InnowatchMonitorView.Services;


namespace InnowatchMonitorView.ViewModels
{

    [Export(typeof(IViewModel))]
    [View(typeof(InnowatchMonitorView.Views.CUM_SystemView))]
    class CUM_SystemViewModel : ViewModelBase , IViewModel
    {
        #region IView member
        static string viewName_;
        public string ViewName
        {
            get { return viewName_; }
            set
            {
                viewName_ = value;
                RaisePropertyChanged(() => ViewName);
            }
        }
        #endregion

        #region field

        DateTime currentTime_;
        ServerMonitorCollection systemDataList_;
        SystemMonitor selectedSystemData_;
       
        bool isAllCheck_;

        ICommand checkCommand_;
        ICommand refreshCommand_;
        
        #endregion

        #region CLR member

        public DateTime CurrentTime
        {
            get { return currentTime_; }
            set
            {
                currentTime_ = value;
                RaisePropertyChanged(() => CurrentTime);
            }
        }

        public ServerMonitorCollection SystemDataList
        {
            get
            {
                return systemDataList_;
            }

            set
            {
                systemDataList_ = value;
                RaisePropertyChanged(() => SystemDataList);
            }
        }

        public SystemMonitor SeletedSystemData
        {
            get { return selectedSystemData_; }

            set
            {
                selectedSystemData_ = value;
                RaisePropertyChanged(() => SeletedSystemData);
            }
        }

        public bool IsAllCheck
        {
            get { return isAllCheck_; }
            set
            {
                isAllCheck_ = value;
                RaisePropertyChanged(() => IsAllCheck);
            }
        }
      
        #endregion

        #region Command

        public ICommand CheckCommand
        {
            get
            {
                if (checkCommand_ == null)
                {
                    checkCommand_ = new DelegateCommand<bool>(DoCheckAll);
                }

                return checkCommand_;
            }


        }

        public void DoCheckAll(bool bCheck)
        {
           
            foreach (SystemMonitor monitor in SystemDataList)
            {
                monitor.IsChecked = bCheck;
            }

            IsAllCheck = bCheck;


            SystemDataList.ResumCollectionNotifyChaned();
        }

        public ICommand RefreshCommand
        {
            get
            {
                if (refreshCommand_ == null)
                {
                    refreshCommand_ = new DelegateCommand(DoRefresh);
                }

                return refreshCommand_;
            }

        }

        public void DoRefresh()
        {
            log_.Debug("Refresh System Status"); 
            Mediator.GetInstance.BroadCast(ServiceMessage.DoRefreshSystemStatus);
        }

        #endregion

        private readonly DataGrid dg_;


        public CUM_SystemViewModel()
            : base("SystemMoitorView", true, true)
        {
            ViewName = "System";

            dg_ = this.GetElement<DataGrid>("SystemDataGrid_Simple");
            dg_.SelectionMode = DataGridSelectionMode.Single;

            systemDataList_ = new ServerMonitorCollection();

            //DoRefresh();


            // 시간 업데이트 함수 등록
            //  Mediator.GetInstance.RegisterTask<DateTime>(ServiceMessage.StateUpdate, OnTextChanged, TaskType.PeriodicTask);


            // Data Fetching 작업 시작
            Mediator.GetInstance.BroadCast(ServiceMessage.GetSystemStatus);
            Mediator.GetInstance.BroadCast(ServiceMessage.ControlSystemStatusService, StatusService.START);
        }

        [RegisterTask(ServiceMessage.ReceviedSystemStatus, TaskType.PeriodicTask)]
        private void ReceviedData(IEnumerable<SystemMonitor> monitors)
        {
         
            Action e = () =>
                {

                    SystemMonitor monitor = SeletedSystemData;

                    systemDataList_.AddOrUpdate(monitors, true);


               //     CollectionSorter.Sort<SystemMonitor>(systemDataList_,
                //         (x, y) => x.ServerID.CompareTo(y.ServerID));

                    this.SeletedSystemData = monitor;


                    if (SeletedSystemData != null)
                    {
                        dg_.SelectedItem = SeletedSystemData;
                        DataGridRow row = (DataGridRow)dg_.ItemContainerGenerator.ContainerFromIndex(dg_.SelectedIndex);
                     //   dg_.MoveFocus(new TraversalRequest(FocusNavigationDirection.Next));
                          //row.IsSelected = true;
                    }

              
                };

            ProcessOnDispatcherThread(e);
          
        
        }
        [RegisterTask(ServiceMessage.GetRefreshSystemStatus, TaskType.ImmediaticTask)]
        private void GetRefreshData(IEnumerable<SystemMonitor> monitors)
        {
            
            Action e = () =>
            {
                systemDataList_.AddOrUpdate(monitors, true);


                CollectionSorter.Sort<SystemMonitor>(systemDataList_,
                     (x, y) => x.ServerID.CompareTo(y.ServerID));

                monitors.ToList().Clear();

            };

            ProcessOnDispatcherThread(e);
        }


        public void OnTextChanged(DateTime dt)
        {
            CurrentTime = dt;
        }



      
    }


    public class ServerMonitorCollection : SuspendableObserableCollection<SystemMonitor>
    {

    }
}
