﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using System.ComponentModel.Composition;

using RPN.Common.Core.Data;
using RPN.Common.Core.Tasks;
using RPN.Common.Core.Service;
using RPN.Common.Core.Message;

using RPN.MON.Data;
using RPN.MON.Data.Models;

namespace RPN.MON.SystemControl.Services
{
    [Export(typeof(ServiceObserver))]
    public class ServerStatusServiceObserver : ServiceObserver
    {
        public ServerStatusServiceObserver()
        {
            this.ServiceName = "ServerStatusService";

            this.RegisterEvent(OnEvent);
        }

        public void OnEvent(IEnumerable<object> serviceItems)
        {

            IEnumerable<SystemMonitor> monitors = null;
            foreach (object o in serviceItems)
            {
                bool bUpdate = o is IEnumerable<SystemMonitor>;

                if (bUpdate == false)
                    return;

                monitors = (IEnumerable<SystemMonitor>)o;
            }



            // ViewModel에 받은 데이터 전송
            Mediator.GetInstance.BroadCast(ServiceMessage.ReceviedSystemStatus ,
               monitors);
        }


       

    }
}
