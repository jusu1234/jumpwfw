﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

using RPN.MON.Data.Models;

namespace RPN.MON.SystemControl.Views
{
    public class MemoryInfoSorter : IComparer
    {
        bool bDescending_;

        public MemoryInfoSorter(bool bDescending)
        {
            bDescending_ = bDescending;
        }

        public int Compare(object x, object y)
        {
            SystemMonitor s1 = (SystemMonitor)x;
            SystemMonitor s2 = (SystemMonitor)x;

            MemoryInfo m1 = s1.Memory;
            MemoryInfo m2 = s2.Memory;

            if (m1 == null || m2 == null)
                return 0;

            int comp = m2.MemoryLoad - m1.MemoryLoad;
            return bDescending_ == true ? -comp : comp;
        }
    }

    public class HddInfoSorter : IComparer
    {
        bool bDescending_;

        public HddInfoSorter(bool bDescending)
        {
            bDescending_ = bDescending;
        }

        public int Compare(object x, object y)
        {

            SystemMonitor s1 = (SystemMonitor)x;
            SystemMonitor s2 = (SystemMonitor)x;

            HddInfo h1, h2;
            IEnumerable<HddInfo> hddInfoList;


            if (s1.Hdds == null || s2.Hdds == null)
                return 0;

            hddInfoList = s1.Hdds;

            // Total만 꺼냄
            h1 = (from h in hddInfoList
                            where h.DriveLetter.Equals("Total")
                            select h).ToList()[0];


            hddInfoList = s2.Hdds;

            // Total만 꺼냄
            h2 = (from h in hddInfoList
                        where h.DriveLetter.Equals("Total")
                        select h).ToList()[0];

            int p1 = Convert.ToInt32(
               Convert.ToDouble(h1.FreeMByte) / (Convert.ToDouble(h1.TotalMByte)) * 100);

            int p2 = Convert.ToInt32(
               Convert.ToDouble(h2.FreeMByte) / (Convert.ToDouble(h2.TotalMByte)) * 100);

            int comp = p2 - p1;
            return bDescending_ == true ? -comp : comp;
        }
    }
    
}
