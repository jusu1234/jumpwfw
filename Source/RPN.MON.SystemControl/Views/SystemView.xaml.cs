﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;


using RPN.MON.SystemControl.ViewModels;
using Infragistics.Controls.Charts;
using System.Windows.Media.Animation;

namespace RPN.MON.SystemControl.Views
{
    /// <summary>
    /// UserControl1.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class SystemView : UserControl
    {
        public SystemView()
        {
            //    InitializeComponent();
        }


        private void ChkAll_Checked(object sender, RoutedEventArgs e)
        {
            SystemViewModel vm = (SystemViewModel)DataContext;

            vm.DoCheckAll(true);
        }

        private void ChkAll_Unchecked(object sender, RoutedEventArgs e)
        {
            SystemViewModel vm = (SystemViewModel)DataContext;

            vm.DoCheckAll(false);
        }

       
        private void DataGrid_Sorting(object sender, DataGridSortingEventArgs e)
        {
            DataGridColumn column = e.Column;

            bool bEnableSorted = column != null && column.CanUserSort && column.DisplayIndex != 7; // Action 제외

            if (bEnableSorted)
            {

                IComparer comparer = null;

                ListSortDirection direction = (column.SortDirection != ListSortDirection.Ascending) ? ListSortDirection.Ascending : ListSortDirection.Descending;

                column.SortDirection = direction;

                ListCollectionView lcv = (ListCollectionView)CollectionViewSource.GetDefaultView(SystemDataGrid_Simple.ItemsSource);


                if (column.DisplayIndex == 4) // MemoryInfo
                {
                    comparer = new MemoryInfoSorter(direction == ListSortDirection.Descending);

                    lcv.CustomSort = comparer;
                }
                else if (column.DisplayIndex == 5) // HddInfo
                {
                    comparer = new HddInfoSorter(direction == ListSortDirection.Descending);

                    lcv.CustomSort = comparer;
                }
                else
                {

                }

                e.Handled = true;


            }
            else
            {
                e.Handled = false;
            }
        }

        private void OnPingSeriesVisibilityClick(object sender, RoutedEventArgs e)
        {
            CheckBox chb = sender as CheckBox;
            if (chb != null && chb.IsChecked != null)
                xmDataChart.Series[0].Opacity = chb.IsChecked.Value ? 1 : 0;
        }

        private void OnHddSeriesVisibilityClick(object sender, RoutedEventArgs e)
        {
            CheckBox chb = sender as CheckBox;
            if (chb != null && chb.IsChecked != null)
                xmDataChart.Series[1].Opacity = chb.IsChecked.Value ? 1 : 0;
        }

        private void OnMemSeriesVisibilityClick(object sender, RoutedEventArgs e)
        {
            CheckBox chb = sender as CheckBox;
            if (chb != null && chb.IsChecked != null)
                xmDataChart.Series[2].Opacity = chb.IsChecked.Value ? 1 : 0;
        }

        private void OnCpuSeriesVisibilityClick(object sender, RoutedEventArgs e)
        {
            CheckBox chb = sender as CheckBox;
            if (chb != null && chb.IsChecked != null)
                xmDataChart.Series[3].Opacity = chb.IsChecked.Value ? 1 : 0;
        }

        private void AnimatePropertyTo(DependencyObject target, double targetValue, string propertyName, double seconds)
        {
            DoubleAnimation da = new DoubleAnimation
            {
                To = targetValue,
                Duration = TimeSpan.FromSeconds(seconds)
            };
            Storyboard sb = new Storyboard
            {
                Duration = TimeSpan.FromSeconds(seconds)
            };
            sb.Children.Add(da);
            Storyboard.SetTarget(da, target);
            Storyboard.SetTargetProperty(da, new PropertyPath(propertyName));

            sb.Begin();
        }

        private void HighlightSeries(Series series)
        {
            AnimatePropertyTo(series, 1.0, "Opacity", 0.3);
            AnimatePropertyTo(series, 5.0, "Thickness", 0);
        }

        private void UnHighlightSeries(Series series)
        {
            AnimatePropertyTo(series, 0.3, "Opacity", 0.3);
            AnimatePropertyTo(series, 3.0, "Thickness", 0);
        }

        private void OnSeriesMouseEnter(object sender, ChartMouseEventArgs e)
        {
            if (e.Series != null)
            {
                HighlightSeries(e.Series);
            }
        }

        private void OnSeriesMouseLeave(object sender, ChartMouseEventArgs e)
        {
            if (e.Series != null)
            {
                UnHighlightSeries(e.Series);
            }
        }

        private void OnLegendItemMouseEnter(object sender, ChartMouseEventArgs e)
        {
            if (e.Series != null)
            {
                HighlightSeries(e.Series);
            }
        }

        private void OnLegendItemMouseLeave(object sender, ChartMouseEventArgs e)
        {
            if (e.Series != null)
            {
                UnHighlightSeries(e.Series);
            }
        }
    }
}
