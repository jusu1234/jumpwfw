﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;

using RPN.MON.Data;
using RPN.MON.Data.Models;

using RPN.Common.Core.Command;
using RPN.Common.Core.Message;


namespace RPN.MON.SystemControl.ViewModels
{
    partial class SystemViewModel
    {
        #region DataGrid 헤더 체크박스
        /// <summary>
        ///  <para> Grid Header 체크박스 부분 클릭시 </para>
        /// </summary>
        ICommand checkCommand_;
        public ICommand CheckCommand
        {
            get
            {
                if (checkCommand_ == null)
                {
                    checkCommand_ = new DelegateCommand<bool>(DoCheckAll);
                }

                return checkCommand_;
            }
        }

        public void DoCheckAll(bool bCheck)
        {

            foreach (SystemMonitor monitor in SystemDataList)
            {
                monitor.IsChecked = bCheck;
            }

            IsAllCheck = bCheck;


            SystemDataList.ResumCollectionNotifyChaned();
        }

        #endregion

        #region 업데이트 버튼

        /// <summary>
        ///  <para> Update 버튼 클릭시 </para>
        /// </summary>
        ICommand refreshCommand_;
        public ICommand RefreshCommand
        {
            get
            {
                if (refreshCommand_ == null)
                {
                    refreshCommand_ = new DelegateCommand(DoRefresh);
                }

                return refreshCommand_;
            }

        }

        private void DoRefresh()
        {
            log_.Debug("Refresh System Status");
            Mediator.GetInstance.BroadCast(ServiceMessage.DoRefreshSystemStatus);
        }

        #endregion

        #region 이력조회 버튼
        /// <summary>
        ///  <para>Grid의 이력조회 버튼 클릭시</para>
        /// </summary>
        ICommand detailViewCommand_;


        public ICommand DetailViewCommand
        {
            get
            {
                if (detailViewCommand_ == null)
                {
                    detailViewCommand_ = new DelegateCommand(DoDetailView);
                }
               
                return detailViewCommand_;
            }

        }

        private void DoDetailView()
        {
            RaisePropertyChanged(() => SeletedSystemData);

            MonitorSearchCondition searchCond = new MonitorSearchCondition();

            searchCond.StartDate = Convert.ToDateTime("2013-01-01");
            searchCond.StartTime = "00:00:00";

            searchCond.EndDate = Convert.ToDateTime("2013-12-31");
            searchCond.EndTime = "23:59:59";

            searchCond.SystemId = "DP_01";

            Mediator.GetInstance.BroadCast(ServiceMessage.SearchSystemStatusHistory, searchCond);
        }





        #endregion

        #region 원격접속 버튼
        /// <summary>
        /// <para> Grid의 원격접속 버튼 클릭시</para>
        /// </summary>
        ICommand remoteDesktopCommand_;

        public ICommand RemoteDesktopCommand
        {
            get
            {
                if (remoteDesktopCommand_ == null)
                {
                    remoteDesktopCommand_ = new DelegateCommand(DoRemoteDesktop);
                }

                return remoteDesktopCommand_;
            }
        }

        // TODO 원격데스크탑 프로그램 실행
        private void DoRemoteDesktop()
        {
        }

        #endregion

        #region 로그보기 버튼
        /// <summary>
        /// <para> Grid의 공유폴더 로그보기 버튼 클릭시</para>
        /// </summary>
        ICommand logListCommand_;
        public ICommand LogListCommand
        {
            get
            {
                if (logListCommand_ == null)
                {
                    logListCommand_ = new DelegateCommand(DoLogList);
                }

                return logListCommand_;
            }
        }

        // TODO 로그보기 기능 구현
        private void DoLogList()
        {
        }


        #endregion

        #region 카메라접속상태 버튼
        /// <summary>
        /// <para>Grid의 카메라접속상태 클릭시</para>
        /// </summary>
        ICommand cameraStatusCommand_;
        public ICommand CameraStatusCommand
        {
            get
            {
                if (cameraStatusCommand_ == null)
                {
                    cameraStatusCommand_ = new DelegateCommand(DoCameraStatus);
                }

                return cameraStatusCommand_;
            }
        }

        // TODO 카메라접속상태 팝업창 구현
        private void DoCameraStatus()
        {
        }

        #endregion
        

       
    }
}
