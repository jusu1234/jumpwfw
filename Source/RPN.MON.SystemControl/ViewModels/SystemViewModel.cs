﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.Composition;
using System.Collections.ObjectModel;
using System.Windows.Data;
using System.Windows.Controls;
using System.ComponentModel;

using RPN.Common.Core;
using RPN.Common.Core.ViewModel;
using RPN.Common.Core.Command;
using RPN.Common.Core.Message;
using RPN.Common.Core.Service;
using RPN.Common.Core.Tasks;
using RPN.Common.Core.Collection;

using RPN.MON.Data;
using RPN.MON.Data.Models;
using RPN.MON.SystemControl.Services;


using System.Windows.Input;

namespace RPN.MON.SystemControl.ViewModels
{

    [Export(typeof(IViewModel))]
    [View(typeof(RPN.MON.SystemControl.Views.SystemView))]
    partial class SystemViewModel : ViewModelBase, IViewModel
    {
        #region IView member
        static string viewName_;
        public string ViewName
        {
            get { return viewName_; }
            set
            {
                viewName_ = value;
                RaisePropertyChanged(() => ViewName);
            }
        }
        #endregion

        #region field

        
        DateTime currentTime_;
        ServerMonitorCollection systemDataList_;
        SystemMonitor selectedSystemData_;

        ServerMonitorHistoryCollection historyDataList_;

        bool isAllCheck_;

        PropertyObserver<SystemViewModel> propertyObserver_;

        #endregion

        #region Property member

        public DateTime CurrentTime
        {
            get { return currentTime_; }
            set
            {
                currentTime_ = value;
                RaisePropertyChanged(() => CurrentTime);
            }
        }

        public ServerMonitorCollection SystemDataList
        {
            get
            {
                return systemDataList_;
            }

            set
            {
                systemDataList_ = value;
                RaisePropertyChanged(() => SystemDataList);
            }
        }

        public ServerMonitorHistoryCollection HistoryDataList
        {
            get
            {
                return historyDataList_;
            }
            set
            {
                historyDataList_ = value;
                RaisePropertyChanged(() => HistoryDataList);
            }
        }
        /*
                [Notify]
                public SystemMonitor SeletedSystemData { get; set; }
         */

        public SystemMonitor SeletedSystemData
        {
            get { return selectedSystemData_; }

            set 
            {
                selectedSystemData_ = value;
                //    RaisePropertyChanged(() => SeletedSystemData);
            }
        }

        public bool IsAllCheck
        {
            get { return isAllCheck_; }
            set
            {
                isAllCheck_ = value;
                RaisePropertyChanged(() => IsAllCheck);
            }
        }


        #endregion



        private readonly DataGrid dg_ = null;


        public SystemViewModel()
            : base("SystemView", true, true)
        {
            ViewName = "System";

            // dg_ = this.GetElement<DataGrid>("SystemDataGrid_Simple");

            if (dg_ != null)
            {
                dg_.SelectionMode = DataGridSelectionMode.Single;
            }

            systemDataList_ = new ServerMonitorCollection();
            historyDataList_ = new ServerMonitorHistoryCollection();

            // 시간 업데이트 함수 등록
            //  Mediator.GetInstance.RegisterTask<DateTime>(ServiceMessage.StateUpdate, OnTextChanged, TaskType.PeriodicTask);


            // Data Fetching 작업 시작
            Mediator.GetInstance.BroadCast(ServiceMessage.GetSystemStatus);
            Mediator.GetInstance.BroadCast(ServiceMessage.ControlSystemStatusService, StatusService.START);
        }


        [RegisterTask(ServiceMessage.ReceviedSystemStatus, TaskType.PeriodicTask)]
        private void ReceviedData(IEnumerable<SystemMonitor> monitors)
        {
            if (monitors == null)
                return;
            Action e = () =>
                {
                    SystemMonitor monitor = SeletedSystemData;

                    systemDataList_.AddOrUpdate(monitors, true);                   

                    //     CollectionSorter.Sort<SystemMonitor>(systemDataList_,
                    //         (x, y) => x.ServerID.CompareTo(y.ServerID));

                    this.SeletedSystemData = monitor;


                    if (dg_ != null && SeletedSystemData != null)
                    {
                        dg_.SelectedItem = SeletedSystemData;
                        DataGridRow row = (DataGridRow)dg_.ItemContainerGenerator.ContainerFromIndex(dg_.SelectedIndex);
                        //   dg_.MoveFocus(new TraversalRequest(FocusNavigationDirection.Next));
                        row.IsSelected = true;
                    }


                };

            ProcessOnDispatcherThread(e);


        }
        [RegisterTask(ServiceMessage.GetRefreshSystemStatus, TaskType.ImmediaticTask)]
        private void GetRefreshData(IEnumerable<SystemMonitor> monitors)
        {
            if (monitors == null)
                return;
            log_.Debug("GetRefreshData");
            Action e = () =>
            {
                SystemMonitor monitor = SeletedSystemData;

                systemDataList_.AddOrUpdate(monitors, true);


                //    CollectionSorter.Sort<SystemMonitor>(systemDataList_,
                //          (x, y) => x.ServerID.CompareTo(y.ServerID));

                this.SeletedSystemData = monitor;


                if (dg_ != null && SeletedSystemData != null)
                {
                    dg_.SelectedItem = SeletedSystemData;
                    DataGridRow row = (DataGridRow)dg_.ItemContainerGenerator.ContainerFromIndex(dg_.SelectedIndex);
                    //   dg_.MoveFocus(new TraversalRequest(FocusNavigationDirection.Next));
                    row.IsSelected = true;
                }

            };

            ProcessOnDispatcherThread(e);
        }

        [RegisterTask(ServiceMessage.GetServerStatusHistory, TaskType.ImmediaticTask)]
        private void GetServerStatusHistory(IEnumerable<SystemMonitorHistory> monitorHistory)
        {
            if (monitorHistory == null)
                return;
            log_.Debug("GetServerStatusHistory");

            Action e = () =>
                {
                    historyDataList_.Clear();
                    historyDataList_.AddOrUpdate(monitorHistory, true);

                    

                };

            ProcessOnDispatcherThread(e);
        }


        public void OnTextChanged(DateTime dt)
        {
            CurrentTime = dt;
        }




    }

    public class ServerMonitorHistoryCollection : SuspendableObserableCollection<SystemMonitorHistory>
    {

    }

    public class ServerMonitorCollection : SuspendableObserableCollection<SystemMonitor>
    {

    }
}
