﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.Text;
using System.Threading;

using RPN.Common.Core.Message;
using RPN.Common.Core.RX;
using RPN.Common.Core.Service;
using RPN.Common.Core.Tasks;
using RPN.Common.Logging;
using RPN.Common.Util;

using InnowatchMonitorData.Models;

namespace InnowatchMonitorConsoleService.Services
{
    public class CameraStatusService : IService
    {
        public string ServiceName { get; set; }

        Logger log_ = Logger.GetLogger("CameraStatusService");

        public event EventHandler<ServiceEventArgs> DataReceived;

        public CameraStatusService()
        {
            ServiceName = "CameraStatusService";
            Mediator.GetInstance.RegisterTask(ServiceMessage.GetCameraStatus, GetCameraStatusData, TaskType.BackgroundTask);
        }

        /// <summary>
        /// ServiceWatcher로 데이터 요청하는 함수
        /// </summary>
        public void GetCameraStatusData()
        {
            log_.Debug("CameraStatus Data Fetching Start");

            for (; ; )
            {
                ConcurrentDictionary<string, CameraMonitor> monitors =
                    new ConcurrentDictionary<string, CameraMonitor>();

                // 병렬 처리

                Performance pf = new Performance();

                pf.BeginTimeWatch();

                Pararell.ForEach(CameraDataModelContainer.GetInstance.GetCameraDataList().Values,
                    camera =>
                    {
                        CameraMonitor monitor;
                        GetCameraStatusData(camera, out monitor);

                        // ServiceWatcher 결과를 콜렉션에 저장..
                        if (monitor != null && !monitors.ContainsKey(monitor.CameraID))
                        {
                            monitors.TryAdd(monitor.CameraID, monitor);
                        }
                    }
                );

                pf.EndTimeWatch();
                TimeSpan ts = pf.GetTimeWatchDuration();
                log_.Debug(string.Format("CameraStatus Data Fetching Completed Elapsed {0}ms", ts.TotalMilliseconds));


                EventHandler<ServiceEventArgs> handler = DataReceived;

                if (handler != null)
                {
                    handler(this,
                            new ServiceEventArgs(new ServiceItem("update", monitors.Values)));
                }

                Thread.Sleep(1000);
            }
        }

        private void GetCameraStatusData(CameraDataModel camera, out CameraMonitor monitor)
        {
            DateTime dt = DateTime.Now;
            Random rnd = new Random();

            monitor = new CameraMonitor();

            ///////////////////////////////////////
            // ServiceWatcher 호출
            //////////////////////////////////

            monitor.CameraID = camera.CameraID;
            monitor.RecordNo = camera.RecordCameraNumber;
            monitor.Md = camera.MediaServerID;
            monitor.Ping = rnd.Next(0, 100);
            monitor.CheckTime = dt;

            //
            // XML Parsing
        }

    }
}
