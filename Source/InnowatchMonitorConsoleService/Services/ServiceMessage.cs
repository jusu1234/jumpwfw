﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using RPN.Common.Core.Message;

namespace InnowatchMonitorConsoleService.Services
{
    public class ServiceMessage : Messages
    {
        // BackGround Data Fetching
        public const string GetSystemStatus = "GetSystemStatus";
        public const string ReceviedSystemStatus = "ReceviedSystemStatus";

        public const string GetCameraStatus = "GetCameraStatus";
        public const string ReceviedCameraStatus = "ReceviedCameraStatus";
    }
}
