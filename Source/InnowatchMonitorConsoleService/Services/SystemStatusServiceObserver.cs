﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using RPN.Common.Core.Tasks;
using RPN.Common.Core.Service;
using RPN.Common.Core.Message;
using RPN.Common.Core.Data;

using InnowatchMonitorData.Models;

namespace InnowatchMonitorConsoleService.Services
{
    public class SystemStatusServiceObserver : ServiceObserver
    {
        public SystemStatusServiceObserver()
        {
            this.ServiceName = "ServerStatusService";

            this.RegisterEvent(OnEvent);
        }

        public void OnEvent(IEnumerable<object> serviceItems)
        {
            IEnumerable<SystemMonitor> monitors = null;
            foreach (object o in serviceItems)
            {
                bool bUpdate = o is IEnumerable<SystemMonitor>;

                if (bUpdate == false)
                    return;

                monitors = (IEnumerable<SystemMonitor>)o;
            }



            // ViewModel에 받은 데이터 전송
            Mediator.GetInstance.BroadCast(ServiceMessage.ReceviedSystemStatus ,
               monitors);
        }
    }
}
