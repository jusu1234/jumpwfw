﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.Text;
using System.Threading;

using RPN.Common.Util;
using RPN.Common.Logging;
using RPN.Common.Core.Message;
using RPN.Common.Core.Service;
using RPN.Common.Core.RX;
using RPN.Common.Core.Tasks;

using InnowatchMonitorData.Models;

namespace InnowatchMonitorConsoleService.Services
{
    public class SystemStatusService : IService
    {
        public string ServiceName { get; set; }

        Logger log_ = Logger.GetLogger(typeof(SystemStatusService));
        
        public event EventHandler<ServiceEventArgs> DataReceived;

        public SystemStatusService()
        {
            this.ServiceName = "StatusService";
            Mediator.GetInstance.RegisterTask(ServiceMessage.GetSystemStatus, GetSystemStatusData, TaskType.BackgroundTask);
        }


        public void GetSystemStatusData()
        {
            log_.Debug("SystemStatus Data Fetching Start");

            for (;;)
            {
                Thread.Sleep(1);

                ConcurrentDictionary<string, SystemMonitor> monitors =
                    new ConcurrentDictionary<string, SystemMonitor>();

                // 병렬 처리

                Performance pf = new Performance();

                pf.BeginTimeWatch();


                Pararell.ForEach(ServerDataModelContainer.GetInstance.GetServerDataList().Values,
                    server =>
                    {
                        SystemMonitor monitor;
                        GetSystemStatusData(server, out monitor);

                        // ServiceWatcher 결과를 콜렉션에 저장..
                        if (monitor != null && !monitors.ContainsKey(monitor.ServerID))
                        {
                            monitors.TryAdd(monitor.ServerID, monitor);
                        }

                    }

                );

                pf.EndTimeWatch();
                TimeSpan ts = pf.GetTimeWatchDuration();
                log_.Debug(string.Format("SystemStatus Data Fetching Completed Elapsed {0}ms", ts.TotalMilliseconds));


                EventHandler<ServiceEventArgs> handler = DataReceived;

                if (handler != null)
                {
                    handler(this,
                            new ServiceEventArgs(new ServiceItem("update", monitors.Values)));
                }

               
            }
        }


        private void GetSystemStatusData(ServerDataModelBase server, out SystemMonitor monitor)
        {

            //  Performance pf1 = new Performance();

            DateTime dt = DateTime.Now;
            Random rnd = new Random();

            monitor = new SystemMonitor();


            //   pf1.BeginTimeWatch();
            string pingInfoStr;
            string cpuInfoStr;
            string hddInfoStr;
            string memInfoStr;

            MemoryInfo memInfo;
            IEnumerable<HddInfo> hddInfos;

            ///////////////////////////////////////
            // ServiceWatcher 호출
            //////////////////////////////////

            // string serviceUrl = server.ServiceURL;

            string serviceUrl = "http://localhost:5000/rest/watcher/";
            pingInfoStr = RestServiceHelper.Request(serviceUrl + "pingtime?host=" + server.ServerIP);
            cpuInfoStr = RestServiceHelper.Request(serviceUrl + "cpuinfo");
            hddInfoStr = RestServiceHelper.Request(serviceUrl + "hddinfo");
            memInfoStr = RestServiceHelper.Request(serviceUrl + "meminfo");

            monitor.ServerID = server.ServerID;
            monitor.IsCamera = server.IsCamera;

            /*
            //
            // XML Parsing
            ServiceWatcherHelper.GetMemoryInfo(memInfoStr, out memInfo);
            ServiceWatcherHelper.GetHddInfo(hddInfoStr, out hddInfos);
                    
            monitor.Ping = Convert.ToInt32(pingInfoStr);  rnd.Next(0, 100); ; 
            monitor.Cpu = Convert.ToInt32(cpuInfoStr);  rnd.Next(0, 100);        

            monitor.Memory = memInfo != null ? memInfo : null;
            monitor.Hdds = hddInfos != null ? hddInfos : null;
            */

            monitor.Ping = rnd.Next(0, 100);
            monitor.Cpu = rnd.Next(0, 100);

            int totalMemory = 8192;
            int availableMemory = rnd.Next(0, 8192);
            memInfo = new MemoryInfo(totalMemory, availableMemory,
                Convert.ToInt32(Convert.ToDouble(availableMemory) / Convert.ToDouble(totalMemory) * 100));

            monitor.Memory = memInfo;
            List<HddInfo> hddInfoList = new List<HddInfo>();


            hddInfoList.Add(new HddInfo("Total", rnd.Next(0, 194830), 194830));

            hddInfos = hddInfoList;
            monitor.Hdds = hddInfos;
            monitor.Check_Time = dt;


            //     pf1.EndTimeWatch();

            //      log_.Debug(string.Format("Loop Service Time Execute Elapsed {0} ms", pf1.GetTimeWatchDuration().TotalMilliseconds));

        }
    }
}
