﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.ServiceProcess;
using System.Configuration.Install;

namespace InnowatchMonitorConsoleService.Execute
{
    [RunInstaller(true)]
    public class N3NServiceInstaller : Installer
    {


        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        private ServiceProcessInstaller serviceProcessInstaller_;
        private ServiceInstaller serviceInstaller_;
        

        public N3NServiceInstaller()
        {
            serviceProcessInstaller_ = new ServiceProcessInstaller();
            serviceInstaller_ = new ServiceInstaller();

            serviceProcessInstaller_.Account = ServiceAccount.LocalSystem;
            serviceProcessInstaller_.Username = null;
            serviceProcessInstaller_.Password = null;

            serviceInstaller_.DisplayName = "N3N Monitor Window Service";
            serviceInstaller_.ServiceName = "N3N_Monitor";
            serviceInstaller_.StartType = ServiceStartMode.Automatic;
            serviceInstaller_.AfterInstall += new InstallEventHandler(AfterInstall);

            this.Installers.AddRange(new Installer[] {
                serviceProcessInstaller_,
                serviceInstaller_
            });
        }



        private void AfterInstall(object sender, InstallEventArgs e)
        {

        }

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}
