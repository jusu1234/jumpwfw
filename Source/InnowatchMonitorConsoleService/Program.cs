﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

using RPN.Common.Logging;


namespace InnowatchMonitorConsoleService
{
    class Program
    {
        private static Logger log_ = Logger.GetLogger("Program");

        static void Main(string[] args)
        {
            var appDomain = AppDomain.CurrentDomain;
            appDomain.UnhandledException += UnhandledException;

            bool isConsole;
            string mode = Convert.ToString(ConfigurationManager.AppSettings["RUNNING_MODE"]);
            isConsole = mode.Equals("Console");
        }


        private static void UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            var exception = (Exception)e.ExceptionObject;
            log_.Error( string.Format("UnhandledException : {0}", exception.ToString()));
        }
    }
}
