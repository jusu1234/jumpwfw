﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

using RPN.Common.Logging;
using RPN.Common.Core.Message;
using RPN.Common.Core.Service;
using RPN.Common.Core.Tasks;

using RPN.MON.Data;
using RPN.MON.Data.Models;
using RPN.MON.LogService.Services;

namespace RPN.MON.LogService
{
    public class ServiceController
    {
        static private Logger log_ = LoggerFactory.GetLogger(typeof(ServiceController));

        private SystemStatusService systemStatusService_;
        private SystemStatusServiceObserver systemStatusServiceObserver_;

        private CameraStatusService cameraStatusService_;
        private CameraStatusServiceObserver cameraStatusServiceObserver_;

        private DatabaseWorker worker_;
        private AlarmWorker alarm_;

        public ServiceController()
        {
            ServerDataModelContainer container = ServerDataModelContainer.GetInstance;

            // 실제 데이터 처리..
            worker_ = new DatabaseWorker();

            //알람
            alarm_ = new AlarmWorker();

            // 서비스 생성 및 Observer 등록
            systemStatusService_ = new SystemStatusService();
            systemStatusServiceObserver_ = new SystemStatusServiceObserver();
            systemStatusServiceObserver_.RegisterService(systemStatusService_);

            cameraStatusService_ = new CameraStatusService();
            cameraStatusServiceObserver_ = new CameraStatusServiceObserver();
            cameraStatusServiceObserver_.RegisterService(cameraStatusService_);

            // Data Fetching 준비
            Mediator.GetInstance.BroadCast(ServiceMessage.GetSystemStatus);
            Mediator.GetInstance.BroadCast(ServiceMessage.GetCameraStatus);
        }

        #region ControlService
        public void Run()
        {
            log_.Debug("Try Start AIMonitor");
            Mediator.GetInstance.BroadCast(ServiceMessage.ControlSystemStatusService,
                        StatusService.START);
            Mediator.GetInstance.BroadCast(ServiceMessage.ControlCameraStatusService,
                       StatusService.START);
        }


        public void Pause()
        {
            log_.Debug("Try Pause AIMonitor");
            Mediator.GetInstance.BroadCast(ServiceMessage.ControlSystemStatusService,
                        StatusService.PAUSE);

            Mediator.GetInstance.BroadCast(ServiceMessage.ControlCameraStatusService,
                      StatusService.PAUSE);
            
        }

        public void Resume()
        {
            log_.Debug("Try Resume AIMonitor");
            Mediator.GetInstance.BroadCast(ServiceMessage.ControlSystemStatusService,
                        StatusService.RESUME);
            Mediator.GetInstance.BroadCast(ServiceMessage.ControlCameraStatusService,
                     StatusService.RESUME);
        }

        public void Stop()
        {
            log_.Debug("Try Stop AIMonitor");
            Mediator.GetInstance.BroadCast(ServiceMessage.ControlSystemStatusService,
                       StatusService.STOP);
            Mediator.GetInstance.BroadCast(ServiceMessage.ControlCameraStatusService,
                    StatusService.STOP);
        }
        #endregion

       
    }
}
