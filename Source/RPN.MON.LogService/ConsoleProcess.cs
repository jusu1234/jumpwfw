﻿using System;
using System.Linq;
using System.Text;
using System.Threading;
using System.Configuration;
using System.Collections.Generic;

using RPN.Common.Logging;
using RPN.Common.Core.Message;

using RPN.MON.LogService.Services;


namespace RPN.MON.LogService
{
    public class ConsoleProcess
    {
        static private Logger log_ = LoggerFactory.GetLogger(typeof(ConsoleProcess));

 
        ServiceController monitor_;

        bool shutDown_;
 
        public ConsoleProcess()
        {
            shutDown_ = false;
            monitor_ = new ServiceController();
        }
        
        public void Run()
        {
          //  log_.Debug("Start Conosole");

           

            while (!shutDown_)
            {
                Console.Write("명령어를 입력하세요. (0 :시작 , 1: 종료, 2: 일시정지 3: 재시작) : ");

                string str = Console.ReadLine();

                try 
                {
                    switch (Convert.ToInt32(str))
                    {
                        case 0:
                            monitor_.Run();
                            break;
                        case 1:
                            shutDown_ = true;
                            break;
                        case 2:
                            monitor_.Pause();
                            break;
                        case 3:
                            monitor_.Resume();
                            break;
                        default :
                            throw new Exception();
                    }

                } catch(Exception ex)
                {
                    Console.WriteLine("잘못된 명령입니다.");
                }
               

            }


        }

    }
}
