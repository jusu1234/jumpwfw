﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

using RPN.Common.Logging;
using RPN.Common.Core.Message;
using RPN.Common.Core.Tasks;

using RPN.MON.Data;
using RPN.MON.Data.Models;

using InnowatchDataHandler;
using InnowatchDataHandler.Commons;

namespace RPN.MON.LogService
{
    public class DatabaseWorker
    {
        Logger log_ = LoggerFactory.GetLogger(typeof(DatabaseWorker));

        // Parameter
        private const string SystemId = "SystemId";
        private const string SystemType = "SystemType";
        private const string StatusCode = "StatusCode";
        private const string StatusValue = "StatusValue";
        private const string CheckTime = "CheckTime";

        // Insert 
        private static readonly string _ = string.Format("INSERT INTO ServerStatusHistory({0}, {1}, {2}, {3}, {4}, LastUpdateDate ) VALUES",
            new string[] { SystemId, SystemType, StatusCode, StatusValue, CheckTime }) ;

        private string InsertServerQuery = _ + string.Format(" ( @{0} , @{1} , @{2} , @{3}, @{4}, DEFAULT)",
                                                    new string[] { SystemId, SystemType, StatusCode, StatusValue, CheckTime });

        private static readonly string __ = string.Format("INSERT INTO CameraStatusHistory({0}, {1}, {2}, {3},LastUpdateDate) VALUES",
            new string[] { SystemId,  StatusCode, StatusValue, CheckTime });
        private string InsertCamerarQuery = __ + string.Format(" ( @{0} , @{1} , @{2} , @{3}, DEFAULT)",
                                                    new string[] { SystemId, StatusCode, StatusValue, CheckTime });

        private DataHandler handler_ =   DataManager.GetInstance.DataHandler;

        public DatabaseWorker()
        {
             // SystemStatusData Receiver 등록
            Mediator.GetInstance.RegisterTask<IEnumerable<SystemMonitor>>(ServiceMessage.ReceviedSystemStatus, GetSystemStatus,
                TaskType.PeriodicTask);

            // CameraStatusData Receiver 등록
            Mediator.GetInstance.RegisterTask<IEnumerable<CameraMonitor>>(ServiceMessage.ReceviedCameraStatus, GetCameraStatus,
                TaskType.PeriodicTask);

        }


        #region Receiver

        private void GetSystemStatus(IEnumerable<SystemMonitor> systems)
        {

            log_.Debug(string.Format("[DATABASE] Recevied SystemStatus {0}", systems.ToList().Count()));
            

            List<QueryInfo> queryList = new List<QueryInfo>();


            foreach (SystemMonitor system in systems)
            {

                UpdateServerStatusHistoryDB(system, queryList);
            }
             
            handler_.SetExecuteDataSet(queryList);
          
        }

        private void GetCameraStatus(IEnumerable<CameraMonitor> cameras)
        {
            log_.Debug(string.Format("[DATABASE] Recevied CameraStatus {0}", cameras.ToList().Count()));

            List<QueryInfo> queryList = new List<QueryInfo>();


            foreach (CameraMonitor camera in cameras)
            {

                UpdateCameraStatusHistoryDB(camera, queryList);
            }

            handler_.SetExecuteDataSet(queryList);
        }

        #endregion

        #region Update ServerStatusHistoryDB

        private void UpdateServerStatusHistoryDB(SystemMonitor system, IList<QueryInfo> queryList)
        {
        
            queryList.Add(GetPingInsertQuery(system));
            queryList.Add(GetCpuInsertQuery(system));

            GetInsertMemoryQuery(system, queryList);
            GetInsertDiskQuery(system, queryList);
        }


        private QueryInfo GetPingInsertQuery(SystemMonitor system)
        {
          
            ParamInfo[] paramInfos = new ParamInfo[]
            { 
               new ParamInfo("@" + SystemId, DbType.String , system.ServerID ),
               new ParamInfo("@" +  SystemType, DbType.String , system.ServerType ),
               new ParamInfo("@" +  StatusCode, DbType.String , "NET_SER" ),
               new ParamInfo("@" +  StatusValue, DbType.Int32 , system.Ping ),
               new ParamInfo("@" +  CheckTime, DbType.DateTime , system.Check_Time )
            };

            return new QueryInfo(InsertServerQuery, paramInfos);

        }


        private QueryInfo GetCpuInsertQuery(SystemMonitor system)
        {
            ParamInfo[] paramInfos = new ParamInfo[]
            { 
               new ParamInfo("@" + SystemId, DbType.String , system.ServerID ),
               new ParamInfo("@" + SystemType, DbType.String , system.ServerType ),
               new ParamInfo("@" + StatusCode, DbType.String , "CPU" ),
               new ParamInfo("@" + StatusValue, DbType.Int32 , system.Cpu ),
               new ParamInfo("@" + CheckTime, DbType.DateTime , system.Check_Time )
            };

            return new QueryInfo(InsertServerQuery, paramInfos);

        }

        private void GetInsertMemoryQuery(SystemMonitor system, IList<QueryInfo> queryList)
        {
            ParamInfo[] paramInfos;

            paramInfos = new ParamInfo[]
            { 
               new ParamInfo("@" + SystemId, DbType.String , system.ServerID ),
               new ParamInfo("@" + SystemType, DbType.String , system.ServerType ),
               new ParamInfo("@" + StatusCode, DbType.String , "MEM_FREE" ),
               new ParamInfo("@" + StatusValue, DbType.Int32 , system.Memory.AvailMemory ),
               new ParamInfo("@" + CheckTime, DbType.DateTime , system.Check_Time )
            };


            queryList.Add(new QueryInfo(InsertServerQuery, paramInfos));

            paramInfos = new ParamInfo[]
            { 
               new ParamInfo("@" + SystemId, DbType.String , system.ServerID ),
               new ParamInfo("@" + SystemType, DbType.String , system.ServerType ),
               new ParamInfo("@" + StatusCode, DbType.String , "MEM_TOT" ),
               new ParamInfo("@" + StatusValue, DbType.Int32 , system.Memory.TotalMemory ),
               new ParamInfo("@" + CheckTime, DbType.DateTime , system.Check_Time )
            };

            queryList.Add(new QueryInfo(InsertServerQuery, paramInfos));


            paramInfos = new ParamInfo[]
            { 
               new ParamInfo("@" + SystemId, DbType.String , system.ServerID ),
               new ParamInfo("@" + SystemType, DbType.String , system.ServerType ),
               new ParamInfo("@" + StatusCode, DbType.String , "MEM_LOAD" ),
               new ParamInfo("@" + StatusValue, DbType.Int32 , system.Memory.MemoryLoad ),
               new ParamInfo("@" + CheckTime, DbType.DateTime , system.Check_Time )
            };

            queryList.Add(new QueryInfo(InsertServerQuery, paramInfos));
        }

        private void GetInsertDiskQuery(SystemMonitor system, IList<QueryInfo> queryList)
        {
            string strTemp = string.Empty;
            foreach (HddInfo hdd in system.Hdds)
            {
                if (hdd.DriveLetter.Equals("TOTAL"))
                    strTemp = "TOT";
                else
                    strTemp = Convert.ToString(hdd.DriveLetter[0]);

                List<ParamInfo> paramInfos = new List<ParamInfo>();
                paramInfos.Add(new ParamInfo("@" + SystemId, DbType.String, system.ServerID));
                paramInfos.Add(new ParamInfo("@" + SystemType, DbType.String, system.ServerType));

                paramInfos.Add(new ParamInfo("@" + StatusCode, DbType.String, string.Format("DISK_{0}_FREE", strTemp)));
                paramInfos.Add(new ParamInfo("@" + StatusValue, DbType.Int32, hdd.FreeMByte));
                paramInfos.Add(new ParamInfo("@" + CheckTime, DbType.DateTime, system.Check_Time));

                queryList.Add(new QueryInfo(InsertServerQuery, paramInfos));
            }


            foreach (HddInfo hdd in system.Hdds)
            {

                if (hdd.DriveLetter.Equals("TOTAL"))
                    strTemp = "TOT";
                else
                    strTemp = Convert.ToString(hdd.DriveLetter[0]);


                List<ParamInfo> paramInfos = new List<ParamInfo>();
                paramInfos.Add(new ParamInfo("@" + SystemId, DbType.String, system.ServerID));
                paramInfos.Add(new ParamInfo("@" + SystemType, DbType.String, system.ServerType));
                paramInfos.Add(new ParamInfo("@" + StatusCode, DbType.String, string.Format("DISK_{0}_TOTAL", strTemp)));
                paramInfos.Add(new ParamInfo("@" + StatusValue, DbType.Int32, hdd.FreeMByte));
                paramInfos.Add(new ParamInfo("@" + CheckTime, DbType.DateTime, system.Check_Time));

                queryList.Add(new QueryInfo(InsertServerQuery, paramInfos));
            }
        }
        #endregion

        #region Update CameraStatusHistoryDB
        private void UpdateCameraStatusHistoryDB(CameraMonitor camera, IList<QueryInfo> queryList)
        {
            queryList.Add(GetCamPingInsertQuery(camera));
            queryList.Add(GetRecordStatusInsertQuery(camera));
        }

        private QueryInfo GetCamPingInsertQuery(CameraMonitor camera)
        {
            ParamInfo[] paramInfos = new ParamInfo[]
            { 
               new ParamInfo("@" + SystemId, DbType.String , camera.CameraID ),
               new ParamInfo("@" + StatusCode, DbType.String , "NET_CAM" ),
               new ParamInfo("@" + StatusValue, DbType.Int32 , camera.Ping ),
               new ParamInfo("@" + CheckTime, DbType.DateTime , camera.CheckTime )
            };

            return new QueryInfo(InsertCamerarQuery, paramInfos);
        }

        
        private QueryInfo GetRecordStatusInsertQuery(CameraMonitor camera)
        {
            ParamInfo[] paramInfos = new ParamInfo[]
            { 
               new ParamInfo("@" + SystemId, DbType.String , camera.CameraID ),
               new ParamInfo("@" + StatusCode, DbType.String , "REC_STAT" ),
               new ParamInfo("@" + StatusValue, DbType.Int32 , camera.RecordStatus ),
               new ParamInfo("@" + CheckTime, DbType.DateTime , camera.CheckTime )
            };

            return new QueryInfo(InsertCamerarQuery, paramInfos);
        }

        #endregion
    }
}
