﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using RPN.Common.Core.Message;
using RPN.Common.Core.Tasks;
using RPN.Common.Logging;
using RPN.MON.Data;
using RPN.MON.Data.Models;

namespace RPN.MON.LogService
{

    /// <summary>
    /// ALARM 처리 클래스
    /// </summary>
    
    // TODO : ALARM Rule에 따른 조건 검사 및 ALARM 발생
    public class AlarmWorker
    {
        Logger log_ = LoggerFactory.GetLogger(typeof(AlarmWorker));

        public AlarmWorker()
        {
            //SystemStatusData Receiver 등록
            Mediator.GetInstance.RegisterTask<IEnumerable<SystemMonitor>>(ServiceMessage.ReceviedSystemStatus, GetSystemStatus,
                TaskType.PeriodicTask);

            // CameraStatusData Receiver 등록
            Mediator.GetInstance.RegisterTask<IEnumerable<CameraMonitor>>(ServiceMessage.ReceviedCameraStatus, GetCameraStatus,
                TaskType.PeriodicTask);
        }


        private void GetSystemStatus(IEnumerable<SystemMonitor> systems)
        {
            log_.Debug(string.Format("[ALARM] Recevied SystemStatus {0}", systems.ToList().Count()));
        }


        private void GetCameraStatus(IEnumerable<CameraMonitor> cameras)
        {
            log_.Debug(string.Format("[ALARM] Recevied CameraStatus {0}", cameras.ToList().Count()));
        }

    }
}
