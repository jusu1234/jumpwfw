﻿using System;
using System.ComponentModel;
using System.ServiceModel;
using System.ServiceProcess;

namespace RPN.MON.LogService
{
    public class N3NServiceBase : ServiceBase
    {
        /// <summary> 
        /// Required designer variable. 
        /// </summary> 
        private IContainer components_ = null;


        public N3NServiceBase()
        {

            this.CanPauseAndContinue = true;
            this.CanShutdown = true;
            this.CanStop = true;
        }


        protected void InitializeComponent(string serviceName)
        {
            //components_ = new Container();

            this.ServiceName = serviceName;
        }

        /// <summary>
        /// 서비스 시작할시
        /// </summary>
        /// <param name="args"></param>
        protected override void OnStart(string[] args)
        {
            base.OnStart(args);
        }

        /// <summary>
        /// 서비스 종료시
        /// </summary>
        protected override void OnStop()
        {
            base.OnStop();
        }

        /// <summary>
        /// 서비스 일시정지시
        /// </summary>
        protected override void OnPause()
        {
            base.OnPause();
        }

        /// <summary>
        /// 서비스 재시작시
        /// </summary>
        protected override void OnContinue()
        {
            base.OnContinue();
        }

        /// <summary>
        /// System ShutDown 발생시
        /// </summary>
        protected override void OnShutdown()
        {
            base.OnShutdown();
        }

        /// <summary>
        /// ServiceContoller.ExcuteCommand(command)
        /// 명령으로 서비스에 명령을 줄수 있다.
        /// </summary>
        /// <param name="command"></param>
        protected override void OnCustomCommand(int command)
        {
            base.OnCustomCommand(command);
        }

        protected override void Dispose(bool disposing)
        {
            
            if (disposing && (components_ != null))
            {
                components_.Dispose();
            }
             

            base.Dispose(disposing);
        }

    }
}
