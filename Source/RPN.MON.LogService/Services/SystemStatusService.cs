﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.Text;
using System.Threading;

using RPN.Common.Util;
using RPN.Common.Logging;
using RPN.Common.Core.Message;
using RPN.Common.Core.Service;
using RPN.Common.Core.RX;
using RPN.Common.Core.Tasks;

using RPN.MON.Data;
using RPN.MON.Data.Models;
using RPN.MON.Data.Util;

namespace RPN.MON.LogService.Services
{
    public class SystemStatusService : StatusService, IService
    {
        public string ServiceName { get; set; }

        Logger log_ = LoggerFactory.GetLogger(typeof(SystemStatusService));
        
        public event EventHandler<ServiceEventArgs> DataReceived;

        Random rnd_ = new Random();
     
        public SystemStatusService()
        {
            this.ServiceName = "SystemStatusService";

            Mediator.GetInstance.RegisterTask(ServiceMessage.GetSystemStatus,
               GetSystemStatusData, TaskType.BackgroundTask);

            Mediator.GetInstance.RegisterTask<int>(ServiceMessage.ControlSystemStatusService ,
                ControlSystemStatusService, TaskType.ImmediaticTask);
        }

         #region ImmediaticTask
        private void ControlSystemStatusService(int ctl)
        {

            if (!ControlService(ctl))
            {
                log_.Warn("Wrong Command Ignored");
            }
            else
            {
                log_.Info( string.Format("SystemStatusService Control Command {0}", ctl));

            }
        }
        #endregion

        #region BackgroundTask
        private void GetSystemStatusData()
        {
            // 시작시 멈춤
            startEvent_.WaitOne();

            log_.Debug("SystemStatus Data Fetching Start");

            while (!shutDown_)
            {
                Thread.Sleep(1);

                // 병렬 처리
                Performance pf = new Performance();

                pf.BeginTimeWatch();

               //foreach(ServerDataModelBase server in ServerDataModelContainer.GetInstance.GetServerDataList().Values)
                Pararell.ForEach(ServerDataModelContainer.GetInstance.GetServerDataList().Values,
                   server =>
                    {
                        SystemMonitor monitor;
                        GetSystemStatusData(server, out monitor);
                        
                       
                        // Event 발생시킴
                        EventHandler<ServiceEventArgs> handler = DataReceived;

                        if (handler != null && monitor != null)
                        {
                            handler(this,
                                    new ServiceEventArgs(new ServiceItem( monitor.ServerID , monitor)));

                            
                        }

                    }

                );

                pf.EndTimeWatch();
                TimeSpan ts = pf.GetTimeWatchDuration();
           //     log_.Debug(string.Format("SystemStatus Data Fetching Completed Elapsed {0}ms", ts.TotalMilliseconds));


                //일시멈춤
               
               pauseEvent_.WaitOne();

       
            }

            log_.Debug("SystemStatus Data Fetching Stop");
            
        }
        #endregion

        #region private func
        private void GetSystemStatusData(ServerDataModelBase server, out SystemMonitor monitor)
        {

            Performance pf1 = new Performance();

            DateTime dt = DateTime.Now;
        
            monitor = new SystemMonitor();


            pf1.BeginTimeWatch();
            string pingInfoStr;
            string cpuInfoStr;
            string hddInfoStr;
            string memInfoStr;

            MemoryInfo memInfo;
            IEnumerable<HddInfo> hddInfos;

            ///////////////////////////////////////
            // ServiceWatcher 호출
            //////////////////////////////////

            // string serviceUrl = server.ServiceURL;

            string serviceUrl = "http://localhost:5000/rest/watcher/";
            pingInfoStr = RestServiceHelper.Request(serviceUrl + "pingtime?host=" + "127.0.0.1");
            cpuInfoStr = RestServiceHelper.Request(serviceUrl + "cpuinfo");
            hddInfoStr = RestServiceHelper.Request(serviceUrl + "hddinfo");
            memInfoStr = RestServiceHelper.Request(serviceUrl + "meminfo");

            monitor.ServerID = server.ServerID;
            monitor.ServerType = server.ServerType;
            
            // XML Parsing
            ServiceWatcherHelper.GetMemoryInfo(memInfoStr, out memInfo);
            ServiceWatcherHelper.GetHddInfo(hddInfoStr, out hddInfos);

            monitor.Ping = Convert.ToInt32(pingInfoStr); 
            monitor.Cpu = Convert.ToInt32(cpuInfoStr);      

       //     monitor.Memory = memInfo != null ? memInfo : null;
       //     monitor.Hdds = hddInfos != null ? hddInfos : null;
            

            monitor.Ping = rnd_.Next(0, 100);
            monitor.Cpu = rnd_.Next(0, 100);

            int totalMemory = 8192;
            int availableMemory = rnd_.Next(0, 8192);
            memInfo = new MemoryInfo(totalMemory, availableMemory,
                Convert.ToInt32(Convert.ToDouble(availableMemory) / Convert.ToDouble(totalMemory) * 100));

            monitor.Memory = memInfo;
            List<HddInfo> hddInfoList = new List<HddInfo>();

            hddInfoList.Add( new HddInfo("C:\\", rnd_.Next(0, 194830), 194830));
            hddInfoList.Add( new HddInfo("D:\\", rnd_.Next(0, 194830), 194830));
            hddInfoList.Add( new HddInfo("TOTAL", rnd_.Next(0, 194830), 194830));
             
            hddInfos = hddInfoList;
            monitor.Hdds = hddInfos;
            monitor.Check_Time = dt;

            pf1.EndTimeWatch();

            //     log_.Debug(string.Format("Loop Service Time Execute Elapsed {0} ms", pf1.GetTimeWatchDuration().TotalMilliseconds));

        }
        #endregion
    }
}
