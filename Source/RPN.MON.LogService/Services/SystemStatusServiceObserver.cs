﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using RPN.Common.Core.Tasks;
using RPN.Common.Core.Service;
using RPN.Common.Core.Message;
using RPN.Common.Core.Data;

using RPN.MON.Data;
using RPN.MON.Data.Models;

namespace RPN.MON.LogService.Services
{
    public class SystemStatusServiceObserver : ServiceObserver
    {
        public SystemStatusServiceObserver()
        {
            this.ServiceName = "SystemStatusService";

            this.RegisterEvent(OnEvent);
        }

        public void OnEvent(IEnumerable<object> serviceItems)
        {
     

            List<SystemMonitor> systems = new List<SystemMonitor>();

            foreach (object o in serviceItems)
            {
                systems.Add((SystemMonitor)o);
            }


            // ViewModel에 받은 데이터 전송
            Mediator.GetInstance.BroadCast(ServiceMessage.ReceviedSystemStatus ,
               systems);
        }
    }
}
