﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.Text;
using System.Threading;

using RPN.Common.Core.Message;
using RPN.Common.Core.RX;
using RPN.Common.Core.Service;
using RPN.Common.Core.Tasks;
using RPN.Common.Logging;
using RPN.Common.Util;

using RPN.MON.Data;
using RPN.MON.Data.Models;

namespace RPN.MON.LogService.Services
{
    public class CameraStatusService : StatusService, IService
    {
        public string ServiceName { get; set; }

        Random rnd_ = new Random();

        private Logger log_ = LoggerFactory.GetLogger(typeof(CameraStatusService) );

        public event EventHandler<ServiceEventArgs> DataReceived;


        public CameraStatusService()
        {
            ServiceName = "CameraStatusService";
            

            Mediator.GetInstance.RegisterTask(ServiceMessage.GetCameraStatus, 
                GetCameraStatusData, TaskType.BackgroundTask);

            Mediator.GetInstance.RegisterTask<int>(ServiceMessage.ControlCameraStatusService,
                ControlCameraStatusService, TaskType.ImmediaticTask);
        }

        private void ControlCameraStatusService(int ctl)
        {

            if (!ControlService(ctl))
            {
                log_.Warn("Wrong Command Ignored");
            }
            else
            {
                log_.Info( string.Format("CameraStatusService Control Command {0}", ctl));

            }

        }

        /// <summary>
        /// ServiceWatcher로 데이터 요청하는 함수
        /// </summary>
        private void GetCameraStatusData()
        {
            // 시작시 멈춤
            startEvent_.WaitOne();

            // 미디어서버에 카메라에  대한 맵핑정보를 생성한다.
            CameraDataModelContainer cameraContainer = CameraDataModelContainer.GetInstance;
            ServerDataModelContainer.GetInstance.CreteCameraMappingInfo();


            log_.Debug("CameraStatus Data Fetching Start");

            while (!shutDown_ )
            {

                Thread.Sleep(1);
            
                // 병렬 처리

                Performance pf = new Performance();

                pf.BeginTimeWatch();

           //     foreach(CameraDataModel camera in CameraDataModelContainer.GetInstance.GetCameraDataList().Values)
                Pararell.ForEach(CameraDataModelContainer.GetInstance.GetCameraDataList().Values,
                    camera =>
                    {
                        CameraMonitor monitor;
                        GetCameraStatusData(camera, out monitor);


                        EventHandler<ServiceEventArgs> handler = DataReceived;

                        if (handler != null)
                        {
                            handler(this,
                                    new ServiceEventArgs(new ServiceItem(monitor.CameraID, monitor)));
                        }
                    }
               );

                pf.EndTimeWatch();
                TimeSpan ts = pf.GetTimeWatchDuration();
          //      log_.Debug(string.Format("CameraStatus Data Fetching Completed Elapsed {0}ms", ts.TotalMilliseconds));

           //     pauseEvent_.Reset();

                pauseEvent_.WaitOne();

            
            }

            log_.Debug("CameraStatus Data Fetching Stop");
        }

        private void GetCameraStatusData(CameraDataModel camera, out CameraMonitor monitor)
        {
            DateTime dt = DateTime.Now;
         
            monitor = new CameraMonitor();


            string pingInfoStr;
            string recordingStatus;

            monitor.CameraID = camera.CameraID;
            monitor.RecordNo = camera.RecordCameraNumber;
            monitor.Md = camera.MediaServerID;

            ///////////////////////////////////////
            // ServiceWatcher 호출
            //////////////////////////////////

            // TODO  RecordingStatus 값 받아오는거 확인 필요.
 
            // string serviceUrl = ServerDataModelContainer.GetInstance.GetMediaServiceURL(camera.MediaServerID);
            // pingInfoStr = RestServiceHelper.Request(serviceUrl + "pingtime?host=" + camera.CameraIP);
            //recordingStatus = RestServiceHelper.Request(serviceUrl + "recordingstatus");

            Thread.Sleep(200);

            pingInfoStr = Convert.ToString(rnd_.Next(0, 100));
            recordingStatus = Convert.ToString(rnd_.Next(0, 2));

            monitor.Ping = Convert.ToInt32(pingInfoStr);
            monitor.RecordStatus = recordingStatus.Equals("1") ? 0 : 1;
            monitor.CheckTime = dt;

        
        }

    }
}
