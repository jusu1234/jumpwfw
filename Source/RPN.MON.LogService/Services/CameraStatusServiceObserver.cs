﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using RPN.Common.Core.Message;
using RPN.Common.Core.Service;
using RPN.Common.Core.Tasks;

using RPN.MON.Data;
using RPN.MON.Data.Models;

namespace RPN.MON.LogService.Services
{
    public class CameraStatusServiceObserver : ServiceObserver
    {
        public CameraStatusServiceObserver()
        {
            this.ServiceName = "CameraStatusService";
            this.RegisterEvent(OnEvent);
        }

        public void OnEvent(IEnumerable<object> serviceItems)
        {
            List<CameraMonitor> cameras = new List<CameraMonitor>();

            foreach (object o in serviceItems)
            {
                cameras.Add((CameraMonitor)o);
            }

            // ViewModel에 받은 데이터 전송
            Mediator.GetInstance.BroadCast(ServiceMessage.ReceviedCameraStatus,
               cameras);
        }
    }
}
