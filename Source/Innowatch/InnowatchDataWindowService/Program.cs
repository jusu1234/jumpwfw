﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Program.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Defines the Program type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace InnowatchDataWindowService
{
    using System;
    using System.ServiceProcess;
    using InnowatchServiceLog;
    using InnowatchServiceType;

    /// <summary>
    /// The program.
    /// </summary>
    public static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        public static void Main()
        {
            ServiceTypeInfo.SetTypeInfo(RunType.WindowService);

            var appDomain = AppDomain.CurrentDomain;

            appDomain.UnhandledException += UnhandledException;

            var servicesToRun = new ServiceBase[]
                                    {
                                        new InnowatchDataWindowService(), 
                                    };

            ServiceBase.Run(servicesToRun);
        }

        private static void UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            var exception = (Exception)e.ExceptionObject;

            Logger.Write(Logger.Error, "UnhandledException", exception.ToString());
        }
    }
}
