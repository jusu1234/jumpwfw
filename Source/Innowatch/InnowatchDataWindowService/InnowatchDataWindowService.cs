﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DataWindowService.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Defines the DataWindowService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace InnowatchDataWindowService
{
    using System;
    using System.Configuration;
    using System.ServiceModel;
    using System.ServiceModel.Web;
    using System.ServiceProcess;

    using InnowatchDataService;

    using InnowatchService;

    using InnowatchServiceLog;

    /// <summary>
    /// The data window service.
    /// </summary>
    public partial class InnowatchDataWindowService : ServiceBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="InnowatchDataWindowService"/> class.
        /// </summary>
        public InnowatchDataWindowService()
        {
            InitializeComponent();

            var dataUrl = ConfigurationManager.AppSettings["DataServiceUrl"];

            this.dataServiceHostForRest = new WebServiceHost(typeof(DataService), new Uri(CheckUrl(dataUrl) + "rest/data/"));
        }

        /// <summary>
        /// The on start.
        /// </summary>
        /// <param name="args">
        /// The args.
        /// </param>
        protected override void OnStart(string[] args)
        {
            try
            {
                this.dataServiceHostForRest.AddServiceEndpoint(
                    typeof(IDataService),
                    new WebHttpBinding
                    {
                        MaxBufferPoolSize = 67108864,
                        MaxBufferSize = 67108864,
                        MaxReceivedMessageSize = 67108864
                    },
                    string.Empty);

                InnowatchNetwork.NetworkHelper.AddDiscovery(this.dataServiceHostForRest, typeof(DataService));                

                this.dataServiceHostForRest.Open();
            }
            catch (CommunicationException cex)
            {
                Logger.Write(Logger.Error, string.Format("Innowatch Data Window Service Start : {0}", cex.Message));
                this.dataServiceHostForRest.Abort();
            }
        }

        /// <summary>
        /// The on stop.
        /// </summary>
        protected override void OnStop()
        {
            try
            {
                this.dataServiceHostForRest.Close();
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, string.Format("Innowatch DAta Window Service Stop : {0}", ex.Message));
            }
        }

        /// <summary>
        /// The check url.
        /// </summary>
        /// <param name="url">
        /// The url.
        /// </param>
        /// <returns>
        /// The string data.
        /// </returns>
        public static string CheckUrl(string url)
        {
            var val = url;

            if (!url.EndsWith("/"))
            {
                val = val + "/";
            }

            if (!url.StartsWith("http://"))
            {
                val = "http://" + val;
            }

            return val;
        }
    }
}
