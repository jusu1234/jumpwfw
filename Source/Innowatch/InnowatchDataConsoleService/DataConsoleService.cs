﻿// -------------------------------------------public -------------------------------------------------------------------------
// <copyright file="DataConsolService.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Defines the DataConsolService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace InnowatchDataConsoleService
{
    using System;
    using System.Configuration;
    using System.ServiceModel;
    using System.ServiceModel.Web;

    using InnowatchDataService;

    using InnowatchService;
    using InnowatchServiceType;
    using InnowatchServiceLog;

    /// <summary>
    /// The data console service.
    /// </summary>
    public class DataConsoleService
    {
        /// <summary>
        /// The main.
        /// </summary>
        /// <param name="args">
        /// The args.
        /// </param>
        public static void Main(string[] args)
        {
            ServiceTypeInfo.SetTypeInfo(RunType.ConsoleService);

            var appDomain = AppDomain.CurrentDomain;

            appDomain.UnhandledException += UnhandledException;

            var dataUrl = ConfigurationManager.AppSettings["DataServiceUrl"];

            var dataServiceHostForRest = new WebServiceHost(typeof(DataService), new Uri(CheckUrl(dataUrl) + "rest/data/"));
            
            try
            {
                dataServiceHostForRest.AddServiceEndpoint(
                    typeof(IDataService),
                    new WebHttpBinding
                        {
                            MaxBufferPoolSize = 67108864, 
                            MaxBufferSize = 67108864, 
                            MaxReceivedMessageSize = 67108864 
                        },
                    string.Empty);

                InnowatchNetwork.NetworkHelper.AddDiscovery(dataServiceHostForRest, typeof(DataService));

                dataServiceHostForRest.Open();
                Console.WriteLine("Start Data Service : ");
                foreach (var item in dataServiceHostForRest.Description.Endpoints)
                {
                    Console.WriteLine("\t" + item.Address);
                }

                Console.ReadLine();
            }
            catch (CommunicationException cex)
            {
                Logger.Write(Logger.Error, string.Format("An exception occurred : {0}", cex.Message));
                dataServiceHostForRest.Abort();
            }
        }

        /// <summary>
        /// The unhandled exception.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private static void UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            var exception = (Exception)e.ExceptionObject;

            Logger.Write(Logger.Error, "UnhandledException", exception.ToString());
        }

        /// <summary>
        /// The check url.
        /// </summary>
        /// <param name="url">
        /// The url.
        /// </param>
        /// <returns>
        /// The url string.
        /// </returns>
        public static string CheckUrl(string url)
        {
            var val = url;

            if (!url.EndsWith("/"))
            {
                val = val + "/";
            }

            if (!url.StartsWith("http://"))
            {
                val = "http://" + val;
            }

            return val;
        }
    }
}
