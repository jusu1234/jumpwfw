﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="InnoTrace.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   디버깅을 위한 TRACE 클래스.
// </summary>
// --------------------------------------------------------------------------------------------------------------------


namespace Innotive.InnoWatch.CommonUtils.Log
{
    using System;
    using System.Diagnostics;
    using System.Reflection;
    using System.Text;

    using log4net;

    /// <summary>
    /// 디버깅을 위한 TRACE 클래스.
    /// </summary>
    public static class InnotiveDebug
    {
        /// <summary>
        /// 로깅을 위한 속성.
        /// </summary>
        public static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public static void WriteLine(string message)
        {
            Debug.WriteLine(
                    "[" + MethodBase.GetCurrentMethod().ReflectedType.FullName + "] " +
                    "[" + MethodBase.GetCurrentMethod().Name + "] " +
                    message);
        }

        /// <summary>
        /// TRACE method.
        /// </summary>
        /// <param name="originClass">
        /// The origin class of the message.
        /// </param>
        /// <param name="priority">
        /// The priority of the message.
        /// </param>
        /// <param name="message">
        /// The message.
        /// </param>
        /// <param name="obj">
        /// String object will be replaced with count in the message.
        /// </param>
        public static void Trace(object originClass, int priority, string message, params object[] obj)
        {
#if DEBUG
            DoTrace(obj, message, originClass, priority);
#endif
        }

        /// <summary>
        /// TRACE method.
        /// </summary>
        /// <param name="priority">
        /// Priority of the message.
        /// </param>
        /// <param name="message">
        /// the Message.
        /// </param>
        /// <param name="obj">
        /// String object will be replaced with count in the message.
        /// </param>
        public static void Trace(int priority, string message, params object[] obj)
        {
            Trace((object)null, priority, message, obj);
        }

        /// <summary>
        /// TRACE method.
        /// </summary>
        /// <param name="message">
        /// the Message.
        /// </param>
        /// <param name="obj">
        /// String object will be replaced with count in the message.
        /// </param>
        public static void Trace(string message, params object[] obj)
        {
            Trace(-1, message, obj);
        }

        /// <summary>
        /// 디버깅용 트레이스 함수
        /// </summary>
        /// <param name="obj">
        /// The obj.
        /// </param>
        /// <param name="message">
        /// The message.
        /// </param>
        /// <param name="originClass">
        /// The origin class.
        /// </param>
        /// <param name="priority">
        /// The priority.
        /// </param>
        private static void DoTrace(object[] obj, string message, object originClass, int priority)
        {
            var count = 0;
            foreach (var strObject in obj)
            {
                string strSubFormat = "{" + count + "}";

                message = strObject == null ? message.Replace(strSubFormat, "null") : message.Replace(strSubFormat, strObject.ToString());

                count++;
            }

            string className = string.Empty;

            if (originClass != null)
            {
                className = originClass.GetType().Name;
            }

            var overallMessage = new StringBuilder();

            if (originClass != null)
            {
                overallMessage.Append("[" + className + "] ");
            }

            overallMessage.Append(message);
            if (priority != -1)
            {
                overallMessage.Append("(Priority : " + priority + ")");
            }

            // System.Diagnostics.Trace.WriteLine(overallMessage.ToString());
            Debug.WriteLine(overallMessage.ToString());
        }

        public static void WriteLogExceptionMessage(Exception ex, string ExceptionKind)
        {
            if (ex != null)
            {
                var dataString = string.Empty;
                dataString += ExceptionKind;
                dataString += string.Format("\nError Message : {0}\n", ex.Message);
                if (null != ex.InnerException)
                {
                    dataString += string.Format("\nInnerException Message : {0}\n", ex.InnerException.Message);
                }

                Log.Error(dataString, ex);
            }
            else
            {
                var dataString = string.Empty;
                dataString += ExceptionKind;

                Log.Error(dataString);
            }
        }

        public static void WriteLogAndTrace(string message)
        {
            Log.Error(message);
            Debug.WriteLine(message);
        }
    }
}