﻿namespace InnowatchServiceLog
{
    using System;
    using System.Diagnostics;
    using System.IO;
    using System.Text;

    /// <summary>
    /// The logger.
    /// </summary>
    public class Logger
    {
        // Show all messages in the log
        public static int Debug = 0;

        // Show information messages and higher
        public static int Info = 1;

        // Show warnings and higer
        public static int Warn = 2;

        // Show error messages and higer
        public static int Error = 3;

        // Show only fatal messages
        public static int Fatal = 4;

        // Show information messages and higher
        public static int Video = 5;

        // 로그 내용을 구분하기 위한 구분자
        public static string LogDelimiter = " | ";

        // Locking을 위해서 사용 Object
        internal static readonly object Lockobj = new object();

        /// <summary>
        /// 로그 메시지에 클래스, 메쏘드 명을 추가해서 로그를 저장
        /// </summary>
        /// <param name="logMode">로그 레벨</param>
        /// <param name="targetClass">타켓 클레스</param>
        /// <param name="methodName">메소드 이름</param>
        /// <param name="message">로그 메시지</param>
        public static void Write(int logMode, object targetClass, string methodName, string message)
        {
            message = targetClass.GetType().Name + LogDelimiter + methodName + LogDelimiter + message;
            Write(logMode, message);
        }

        public static void Write(int logMode, object targetClass, string message)
        {
            Write(logMode, LogDelimiter + targetClass.GetType().Name +
                LogDelimiter + GetMethodInfoStrings.GetCallerMethodName() + LogDelimiter + message);
        }

        /// <summary>
        /// 로그 메시지를 로그 레벨별로 파일에 저장
        /// </summary>
        /// <param name="logMode">로그 레벨</param>
        /// <param name="message">로그 메시지</param>        
        public static void Write(int logMode, string message)
        {
            StreamWriter strWriter = null;
            lock (Lockobj)
            {
                try
                {
                    if ((logMode == Debug) && (Debugger.IsAttached == false))
                        return;

                    string logDir = AppDomain.CurrentDomain.BaseDirectory + "logs\\" + DateTime.Now.ToString("yyyy-MM");

                    if (Directory.Exists(logDir) == false)
                        Directory.CreateDirectory(logDir);

                    string filePath = logDir + "\\InnowatchService_" + DateTime.Now.ToString("yyyy-MM-dd") + ".log";
                    strWriter = new StreamWriter(filePath, true, System.Text.Encoding.Default);
                    strWriter.Write(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + LogDelimiter + GetLogLevelName(logMode) + LogDelimiter + message + Environment.NewLine);
                    strWriter.Flush();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
                finally
                {
                    if (strWriter != null)
                        strWriter.Close();
                }
            }
        }

        /// <summary>
        /// 로그 메시지를 로그 레벨별로 파일에 저장
        /// </summary>
        /// <param name="logMode">로그 레벨</param>
        /// <param name="exception">로그 메세지</param>        
        public static void Write(int logMode, Exception exception)
        {
            StreamWriter strWriter = null;
            lock (Lockobj)
            {
                try
                {
                    if ((logMode == Debug) && (Debugger.IsAttached == false))
                        return;

                    string logDir = AppDomain.CurrentDomain.BaseDirectory + "logs\\" + DateTime.Now.ToString("yyyy-MM");

                    if (Directory.Exists(logDir) == false)
                        Directory.CreateDirectory(logDir);

                    string filePath = logDir + "\\InnowatchService_" + DateTime.Now.ToString("yyyy-MM-dd") + ".log";
                    strWriter = new StreamWriter(filePath, true, System.Text.Encoding.Default);

                    StringBuilder logMessage = new StringBuilder(2048);
                    logMessage.AppendLine("");
                    logMessage.AppendLine("===================================================================================================");
                    logMessage.AppendLine(string.Format("- Exception Time : {0}", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss::fff")));
                    if (false == string.IsNullOrEmpty(GetMethodInfoStrings.GetCalllerFileName()))
                    {
                        logMessage.AppendLine(string.Format("- File Name : {0}", GetMethodInfoStrings.GetCalllerFileName()));
                    }
                    logMessage.AppendLine(string.Format("- Method Name : {0}", GetMethodInfoStrings.GetCallerMethodName()));
                    logMessage.AppendLine("- Message");
                    logMessage.AppendLine(exception.Message);
                    logMessage.AppendLine("- Strack Trace");
                    logMessage.AppendLine(exception.StackTrace);

                    strWriter.Write(logMessage.ToString());
                    strWriter.Flush();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
                finally
                {
                    if (strWriter != null)
                        strWriter.Close();
                }
            }
        }

        /// <summary>
        /// 로그 레벨에 해당하는 로그 레벨명을 리턴
        /// </summary>
        /// <param name="logMode">로그 레벨</param>
        /// <return>로그 레벨명</return>
        public static string GetLogLevelName(int logMode)
        {
            var logLevelName = "";

            if (logMode == Debug)
            {
                logLevelName = "debug";
            }
            else if (logMode == Info)
            {
                logLevelName = "info";
            }
            else if (logMode == Warn)
            {
                logLevelName = "warn";
            }
            else if (logMode == Error)
            {
                logLevelName = "error";
            }
            else if (logMode == Fatal)
            {
                logLevelName = "fatal";
            }
            else if (logMode == Video)
            {
                logLevelName = "video";
            }

            return logLevelName;
        }
    }
}
