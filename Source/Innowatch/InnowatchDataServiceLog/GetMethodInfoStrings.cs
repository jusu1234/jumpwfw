﻿// -----------------------------------------------------------------------
// <copyright file="GetMethodInfoStrings.cs" company="Microsoft">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace InnowatchServiceLog
{
    using System.Diagnostics;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class GetMethodInfoStrings
    {
        public static string GetMethodName(int frame = 1)
        {
            var stackTrace = new StackTrace(true);
            var stackFrame = stackTrace.GetFrame(frame);

            return null != stackFrame.GetMethod() ? stackFrame.GetMethod().Name : "";
        }

        public static string GetFileName()
        {
            var stackTrace = new StackTrace(true);
            var stackFrame = stackTrace.GetFrame(1);

            return stackFrame.GetFileName() ?? "";

        }

        public static string GetFileLineNumber()
        {
            var stackTrace = new StackTrace(true);
            var stackFrame = stackTrace.GetFrame(1);

            return 0 != stackFrame.GetFileLineNumber() ? stackFrame.GetFileLineNumber().ToString() : "";
        }

        public static string GetCallerMethodName()
        {
            var stackTrace = new StackTrace(true);
            var stackFrame = stackTrace.GetFrame(2);

            return null != stackFrame.GetMethod() ? stackFrame.GetMethod().Name : "";
        }

        public static string GetCalllerFileName()
        {
            var stackTrace = new StackTrace(true);
            var stackFrame = stackTrace.GetFrame(2);

            return stackFrame.GetFileName() ?? "";
        }

        public static string GetCallerFileLineNumber()
        {
            var stackTrace = new StackTrace(true);
            var stackFrame = stackTrace.GetFrame(2);

            return 0 != stackFrame.GetFileLineNumber() ? stackFrame.GetFileLineNumber().ToString() : "";
        }
    }
}
