﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using InnowatchServiceType;

namespace InnowatchServiceLog.ConsoleMessage
{
    public class DataServiceMessage : BaseMessage
    {
        public DataServiceMessage()
        {
            this.InitializeDefaultMessage("DataService");
        }

        public DataServiceMessage(params string[] queryStrings)
        {
            this.InitializeDefaultMessage("DataService", queryStrings);
        }
    }
}
