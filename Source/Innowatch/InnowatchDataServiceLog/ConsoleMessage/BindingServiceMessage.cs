﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InnowatchServiceLog.ConsoleMessage
{
    public class BindingServiceMessage : BaseMessage
    {
        public BindingServiceMessage()
        {
            this.InitializeDefaultMessage("BindingService");
        }

        public BindingServiceMessage(params string[] queryStrings)
        {
            this.InitializeDefaultMessage("BindingService", queryStrings);
        }
    }
}
