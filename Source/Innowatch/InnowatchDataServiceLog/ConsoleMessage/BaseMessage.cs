﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using InnowatchServiceType;

namespace InnowatchServiceLog.ConsoleMessage
{
    public class BaseMessage : IDisposable
    {
        /// <summary>
        /// 기본 메시지 접두.
        /// 0: 스레드 ID, 1: 호출 메서드 이름, 2: 메시지.
        /// </summary>
        private string messageFormat;

        private readonly string receiveRequestMessage = "Request received. ";

        private readonly string endRequestMessage = "Request end.";

        private bool isConsoleService;

        /// <summary>
        /// 호출된 QueryString 으로 기본 메시지를 만든다.
        /// </summary>
        /// <param name="serviceName"> </param>
        /// <param name="queryStrings">The QueryString.</param>
        protected void InitializeDefaultMessage(string serviceName, string[] queryStrings = null)
        {
            this.isConsoleService = ServiceTypeInfo.GetTypeInfo() == RunType.ConsoleService;
            if (!this.isConsoleService)
                return;
            try
            {
                this.messageFormat = string.Format(
                           "[{0}] [Thread ID : {1}] {2} : {{0}}",
                           serviceName,
                           Thread.CurrentThread.ManagedThreadId,
                           GetMethodInfoStrings.GetMethodName(3));

                var args = string.Empty;
                if (queryStrings != null)
                {
                    args =
                        queryStrings.Aggregate(string.Empty, (current, s) => current + string.Format("{0},", s));
                    args = args.Remove(args.Length - 1);
                    args = string.Format("({0}) ", args);
                }

                args += this.receiveRequestMessage;
                Console.WriteLine(string.Format(messageFormat, args));
            }
            catch (Exception ex)
            {
                Console.WriteLine(string.Format(messageFormat, ex));
            }
        }

        public void PrintResponseMessage(string message = null)
        {
            if (!this.isConsoleService)
                return;

            var sendMessage = string.IsNullOrWhiteSpace(message) ? string.Empty : string.Format("\r\n{0}", message);
            var sendResponse = string.Format("Send response message{0}", sendMessage);
            Console.WriteLine(string.Format(messageFormat, sendResponse));
        }

        public void PrintExceptionMessage(Exception ex)
        {
            Logger.Write(Logger.Error, ex);

            if (!this.isConsoleService)
                return;

            Console.WriteLine(messageFormat, ex);
        }

        public void PrintMessage(string format, params object[] args)
        {
            if (!this.isConsoleService)
                return;

            var message = string.Format(format, args);

            this.PrintMessage(message);
        }

        public void PrintMessage(string message)
        {
            if (!this.isConsoleService)
                return;

            Console.WriteLine(messageFormat, message);
        }

        #region IDisposable Implement

        public void Dispose()
        {
            if (!this.isConsoleService)
                return;

            Console.WriteLine(string.Format(messageFormat, this.endRequestMessage));
        }

        #endregion
    }
}
