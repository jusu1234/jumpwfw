﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InnowatchServiceLog.ConsoleMessage
{
    public class EventServiceMessage : BaseMessage
    {
        public EventServiceMessage()
        {
            this.InitializeDefaultMessage("EventService");
        }

        public EventServiceMessage(params string[] queryStrings)
        {
            this.InitializeDefaultMessage("EventService", queryStrings);
        }
    }
}
