﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using InnowatchServiceType;

namespace InnowatchServiceLog.ConsoleMessage
{
    public class DataHandlerMessage
    {
        public static void PrintExceptionMessage(Exception ex)
        {
            if(ServiceTypeInfo.GetTypeInfo() != RunType.ConsoleService)
                return;

            try
            {
                var stackMerge = string.Empty;
                for (var i = 1; i < 6; i++)
                {
                    stackMerge = stackMerge.Insert(0, string.Format("\r\n{0}()", GetMethodInfoStrings.GetMethodName(i)));
                }

                var message = string.Format(
                        "[DataHandler][Thread ID : {0}] {1} : {2}",
                        Thread.CurrentThread.ManagedThreadId,
                        stackMerge.Insert(0, "Catch DataHandler Exception!"),
                        ex);
                Console.WriteLine(message);
                Logger.Write(Logger.Error, ex);
            }
            catch (Exception e)
            {
                Console.WriteLine("[DataHandler Inner Exception][Thread ID : {0}] {1} ", Thread.CurrentThread.ManagedThreadId, e);
                Logger.Write(Logger.Error, e);
            }
        }
    }
}
