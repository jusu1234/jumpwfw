﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InnowatchServiceType
{
    public class ServiceTypeInfo
    {
        //[ThreadStaticAttribute]
        private static RunType appType;

        static ServiceTypeInfo()
        {
            appType = RunType.None;
        }

        public static void SetTypeInfo(RunType type)
        {
            appType = type;
        }

        public static RunType GetTypeInfo()
        {
            return appType;
        }
    }

    public enum RunType
    {
        None,

        ConsoleService,

        WindowService
    }
}
