﻿
namespace InnowatchNetwork
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.ServiceModel;
    using System.ServiceModel.Description;
    using System.ServiceModel.Discovery;
    using System.Text;
    using System.Xml.Linq;

    public class NetworkHelper
    {
        /// <summary>
        /// 현재 컴퓨터 주소들을 XML 형식으로 반환 함.
        /// </summary>
        public static XElement GetLocalAddresses()
        {
            var root = new XElement("Addresses");

            IPAddress[] address = Dns.GetHostAddresses(Dns.GetHostName());

            foreach (var item in address)
            {
                root.Add(new XElement("Address", item.ToString()));
            }

            return root;
        }

        public static void AddDiscovery(ServiceHost host, Type serviceType)
        {
            // Add a ServiceDiscoveryBehavior                
            host.Description.Behaviors.Add(new ServiceDiscoveryBehavior());
                        
            // Add a UdpDiscoveryEndpoint
            var udpDiscoveryEndpoint = new UdpDiscoveryEndpoint();            
            host.AddServiceEndpoint(udpDiscoveryEndpoint);

            // Add a discovery behavior for Meta Extensions
            EndpointDiscoveryBehavior endpointDiscoveryBehavior = new EndpointDiscoveryBehavior();
            endpointDiscoveryBehavior.Extensions.Add(GetLocalAddresses());
            host.Description.Endpoints[0].Behaviors.Add(endpointDiscoveryBehavior);
        }
    }
}
