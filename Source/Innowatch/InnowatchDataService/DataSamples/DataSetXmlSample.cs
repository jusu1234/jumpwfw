﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InnowatchService.DataSamples
{
    public class DataSetXmlSample
    {
        public const string UrlCaptureInfo = @"
<NewDataSet>
  <xs:schema id=""NewDataSet"" xmlns="""" xmlns:xs=""http://www.w3.org/2001/XMLSchema"" xmlns:msdata=""urn:schemas-microsoft-com:xml-msdata"">
    <xs:element name=""NewDataSet"" msdata:IsDataSet=""true"" msdata:Locale="""">
      <xs:complexType>
        <xs:choice minOccurs=""0"" maxOccurs=""unbounded"">
          <xs:element name=""Table"">
            <xs:complexType>
              <xs:sequence>
                <xs:element name=""ViewServerID"" type=""xs:string"" minOccurs=""0"" />
                <xs:element name=""ViewServerIP"" type=""xs:string"" minOccurs=""0"" />
                <xs:element name=""ViewServerPort"" type=""xs:int"" minOccurs=""0"" />
                <xs:element name=""EventServiceUrl"" type=""xs:string"" minOccurs=""0"" />
                <xs:element name=""ViewServerType"" type=""xs:string"" minOccurs=""0"" />
                <xs:element name=""ViewServerDescription"" type=""xs:string"" minOccurs=""0"" />
                <xs:element name=""ActiveCode"" type=""xs:string"" minOccurs=""0"" />
                <xs:element name=""DefaultConfig"" type=""xs:string"" minOccurs=""0"" />
                <xs:element name=""Config"" type=""xs:string"" minOccurs=""0"" />
              </xs:sequence>
            </xs:complexType>
          </xs:element>
        </xs:choice>
      </xs:complexType>
    </xs:element>
  </xs:schema>
  <Table>
    <ViewServerID>CP1</ViewServerID>
    <ViewServerIP>172.16.10.232</ViewServerIP>
    <ViewServerType>URL CAPTURE</ViewServerType>
    <ViewServerDescription>Test Url Capture ServerInfo</ViewServerDescription>
  </Table>
  <Table>
    <ViewServerID>CP2</ViewServerID>
    <ViewServerIP>172.16.10.235</ViewServerIP>
    <ViewServerType>URL CAPTURE</ViewServerType>
    <ViewServerDescription>Test Url Capture ServerInfo</ViewServerDescription>
  </Table>
</NewDataSet>";

    }
}