﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IDataService.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Defines the IDataService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace InnowatchDataService
{
    using System.IO;
    using System.ServiceModel;
    using System.ServiceModel.Web;

    /// <summary>
    /// The DataService interface.
    /// </summary>
    [ServiceContract]
    public interface IDataService
    {
        [OperationContract]
        [WebGet(UriTemplate = "test")]
        Stream GetTest();

        #region Common Interface

        #region GET

        [OperationContract]
        [WebGet(UriTemplate = "ConnectionCheck")]
        Stream ConnectionCheck();

        [OperationContract]
        [WebGet(UriTemplate = "Getlicense?ip={ip}&type={type}")]
        Stream GetLicense(string ip, string type = null);

        #endregion // GET

        #region SET

        /// <summary>
        /// The set license.
        /// </summary>
        /// <param name="stream">
        /// The stream.
        /// </param>
        /// <param name="type">
        /// The type.
        /// </param>
        /// <returns>
        /// The <see cref="Stream"/>.
        /// </returns>
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "SetLicense?ip={ip}&type={type}")]
        Stream SetLicense(Stream stream, string ip, string type = null);

        #endregion // SET

        #endregion // Common Interface


        //------------------------------------------------------------------------------------------------------------------------//


        #region View Config Interface

        #region GET

        [OperationContract]
        [WebGet(UriTemplate = "GetViewCommonDefaultConfig?type={type}", RequestFormat = WebMessageFormat.Xml)]
        Stream GetViewCommonDefaultConfig(string type);

        [OperationContract]
        [WebGet(UriTemplate = "GetViewCommonConfig?type={type}", RequestFormat = WebMessageFormat.Xml)]
        Stream GetViewCommonConfig(string type);

        [OperationContract]
        [WebGet(UriTemplate = "GetViewDefaultConfig?ip={ip}", RequestFormat = WebMessageFormat.Xml)]
        Stream GetViewDefaultConfig(string ip);

        [OperationContract]
        [WebGet(UriTemplate = "GetViewConfig?ip={ip}", RequestFormat = WebMessageFormat.Xml)]
        Stream GetViewConfig(string ip);

        #endregion // GET

        #region POST

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "SetViewCommonDefaultConfig?type={type}")]
        void SetViewCommonDefaultConfig(Stream stream, string type);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "SetViewCommonConfig?type={type}")]
        void SetViewCommonConfig(Stream stream, string type);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "SetViewDefaultConfig?ip={ip}")]
        void SetViewDefaultConfig(Stream stream, string ip);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "SetViewConfig?ip={ip}")]
        void SetViewConfig(Stream stream, string ip);

        #endregion // POST

        #endregion // View Config Interface


        //------------------------------------------------------------------------------------------------------------------------//



        #region Command Interface

        #region GET

        [OperationContract]
        [WebGet(UriTemplate = "GetClientEventServiceUrl?clientid={clientId}", RequestFormat = WebMessageFormat.Xml)]
        Stream GetClientEventServiceUrl(string clientId);

        [OperationContract]
        [WebGet(UriTemplate = "GetVideoInformation?projectId={projectId}", RequestFormat = WebMessageFormat.Xml)]
        Stream GetVideoInformation(string projectId);

        // Configuration 에서도 같이 사용한다.
        [OperationContract]
        [WebGet(UriTemplate = "LoginForClient?userId={userId}&password={password}")]
        Stream GetLogInUserInfoForClient(string userId, string password);

        [OperationContract]
        [WebGet(UriTemplate = "LoginForCommand?userId={userId}&password={password}")]
        Stream GetLogInUserInfoForCommand(string userId, string password);

        [OperationContract]
        [WebGet(UriTemplate = "LoginForViewer?userId={userId}&password={password}")]
        Stream GetLogInUserInfoForViewer(string userId, string password);

        [OperationContract]
        [WebGet(UriTemplate = "LoginForPlayback?userId={userId}&password={password}")]
        Stream GetLogInUserInfoForPlayback(string userId, string password);

        [OperationContract]
        [WebGet(UriTemplate = "GetCameraGroupInfo?projectId={projectId}&userId={userId}&type={type}", RequestFormat = WebMessageFormat.Xml)]
        Stream GetCameraGroupInfoForClient(string projectId, string userId, string type);


        [OperationContract]
        [WebGet(UriTemplate = "GetCameraGroupInfoByGrouping?projectId={projectId}&group={group}", RequestFormat = WebMessageFormat.Xml)]
        Stream GetCameraGroupInfoForClientByGrouping(string projectId, string group);

        [OperationContract]
        [WebGet(UriTemplate = "GetFavoriteInfo?projectId={projectId}&userId={userId}&type={type}")]
        Stream GetFavoriteInfo(string projectId, string userId, string type);

        [OperationContract]
        [WebGet(UriTemplate = "GetLastFavoriteGroupId")]
        Stream GetLastFavoriteGroupId();

        [OperationContract]
        [WebGet(UriTemplate = "GetCameraInformation?ip={mdIp}")]
        Stream GetCameraInformation(string mdIp);

        [OperationContract]
        [WebGet(UriTemplate = "GetCameraControllerInfo?projectId={projectId}")]
        Stream GetCameraControllerInformation(string projectId);

        [OperationContract]
        [WebGet(UriTemplate = "GetMapLayout?projectId={projectId}")]
        Stream GetMapLayout(string projectId);

        [OperationContract]
        [WebGet(UriTemplate = "GetLocationBookmark?projectId={projectId}&userId={userId}&type={type}")]
        Stream GetLocationBookmark(string projectId, string userId, string type);

        /// <summary>
        /// The get favorite slide.
        /// </summary>
        /// <param name="projectId">
        /// The project id.
        /// </param>
        /// <param name="userId">
        /// The user id.
        /// </param>
        /// <param name="type">
        /// The type.
        /// </param>
        /// <returns>
        /// The <see cref="Stream"/>.
        /// </returns>
        [OperationContract]
        [WebGet(UriTemplate = "GetFavoriteSlide?projectId={projectId}&userId={userId}&type={type}")]
        Stream GetFavoriteSlide(string projectId, string userId, string type);

        [OperationContract]
        [WebGet(UriTemplate = "GetFavoriteSchedule?projectId={projectId}&userId={userId}&type={type}")]
        Stream GetFavoriteSchedule(string projectId, string userId, string type);

        #endregion // GET

        #region POST

        #region Favorite

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "AddFavoriteInfo?projectId={projectId}&userId={userId}&type={type}")]
        Stream AddFavoriteInfo(Stream stream, string projectId, string userId, string type);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "UpdateFavoriteInfo?projectId={projectId}&userId={userId}&type={type}")]
        void UpdateFavoriteInfo(Stream stream, string projectId, string userId, string type);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "UpdateFavoriteData?projectId={projectId}&userId={userId}&type={type}")]
        void UpdateFavorite(Stream stream, string projectId, string userId, string type);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "DeleteFavorite?projectId={projectId}&userId={userId}&type={type}")]
        void DeleteFavorite(Stream stream, string projectId, string userId, string type);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "DeleteFavoriteGroup?projectId={projectId}&userId={userId}&type={type}")]
        void DeleteFavoriteGroup(Stream stream, string projectId, string userId, string type);

        #endregion // Favorite

        #region FavoriteSlide

        /// <summary>
        /// The add favorite slide.
        /// </summary>
        /// <param name="stream">
        /// The stream.
        /// </param>
        /// <param name="projectId">
        /// The project id.
        /// </param>
        /// <param name="userId">
        /// The user id.
        /// </param>
        /// <param name="type">
        /// The type.
        /// </param>
        /// <returns>
        /// The <see cref="Stream"/>.
        /// </returns>
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "AddFavoriteSlide?projectId={projectId}&userId={userId}&type={type}")]
        Stream AddFavoriteSlide(Stream stream, string projectId, string userId, string type);

        /// <summary>
        /// The update favorite slide.
        /// </summary>
        /// <param name="stream">
        /// The stream.
        /// </param>
        /// <param name="projectId">
        /// The project id.
        /// </param>
        /// <param name="userId">
        /// The user id.
        /// </param>
        /// <param name="type">
        /// The type.
        /// </param>
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "UpdateFavoriteSlide?projectId={projectId}&userId={userId}&type={type}")]
        Stream UpdateFavoriteSlide(Stream stream, string projectId, string userId, string type);

        /// <summary>
        /// The delete favorite slide.
        /// </summary>
        /// <param name="projectId">
        /// The project id.
        /// </param>
        /// <param name="userId">
        /// The user id.
        /// </param>
        /// <param name="type">
        /// The type.
        /// </param>
        /// <param name="name">
        /// The name.
        /// </param>
        [OperationContract]
        [WebGet(UriTemplate = "DeleteFavoriteSlide?projectId={projectId}&userId={userId}&type={type}&name={name}")]
        Stream DeleteFavoriteSlide(string projectId, string userId, string type, string name);

        #endregion // FavoriteSlide

        #region FavoriteSchedule

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "SetFavoriteSchedule?projectId={projectId}&userId={userId}&type={type}")]
        Stream SetFavoriteSchedule(Stream stream, string projectId, string userId, string type);

        #endregion //FavoriteSchedule

        #region CameraGroup

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "AddCameraGroupInfo?projectId={projectId}&userId={userId}&type={type}")]
        Stream AddCameraGroupInfo(Stream stream, string projectId, string userId, string type);

        #endregion // CameraGroup

        #region MapLayout

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "SetMapLayout?projectId={projectId}&userId={userId}&type={type}")]
        Stream SetMapLayout(Stream stream, string projectId, string userId, string type);

        #endregion // MapLayout

        #region

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "SetLocationBookmark?projectId={projectId}&userId={userId}&type={type}")]
        Stream SetLocationBookmark(Stream stream, string projectId, string userId, string type);

        #endregion // LocationBookmark

        #endregion // POST

        #endregion // Command Interface


        //------------------------------------------------------------------------------------------------------------------------//


        #region Playback Interface

        #region GET

        /// <summary>
        /// The get video information for playback.
        /// </summary>
        /// <param name="projectId">
        /// The project id.
        /// </param>
        /// <returns>
        /// The response data.
        /// </returns>
        [OperationContract]
        [WebGet(UriTemplate = "GetVideoInformationForPlayback?projectId={projectId}", RequestFormat = WebMessageFormat.Xml)]
        Stream GetVideoInformationForPlayback(string projectId);

        /// <summary>
        /// The get timeline for playback.
        /// </summary>
        /// <param name="stream">
        /// The stream.
        /// </param>
        /// <returns>
        /// The response data.
        /// </returns>
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "GetTimelineForPlayback")]
        Stream GetTimelineForPlayback(Stream stream);

        /// <summary>
        /// The get search record time for playback.
        /// </summary>
        /// <param name="stream">
        /// The stream.
        /// </param>
        /// <returns>
        /// The response data.
        /// </returns>
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "GetSearchRecordTimeForPlayback")]
        Stream GetSearchRecordTimeForPlayback(Stream stream);

        /// <summary>
        /// The get recording date info.
        /// </summary>
        /// <param name="stream">
        /// The stream.
        /// </param>
        /// <returns>
        /// The <see cref="Stream"/>.
        /// </returns>
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "GetRecordingDateInfo")]
        Stream GetRecordingDateInfo(Stream stream);

        #endregion // GET

        #region POST



        #endregion //POST

        #endregion // Playback Interface


        //------------------------------------------------------------------------------------------------------------------------//


        #region Configuration Interface

        #region GET

        #region Get of Cameras

        [OperationContract]
        [WebGet(UriTemplate = "Login?userId={userId}&password={password}")]
        Stream GetLogInUserInfo(string userId, string password);

        [OperationContract]
        [WebGet(UriTemplate = "GetUserInfo?privilege={privilege}")]
        Stream GetUserInfoByPrivilege(int privilege);

        [OperationContract]
        [WebGet(UriTemplate = "GetUserPrivilegeInfo?privilege={privilege}")]
        Stream GetUserPrivilegeInfoByPrivilege(int privilege);

        [OperationContract]
        [WebGet(UriTemplate = "GetProjectInfo")]
        Stream GetProjectInfo();

        [OperationContract]
        [WebGet(UriTemplate = "GetMediaServerInfo")]
        Stream GetMediaServerInfo();

        [OperationContract]
        [WebGet(UriTemplate = "GetRecorderInfo")]
        Stream GetRecorderInfo();

        [OperationContract]
        [WebGet(UriTemplate = "GetViewServerInfo")]
        Stream GetViewServerInfo();

        [OperationContract]
        [WebGet(UriTemplate = "GetDisplayInfo")]
        Stream GetDisplayInfo();

        [OperationContract]
        [WebGet(UriTemplate = "GetCameraControllerServiceInfo")]
        Stream GetCameraControllerServiceInfo();

        [OperationContract]
        [WebGet(UriTemplate = "GetExternalCommandServiceInfo")]
        Stream GetExternalCommandServiceInfo();

        [OperationContract]
        [WebGet(UriTemplate = "GetVideoSpecInfo")]
        Stream GetVideoSpecInfo();

        [OperationContract]
        [WebGet(UriTemplate = "GetPtzConnectionInfo")]
        Stream GetPtzConnectionInfo();

        [OperationContract]
        [WebGet(UriTemplate = "GetCameraControlSettingForConfiguration")]
        Stream GetCameraControlSettingForConfiguration();

        [OperationContract]
        [WebGet(UriTemplate = "GetCameraInfo?projectId={projectId}")]
        Stream GetCameraInfo(string projectId);

        [OperationContract]
        [WebGet(UriTemplate = "GetCameraVideoMappingByCameraIdInfo?cameraId={cameraId}")]
        Stream GetCameraVideoMappingByCameraIdInfo(string cameraId);

        [OperationContract]
        [WebGet(UriTemplate = "GetAllCameraVideoSpecMapping")]
        Stream GetAllCameraVideoSpecMapping();

        [OperationContract]
        [WebGet(UriTemplate = "GetCameraGroupForConfiguration?projectId={projectId}&userId={userId}")]
        Stream GetCameraGroupForConfiguration(string projectId, string userId);

        [OperationContract]
        [WebGet(UriTemplate = "GetCameraGroupMappingForConfiguration?projectId={projectId}&userId={userId}")]
        Stream GetCameraGroupMappingForConfiguration(string projectId, string userId);

        [OperationContract]
        [WebGet(UriTemplate = "GetCameraIdForConfiguration?projectId={projectId}")]
        Stream GetCameraIdForConfiguration(string projectId);

        [OperationContract]
        [WebGet(UriTemplate = "GetCameraGroupIdForConfiguration?projectId={projectId}&userId={userId}")]
        Stream GetCameraGroupIdForConfiguration(string projectId, string userId);

        [OperationContract]
        [WebGet(UriTemplate = "GetFavoriteInfoForConfiguration?type={type}")]
        Stream GetFavoriteInfoForConfiguration(string type);

        [OperationContract]
        [WebGet(UriTemplate = "GetFavoriteGroupInfoForConfiguration?projectId={projectId}&userId={userId}&type={type}")]
        Stream GetFavoriteGroupInfoForConfiguration(string projectId, string userId, string type);

        [OperationContract]
        [WebGet(UriTemplate = "GetFavoriteGroupMappingInfoForConfiguration?projectId={projectId}&userId={userId}&type={type}")]
        Stream GetFavoriteGroupMappingInfoForConfiguration(string projectId, string userId, string type);

        [OperationContract]
        [WebGet(UriTemplate = "GetFavoriteIdForConfiguration?type={type}")]
        Stream GetFavoriteIdForConfiguration(string type);

        [OperationContract]
        [WebGet(UriTemplate = "GetFavoriteGroupIdForConfiguration?projectId={projectId}&userId={userId}&type={type}")]
        Stream GetFavoriteGroupIdForConfiguration(string projectId, string userId, string type);

        [OperationContract]
        [WebGet(UriTemplate = "GetMediaServerIdInfo")]
        Stream GetMediaServerIdInfo();

        /// <summary>
        /// The get multi camera controller id info.
        /// </summary>
        /// <returns>
        /// The response data.
        /// </returns>
        [OperationContract]
        [WebGet(UriTemplate = "GetMultiCameraControllerIdInfo")]
        Stream GetMultiCameraControllerIdInfo();

        [OperationContract]
        [WebGet(UriTemplate = "GetCameraControlSettingIdInfo")]
        Stream GetCameraControlSettingIdInfo();

        [OperationContract]
        [WebGet(UriTemplate = "GetPtzConnectionInfoIdInfo")]
        Stream GetPtzConnectionInfoIdInfo();

        [OperationContract]
        [WebGet(UriTemplate = "GetVendorListInfo")]
        Stream GetVendorListInfo();

        [OperationContract]
        [WebGet(UriTemplate = "GetAllUserInfo")]
        Stream GetAllUserInfo();

        [OperationContract]
        [WebGet(UriTemplate = "GetAllUserPrivilegeInfo")]
        Stream GetAllUserPrivilegeInfo();

        [OperationContract]
        [WebGet(UriTemplate = "GetAllCameraGroupInfoForConfiguration")]
        Stream GetAllCameraGroupInfoForConfiguration();

        [OperationContract]
        [WebGet(UriTemplate = "GetAllCameraGroupMappingInfoForConfiguration")]
        Stream GetAllCameraGroupMappingInfoForConfiguration();

        [OperationContract]
        [WebGet(UriTemplate = "GetAllfavoriteGroupInfoForConfiguration")]
        Stream GetAllfavoriteGroupInfoForConfiguration();

        [OperationContract]
        [WebGet(UriTemplate = "GetAllFavoriteGroupMappingInfoForConfiguration")]
        Stream GetAllFavoriteGroupMappingInfoForConfiguration();

        [OperationContract]
        [WebGet(UriTemplate = "GetAllCameraInfoForConfiguration")]
        Stream GetAllCameraInfoForConfiguration();

        [OperationContract]
        [WebGet(UriTemplate = "GetAllProjectMappingInfo")]
        Stream GetAllProjectMappingInfo();

        [OperationContract]
        [WebGet(UriTemplate = "GetAllCameraVideoMappingInfo")]
        Stream GetAllCameraVideoMappingInfo();

        [OperationContract]
        [WebGet(UriTemplate = "GetRecordCameraNumber")]
        Stream GetRecordCameraNumber();

        #endregion // Get of Cameras

        #region Get of Nvr

        /// <summary>
        /// The get storage info.
        /// </summary>
        /// <returns>
        /// The stream of storage data set.
        /// </returns>
        [OperationContract]
        [WebGet(UriTemplate = "GetStorageInfo")]
        Stream GetStorageInfo();

        /// <summary>
        /// The get record schedule info.
        /// </summary>
        /// <returns>
        /// The stream of record schedule data set.
        /// </returns>
        [OperationContract]
        [WebGet(UriTemplate = "GetRecordScheduleInfo")]
        Stream GetRecordScheduleInfo();

        /// <summary>
        /// The get archive schedule info.
        /// </summary>
        /// <returns>
        /// The steram of archive schedule data set.
        /// </returns>
        [OperationContract]
        [WebGet(UriTemplate = "GetArchiveScheduleInfo")]
        Stream GetArchiveScheduleInfo();

        /// <summary>
        /// The get record camera info.
        /// </summary>
        /// <returns>
        /// The stream of record camera data set.
        /// </returns>
        [OperationContract]
        [WebGet(UriTemplate = "GetRecordCameraInfo")]
        Stream GetRecordCameraInfo();

        /// <summary>
        /// The get storage list for record camera.
        /// </summary>
        /// <returns>
        /// The stream of storage list for record camera data set.
        /// </returns>
        [OperationContract]
        [WebGet(UriTemplate = "GetStorageListForRecordCamera")]
        Stream GetStorageListForRecordCamera();

        /// <summary>
        /// The get record schedule list for record camera.
        /// </summary>
        /// <returns>
        /// The stream of record schedule list for record camera data set.
        /// </returns>
        [OperationContract]
        [WebGet(UriTemplate = "GetRecordScheduleListForRecordCamera")]
        Stream GetRecordScheduleListForRecordCamera();

        /// <summary>
        /// The get archive schedule list for record camera.
        /// </summary>
        /// <returns>
        /// The stream of archive schedule list for record camera data set.
        /// </returns>
        [OperationContract]
        [WebGet(UriTemplate = "GetArchiveScheduleListForRecordCamera")]
        Stream GetArchiveScheduleListForRecordCamera();

        /// <summary>
        /// The get recorder list for record camera.
        /// </summary>
        /// <returns>
        /// The stream of recorder list for record camera data set.
        /// </returns>
        [OperationContract]
        [WebGet(UriTemplate = "GetRecorderListForRecordCamera")]
        Stream GetRecorderListForRecordCamera();

        /// <summary>
        /// The get camera to record info.
        /// </summary>
        /// <returns>
        /// The <see cref="Stream"/>.
        /// </returns>
        [OperationContract]
        [WebGet(UriTemplate = "GetCameraToRecordInfo")]
        Stream GetCameraToRecordInfo();
                
        #endregion // Get of Nvr

        #region GetData of Record

        /// <summary>
        /// The get video information process.
        /// </summary>
        /// <param name="recorderId">
        /// The recorder id.
        /// </param>
        /// <param name="recordcameranumber">
        /// The record camera number. (if -1 all data return)
        /// </param>
        /// <returns>
        /// The <see cref="Stream"/>.
        /// </returns>
        [OperationContract]
        [WebGet(UriTemplate = "getvideoinformationprocess?recorderid={recorderid}&recordcameranumber={recordcameranumber}")]
        Stream GetVideoInformationProcess(string recorderId, string recordcameranumber);

        /// <summary>
        /// The get recorder process.
        /// </summary>
        /// <returns>
        /// The <see cref="Stream"/>.
        /// </returns>
        [OperationContract]
        [WebGet(UriTemplate = "getrecorderprocess?recorderip={recorderip}")]
        Stream GetRecorderProcess(string recorderIp);
        
        /// <summary>
        /// The get storage process.
        /// </summary>
        /// <returns>
        /// The <see cref="Stream"/>.
        /// </returns>
        [OperationContract]
        [WebGet(UriTemplate = "getstorageprocess")]
        Stream GetStorageProcess();

        /// <summary>
        /// The get record schedule data process.
        /// </summary>
        /// <returns>
        /// The <see cref="Stream"/>.
        /// </returns>
        [OperationContract]
        [WebGet(UriTemplate = "getrecordscheduledataprocess?recordschedulenumber={recordschedulenumber}")]
        Stream GetRecordScheduleDataProcess(string recordScheduleNumber);

        #endregion // GetData of Record

        #endregion // GET

        #region POST

        #region Post of Cameras

        #region User

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "AddUserInfo")]
        Stream AddUserInfo(Stream stream);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "ImportUserInfo")]
        Stream ImportUserInfo(Stream stream);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "AddUserPrivilegeInfo")]
        Stream AddUserPrivilegeInfo(Stream stream);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "ImportUserPrivilegeInfo")]
        Stream ImportUserPrivilegeInfo(Stream stream);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "UpdateUserInfo")]
        Stream UpdateUserInfo(Stream stream);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "UpdateUserPrivilegeInfo")]
        Stream UpdateUserPrivilegeInfo(Stream stream);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "DeleteUserInfo")]
        Stream DeleteUserInfo(Stream stream);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "DeleteUserPrivilegeInfo")]
        Stream DeleteUserPrivilegeInfo(Stream stream);

        #endregion // User

        #region Project

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "AddProjectInfo")]
        Stream AddProjectInfo(Stream stream);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "ImportProjectInfo")]
        Stream ImportProjectInfo(Stream stream);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "UpdateProjectInfo")]
        Stream UpdateProjectInfo(Stream stream);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "DeleteProjectInfo")]
        Stream DeleteProjectInfo(Stream stream);

        #endregion // Project

        #region MediaServer

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "AddMediaServerInfo")]
        Stream AddMediaServerInfo(Stream stream);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "ImportMediaServerInfo")]
        Stream ImportMediaServerInfo(Stream stream);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "UpdateMediaServerInfo")]
        Stream UpdateMediaServerInfo(Stream stream);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "DeleteMediaServerInfo")]
        Stream DeleteMediaServerInfo(Stream stream);

        #endregion // MediaServer

        #region Recorder

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "AddRecorderInfo")]
        Stream AddRecorderInfo(Stream stream);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "ImportRecorderInfo")]
        Stream ImportRecorderInfo(Stream stream);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "UpdateRecorderInfo")]
        Stream UpdateRecorderInfo(Stream stream);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "DeleteRecorderInfo")]
        Stream DeleteRecorderInfo(Stream stream);

        #endregion // Recorder

        #region ViewServer

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "AddViewServerInfo")]
        Stream AddViewServerInfo(Stream stream);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "ImportViewServerInfo")]
        Stream ImportViewServerInfo(Stream stream);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "UpdateViewServerInfo")]
        Stream UpdateViewServerInfo(Stream stream);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "DeleteViewServerInfo")]
        Stream DeleteViewServerInfo(Stream stream);

        #endregion // ViewServer

        #region Display

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "AddDisplayInfo")]
        Stream AddDisplayInfo(Stream stream);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "ImportDisplayInfo")]
        Stream ImportDisplayInfo(Stream stream);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "UpdateDisplayInfo")]
        Stream UpdateDisplayInfo(Stream stream);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "DeleteDisplayInfo")]
        Stream DeleteDisplayInfo(Stream stream);

        #endregion // Display

        #region CameraControllerService

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "AddCameraControllerServiceInfo")]
        Stream AddCameraControllerServiceInfo(Stream stream);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "ImportCameraControllerServiceInfo")]
        Stream ImportCameraControllerServiceInfo(Stream stream);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "UpdateCameraControllerServiceInfo")]
        Stream UpdateCameraControllerServiceInfo(Stream stream);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "DeleteCameraControllerServiceInfo")]
        Stream DeleteCameraControllerServiceInfo(Stream stream);

        #endregion // CameraControllerService

        #region ExternalCommandService

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "AddExternalCommandServiceInfo")]
        Stream AddExternalCommandServiceInfo(Stream stream);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "ImportExternalCommandServiceInfo")]
        Stream ImportExternalCommandServiceInfo(Stream stream);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "UpdateExternalCommandServiceInfo")]
        Stream UpdateExternalCommandServiceInfo(Stream stream);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "DeleteExternalCommandServiceInfo")]
        Stream DeleteExternalCommandServiceInfo(Stream stream);

        #endregion // ExternalCommandService

        #region VideoSpec

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "AddVideoSpecInfo")]
        Stream AddVideoSpecInfo(Stream stream);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "ImportVideoSpecInfo")]
        Stream ImportVideoSpecInfo(Stream stream);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "UpdateVideoSpecInfo")]
        Stream UpdateVideoSpecInfo(Stream stream);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "DeleteVideoSpecInfo")]
        Stream DeleteVideoSpecInfo(Stream stream);

        #endregion // VideoSpec

        #region PTZ Connection

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "AddPtzConnectionInfo")]
        Stream AddPtzConnectionInfo(Stream stream);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "ImportPtzConnectionInfo")]
        Stream ImportPtzConnectionInfo(Stream stream);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "UpdatePtzConnectionInfo")]
        Stream UpdatePtzConnectionInfo(Stream stream);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "DeletePtzConnectionInfo")]
        Stream DeletePtzConnectionInfo(Stream stream);

        #endregion // PTZ Connection

        #region CameraControlSetting

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "AddCameraControlSettingForConfiguration")]
        Stream AddCameraControlSettingForConfiguration(Stream stream);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "ImportCameraControlSettingInfo")]
        Stream ImportCameraControlSettingInfo(Stream stream);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "UpdateCameraControlSettingForConfiguration")]
        Stream UpdateCameraControlSettingForConfiguration(Stream stream);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "DeleteCameraControlSettingForConfiguration")]
        Stream DeleteCameraControlSettingForConfiguration(Stream stream);

        #endregion // CameraControlSetting

        #region Camera

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "AddCameraInfo")]
        Stream AddCameraInfo(Stream stream);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "UpdateCameraInfo")]
        Stream UpdateCameraInfo(Stream stream);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "DeleteCameraInfo")]
        Stream DeleteCameraInfo(Stream stream);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "DeleteCameraTable")]
        Stream DeleteCameraTable();

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "DeleteCameraVideoMappingTable")]
        Stream DeleteCameraVideoMappingTable();

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "DeleteProjectMappingTable")]
        Stream DeleteProjectMappingTable();

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "ImportCameraInfo")]
        Stream ImportCameraInfo(Stream stream);

        // TODO : CSV 사용 하지 않는 것으로 결정될 경우 아래 3개 메서드(AddCameraTable, AddCameraVideoMappingTable, AddProjectMappingTable) 삭제할 것 - by shwlee.

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "AddCameraTable")]
        Stream AddCameraTable(Stream stream);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "AddCameraVideoMappingTable")]
        Stream AddCameraVideoMappingTable(Stream stream);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "AddProjectMappingTable")]
        Stream AddProjectMappingTable(Stream stream);

        #endregion Camera

        #region CameraGroup

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "AddCameraGroupForConfiguration")]
        void AddCameraGroupForConfiguration(Stream stream);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "UpdateCameraGroupForConfiguration")]
        void UpdateCameraGroupForConfiguration(Stream stream);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "DeleteCameraGroupForConfiguration")]
        void DeleteCameraGroupForConfiguration(Stream stream);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "DeleteCameraGroupTable")]
        void DeleteCameraGroupTable();

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "AddCameraGroupTable")]
        void AddCameraGroupTable(Stream stream);

        #endregion // CameraGroup

        #region CameraGroupMapping

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "AddCameraGroupMappingForConfiguration")]
        void AddCameraGroupMappingForConfiguration(Stream stream);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "UpdateCameraGroupMappingForConfiguration")]
        void UpdateCameraGroupMappingForConfiguration(Stream stream);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "DeleteCameraGroupMappingForConfiguration")]
        void DeleteCameraGroupMappingForConfiguration(Stream stream);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "DeleteCameraGroupMappingTable")]
        void DeleteCameraGroupMappingTable();

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "AddCameraGroupMappingTable")]
        void AddCameraGroupMappingTable(Stream stream);

        #endregion // CameraGroupMapping

        #region Favorite

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "AddFavoriteInfoForConfiguration")]
        void AddFavoriteInfoForConfiguration(Stream stream);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "UpdateFavoriteInfoForConfiguration")]
        void UpdateFavoriteInfoForConfiguration(Stream stream);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "DeleteFavoriteInfoForConfiguration")]
        void DeleteFavoriteInfoForConfiguration(Stream stream);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "DeleteFavoriteTable")]
        void DeleteFavoriteTable();

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "AddFavoriteTable")]
        void AddFavoriteTable(Stream stream);

        #endregion // Favorite

        #region FavoriteGroup

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "AddFavoriteGroupForConfiguration?projectId={projectId}&userId={userId}")]
        void AddFavoriteGroupForConfiguration(Stream stream, string projectId, string userId);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "UpdateFavoriteGroupForConfiguration?projectId={projectId}&userId={userId}")]
        void UpdateFavoriteGroupForConfiguration(Stream stream, string projectId, string userId);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "DeleteFavoriteGroupForConfiguration?projectId={projectId}&userId={userId}")]
        void DeleteFavoriteGroupForConfiguration(Stream stream, string projectId, string userId);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "DeleteFavoriteGroupTable")]
        void DeleteFavoriteGroupTable();

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "AddFavoriteGroupTable")]
        void AddFavoriteGroupTable(Stream stream);

        #endregion // FavoriteGroup

        #region FavoriteGroupMapping

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "AddFavoriteGroupMappingForConfiguration")]
        void AddFavoriteGroupMappingForConfiguration(Stream stream);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "UpdateFavoriteGroupMappingForConfiguration")]
        void UpdateFavoriteGroupMappingForConfiguration(Stream stream);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "DeleteFavoriteGroupMappingForConfiguration")]
        void DeleteFavoriteGroupMappingForConfiguration(Stream stream);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "DeleteFavoriteGroupMappingTable")]
        void DeleteFavoriteGroupMappingTable();

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "SaveFavoriteManager?projectId={projectId}&userId={userId}&type={type}")]
        void SaveFavoriteManager(Stream stream, string projectId, string userId, string type);

        #endregion // FavoriteGroupMapping

        #endregion

        #region Post of Nvr

        #region Storage

        /// <summary>
        /// The add storage info.
        /// </summary>
        /// <param name="stream">
        /// The stream.
        /// </param>
        /// <returns>
        /// The response data.
        /// </returns>
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "AddStorageInfo")]
        Stream AddStorageInfo(Stream stream);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "ImportStorageInfo")]
        Stream ImportStorageInfo(Stream stream);

        /// <summary>
        /// The update storage info.
        /// </summary>
        /// <param name="stream">
        /// The stream.
        /// </param>
        /// <returns>
        /// The response data.
        /// </returns>
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "UpdateStorageInfo")]
        Stream UpdateStorageInfo(Stream stream);

        /// <summary>
        /// The delete storage info.
        /// </summary>
        /// <param name="stream">
        /// The stream.
        /// </param>
        /// <returns>
        /// The response data.
        /// </returns>
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "DeleteStorageInfo")]
        Stream DeleteStorageInfo(Stream stream);

        /// <summary>
        /// The set storage update process.
        /// </summary>
        /// <param name="storageCapacity">
        /// The storage Capacity.
        /// </param> 
        /// <param name="storageRemain">
        /// The storage Remain.
        /// </param>
        /// <param name="storageNumber">
        /// The storage Number.
        /// </param>
        /// <returns>
        /// The <see cref="Stream"/>.
        /// </returns>
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "setstorageupdateprocess?storagecapacity={storagecapacity}&storageremain={storageremain}&storagenumber={storagenumber}")]
        Stream SetStorageUpdateProcess(string storageCapacity, string storageRemain, string storageNumber);

        #endregion // Storage

        #region Record Schedule

        /// <summary>
        /// The add record schedule info.
        /// </summary>
        /// <param name="stream">
        /// The stream.
        /// </param>
        /// <returns>
        /// The response data.
        /// </returns>
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "AddRecordScheduleInfo")]
        Stream AddRecordScheduleInfo(Stream stream);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "ImportRecordScheduleInfo")]
        Stream ImportRecordScheduleInfo(Stream stream);

        /// <summary>
        /// The update record schedule info.
        /// </summary>
        /// <param name="stream">
        /// The stream.
        /// </param>
        /// <returns>
        /// The response data.
        /// </returns>
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "UpdateRecordScheduleInfo")]
        Stream UpdateRecordScheduleInfo(Stream stream);

        /// <summary>
        /// The delete record schedule info.
        /// </summary>
        /// <param name="stream">
        /// The stream.
        /// </param>
        /// <returns>
        /// The response data.
        /// </returns>
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "DeleteRecordScheduleInfo")]
        Stream DeleteRecordScheduleInfo(Stream stream);

        #endregion // Record Schedule

        #region Archive Schedule

        /// <summary>
        /// The add archive schedule info.
        /// </summary>
        /// <param name="stream">
        /// The stream.
        /// </param>
        /// <returns>
        /// The response data.
        /// </returns>
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "AddArchiveScheduleInfo")]
        Stream AddArchiveScheduleInfo(Stream stream);

        /// <summary>
        /// The update archive schedule info.
        /// </summary>
        /// <param name="stream">
        /// The stream.
        /// </param>
        /// <returns>
        /// The response data.
        /// </returns>
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "UpdateArchiveScheduleInfo")]
        Stream UpdateArchiveScheduleInfo(Stream stream);

        /// <summary>
        /// The import archive schedule info.
        /// </summary>
        /// <param name="stream">
        /// The stream.
        /// </param>
        /// <returns>
        /// The response data.
        /// </returns>
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "ImportArchiveScheduleInfo")]
        Stream ImportArchiveScheduleInfo(Stream stream);

        /// <summary>
        /// The delete archive schedule info.
        /// </summary>
        /// <param name="stream">
        /// The stream.
        /// </param>
        /// <returns>
        /// The response data.
        /// </returns>
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "DeleteArchiveScheduleInfo")]
        Stream DeleteArchiveScheduleInfo(Stream stream);

        #endregion // Archive Schedule

        #region Record Camera

        /// <summary>
        /// The add record camera info.
        /// </summary>
        /// <param name="stream">
        /// The stream.
        /// </param>
        /// <returns>
        /// The response data.
        /// </returns>
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "AddRecordCameraInfo")]
        Stream AddRecordCameraInfo(Stream stream);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "ImportRecordCameraInfo")]
        Stream ImportRecordCameraInfo(Stream stream);

        /// <summary>
        /// The update record camera info.
        /// </summary>
        /// <param name="stream">
        /// The stream.
        /// </param>
        /// <returns>
        /// The response data.
        /// </returns>
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "UpdateRecordCameraInfo")]
        Stream UpdateRecordCameraInfo(Stream stream);

        /// <summary>
        /// The delete record camera info.
        /// </summary>
        /// <param name="stream">
        /// The stream.
        /// </param>
        /// <returns>
        /// The response data.
        /// </returns>
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "DeleteRecordCameraInfo")]
        Stream DeleteRecordCameraInfo(Stream stream);

        #endregion // Record Camera

        #region Configuration

        /// <summary>
        /// The add configuration info.
        /// </summary>
        /// <param name="stream">
        /// The stream.
        /// </param>
        /// <returns>
        /// The response data.
        /// </returns>
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "AddConfigurationInfo")]
        Stream AddConfigurationInfo(Stream stream);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "ImportConfigurationInfo")]
        Stream ImportConfigurationInfo(Stream stream);

        /// <summary>
        /// The update configuration info.
        /// </summary>
        /// <param name="stream">
        /// The stream.
        /// </param>
        /// <returns>
        /// The response data.
        /// </returns>
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "UpdateConfigurationInfo")]
        Stream UpdateConfigurationInfo(Stream stream);

        /// <summary>
        /// The delete configuration info.
        /// </summary>
        /// <param name="stream">
        /// The stream.
        /// </param>
        /// <returns>
        /// The response data.
        /// </returns>
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "DeleteConfigurationInfo")]
        Stream DeleteConfigurationInfo(Stream stream);

        #endregion // Configuration

        #region Delete Record Media Index

        /// <summary>
        /// The request delete to record media index.
        /// </summary>
        /// <param name="stream">
        /// The stream.
        /// </param>
        /// <param name="ip">
        /// The ip.
        /// </param>
        /// <param name="port">
        /// The port.
        /// </param>
        /// <returns>
        /// The <see cref="Stream"/>.
        /// </returns>
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "RequestDeleteToRecordMediaIndex?ip={ip}&port={port}")]
        Stream RequestDeleteToRecordMediaIndex(Stream stream, string ip, string port);

        #endregion // Delete Record Media Index

        #region Changed Notify

        /// <summary>
        /// The request record type changed.
        /// </summary>
        /// <param name="recordCameraNumber">
        /// The record camera number.
        /// </param>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <param name="recorderId">
        /// The recorder Id.
        /// </param>
        /// <returns>
        /// The <see cref="Stream"/>.
        /// </returns>
        [OperationContract]
        [WebGet(UriTemplate = "RequestRecordTypeChanged?recordCameraNumber={recordCameraNumber}&field=RecordType&value={value}&recorderId={recorderId}")]
        Stream RequestRecordTypeChanged(string recordCameraNumber, string value, string recorderId);
        
        #endregion // Changed Notify

        #region Delete Reserved

        /// <summary>
        /// The reserve recording index delete.
        /// </summary>
        /// <param name="recordCameraNumber">
        /// The record Camera Number.
        /// </param>
        /// <returns>
        /// The <see cref="Stream"/>.
        /// </returns>
        [OperationContract]
        [WebGet(UriTemplate = "ReserveRecordingIndexDelete?recordCameraNumber={recordCameraNumber}")]
        Stream ReserveRecordingIndexDelete(string recordCameraNumber);

        /// <summary>
        /// The reserve cancel recording index delete.
        /// </summary>
        /// <param name="recordCameraNumber">
        /// The record camera number.
        /// </param>
        /// <returns>
        /// The <see cref="Stream"/>.
        /// </returns>
        [OperationContract]
        [WebGet(UriTemplate = "ReserveCancelRecordingIndexDelete?recordCameraNumber={recordCameraNumber}")]
        Stream ReserveCancelRecordingIndexDelete(string recordCameraNumber);

        /// <summary>
        /// The reserve recording index delete by record camera.
        /// </summary>
        /// <param name="recordCameraNumber">
        /// The record camera number.
        /// </param>
        /// <param name="recorderId">
        /// The recorder Id.
        /// </param>
        /// <returns>
        /// The <see cref="Stream"/>.
        /// </returns>
        [OperationContract]
        [WebGet(UriTemplate = "ReserveRecordingIndexDeleteByRecordCamera?recordCameraNumber={recordCameraNumber}&recorderId={recorderId}")]
        Stream ReserveRecordingIndexDeleteByRecordCamera(string recordCameraNumber, string recorderId);

        /// <summary>
        /// The reserve cancel recording index delete by record camera.
        /// </summary>
        /// <param name="recordCameraNumber">
        /// The record camera number.
        /// </param>
        /// <param name="recorderId">
        /// The recorder Id.
        /// </param>
        /// <returns>
        /// The <see cref="Stream"/>.
        /// </returns>
        [OperationContract]
        [WebGet(UriTemplate = "ReserveCancelRecordingIndexDeleteByRecordCamera?recordCameraNumber={recordCameraNumber}&recorderId={recorderId}")]
        Stream ReserveCancelRecordingIndexDeleteByRecordCamera(string recordCameraNumber, string recorderId);

        #endregion // Delete Reserved

        #endregion // Post of Nvr

        #endregion // POST

        #endregion // Configuration Interface

        #region URLCapture

        [OperationContract]
        [WebGet(UriTemplate = "GetUrlCaptureServerInfo")]
        Stream GetUrlCaptureServerInfo();

        [OperationContract]
        [WebGet(UriTemplate = "GetPingInfo?host={host}")]
        Stream GetPingInfo(string host);

        #endregion

        #region Map
        //[OperationContract]
        //[WebInvoke(Method = "POST", UriTemplate = "SetDataKmlPlaceMaker?projectId={projectId}&userId={userId}&type={type}")]
        //string SetDataKmlPlaceMaker(Stream stream, string projectId, string userId, string type);
        #endregion
        //------------------------------------------------------------------------------------------------------------------------//

        #region RPN

        #region GET

        #region Common

        [OperationContract]
        [WebGet(UriTemplate = "GetCommonCode?code={code}")]
        Stream GetCommonCode(string code);

        #endregion //GetCommonCode


        #region GetServerStatus

        [OperationContract]
        [WebGet(UriTemplate = "GetServerStatus")]
        Stream GetServerStatus();

        #endregion //GetServerStatus

        #region GetSystemStatusHistory

        [OperationContract]
        [WebGet(UriTemplate = "GetServerStatusHistory?StartTime={StartTime}&EndTime={EndTime}&SystemId={SystemID}")]
        Stream GetServerStatusHistory(string StartTime, string EndTime, string SystemId);

        #endregion //GetSystemStatusHistory

        #region GetCameraStatus

        [OperationContract]
        [WebGet(UriTemplate = "GetCameraStatus")]
        Stream GetCameraStatus();

        #endregion //GetCameraStatus


        #region GetAlarmRuleList

        [OperationContract]
        [WebGet(UriTemplate = "GetAlarmRuleList")]
        Stream GetAlarmRuleList();

        #endregion //GetCameraStatus


        #region GetAlarmRuleMapping

        [OperationContract]
        [WebGet(UriTemplate = "GetAlarmRuleMapping")]
        Stream GetAlarmRuleMapping();

        #endregion //GetAlarmRuleMapping


        #endregion //GET



        #region POST

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "AddAlarmRule")]
        Stream AddAlarmRule(Stream stream);

        
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "DeleteAlarmRule")]
        Stream DeleteAlarmRule(Stream stream);
        

        #endregion //POST

        #endregion //RPN




    }
}
