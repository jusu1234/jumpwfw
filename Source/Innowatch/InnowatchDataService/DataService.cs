﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DataService.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   The data service.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace InnowatchService
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Data;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.ServiceModel.Web;
    using System.Text;
    using System.Xml;
    using System.Xml.Serialization;

    using Innotive.InnoWatch.CommonUtils.Log;

    using InnowatchDataHandler;
    using InnowatchDataHandler.Commons;
    using InnowatchDataHandler.Commons.DataService;

    using InnowatchDataService;
    using InnowatchDataService.Commons;

    using InnowatchService.Commons;
    using InnowatchService.Favorite;
    using InnowatchService.PlaybackVideoInformationInfo;
    using InnowatchService.SearchRecordTime;
    using InnowatchService.TimelineInfo;
    using InnowatchService.VideoInformationInfo;

    using InnowatchServiceLog;
    using InnowatchServiceLog.ConsoleMessage;
    using System.Xml.Linq;

    /// <summary>
    /// The data service.
    /// </summary>
    public class DataService : IDataService
    {
        private DataTable dataTable = new DataTable();

        private string port = null;

        public Stream GetTest()
        {
            var consolePrinter = new DataServiceMessage();

            try
            {
                if (WebOperationContext.Current == null)
                {
                    throw new Exception("Can not get current WebOpreationContext.");
                }

                // Content는 무조건 xml임.
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/xml";

                var resultString = DataManager.GetInstance.GetDataForConfiguration.GetProjectInfo();

                var bytes = Encoding.UTF8.GetBytes(resultString);

                return new MemoryStream(bytes);
            }
            catch (Exception ex)
            {
                consolePrinter.PrintExceptionMessage(ex);
                return CommonUtil.ResponseData(ResponseType.Failed, ex.Message);
            }
        }

        #region Common Interface

        #region GetData

        /// <summary>
        /// The connection check.
        /// </summary>
        /// <returns>
        /// The <see cref="Stream"/>.
        /// </returns>
        /// <exception cref="Exception">
        /// </exception>
        public Stream ConnectionCheck()
        {
            using (var consolePrinter = new DataServiceMessage())
            {
                try
                {
                    if (WebOperationContext.Current == null)
                    {
                        throw new Exception("Can not get current WebOpreationContext.");
                    }

                    // Content는 무조건 xml임.
                    WebOperationContext.Current.OutgoingResponse.ContentType = "application/xml";

                    consolePrinter.PrintResponseMessage(ResponseType.Success.ToString());
                    return CommonUtil.ResponseData(ResponseType.Success);
                }
                catch (Exception ex)
                {
                    consolePrinter.PrintExceptionMessage(ex);
                    return CommonUtil.ResponseData(ResponseType.Failed, ex.Message);
                }
            }
        }

        /// <summary>
        /// The get license.
        /// </summary>
        /// <param name="ip">
        /// The ip.
        /// </param>
        /// <param name="type">
        /// The type.
        /// </param>
        /// <returns>
        /// The <see cref="Stream"/>.
        /// </returns>
        /// <exception cref="Exception">
        /// </exception>
        public Stream GetLicense(string ip, string type = null)
        {
            using (var consolePrinter = new DataServiceMessage())
            {
                string resultStr;

                try
                {
                    if (WebOperationContext.Current == null)
                    {
                        throw new Exception("Can not get current WebOpreationContext.");
                    }

                    // Content는 무조건 xml임.
                    WebOperationContext.Current.OutgoingResponse.ContentType = "application/xml";

                    // 완성된 xml string을 반환한다.
                    resultStr = DataManager.GetInstance.GetDataForClient.GetLicense(ip, type);

                    if (string.IsNullOrEmpty(resultStr))
                    {
                        throw new Exception(string.Format("GetData({0}) fail.", ip));
                    }
                }
                catch (Exception ex)
                {
                    // 뭔가 잘못되면 다음과 같은 Element에 들어감.
                    resultStr = string.Format("<Error>{0}</Error>", ex.Message);
                }

                consolePrinter.PrintResponseMessage(resultStr);

                // 인코딩은 UTF8 임.
                var bytes = Encoding.UTF8.GetBytes(resultStr);
                return new MemoryStream(bytes);
            }
        }

        #endregion // GetData

        #region SetData

        /// <summary>
        /// The set license.
        /// </summary>
        /// <param name="stream">
        /// The stream.
        /// </param>
        /// <param name="ip">
        /// The ip.
        /// </param>
        /// <param name="type">
        /// The type.
        /// </param>
        /// <returns>
        /// The <see cref="Stream"/>.
        /// </returns>
        public Stream SetLicense(Stream stream, string ip, string type = null)
        {
            try
            {
                using (var readStream = new StreamReader(stream))
                {
                    var licenseDataSet = new DataSet();

                    licenseDataSet.ReadXml(readStream);

                    var dataSet = new DataSet();

                    var viewServerIpList = DataManager.GetInstance.GetDataForConfiguration.GetViewServerInfo();

                    var bytes = Encoding.UTF8.GetBytes(viewServerIpList);

                    dataSet.ReadXml(new MemoryStream(bytes));

                    var resultString = string.Empty;

                    foreach (DataRow dataRow in dataSet.Tables[0].Rows)
                    {
                        if (!dataRow["ViewServerIP"].ToString().Equals(ip) || !dataRow["ViewServerType"].ToString().Equals(type))
                        {
                            continue;
                        }

                        resultString = DataManager.GetInstance.SetData.SetLicense(licenseDataSet.Tables[0].Rows[0]["ActivationCode"].ToString().Trim(), ip, type);

                        if (!string.IsNullOrWhiteSpace(resultString))
                        {
                            return CommonUtil.ResponseData(ResponseType.Failed, resultString);
                        }

                        return CommonUtil.ResponseData(ResponseType.Success);
                    }

                    return CommonUtil.ResponseData(ResponseType.Failed);
                }
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return CommonUtil.ResponseData(ResponseType.Failed, ex.Message);
            }
        }

        #endregion // SetData

        #endregion // Common Interface

        //------------------------------------------------------------------------------------------------------------------------//

        #region View Config Interface

        #region GetData

        /// <summary>
        /// The get view common default config.
        /// </summary>
        /// <param name="type">
        /// The type.
        /// </param>
        /// <returns>
        /// The <see cref="Stream"/>.
        /// </returns>
        /// <exception cref="Exception">
        /// </exception>
        public Stream GetViewCommonDefaultConfig(string type)
        {
            var resultStr = string.Empty;

            try
            {
                if (WebOperationContext.Current == null)
                {
                    throw new Exception("Can not get current WebOpreationContext.");
                }

                // Content는 무조건 xml임.
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/xml";

                const string configFileDir = "ConfigFiles/";

                // Type에 따른 파일명
                var filename = string.Empty;

                if (string.Compare(type.Trim(), "command", true) == 0)
                {
                    filename = "command_common.config.default";
                }
                if (string.Compare(type.Trim(), "viewer", true) == 0)
                {
                    filename = "viewer_common.config.default";
                }
                if (string.Compare(type.Trim(), "display", true) == 0)
                {
                    filename = "display_common.config.default";
                }

                var path = string.Format("{0}{1}", configFileDir, filename);

                if (!string.IsNullOrEmpty(path))
                {
                    if (File.Exists(path))
                    {
                        resultStr = string.Format("{0}", File.ReadAllText(path));
                    }
                }
            }
            catch (Exception ex)
            {
                // 뭔가 잘못되면 다음과 같은 Element에 들어감.
                resultStr = string.Format("<Error>{0}</Error>", ex.Message);
            }

            // 인코딩은 UTF8 임.
            var bytes = Encoding.UTF8.GetBytes(resultStr);
            return new MemoryStream(bytes);
        }

        /// <summary>
        /// The get view common config.
        /// </summary>
        /// <param name="type">
        /// The type.
        /// </param>
        /// <returns>
        /// The <see cref="Stream"/>.
        /// </returns>
        /// <exception cref="Exception">
        /// </exception>
        public Stream GetViewCommonConfig(string type)
        {
            var resultStr = string.Empty;

            try
            {
                if (WebOperationContext.Current == null)
                {
                    throw new Exception("Can not get current WebOpreationContext.");
                }

                // Content는 무조건 xml임.
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/xml";

                // Type에 따른 파일명
                const string configFileDir = "ConfigFiles/";
                var filename = string.Empty;

                if (string.Compare(type.Trim(), "command", true) == 0)
                {
                    filename = "command_common.config";
                }
                if (string.Compare(type.Trim(), "viewer", true) == 0)
                {
                    filename = "viewer_common.config";
                }
                if (string.Compare(type.Trim(), "display", true) == 0)
                {
                    filename = "display_common.config";
                }

                var path = string.Format("{0}{1}", configFileDir, filename);
                if (!string.IsNullOrEmpty(path))
                {
                    if (File.Exists(path))
                    {
                        resultStr = string.Format("{0}", File.ReadAllText(path));
                    }
                }
            }
            catch (Exception ex)
            {
                // 뭔가 잘못되면 다음과 같은 Element에 들어감.
                resultStr = string.Format("<Error>{0}</Error>", ex.Message);
            }

            // 인코딩은 UTF8 임.
            var bytes = Encoding.UTF8.GetBytes(resultStr);
            return new MemoryStream(bytes);
        }

        /// <summary>
        /// The get view default config.
        /// </summary>
        /// <param name="ip">
        /// The ip.
        /// </param>
        /// <returns>
        /// The <see cref="Stream"/>.
        /// </returns>
        /// <exception cref="Exception">
        /// </exception>
        public Stream GetViewDefaultConfig(string ip)
        {
            string resultStr;

            try
            {
                if (WebOperationContext.Current == null)
                {
                    throw new Exception("Can not get current WebOpreationContext.");
                }

                // Content는 무조건 xml임.
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/xml";

                resultStr = DataManager.GetInstance.SetData.GetViewServerDefaultConfg(ip);
            }
            catch (Exception ex)
            {
                // 뭔가 잘못되면 다음과 같은 Element에 들어감.
                resultStr = string.Format("<Error>{0}</Error>", ex.Message);
            }

            // 인코딩은 UTF8 임.
            var bytes = Encoding.UTF8.GetBytes(resultStr);
            return new MemoryStream(bytes);
        }

        /// <summary>
        /// The get view config.
        /// </summary>
        /// <param name="ip">
        /// The ip.
        /// </param>
        /// <returns>
        /// The <see cref="Stream"/>.
        /// </returns>
        /// <exception cref="Exception">
        /// </exception>
        public Stream GetViewConfig(string ip)
        {
            string resultStr;

            try
            {
                if (WebOperationContext.Current == null)
                {
                    throw new Exception("Can not get current WebOpreationContext.");
                }

                // Content는 무조건 xml임.
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/xml";

                resultStr = DataManager.GetInstance.SetData.GetViewServerConfg(ip);
            }
            catch (Exception ex)
            {
                // 뭔가 잘못되면 다음과 같은 Element에 들어감.
                resultStr = string.Format("<Error>{0}</Error>", ex.Message);
            }

            // 인코딩은 UTF8 임.
            var bytes = Encoding.UTF8.GetBytes(resultStr);
            return new MemoryStream(bytes);
        }

        #endregion // GetData

        #region SetData

        /// <summary>
        /// The set view common default config.
        /// </summary>
        /// <param name="stream">
        /// The stream.
        /// </param>
        /// <param name="type">
        /// The type.
        /// </param>
        public void SetViewCommonDefaultConfig(Stream stream, string type)
        {
            try
            {
                const string configFileDir = "ConfigFiles/";

                // Type에 따른 파일명
                var filename = string.Empty;
                if (string.Compare(type.Trim(), "command", true) == 0)
                {
                    filename = "command_common.config.default";
                }
                if (string.Compare(type.Trim(), "viewer", true) == 0)
                {
                    filename = "viewer_common.config.default";
                }
                if (string.Compare(type.Trim(), "display", true) == 0)
                {
                    filename = "display_common.config.default";
                }

                var path = string.Format("{0}{1}", configFileDir, filename);
                if (!string.IsNullOrEmpty(path))
                {
                    using (var readStream = new StreamReader(stream))
                    {
                        File.WriteAllText(path, readStream.ReadToEnd());
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
            }
        }

        /// <summary>
        /// The set view common config.
        /// </summary>
        /// <param name="stream">
        /// The stream.
        /// </param>
        /// <param name="type">
        /// The type.
        /// </param>
        public void SetViewCommonConfig(Stream stream, string type)
        {
            try
            {
                const string configFileDir = "ConfigFiles/";

                // Type에 따른 파일명
                var filename = string.Empty;
                if (string.Compare(type.Trim(), "command", true) == 0)
                {
                    filename = "command_common.config";
                }
                if (string.Compare(type.Trim(), "viewer", true) == 0)
                {
                    filename = "viewer_common.config";
                }
                if (string.Compare(type.Trim(), "display", true) == 0)
                {
                    filename = "display_common.config";
                }

                var path = string.Format("{0}{1}", configFileDir, filename);
                if (!string.IsNullOrEmpty(path))
                {
                    using (var readStream = new StreamReader(stream))
                    {
                        File.WriteAllText(path, readStream.ReadToEnd());
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
            }
        }

        /// <summary>
        /// The set view default config.
        /// </summary>
        /// <param name="stream">
        /// The stream.
        /// </param>
        /// <param name="ip">
        /// The ip.
        /// </param>
        public void SetViewDefaultConfig(Stream stream, string ip)
        {
            try
            {
                string content;
                using (var readStream = new StreamReader(stream))
                {
                    content = readStream.ReadToEnd();
                }

                DataManager.GetInstance.SetData.UpdateViewServerDefaultConfg(ip, content);
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
            }
        }

        /// <summary>
        /// The set view config.
        /// </summary>
        /// <param name="stream">
        /// The stream.
        /// </param>
        /// <param name="ip">
        /// The ip.
        /// </param>
        public void SetViewConfig(Stream stream, string ip)
        {
            try
            {
                string content;
                using (var readStream = new StreamReader(stream))
                {
                    content = readStream.ReadToEnd();
                }

                DataManager.GetInstance.SetData.UpdateViewServerConfg(ip, content);
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
            }
        }

        #endregion // SetData

        #endregion // View Config Interface

        //------------------------------------------------------------------------------------------------------------------------//

        #region Command

        #region GetData

        /// <summary>
        /// The get client event service url.
        /// </summary>
        /// <param name="clientId">
        /// The client id.
        /// </param>
        /// <returns>
        /// The <see cref="Stream"/>.
        /// </returns>
        /// <exception cref="Exception">
        /// </exception>
        public Stream GetClientEventServiceUrl(string clientId)
        {
            using (var consolePrinter = new DataServiceMessage(clientId))
            {
                try
                {
                    if (WebOperationContext.Current == null)
                    {
                        throw new Exception("Can not get current WebOpreationContext.");
                    }

                    // Content는 무조건 xml임.
                    WebOperationContext.Current.OutgoingResponse.ContentType = "application/xml";

                    var resultString = DataManager.GetInstance.GetDataForClient.GetClientEventServiceUrl(clientId);

                    consolePrinter.PrintResponseMessage();

                    var bytes = Encoding.UTF8.GetBytes(resultString);
                    return new MemoryStream(bytes);
                }
                catch (Exception ex)
                {
                    consolePrinter.PrintExceptionMessage(ex);
                    //Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                    return null;
                }
            }
        }

        /// <summary>
        /// The get video information.
        /// </summary>
        /// <param name="projectId">
        /// The project id.
        /// </param>
        /// <returns>
        /// The xml of video information.
        /// </returns>
        public Stream GetVideoInformation(string projectId)
        {
            using (var consolePrinter = new DataServiceMessage(projectId))
            {
                try
                {
                    if (WebOperationContext.Current == null)
                    {
                        throw new Exception("Can not get current WebOpreationContext.");
                    }

                    // Content는 무조건 xml임.
                    WebOperationContext.Current.OutgoingResponse.ContentType = "application/xml";

                    var createData = new CreateToVideoInformationData();
                    var cameraData = DataManager.GetInstance.GetDataForClient.GetCameraInfoForVideoInformation(projectId);
                    var mergerData = DataManager.GetInstance.GetDataForClient.GetMediaServerInfoForVideoInformation(projectId);

                    var resultString = createData.CreateVideoInformation(cameraData, mergerData);

                    consolePrinter.PrintResponseMessage();

                    //// DataSet 전송 구조 test
                    //var resultString = this.GetSampleVideoInformationDataSet();
                    var bytes = Encoding.UTF8.GetBytes(resultString);

                    return new MemoryStream(bytes);
                }
                catch (Exception ex)
                {
                    consolePrinter.PrintExceptionMessage(ex);
                    //Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                    return null;
                }
            }
        }

        /// <summary>
        /// The get log in user info for client.
        /// </summary>
        /// <param name="userId">
        /// The user id.
        /// </param>
        /// <param name="password">
        /// The password.
        /// </param>
        /// <returns>
        /// The <see cref="Stream"/>.
        /// </returns>
        /// <exception cref="Exception">
        /// </exception>
        public Stream GetLogInUserInfoForClient(string userId, string password)
        {
            using (var consolePrinter = new DataServiceMessage(userId, password))
            {
                try
                {
                    if (WebOperationContext.Current == null)
                    {
                        throw new Exception("Can not get current WebOpreationContext.");
                    }

                    // Content는 무조건 xml임.
                    WebOperationContext.Current.OutgoingResponse.ContentType = "application/xml";

                    var resultString = DataManager.GetInstance.GetDataForClient.GetUserInfo(userId, password, string.Empty);

                    consolePrinter.PrintResponseMessage(resultString);

                    var bytes = Encoding.UTF8.GetBytes(resultString);
                    return new MemoryStream(bytes);
                }
                catch (Exception ex)
                {
                    consolePrinter.PrintExceptionMessage(ex);
                    //Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                    return null;
                }
            }
        }

        public Stream GetLogInUserInfoForCommand(string userId, string password)
        {
            using (var consolePrinter = new DataServiceMessage(userId, password))
            {
                try
                {
                    if (WebOperationContext.Current == null)
                    {
                        throw new Exception("Can not get current WebOpreationContext.");
                    }

                    // Content는 무조건 xml임.
                    WebOperationContext.Current.OutgoingResponse.ContentType = "application/xml";

                    var resultString = DataManager.GetInstance.GetDataForClient.GetUserInfo(userId, password, "COMMAND");

                    consolePrinter.PrintResponseMessage(resultString);

                    var bytes = Encoding.UTF8.GetBytes(resultString);
                    return new MemoryStream(bytes);
                }
                catch (Exception ex)
                {
                    consolePrinter.PrintExceptionMessage(ex);
                    //Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                    return null;
                }
            }
        }

        public Stream GetLogInUserInfoForViewer(string userId, string password)
        {
            using (var consolePrinter = new DataServiceMessage(userId, password))
            {
                try
                {
                    if (WebOperationContext.Current == null)
                    {
                        throw new Exception("Can not get current WebOpreationContext.");
                    }

                    // Content는 무조건 xml임.
                    WebOperationContext.Current.OutgoingResponse.ContentType = "application/xml";

                    var resultString = DataManager.GetInstance.GetDataForClient.GetUserInfo(userId, password, "VIEWER");

                    consolePrinter.PrintResponseMessage(resultString);

                    var bytes = Encoding.UTF8.GetBytes(resultString);
                    return new MemoryStream(bytes);
                }
                catch (Exception ex)
                {
                    consolePrinter.PrintExceptionMessage(ex);
                    //Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                    return null;
                }
            }
        }

        public Stream GetLogInUserInfoForPlayback(string userId, string password)
        {
            using (var consolePrinter = new DataServiceMessage(userId, password))
            {
                try
                {
                    if (WebOperationContext.Current == null)
                    {
                        throw new Exception("Can not get current WebOpreationContext.");
                    }

                    // Content는 무조건 xml임.
                    WebOperationContext.Current.OutgoingResponse.ContentType = "application/xml";

                    var resultString = DataManager.GetInstance.GetDataForClient.GetUserInfo(userId, password, "PLAYBACK");

                    consolePrinter.PrintResponseMessage(resultString);

                    var bytes = Encoding.UTF8.GetBytes(resultString);
                    return new MemoryStream(bytes);
                }
                catch (Exception ex)
                {
                    consolePrinter.PrintExceptionMessage(ex);
                    //Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                    return null;
                }
            }
        }

        /// <summary>
        /// The get camera group.
        /// </summary>
        /// <param name="projectId">The project id.</param>
        /// <param name="userId">The user id.</param>
        /// <param name="type">The type.</param>
        /// <returns>The xml of camera group.</returns>
        public Stream GetCameraGroupInfoForClient(string projectId, string userId, string type)
        {
            using (var consolePrinter = new DataServiceMessage(projectId, userId, type))
            {
                try
                {
                    if (WebOperationContext.Current == null)
                    {
                        throw new Exception("Can not get current WebOpreationContext.");
                    }

                    // Content는 무조건 xml임.
                    WebOperationContext.Current.OutgoingResponse.ContentType = "application/xml";

                    var dataSet = new DataSet("CameraGroup");

                    var parentCameraGroupDataTable =
                        DataManager.GetInstance.SetData.SelectParentCameraGroupForClient(projectId, userId, type.ToLower()).Tables[0];
                    parentCameraGroupDataTable.TableName = "ParentCameraGroup";

                    var childCameraGroupDataTable =
                        DataManager.GetInstance.SetData.SelectChildCameraGroupForClient(projectId, userId, type.ToLower()).Tables[0];
                    childCameraGroupDataTable.TableName = "ChildCameraGroup";

                    var cameraMappingDataTable =
                        DataManager.GetInstance.SetData.SelectCameraGroupMappingForClient(projectId, userId, type.ToLower()).Tables[0].Copy();
                    cameraMappingDataTable.TableName = "CameraMapping";

                    dataSet.Tables.Add(parentCameraGroupDataTable.Copy());
                    dataSet.Tables.Add(childCameraGroupDataTable.Copy());
                    dataSet.Tables.Add(cameraMappingDataTable.Copy());

                    var xmlDocument = SetXmlToCameraGroup(dataSet);

                    var resultString = xmlDocument.InnerXml;

                    consolePrinter.PrintResponseMessage();

                    var bytes = Encoding.UTF8.GetBytes(resultString);

                    return new MemoryStream(bytes);
                }
                catch (Exception ex)
                {
                    consolePrinter.PrintExceptionMessage(ex);
                    //Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                    return null;
                }
            }
        }

        /// <summary>
        /// The set xml to camera group.
        /// </summary>
        /// <param name="dataSet">The camera group dataset.</param>
        /// <returns>The xml of camera group.</returns>
        private static XmlDocument SetXmlToCameraGroup(DataSet dataSet)
        {
            var xmlDocument = new XmlDocument();

            var xmlDeclaration = xmlDocument.CreateXmlDeclaration("1.0", "utf-8", null);
            xmlDocument.AppendChild(xmlDeclaration);

            var root = xmlDocument.CreateElement("CameraGroupInfoList");
            xmlDocument.AppendChild(root);

            for (var i = 0; i < dataSet.Tables.Count; i++)
            {
                foreach (DataRow row in dataSet.Tables[i].Rows)
                {
                    var cameraGroupInfo = xmlDocument.CreateElement("CameraGroupInfo");
                    root.AppendChild(cameraGroupInfo);

                    var attributeIsFolder = xmlDocument.CreateAttribute("IsFolder");
                    var attributeId = xmlDocument.CreateAttribute("ID");
                    var attributeCameraGroupName = xmlDocument.CreateAttribute("CameraGroupName");
                    var attributeParentId = xmlDocument.CreateAttribute("ParentID");
                    var attributeItemOrder = xmlDocument.CreateAttribute("ItemOrder");
                    var attributeCameraId = xmlDocument.CreateAttribute("CameraID");
                    var attributeCameraName = xmlDocument.CreateAttribute("CameraName");

                    // Folder 정보
                    if (!dataSet.Tables[i].TableName.Equals("CameraMapping"))
                    {
                        attributeIsFolder.Value = "true";
                        attributeId.Value = row["CameraGroupId"].ToString();
                        attributeCameraGroupName.Value = row["CameraGroupName"].ToString();
                        attributeParentId.Value = row["ParentID"].ToString();
                        attributeItemOrder.Value = row["ItemOrder"].ToString();
                        attributeCameraId.Value = string.Empty;
                        attributeCameraName.Value = string.Empty;
                    }
                    else
                    {
                        attributeIsFolder.Value = "false";
                        attributeId.Value = string.Empty;
                        attributeCameraGroupName.Value = string.Empty;
                        attributeParentId.Value = row["CameraGroupId"].ToString();
                        attributeItemOrder.Value = row["ItemOrder"].ToString();
                        attributeCameraId.Value = row["ProjectCameraID"].ToString();
                        attributeCameraName.Value = row["ProjectCameraName"].ToString();
                    }

                    cameraGroupInfo.Attributes.Append(attributeIsFolder);
                    cameraGroupInfo.Attributes.Append(attributeId);
                    cameraGroupInfo.Attributes.Append(attributeCameraGroupName);
                    cameraGroupInfo.Attributes.Append(attributeParentId);
                    cameraGroupInfo.Attributes.Append(attributeItemOrder);
                    cameraGroupInfo.Attributes.Append(attributeCameraId);
                    cameraGroupInfo.Attributes.Append(attributeCameraName);
                }
            }

            return xmlDocument;
        }

        /// <summary>
        /// MD, RC 별로 Grouping 된 Camera 정보를 반환한다.
        /// </summary>
        /// <param name="projectId">The project ID.</param>
        /// <param name="group">The grouping target. (MD or RC)</param>
        /// <returns></returns>
        public Stream GetCameraGroupInfoForClientByGrouping(string projectId, string group)
        {
            using (var consolePrinter = new DataServiceMessage(projectId, group))
            {
                try
                {
                    if (WebOperationContext.Current == null)
                    {
                        throw new Exception("Can not get current WebOpreationContext.");
                    }

                    // Content는 무조건 xml임.
                    WebOperationContext.Current.OutgoingResponse.ContentType = "application/xml";

                    var resultSet =
                        DataManager.GetInstance.GetDataForClient.GetCameraGroupForClientByGrouping(projectId, group);

                    var groups =
                        resultSet.Tables[0].Rows.Cast<DataRow>().GroupBy(row => row["Group"].ToString());

                    var groupXml = this.GetCameraGoupInfoXmlByGrouping(groups);

                    consolePrinter.PrintResponseMessage();

                    var bytes = Encoding.UTF8.GetBytes(groupXml.OuterXml);

                    return new MemoryStream(bytes);
                }
                catch (Exception ex)
                {
                    consolePrinter.PrintExceptionMessage(ex);
                    //Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                    return null;
                }
            }
        }

        private XmlDocument GetCameraGoupInfoXmlByGrouping(IEnumerable<IGrouping<string, DataRow>> groups)
        {
            var xmlDocument = new XmlDocument();

            var xmlDeclaration = xmlDocument.CreateXmlDeclaration("1.0", "utf-8", null);
            xmlDocument.AppendChild(xmlDeclaration);

            var root = xmlDocument.CreateElement("CameraGroupInfoList");
            xmlDocument.AppendChild(root);

            IGrouping<string, DataRow> notAllocated = null;
            var groupId = 0;
            foreach (var group in groups)
            {
                if (string.IsNullOrWhiteSpace(group.Key))
                {
                    notAllocated = group;
                    continue;
                }

                this.AppendCameraGroupElement(xmlDocument, root, group.Key, groupId.ToString());



                //// Group 생성.
                //var cameraGroupInfo = xmlDocument.CreateElement("CameraGroupInfo");
                //root.AppendChild(cameraGroupInfo);

                //var attributeIsFolder = xmlDocument.CreateAttribute("IsFolder");
                //var attributeId = xmlDocument.CreateAttribute("ID");
                //var attributeCameraGroupName = xmlDocument.CreateAttribute("CameraGroupName");
                //var attributeParentId = xmlDocument.CreateAttribute("ParentID");
                //var attributeItemOrder = xmlDocument.CreateAttribute("ItemOrder");
                //var attributeCameraId = xmlDocument.CreateAttribute("CameraID");
                //var attributeCameraName = xmlDocument.CreateAttribute("CameraName");

                //attributeIsFolder.Value = "true";
                //attributeId.Value = groupId.ToString();
                //attributeCameraGroupName.Value = group.Key;
                //attributeParentId.Value = "-1";
                //attributeItemOrder.Value = groupId.ToString(); // Group의 Order는 Id로 대체한다.
                //attributeCameraId.Value = string.Empty;
                //attributeCameraName.Value = string.Empty;

                //cameraGroupInfo.Attributes.Append(attributeIsFolder);
                //cameraGroupInfo.Attributes.Append(attributeId);
                //cameraGroupInfo.Attributes.Append(attributeCameraGroupName);
                //cameraGroupInfo.Attributes.Append(attributeParentId);
                //cameraGroupInfo.Attributes.Append(attributeItemOrder);
                //cameraGroupInfo.Attributes.Append(attributeCameraId);
                //cameraGroupInfo.Attributes.Append(attributeCameraName);

                var cameraOrder = 0;
                foreach (var row in group)
                {
                    if (string.IsNullOrWhiteSpace(row["CameraID"].ToString()))
                    {
                        continue;
                    }

                    this.AppendCameraGroupChildElement(
                        xmlDocument, 
                        root, 
                        row["CameraID"].ToString(), 
                        row["CameraName"].ToString(), 
                        groupId.ToString(), 
                        cameraOrder.ToString());




                    //var cameraInfo = xmlDocument.CreateElement("CameraGroupInfo");
                    //root.AppendChild(cameraInfo);

                    //var cameraAttributeIsFolder = xmlDocument.CreateAttribute("IsFolder");
                    //var cameraAttributeId = xmlDocument.CreateAttribute("ID");
                    //var cameraAttributeCameraGroupName = xmlDocument.CreateAttribute("CameraGroupName");
                    //var cameraAttributeParentId = xmlDocument.CreateAttribute("ParentID");
                    //var cameraAttributeItemOrder = xmlDocument.CreateAttribute("ItemOrder");
                    //var cameraAttributeCameraId = xmlDocument.CreateAttribute("CameraID");
                    //var cameraAttributeCameraName = xmlDocument.CreateAttribute("CameraName");

                    //cameraAttributeIsFolder.Value = "false";
                    //cameraAttributeId.Value = string.Empty;
                    //cameraAttributeCameraGroupName.Value = string.Empty;
                    //cameraAttributeParentId.Value = groupId.ToString();
                    //cameraAttributeItemOrder.Value = cameraOrder.ToString();
                    //cameraAttributeCameraId.Value = row["CameraID"].ToString();
                    //cameraAttributeCameraName.Value = row["CameraName"].ToString();

                    //cameraInfo.Attributes.Append(cameraAttributeIsFolder);
                    //cameraInfo.Attributes.Append(cameraAttributeId);
                    //cameraInfo.Attributes.Append(cameraAttributeCameraGroupName);
                    //cameraInfo.Attributes.Append(cameraAttributeParentId);
                    //cameraInfo.Attributes.Append(cameraAttributeItemOrder);
                    //cameraInfo.Attributes.Append(cameraAttributeCameraId);
                    //cameraInfo.Attributes.Append(cameraAttributeCameraName);

                    cameraOrder++;
                }

                groupId++;
            }

            if (notAllocated != null)
            {
                this.AppendCameraGroupElement(xmlDocument, root, "Not Allocated", groupId.ToString());
                var cameraOrder = 0;
                foreach (var row in notAllocated)
                {
                    this.AppendCameraGroupChildElement(
                        xmlDocument, 
                        root, 
                        row["CameraID"].ToString(), 
                        row["CameraName"].ToString(), 
                        groupId.ToString(), 
                        cameraOrder.ToString());

                    cameraOrder++;
                }
            }

            return xmlDocument;
        }

        private XmlElement CreateCameraGroupingElement(XmlDocument xmlDocument, XmlElement root)
        {
            var cameraInfo = xmlDocument.CreateElement("CameraGroupInfo");
            root.AppendChild(cameraInfo);

            var cameraAttributeIsFolder = xmlDocument.CreateAttribute("IsFolder");
            var cameraAttributeId = xmlDocument.CreateAttribute("ID");
            var cameraAttributeCameraGroupName = xmlDocument.CreateAttribute("CameraGroupName");
            var cameraAttributeParentId = xmlDocument.CreateAttribute("ParentID");
            var cameraAttributeItemOrder = xmlDocument.CreateAttribute("ItemOrder");
            var cameraAttributeCameraId = xmlDocument.CreateAttribute("CameraID");
            var cameraAttributeCameraName = xmlDocument.CreateAttribute("CameraName");
            
            cameraInfo.Attributes.Append(cameraAttributeIsFolder);
            cameraInfo.Attributes.Append(cameraAttributeId);
            cameraInfo.Attributes.Append(cameraAttributeCameraGroupName);
            cameraInfo.Attributes.Append(cameraAttributeParentId);
            cameraInfo.Attributes.Append(cameraAttributeItemOrder);
            cameraInfo.Attributes.Append(cameraAttributeCameraId);
            cameraInfo.Attributes.Append(cameraAttributeCameraName);

            return cameraInfo;
        }

        private void AppendCameraGroupElement(
            XmlDocument xmlDocument, 
            XmlElement root, 
            string groupName,
            string groupId)
        {
            var groupElement = this.CreateCameraGroupingElement(xmlDocument, root);
            groupElement.Attributes["IsFolder"].Value = "true";
            groupElement.Attributes["ID"].Value = groupId;
            groupElement.Attributes["CameraGroupName"].Value = groupName;
            groupElement.Attributes["ParentID"].Value = "-1";
            groupElement.Attributes["ItemOrder"].Value = groupId; // Group의 Order는 Id로 대체한다.
            groupElement.Attributes["CameraID"].Value = string.Empty;
            groupElement.Attributes["CameraName"].Value = string.Empty;
        }

        private void AppendCameraGroupChildElement(
            XmlDocument xmlDocument,
            XmlElement root,
            string childId,
            string childName,
            string parentId,
            string itemOrder)
        {
            var groupElement = this.CreateCameraGroupingElement(xmlDocument, root);
            groupElement.Attributes["IsFolder"].Value = "false";
            groupElement.Attributes["ID"].Value = string.Empty;
            groupElement.Attributes["CameraGroupName"].Value = string.Empty;
            groupElement.Attributes["ParentID"].Value = parentId;
            groupElement.Attributes["ItemOrder"].Value = itemOrder; // Group의 Order는 Id로 대체한다.
            groupElement.Attributes["CameraID"].Value = childId;
            groupElement.Attributes["CameraName"].Value = childName;
        }


        /// <summary>
        /// The get favorite info.
        /// </summary>
        /// <param name="projectId">The project id.</param>
        /// <param name="userId">The user id.</param>
        /// <param name="type"> </param>
        /// <returns>The xml of get favorite info.</returns>
        public Stream GetFavoriteInfo(string projectId, string userId, string type)
        {
            using (var consolePrinter = new DataServiceMessage(projectId, userId, type))
            {
                try
                {
                    if (WebOperationContext.Current == null)
                    {
                        throw new Exception("Can not get current WebOpreationContext.");
                    }

                    // Content는 무조건 xml임.
                    WebOperationContext.Current.OutgoingResponse.ContentType = "application/xml";

                    var favoriteGroupDataSet = DataManager.GetInstance.GetDataForClient.GetFavoriteGroupForClient(
                        projectId, userId, type.ToLower());
                    var favoriteGroupMappingDataSet =
                        DataManager.GetInstance.GetDataForClient.GetFavoriteGroupMappingForClient(
                            projectId, userId, type.ToLower());

                    var getFavoriteInfoDataSet = new DataSet("GetFavoriteInfo");

                    favoriteGroupDataSet.Tables[0].TableName = "FavoriteGroup";
                    favoriteGroupMappingDataSet.Tables[0].TableName = "Favorite";

                    var favoriteGroupDataTable = favoriteGroupDataSet.Tables[0].Copy();
                    var favoriteGroupMappingDataTable = favoriteGroupMappingDataSet.Tables[0].Copy();

                    getFavoriteInfoDataSet.Tables.Add(favoriteGroupDataTable);
                    getFavoriteInfoDataSet.Tables.Add(favoriteGroupMappingDataTable);

                    var resultString = XmlConverter.DataSetToXml(getFavoriteInfoDataSet);

                    consolePrinter.PrintResponseMessage();

                    var bytes = Encoding.UTF8.GetBytes(resultString);

                    return new MemoryStream(bytes);
                }
                catch (Exception ex)
                {
                    consolePrinter.PrintExceptionMessage(ex);
                    //Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                    return null;
                }
            }
        }

        /// <summary>
        /// The get last favorite group id.
        /// </summary>
        /// <returns>
        /// The <see cref="Stream"/>.
        /// </returns>
        /// <exception cref="Exception">
        /// </exception>
        public Stream GetLastFavoriteGroupId()
        {
            using (var consolePrinter = new DataServiceMessage())
            {
                try
                {
                    if (WebOperationContext.Current == null)
                    {
                        throw new Exception("Can not get current WebOpreationContext.");
                    }

                    // Content는 무조건 xml임.
                    WebOperationContext.Current.OutgoingResponse.ContentType = "application/xml";

                    var dataset = DataManager.GetInstance.GetDataForClient.GetLastFavoriteGroupId();

                    consolePrinter.PrintResponseMessage();

                    var resultString = XmlConverter.DataSetToXml(dataset);

                    var bytes = Encoding.UTF8.GetBytes(resultString);

                    return new MemoryStream(bytes);
                }
                catch (Exception ex)
                {
                    consolePrinter.PrintExceptionMessage(ex);
                    //Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                    return null;
                }
            }
        }

        /// <summary>
        /// The get camera information.
        /// </summary>
        /// <param name="mdIp">
        /// The md ip.
        /// </param>
        /// <returns>
        /// The <see cref="Stream"/>.
        /// </returns>
        /// <exception cref="Exception">
        /// </exception>
        public Stream GetCameraInformation(string mdIp)
        {
            using (var consolePrinter = new DataServiceMessage(mdIp))
            {
                string resultStr;
                try
                {
                    if (WebOperationContext.Current == null)
                    {
                        throw new Exception("Can not get current WebOpreationContext.");
                    }

                    // Content는 무조건 xml임.
                    WebOperationContext.Current.OutgoingResponse.ContentType = "application/xml";

                    // 완성된 xml string을 반환한다.
                    resultStr = DataManager.GetInstance.GetDataForClient.GetCameraDataToMediaConfigXml(mdIp);
                    if (string.IsNullOrEmpty(resultStr))
                    {
                        throw new Exception(string.Format("GetData({0}) fail.", mdIp));
                    }

                    consolePrinter.PrintResponseMessage();
                }
                catch (Exception ex)
                {
                    // 뭔가 잘못되면 다음과 같은 Element에 들어감.
                    resultStr = string.Format("<Error>{0}</Error>", ex.Message);
                    consolePrinter.PrintExceptionMessage(ex);
                    consolePrinter.PrintMessage(resultStr);
                }

                // 인코딩은 UTF8 임.
                var bytes = Encoding.UTF8.GetBytes(resultStr);
                return new MemoryStream(bytes);
            }
        }

        /// <summary>
        /// The get camera controller information.
        /// </summary>
        /// <param name="projectId">
        /// The project id.
        /// </param>
        /// <returns>
        /// The response data.
        /// </returns>
        public Stream GetCameraControllerInformation(string projectId)
        {
            using (var consolePrinter = new DataServiceMessage(projectId))
            {
                string resultStr;

                try
                {
                    if (WebOperationContext.Current == null)
                    {
                        throw new Exception("Can not get current WebOpreationContext.");
                    }

                    // Content는 무조건 xml임.
                    WebOperationContext.Current.OutgoingResponse.ContentType = "application/xml";

                    // PTZ Information List 정보를 받아온다.
                    var ptzInfoDataTable = DataManager.GetInstance.GetDataForClient.GetPtzInformationList(projectId);

                    // Camera Controller Info 정보를 받아온다.
                    var cameraControllerInfoDataTable = DataManager.GetInstance.GetDataForClient.GetCameraControllerInfo(projectId);

                    resultStr = this.MakeCamereControllerInfo(ptzInfoDataTable, cameraControllerInfoDataTable);

                    if (string.IsNullOrEmpty(resultStr))
                    {
                        throw new Exception(string.Format("GetData({0}) fail.", "CameraControllerInfo"));
                    }

                    consolePrinter.PrintResponseMessage();
                }
                catch (Exception ex)
                {
                    // 뭔가 잘못되면 다음과 같은 Element에 들어감.
                    resultStr = string.Format("<Error>{0}</Error>", ex.Message);
                    consolePrinter.PrintExceptionMessage(ex);
                    consolePrinter.PrintMessage(resultStr);
                }

                // 인코딩은 UTF8 임.
                var bytes = Encoding.UTF8.GetBytes(resultStr);
                return new MemoryStream(bytes);
            }
        }

        private string MakeCamereControllerInfo(DataTable ptzInfo, DataTable cameracontrollerInfo)
        {
            var cameraControllerInfoDataSet = new DataSet("CameraControllerInformation");

            var ptzDataTable = ptzInfo.Copy();
            ptzDataTable.TableName = "PtzInformationList";

            var cameraControllerInfoDataTable = cameracontrollerInfo.Copy();
            cameraControllerInfoDataTable.TableName = "CameraControllerInfo";

            cameraControllerInfoDataSet.Tables.Add(ptzDataTable);
            cameraControllerInfoDataSet.Tables.Add(cameraControllerInfoDataTable);

            return CommonUtil.XmlToDataSet(cameraControllerInfoDataSet);
        }

        public Stream GetMapLayout(string projectId)
        {
            using (var consolePrinter = new DataServiceMessage(projectId))
            {
                string layoutData;

                try
                {
                    // 저장 위치에 있는 xaml 문자열을 가져와서 Stream으로 return.
                    if (WebOperationContext.Current == null)
                    {
                        throw new Exception("Can not get current WebOpreationContext.");
                    }

                    // Content는 무조건 xml임.
                    WebOperationContext.Current.OutgoingResponse.ContentType = "application/xml";

                    layoutData = DataManager.GetInstance.GetDataForClient.GetMapLayout(projectId);
                    consolePrinter.PrintResponseMessage(layoutData);
                }
                catch (Exception ex)
                {
                    // 뭔가 잘못되면 다음과 같은 Element에 들어감.
                    layoutData = string.Format("<Error>{0}</Error>", ex.Message);
                    consolePrinter.PrintExceptionMessage(ex);
                    consolePrinter.PrintMessage(layoutData);
                    //Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                }

                var bytes = Encoding.UTF8.GetBytes(layoutData);
                return new MemoryStream(bytes);
            }
        }

        public Stream GetLocationBookmark(string projectId, string userId, string type)
        {
            using (var consolePrinter = new DataServiceMessage(projectId, userId, type))
            {
                string locationBookmarkData;

                try
                {
                    // 저장 위치에 있는 xaml 문자열을 가져와서 Stream으로 return.
                    if (WebOperationContext.Current == null)
                    {
                        throw new Exception("Can not get current WebOpreationContext.");
                    }

                    // Content는 무조건 xml임.
                    WebOperationContext.Current.OutgoingResponse.ContentType = "application/xml";

                    locationBookmarkData = DataManager.GetInstance.GetDataForClient.GetLocationBookmark(projectId, userId);
                    consolePrinter.PrintResponseMessage();
                }
                catch (Exception ex)
                {
                    // 뭔가 잘못되면 다음과 같은 Element에 들어감.
                    locationBookmarkData = string.Format("<Error>{0}</Error>", ex.Message);
                    consolePrinter.PrintExceptionMessage(ex);
                    consolePrinter.PrintMessage(locationBookmarkData);
                    //Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                }

                var bytes = Encoding.UTF8.GetBytes(locationBookmarkData ?? string.Empty);

                return new MemoryStream(bytes);
            }
        }

        public Stream GetFavoriteSlide(string projectId, string userId, string type)
        {
            using (var consolePrinter = new DataServiceMessage(projectId, userId, type))
            {
                string favoriteSlideData;

                try
                {
                    // 저장 위치에 있는 xaml 문자열을 가져와서 Stream으로 return.
                    if (WebOperationContext.Current == null)
                    {
                        throw new Exception("Can not get current WebOpreationContext.");
                    }

                    // Content는 무조건 xml임.
                    WebOperationContext.Current.OutgoingResponse.ContentType = "application/xml";

                    favoriteSlideData = DataManager.GetInstance.GetDataForClient.GetFavoriteSlide(projectId, userId, type);
                    consolePrinter.PrintResponseMessage();
                }
                catch (Exception ex)
                {
                    // 뭔가 잘못되면 다음과 같은 Element에 들어감.
                    favoriteSlideData = string.Format("<Error>{0}</Error>", ex.Message);
                    consolePrinter.PrintExceptionMessage(ex);
                    consolePrinter.PrintMessage(favoriteSlideData);
                    //Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                }

                var bytes = Encoding.UTF8.GetBytes(favoriteSlideData ?? string.Empty);

                return new MemoryStream(bytes);
            }
        }

        #endregion // GetData

        #region SetData

        #region Favorite

        /// <summary>
        /// The add favorite info.
        /// </summary>
        /// <param name="stream">The stream.</param>
        /// <param name="projectId">The project id.</param>
        /// <param name="userId">The user id.</param>
        /// <param name="type">The type.</param>
        /// <returns>The result value.</returns>
        public Stream AddFavoriteInfo(Stream stream, string projectId, string userId, string type)
        {
            using (var consolePrinter = new DataServiceMessage(projectId, userId, type))
            {
                try
                {
                    if (WebOperationContext.Current == null)
                    {
                        throw new Exception("Can not get current WebOpreationContext.");
                    }

                    // Content는 무조건 xml임.
                    WebOperationContext.Current.OutgoingResponse.ContentType = "application/xml";

                    // xmlDataSet 을 DataSet으로 변환하여 필요한 정보를 Insert 한다.
                    // DataSet 은 Favorite / Favorite Group 에 대한 2개의 Table 을 가지고 있다.

                    using (var readStream = new StreamReader(stream))
                    {
                        var favoriteDataTable = new DataTable();

                        favoriteDataTable.ReadXml(readStream);

                        // Favorite / FavoriteMapping 추가.
                        if (favoriteDataTable.TableName.Equals("Favorite"))
                        {
                            var itemOrder = 0;
                            var favoriteId = 0;

                            foreach (DataRow row in favoriteDataTable.Rows)
                            {
                                var dataSet = DataManager.GetInstance.SetData.InsertFavorite(
                                    row["FavoriteName"].ToString(), row["Data"].ToString(), type.ToLower());

                                foreach (DataRow dataRow in dataSet.Tables[0].Rows)
                                {
                                    // 1. ParentID 를 이용해서 기존에 Mapping Table 을 조회 한 후
                                    // 2. 조회 된 정보의 Count 에서 + 1 값이 ItemOrder 로 추가된다.
                                    var itemOrderDataSet =
                                        DataManager.GetInstance.GetDataForClient.GetFavoriteItemOrderForClient(
                                            "FavoriteGroupMapping",
                                            "FavoriteGroupID",
                                            Convert.ToInt32(row["ParentID"].ToString()),
                                            type.ToLower(),
                                            projectId,
                                            userId);

                                    itemOrder = itemOrderDataSet == null
                                                    ? 1
                                                    : Convert.ToInt32(itemOrderDataSet.Tables[0].Rows[0][0].ToString()) + 1;
                                    favoriteId = Convert.ToInt32(dataRow["FavoriteID"].ToString());

                                    DataManager.GetInstance.SetData.InsertFavoriteGroupMapping(
                                        Convert.ToInt32(row["ParentID"].ToString()), favoriteId, itemOrder);
                                }
                            }

                            var resultString = XmlConverter.XmlToAddFavoriteResponseData(
                                favoriteId.ToString(), itemOrder.ToString());

                            consolePrinter.PrintResponseMessage();

                            var bytes = Encoding.UTF8.GetBytes(resultString);

                            return new MemoryStream(bytes);
                        }
                        else
                        {
                            var favoriteGroupId = 0;
                            var itemOrder = 0;

                            foreach (DataRow row in favoriteDataTable.Rows)
                            {
                                // 1. ParentID 를 이용해서 FavoriteGroup Table 을 조회하여 ItemOrder Count 를 받아온다.
                                // 2. 조회 된 정보의 Count 에서 + 1 값이 ItemOrder 로 추가된다.
                                var itemOrderDataSet =
                                    DataManager.GetInstance.GetDataForClient.GetFavoriteItemOrderForClient(
                                        "FavoriteGroup",
                                        "ParentID",
                                        Convert.ToInt32(row["ParentID"].ToString()),
                                        type.ToLower(),
                                        projectId,
                                        userId);

                                if (itemOrderDataSet == null)
                                {
                                    itemOrder = 1;
                                }
                                else
                                {
                                    itemOrder = Convert.ToInt32(itemOrderDataSet.Tables[0].Rows[0][0].ToString()) + 1;
                                }

                                var favoriteGroupDataSet =
                                    DataManager.GetInstance.SetData.InsertFavoriteGroup(
                                        Convert.ToInt32(row["ParentID"].ToString()),
                                        row["FavoriteGroupName"].ToString(),
                                        projectId,
                                        itemOrder,
                                        userId,
                                        type.ToLower());

                                foreach (DataRow dataRow in favoriteGroupDataSet.Tables[0].Rows)
                                {
                                    favoriteGroupId = Convert.ToInt32(dataRow["FavoriteGroupID"].ToString());
                                }
                            }

                            var resultString = XmlConverter.XmlToAddFavoriteResponseData(
                                favoriteGroupId.ToString(), itemOrder.ToString());

                            consolePrinter.PrintResponseMessage();

                            var bytes = Encoding.UTF8.GetBytes(resultString);

                            return new MemoryStream(bytes);
                        }
                    }
                }
                catch (Exception ex)
                {
                    consolePrinter.PrintExceptionMessage(ex);
                    //Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                    return null;
                }
            }
        }

        /// <summary>
        /// The update favorite info.
        /// </summary>
        /// <param name="stream">The stream.</param>
        /// <param name="projectId">The project id.</param>
        /// <param name="userId">The user id.</param>
        /// <param name="type">The type.</param>
        /// <returns>The result value.</returns>
        public void UpdateFavoriteInfo(Stream stream, string projectId, string userId, string type)
        {
            using (var consolePrinter = new DataServiceMessage(projectId, userId, type))
            {
                try
                {
                    using (var readStream = new StreamReader(stream))
                    {
                        var favoriteDataTable = new DataTable();

                        favoriteDataTable.ReadXml(readStream);

                        foreach (DataRow row in favoriteDataTable.Rows)
                        {
                            if (row["IsFolder"].ToString().ToLower().Equals("true"))
                            {
                                // 폴더(FavoriteGroup) 수정
                                DataManager.GetInstance.SetData.UpdateFavoriteGroup(
                                    Convert.ToInt32(row["CurrentID"].ToString()),
                                    Convert.ToInt32(row["ParentID"].ToString()),
                                    row["CurrentName"].ToString(),
                                    projectId,
                                    Convert.ToInt32(row["ItemOrder"].ToString()),
                                    userId,
                                    type.ToLower());
                            }
                            else
                            {
                                // Favorite
                                DataManager.GetInstance.SetData.UpdateFavoriteGroupMapping(
                                    Convert.ToInt32(row["ParentID"].ToString()),
                                    Convert.ToInt32(row["CurrentID"].ToString()),
                                    Convert.ToInt32(row["ItemOrder"].ToString()));
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    //Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                    consolePrinter.PrintExceptionMessage(ex);
                }
            }
        }

        /// <summary>
        /// The update favorite data.
        /// </summary>
        /// <param name="stream">The stream.</param>
        /// <returns>The result value.</returns>
        public void UpdateFavorite(Stream stream, string projectId, string userId, string type)
        {
            using (var consolePrinter = new DataServiceMessage(projectId, userId, type))
            {
                try
                {
                    using (var readStream = new StreamReader(stream))
                    {
                        var favoriteDataTable = new DataTable();

                        favoriteDataTable.ReadXml(readStream);

                        foreach (DataRow row in favoriteDataTable.Rows)
                        {
                            DataManager.GetInstance.SetData.UpdateFavorite(
                                Convert.ToInt32(row["FavoriteID"].ToString()),
                                row["FavoriteName"].ToString(),
                                row["Data"].ToString(),
                                row["Type"].ToString().ToLower());
                        }
                    }
                }
                catch (Exception ex)
                {
                    //Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                    consolePrinter.PrintExceptionMessage(ex);
                }
            }
        }

        /// <summary>
        /// The delete favorite.
        /// </summary>
        /// <param name="stream">The stream.</param>
        public void DeleteFavorite(Stream stream, string projectId, string userId, string type)
        {
            using (var consolePrinter = new DataServiceMessage(projectId, userId, type))
            {
                try
                {
                    using (var readStream = new StreamReader(stream))
                    {
                        var favoriteDataTable = new DataTable();

                        favoriteDataTable.ReadXml(readStream);

                        foreach (DataRow row in favoriteDataTable.Rows)
                        {
                            DataManager.GetInstance.SetData.DeleteFavoriteGroupMappingByFavoriteId(
                                Convert.ToInt32(row["FavoriteID"].ToString()));
                            DataManager.GetInstance.SetData.DeleteFavorite(
                                Convert.ToInt32(row["FavoriteID"].ToString()), row["Type"].ToString().ToLower());
                        }
                    }
                }
                catch (Exception ex)
                {
                    //Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                    consolePrinter.PrintExceptionMessage(ex);
                }
            }
        }

        /// <summary>
        /// The delete favorite group.
        /// </summary>
        /// <param name="stream">The stream.</param>
        /// <param name="projectId">The project id.</param>
        /// <param name="userId">The user id.</param>
        /// <param name="type">The type.</param>
        public void DeleteFavoriteGroup(Stream stream, string projectId, string userId, string type)
        {
            using (var consolePrinter = new DataServiceMessage(projectId, userId, type))
            {
                try
                {
                    using (var readStream = new StreamReader(stream))
                    {
                        var favoriteDataTable = new DataTable();

                        favoriteDataTable.ReadXml(readStream);

                        foreach (DataRow row in favoriteDataTable.Rows)
                        {
                            DataManager.GetInstance.SetData.DeleteFavoriteGroupMappingByFavoriteGroupId(
                                Convert.ToInt32(row["FavoriteGroupID"].ToString()));
                            DataManager.GetInstance.SetData.DeleteFavoriteGroup(
                                Convert.ToInt32(row["FavoriteGroupID"].ToString()), projectId, userId, type.ToLower());
                        }

                        DataGarbageCollecter.Invoke(DeleteType.CommandFavorite);
                    }
                }
                catch (Exception ex)
                {
                    //Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                    consolePrinter.PrintExceptionMessage(ex);
                }
            }
        }

        #endregion // Favorite

        #region FavoriteSlide

        /// <summary>
        /// The add favorite slide.
        /// </summary>
        /// <param name="stream">
        /// The stream.
        /// </param>
        /// <param name="projectId">
        /// The project id.
        /// </param>
        /// <param name="userId">
        /// The user id.
        /// </param>
        /// <param name="type">
        /// The type.
        /// </param>
        /// <returns>
        /// The <see cref="Stream"/>.
        /// </returns>
        public Stream AddFavoriteSlide(Stream stream, string projectId, string userId, string type)
        {
            using (var consolePrinter = new DataServiceMessage(projectId, userId, type))
            {
                try
                {
                    using (var readStream = new StreamReader(stream))
                    {
                        var favoriteDataTable = new DataTable();

                        favoriteDataTable.ReadXml(readStream);

                        foreach (DataRow row in favoriteDataTable.Rows)
                        {
                            var resultString = DataManager.GetInstance.SetData.AddFavoriteSlide(
                                row["SlideName"].ToString(), row["SlideInfo"].ToString(), projectId, userId, type);

                            if (!string.IsNullOrWhiteSpace(resultString))
                            {
                                return CommonUtil.ResponseData(ResponseType.Failed, resultString);
                            }
                        }
                    }

                    return CommonUtil.ResponseData(ResponseType.Success);
                }
                catch (Exception ex)
                {
                    //Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                    consolePrinter.PrintExceptionMessage(ex);
                    return CommonUtil.ResponseData(ResponseType.Failed, ex.ToString());
                }
            }
        }

        public Stream UpdateFavoriteSlide(Stream stream, string projectId, string userId, string type)
        {
            using (var consolePrinter = new DataServiceMessage(projectId, userId, type))
            {
                try
                {
                    using (var readStream = new StreamReader(stream))
                    {
                        var favoriteDataTable = new DataTable();

                        favoriteDataTable.ReadXml(readStream);

                        foreach (DataRow row in favoriteDataTable.Rows)
                        {
                            var resultString = DataManager.GetInstance.SetData.UpdateFavoriteSlide(
                                row["SlideName"].ToString(), row["SlideInfo"].ToString(), projectId, userId, type);

                            if (!string.IsNullOrWhiteSpace(resultString))
                            {
                                return CommonUtil.ResponseData(ResponseType.Failed, resultString);
                            }
                        }
                    }

                    return CommonUtil.ResponseData(ResponseType.Success);
                }
                catch (Exception ex)
                {
                    //Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                    consolePrinter.PrintExceptionMessage(ex);
                    return CommonUtil.ResponseData(ResponseType.Failed, ex.ToString());
                }
            }
        }

        /// <summary>
        /// The delete favorite slide.
        /// </summary>
        /// <param name="projectId">
        /// The project id.
        /// </param>
        /// <param name="userId">
        /// The user id.
        /// </param>
        /// <param name="type">
        /// The type.
        /// </param>
        /// <param name="name">
        /// The name.
        /// </param>
        /// <returns>
        /// The <see cref="Stream"/>.
        /// </returns>
        public Stream DeleteFavoriteSlide(string projectId, string userId, string type, string name)
        {
            using (var consolePrinter = new DataServiceMessage(projectId, userId, type))
            {
                try
                {
                    var resultString = DataManager.GetInstance.SetData.DeleteFavoriteSlide(
                                projectId, userId, type, name);

                    return !string.IsNullOrWhiteSpace(resultString) ? CommonUtil.ResponseData(ResponseType.Failed, resultString) : CommonUtil.ResponseData(ResponseType.Success);
                }
                catch (Exception ex)
                {
                    //Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                    consolePrinter.PrintExceptionMessage(ex);
                    return CommonUtil.ResponseData(ResponseType.Failed, ex.ToString());
                }
            }
        }

        #endregion // FavoriteSlide

        #region FavoriteSchedule

        public Stream GetFavoriteSchedule(string projectId, string userId, string type)
        {
            using (var consolePrinter = new DataServiceMessage(projectId, userId, type))
            {
                var scheduleInfo = string.Empty;

                try
                {
                    // 저장 위치에 있는 xaml 문자열을 가져와서 Stream으로 return.
                    if (WebOperationContext.Current == null)
                    {
                        throw new Exception("Can not get current WebOpreationContext.");
                    }

                    // Content는 무조건 xml임.
                    WebOperationContext.Current.OutgoingResponse.ContentType = "application/xml";

                    scheduleInfo = DataManager.GetInstance.GetDataForClient.GetFavoriteSchedule(projectId, userId, type);
                    consolePrinter.PrintResponseMessage();

                    if (string.IsNullOrWhiteSpace(scheduleInfo))
                    {
                        throw new Exception("Get FavoriteSchedule Failed.");
                    }

                    var bytes = Encoding.UTF8.GetBytes(scheduleInfo);

                    return new MemoryStream(bytes);
                }
                catch (Exception ex)
                {
                    // 뭔가 잘못되면 다음과 같은 Element에 들어감.
                    scheduleInfo = string.Format("<Error>{0}</Error>", ex.Message);
                    consolePrinter.PrintExceptionMessage(ex);
                    consolePrinter.PrintMessage(scheduleInfo);
                    return null;
                }
            }
        }

        public Stream SetFavoriteSchedule(Stream stream, string projectId, string userId, string type)
        {
            using (var consolePrinter = new DataServiceMessage(projectId, userId, type))
            {
                var saveCompleted = string.Empty;

                try
                {
                    // 저장 위치에 있는 xaml 문자열을 가져와서 Stream으로 return.
                    if (WebOperationContext.Current == null)
                    {
                        throw new Exception("Can not get current WebOpreationContext.");
                    }

                    // Content는 무조건 xml임.
                    WebOperationContext.Current.OutgoingResponse.ContentType = "application/xml";

                    using (var reader = new StreamReader(stream))
                    {
                        var info = reader.ReadToEnd();

                        if (string.IsNullOrWhiteSpace(info))
                        {
                            throw new Exception("Receive empty FavoriteSchedule.");
                        }

                        saveCompleted = DataManager.GetInstance.SetData.AddOrUpdateFavoriteSchedule(projectId, userId, type, info);
                        if (!string.IsNullOrWhiteSpace(saveCompleted))
                        {
                            throw new Exception(saveCompleted);
                        }

                        consolePrinter.PrintResponseMessage();
                        return CommonUtil.ResponseData(ResponseType.Success);
                    }
                }
                catch (Exception ex)
                {
                    // 뭔가 잘못되면 다음과 같은 Element에 들어감.
                    saveCompleted = string.Format("<Error>{0}</Error>", ex.Message);
                    consolePrinter.PrintExceptionMessage(ex);
                    consolePrinter.PrintMessage(saveCompleted);
                    return CommonUtil.ResponseData(ResponseType.Failed, ex.Message);
                }
            }
        }

        #endregion

        #region CameraGroup

        /// <summary>
        /// The add camera group info.
        /// </summary>
        /// <param name="stream">The stream.</param>
        /// <param name="projectId">The project id.</param>
        /// <param name="userId">The user id.</param>
        /// <param name="type">The type.</param>
        /// <returns>The result value.</returns>
        public Stream AddCameraGroupInfo(Stream stream, string projectId, string userId, string type)
        {
            using (var consolePrinter = new DataServiceMessage(projectId, userId, type))
            {
                try
                {
                    DataManager.GetInstance.SetData.DeleteCameraGroup(projectId, userId, type);

                    var cameraGroupIdDictionary = new Dictionary<int, int>();

                    if (WebOperationContext.Current == null)
                    {
                        throw new Exception("Can not get current WebOpreationContext.");
                    }

                    // Content는 무조건 xml임.
                    WebOperationContext.Current.OutgoingResponse.ContentType = "application/xml";

                    var cameraGroupDataTable = SetDataTableToXmlData(stream);
                    var cameraGroupDataSet = SortCameraGroupDataSet(cameraGroupDataTable);

                    for (var i = 0; i < cameraGroupDataSet.Tables.Count; i++)
                    {
                        foreach (DataRow row in cameraGroupDataSet.Tables[i].Rows)
                        {
                            switch (i)
                            {
                                // CameraGroup 저장 시 CameraGroupID @@IDENTITY 값을 받아온다.
                                // Client CameraGroupID 와 Database CameraGroupID 를 Dictionary<int, int>로 관리한다.
                                // Key = Client CameraGroupID, Value = Database CameraGroupID
                                case 0:
                                    {
                                        var identityDataTable =
                                            DataManager.GetInstance.SetData.InsertCameraGroup(
                                                int.Parse(row["ParentID"].ToString()),
                                                row["CameraGroupName"].ToString(),
                                                projectId,
                                                int.Parse(row["ItemOrder"].ToString()),
                                                userId.ToLower(),
                                                type.ToLower());

                                        var cameraGroupId = int.Parse(identityDataTable.Rows[0]["CameraGroupID"].ToString());

                                        cameraGroupIdDictionary.Add(
                                            Convert.ToInt32(row["CameraGroupID"].ToString()), cameraGroupId);

                                        break;
                                    }

                                case 1:
                                    {
                                        var key = int.Parse(row["ParentID"].ToString());

                                        if (cameraGroupIdDictionary.ContainsKey(key))
                                        {
                                            var identityDataTable =
                                                DataManager.GetInstance.SetData.InsertCameraGroup(
                                                    cameraGroupIdDictionary[key],
                                                    row["CameraGroupName"].ToString(),
                                                    projectId,
                                                    int.Parse(row["ItemOrder"].ToString()),
                                                    userId.ToLower(),
                                                    type.ToLower());

                                            var cameraGroupId = int.Parse(identityDataTable.Rows[0]["CameraGroupID"].ToString());

                                            cameraGroupIdDictionary.Add(
                                                Convert.ToInt32(row["CameraGroupID"].ToString()), cameraGroupId);
                                        }

                                        break;
                                    }

                                case 2:
                                    {
                                        var key = Int32.Parse(row["ParentID"].ToString());

                                        var projectMappingTable =
                                            DataManager.GetInstance.SetData.SelectProjectMappingWithoutProjectIdColumn(
                                                projectId).Tables[0];

                                        if (cameraGroupIdDictionary.ContainsKey(key))
                                        {
                                            foreach (
                                                var dataRow in
                                                    projectMappingTable.Rows.Cast<DataRow>().Where(
                                                        dataRow =>
                                                        dataRow["ProjectCameraID"].ToString().Equals(
                                                            row["CameraID"].ToString())))
                                            {
                                                DataManager.GetInstance.SetData.InsertCameraGroupMapping(
                                                    cameraGroupIdDictionary[key],
                                                    dataRow["CameraID"].ToString(),
                                                    Int32.Parse(row["ItemOrder"].ToString()));

                                                break;
                                            }
                                        }

                                        break;
                                    }
                            }
                        }
                    }

                    // 삭제된 CameraGroup 정리 작업.
                    DataGarbageCollecter.Invoke(DeleteType.Camera);

                    var resultString = XmlConverter.XmlToAddCameraGroupResponseData("Success");
                    consolePrinter.PrintResponseMessage(resultString);
                    var bytes = Encoding.UTF8.GetBytes(resultString);

                    return new MemoryStream(bytes);
                }
                catch (Exception ex)
                {
                    Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);

                    var resultString = XmlConverter.XmlToAddCameraGroupResponseData("Failed");
                    consolePrinter.PrintMessage(resultString);
                    consolePrinter.PrintExceptionMessage(ex);
                    var bytes = Encoding.UTF8.GetBytes(resultString);

                    return new MemoryStream(bytes);
                }
            }
        }

        #region CameraGroup Methods

        private static DataTable SetDataTableToXmlData(Stream stream)
        {
            using (var readStream = new StreamReader(stream))
            {
                var xmlDocument = new XmlDocument();

                xmlDocument.LoadXml(readStream.ReadToEnd());

                var cameraGroupDataTable = SetCameraGroupDataTable("CameraGroupTable");

                var elementList = xmlDocument.GetElementsByTagName("CameraGroupInfo");

                for (var i = 0; i < elementList.Count; i++)
                {
                    var xmlAttributeCollection = elementList[i].Attributes;

                    if (xmlAttributeCollection == null)
                    {
                        continue;
                    }

                    var newRow = cameraGroupDataTable.NewRow();
                    newRow["IsFolder"] = xmlAttributeCollection["IsFolder"].Value.ToLower();
                    newRow["CameraGroupID"] = xmlAttributeCollection["ID"].Value;
                    newRow["CameraGroupName"] = xmlAttributeCollection["CameraGroupName"].Value;
                    newRow["ParentID"] = xmlAttributeCollection["ParentID"].Value;
                    newRow["ItemOrder"] = xmlAttributeCollection["ItemOrder"].Value;
                    newRow["CameraID"] = xmlAttributeCollection["CameraID"].Value;

                    cameraGroupDataTable.Rows.Add(newRow);
                }
                return cameraGroupDataTable;
            }
        }

        /// <summary>
        /// 카메라 그룹 정보를 Parent Folder / Child Folder / Mapping 정보로 분류하고
        /// 각각의 DataTable 에 저장하여 하나의 DataSet 으로 반환한다.
        /// </summary>
        /// <param name="dataTable">XML 정보를 DataSet으로 만든 정렬되지 않은 DataTable</param>
        /// <returns>정렬 된 DataSet.</returns>
        private static DataSet SortCameraGroupDataSet(DataTable dataTable)
        {
            var dataSet = new DataSet("CameraGroup");

            var parentDataTable = new DataTable("RootFolderTable");
            parentDataTable = FolderTableColumnSetting(parentDataTable);

            var childeDataTable = new DataTable("ChildFolderTable");
            childeDataTable = FolderTableColumnSetting(childeDataTable);

            var cameraGroupMappingTable = new DataTable("CameraGroupMappingTable");
            CameraMappingTableColumnSetting(cameraGroupMappingTable);

            foreach (DataRow cameraGroupRow in dataTable.Rows)
            {
                // true : CameraGroup(Folder)
                if (Convert.ToBoolean(cameraGroupRow["IsFolder"].ToString()))
                {
                    // Parent 와 Child 분리.(-1 이면 Parent)
                    if (cameraGroupRow["ParentID"].ToString().Equals("-1"))
                    {
                        var newRow = SetFolderRow(cameraGroupRow, parentDataTable);
                        parentDataTable.Rows.Add(newRow);
                    }
                    else
                    {
                        var newRow = SetFolderRow(cameraGroupRow, childeDataTable);
                        childeDataTable.Rows.Add(newRow);
                    }
                }
                else
                {
                    var newRow = cameraGroupMappingTable.NewRow();
                    newRow["ParentID"] = cameraGroupRow["ParentID"].ToString();
                    newRow["CameraID"] = cameraGroupRow["CameraID"].ToString();
                    newRow["ItemOrder"] = cameraGroupRow["ItemOrder"].ToString();

                    cameraGroupMappingTable.Rows.Add(newRow);
                }
            }

            dataSet.Tables.Add(parentDataTable);
            dataSet.Tables.Add(childeDataTable);
            dataSet.Tables.Add(cameraGroupMappingTable);

            return dataSet;
        }

        private static void CameraMappingTableColumnSetting(DataTable cameraGroupMappingTable)
        {
            var columnParentId = new DataColumn("ParentID", typeof(string));
            var columnCameraId = new DataColumn("CameraID", typeof(string));
            var columnItemOrder = new DataColumn("ItemOrder", typeof(string));

            cameraGroupMappingTable.Columns.Add(columnParentId);
            cameraGroupMappingTable.Columns.Add(columnCameraId);
            cameraGroupMappingTable.Columns.Add(columnItemOrder);
        }

        private static DataRow SetFolderRow(DataRow row, DataTable newTable)
        {
            var newRow = newTable.NewRow();
            newRow["IsFolder"] = row["IsFolder"].ToString().ToLower();
            newRow["CameraGroupID"] = row["CameraGroupID"].ToString();
            newRow["CameraGroupName"] = row["CameraGroupName"].ToString();
            newRow["ParentID"] = row["ParentID"].ToString();
            newRow["ItemOrder"] = row["ItemOrder"].ToString();

            return newRow;
        }

        private static DataTable SetCameraGroupDataTable(string tableName)
        {
            var dataTable = new DataTable(tableName);
            var columnIsFolder = new DataColumn("IsFolder", typeof(string));
            var columnCameraGroupId = new DataColumn("CameraGroupID", typeof(string));
            var columnsCameraGroupName = new DataColumn("CameraGroupName", typeof(string));
            var columnsParentId = new DataColumn("ParentID", typeof(string));
            var columnsItemOrder = new DataColumn("ItemOrder", typeof(string));
            var columnsCameraId = new DataColumn("CameraID", typeof(string));

            dataTable.Columns.Add(columnIsFolder);
            dataTable.Columns.Add(columnCameraGroupId);
            dataTable.Columns.Add(columnsCameraGroupName);
            dataTable.Columns.Add(columnsParentId);
            dataTable.Columns.Add(columnsItemOrder);
            dataTable.Columns.Add(columnsCameraId);

            return dataTable;
        }

        private static DataTable FolderTableColumnSetting(DataTable dataTable)
        {
            var columnIsFolder = new DataColumn("IsFolder", typeof(string));
            var columnCameraGroupId = new DataColumn("CameraGroupID", typeof(string));
            var columnsCameraGroupName = new DataColumn("CameraGroupName", typeof(string));
            var columnsParentId = new DataColumn("ParentID", typeof(string));
            var columnsItemOrder = new DataColumn("ItemOrder", typeof(string));

            dataTable.Columns.Add(columnIsFolder);
            dataTable.Columns.Add(columnCameraGroupId);
            dataTable.Columns.Add(columnsCameraGroupName);
            dataTable.Columns.Add(columnsParentId);
            dataTable.Columns.Add(columnsItemOrder);

            return dataTable;
        }

        #endregion // CameraGroup Methods

        #endregion // CameraGroup

        #region MapLayout

        public Stream SetMapLayout(Stream stream, string projectId, string userId, string type)
        {
            using (var consolePrinter = new DataServiceMessage(projectId, userId, type))
            {
                // 전달받은 Stream을 저장 위치에 문자열로 저장.
                var saveResultData = string.Empty;
                try
                {
                    // 저장 위치에 있는 xaml 문자열을 가져와서 Stream으로 return.
                    if (WebOperationContext.Current == null)
                    {
                        throw new Exception("Can not get current WebOpreationContext.");
                    }

                    // Content는 무조건 xml임.
                    WebOperationContext.Current.OutgoingResponse.ContentType = "application/xml";

                    using (var readStream = new StreamReader(stream))
                    {
                        var data = readStream.ReadToEnd();
                        DataManager.GetInstance.SetData.SetMapLayout(projectId, data);
                    }

                    consolePrinter.PrintResponseMessage();
                }
                catch (Exception ex)
                {
                    // 뭔가 잘못되면 다음과 같은 Element에 들어감.
                    saveResultData = string.Format("<Error>{0}</Error>", ex.Message);
                    //Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                    consolePrinter.PrintExceptionMessage(ex);
                    consolePrinter.PrintMessage(saveResultData);
                }

                var bytes = Encoding.UTF8.GetBytes(saveResultData);

                return new MemoryStream(bytes);
            }
        }

        #endregion // MapLayout

        #region LocationBookmark

        public Stream SetLocationBookmark(Stream stream, string projectId, string userId, string type)
        {
            using (var consolePrinter = new DataServiceMessage(projectId, userId, type))
            {
                // 전달받은 Stream을 저장 위치에 문자열로 저장.
                var saveResultData = string.Empty;
                try
                {
                    // 저장 위치에 있는 xaml 문자열을 가져와서 Stream으로 return.
                    if (WebOperationContext.Current == null)
                    {
                        throw new Exception("Can not get current WebOpreationContext.");
                    }

                    // Content는 무조건 xml임.
                    WebOperationContext.Current.OutgoingResponse.ContentType = "application/xml";

                    using (var readStream = new StreamReader(stream))
                    {
                        var data = readStream.ReadToEnd();
                        DataManager.GetInstance.SetData.SetLocationBookmark(data, projectId, userId);
                    }

                    consolePrinter.PrintResponseMessage();
                }
                catch (Exception ex)
                {
                    // 뭔가 잘못되면 다음과 같은 Element에 들어감.
                    saveResultData = string.Format("<Error>{0}</Error>", ex.Message);
                    //Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                    consolePrinter.PrintExceptionMessage(ex);
                    consolePrinter.PrintMessage(saveResultData);
                }

                var bytes = Encoding.UTF8.GetBytes(saveResultData);

                return new MemoryStream(bytes);
            }
        }

        #endregion // LocationBookmark

        #endregion // SetData

        #endregion // Command

        //------------------------------------------------------------------------------------------------------------------------//

        #region Playback

        #region GetData

        /// <summary>
        /// The get video information for playback.
        /// </summary>
        /// <param name="projectId">
        /// The project id.
        /// </param>
        /// <returns>
        /// The memory stream of video information for playback data.
        /// </returns>
        public Stream GetVideoInformationForPlayback(string projectId)
        {
            try
            {
                if (WebOperationContext.Current == null)
                {
                    throw new Exception("Can not get current WebOpreationContext.");
                }

                // Content는 무조건 xml임.
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/xml";

                var cameraInfo = DataManager.GetInstance.GetDataForClient.GetCameraInfo(projectId);

                var resolutionInfo = DataManager.GetInstance.GetDataForClient.GetResolutionInfo(projectId);

                var mediaInfo = DataManager.GetInstance.GetDataForClient.GetMergeInformation();

                var createData = new CreateToPlaybackVideoInformationData(cameraInfo, resolutionInfo, mediaInfo);

                var resultString = createData.CreatePlaybackVideoInformation();

                var bytes = Encoding.UTF8.GetBytes(resultString);

                return new MemoryStream(bytes);
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return null;
            }
        }

        /// <summary>
        /// The get timeline for playback.
        /// </summary>
        /// <param name="stream">
        /// The stream.
        /// </param>
        /// <returns>
        /// The The memory stream of timeline for playback..
        /// </returns>
        /// <exception cref="Exception">
        /// The exception.
        /// </exception>
        public Stream GetTimelineForPlayback(Stream stream)
        {
            try
            {
                if (WebOperationContext.Current == null)
                {
                    throw new Exception("Can not get current WebOpreationContext.");
                }

                // Content는 무조건 xml임.
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/xml";

                InnotiveDebug.Trace("Get Timeline Request Data Start");

                var timelineRequestData = this.GetTimelineRequestData(stream);

                InnotiveDebug.Trace("Get Timeline Request Data End");

                InnotiveDebug.Trace("Record Media Date Query Start");

                // TODO : RC에 Data를 요청 / 받은 데이터를 큐에 저장하여 반환
                var recordMediaDatas = this.TimelineDataRequestOfRecorder(timelineRequestData);

                InnotiveDebug.Trace("Record Media Date Query End");

                // TODO : 각 RC로 부터 받은 Data를 취합해서 PB로 전송. 
                var responseTimeLineData = new CreateToTimelineResponseData(
                    timelineRequestData.RecordCameraInfoList.RecordCameraInfo, recordMediaDatas, timelineRequestData.RecordIntervalSeconds);

                var resultString = responseTimeLineData.CreateResponseData();
                Console.WriteLine("GetTimelineForPlayback : " + resultString);

                var bytes = Encoding.UTF8.GetBytes(resultString);

                return new MemoryStream(bytes);
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return null;
            }
        }

        /// <summary>
        /// The timeline data request of recorder.
        /// 각 레코더로 Timeline Data 를 요청한다.
        /// </summary>
        /// <param name="requestData">
        /// The timeline request data.
        /// Playback 에서 요청한 Timeline 정보.
        /// </param>
        /// <returns>
        /// The System.Data.DataSet.
        /// </returns>
        private DataSet TimelineDataRequestOfRecorder(TimelineRequestData requestData)
        {
            var dataSet = new DataSet();
            var queue = new Queue();
            var restPort = new DataSet();

            var bytes = Encoding.UTF8.GetBytes(DataManager.GetInstance.GetDataForConfiguration.GetRecorderInfo());
            restPort.ReadXml(new MemoryStream(bytes));

            this.port = restPort.Tables[0].Rows[0]["RESTServerPort"].ToString();

            var requestDataSet = this.CreateNewRequestDataSet(requestData, dataSet, 0);
            foreach (DataTable data in requestDataSet.Tables)
            {
                this.dataTable = data;

                var resultStream = this.RequestRecordIndex(requestData, this.dataTable, this.port);
                var responseDataSet = this.SetDataTable(resultStream);
                if (responseDataSet == null)
                    continue;

                foreach (var table in responseDataSet.Tables)
                {
                    queue.Enqueue(table);
                }
            }

            var resultDataSet = new DataSet();
            foreach (DataTable queueData in queue)
            {
                resultDataSet.Tables.Add(queueData.Copy());
            }


            var cameraNumberList =
                (from recordCamera in requestData.RecordCameraInfoList.RecordCameraInfo
                 select recordCamera.RecordCameraNumber).ToList();

            var eventDataTable =
                DataManager.GetInstance.GetDataForClient.GetEventRecordDateInfo(
                    requestData.BeginDate,
                    requestData.EndDate,
                    cameraNumberList,
                    requestData.EventIntervalSeconds);

            resultDataSet.Tables.Add(eventDataTable);

            return resultDataSet;
        }

        private Stream RequestRecordIndex(TimelineRequestData requestData, DataTable data, string servicePort)
        {
            try
            {
                var outerXml = this.CreateRequestXmlData(requestData, data);

                var param = new RequestParameter()
                {
                    Url = string.Format(@"http://{0}:{1}/rest/data/getrecordindexprocess", data.TableName, servicePort),
                    EncodingOption = "UTF8",
                    PostMessage = outerXml
                };

                return DataServiceHandler.RequestToStreamResponse(param, true, true);
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);

                return null;
            }
        }

        /// <summary>
        /// RC에서 받은 데이터를 DataTable 로 변환
        /// </summary>
        /// <param name="response"></param>
        /// <returns>
        /// DataTable
        /// </returns>
        private DataSet SetDataTable(Stream response)
        {
            try
            {
                var dataSet = new DataSet();
                dataSet.ReadXml(response);

                int? recordCameraNumber = null;

                var responseData = new DataSet();
                var data = new DataTable();

                for (var i = 0; i < dataSet.Tables.Count; i++)
                {
                    if (!int.Parse(dataSet.Tables[i].Rows[0]["RecordCameraNumber"].ToString()).Equals(recordCameraNumber))
                    {
                        var newDataTable = new DataTable
                        {
                            TableName = string.Format("RecordCamera{0}DateDataTable", dataSet.Tables[i].Rows[0]["RecordCameraNumber"])
                        };

                        var columnRecordCameraNumner = new DataColumn("RecordCameraNumber", typeof(int));
                        var columnBeginDate = new DataColumn("BeginDate", typeof(long));
                        var columnEndDate = new DataColumn("EndDate", typeof(long));

                        newDataTable.Columns.Add(columnRecordCameraNumner);
                        newDataTable.Columns.Add(columnBeginDate);
                        newDataTable.Columns.Add(columnEndDate);

                        newDataTable.Rows.Add(this.SetDataRow(newDataTable, dataSet.Tables[i].Rows[0]));

                        recordCameraNumber = int.Parse(dataSet.Tables[i].Rows[0]["RecordCameraNumber"].ToString());
                        data = newDataTable.Copy();
                        responseData.Tables.Add(data);
                        continue;
                    }

                    data.Rows.Add(SetDataRow(data, dataSet.Tables[i].Rows[0]));
                    dataSet.Tables.Add(data);
                }

                return dataSet;
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return null;
            }
        }

        private DataRow SetDataRow(DataTable newDataTable, DataRow dataRow)
        {
            var newRow = newDataTable.NewRow();

            newRow["RecordCameraNumber"] = int.Parse(dataRow["RecordCameraNumber"].ToString());
            newRow["BeginDate"] = long.Parse(dataRow["BeginDate"].ToString());
            newRow["EndDate"] = long.Parse(dataRow["EndDate"].ToString());

            return newRow;
        }

        /// <summary>
        /// The create request xml data.
        /// Timeline 정보를 Recorder 에 요청하기 위한 xml 데이터 생성.
        /// </summary>
        /// <param name="requestData">
        /// The timeline Request Data.
        /// </param>
        /// <param name="data">
        /// The datatable.
        /// </param>
        /// <returns>
        /// The request timeline xml data.
        /// </returns>
        private string CreateRequestXmlData(TimelineRequestData requestData, DataTable data)
        {
            var xmlDocument = new XmlDocument();

            var xmlDec = xmlDocument.CreateXmlDeclaration("1.0", "utf-8", null);
            xmlDocument.AppendChild(xmlDec);

            var root = xmlDocument.CreateElement("TimelineRequestData");
            xmlDocument.AppendChild(root);

            var beginDate = xmlDocument.CreateElement("BeginDate");
            beginDate.InnerText = requestData.BeginDate;
            root.AppendChild(beginDate);

            var endDate = xmlDocument.CreateElement("EndDate");
            endDate.InnerText = requestData.EndDate;
            root.AppendChild(endDate);

            var recordIntervalSeconds = xmlDocument.CreateElement("RecordIntervalSeconds");
            recordIntervalSeconds.InnerText = requestData.RecordIntervalSeconds;
            root.AppendChild(recordIntervalSeconds);

            var recordCameraInfoList = xmlDocument.CreateElement("RecordCameraInfoList");
            root.AppendChild(recordCameraInfoList);

            foreach (DataRow dataRow in data.Rows)
            {
                var recordCameraInfo = xmlDocument.CreateElement("RecordCameraInfo");
                recordCameraInfoList.AppendChild(recordCameraInfo);

                var recordCameraNumber = xmlDocument.CreateAttribute("RecordCameraNumber");
                recordCameraNumber.InnerText = dataRow["RecordCameraNumber"].ToString();
                recordCameraInfo.Attributes.Append(recordCameraNumber);
            }

            return xmlDocument.OuterXml;
        }

        private DataSet CreateNewRequestDataSet(TimelineRequestData requestData, DataSet dataSet, int count)
        {
            foreach (var recordCameraInfo in requestData.RecordCameraInfoList.RecordCameraInfo)
            {
                if (count.Equals(0))
                {
                    var data = this.CreateNewRequestDataTable(recordCameraInfo);

                    dataSet.Tables.Add(data);

                    count++;
                }
                else
                {
                    var comparedDataTable = this.ComparedRecorderIP(recordCameraInfo, dataSet);

                    // TODO : 동일한 RecorderIP 검색
                    if (comparedDataTable != null)
                    {
                        var newRow = comparedDataTable.NewRow();

                        newRow["RecordCameraNumber"] = recordCameraInfo.RecordCameraNumber;
                        newRow["RecorderIP"] = recordCameraInfo.RecorderIP;

                        comparedDataTable.Rows.Add(newRow);

                        count++;
                    }
                    else
                    {
                        var dataTable = this.CreateNewRequestDataTable(recordCameraInfo);

                        dataSet.Tables.Add(dataTable);

                        count++;
                    }
                }
            }

            return dataSet;
        }

        private DataTable ComparedRecorderIP(RecordCameraInfo recordCameraInfo, DataSet dataSet)
        {
            return dataSet.Tables.Cast<DataTable>().FirstOrDefault(dataTable => dataTable.TableName.Equals(recordCameraInfo.RecorderIP));
        }

        private DataTable CreateNewRequestDataTable(RecordCameraInfo item)
        {
            var dataTable = new DataTable(item.RecorderIP);

            var columnRecordCameraNumber = new DataColumn("RecordCameraNumber", typeof(string));
            var columnRecorderIp = new DataColumn("RecorderIP", typeof(string));
            dataTable.Columns.Add(columnRecordCameraNumber);
            dataTable.Columns.Add(columnRecorderIp);

            var newRow = dataTable.NewRow();

            newRow["RecordCameraNumber"] = item.RecordCameraNumber;
            newRow["RecorderIP"] = item.RecorderIP;

            dataTable.Rows.Add(newRow);

            return dataTable;
        }

        /// <summary>
        /// The get search record time for playback.
        /// </summary>
        /// <param name="stream">
        /// The stream.
        /// </param>
        /// <returns>
        /// The response data of search record time.
        /// </returns>
        public Stream GetSearchRecordTimeForPlayback(Stream stream)
        {
            if (WebOperationContext.Current == null)
            {
                throw new Exception("Can not get current WebOpreationContext.");
            }

            // Content는 무조건 xml임.
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/xml";

            try
            {
                // TODO : RecordMediaIndex 정보를 받기위한 RC로 데이터 요청.
                var searchRecordTimeRequestData = this.GetSearchRecordTimeRequestData(stream);

                InnotiveDebug.Trace("Search Frame Request Data Start");

                var searchFrameData = this.SearchFrameDataRequestOfRecorder(searchRecordTimeRequestData);

                InnotiveDebug.Trace("Search Frame Request Data End");

                var resultString = CommonUtil.XmlToDataSet(searchFrameData);

                var bytes = Encoding.UTF8.GetBytes(resultString);

                return new MemoryStream(bytes);
            }
            catch (Exception)
            {
                return null;
            }
        }

        /// <summary>
        /// The get recording date info.
        /// </summary>
        /// <param name="stream">
        /// The stream.
        /// </param>
        /// <returns>
        /// The <see cref="Stream"/>.
        /// </returns>
        /// <exception cref="Exception">
        /// </exception>
        public Stream GetRecordingDateInfo(Stream stream)
        {
            if (WebOperationContext.Current == null)
            {
                throw new Exception("Can not get current WebOpreationContext.");
            }

            // Content는 무조건 xml임.
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/xml";

            try
            {
                InnotiveDebug.Trace("Recording Date Info Start");

                var getRecordingCalendarDateRequestData = this.GetRecordingCalendarDateRequestData(stream);

                InnotiveDebug.Trace("Recording Date Info End");

                if (string.IsNullOrWhiteSpace(getRecordingCalendarDateRequestData))
                {
                    return null;
                }

                var bytes = Encoding.UTF8.GetBytes(getRecordingCalendarDateRequestData);

                return new MemoryStream(bytes);
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.ToString());
                return null;
            }
        }

        private DataSet SearchFrameDataRequestOfRecorder(SearchRecordTimeRequestData searchRecordTimeRequestData)
        {
            var dataSet = new DataSet();
            var queue = new Queue();
            var restPort = new DataSet();

            var bytes = Encoding.UTF8.GetBytes(DataManager.GetInstance.GetDataForConfiguration.GetRecorderInfo());
            restPort.ReadXml(new MemoryStream(bytes));

            this.port = restPort.Tables[0].Rows[0]["RESTServerPort"].ToString();

            var requestDataSet = this.CreateNewSearchFrameRequestData(searchRecordTimeRequestData, dataSet, 0);

            foreach (DataTable data in requestDataSet.Tables)
            {
                this.dataTable = data;

                var resultStream = this.SearchFrameRequestRecordIndex(searchRecordTimeRequestData, this.dataTable, this.port);

                var responseDataSet = new DataSet();
                responseDataSet.ReadXml(resultStream);

                foreach (var table in responseDataSet.Tables)
                {
                    queue.Enqueue(table);
                }
            }

            var resultDataSet = new DataSet();
            foreach (DataTable queueData in queue)
            {
                resultDataSet.Tables.Add(queueData.Copy());
            }

            return resultDataSet;
        }

        private Stream SearchFrameRequestRecordIndex(SearchRecordTimeRequestData searchRecordTimeRequestData, DataTable data, string servicePort)
        {
            try
            {
                var outerXml = this.CreateSearchFrameRequestXmlData(searchRecordTimeRequestData, data);

                var param = new RequestParameter()
                {
                    Url = string.Format(@"http://{0}:{1}/rest/data/recorderframesearchprocess", data.TableName, servicePort),
                    EncodingOption = "UTF8",
                    PostMessage = outerXml
                };

                return DataServiceHandler.RequestToStreamResponse(param, true, true);
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);

                return null;
            }
        }

        private DataSet CreateNewSearchFrameRequestData(SearchRecordTimeRequestData searchRecordTimeRequestData, DataSet dataSet, int count)
        {
            foreach (var recordCameraInfo in searchRecordTimeRequestData.SearchRecordCameraInfoList.RecordCameraInfo)
            {
                if (count.Equals(0))
                {
                    var data = this.CreateNewRequestDataTable(recordCameraInfo);

                    dataSet.Tables.Add(data);

                    count++;
                }
                else
                {
                    var comparedDataTable = this.ComparedRecorderIP(recordCameraInfo, dataSet);

                    // TODO : 동일한 RecorderIP 검색
                    if (comparedDataTable != null)
                    {
                        var newRow = comparedDataTable.NewRow();

                        newRow["RecordCameraNumber"] = recordCameraInfo.RecordCameraNumber;
                        newRow["RecorderIP"] = recordCameraInfo.RecorderIP;

                        comparedDataTable.Rows.Add(newRow);

                        count++;
                    }
                    else
                    {
                        var dataTable = this.CreateNewRequestDataTable(recordCameraInfo);

                        dataSet.Tables.Add(dataTable);

                        count++;
                    }
                }
            }

            return dataSet;
        }

        private string CreateSearchFrameRequestXmlData(SearchRecordTimeRequestData recorderInfo, DataTable data)
        {
            var xmlDoc = new XmlDocument();

            var xmlDec = xmlDoc.CreateXmlDeclaration("1.0", "utf-8", null);
            xmlDoc.AppendChild(xmlDec);

            var root = xmlDoc.CreateElement("SearchFrameTimeData");
            xmlDoc.AppendChild(root);

            var elementRecordCameraNumbers = xmlDoc.CreateElement("RecordCameraNumbers");
            root.AppendChild(elementRecordCameraNumbers);

            foreach (DataRow dataRow in data.Rows)
            {
                var elementRecordCameraNumber = xmlDoc.CreateElement("RecordCameraNumber");
                elementRecordCameraNumber.InnerText = dataRow["RecordCameraNumber"].ToString();
                elementRecordCameraNumbers.AppendChild(elementRecordCameraNumber);
            }

            var elementMergeIntervalSeconds = xmlDoc.CreateElement("MergeIntervalSeconds");
            elementMergeIntervalSeconds.InnerText = recorderInfo.MergeIntervalSeconds;
            root.AppendChild(elementMergeIntervalSeconds);

            return xmlDoc.OuterXml;
        }

        private TimelineRequestData GetTimelineRequestData(Stream stream)
        {
            TimelineRequestData timelineRequestData = null;

            using (var readStream = new StreamReader(stream))
            {
                var xmlDocument = new XmlDocument();
                xmlDocument.Load(readStream);

                timelineRequestData = this.ReadDataFromXmlForTimelineRequestData(xmlDocument.OuterXml);
            }

            return timelineRequestData;
        }

        private TimelineRequestData ReadDataFromXmlForTimelineRequestData(string xmlData)
        {
            var serializer = new XmlSerializer(typeof(TimelineRequestData));
            var stringReader = new StringReader(xmlData);
            var xmlReader = new XmlTextReader(stringReader);

            var data = serializer.Deserialize(xmlReader) as TimelineRequestData;

            xmlReader.Close();
            stringReader.Close();

            return data;
        }

        private SearchRecordTimeRequestData GetSearchRecordTimeRequestData(Stream stream)
        {
            SearchRecordTimeRequestData searchRecordTimeRequestData = null;

            using (var readStream = new StreamReader(stream))
            {
                var xmlDocument = new XmlDocument();
                xmlDocument.Load(readStream);

                searchRecordTimeRequestData = this.ReadDataFromXmlForSearchRecordTimeRequestData(xmlDocument.OuterXml);
            }

            return searchRecordTimeRequestData;
        }

        private string GetRecordingCalendarDateRequestData(Stream stream)
        {
            using (var readStream = new StreamReader(stream))
            {
                var xmlDocument = new XmlDocument();
                xmlDocument.Load(readStream);

                var elementList = xmlDocument.GetElementsByTagName("Camera");

                var xmlAttributeCollection = elementList[0].Attributes;

                if (xmlAttributeCollection == null)
                {
                    Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), "RecordCameraNumber is null");

                    return CommonUtil.ResponseData(ResponseType.Failed, "RecordCameraNumber is null").ToString();
                }
                else
                {
                    var restPort = new DataSet();

                    var bytes = Encoding.UTF8.GetBytes(
                        DataManager.GetInstance.GetDataForConfiguration.GetRecorderInfo());
                    restPort.ReadXml(new MemoryStream(bytes));

                    var rcPort = restPort.Tables[0].Rows[0]["RESTServerPort"].ToString();

                    var cameraNumber = xmlAttributeCollection["Number"].Value;
                    var ip = xmlAttributeCollection["IP"].Value;
                    var beginDate = xmlAttributeCollection["BeginDate"].Value;
                    var endDate = xmlAttributeCollection["EndDate"].Value;

                    var param = new RequestParameter()
                        {
                            Url =
                                string.Format(
                                    @"http://{0}:{1}/rest/data/getrecordingdateinfoprocess?recordcameranumber={2}&begindate={3}&enddate={4}",
                                    ip,
                                    rcPort,
                                    cameraNumber,
                                    beginDate,
                                    endDate),
                            EncodingOption = "UTF8",
                            PostMessage = string.Empty
                        };

                    var responseData = DataServiceHandler.Request(param, false);

                    if (responseData == null)
                    {
                        return string.Empty;
                    }

                    var resultString = this.SetEventRercordingCalendarData(xmlAttributeCollection, DataServiceHandler.Request(param, false));

                    return resultString;
                }
            }
        }

        private string SetEventRercordingCalendarData(XmlAttributeCollection requestData, string getRecordingCalendarDateRequestData)
        {
            var getRecordingCalendarData = new XmlDocument();
            getRecordingCalendarData.LoadXml(getRecordingCalendarDateRequestData);

            var elementList = getRecordingCalendarData.GetElementsByTagName("RecordDate");

            var camera = requestData["Number"].Value;
            var beginDate = requestData["BeginDate"].Value;
            var endDate = requestData["EndDate"].Value;

            var eventRecordingCalendarDataSet = DataManager.GetInstance.GetDataForClient.GetEventRecordCalendarData(int.Parse(camera), string.Format("{0} 00:00:00.000", beginDate), string.Format("{0} 23:59:59:999", endDate));

            foreach (DataRow dataRow in eventRecordingCalendarDataSet.Tables[0].Rows)
            {
                foreach (XmlNode xmlNode in elementList)
                {
                    if (xmlNode.Attributes == null || !dataRow["RecordingDate"].ToString().Equals(xmlNode.Attributes["Day"].Value))
                    {
                        continue;
                    }

                    xmlNode.Attributes["Event"].Value = "true";
                    break;
                }
            }

            return getRecordingCalendarData.OuterXml;
        }

        private SearchRecordTimeRequestData ReadDataFromXmlForSearchRecordTimeRequestData(string xmlData)
        {
            var serializer = new XmlSerializer(typeof(SearchRecordTimeRequestData));
            var stringReader = new StringReader(xmlData);
            var xmlReader = new XmlTextReader(stringReader);

            var data = serializer.Deserialize(xmlReader) as SearchRecordTimeRequestData;

            xmlReader.Close();
            stringReader.Close();

            return data;
        }

        #endregion // GetData

        #region SetData

        #endregion // SetData

        #endregion // Playback

        //------------------------------------------------------------------------------------------------------------------------//

        #region Configuration

        #region GetData

        #region GetData of Cameras

        /// <summary>
        /// The get user info.
        /// </summary>
        /// <param name="userId">The user id.</param>
        /// <param name="password">The password.</param>
        /// <returns>The xml of user info.</returns>
        public Stream GetLogInUserInfo(string userId, string password)
        {
            try
            {
                if (WebOperationContext.Current == null)
                {
                    throw new Exception("Can not get current WebOpreationContext.");
                }

                // Content는 무조건 xml임.
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/xml";

                var resultString = DataManager.GetInstance.GetDataForConfiguration.GetUserInfo(userId, password, string.Empty);

                var bytes = Encoding.UTF8.GetBytes(resultString);

                return new MemoryStream(bytes);
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return null;
            }
        }

        /// <summary>
        /// The get user info by privilege.
        /// </summary>
        /// <param name="privilege">The privilege.</param>
        /// <returns>The stream of user info.</returns>
        public Stream GetUserInfoByPrivilege(int privilege)
        {
            try
            {
                if (WebOperationContext.Current == null)
                {
                    throw new Exception("Can not get current WebOpreationContext.");
                }

                // Content는 무조건 xml임.
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/xml";

                var resultString = DataManager.GetInstance.GetDataForConfiguration.GetUserInfo(privilege);

                var bytes = Encoding.UTF8.GetBytes(resultString);

                return new MemoryStream(bytes);
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return null;
            }
        }

        /// <summary>
        /// The get user info by privilege.
        /// </summary>
        /// <param name="privilege">The privilege.</param>
        /// <returns>The stream of user info.</returns>
        public Stream GetUserPrivilegeInfoByPrivilege(int privilege)
        {
            try
            {
                if (WebOperationContext.Current == null)
                {
                    throw new Exception("Can not get current WebOpreationContext.");
                }

                // Content는 무조건 xml임.
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/xml";

                var resultString = DataManager.GetInstance.GetDataForConfiguration.GetUserPrivilegeInfo(privilege);

                var bytes = Encoding.UTF8.GetBytes(resultString);

                return new MemoryStream(bytes);
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return null;
            }
        }

        /// <summary>
        /// The get project info.
        /// </summary>
        /// <returns>The stream of project info.</returns>
        public Stream GetProjectInfo()
        {
            try
            {
                if (WebOperationContext.Current == null)
                {
                    throw new Exception("Can not get current WebOpreationContext.");
                }

                // Content는 무조건 xml임.
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/xml";

                var resultString = DataManager.GetInstance.GetDataForConfiguration.GetProjectInfo();

                var bytes = Encoding.UTF8.GetBytes(resultString);

                return new MemoryStream(bytes);
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return null;
            }
        }

        /// <summary>
        /// The get media server info.
        /// </summary>
        /// <returns>The xml to media server info.</returns>
        public Stream GetMediaServerInfo()
        {
            try
            {
                if (WebOperationContext.Current == null)
                {
                    throw new Exception("Can not get current WebOpreationContext.");
                }

                // Content는 무조건 xml임.
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/xml";

                var resultString = DataManager.GetInstance.GetDataForConfiguration.GetMediaServerInfo();

                var bytes = Encoding.UTF8.GetBytes(resultString);

                return new MemoryStream(bytes);
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return null;
            }
        }

        /// <summary>
        /// The get recorder info.
        /// </summary>
        /// <returns>The xml to recorder info.</returns>
        public Stream GetRecorderInfo()
        {
            try
            {
                if (WebOperationContext.Current == null)
                {
                    throw new Exception("Can not get current WebOpreationContext.");
                }

                // Content는 무조건 xml임.
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/xml";

                var resultString = DataManager.GetInstance.GetDataForConfiguration.GetRecorderInfo();

                var bytes = Encoding.UTF8.GetBytes(resultString);

                return new MemoryStream(bytes);
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return null;
            }
        }

        /// <summary>
        /// The get view server info.
        /// </summary>
        /// <returns>The xml to view server info.</returns>
        public Stream GetViewServerInfo()
        {
            try
            {
                if (WebOperationContext.Current == null)
                {
                    throw new Exception("Can not get current WebOpreationContext.");
                }

                // Content는 무조건 xml임.
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/xml";

                var resultString = DataManager.GetInstance.GetDataForConfiguration.GetViewServerInfo();

                var bytes = Encoding.UTF8.GetBytes(resultString);

                return new MemoryStream(bytes);
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return null;
            }
        }

        /// <summary>
        /// The get display info.
        /// </summary>
        /// <returns>The xml to display info.</returns>
        public Stream GetDisplayInfo()
        {
            try
            {
                if (WebOperationContext.Current == null)
                {
                    throw new Exception("Can not get current WebOpreationContext.");
                }

                // Content는 무조건 xml임.
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/xml";

                var resultString = DataManager.GetInstance.GetDataForConfiguration.GetDisplayInfo();

                var bytes = Encoding.UTF8.GetBytes(resultString);

                return new MemoryStream(bytes);
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return null;
            }
        }

        /// <summary>
        /// The get camera controller service info.
        /// </summary>
        /// <returns>The xml to view server info.</returns>
        public Stream GetCameraControllerServiceInfo()
        {
            try
            {
                if (WebOperationContext.Current == null)
                {
                    throw new Exception("Can not get current WebOpreationContext.");
                }

                // Content는 무조건 xml임.
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/xml";

                var resultString = DataManager.GetInstance.GetDataForConfiguration.GetCameraControllerServiceInfo();

                var bytes = Encoding.UTF8.GetBytes(resultString);

                return new MemoryStream(bytes);
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return null;
            }
        }

        /// <summary>
        /// The get external command service info.
        /// </summary>
        /// <returns>The xml to external command service info.</returns>
        public Stream GetExternalCommandServiceInfo()
        {
            try
            {
                if (WebOperationContext.Current == null)
                {
                    throw new Exception("Can not get current WebOpreationContext.");
                }

                // Content는 무조건 xml임.
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/xml";

                var resultString = DataManager.GetInstance.GetDataForConfiguration.GetExternalCommandServiceInfo();

                var bytes = Encoding.UTF8.GetBytes(resultString);

                return new MemoryStream(bytes);
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return null;
            }
        }

        /// <summary>
        /// The get video spec info.
        /// </summary>
        /// <returns>The xml to video spec info.</returns>
        public Stream GetVideoSpecInfo()
        {
            try
            {
                if (WebOperationContext.Current == null)
                {
                    throw new Exception("Can not get current WebOpreationContext.");
                }

                // Content는 무조건 xml임.
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/xml";

                var resultString = DataManager.GetInstance.GetDataForConfiguration.GetVideoSpecInfo();

                var bytes = Encoding.UTF8.GetBytes(resultString);

                return new MemoryStream(bytes);
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return null;
            }
        }

        public Stream GetPtzConnectionInfo()
        {
            try
            {
                if (WebOperationContext.Current == null)
                {
                    throw new Exception("Can not get current WebOpreationContext.");
                }

                // Content는 무조건 xml임.
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/xml";

                var resultString = DataManager.GetInstance.GetDataForConfiguration.GetPtzConnectionInfo();

                var bytes = Encoding.UTF8.GetBytes(resultString);

                return new MemoryStream(bytes);
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return null;
            }
        }

        public Stream GetCameraControlSettingForConfiguration()
        {
            try
            {
                if (WebOperationContext.Current == null)
                {
                    throw new Exception("Can not get current WebOpreationContext.");
                }

                // Content는 무조건 xml임.
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/xml";

                var resultString =
                    DataManager.GetInstance.GetDataForConfiguration.GetCameraControlSettingForConfiguration();

                var bytes = Encoding.UTF8.GetBytes(resultString);

                return new MemoryStream(bytes);
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return null;
            }
        }

        public Stream GetCameraInfo(string projectId)
        {

            try
            {
                if (WebOperationContext.Current == null)
                {
                    throw new Exception("Can not get current WebOpreationContext.");
                }

                // Content는 무조건 xml임.
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/xml";

                var resultTable = DataManager.GetInstance.GetDataForConfiguration.GetCameraInfo(projectId);
                
                // 삭제 대기 - 2013. 05. 09 - by shwlee.
                //var cameraDataTable = DataManager.GetInstance.GetDataForConfiguration.GetCameraInfo().Tables[0];
                //cameraDataTable.TableName = "Camera";

                //var projectMappingDataTable =
                //    DataManager.GetInstance.SetData.SelectProjectMappingWithoutProjectIdColumn(projectId).Tables[0];
                //projectMappingDataTable.TableName = "ProjectMapping";

                //var dataSet = new DataSet();
                //dataSet.Tables.Add(cameraDataTable.Copy());
                //dataSet.Tables.Add(projectMappingDataTable.Copy());

                var resultString = XmlConverter.DataSetToXml(resultTable);

                var bytes = Encoding.UTF8.GetBytes(resultString);

                return new MemoryStream(bytes);
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return null;
            }
        }

        public Stream GetAllCameraGroupInfoForConfiguration()
        {
            try
            {
                if (WebOperationContext.Current == null)
                {
                    throw new Exception("Can not get current WebOpreationContext.");
                }

                // Content는 무조건 xml임.
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/xml";

                var resultString = DataManager.GetInstance.GetDataForConfiguration.GetAllCameraGroup();

                var bytes = Encoding.UTF8.GetBytes(resultString);

                return new MemoryStream(bytes);
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return null;
            }
        }

        public Stream GetCameraVideoMappingByCameraIdInfo(string cameraId)
        {
            try
            {
                if (WebOperationContext.Current == null)
                {
                    throw new Exception("Can not get current WebOpreationContext.");
                }

                // Content는 무조건 xml임.
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/xml";

                var resultString =
                    DataManager.GetInstance.GetDataForConfiguration.GetCameraVideoMappingByCameraId(cameraId);

                var bytes = Encoding.UTF8.GetBytes(resultString);

                return new MemoryStream(bytes);
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return null;
            }
        }

        public Stream GetAllCameraVideoSpecMapping()
        {
            try
            {
                if (WebOperationContext.Current == null)
                {
                    throw new Exception("Can not get current WebOpreationContext.");
                }

                // Content는 무조건 xml임.
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/xml";

                var resultString =
                    DataManager.GetInstance.GetDataForConfiguration.GetAllCameraVideoSpecMapping();

                var bytes = Encoding.UTF8.GetBytes(resultString);

                return new MemoryStream(bytes);
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return null;
            }
        }

        public Stream GetCameraGroupForConfiguration(string projectId, string userId)
        {
            try
            {
                if (WebOperationContext.Current == null)
                {
                    throw new Exception("Can not get current WebOpreationContext.");
                }

                // Content는 무조건 xml임.
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/xml";

                var resultString = DataManager.GetInstance.GetDataForConfiguration.GetCameraGroup(projectId, userId);

                var bytes = Encoding.UTF8.GetBytes(XmlConverter.DataSetToXml(resultString));

                return new MemoryStream(bytes);
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return null;
            }
        }

        public Stream GetCameraGroupMappingForConfiguration(string projectId, string userId)
        {
            try
            {
                if (WebOperationContext.Current == null)
                {
                    throw new Exception("Can not get current WebOpreationContext.");
                }

                // Content는 무조건 xml임.
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/xml";

                var resultString = DataManager.GetInstance.GetDataForConfiguration.GetCameraGroupMapping(
                    projectId, userId);

                var bytes = Encoding.UTF8.GetBytes(XmlConverter.DataSetToXml(resultString));

                return new MemoryStream(bytes);
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return null;
            }
        }

        public Stream GetAllCameraGroupMappingInfoForConfiguration()
        {
            try
            {
                if (WebOperationContext.Current == null)
                {
                    throw new Exception("Can not get current WebOpreationContext.");
                }

                // Content는 무조건 xml임.
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/xml";

                var resultString = DataManager.GetInstance.GetDataForConfiguration.GetAllCameraGroupMapping();

                var bytes = Encoding.UTF8.GetBytes(resultString);

                return new MemoryStream(bytes);
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return null;
            }
        }

        public Stream GetCameraIdForConfiguration(string projectId)
        {
            try
            {
                if (WebOperationContext.Current == null)
                {
                    throw new Exception("Can not get current WebOpreationContext.");
                }

                // Content는 무조건 xml임.
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/xml";

                var resultString = DataManager.GetInstance.GetDataForConfiguration.GetCameraIdForConfiguration(
                    projectId);

                var bytes = Encoding.UTF8.GetBytes(resultString);

                return new MemoryStream(bytes);
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return null;
            }
        }

        public Stream GetCameraGroupIdForConfiguration(string projectId, string userId)
        {
            try
            {
                if (WebOperationContext.Current == null)
                {
                    throw new Exception("Can not get current WebOpreationContext.");
                }

                // Content는 무조건 xml임.
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/xml";

                var resultString =
                    DataManager.GetInstance.GetDataForConfiguration.GetCameraGroupIdForConfiguration(projectId, userId);

                var bytes = Encoding.UTF8.GetBytes(resultString);

                return new MemoryStream(bytes);
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return null;
            }
        }

        public Stream GetFavoriteInfoForConfiguration(string type)
        {
            try
            {
                if (WebOperationContext.Current == null)
                {
                    throw new Exception("Can not get current WebOpreationContext.");
                }

                // Content는 무조건 xml임.
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/xml";

                var resultString = DataManager.GetInstance.GetDataForConfiguration.GetFavoriteInfo(type.ToLower());

                var bytes = Encoding.UTF8.GetBytes(resultString);

                return new MemoryStream(bytes);
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return null;
            }
        }

        public Stream GetFavoriteGroupInfoForConfiguration(string projectId, string userId, string type)
        {
            try
            {
                if (WebOperationContext.Current == null)
                {
                    throw new Exception("Can not get current WebOpreationContext.");
                }

                // Content는 무조건 xml임.
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/xml";

                var resultString =
                    DataManager.GetInstance.GetDataForConfiguration.GetFavoriteGroupByProjectIdAndUserId(
                        projectId, userId, type);

                var bytes = Encoding.UTF8.GetBytes(resultString);

                return new MemoryStream(bytes);
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return null;
            }
        }

        public Stream GetFavoriteGroupMappingInfoForConfiguration(string projectId, string userId, string type)
        {
            try
            {
                if (WebOperationContext.Current == null)
                {
                    throw new Exception("Can not get current WebOpreationContext.");
                }

                // Content는 무조건 xml임.
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/xml";

                var resultString = DataManager.GetInstance.GetDataForConfiguration.GetFavoriteGroupMapping(
                    projectId, userId, type.ToLower());

                var bytes = Encoding.UTF8.GetBytes(XmlConverter.DataSetToXml(resultString));

                return new MemoryStream(bytes);
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return null;
            }
        }

        public Stream GetFavoriteIdForConfiguration(string type)
        {
            try
            {
                if (WebOperationContext.Current == null)
                {
                    throw new Exception("Can not get current WebOpreationContext.");
                }

                // Content는 무조건 xml임.
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/xml";

                var resultString = DataManager.GetInstance.GetDataForConfiguration.GetFavoriteIdList(type.ToLower());

                var bytes = Encoding.UTF8.GetBytes(resultString);

                return new MemoryStream(bytes);
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return null;
            }
        }

        public Stream GetFavoriteGroupIdForConfiguration(string projectId, string userId, string type)
        {
            try
            {
                if (WebOperationContext.Current == null)
                {
                    throw new Exception("Can not get current WebOpreationContext.");
                }

                // Content는 무조건 xml임.
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/xml";

                var resultString = DataManager.GetInstance.GetDataForConfiguration.GetFavoriteGroupIdList(
                    projectId, userId.ToLower(), type.ToLower());

                var bytes = Encoding.UTF8.GetBytes(resultString);

                return new MemoryStream(bytes);
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return null;
            }
        }

        public Stream GetMediaServerIdInfo()
        {
            try
            {
                if (WebOperationContext.Current == null)
                {
                    throw new Exception("Can not get current WebOpreationContext.");
                }

                // Content는 무조건 xml임.
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/xml";

                var resultString = DataManager.GetInstance.GetDataForConfiguration.GetMediaServerId();

                var bytes = Encoding.UTF8.GetBytes(resultString);

                return new MemoryStream(bytes);
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return null;
            }
        }

        /// <summary>
        /// The get multi camera controller id info.
        /// </summary>
        /// <returns>
        /// The response data.
        /// </returns>
        public Stream GetMultiCameraControllerIdInfo()
        {
            try
            {
                if (WebOperationContext.Current == null)
                {
                    throw new Exception("Can not get current WebOpreationContext.");
                }

                // Content는 무조건 xml임.
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/xml";

                var resultString = DataManager.GetInstance.GetDataForConfiguration.GetMultiCameraControllerInfo();

                var bytes = Encoding.UTF8.GetBytes(resultString);

                return new MemoryStream(bytes);
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return null;
            }
        }

        public Stream GetCameraControlSettingIdInfo()
        {
            try
            {
                if (WebOperationContext.Current == null)
                {
                    throw new Exception("Can not get current WebOpreationContext.");
                }

                // Content는 무조건 xml임.
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/xml";

                var resultString = DataManager.GetInstance.GetDataForConfiguration.GetCameraControlSettingId();

                var bytes = Encoding.UTF8.GetBytes(resultString);

                return new MemoryStream(bytes);
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return null;
            }
        }

        public Stream GetPtzConnectionInfoIdInfo()
        {
            try
            {
                if (WebOperationContext.Current == null)
                {
                    throw new Exception("Can not get current WebOpreationContext.");
                }

                // Content는 무조건 xml임.
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/xml";

                var resultString = DataManager.GetInstance.GetDataForConfiguration.GetPtzConnectionInfoId();

                var bytes = Encoding.UTF8.GetBytes(resultString);

                return new MemoryStream(bytes);
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return null;
            }
        }

        public Stream GetVendorListInfo()
        {
            try
            {
                if (WebOperationContext.Current == null)
                {
                    throw new Exception("Can not get current WebOpreationContext.");
                }

                // Content는 무조건 xml임.
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/xml";

                var resultString = DataManager.GetInstance.GetDataForConfiguration.GetVendorList();

                var bytes = Encoding.UTF8.GetBytes(resultString);

                return new MemoryStream(bytes);
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return null;
            }
        }

        public Stream GetAllUserInfo()
        {
            try
            {
                if (WebOperationContext.Current == null)
                {
                    throw new Exception("Can not get current WebOpreationContext.");
                }

                // Content는 무조건 xml임.
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/xml";

                var resultString = DataManager.GetInstance.GetDataForConfiguration.GetAllUserInfo();

                var bytes = Encoding.UTF8.GetBytes(resultString);

                return new MemoryStream(bytes);
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return null;
            }
        }

        public Stream GetAllUserPrivilegeInfo()
        {
            try
            {
                if (WebOperationContext.Current == null)
                {
                    throw new Exception("Can not get current WebOpreationContext.");
                }

                // Content는 무조건 xml임.
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/xml";

                var resultString = DataManager.GetInstance.GetDataForConfiguration.GetAllUserPrivilegeInfo();

                var bytes = Encoding.UTF8.GetBytes(resultString);

                return new MemoryStream(bytes);
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return null;
            }
        }

        public Stream GetAllfavoriteGroupInfoForConfiguration()
        {
            try
            {
                if (WebOperationContext.Current == null)
                {
                    throw new Exception("Can not get current WebOpreationContext.");
                }

                // Content는 무조건 xml임.
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/xml";

                var resultString = DataManager.GetInstance.GetDataForConfiguration.GetAllFavoriteGroupInfo();

                var bytes = Encoding.UTF8.GetBytes(resultString);

                return new MemoryStream(bytes);
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return null;
            }
        }

        public Stream GetAllFavoriteGroupMappingInfoForConfiguration()
        {
            try
            {
                if (WebOperationContext.Current == null)
                {
                    throw new Exception("Can not get current WebOpreationContext.");
                }

                // Content는 무조건 xml임.
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/xml";

                var resultString = DataManager.GetInstance.GetDataForConfiguration.GetAllFavoriteGroupMappingInfo();

                var bytes = Encoding.UTF8.GetBytes(resultString);

                return new MemoryStream(bytes);
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return null;
            }
        }

        public Stream GetAllCameraInfoForConfiguration()
        {
            try
            {
                if (WebOperationContext.Current == null)
                {
                    throw new Exception("Can not get current WebOpreationContext.");
                }

                // Content는 무조건 xml임.
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/xml";

                var resultString = DataManager.GetInstance.GetDataForConfiguration.GetAllCameraInfo();

                var bytes = Encoding.UTF8.GetBytes(resultString);

                return new MemoryStream(bytes);
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return null;
            }
        }

        public Stream GetAllProjectMappingInfo()
        {
            try
            {
                if (WebOperationContext.Current == null)
                {
                    throw new Exception("Can not get current WebOpreationContext.");
                }

                // Content는 무조건 xml임.
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/xml";

                var resultString = DataManager.GetInstance.GetDataForConfiguration.GetAllProjectMappingInfo();

                var bytes = Encoding.UTF8.GetBytes(resultString);

                return new MemoryStream(bytes);
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return null;
            }
        }

        public Stream GetAllCameraVideoMappingInfo()
        {
            try
            {
                if (WebOperationContext.Current == null)
                {
                    throw new Exception("Can not get current WebOpreationContext.");
                }

                // Content는 무조건 xml임.
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/xml";

                var resultString = DataManager.GetInstance.GetDataForConfiguration.GetAllCameraVideoMappingInfo();

                var bytes = Encoding.UTF8.GetBytes(resultString);

                return new MemoryStream(bytes);
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return null;
            }
        }

        public Stream GetRecordCameraNumber()
        {
            try
            {
                if (WebOperationContext.Current == null)
                {
                    throw new Exception("Can not get current WebOpreationContext.");
                }

                // Content는 무조건 xml임.
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/xml";

                var resultString = DataManager.GetInstance.GetDataForConfiguration.GetRecordCameraNumber();

                var bytes = Encoding.UTF8.GetBytes(resultString);

                return new MemoryStream(bytes);
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return null;
            }
        }

        #endregion // GetData of Cameras

        #region GetData of Nvr

        /// <summary>
        /// The get storage info.
        /// </summary>
        /// <returns>
        /// The memory stream of storage info data set.
        /// </returns>
        public Stream GetStorageInfo()
        {
            try
            {
                if (WebOperationContext.Current == null)
                {
                    throw new Exception("Can not get current WebOpreationContext.");
                }

                // Content는 무조건 xml임.
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/xml";

                var resultString = DataManager.GetInstance.GetDataForConfiguration.GetStorageInfo();

                var bytes = Encoding.UTF8.GetBytes(resultString);

                return new MemoryStream(bytes);
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return null;
            }
        }

        /// <summary>
        /// The get record schedule info.
        /// </summary>
        /// <returns>
        /// The memory stream of record info data set. 
        /// </returns>
        /// <exception cref="Exception">
        /// The exception.
        /// </exception>
        public Stream GetRecordScheduleInfo()
        {
            try
            {
                if (WebOperationContext.Current == null)
                {
                    throw new Exception("Can not get current WebOpreationContext.");
                }

                // Content는 무조건 xml임.
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/xml";

                var resultString = DataManager.GetInstance.GetDataForConfiguration.GetRecordScheduleInfo();

                var bytes = Encoding.UTF8.GetBytes(resultString);

                return new MemoryStream(bytes);
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return null;
            }
        }

        /// <summary>
        /// The get archive schedule info.
        /// </summary>
        /// <returns>
        /// The memory stream of archive info data set.
        /// </returns>
        public Stream GetArchiveScheduleInfo()
        {
            try
            {
                if (WebOperationContext.Current == null)
                {
                    throw new Exception("Can not get current WebOpreationContext.");
                }

                // Content는 무조건 xml임.
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/xml";

                var resultString = DataManager.GetInstance.GetDataForConfiguration.GetArchiveScheduleInfo();

                var bytes = Encoding.UTF8.GetBytes(resultString);

                return new MemoryStream(bytes);
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return null;
            }
        }

        /// <summary>
        /// The get record camera info.
        /// </summary>
        /// <returns>
        /// The memory stream of record camera info data set.
        /// </returns>
        /// <exception cref="Exception">
        /// The exception.
        /// </exception>
        public Stream GetRecordCameraInfo()
        {
            try
            {
                if (WebOperationContext.Current == null)
                {
                    throw new Exception("Can not get current WebOpreationContext.");
                }

                // Content는 무조건 xml임.
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/xml";

                var resultString = DataManager.GetInstance.GetDataForConfiguration.GetRecordCameraInfo();

                var bytes = Encoding.UTF8.GetBytes(resultString);

                return new MemoryStream(bytes);
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return null;
            }
        }

        /// <summary>
        /// The get storage list for record camera.
        /// </summary>
        /// <returns>
        /// The memory stream of storage list for record camera data set.
        /// </returns>
        /// <exception cref="Exception">
        /// The exception.
        /// </exception>
        public Stream GetStorageListForRecordCamera()
        {
            try
            {
                if (WebOperationContext.Current == null)
                {
                    throw new Exception("Can not get current WebOpreationContext.");
                }

                // Content는 무조건 xml임.
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/xml";

                var resultString = DataManager.GetInstance.GetDataForConfiguration.GetRecordStorageList();

                var bytes = Encoding.UTF8.GetBytes(resultString);

                return new MemoryStream(bytes);
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return null;
            }
        }

        /// <summary>
        /// The get record schedule list for record camera.
        /// </summary>
        /// <returns>
        /// The memory stream of record schedule list  for record camera data set.
        /// </returns>
        /// <exception cref="Exception">
        /// The exception.
        /// </exception>
        public Stream GetRecordScheduleListForRecordCamera()
        {
            try
            {
                if (WebOperationContext.Current == null)
                {
                    throw new Exception("Can not get current WebOpreationContext.");
                }

                // Content는 무조건 xml임.
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/xml";

                var resultString = DataManager.GetInstance.GetDataForConfiguration.GetRecordScheduleList();

                var bytes = Encoding.UTF8.GetBytes(resultString);

                return new MemoryStream(bytes);
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return null;
            }
        }

        /// <summary>
        /// The get archive schedule list  for record camera.
        /// </summary>
        /// <returns>
        /// The memory stream of archive schedule list for record camera data set.
        /// </returns>
        /// <exception cref="Exception">
        /// The exception.
        /// </exception>
        public Stream GetArchiveScheduleListForRecordCamera()
        {
            try
            {
                if (WebOperationContext.Current == null)
                {
                    throw new Exception("Can not get current WebOpreationContext.");
                }

                // Content는 무조건 xml임.
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/xml";

                var resultString = DataManager.GetInstance.GetDataForConfiguration.GetArchiveScheduleList();

                var bytes = Encoding.UTF8.GetBytes(resultString);

                return new MemoryStream(bytes);
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return null;
            }
        }

        /// <summary>
        /// The get recorder list for record camera.
        /// </summary>
        /// <returns>
        /// The memory stream of recorder list for record camera data set.
        /// </returns>
        /// <exception cref="Exception">
        /// The exception.
        /// </exception>
        public Stream GetRecorderListForRecordCamera()
        {
            try
            {
                if (WebOperationContext.Current == null)
                {
                    throw new Exception("Can not get current WebOpreationContext.");
                }

                // Content는 무조건 xml임.
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/xml";

                var resultString = DataManager.GetInstance.GetDataForConfiguration.GetRecorderList();

                var bytes = Encoding.UTF8.GetBytes(resultString);

                return new MemoryStream(bytes);
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return null;
            }
        }

        /// <summary>
        /// The get camera to record info.
        /// </summary>
        /// <returns>
        /// The <see cref="Stream"/>.
        /// </returns>
        /// <exception cref="Exception">
        /// </exception>
        public Stream GetCameraToRecordInfo()
        {
            try
            {
                if (WebOperationContext.Current == null)
                {
                    throw new Exception("Can not get current WebOpreationContext.");
                }

                // Content는 무조건 xml임.
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/xml";

                var resultString = DataManager.GetInstance.GetDataForConfiguration.GetCameraToRecordInfo();

                var bytes = Encoding.UTF8.GetBytes(resultString);

                return new MemoryStream(bytes);
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return null;
            }
        }

        #endregion // GetData of Nvr

        #region GetData of Recorder

        /// <summary>
        /// The get video information process.
        /// </summary>
        /// <param name="recorderId">
        /// The recorder id.
        /// </param>
        /// <returns>
        /// The <see cref="Stream"/>.
        /// </returns>
        /// <exception cref="Exception">
        /// </exception>
        public Stream GetVideoInformationProcess(string recorderId, string recordcameranumber)
        {
            try
            {
                if (WebOperationContext.Current == null)
                {
                    throw new Exception("Can not get current WebOpreationContext.");
                }

                // Content는 무조건 xml임.
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/xml";

                const string Header = @"<?xml version=""1.0"" encoding=""utf-8""?>";

                var resultString = string.Format("{0}\n{1}", Header, DataManager.GetInstance.GetDataForClient.GetVideoInformationForNvr(recorderId, recordcameranumber));

                var bytes = Encoding.UTF8.GetBytes(resultString);

                return new MemoryStream(bytes);
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return null;
            }
        }

        /// <summary>
        /// The get recorder process.
        /// </summary>
        /// <returns>
        /// The <see cref="Stream"/>.
        /// </returns>
        public Stream GetRecorderProcess(string recorderIp)
        {
            try
            {
                if (WebOperationContext.Current == null)
                {
                    throw new Exception("Can not get current WebOpreationContext.");
                }

                // Content는 무조건 xml임.
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/xml";

                const string Header = @"<?xml version=""1.0"" encoding=""utf-8""?>";

                var resultString = string.Format("{0}\n{1}", Header, DataManager.GetInstance.GetDataForClient.GetRecorderByIp(recorderIp));

                var bytes = Encoding.UTF8.GetBytes(resultString);

                return new MemoryStream(bytes);
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return null;
            }
        }

        /// <summary>
        /// The get storage process.
        /// </summary>
        /// <returns>
        /// The <see cref="Stream"/>.
        /// </returns>
        /// <exception cref="Exception">
        /// </exception>
        public Stream GetStorageProcess()
        {
            try
            {
                if (WebOperationContext.Current == null)
                {
                    throw new Exception("Can not get current WebOpreationContext.");
                }

                // Content는 무조건 xml임.
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/xml";

                const string Header = @"<?xml version=""1.0"" encoding=""utf-8""?>";

                var resultString = string.Format("{0}\n{1}", Header, DataManager.GetInstance.GetDataForConfiguration.GetStorageInfo());

                var bytes = Encoding.UTF8.GetBytes(resultString);

                return new MemoryStream(bytes);
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return null;
            }
        }

        /// <summary>
        /// The get record schedule data process.
        /// </summary>
        /// <param name="recordScheduleNumber">
        /// The record schedule number.
        /// </param>
        /// <returns>
        /// The <see cref="Stream"/>.
        /// </returns>
        /// <exception cref="Exception">
        /// </exception>
        public Stream GetRecordScheduleDataProcess(string recordScheduleNumber)
        {
            try
            {
                if (WebOperationContext.Current == null)
                {
                    throw new Exception("Can not get current WebOpreationContext.");
                }

                // Content는 무조건 xml임.
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/xml";

                const string Header = @"<?xml version=""1.0"" encoding=""utf-8""?>";

                var resultString = string.Format("{0}\n{1}", Header, DataManager.GetInstance.GetDataForClient.GetRecordScheduleByRecordScheduleNumber(int.Parse(recordScheduleNumber)));

                var bytes = Encoding.UTF8.GetBytes(resultString);

                return new MemoryStream(bytes);
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return null;
            }
        }

        #endregion // GetData of Recorder

        #endregion // GetData

        #region SetData

        #region SetData of Cameras

        #region User

        /// <summary>
        /// The add user info.
        /// </summary>
        /// <param name="stream">
        /// The stream to user info.
        /// </param>
        /// <returns>
        /// The System.IO.Stream.
        /// </returns>
        public Stream AddUserInfo(Stream stream)
        {
            try
            {
                using (var readStream = new StreamReader(stream))
                {
                    var userInfoDataSet = new DataSet();

                    userInfoDataSet.ReadXml(readStream);

                    foreach (DataRow row in userInfoDataSet.Tables[0].Rows)
                    {
                        var resultString = DataManager.GetInstance.SetData.InsertUser(
                                                        row["UserID"].ToString().Trim(),
                                                        row["Password"].ToString().Trim(),
                                                        row["Name"].ToString().Trim(),
                                                        int.Parse(row["Privilege"].ToString().Trim()),
                                                        row["MobileNumber"].ToString().Trim(),
                                                        row["OfficeTelNumber"].ToString().Trim(),
                                                        row["Email"].ToString().Trim(),
                                                        bool.Parse(row["UseCommand"].ToString().Trim()),
                                                        bool.Parse(row["UseViewer"].ToString().Trim()),
                                                        bool.Parse(row["UsePlayback"].ToString().Trim()));

                        if (!string.IsNullOrWhiteSpace(resultString))
                        {
                            return CommonUtil.ResponseData(ResponseType.Failed, resultString);
                        }
                    }

                    return CommonUtil.ResponseData(ResponseType.Success);
                }
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return CommonUtil.ResponseData(ResponseType.Failed, ex.Message);
            }
        }

        public Stream ImportUserInfo(Stream stream)
        {
            try
            {
                using (var readStream = new StreamReader(stream))
                {
                    var userInfoDataSet = new DataSet();

                    userInfoDataSet.ReadXml(readStream);

                    var count = 0;

                    foreach (DataRow row in userInfoDataSet.Tables[0].Rows)
                    {
                        var resultString = DataManager.GetInstance.SetData.ImportUser(
                                                        row["UserID"].ToString().Trim(),
                                                        row["Password"].ToString().Trim(),
                                                        row["Name"].ToString().Trim(),
                                                        int.Parse(row["Privilege"].ToString().Trim()),
                                                        row["MobileNumber"].ToString().Trim(),
                                                        row["OfficeTelNumber"].ToString().Trim(),
                                                        row["Email"].ToString().Trim(),
                                                        bool.Parse(row["UseCommand"].ToString().Trim()),
                                                        bool.Parse(row["UseViewer"].ToString().Trim()),
                                                        bool.Parse(row["UsePlayback"].ToString().Trim()),
                                                        count);

                        if (!string.IsNullOrWhiteSpace(resultString))
                        {
                            return CommonUtil.ResponseData(ResponseType.Failed, resultString);
                        }

                        count++;
                    }

                    return CommonUtil.ResponseData(ResponseType.Success);
                }
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return CommonUtil.ResponseData(ResponseType.Failed, ex.Message);
            }
        }

        /// <summary>
        /// The add user info.
        /// </summary>
        /// <param name="stream">The stream to user info.</param>
        public Stream AddUserPrivilegeInfo(Stream stream)
        {
            try
            {
                using (var readStream = new StreamReader(stream))
                {
                    var userInfoDataSet = new DataSet();

                    userInfoDataSet.ReadXml(readStream);

                    foreach (DataRow row in userInfoDataSet.Tables[0].Rows)
                    {
                        var resultString = DataManager.GetInstance.SetData.InsertUserPrivilege(
                                                        Int32.Parse(row["Privilege"].ToString().Trim()),
                                                        row["Grade"].ToString().Trim(),
                                                        Boolean.Parse(row["Sync"].ToString().Trim()),
                                                        Boolean.Parse(row["RdsControl"].ToString().Trim()),
                                                        Boolean.Parse(row["PtzControl"].ToString().Trim()),
                                                        Boolean.Parse(row["FavoriteSave"].ToString().Trim()),
                                                        Boolean.Parse(row["ResourceCamera"].ToString().Trim()),
                                                        Boolean.Parse(row["ResourceLayout"].ToString().Trim()),
                                                        Boolean.Parse(row["ResourceMap"].ToString().Trim()),
                                                        Boolean.Parse(row["ResourceImage"].ToString().Trim()),
                                                        Boolean.Parse(row["PlaybackExport"].ToString().Trim()));

                        if (!string.IsNullOrWhiteSpace(resultString))
                        {
                            return CommonUtil.ResponseData(ResponseType.Failed, resultString);
                        }
                    }

                    return CommonUtil.ResponseData(ResponseType.Success);
                }
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return CommonUtil.ResponseData(ResponseType.Failed, ex.Message);
            }
        }

        public Stream ImportUserPrivilegeInfo(Stream stream)
        {
            try
            {
                using (var readStream = new StreamReader(stream))
                {
                    var userInfoDataSet = new DataSet();

                    userInfoDataSet.ReadXml(readStream);

                    var count = 0;

                    foreach (DataRow row in userInfoDataSet.Tables[0].Rows)
                    {
                        var resultString = DataManager.GetInstance.SetData.ImportUserPrivilege(
                                                        Int32.Parse(row["Privilege"].ToString().Trim()),
                                                        row["Grade"].ToString().Trim(),
                                                        Boolean.Parse(row["Sync"].ToString().Trim()),
                                                        Boolean.Parse(row["RdsControl"].ToString().Trim()),
                                                        Boolean.Parse(row["PtzControl"].ToString().Trim()),
                                                        Boolean.Parse(row["FavoriteSave"].ToString().Trim()),
                                                        Boolean.Parse(row["ResourceCamera"].ToString().Trim()),
                                                        Boolean.Parse(row["ResourceLayout"].ToString().Trim()),
                                                        Boolean.Parse(row["ResourceMap"].ToString().Trim()),
                                                        Boolean.Parse(row["ResourceImage"].ToString().Trim()),
                                                        Boolean.Parse(row["PlaybackExport"].ToString().Trim()),
                                                        count);

                        if (!string.IsNullOrWhiteSpace(resultString))
                        {
                            return CommonUtil.ResponseData(ResponseType.Failed, resultString);
                        }

                        count++;
                    }

                    return CommonUtil.ResponseData(ResponseType.Success);
                }
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return CommonUtil.ResponseData(ResponseType.Failed, ex.Message);
            }
        }

        /// <summary>
        /// The update user info.
        /// </summary>
        /// <param name="stream">The stream to user info.</param>
        public Stream UpdateUserInfo(Stream stream)
        {
            try
            {
                using (var readStream = new StreamReader(stream))
                {
                    var userInfoDataSet = new DataSet();

                    userInfoDataSet.ReadXml(readStream);

                    foreach (DataRow row in userInfoDataSet.Tables[0].Rows)
                    {
                        var result = DataManager.GetInstance.SetData.UpdateUser(
                                                row["UserID"].ToString().Trim(),
                                                row["Password"].ToString().Trim(),
                                                row["Name"].ToString().Trim(),
                                                Int32.Parse(row["Privilege"].ToString().Trim()),
                                                row["MobileNumber"].ToString().Trim(),
                                                row["OfficeTelNumber"].ToString().Trim(),
                                                row["Email"].ToString().Trim(),
                                                Boolean.Parse(row["UseCommand"].ToString().Trim()),
                                                Boolean.Parse(row["UseViewer"].ToString().Trim()),
                                                Boolean.Parse(row["UsePlayback"].ToString().Trim()));

                        if (!string.IsNullOrWhiteSpace(result))
                        {
                            return CommonUtil.ResponseData(ResponseType.Failed, result);
                        }
                    }
                }

                return CommonUtil.ResponseData(ResponseType.Success);
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return CommonUtil.ResponseData(ResponseType.Failed, ex.Message); ;
            }
        }

        /// <summary>
        /// The update user info.
        /// </summary>
        /// <param name="stream">The stream to user info.</param>
        public Stream UpdateUserPrivilegeInfo(Stream stream)
        {
            try
            {
                using (var readStream = new StreamReader(stream))
                {
                    var userInfoDataSet = new DataSet();

                    userInfoDataSet.ReadXml(readStream);

                    foreach (DataRow row in userInfoDataSet.Tables[0].Rows)
                    {
                        var result = DataManager.GetInstance.SetData.UpdateUserPrivilege(
                                                    Int32.Parse(row["Privilege"].ToString().Trim()),
                                                    row["Grade"].ToString().Trim(),
                                                    Boolean.Parse(row["Sync"].ToString().Trim()),
                                                    Boolean.Parse(row["RdsControl"].ToString().Trim()),
                                                    Boolean.Parse(row["PtzControl"].ToString().Trim()),
                                                    Boolean.Parse(row["FavoriteSave"].ToString().Trim()),
                                                    Boolean.Parse(row["ResourceCamera"].ToString().Trim()),
                                                    Boolean.Parse(row["ResourceLayout"].ToString().Trim()),
                                                    Boolean.Parse(row["ResourceMap"].ToString().Trim()),
                                                    Boolean.Parse(row["ResourceImage"].ToString().Trim()),
                                                    Boolean.Parse(row["PlaybackExport"].ToString().Trim()));

                        if (!string.IsNullOrWhiteSpace(result))
                        {
                            return CommonUtil.ResponseData(ResponseType.Failed, result);
                        }
                    }
                }

                return CommonUtil.ResponseData(ResponseType.Success);
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return CommonUtil.ResponseData(ResponseType.Failed, ex.Message); ;
            }
        }

        /// <summary>
        /// The delete user info.
        /// </summary>
        /// <param name="stream">The stream to user info.</param>
        public Stream DeleteUserInfo(Stream stream)
        {
            try
            {
                using (var readStream = new StreamReader(stream))
                {
                    var userInfoDataSet = new DataSet();

                    userInfoDataSet.ReadXml(readStream);

                    foreach (DataRow row in userInfoDataSet.Tables[0].Rows)
                    {
                        var result = DataManager.GetInstance.SetData.DeleteUser(row["UserID"].ToString().Trim());

                        if (!string.IsNullOrWhiteSpace(result))
                        {
                            return CommonUtil.ResponseData(ResponseType.Failed, result);
                        }
                    }
                }

                return CommonUtil.ResponseData(ResponseType.Success);
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return CommonUtil.ResponseData(ResponseType.Failed, ex.Message); ;
            }
        }

        /// <summary>
        /// The delete user info.
        /// </summary>
        /// <param name="stream">The stream to user info.</param>
        public Stream DeleteUserPrivilegeInfo(Stream stream)
        {
            try
            {
                using (var readStream = new StreamReader(stream))
                {
                    var userInfoDataSet = new DataSet();

                    userInfoDataSet.ReadXml(readStream);

                    foreach (DataRow row in userInfoDataSet.Tables[0].Rows)
                    {
                        int parse_result = 0;
                        if (Int32.TryParse(row["Privilege"].ToString().Trim(), out parse_result))
                        {
                            var result = DataManager.GetInstance.SetData.DeleteUserPrivilege(parse_result);

                            if (!string.IsNullOrWhiteSpace(result))
                            {
                                return CommonUtil.ResponseData(ResponseType.Failed, result);
                            }
                        }
                    }
                }

                return CommonUtil.ResponseData(ResponseType.Success);
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return CommonUtil.ResponseData(ResponseType.Failed, ex.Message); ;
            }
        }

        #endregion // User

        #region Project

        /// <summary>
        /// 프로젝트 정보 등록.
        /// </summary>
        /// <param name="stream">프로젝트 등록 정보.</param>
        public Stream AddProjectInfo(Stream stream)
        {
            try
            {
                using (var readStream = new StreamReader(stream))
                {
                    var projectInfoDataSet = new DataSet();

                    projectInfoDataSet.ReadXml(readStream);

                    foreach (DataRow row in projectInfoDataSet.Tables[0].Rows)
                    {
                        var result = DataManager.GetInstance.SetData.InsertProjcet(
                                                row["ProjectID"].ToString().Trim(),
                                                row["ProjectName"].ToString().Trim(),
                                                row["ProjectDescription"].ToString().Trim());

                        if (!string.IsNullOrWhiteSpace(result))
                        {
                            return CommonUtil.ResponseData(ResponseType.Failed, result);
                        }
                    }
                }

                return CommonUtil.ResponseData(ResponseType.Success);
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return CommonUtil.ResponseData(ResponseType.Failed, ex.Message);
            }
        }

        public Stream ImportProjectInfo(Stream stream)
        {
            try
            {
                using (var readStream = new StreamReader(stream))
                {
                    var projectInfoDataSet = new DataSet();

                    projectInfoDataSet.ReadXml(readStream);

                    var count = 0;

                    foreach (DataRow row in projectInfoDataSet.Tables[0].Rows)
                    {
                        var result = DataManager.GetInstance.SetData.ImportProjcet(
                                                row["ProjectID"].ToString().Trim(),
                                                row["ProjectName"].ToString().Trim(),
                                                row["ProjectDescription"].ToString().Trim(),
                                                count);

                        if (!string.IsNullOrWhiteSpace(result))
                        {
                            return CommonUtil.ResponseData(ResponseType.Failed, result);
                        }

                        count++;
                    }
                }

                return CommonUtil.ResponseData(ResponseType.Success);
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return CommonUtil.ResponseData(ResponseType.Failed, ex.Message);
            }
        }

        /// <summary>
        /// 프로젝트 정보 수정.
        /// </summary>
        /// <param name="stream">프로젝트 수정 정보.</param>
        public Stream UpdateProjectInfo(Stream stream)
        {
            try
            {
                // 저장 위치에 있는 xaml 문자열을 가져와서 Stream으로 return.
                if (WebOperationContext.Current == null)
                {
                    throw new Exception("Can not get current WebOpreationContext.");
                }

                // Content는 무조건 xml임.
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/xml";

                Stream responseData = null;
                using (var readStream = new StreamReader(stream))
                {
                    var projectInfoDataSet = new DataSet();

                    projectInfoDataSet.ReadXml(readStream);

                    foreach (DataRow row in projectInfoDataSet.Tables[0].Rows)
                    {
                        var result = DataManager.GetInstance.SetData.UpdateProjcet(
                                                row["ProjectID"].ToString().Trim(),
                                                row["ProjectName"].ToString().Trim(),
                                                row["ProjectDescription"].ToString().Trim());

                        if (!string.IsNullOrWhiteSpace(result))
                        {
                            responseData = CommonUtil.ResponseData(ResponseType.Failed, result);
                            return responseData;
                        }
                    }

                    responseData = CommonUtil.ResponseData(ResponseType.Success);
                }

                return responseData;
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return CommonUtil.ResponseData(ResponseType.Failed, ex.Message);
            }
        }

        /// <summary>
        /// 프로젝트 삭제.
        /// 프로젝트를 삭제하면 CameraGroup / Favorite 의 동일한 ProjectID 에 대해서 
        /// 데이터를 삭제한다.
        /// </summary>
        /// <param name="stream">프로젝트 ID.</param>
        public Stream DeleteProjectInfo(Stream stream)
        {
            try
            {
                Stream resultStream = null;
                using (var readStream = new StreamReader(stream))
                {
                    var projectInfoDataTable = new DataTable();

                    projectInfoDataTable.ReadXml(readStream);

                    foreach (
                        var projectId in
                            from DataRow row in projectInfoDataTable.Rows select row["ProjectID"].ToString().Trim())
                    {
                        var deleteResult = DataManager.GetInstance.SetData.DeleteProjcet(projectId);
                        if (!string.IsNullOrWhiteSpace(deleteResult))
                        {
                            resultStream = CommonUtil.ResponseData(ResponseType.Failed, deleteResult);
                            return resultStream;
                        }

                        DataManager.GetInstance.SetData.DeleteCameraGroupByProjectId(projectId, string.Empty);
                        DataGarbageCollecter.Invoke(DeleteType.Camera);

                        DataManager.GetInstance.SetData.DeleteFavoriteGroupByProjectId(projectId);
                        DataGarbageCollecter.Invoke(DeleteType.CommandFavorite);
                    }

                    resultStream = CommonUtil.ResponseData(ResponseType.Success);
                }

                return resultStream;
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return CommonUtil.ResponseData(ResponseType.Failed, ex.Message);
            }
        }

        #endregion // Project

        #region MediaServer

        /// <summary>
        /// 미디어서버 정보 추가.
        /// </summary>
        /// <param name="stream">추가 할 미디어서버 정보.</param>
        public Stream AddMediaServerInfo(Stream stream)
        {
            try
            {
                using (var readStream = new StreamReader(stream))
                {
                    var mediaServerDataSet = new DataSet();

                    mediaServerDataSet.ReadXml(readStream);

                    foreach (DataRow row in mediaServerDataSet.Tables[0].Rows)
                    {
                        var resultString =
                            DataManager.GetInstance.SetData.InsertMediaServer(
                                row["MediaServerID"].ToString().Trim(),
                                row["MediaServerIP"].ToString().Trim(),
                                row["MediaServerDescription"].ToString().Trim(),
                                int.Parse(row["VideoBoardWidth"].ToString()),
                                int.Parse(row["VideoBoardHeight"].ToString()),
                                int.Parse(row["VideoBoardRow"].ToString()),
                                int.Parse(row["VideoBoardColumn"].ToString()),
                                row["TransmitIP"].ToString().Trim(),
                                IsCheckedForNullableInt.IsCheckedData(row["TransmitPort"].ToString().Trim()),
                                row["ConnectMode"].ToString().Trim(),
                                row["StatusServiceUrl"].ToString().Trim(),
                                row["ActiveCode"].ToString().Trim());

                        if (!string.IsNullOrWhiteSpace(resultString))
                        {
                            return CommonUtil.ResponseData(ResponseType.Failed, resultString);
                        }
                    }
                }

                return CommonUtil.ResponseData(ResponseType.Success);
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return CommonUtil.ResponseData(ResponseType.Failed, ex.Message);
            }
        }

        public Stream ImportMediaServerInfo(Stream stream)
        {
            try
            {
                using (var readStream = new StreamReader(stream))
                {
                    var mediaServerDataSet = new DataSet();

                    mediaServerDataSet.ReadXml(readStream);

                    var count = 0;

                    foreach (DataRow row in mediaServerDataSet.Tables[0].Rows)
                    {
                        var resultString = DataManager.GetInstance.SetData.ImportMediaServer(
                                             row["MediaServerID"].ToString().Trim(),
                                             row["MediaServerIP"].ToString().Trim(),
                                             row["MediaServerDescription"].ToString().Trim(),
                                             int.Parse(row["VideoBoardWidth"].ToString()),
                                             int.Parse(row["VideoBoardHeight"].ToString()),
                                             int.Parse(row["VideoBoardRow"].ToString()),
                                             int.Parse(row["VideoBoardColumn"].ToString()),
                                             row["TransmitIP"].ToString().Trim(),
                                             IsCheckedForNullableInt.IsCheckedData(row["TransmitPort"].ToString().Trim()),
                                             row["ConnectMode"].ToString().Trim(),
                                             row["ActiveCode"].ToString().Trim(),
                                             row["StatusServiceUrl"].ToString().Trim(),
                                             count);

                        if (!string.IsNullOrWhiteSpace(resultString))
                        {
                            return CommonUtil.ResponseData(ResponseType.Failed, resultString);
                        }

                        count++;
                    }
                }

                return CommonUtil.ResponseData(ResponseType.Success);
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return CommonUtil.ResponseData(ResponseType.Failed, ex.Message);
            }
        }

        /// <summary>
        /// 미디어서버 정보 수정.
        /// </summary>
        /// <param name="stream">수정 할 미디어서버 정보.</param>
        public Stream UpdateMediaServerInfo(Stream stream)
        {
            try
            {
                var resultString = string.Empty;

                using (var readStream = new StreamReader(stream))
                {
                    var mediaServerDataSet = new DataSet();

                    mediaServerDataSet.ReadXml(readStream);

                    var row = mediaServerDataSet.Tables[0].Rows[0];

                    resultString = DataManager.GetInstance.SetData.UpdateMediaServer(
                                                row["MediaServerID"].ToString().Trim(),
                                                row["MediaServerIP"].ToString().Trim(),
                                                row["MediaServerDescription"].ToString().Trim(),
                                                int.Parse(row["VideoBoardWidth"].ToString().Trim()),
                                                int.Parse(row["VideoBoardHeight"].ToString().Trim()),
                                                int.Parse(row["VideoBoardRow"].ToString().Trim()),
                                                int.Parse(row["VideoBoardColumn"].ToString().Trim()),
                                                row["TransmitIP"].ToString().Trim(),
                                                IsCheckedForNullableInt.IsCheckedData(row["TransmitPort"].ToString().Trim()),
                                                row["ConnectMode"].ToString().Trim(),
                                                row["StatusServiceUrl"].ToString().Trim(),
                                                row["ActiveCode"].ToString().Trim());
                }

                return CommonUtil.ResponseData(string.IsNullOrWhiteSpace(resultString) ? ResponseType.Success : ResponseType.Failed, resultString);
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return CommonUtil.ResponseData(ResponseType.Failed, ex.Message);
            }
        }

        /// <summary>
        /// 미디어서버 정보 삭제.
        /// </summary>
        /// <param name="stream">삭제 할 미디어서버 정보.</param>
        public Stream DeleteMediaServerInfo(Stream stream)
        {
            try
            {
                using (var readStream = new StreamReader(stream))
                {
                    var mediaServerInfoDataTable = new DataTable();

                    mediaServerInfoDataTable.ReadXml(readStream);

                    var resultString = string.Empty;

                    foreach (var mediaServerId in
                            from DataRow row in mediaServerInfoDataTable.Rows
                            select row["MediaServerID"].ToString().Trim())
                    {
                        resultString = DataManager.GetInstance.SetData.DeleteMediaServer(mediaServerId);

                        if (!string.IsNullOrWhiteSpace(resultString))
                        {
                            return CommonUtil.ResponseData(ResponseType.Failed, resultString);
                        }
                    }

                    var mediaServerDataSet = new DataSet();
                    mediaServerDataSet.Tables.Add(mediaServerInfoDataTable);
                }

                return CommonUtil.ResponseData(ResponseType.Success);
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return CommonUtil.ResponseData(ResponseType.Failed, ex.Message);
            }
        }

        #endregion // MediaServer

        #region Recorder

        /// <summary>
        /// 미디어서버 정보 추가.
        /// </summary>
        /// <param name="stream">추가 할 미디어서버 정보.</param>
        public Stream AddRecorderInfo(Stream stream)
        {
            try
            {
                using (var readStream = new StreamReader(stream))
                {
                    var recorderDataSet = new DataSet();

                    recorderDataSet.ReadXml(readStream);

                    foreach (DataRow row in recorderDataSet.Tables[0].Rows)
                    {
                        var resultString = DataManager.GetInstance.SetData.InsertRecorder(
                                             row["RecorderID"].ToString().Trim(),
                                             row["RecorderIP"].ToString().Trim(),
                                             row["RecorderDescription"].ToString().Trim(),
                                             int.Parse(row["RESTServerPort"].ToString()),
                                             int.Parse(row["IMSPServerPort"].ToString()),
                                             row["EventRecordStorageList"].ToString().Trim(),
                                             row["StatusServiceUrl"].ToString().Trim(),
                                             row["ActiveCode"].ToString().Trim());

                        if (!string.IsNullOrWhiteSpace(resultString))
                        {
                            return CommonUtil.ResponseData(ResponseType.Failed, resultString);
                        }
                    }
                }

                return CommonUtil.ResponseData(ResponseType.Success);
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return CommonUtil.ResponseData(ResponseType.Failed, ex.Message);
            }
        }

        public Stream ImportRecorderInfo(Stream stream)
        {
            try
            {
                using (var readStream = new StreamReader(stream))
                {
                    var recorderDataSet = new DataSet();

                    recorderDataSet.ReadXml(readStream);

                    var count = 0;

                    foreach (DataRow row in recorderDataSet.Tables[0].Rows)
                    {
                        var resultString = DataManager.GetInstance.SetData.ImportRecorder(
                                             row["RecorderID"].ToString().Trim(),
                                             row["RecorderIP"].ToString().Trim(),
                                             row["RecorderDescription"].ToString().Trim(),
                                             int.Parse(row["RESTServerPort"].ToString()),
                                             int.Parse(row["IMSPServerPort"].ToString()),
                                             row["EventRecordStorageList"].ToString().Trim(),
                                             row["StatusServiceUrl"].ToString().Trim(),
                                             row["ActiveCode"].ToString().Trim(),
                                             count);

                        if (!string.IsNullOrWhiteSpace(resultString))
                        {
                            return CommonUtil.ResponseData(ResponseType.Failed, resultString);
                        }

                        count++;
                    }
                }

                return CommonUtil.ResponseData(ResponseType.Success);
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return CommonUtil.ResponseData(ResponseType.Failed, ex.Message);
            }
        }

        /// <summary>
        /// 미디어서버 정보 수정.
        /// </summary>
        /// <param name="stream">수정 할 미디어서버 정보.</param>
        public Stream UpdateRecorderInfo(Stream stream)
        {
            try
            {
                var resultString = string.Empty;

                using (var readStream = new StreamReader(stream))
                {
                    var recorderDataSet = new DataSet();

                    recorderDataSet.ReadXml(readStream);

                    var row = recorderDataSet.Tables[0].Rows[0];

                    resultString = DataManager.GetInstance.SetData.UpdateRecorder(
                                                 row["RecorderID"].ToString().Trim(),
                                                 row["RecorderIP"].ToString().Trim(),
                                                 row["RecorderDescription"].ToString().Trim(),
                                                 int.Parse(row["RESTServerPort"].ToString()),
                                                 int.Parse(row["IMSPServerPort"].ToString()),
                                                 row["EventRecordStorageList"].ToString().Trim(),
                                                 row["StatusServiceUrl"].ToString().Trim(),
                                                 row["ActiveCode"].ToString().Trim());
                }

                return CommonUtil.ResponseData(string.IsNullOrWhiteSpace(resultString) ? ResponseType.Success : ResponseType.Failed, resultString);
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return CommonUtil.ResponseData(ResponseType.Failed, ex.Message);
            }
        }

        /// <summary>
        /// 레코더 정보 삭제.
        /// </summary>
        /// <param name="stream">삭제 할 레코더 정보.</param>
        public Stream DeleteRecorderInfo(Stream stream)
        {
            try
            {
                using (var readStream = new StreamReader(stream))
                {
                    var recorderDataSetTable = new DataTable();

                    recorderDataSetTable.ReadXml(readStream);

                    var resultString = string.Empty;

                    foreach (var recorderId in
                            from DataRow row in recorderDataSetTable.Rows
                            select row["RecorderID"].ToString().Trim())
                    {
                        resultString = DataManager.GetInstance.SetData.DeleteRecorder(recorderId);

                        if (!string.IsNullOrWhiteSpace(resultString))
                        {
                            return CommonUtil.ResponseData(ResponseType.Failed, resultString);
                        }
                    }

                    var recorderDataSet = new DataSet();
                    recorderDataSet.Tables.Add(recorderDataSetTable);
                }

                return CommonUtil.ResponseData(ResponseType.Success);
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return CommonUtil.ResponseData(ResponseType.Failed, ex.Message);
            }
        }

        #endregion // Recorder

        #region ViewServer

        /// <summary>
        /// 뷰서버 정보 추가.
        /// </summary>
        /// <param name="stream">추가 할 뷰서버 정보.</param>
        public Stream AddViewServerInfo(Stream stream)
        {
            try
            {
                using (var readStream = new StreamReader(stream))
                {
                    var viewServerDataSet = new DataSet();

                    viewServerDataSet.ReadXml(readStream);

                    foreach (DataRow row in viewServerDataSet.Tables[0].Rows)
                    {
                        var serverType = "COMMAND";

                        if (!string.IsNullOrEmpty(row["ViewServerType"].ToString().Trim()))
                        {
                            serverType = row["ViewServerType"].ToString().Trim();
                        }

                        var resultString = DataManager.GetInstance.SetData.InsertViewServer(
                                                    row["ViewServerID"].ToString().Trim(),
                                                    row["ViewServerIP"].ToString().Trim(),
                                                    IsCheckedForNullableInt.IsCheckedData(row["ViewServerPort"].ToString().Trim()),
                                                    row["ServiceUrl"].ToString().Trim(),
                                                    serverType,
                                                    row["ViewServerDescription"].ToString().Trim(),
                                                    row["ActiveCode"].ToString().Trim());

                        if (!string.IsNullOrWhiteSpace(resultString))
                        {
                            return CommonUtil.ResponseData(ResponseType.Failed, resultString);
                        }
                    }
                }

                return CommonUtil.ResponseData(ResponseType.Success);
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return CommonUtil.ResponseData(ResponseType.Failed, ex.Message);
            }
        }

        public Stream ImportViewServerInfo(Stream stream)
        {
            try
            {
                using (var readStream = new StreamReader(stream))
                {
                    var viewServerDataSet = new DataSet();

                    viewServerDataSet.ReadXml(readStream);

                    var count = 0;

                    foreach (DataRow row in viewServerDataSet.Tables[0].Rows)
                    {
                        var serverType = "COMMAND";

                        if (!string.IsNullOrEmpty(row["ViewServerType"].ToString().Trim()))
                        {
                            serverType = row["ViewServerType"].ToString().Trim();
                        }

                        var resultString = DataManager.GetInstance.SetData.ImportViewServer(
                                                    row["ViewServerID"].ToString().Trim(),
                                                    row["ViewServerIP"].ToString().Trim(),
                                                    IsCheckedForNullableInt.IsCheckedData(row["ViewServerPort"].ToString().Trim()),
                                                    row["ServiceUrl"].ToString().Trim(),
                                                    serverType,
                                                    row["ViewServerDescription"].ToString().Trim(),
                                                    row["ActiveCode"].ToString().Trim(),
                                                    count);

                        count++;

                        if (!string.IsNullOrWhiteSpace(resultString))
                        {
                            return CommonUtil.ResponseData(ResponseType.Failed, resultString);
                        }
                    }
                }

                return CommonUtil.ResponseData(ResponseType.Success);
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return CommonUtil.ResponseData(ResponseType.Failed, ex.Message);
            }
        }

        /// <summary>
        /// 미디어서버 정보 수정.
        /// </summary>
        /// <param name="stream">수정 할 미디어서버 정보.</param>
        public Stream UpdateViewServerInfo(Stream stream)
        {
            try
            {
                using (var readStream = new StreamReader(stream))
                {
                    var viewServerDataSet = new DataSet();

                    viewServerDataSet.ReadXml(readStream);

                    var row = viewServerDataSet.Tables[0].Rows[0];

                    var serverType = "COMMAND";

                    if (!string.IsNullOrEmpty(row["ViewServerType"].ToString().Trim()))
                    {
                        serverType = row["ViewServerType"].ToString().Trim();
                    }

                    var resultString = DataManager.GetInstance.SetData.UpdateViewServer(
                                            row["ViewServerID"].ToString().Trim(),
                                            row["ViewServerIP"].ToString().Trim(),
                                            IsCheckedForNullableInt.IsCheckedData(row["ViewServerPort"].ToString().Trim()),
                                            row["ServiceUrl"].ToString().Trim(),
                                            serverType,
                                            row["ViewServerDescription"].ToString().Trim(),
                                            row["ActiveCode"].ToString().Trim());

                    return CommonUtil.ResponseData(
                        string.IsNullOrWhiteSpace(resultString) ? ResponseType.Success : ResponseType.Failed,
                        resultString);
                }
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return CommonUtil.ResponseData(ResponseType.Failed, ex.Message);
            }
        }

        /// <summary>
        /// 미디어서버 정보 삭제.
        /// </summary>
        /// <param name="stream">삭제 할 미디어서버 정보.</param>
        public Stream DeleteViewServerInfo(Stream stream)
        {
            try
            {
                using (var readStream = new StreamReader(stream))
                {
                    var viewServerInfoDataTable = new DataTable();

                    viewServerInfoDataTable.ReadXml(readStream);

                    string resultString;

                    foreach (var viewServerId in
                            from DataRow row in viewServerInfoDataTable.Rows
                            select row["ViewServerID"].ToString().Trim())
                    {
                        resultString = DataManager.GetInstance.SetData.DeleteViewServer(viewServerId);

                        if (!string.IsNullOrWhiteSpace(resultString))
                        {
                            return CommonUtil.ResponseData(ResponseType.Failed, resultString);
                        }
                    }

                    var viewServerDataSet = new DataSet();
                    viewServerDataSet.Tables.Add(viewServerInfoDataTable);
                }

                return CommonUtil.ResponseData(ResponseType.Success);
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return CommonUtil.ResponseData(ResponseType.Failed, ex.Message);
            }
        }

        #endregion // ViewServer

        #region Display

        /// <summary>
        /// Display 정보 추가.
        /// </summary>
        /// <param name="stream">추가 할 Display 정보.</param>
        public Stream AddDisplayInfo(Stream stream)
        {
            try
            {
                using (var readStream = new StreamReader(stream))
                {
                    var viewServerDataSet = new DataSet();

                    viewServerDataSet.ReadXml(readStream);

                    foreach (DataRow row in viewServerDataSet.Tables[0].Rows)
                    {
                        var resultString = DataManager.GetInstance.SetData.InsertDisplay(
                                                    row["DisplayID"].ToString().Trim(),
                                                    row["DisplayIP"].ToString().Trim(),
                                                    row["ServiceUrl"].ToString().Trim(),
                                                    row["DisplayDescription"].ToString().Trim(),
                                                    row["ActiveCode"].ToString().Trim());

                        if (!string.IsNullOrWhiteSpace(resultString))
                        {
                            return CommonUtil.ResponseData(ResponseType.Failed, resultString);
                        }
                    }
                }

                return CommonUtil.ResponseData(ResponseType.Success);
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return CommonUtil.ResponseData(ResponseType.Failed, ex.Message);
            }
        }

        public Stream ImportDisplayInfo(Stream stream)
        {
            try
            {
                using (var readStream = new StreamReader(stream))
                {
                    var viewServerDataSet = new DataSet();

                    viewServerDataSet.ReadXml(readStream);

                    var count = 0;

                    foreach (DataRow row in viewServerDataSet.Tables[0].Rows)
                    {
                        var resultString = DataManager.GetInstance.SetData.ImportDisplay(
                                                    row["DisplayID"].ToString().Trim(),
                                                    row["DisplayIP"].ToString().Trim(),
                                                    row["ServiceUrl"].ToString().Trim(),
                                                    row["DisplayDescription"].ToString().Trim(),
                                                    row["ActiveCode"].ToString().Trim(),
                                                    count);

                        count++;

                        if (!string.IsNullOrWhiteSpace(resultString))
                        {
                            return CommonUtil.ResponseData(ResponseType.Failed, resultString);
                        }
                    }
                }

                return CommonUtil.ResponseData(ResponseType.Success);
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return CommonUtil.ResponseData(ResponseType.Failed, ex.Message);
            }
        }

        /// <summary>
        /// Display 정보 수정.
        /// </summary>
        /// <param name="stream">수정 할 Display 정보.</param>
        public Stream UpdateDisplayInfo(Stream stream)
        {
            try
            {
                using (var readStream = new StreamReader(stream))
                {
                    var viewServerDataSet = new DataSet();

                    viewServerDataSet.ReadXml(readStream);

                    var row = viewServerDataSet.Tables[0].Rows[0];

                    var resultString = DataManager.GetInstance.SetData.UpdateDisplay(
                                            row["DisplayID"].ToString().Trim(),
                                            row["DisplayIP"].ToString().Trim(),
                                            row["ServiceUrl"].ToString().Trim(),
                                            row["DisplayDescription"].ToString().Trim(),
                                            row["ActiveCode"].ToString().Trim());

                    return CommonUtil.ResponseData(
                        string.IsNullOrWhiteSpace(resultString) ? ResponseType.Success : ResponseType.Failed,
                        resultString);
                }
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return CommonUtil.ResponseData(ResponseType.Failed, ex.Message);
            }
        }

        /// <summary>
        /// Display 정보 삭제.
        /// </summary>
        /// <param name="stream">삭제 할 Display 정보.</param>
        public Stream DeleteDisplayInfo(Stream stream)
        {
            try
            {
                using (var readStream = new StreamReader(stream))
                {
                    var viewServerInfoDataTable = new DataTable();

                    viewServerInfoDataTable.ReadXml(readStream);

                    string resultString;

                    foreach (var viewServerId in
                            from DataRow row in viewServerInfoDataTable.Rows
                            select row["DisplayID"].ToString().Trim())
                    {
                        resultString = DataManager.GetInstance.SetData.DeleteDisplay(viewServerId);

                        if (!string.IsNullOrWhiteSpace(resultString))
                        {
                            return CommonUtil.ResponseData(ResponseType.Failed, resultString);
                        }
                    }

                    var viewServerDataSet = new DataSet();
                    viewServerDataSet.Tables.Add(viewServerInfoDataTable);
                }

                return CommonUtil.ResponseData(ResponseType.Success);
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return CommonUtil.ResponseData(ResponseType.Failed, ex.Message);
            }
        }

        #endregion // Display

        #region CameraControllerService

        /// <summary>
        /// Camera Controller Service 정보 추가.
        /// </summary>
        /// <param name="stream">추가 할 Camera Controller Service 정보.</param>
        public Stream AddCameraControllerServiceInfo(Stream stream)
        {
            try
            {
                using (var readStream = new StreamReader(stream))
                {
                    var viewServerDataSet = new DataSet();

                    viewServerDataSet.ReadXml(readStream);

                    foreach (DataRow row in viewServerDataSet.Tables[0].Rows)
                    {
                        var resultString = DataManager.GetInstance.SetData.InsertCameraControllerService(
                                                    row["CameraControllerServiceID"].ToString().Trim(),
                                                    row["CameraControllerServiceIP"].ToString().Trim(),
                                                    row["ServiceUrl"].ToString().Trim(),
                                                    row["CameraControllerServiceDescription"].ToString().Trim(),
                                                    row["ActiveCode"].ToString().Trim());

                        if (!string.IsNullOrWhiteSpace(resultString))
                        {
                            return CommonUtil.ResponseData(ResponseType.Failed, resultString);
                        }
                    }
                }

                return CommonUtil.ResponseData(ResponseType.Success);
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return CommonUtil.ResponseData(ResponseType.Failed, ex.Message);
            }
        }

        public Stream ImportCameraControllerServiceInfo(Stream stream)
        {
            try
            {
                using (var readStream = new StreamReader(stream))
                {
                    var viewServerDataSet = new DataSet();

                    viewServerDataSet.ReadXml(readStream);

                    var count = 0;

                    foreach (DataRow row in viewServerDataSet.Tables[0].Rows)
                    {
                        var resultString = DataManager.GetInstance.SetData.ImportCameraControllerService(
                                                    row["CameraControllerServiceID"].ToString().Trim(),
                                                    row["CameraControllerServiceIP"].ToString().Trim(),
                                                    row["ServiceUrl"].ToString().Trim(),
                                                    row["CameraControllerServiceDescription"].ToString().Trim(),
                                                    row["ActiveCode"].ToString().Trim(),
                                                    count);

                        count++;

                        if (!string.IsNullOrWhiteSpace(resultString))
                        {
                            return CommonUtil.ResponseData(ResponseType.Failed, resultString);
                        }
                    }
                }

                return CommonUtil.ResponseData(ResponseType.Success);
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return CommonUtil.ResponseData(ResponseType.Failed, ex.Message);
            }
        }

        /// <summary>
        /// Camera Controller Service 정보 수정.
        /// </summary>
        /// <param name="stream">수정 할 Camera Controller Service 정보.</param>
        public Stream UpdateCameraControllerServiceInfo(Stream stream)
        {
            try
            {
                using (var readStream = new StreamReader(stream))
                {
                    var viewServerDataSet = new DataSet();

                    viewServerDataSet.ReadXml(readStream);

                    var row = viewServerDataSet.Tables[0].Rows[0];

                    var resultString = DataManager.GetInstance.SetData.UpdateCameraControllerService(
                                            row["CameraControllerServiceID"].ToString().Trim(),
                                            row["CameraControllerServiceIP"].ToString().Trim(),
                                            row["ServiceUrl"].ToString().Trim(),
                                            row["CameraControllerServiceDescription"].ToString().Trim(),
                                            row["ActiveCode"].ToString().Trim());

                    return CommonUtil.ResponseData(
                        string.IsNullOrWhiteSpace(resultString) ? ResponseType.Success : ResponseType.Failed,
                        resultString);
                }
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return CommonUtil.ResponseData(ResponseType.Failed, ex.Message);
            }
        }

        /// <summary>
        /// Camera Controller Service 정보 삭제.
        /// </summary>
        /// <param name="stream">삭제 할 Camera Controller Service 정보.</param>
        public Stream DeleteCameraControllerServiceInfo(Stream stream)
        {
            try
            {
                using (var readStream = new StreamReader(stream))
                {
                    var viewServerInfoDataTable = new DataTable();

                    viewServerInfoDataTable.ReadXml(readStream);

                    string resultString;

                    foreach (var viewServerId in
                            from DataRow row in viewServerInfoDataTable.Rows
                            select row["CameraControllerServiceID"].ToString().Trim())
                    {
                        resultString = DataManager.GetInstance.SetData.DeleteCameraControllerService(viewServerId);

                        if (!string.IsNullOrWhiteSpace(resultString))
                        {
                            return CommonUtil.ResponseData(ResponseType.Failed, resultString);
                        }
                    }

                    var viewServerDataSet = new DataSet();
                    viewServerDataSet.Tables.Add(viewServerInfoDataTable);
                }

                return CommonUtil.ResponseData(ResponseType.Success);
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return CommonUtil.ResponseData(ResponseType.Failed, ex.Message);
            }
        }

        #endregion // CameraControllerService

        #region ExternalCommandService

        /// <summary>
        /// External Command Service 정보 추가.
        /// </summary>
        /// <param name="stream">추가 할 External Command Service 정보.</param>
        public Stream AddExternalCommandServiceInfo(Stream stream)
        {
            try
            {
                using (var readStream = new StreamReader(stream))
                {
                    var viewServerDataSet = new DataSet();

                    viewServerDataSet.ReadXml(readStream);

                    foreach (DataRow row in viewServerDataSet.Tables[0].Rows)
                    {
                        var resultString = DataManager.GetInstance.SetData.InsertExternalCommandService(
                                                    row["ExternalCommandServiceID"].ToString().Trim(),
                                                    row["ExternalCommandServiceIP"].ToString().Trim(),
                                                    row["ServiceUrl"].ToString().Trim(),
                                                    row["ExternalCommandServiceDescription"].ToString().Trim(),
                                                    row["ActiveCode"].ToString().Trim());

                        if (!string.IsNullOrWhiteSpace(resultString))
                        {
                            return CommonUtil.ResponseData(ResponseType.Failed, resultString);
                        }
                    }
                }

                return CommonUtil.ResponseData(ResponseType.Success);
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return CommonUtil.ResponseData(ResponseType.Failed, ex.Message);
            }
        }

        public Stream ImportExternalCommandServiceInfo(Stream stream)
        {
            try
            {
                using (var readStream = new StreamReader(stream))
                {
                    var viewServerDataSet = new DataSet();

                    viewServerDataSet.ReadXml(readStream);

                    var count = 0;

                    foreach (DataRow row in viewServerDataSet.Tables[0].Rows)
                    {
                        var resultString = DataManager.GetInstance.SetData.ImportExternalCommandService(
                                                    row["ExternalCommandServiceID"].ToString().Trim(),
                                                    row["ExternalCommandServiceIP"].ToString().Trim(),
                                                    row["ServiceUrl"].ToString().Trim(),
                                                    row["ExternalCommandServiceDescription"].ToString().Trim(),
                                                    row["ActiveCode"].ToString().Trim(),
                                                    count);

                        count++;

                        if (!string.IsNullOrWhiteSpace(resultString))
                        {
                            return CommonUtil.ResponseData(ResponseType.Failed, resultString);
                        }
                    }
                }

                return CommonUtil.ResponseData(ResponseType.Success);
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return CommonUtil.ResponseData(ResponseType.Failed, ex.Message);
            }
        }

        /// <summary>
        /// External Command Service 정보 수정.
        /// </summary>
        /// <param name="stream">수정 할 External Command Service 정보.</param>
        public Stream UpdateExternalCommandServiceInfo(Stream stream)
        {
            try
            {
                using (var readStream = new StreamReader(stream))
                {
                    var viewServerDataSet = new DataSet();

                    viewServerDataSet.ReadXml(readStream);

                    var row = viewServerDataSet.Tables[0].Rows[0];

                    var resultString = DataManager.GetInstance.SetData.UpdateExternalCommandService(
                                            row["ExternalCommandServiceID"].ToString().Trim(),
                                            row["ExternalCommandServiceIP"].ToString().Trim(),
                                            row["ServiceUrl"].ToString().Trim(),
                                            row["ExternalCommandServiceDescription"].ToString().Trim(),
                                            row["ActiveCode"].ToString().Trim());

                    return CommonUtil.ResponseData(
                        string.IsNullOrWhiteSpace(resultString) ? ResponseType.Success : ResponseType.Failed,
                        resultString);
                }
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return CommonUtil.ResponseData(ResponseType.Failed, ex.Message);
            }
        }

        /// <summary>
        /// External Command Service 정보 삭제.
        /// </summary>
        /// <param name="stream">삭제 할 External Command Service 정보.</param>
        public Stream DeleteExternalCommandServiceInfo(Stream stream)
        {
            try
            {
                using (var readStream = new StreamReader(stream))
                {
                    var viewServerInfoDataTable = new DataTable();

                    viewServerInfoDataTable.ReadXml(readStream);

                    string resultString;

                    foreach (var viewServerId in
                            from DataRow row in viewServerInfoDataTable.Rows
                            select row["ExternalCommandServiceID"].ToString().Trim())
                    {
                        resultString = DataManager.GetInstance.SetData.DeleteExternalCommandService(viewServerId);

                        if (!string.IsNullOrWhiteSpace(resultString))
                        {
                            return CommonUtil.ResponseData(ResponseType.Failed, resultString);
                        }
                    }

                    var viewServerDataSet = new DataSet();
                    viewServerDataSet.Tables.Add(viewServerInfoDataTable);
                }

                return CommonUtil.ResponseData(ResponseType.Success);
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return CommonUtil.ResponseData(ResponseType.Failed, ex.Message);
            }
        }

        #endregion // ExternalCommandService

        #region VideoSpec

        /// <summary>
        /// 비디오스펙 정보 추가.
        /// </summary>
        /// <param name="stream">추가 할 비디오스펙 정보.</param>
        public Stream AddVideoSpecInfo(Stream stream)
        {
            try
            {
                using (var readStream = new StreamReader(stream))
                {
                    var videoSpecDataSet = new DataSet();

                    videoSpecDataSet.ReadXml(readStream);

                    var resultString = string.Empty;

                    foreach (DataRow row in videoSpecDataSet.Tables[0].Rows)
                    {
                        resultString = DataManager.GetInstance.SetData.InsertVideoSpec(
                                            row["VideoSpecID"].ToString().Trim(),
                                            row["VideoSpecName"].ToString().Trim(),
                                            int.Parse(row["Width"].ToString().Trim()),
                                            int.Parse(row["Height"].ToString().Trim()),
                                            int.Parse(row["FPS"].ToString().Trim()),
                                            row["Codec"].ToString().Trim(),
                                            row["RtspUrl"].ToString().Trim(),
                                            bool.Parse(row["IsPOD"].ToString().Trim()),
                                            int.Parse(row["Bitrate"].ToString().Trim()),
                                            row["ExtraParams"].ToString().Trim());

                        if (!string.IsNullOrWhiteSpace(resultString))
                        {
                            break;
                        }
                    }

                    return CommonUtil.ResponseData(
                            string.IsNullOrWhiteSpace(resultString) ? ResponseType.Success : ResponseType.Failed,
                            resultString);
                }
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return CommonUtil.ResponseData(ResponseType.Failed, ex.Message);
            }
        }

        public Stream ImportVideoSpecInfo(Stream stream)
        {
            try
            {
                using (var readStream = new StreamReader(stream))
                {
                    var videoSpecDataSet = new DataSet();

                    videoSpecDataSet.ReadXml(readStream);

                    var resultString = string.Empty;
                    var count = 0;

                    foreach (DataRow row in videoSpecDataSet.Tables[0].Rows)
                    {
                        resultString = DataManager.GetInstance.SetData.ImportVideoSpec(
                                            row["VideoSpecID"].ToString().Trim(),
                                            row["VideoSpecName"].ToString().Trim(),
                                            int.Parse(row["Width"].ToString().Trim()),
                                            int.Parse(row["Height"].ToString().Trim()),
                                            int.Parse(row["FPS"].ToString().Trim()),
                                            row["Codec"].ToString().Trim(),
                                            row["RtspUrl"].ToString().Trim(),
                                            bool.Parse(row["IsPOD"].ToString().Trim()),
                                            int.Parse(row["Bitrate"].ToString().Trim()),
                                            row["ExtraParams"].ToString().Trim(),
                                            count);

                        if (!string.IsNullOrWhiteSpace(resultString))
                        {
                            break;
                        }

                        count++;
                    }

                    return CommonUtil.ResponseData(
                            string.IsNullOrWhiteSpace(resultString) ? ResponseType.Success : ResponseType.Failed,
                            resultString);
                }
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return CommonUtil.ResponseData(ResponseType.Failed, ex.Message);
            }
        }

        /// <summary>
        /// 비디오스펙 정보 수정.
        /// </summary>
        /// <param name="stream">수정 할 비디오스펙 정보.</param>
        public Stream UpdateVideoSpecInfo(Stream stream)
        {
            try
            {
                using (var readStream = new StreamReader(stream))
                {
                    var videoSpecDataSet = new DataSet();

                    videoSpecDataSet.ReadXml(readStream);

                    var row = videoSpecDataSet.Tables[0].Rows[0];

                    var resultString = DataManager.GetInstance.SetData.UpdateVideoSpec(
                                    row["VideoSpecID"].ToString().Trim(),
                                    row["VideoSpecName"].ToString().Trim(),
                                    int.Parse(row["Width"].ToString().Trim()),
                                    int.Parse(row["Height"].ToString().Trim()),
                                    int.Parse(row["FPS"].ToString().Trim()),
                                    row["Codec"].ToString().Trim(),
                                    row["RtspUrl"].ToString().Trim(),
                                    bool.Parse(row["IsPOD"].ToString().Trim()),
                                    int.Parse(row["Bitrate"].ToString().Trim()),
                                    row["ExtraParams"].ToString().Trim());

                    return CommonUtil.ResponseData(
                            string.IsNullOrWhiteSpace(resultString) ? ResponseType.Success : ResponseType.Failed,
                            resultString);
                }
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return CommonUtil.ResponseData(ResponseType.Failed, ex.Message);
            }
        }

        /// <summary>
        /// 비디오스펙 정보 삭제.
        /// </summary>
        /// <param name="stream">삭제 할 비디오스펙 정보.</param>
        public Stream DeleteVideoSpecInfo(Stream stream)
        {
            try
            {
                using (var readStream = new StreamReader(stream))
                {
                    var videoSpecDataTable = new DataTable();

                    videoSpecDataTable.ReadXml(readStream);

                    var videoSpecDataSet = new DataSet();
                    videoSpecDataSet.Tables.Add(videoSpecDataTable);

                    var dataSet = new DataSet();
                    dataSet.Tables.Add(videoSpecDataTable.Copy());

                    foreach (var videoSpecId in
                            from DataRow row in videoSpecDataTable.Rows select row["VideoSpecID"].ToString().Trim())
                    {
                        var deleteResultString = DataManager.GetInstance.SetData.DeleteVideoSpec(videoSpecId);

                        if (!string.IsNullOrWhiteSpace(deleteResultString))
                        {
                            return CommonUtil.ResponseData(ResponseType.Failed, deleteResultString);
                        }

                        var deleteMappingResultString = DataManager.GetInstance.SetData.DeleteCameraVideoMappingForDeleteVideoSpec(videoSpecId);

                        if (!string.IsNullOrWhiteSpace(deleteMappingResultString))
                        {
                            return CommonUtil.ResponseData(ResponseType.Failed, deleteMappingResultString);
                        }
                    }
                }

                return CommonUtil.ResponseData(ResponseType.Success);
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return CommonUtil.ResponseData(ResponseType.Failed, ex.Message);
            }
        }

        #endregion // VideoSpec

        #region PTZ Connection

        /// <summary>
        /// PTZ 연결 정보 추가.
        /// </summary>
        /// <param name="stream">추가 할 PTZ 연결 정보.</param>
        public Stream AddPtzConnectionInfo(Stream stream)
        {
            try
            {
                using (var readStream = new StreamReader(stream))
                {
                    var ptzConnectionDataSet = new DataSet();

                    ptzConnectionDataSet.ReadXml(readStream);

                    var resultString = string.Empty;
                    foreach (DataRow row in ptzConnectionDataSet.Tables[0].Rows)
                    {
                        resultString = DataManager.GetInstance.SetData.InsertPtzConnectionInfo(
                                            row["PtzConnectionInfoID"].ToString().Trim(),
                                            row["ConnectIP"].ToString().Trim(),
                                            IsCheckedForNullableInt.IsCheckedData(row["ConnectPort"].ToString().Trim()),
                                            row["ConnectID"].ToString().Trim(),
                                            row["ConnectPassword"].ToString().Trim(),
                                            row["SerialPort"].ToString().Trim(),
                                            IsCheckedForNullableInt.IsCheckedData(row["BaudRate"].ToString().Trim()),
                                            IsCheckedForNullableInt.IsCheckedData(row["ParityBit"].ToString().Trim()),
                                            IsCheckedForNullableInt.IsCheckedData(row["DataCount"].ToString().Trim()),
                                            IsCheckedForNullableInt.IsCheckedData(row["StopBit"].ToString().Trim()),
                                            row["Channel"].ToString().Trim(),
                                            IsCheckedForNullableInt.IsCheckedData(row["AudioPort"].ToString().Trim()),
                                            row["GroupSelectData"].ToString().Trim());

                        if (!string.IsNullOrWhiteSpace(resultString))
                            break;
                    }

                    return CommonUtil.ResponseData(
                            string.IsNullOrWhiteSpace(resultString) ? ResponseType.Success : ResponseType.Failed,
                            resultString);
                }
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return CommonUtil.ResponseData(ResponseType.Failed, ex.Message);
            }
        }

        public Stream ImportPtzConnectionInfo(Stream stream)
        {
            try
            {
                using (var readStream = new StreamReader(stream))
                {
                    var ptzConnectionDataSet = new DataSet();

                    ptzConnectionDataSet.ReadXml(readStream);

                    var resultString = string.Empty;

                    var count = 0;

                    foreach (DataRow row in ptzConnectionDataSet.Tables[0].Rows)
                    {
                        resultString = DataManager.GetInstance.SetData.ImportPtzConnectionInfo(
                                            row["PtzConnectionInfoID"].ToString().Trim(),
                                            row["ConnectIP"].ToString().Trim(),
                                            IsCheckedForNullableInt.IsCheckedData(row["ConnectPort"].ToString().Trim()),
                                            row["ConnectID"].ToString().Trim(),
                                            row["ConnectPassword"].ToString().Trim(),
                                            row["SerialPort"].ToString().Trim(),
                                            IsCheckedForNullableInt.IsCheckedData(row["BaudRate"].ToString().Trim()),
                                            IsCheckedForNullableInt.IsCheckedData(row["ParityBit"].ToString().Trim()),
                                            IsCheckedForNullableInt.IsCheckedData(row["DataCount"].ToString().Trim()),
                                            IsCheckedForNullableInt.IsCheckedData(row["StopBit"].ToString().Trim()),
                                            row["Channel"].ToString().Trim(),
                                            IsCheckedForNullableInt.IsCheckedData(row["AudioPort"].ToString().Trim()),
                                            row["GroupSelectData"].ToString().Trim(),
                                            count);

                        if (!string.IsNullOrWhiteSpace(resultString))
                        {
                            break;
                        }

                        count++;
                    }

                    return CommonUtil.ResponseData(
                            string.IsNullOrWhiteSpace(resultString) ? ResponseType.Success : ResponseType.Failed,
                            resultString);
                }
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return CommonUtil.ResponseData(ResponseType.Failed, ex.Message);
            }
        }

        /// <summary>
        /// PTZ 연결 정보 수정.
        /// </summary>
        /// <param name="stream">수정 할 PTZ 연결 정보</param>
        public Stream UpdatePtzConnectionInfo(Stream stream)
        {
            try
            {
                using (var readStream = new StreamReader(stream))
                {
                    var ptzConnectionDataSet = new DataSet();

                    ptzConnectionDataSet.ReadXml(readStream);

                    var row = ptzConnectionDataSet.Tables[0].Rows[0];

                    var resultString = DataManager.GetInstance.SetData.UpdatePtzConnectionInfo(
                                        row["PtzConnectionInfoID"].ToString().Trim(),
                                        row["ConnectIP"].ToString().Trim(),
                                        IsCheckedForNullableInt.IsCheckedData(row["ConnectPort"].ToString().Trim()),
                                        row["ConnectID"].ToString().Trim(),
                                        row["ConnectPassword"].ToString().Trim(),
                                        row["SerialPort"].ToString().Trim(),
                                        IsCheckedForNullableInt.IsCheckedData(row["BaudRate"].ToString().Trim()),
                                        IsCheckedForNullableInt.IsCheckedData(row["ParityBit"].ToString().Trim()),
                                        IsCheckedForNullableInt.IsCheckedData(row["DataCount"].ToString().Trim()),
                                        IsCheckedForNullableInt.IsCheckedData(row["StopBit"].ToString().Trim()),
                                        row["Channel"].ToString().Trim(),
                                        IsCheckedForNullableInt.IsCheckedData(row["AudioPort"].ToString().Trim()),
                                        row["GroupSelectData"].ToString().Trim());

                    return CommonUtil.ResponseData(
                            string.IsNullOrWhiteSpace(resultString) ? ResponseType.Success : ResponseType.Failed,
                            resultString);
                }
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return CommonUtil.ResponseData(ResponseType.Failed, ex.Message);
            }
        }

        /// <summary>
        /// PTZ 연결 정보 삭제.
        /// </summary>
        /// <param name="stream">삭제 할 PTZ 연결 정보.</param>
        public Stream DeletePtzConnectionInfo(Stream stream)
        {
            try
            {
                using (var readStream = new StreamReader(stream))
                {
                    var ptzConnectionDataTable = new DataTable();

                    ptzConnectionDataTable.ReadXml(readStream);

                    foreach (var ptzConnectionInfoId in
                            from DataRow row in ptzConnectionDataTable.Rows
                            select row["PtzConnectionInfoID"].ToString().Trim())
                    {
                        var deleteResultString = DataManager.GetInstance.SetData.DeletePtzConnectionInfo(ptzConnectionInfoId);
                        if (!string.IsNullOrWhiteSpace(deleteResultString))
                        {
                            return CommonUtil.ResponseData(ResponseType.Failed, deleteResultString);
                        }

                        var deleteMappingResultString =
                            DataManager.GetInstance.SetData.UpdateCameraForDeletePtzConnectionInfo(ptzConnectionInfoId);
                        if (!string.IsNullOrWhiteSpace(deleteMappingResultString))
                        {
                            return CommonUtil.ResponseData(ResponseType.Failed, deleteMappingResultString);
                        }
                    }
                }

                return CommonUtil.ResponseData(ResponseType.Success);
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return CommonUtil.ResponseData(ResponseType.Failed, ex.Message);
            }
        }

        #endregion // PTZ Connection

        #region CameraController

        /// <summary>
        /// 카메라 컨트롤 정보를 추가한다.
        /// </summary>
        /// <param name="stream">추가 할 카메라 컨트롤 정보.</param>
        public Stream AddCameraControlSettingForConfiguration(Stream stream)
        {
            try
            {
                using (var readStream = new StreamReader(stream))
                {
                    var cameraControllerSettingDataSet = new DataSet();

                    cameraControllerSettingDataSet.ReadXml(readStream);

                    var resultString = string.Empty;
                    foreach (DataRow row in cameraControllerSettingDataSet.Tables[0].Rows)
                    {
                        resultString = DataManager.GetInstance.SetData.InsertCameraControlSetting(
                                            row["CameraControlSettingID"].ToString().Trim(),
                                            row["ModuleFileName"].ToString().Trim(),
                                            bool.Parse(row["UsePanTilt"].ToString().Trim()),
                                            bool.Parse(row["UseSpeed"].ToString().Trim()),
                                            bool.Parse(row["UseZoom"].ToString().Trim()),
                                            bool.Parse(row["UseFocus"].ToString().Trim()),
                                            bool.Parse(row["UseIris"].ToString().Trim()),
                                            bool.Parse(row["UsePreset"].ToString().Trim()),
                                            int.Parse(row["PresetCount"].ToString().Trim()),
                                            bool.Parse(row["UseCameraPower"].ToString().Trim()),
                                            bool.Parse(row["UseLightPower"].ToString().Trim()),
                                            bool.Parse(row["UseAux1Power"].ToString().Trim()),
                                            bool.Parse(row["UseAux2Power"].ToString().Trim()),
                                            Int32.Parse(row["AudioMode"].ToString().Trim()),
                                            row["Description"].ToString().Trim());

                        if (!string.IsNullOrWhiteSpace(resultString))
                        {
                            break;
                        }
                    }

                    return CommonUtil.ResponseData(
                            string.IsNullOrWhiteSpace(resultString) ? ResponseType.Success : ResponseType.Failed,
                            resultString);
                }
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return CommonUtil.ResponseData(ResponseType.Failed, ex.Message);
            }
        }

        public Stream ImportCameraControlSettingInfo(Stream stream)
        {
            try
            {
                using (var readStream = new StreamReader(stream))
                {
                    var cameraControllerSettingDataSet = new DataSet();

                    cameraControllerSettingDataSet.ReadXml(readStream);

                    var resultString = string.Empty;

                    var count = 0;

                    foreach (DataRow row in cameraControllerSettingDataSet.Tables[0].Rows)
                    {
                        resultString = DataManager.GetInstance.SetData.ImportCameraControlSetting(
                                            row["CameraControlSettingID"].ToString().Trim(),
                                            row["ModuleFileName"].ToString().Trim(),
                                            bool.Parse(row["UsePanTilt"].ToString().Trim()),
                                            bool.Parse(row["UseSpeed"].ToString().Trim()),
                                            bool.Parse(row["UseZoom"].ToString().Trim()),
                                            bool.Parse(row["UseFocus"].ToString().Trim()),
                                            bool.Parse(row["UseIris"].ToString().Trim()),
                                            bool.Parse(row["UsePreset"].ToString().Trim()),
                                            int.Parse(row["PresetCount"].ToString().Trim()),
                                            bool.Parse(row["UseCameraPower"].ToString().Trim()),
                                            bool.Parse(row["UseLightPower"].ToString().Trim()),
                                            bool.Parse(row["UseAux1Power"].ToString().Trim()),
                                            bool.Parse(row["UseAux2Power"].ToString().Trim()),
                                            int.Parse(row["AudioMode"].ToString().Trim()),
                                            row["Description"].ToString().Trim(),
                                            count);

                        if (!string.IsNullOrWhiteSpace(resultString))
                        {
                            break;
                        }

                        count++;
                    }

                    return CommonUtil.ResponseData(
                            string.IsNullOrWhiteSpace(resultString) ? ResponseType.Success : ResponseType.Failed,
                            resultString);
                }
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return CommonUtil.ResponseData(ResponseType.Failed, ex.Message);
            }
        }

        /// <summary>
        /// 카메라 컨트롤 정보를 수정한다.
        /// </summary>
        /// <param name="stream">수정 할 카메라 컨트롤 정보.</param>
        public Stream UpdateCameraControlSettingForConfiguration(Stream stream)
        {
            try
            {
                using (var readStream = new StreamReader(stream))
                {
                    var cameraControllerSettingDataSet = new DataSet();

                    cameraControllerSettingDataSet.ReadXml(readStream);

                    var row = cameraControllerSettingDataSet.Tables[0].Rows[0];
                    var resultString = DataManager.GetInstance.SetData.UpdateCameraControlSetting(
                            row["CameraControlSettingID"].ToString().Trim(),
                            row["ModuleFileName"].ToString().Trim(),
                            bool.Parse(row["UsePanTilt"].ToString().Trim()),
                            bool.Parse(row["UseSpeed"].ToString().Trim()),
                            bool.Parse(row["UseZoom"].ToString().Trim()),
                            bool.Parse(row["UseFocus"].ToString().Trim()),
                            bool.Parse(row["UseIris"].ToString().Trim()),
                            bool.Parse(row["UsePreset"].ToString().Trim()),
                            Int32.Parse(row["PresetCount"].ToString().Trim()),
                            bool.Parse(row["UseCameraPower"].ToString().Trim()),
                            bool.Parse(row["UseLightPower"].ToString().Trim()),
                            bool.Parse(row["UseAux1Power"].ToString().Trim()),
                            bool.Parse(row["UseAux2Power"].ToString().Trim()),
                            Int32.Parse(row["AudioMode"].ToString().Trim()),
                            row["Description"].ToString().Trim());

                    return CommonUtil.ResponseData(
                            string.IsNullOrWhiteSpace(resultString) ? ResponseType.Success : ResponseType.Failed,
                            resultString);
                }
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return CommonUtil.ResponseData(ResponseType.Failed, ex.Message);
            }
        }

        /// <summary>
        /// 카메라 컨트롤 정보를 삭제한다.
        /// </summary>
        /// <param name="stream">삭제 할 카메라 컨트롤 정보.</param>
        public Stream DeleteCameraControlSettingForConfiguration(Stream stream)
        {
            try
            {
                using (var readStream = new StreamReader(stream))
                {
                    var cameraControlSettingDataTable = new DataTable();

                    cameraControlSettingDataTable.ReadXml(readStream);

                    foreach (var cameraControlSettingId in
                            from DataRow row in cameraControlSettingDataTable.Rows
                            select row["CameraControlSettingID"].ToString().Trim())
                    {
                        var deleteResultString = DataManager.GetInstance.SetData.DeleteCameraControlSetting(cameraControlSettingId);
                        if (!string.IsNullOrWhiteSpace(deleteResultString))
                        {
                            return CommonUtil.ResponseData(ResponseType.Failed, deleteResultString);
                        }

                        var deleteCameraResultString =
                            DataManager.GetInstance.SetData.UpdateCameraForDeleteCameraControlSetting(
                                cameraControlSettingId);
                        if (!string.IsNullOrWhiteSpace(deleteCameraResultString))
                        {
                            return CommonUtil.ResponseData(ResponseType.Failed, deleteResultString);
                        }
                    }
                }

                return CommonUtil.ResponseData(ResponseType.Success);
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return CommonUtil.ResponseData(ResponseType.Failed, ex.Message);
            }
        }

        #endregion // CameraController

        #region Camera

        /// <summary>
        /// 카메라 정보를 추가한다.
        /// </summary>
        /// <param name="stream">
        /// 추가 할 카메라 정보.
        /// </param>
        public Stream AddCameraInfo(Stream stream)
        {
            try
            {
                using (var readStream = new StreamReader(stream))
                {
                    var cameraDataSet = new DataSet();

                    cameraDataSet.ReadXml(readStream);

                    var resultString = string.Empty;

                    for (var i = 0; i < cameraDataSet.Tables.Count; i++)
                    {
                        foreach (DataRow row in cameraDataSet.Tables[i].Rows)
                        {
                            if (!string.IsNullOrWhiteSpace(resultString))
                            {
                                break;
                            }

                            switch (cameraDataSet.Tables[i].TableName)
                            {
                                case "DeleteProjectMapping":
                                    {
                                        resultString = DataManager.GetInstance.SetData.DeleteProjectMapping(
                                            row["ProjectID"].ToString().Trim(), row["CameraID"].ToString().Trim());

                                        break;
                                    }

                                case "DeleteCameraVideoMapping":
                                    {
                                        resultString = DataManager.GetInstance.SetData.DeleteCameraVideoMapping(
                                            row["CameraID"].ToString().Trim());

                                        break;
                                    }

                                case "Camera":
                                    {
                                        resultString = DataManager.GetInstance.SetData.InsertCamera(
                                            row["CameraID"].ToString().Trim(),
                                            row["CameraDescription"].ToString().Trim(),
                                            row["CameraIP"].ToString().Trim(),
                                            int.Parse(row["CameraPort"].ToString().Trim()),
                                            row["ConnectMode"].ToString().Trim(),
                                            row["MediaServerID"].ToString().Trim(),
                                            row["MediaServerExtraParams"].ToString().Trim(),
                                            row["RtspID"].ToString().Trim(),
                                            row["RtspPassword"].ToString().Trim(),
                                            row["ControlType"].ToString().Trim(),
                                            row["CameraControlSettingID"].ToString().Trim(),
                                            row["PtzConnectionInfoID"].ToString().Trim(),
                                            row["ImageClippingArea"].ToString().Trim(),
                                            bool.Parse(row["Enable"].ToString().Trim()),
                                            bool.Parse(row["ShowAudioLevel"].ToString().Trim()),
                                            row["AnalogCameraMemo"].ToString().Trim(),
                                            IsCheckedForNullableInt.IsCheckedData(row["MediaServerPosition"].ToString().Trim()),
                                            IsCheckedForNullableInt.IsCheckedData(row["Vendor"].ToString().Trim()),
                                            IsCheckedForNullableInt.IsCheckedData(row["RecordCameraNumber"].ToString().Trim()),
                                            row["RdsHost"].ToString().Trim(),
                                            IsCheckedForNullableInt.IsCheckedData(row["RdsPort"].ToString().Trim()),
                                            CommonUtil.ConvertToDbTypeString(row["MultiCameraControllerId"].ToString().Trim()).ToString(),
                                            row["ResizingCodec"].ToString().Trim(),
                                            row["Channel"].ToString().Trim());

                                        break;
                                    }

                                case "ProjectMapping":
                                    {
                                        resultString = DataManager.GetInstance.SetData.InsertProjectMapping(
                                            row["ProjectID"].ToString().Trim(),
                                            row["CameraID"].ToString().Trim(),
                                            row["ProjectCameraID"].ToString().Trim(),
                                            row["ProjectCameraName"].ToString().Trim());

                                        break;
                                    }

                                case "CameraVideoMapping":
                                    {
                                        resultString = DataManager.GetInstance.SetData.InsertCameraVideoMapping(
                                            row["CameraID"].ToString().Trim(), row["VideoSpecID"].ToString().Trim());

                                        break;
                                    }
                            }
                        }

                        if (!string.IsNullOrWhiteSpace(resultString))
                        {
                            return CommonUtil.ResponseData(ResponseType.Failed, resultString);
                        }
                    }
                }

                return CommonUtil.ResponseData(ResponseType.Success);
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return CommonUtil.ResponseData(ResponseType.Failed, ex.Message); ;
            }
        }

        public Stream ImportCameraInfo(Stream stream)
        {
            if (WebOperationContext.Current == null)
            {
                throw new Exception("Can not get current WebOpreationContext.");
            }

            // Content는 무조건 xml임.
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/xml";
            try
            {
                using (var readStream = new StreamReader(stream))
                {
                    var dataSet = new DataSet();
                    dataSet.ReadXml(readStream);

                    var count = 0;

                    if (dataSet.Tables.Count == 3)
                    {
                        var resultString = string.Empty;

                        foreach (DataRow row in dataSet.Tables["CameraInfo"].Rows)
                        {
                            resultString = DataManager.GetInstance.SetData.ImportCamera(
                                        row["CameraID"].ToString().Trim(),
                                        row["CameraDescription"].ToString().Trim(),
                                        row["CameraIP"].ToString().Trim(),
                                        int.Parse(row["CameraPort"].ToString().Trim()),
                                        row["ConnectMode"].ToString().Trim(),
                                        row["MediaServerID"].ToString().Trim(),
                                        row["MediaServerExtraParams"].ToString().Trim(),
                                        row["RtspID"].ToString().Trim(),
                                        row["RtspPassword"].ToString().Trim(),
                                        row["ControlType"].ToString().Trim(),
                                        row["CameraControlSettingID"].ToString().Trim(),
                                        row["PtzConnectionInfoID"].ToString().Trim(),
                                        row["ImageClippingArea"].ToString().Trim(),
                                        bool.Parse(row["Enable"].ToString().Trim()),
                                        string.IsNullOrWhiteSpace(row["ShowAudioLevel"].ToString()) ? false : bool.Parse(row["ShowAudioLevel"].ToString().Trim()),                                        
                                        row["AnalogCameraMemo"].ToString().Trim(),
                                        IsCheckedForNullableInt.IsCheckedData(row["MediaServerPosition"].ToString().Trim()),
                                        IsCheckedForNullableInt.IsCheckedData(row["Vendor"].ToString().Trim()),
                                        IsCheckedForNullableInt.IsCheckedData(row["RecordCameraNumber"].ToString().Trim()),
                                        row["RdsHost"].ToString().Trim(),
                                        IsCheckedForNullableInt.IsCheckedData(row["RdsPort"].ToString().Trim()),
                                        CommonUtil.ConvertToDbTypeString(row["MultiCameraControllerId"].ToString().Trim()).ToString(),
                                        row["Resizingcodec"].ToString().Trim(),
                                        row["Channel"].ToString().Trim(),
                                        count);

                            if (!string.IsNullOrWhiteSpace(resultString))
                            {
                                return CommonUtil.ResponseData(ResponseType.Failed, "[CameraInfo Import] Camera ID : " + row["CameraID"].ToString().Trim() + " " + resultString);
                            }

                            count++;
                        }

                        foreach (DataRow row in dataSet.Tables["ProjectMapping"].Rows)
                        {
                            resultString =
                                DataManager.GetInstance.SetData.InsertProjectMapping(
                                    row["ProjectID"].ToString().Trim(),
                                    row["CameraID"].ToString().Trim(),
                                    row["ProjectCameraID"].ToString().Trim(),
                                    row["ProjectCameraName"].ToString().Trim());

                            if (!string.IsNullOrWhiteSpace(resultString))
                            {
                                return CommonUtil.ResponseData(ResponseType.Failed, "[ProjectMapping Import] Project ID : " + row["ProjectID"].ToString().Trim() + " " + resultString);
                            }
                        }

                        foreach (DataRow row in dataSet.Tables["CameraVideoMapping"].Rows)
                        {
                            resultString = DataManager.GetInstance.SetData.InsertCameraVideoMapping(
                                    row["CameraID"].ToString().Trim(),
                                    row["VideoSpecID"].ToString().Trim());

                            if (!string.IsNullOrWhiteSpace(resultString))
                            {
                                return CommonUtil.ResponseData(ResponseType.Failed, "[CameraVideoMapping Import] Camera ID : " + row["CameraID"].ToString().Trim() + " " + resultString);
                            }
                        }
                    }
                }

                return CommonUtil.ResponseData(ResponseType.Success);
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return CommonUtil.ResponseData(ResponseType.Failed, ex.Message);
            }
        }

        /// <summary>
        /// The add camera table.
        /// </summary>
        /// <param name="stream">
        /// The stream.
        /// </param>
        /// <returns>
        /// The <see cref="Stream"/>.
        /// </returns>
        public Stream AddCameraTable(Stream stream)
        {
            try
            {
                using (var readStream = new StreamReader(stream))
                {
                    var cameraDataTable = new DataTable();

                    cameraDataTable.ReadXml(readStream);

                    var resultString = string.Empty;

                    foreach (DataRow row in cameraDataTable.Rows)
                    {
                        resultString = DataManager.GetInstance.SetData.InsertCamera(
                                    row["CameraID"].ToString().Trim(),
                                    row["CameraDescription"].ToString().Trim(),
                                    row["CameraIP"].ToString().Trim(),
                                    int.Parse(row["CameraPort"].ToString().Trim()),
                                    row["ConnectMode"].ToString().Trim(),
                                    row["MediaServerID"].ToString().Trim(),
                                    row["MediaServerExtraParams"].ToString().Trim(),
                                    row["RtspID"].ToString().Trim(),
                                    row["RtspPassword"].ToString().Trim(),
                                    row["ControlType"].ToString().Trim(),
                                    row["CameraControlSettingID"].ToString().Trim(),
                                    row["PtzConnectionInfoID"].ToString().Trim(),
                                    row["ImageClippingArea"].ToString().Trim(),
                                    bool.Parse(row["Enable"].ToString().Trim()),
                                    bool.Parse(row["ShowAudioLevel"].ToString().Trim()),
                                    row["AnalogCameraMemo"].ToString().Trim(),
                                    IsCheckedForNullableInt.IsCheckedData(row["MediaServerPosition"].ToString().Trim()),
                                    IsCheckedForNullableInt.IsCheckedData(row["Vendor"].ToString().Trim()),
                                    IsCheckedForNullableInt.IsCheckedData(row["RecordCameraNumber"].ToString().Trim()),
                                    row["RdsHost"].ToString().Trim(),
                                    IsCheckedForNullableInt.IsCheckedData(row["RdsPort"].ToString().Trim()),
                                    CommonUtil.ConvertToDbTypeString(row["MultiCameraControllerId"].ToString().Trim()).ToString(),
                                    row["Resizingcodec"].ToString().Trim(),
                                    row["Channel"].ToString().Trim());

                        if (!string.IsNullOrWhiteSpace(resultString))
                        {
                            return CommonUtil.ResponseData(ResponseType.Failed, resultString);
                        }
                    }

                    var cameraDataSet = new DataSet();
                    cameraDataSet.Tables.Add(cameraDataTable);
                }

                return CommonUtil.ResponseData(ResponseType.Success);
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return CommonUtil.ResponseData(ResponseType.Failed, ex.Message);
            }
        }

        /// <summary>
        /// The add camera video mapping table.
        /// </summary>
        /// <param name="stream">
        /// The stream.
        /// </param>
        /// <returns>
        /// The <see cref="Stream"/>.
        /// </returns>
        public Stream AddCameraVideoMappingTable(Stream stream)
        {
            try
            {
                using (var readStream = new StreamReader(stream))
                {
                    var cameraDataTable = new DataTable();

                    cameraDataTable.ReadXml(readStream);

                    foreach (DataRow row in cameraDataTable.Rows)
                    {
                        var resultString = DataManager.GetInstance.SetData.InsertCameraVideoMapping(
                                    row["CameraID"].ToString().Trim(), row["VideoSpecID"].ToString().Trim());

                        if (!string.IsNullOrWhiteSpace(resultString))
                        {
                            return CommonUtil.ResponseData(ResponseType.Failed, resultString);
                        }
                    }

                    var cameraDataSet = new DataSet();
                    cameraDataSet.Tables.Add(cameraDataTable);
                }

                return CommonUtil.ResponseData(ResponseType.Success);
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return CommonUtil.ResponseData(ResponseType.Failed, ex.Message);
            }
        }

        /// <summary>
        /// The add project mapping table.
        /// </summary>
        /// <param name="stream">
        /// The stream.
        /// </param>
        /// <returns>
        /// The <see cref="Stream"/>.
        /// </returns>
        public Stream AddProjectMappingTable(Stream stream)
        {
            try
            {
                using (var readStream = new StreamReader(stream))
                {
                    var cameraDataTable = new DataTable();

                    cameraDataTable.ReadXml(readStream);

                    foreach (DataRow row in cameraDataTable.Rows)
                    {
                        var resultString = DataManager.GetInstance.SetData.InsertProjectMapping(
                            row["ProjectID"].ToString().Trim(),
                            row["CameraID"].ToString().Trim(),
                            row["ProjectCameraID"].ToString().Trim(),
                            row["ProjectCameraName"].ToString().Trim());

                        if (!string.IsNullOrWhiteSpace(resultString))
                        {
                            return CommonUtil.ResponseData(ResponseType.Failed, resultString);
                        }
                    }

                    var cameraDataSet = new DataSet();
                    cameraDataSet.Tables.Add(cameraDataTable);
                }

                return CommonUtil.ResponseData(ResponseType.Success);
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return CommonUtil.ResponseData(ResponseType.Failed, ex.Message);
            }
        }

        /// <summary>
        /// 카메라 정보를 수정한다.
        /// </summary>
        /// <param name="stream">수정 할 카메라 정보.</param>
        public Stream UpdateCameraInfo(Stream stream)
        {
            try
            {
                using (var readStream = new StreamReader(stream))
                {
                    var cameraDataSet = new DataSet();

                    cameraDataSet.ReadXml(readStream);

                    var resultString = string.Empty;

                    for (var i = 0; i < cameraDataSet.Tables.Count; i++)
                    {
                        foreach (DataRow row in cameraDataSet.Tables[i].Rows)
                        {
                            if (!string.IsNullOrWhiteSpace(resultString))
                            {
                                break;
                            }

                            switch (cameraDataSet.Tables[i].TableName)
                            {
                                case "DeleteProjectMapping":
                                    {
                                        resultString = DataManager.GetInstance.SetData.DeleteProjectMapping(
                                            row["ProjectID"].ToString().Trim(), row["CameraID"].ToString().Trim());

                                        break;
                                    }

                                case "DeleteCameraVideoMapping":
                                    {
                                        resultString = DataManager.GetInstance.SetData.DeleteCameraVideoMapping(
                                            row["CameraID"].ToString().Trim());

                                        break;
                                    }

                                case "Camera":
                                    {
                                        resultString = DataManager.GetInstance.SetData.UpdateCamera(
                                            row["CameraID"].ToString().Trim(),
                                            row["CameraDescription"].ToString().Trim(),
                                            row["CameraIP"].ToString().Trim(),
                                            int.Parse(row["CameraPort"].ToString().Trim()),
                                            row["ConnectMode"].ToString().Trim(),
                                            row["MediaServerID"].ToString().Trim(),
                                            row["MediaServerExtraParams"].ToString().Trim(),
                                            row["RtspID"].ToString().Trim(),
                                            row["RtspPassword"].ToString().Trim(),
                                            row["ControlType"].ToString().Trim(),
                                            row["CameraControlSettingID"].ToString().Trim(),
                                            row["PtzConnectionInfoID"].ToString().Trim(),
                                            row["ImageClippingArea"].ToString().Trim(),
                                            bool.Parse(row["Enable"].ToString().Trim()),
                                            bool.Parse(row["ShowAudioLevel"].ToString().Trim()),
                                            row["AnalogCameraMemo"].ToString().Trim(),
                                            IsCheckedForNullableInt.IsCheckedData(
                                            row["MediaServerPosition"].ToString().Trim()),
                                            IsCheckedForNullableInt.IsCheckedData(row["Vendor"].ToString().Trim()),
                                            IsCheckedForNullableInt.IsCheckedData(
                                            row["RecordCameraNumber"].ToString().Trim()),
                                            row["RdsHost"].ToString().Trim(),
                                            IsCheckedForNullableInt.IsCheckedData(row["RdsPort"].ToString().Trim()),
                                            CommonUtil.ConvertToDbTypeString(row["MultiCameraControllerId"].ToString().Trim()).ToString(),
                                            row["ResizingCodec"].ToString().Trim(),
                                            row["Channel"].ToString().Trim());

                                        break;
                                    }
                                case "ProjectMapping":
                                    {
                                        resultString = DataManager.GetInstance.SetData.InsertProjectMapping(
                                            row["ProjectID"].ToString().Trim(),
                                            row["CameraID"].ToString().Trim(),
                                            row["ProjectCameraID"].ToString().Trim(),
                                            row["ProjectCameraName"].ToString().Trim());

                                        break;
                                    }
                                case "CameraVideoMapping":
                                    {
                                        resultString = DataManager.GetInstance.SetData.InsertCameraVideoMapping(
                                            row["CameraID"].ToString().Trim(), row["VideoSpecID"].ToString().Trim());

                                        break;
                                    }
                            }
                        }

                        if (!string.IsNullOrWhiteSpace(resultString))
                        {
                            return CommonUtil.ResponseData(ResponseType.Failed, resultString);
                        }
                    }
                }

                return CommonUtil.ResponseData(ResponseType.Success);
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return CommonUtil.ResponseData(ResponseType.Failed, ex.Message);
            }
        }

        public Stream DeleteCameraInfo(Stream stream)
        {
            try
            {
                using (var readStream = new StreamReader(stream))
                {
                    var cameraDataSet = new DataSet();

                    cameraDataSet.ReadXml(readStream);

                    var resultString = string.Empty;

                    for (var i = 0; i < cameraDataSet.Tables.Count; i++)
                    {
                        foreach (DataRow row in cameraDataSet.Tables[i].Rows)
                        {
                            if (!string.IsNullOrWhiteSpace(resultString))
                            {
                                break;
                            }

                            switch (cameraDataSet.Tables[i].TableName)
                            {
                                case "DeleteCamera":
                                    {
                                        resultString = DataManager.GetInstance.SetData.DeleteCamera(row["CameraID"].ToString().Trim());

                                        break;
                                    }
                                case "DeleteProjectMapping":
                                    {
                                        resultString = DataManager.GetInstance.SetData.DeleteProjectMapping(
                                            row["ProjectID"].ToString().Trim(), row["CameraID"].ToString().Trim());

                                        break;
                                    }
                                case "DeleteCameraVideoMapping":
                                    {
                                        resultString = DataManager.GetInstance.SetData.DeleteCameraVideoMapping(
                                            row["CameraID"].ToString().Trim());

                                        break;
                                    }
                            }
                        }

                        if (!string.IsNullOrWhiteSpace(resultString))
                        {
                            return CommonUtil.ResponseData(ResponseType.Failed, resultString);
                        }
                    }
                }

                return CommonUtil.ResponseData(ResponseType.Success);
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return CommonUtil.ResponseData(ResponseType.Failed, ex.Message);
            }
        }

        public Stream DeleteCameraTable()
        {
            try
            {
                var resultString = DataManager.GetInstance.SetData.DeleteCameraTable();

                return CommonUtil.ResponseData(
                        string.IsNullOrWhiteSpace(resultString) ? ResponseType.Success : ResponseType.Failed,
                        resultString);
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return CommonUtil.ResponseData(ResponseType.Failed, ex.Message);
            }
        }

        public Stream DeleteCameraVideoMappingTable()
        {
            try
            {
                var resultString = DataManager.GetInstance.SetData.DeleteCameraVideoMappingTable();

                return CommonUtil.ResponseData(
                        string.IsNullOrWhiteSpace(resultString) ? ResponseType.Success : ResponseType.Failed,
                        resultString);
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return CommonUtil.ResponseData(ResponseType.Failed, ex.Message);
            }
        }

        public Stream DeleteProjectMappingTable()
        {
            try
            {
                var resultString = DataManager.GetInstance.SetData.DeleteProjectMappingTable();

                return CommonUtil.ResponseData(
                        string.IsNullOrWhiteSpace(resultString) ? ResponseType.Success : ResponseType.Failed,
                        resultString);
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return CommonUtil.ResponseData(ResponseType.Failed, ex.Message);
            }
        }

        #endregion // Camera

        #region CameraGroup

        /// <summary>
        /// 카메라 그룹 정보를 추가한다.
        /// </summary>
        /// <param name="stream">
        /// 추가 할 카메라 그룹 정보.
        /// </param>
        public void AddCameraGroupForConfiguration(Stream stream)
        {
            try
            {
                using (var readStream = new StreamReader(stream))
                {
                    var cameraGroupDataTable = new DataTable();

                    cameraGroupDataTable.ReadXml(readStream);

                    foreach (DataRow row in cameraGroupDataTable.Rows)
                    {
                        DataManager.GetInstance.SetData.InsertCameraGroup(
                            int.Parse(row["ParentID"].ToString().Trim()),
                            row["CameraGroupName"].ToString().Trim(),
                            row["ProjectID"].ToString().Trim(),
                            int.Parse(row["ItemOrder"].ToString().Trim()),
                            row["UserID"].ToString().Trim(),
                            row["Type"].ToString().Trim());
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return;
            }
        }

        /// <summary>
        /// 카메라 그룹 정보를 수정한다.
        /// </summary>
        /// <param name="stream">
        /// 수정 할 카메라 그룹 정보.
        /// </param>
        public void UpdateCameraGroupForConfiguration(Stream stream)
        {
            try
            {
                using (var readStream = new StreamReader(stream))
                {
                    var cameraGroupDataTable = new DataTable();

                    cameraGroupDataTable.ReadXml(readStream);

                    foreach (DataRow row in cameraGroupDataTable.Rows)
                    {
                        DataManager.GetInstance.SetData.UpdateCameraGroup(
                            int.Parse(row["CameraGroupID"].ToString().Trim()),
                            int.Parse(row["ParentID"].ToString().Trim()),
                            row["CameraGroupName"].ToString().Trim(),
                            row["ProjectID"].ToString().Trim(),
                            int.Parse(row["ItemOrder"].ToString().Trim()),
                            row["UserID"].ToString().Trim(),
                            row["Type"].ToString().Trim());
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return;
            }
        }

        /// <summary>
        /// 카메라 그룹 정보를 삭제한다.
        /// </summary>
        /// <param name="stream">
        /// 삭제 할 카메라 그룹 정보.
        /// </param>
        public void DeleteCameraGroupForConfiguration(Stream stream)
        {
            try
            {
                using (var readStream = new StreamReader(stream))
                {
                    var cameraGroupDataTable = new DataTable();

                    cameraGroupDataTable.ReadXml(readStream);

                    foreach (DataRow row in cameraGroupDataTable.Rows)
                    {
                        DataManager.GetInstance.SetData.DeleteCameraGroup(
                            int.Parse(row["CameraGroupID"].ToString().Trim()),
                            row["ProjectID"].ToString().Trim(),
                            row["UserID"].ToString().Trim());
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return;
            }
        }

        /// <summary>
        /// The delete camera group table.
        /// </summary>
        public void DeleteCameraGroupTable()
        {
            try
            {
                DataManager.GetInstance.SetData.DeleteCameraGroupTable();
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return;
            }
        }

        /// <summary>
        /// The add camera group table.
        /// </summary>
        /// <param name="stream">
        /// The stream.
        /// </param>
        public void AddCameraGroupTable(Stream stream)
        {
            try
            {
                using (var readStream = new StreamReader(stream))
                {
                    var cameraGroupDataTable = new DataTable();

                    cameraGroupDataTable.ReadXml(readStream);

                    foreach (DataRow row in cameraGroupDataTable.Rows)
                    {
                        DataManager.GetInstance.SetData.InsertCameraGroupTable(
                            int.Parse(row["CameraGroupID"].ToString().Trim()),
                            int.Parse(row["ParentID"].ToString().Trim()),
                            row["CameraGroupName"].ToString().Trim(),
                            row["ProjectID"].ToString().Trim(),
                            int.Parse(row["ItemOrder"].ToString().Trim()),
                            row["UserID"].ToString().Trim(),
                            row["Type"].ToString().Trim());
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.ToString());
                return;
            }
        }

        #endregion // CameraGroup

        #region CameraGroupMapping

        /// <summary>
        /// 카메라 그룹 맵핑 정보를 추가한다.
        /// </summary>
        /// <param name="stream">추가 할 카메라 그룹 맵핑 정보</param>
        public void AddCameraGroupMappingForConfiguration(Stream stream)
        {
            try
            {
                using (var readStream = new StreamReader(stream))
                {
                    var cameraGroupMappingDataTable = new DataTable();

                    cameraGroupMappingDataTable.ReadXml(readStream);

                    foreach (DataRow row in cameraGroupMappingDataTable.Rows)
                    {
                        DataManager.GetInstance.SetData.InsertCameraGroupMapping(
                            Int32.Parse(row["CameraGroupID"].ToString().Trim()),
                            row["CameraID"].ToString().Trim(),
                            Int32.Parse(row["ItemOrder"].ToString().Trim()));
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return;
            }
        }

        /// <summary>
        /// 카메라 그룹 맵핑 정보를 수정한다.
        /// </summary>
        /// <param name="stream">수정 할 카메라 그룹 맵핑 정보</param>
        public void UpdateCameraGroupMappingForConfiguration(Stream stream)
        {
            try
            {
                using (var readStream = new StreamReader(stream))
                {
                    var cameraGroupMappingDataTable = new DataTable();

                    cameraGroupMappingDataTable.ReadXml(readStream);

                    foreach (DataRow row in cameraGroupMappingDataTable.Rows)
                    {
                        DataManager.GetInstance.SetData.UpdateCameraGroupMapping(
                            Int32.Parse(row["CurrentCameraGroupID"].ToString().Trim()),
                            Int32.Parse(row["CameraGroupID"].ToString().Trim()),
                            row["CameraID"].ToString().Trim(),
                            Int32.Parse(row["ItemOrder"].ToString().Trim()));
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return;
            }
        }

        /// <summary>
        /// 카메라 그룹 맵핑 정보를 삭제한다.
        /// </summary>
        /// <param name="stream">삭제 할 카메라 그룹 맵핑 정보</param>
        public void DeleteCameraGroupMappingForConfiguration(Stream stream)
        {
            try
            {
                using (var readStream = new StreamReader(stream))
                {
                    var cameraGroupMappingDataTable = new DataTable();

                    cameraGroupMappingDataTable.ReadXml(readStream);

                    foreach (DataRow row in cameraGroupMappingDataTable.Rows)
                    {
                        DataManager.GetInstance.SetData.DeleteCameraGroupMapping(
                            Int32.Parse(row["CameraGroupID"].ToString().Trim()), row["CameraID"].ToString().Trim());
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return;
            }
        }

        public void DeleteCameraGroupMappingTable()
        {
            try
            {
                DataManager.GetInstance.SetData.DeleteCameraGroupMappingTable();
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return;
            }
        }

        public void AddCameraGroupMappingTable(Stream stream)
        {
            try
            {
                using (var readStream = new StreamReader(stream))
                {
                    var cameraGroupDataTable = new DataTable();

                    cameraGroupDataTable.ReadXml(readStream);

                    foreach (DataRow row in cameraGroupDataTable.Rows)
                    {
                        DataManager.GetInstance.SetData.InsertCameraGroupMapping(
                            Int32.Parse(row["CameraGroupID"].ToString().Trim()),
                            row["CameraID"].ToString().Trim(),
                            Int32.Parse(row["ItemOrder"].ToString().Trim()));
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return;
            }
        }

        #endregion // CameraGroupMapping

        #region Favorite

        /// <summary>
        /// 즐겨찾기 정보를 추가한다.
        /// </summary>
        /// <param name="stream">추가 할 즐겨찾기 정보.</param>
        public void AddFavoriteInfoForConfiguration(Stream stream)
        {
            try
            {
                using (var readStream = new StreamReader(stream))
                {
                    var favoriteDataTable = new DataTable();

                    favoriteDataTable.ReadXml(readStream);

                    foreach (DataRow row in favoriteDataTable.Rows)
                    {
                        DataManager.GetInstance.SetData.InsertFavorite(
                            row["FavoriteName"].ToString().Trim(),
                            row["Data"].ToString().Trim(),
                            row["Type"].ToString().ToLower());
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return;
            }
        }

        /// <summary>
        /// 즐겨찾기 정보를 수정한다.
        /// </summary>
        /// <param name="stream">수정 할 즐겨찾기 정보.</param>
        public void UpdateFavoriteInfoForConfiguration(Stream stream)
        {
            try
            {
                using (var readStream = new StreamReader(stream))
                {
                    var favoriteDataTable = new DataTable();

                    favoriteDataTable.ReadXml(readStream);

                    foreach (DataRow row in favoriteDataTable.Rows)
                    {
                        DataManager.GetInstance.SetData.UpdateFavorite(
                            Int32.Parse(row["FavoriteID"].ToString().Trim()),
                            row["FavoriteName"].ToString().Trim(),
                            row["Data"].ToString().Trim(),
                            row["Type"].ToString().ToLower());
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return;
            }
        }

        /// <summary>
        /// 즐겨찾기 정보를 삭제한다.
        /// </summary>
        /// <param name="stream">삭제 할 즐겨찾기 정보.</param>
        public void DeleteFavoriteInfoForConfiguration(Stream stream)
        {
            try
            {
                using (var readStream = new StreamReader(stream))
                {
                    var favoriteDataTable = new DataTable();

                    favoriteDataTable.ReadXml(readStream);

                    foreach (DataRow row in favoriteDataTable.Rows)
                    {
                        DataManager.GetInstance.SetData.DeleteFavorite(
                            Int32.Parse(row["FavoriteID"].ToString().Trim()), row["Type"].ToString().ToLower());
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return;
            }
        }

        public void DeleteFavoriteTable()
        {
            try
            {
                DataManager.GetInstance.SetData.DeleteFavoriteTable();
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return;
            }
        }

        public void AddFavoriteTable(Stream stream)
        {
            try
            {
                using (var readStream = new StreamReader(stream))
                {
                    var favoriteDataTable = new DataTable();

                    favoriteDataTable.ReadXml(readStream);

                    foreach (DataRow row in favoriteDataTable.Rows)
                    {
                        DataManager.GetInstance.SetData.InsertFavoriteTable(
                            Convert.ToInt32(row["FavoriteID"].ToString()),
                            row["FavoriteName"].ToString(),
                            row["Data"].ToString(),
                            row["Type"].ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return;
            }
        }

        #endregion // Favorite

        #region FavoriteGroup

        public void AddFavoriteGroupForConfiguration(Stream stream, string projectId, string userId)
        {
            try
            {
                using (var readStream = new StreamReader(stream))
                {
                    var favoriteGroupDataTable = new DataTable();

                    favoriteGroupDataTable.ReadXml(readStream);

                    foreach (DataRow row in favoriteGroupDataTable.Rows)
                    {
                        DataManager.GetInstance.SetData.InsertFavoriteGroup(
                            Int32.Parse(row["ParentID"].ToString().Trim()),
                            row["FavoriteGroupName"].ToString().Trim(),
                            row["ProjectID"].ToString().Trim(),
                            Int32.Parse(row["ItemOrder"].ToString().Trim()),
                            row["UserID"].ToString().Trim(),
                            row["Type"].ToString().ToLower());
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return;
            }
        }

        public void UpdateFavoriteGroupForConfiguration(Stream stream, string projectId, string userId)
        {
            try
            {
                using (var readStream = new StreamReader(stream))
                {
                    var favoriteGroupDataTable = new DataTable();

                    favoriteGroupDataTable.ReadXml(readStream);

                    foreach (DataRow row in favoriteGroupDataTable.Rows)
                    {
                        DataManager.GetInstance.SetData.UpdateFavoriteGroup(
                            Int32.Parse(row["FavoriteGroupID"].ToString().Trim()),
                            Int32.Parse(row["ParentID"].ToString().Trim()),
                            row["FavoriteGroupName"].ToString().Trim(),
                            row["ProjectID"].ToString().Trim(),
                            Int32.Parse(row["ItemOrder"].ToString().Trim()),
                            row["UserID"].ToString().Trim(),
                            row["Type"].ToString().ToLower());
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return;
            }
        }

        public void DeleteFavoriteGroupForConfiguration(Stream stream, string projectId, string userId)
        {
            try
            {
                using (var readStream = new StreamReader(stream))
                {
                    var favoriteGroupDataTable = new DataTable();

                    favoriteGroupDataTable.ReadXml(readStream);

                    foreach (DataRow row in favoriteGroupDataTable.Rows)
                    {
                        DataManager.GetInstance.SetData.DeleteFavoriteGroup(
                            Int32.Parse(row["FavoriteGroupID"].ToString().Trim()),
                            projectId,
                            userId.ToLower(),
                            row["Type"].ToString().ToLower());
                    }

                    DataGarbageCollecter.Invoke(DeleteType.CommandFavorite);
                }
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return;
            }
        }

        public void DeleteFavoriteGroupTable()
        {
            try
            {
                DataManager.GetInstance.SetData.DeleteFavoriteGroupTable();
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return;
            }
        }

        public void AddFavoriteGroupTable(Stream stream)
        {
            try
            {
                using (var readStream = new StreamReader(stream))
                {
                    var favoriteGroupDataTable = new DataTable();

                    favoriteGroupDataTable.ReadXml(readStream);

                    Debug.Assert(false, "type  정보를 추가하고 사용해야 합니다.");
                    foreach (DataRow row in favoriteGroupDataTable.Rows)
                    {

                        DataManager.GetInstance.SetData.InsertFavoriteGroupTable(
                            Int32.Parse(row["FavoriteGroupID"].ToString().Trim()),
                            Int32.Parse(row["ParentID"].ToString().Trim()),
                            row["FavoriteGroupName"].ToString().Trim(),
                            row["ProjectID"].ToString().Trim(),
                            Int32.Parse(row["ItemOrder"].ToString().Trim()),
                            row["UserID"].ToString().Trim(),
                            string.Empty);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return;
            }
        }

        #endregion // FavoriteGroup

        #region FavoriteGroupMapping

        public void AddFavoriteGroupMappingForConfiguration(Stream stream)
        {
            try
            {
                using (var readStream = new StreamReader(stream))
                {
                    var favoriteGroupMappingDataTable = new DataTable();

                    favoriteGroupMappingDataTable.ReadXml(readStream);

                    foreach (DataRow row in favoriteGroupMappingDataTable.Rows)
                    {
                        DataManager.GetInstance.SetData.InsertFavoriteGroupMapping(
                            Int32.Parse(row["FavoriteGroupID"].ToString().Trim()),
                            Int32.Parse(row["FavoriteID"].ToString().Trim()),
                            Int32.Parse(row["ItemOrder"].ToString().Trim()));
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return;
            }
        }

        public void UpdateFavoriteGroupMappingForConfiguration(Stream stream)
        {
            try
            {
                using (var readStream = new StreamReader(stream))
                {
                    var favoriteGroupMappingDataTable = new DataTable();

                    favoriteGroupMappingDataTable.ReadXml(readStream);

                    foreach (DataRow row in favoriteGroupMappingDataTable.Rows)
                    {
                        DataManager.GetInstance.SetData.UpdateFavoriteGroupMappingForConfiguration(
                            Int32.Parse(row["CurrentFavoriteGroupID"].ToString().Trim()),
                            Int32.Parse(row["FavoriteGroupID"].ToString().Trim()),
                            Int32.Parse(row["FavoriteID"].ToString().Trim()),
                            Int32.Parse(row["ItemOrder"].ToString().Trim()));
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return;
            }
        }

        public void DeleteFavoriteGroupMappingForConfiguration(Stream stream)
        {
            try
            {
                using (var readStream = new StreamReader(stream))
                {
                    var favoriteGroupMappingDataTable = new DataTable();

                    favoriteGroupMappingDataTable.ReadXml(readStream);

                    foreach (DataRow row in favoriteGroupMappingDataTable.Rows)
                    {
                        DataManager.GetInstance.SetData.DeleteFavoriteGroupMappingByFavoriteGroupIdAndFavoriteId(
                            Int32.Parse(row["FavoriteGroupID"].ToString().Trim()),
                            Int32.Parse(row["FavoriteID"].ToString().Trim()));
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return;
            }
        }

        public void DeleteFavoriteGroupMappingTable()
        {
            try
            {
                DataManager.GetInstance.SetData.DeleteFavoriteGroupMappingTable();
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return;
            }
        }

        /// <summary>
        /// 즐겨찾기를 관리하는데 필요한 모든 데이타를 저장한다.
        /// </summary>
        /// <param name="stream">
        /// 수신 받은 Stream. 
        /// </param>
        /// <param name="projectId">
        /// 프로젝트 아이디.
        /// </param>
        /// <param name="userId">
        /// 사용자 아이다.
        /// </param>
        /// <param name="type">
        /// 프로그램 타입.
        /// </param>
        public void SaveFavoriteManager(Stream stream, string projectId, string userId, string type)
        {
            // CM으로 받은 정보 Deserialize.
            var favoriteManger = new FavoriteManager(stream);

            var favoriteIds = DataManager.GetInstance.GetDataForClient.GetFavoriteItemIDs(projectId, userId, type);

            // FavoriteGroup 삭제.
            DataManager.GetInstance.SetData.DeleteFavoriteGroup(projectId, userId, type);

            // FavoriteGroupMapping에서 관련된 모든 데이타를 제거한다.
            DataManager.GetInstance.SetData.DeleteFavoriteGroupMapping(favoriteIds);

            // Favorite 삭제.
            foreach (var favoriteID in favoriteIds)
            {
                DataManager.GetInstance.SetData.DeleteFavorite(favoriteID, type);
            }


            var favorites = favoriteManger.GetFavoriteItem(favoriteManger, false);
            var favoriteGroups = favoriteManger.GetFavoriteItem(favoriteManger, true);

            foreach (var favoriteGroup in favoriteGroups)
            {
                DataManager.GetInstance.SetData.InsertFavoriteGroupTable(
                        favoriteGroup.CurrentID,
                        favoriteGroup.ParentID,
                        favoriteGroup.CurrentName,
                        projectId,
                        favoriteGroup.ItemOrder,
                        userId,
                        type);
            }

            foreach (var favorite in favorites)
            {
                DataManager.GetInstance.SetData.InsertFavoriteTable(favorite.CurrentID, favorite.CurrentName, favorite.Data, type);
            }

            // 4. 모든 데이타를 다시 저장한다.
            foreach (var favorite in favoriteManger)
            {
                if (!favorite.IsFolder)
                {
                    DataManager.GetInstance.SetData.InsertFavoriteGroupMapping(
                        favorite.ParentID,
                        favorite.CurrentID,
                        favorite.ItemOrder);
                }
            }




            //// 1. 삭제된 즐겨찾기 아이템을 Favorite 테이블에서 제거한다.
            //var deleteFavoriteIDList = favoriteManger.GetDeleteFavoriteIDList(projectId, userId, type);
            //foreach (var id in deleteFavoriteIDList)
            //{
            //    DataManager.GetInstance.SetData.DeleteFavorite(id, type);
            //}

            //// 2. Favorite Group에서 관련된 모든 데이타를 제거한다.
            //DataManager.GetInstance.SetData.DeleteFavoriteGroup(projectId, userId, type);



            //var ids = GetFavoriteGroupID();
            //favoriteManger.ChangeGroupID(ids);

            //// 4. 모든 데이타를 다시 저장한다.
            //foreach (var favorite in favoriteManger)
            //{
            //    if (favorite.IsFolder)
            //    {
            //        DataManager.GetInstance.SetData.InsertFavoriteGroupTable(
            //            favorite.CurrentID,
            //            favorite.ParentID,
            //            favorite.CurrentName,
            //            projectId,
            //            favorite.ItemOrder,
            //            userId,
            //            type);
            //    }
            //    else
            //    {
            //        DataManager.GetInstance.SetData.InsertFavoriteGroupMapping(
            //            favorite.ParentID,
            //            favorite.CurrentID,
            //            favorite.ItemOrder);
            //    }
            //}
        }

        private static List<int> GetFavoriteGroupID()
        {
            var dataSet = DataManager.GetInstance.GetDataForClient.GetFavoriteGroupForClient();
            var table = dataSet.Tables[0];
            return (from DataRow row in table.Rows select Convert.ToInt32(row["FavoriteGroupID"])).ToList();
        }

        #endregion // FavoriteGroupMapping

        #endregion // SetData of Cameras

        #region SetData of Nvr

        #region Storage

        /// <summary>
        /// The add storage info.
        /// </summary>
        /// <param name="stream">
        /// The stream.
        /// </param>
        /// <returns>
        /// The response data.
        /// </returns>
        /// <exception cref="Exception">
        /// The exception.
        /// </exception>
        public Stream AddStorageInfo(Stream stream)
        {
            try
            {
                // 저장 위치에 있는 xaml 문자열을 가져와서 Stream으로 return.
                if (WebOperationContext.Current == null)
                {
                    throw new Exception("Can not get current WebOpreationContext.");
                }

                // Content는 무조건 xml임.
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/xml";

                using (var readStream = new StreamReader(stream))
                {
                    var storageDataSet = new DataSet();

                    storageDataSet.ReadXml(readStream);

                    foreach (DataRow row in storageDataSet.Tables[0].Rows)
                    {
                        var resultString =
                            DataManager.GetInstance.SetData.InsertStorage(
                                int.Parse(row["StorageNumber"].ToString().Trim()),
                                row["StorageName"].ToString().Trim(),
                                row["StoragePath"].ToString().Trim(),
                                int.Parse(row["StorageType"].ToString().Trim()),
                                int.Parse(row["StorageCapacity"].ToString().Trim()),
                                int.Parse(row["StorageRemain"].ToString().Trim()),
                                int.Parse(row["LowerLimit"].ToString().Trim()),
                                int.Parse(row["ErasingMethod"].ToString().Trim()));

                        if (!string.IsNullOrWhiteSpace(resultString))
                        {
                            return CommonUtil.ResponseData(ResponseType.Failed, resultString);
                        }
                    }
                }

                return CommonUtil.ResponseData(ResponseType.Success);
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return CommonUtil.ResponseData(ResponseType.Failed, ex.Message);
            }
        }

        /// <summary>
        /// The import storage info.
        /// </summary>
        /// <param name="stream">
        /// The stream.
        /// </param>
        /// <returns>
        /// The <see cref="Stream"/>.
        /// </returns>
        /// <exception cref="Exception">
        /// </exception>
        public Stream ImportStorageInfo(Stream stream)
        {
            try
            {
                // 저장 위치에 있는 xaml 문자열을 가져와서 Stream으로 return.
                if (WebOperationContext.Current == null)
                {
                    throw new Exception("Can not get current WebOpreationContext.");
                }

                // Content는 무조건 xml임.
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/xml";

                using (var readStream = new StreamReader(stream))
                {
                    var storageDataSet = new DataSet();

                    storageDataSet.ReadXml(readStream);

                    var count = 0;

                    foreach (DataRow row in storageDataSet.Tables[0].Rows)
                    {
                        var resultString =
                            DataManager.GetInstance.SetData.ImportStorageInfo(
                                int.Parse(row["StorageNumber"].ToString().Trim()),
                                row["StorageName"].ToString().Trim(),
                                row["StoragePath"].ToString().Trim(),
                                int.Parse(row["StorageType"].ToString().Trim()),
                                int.Parse(row["StorageCapacity"].ToString().Trim()),
                                int.Parse(row["StorageRemain"].ToString().Trim()),
                                int.Parse(row["LowerLimit"].ToString().Trim()),
                                int.Parse(row["ErasingMethod"].ToString().Trim()),
                                count);

                        if (!string.IsNullOrWhiteSpace(resultString))
                        {
                            return CommonUtil.ResponseData(ResponseType.Failed, resultString);
                        }

                        count++;
                    }
                }

                return CommonUtil.ResponseData(ResponseType.Success);
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return CommonUtil.ResponseData(ResponseType.Failed, ex.Message);
            }
        }

        /// <summary>
        /// The update storage info.
        /// </summary>
        /// <param name="stream">
        /// The stream.
        /// </param>
        /// <returns>
        /// The response data.
        /// </returns>
        /// <exception cref="Exception">
        /// The exception.
        /// </exception>
        public Stream UpdateStorageInfo(Stream stream)
        {
            try
            {
                // 저장 위치에 있는 xaml 문자열을 가져와서 Stream으로 return.
                if (WebOperationContext.Current == null)
                {
                    throw new Exception("Can not get current WebOpreationContext.");
                }

                // Content는 무조건 xml임.
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/xml";

                Stream responseData = null;

                using (var readStream = new StreamReader(stream))
                {
                    var storageDataSet = new DataSet();

                    storageDataSet.ReadXml(readStream);

                    foreach (DataRow row in storageDataSet.Tables[0].Rows)
                    {
                        responseData =
                            DataManager.GetInstance.SetData.UpdateStorage(
                                int.Parse(row["StorageNumber"].ToString().Trim()),
                                row["StorageName"].ToString().Trim(),
                                row["StoragePath"].ToString().Trim(),
                                int.Parse(row["StorageType"].ToString().Trim()),
                                int.Parse(row["StorageCapacity"].ToString().Trim()),
                                int.Parse(row["StorageRemain"].ToString().Trim()),
                                int.Parse(row["LowerLimit"].ToString().Trim()),
                                int.Parse(row["ErasingMethod"].ToString().Trim()));
                    }
                }

                return responseData;
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return CommonUtil.ResponseData(ResponseType.Failed, ex.Message);
            }
        }

        /// <summary>
        /// The delete storage info.
        /// </summary>
        /// <param name="stream">
        /// The stream.
        /// </param>
        /// <returns>
        /// The response data.
        /// </returns>
        /// <exception cref="Exception">
        /// The exception.
        /// </exception>
        public Stream DeleteStorageInfo(Stream stream)
        {
            try
            {
                // 저장 위치에 있는 xaml 문자열을 가져와서 Stream으로 return.
                if (WebOperationContext.Current == null)
                {
                    throw new Exception("Can not get current WebOpreationContext.");
                }

                // Content는 무조건 xml임.
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/xml";

                Stream responseData = null;

                using (var readStream = new StreamReader(stream))
                {
                    var storageDataSet = new DataSet();

                    storageDataSet.ReadXml(readStream);

                    foreach (DataRow row in storageDataSet.Tables[0].Rows)
                    {
                        responseData =
                            DataManager.GetInstance.SetData.DeleteStorage(
                                int.Parse(row["StorageNumber"].ToString().Trim()));
                    }
                }

                return responseData;
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return CommonUtil.ResponseData(ResponseType.Failed, ex.Message);
            }
        }

        /// <summary>
        /// The set storage update process.
        /// Recorder 에서 요청한 StorageNumber 에 대한 Capacity, Remain 정보를 업데이트 한다.
        /// </summary>
        /// <param name="storageCapacity">
        /// The stroage capacity.
        /// </param>
        /// <param name="storageRemain">
        /// The storage remain.
        /// </param>
        /// <param name="storageNumber">
        /// The storage number.
        /// </param>
        /// <returns>
        /// The <see cref="Stream"/>.
        /// </returns>
        public Stream SetStorageUpdateProcess(string storageCapacity, string storageRemain, string storageNumber)
        {
            try
            {
                // 저장 위치에 있는 xaml 문자열을 가져와서 Stream으로 return.
                if (WebOperationContext.Current == null)
                {
                    throw new Exception("Can not get current WebOpreationContext.");
                }

                // Content는 무조건 xml임.
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/xml";

                var result = DataManager.GetInstance.SetData.SetStroageUpdateProcess(int.Parse(storageCapacity), int.Parse(storageRemain), int.Parse(storageRemain));

                var storageDataSet = new DataSet();
                var dataTable = new DataTable();

                var columnStorageCapacity = new DataColumn("StorageCapacity", typeof(string));
                var columnStorageRemain = new DataColumn("StorageRemain", typeof(string));
                var columnStorageNumber = new DataColumn("StorageNumber", typeof(string));

                dataTable.Columns.Add(columnStorageCapacity);
                dataTable.Columns.Add(columnStorageRemain);
                dataTable.Columns.Add(columnStorageNumber);

                var newRow = dataTable.NewRow();
                newRow["StorageCapacity"] = storageCapacity;
                newRow["StorageRemain"] = storageRemain;
                newRow["StorageNumber"] = storageNumber;

                return result;
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return CommonUtil.ResponseData(ResponseType.Failed, ex.Message);
            }
        }

        #endregion // Storage

        #region Record Schedule

        /// <summary>
        /// The add record schedule info.
        /// </summary>
        /// <param name="stream">
        /// The stream.
        /// </param>
        /// <returns>
        /// The response data.
        /// </returns>
        /// <exception cref="Exception">
        /// The exception.
        /// </exception>
        public Stream AddRecordScheduleInfo(Stream stream)
        {
            try
            {
                // 저장 위치에 있는 xaml 문자열을 가져와서 Stream으로 return.
                if (WebOperationContext.Current == null)
                {
                    throw new Exception("Can not get current WebOpreationContext.");
                }

                // Content는 무조건 xml임.
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/xml";

                using (var readStream = new StreamReader(stream))
                {
                    var recordScheduleDataSet = new DataSet();

                    recordScheduleDataSet.ReadXml(readStream);

                    foreach (DataRow row in recordScheduleDataSet.Tables[0].Rows)
                    {
                        var resultString =
                            DataManager.GetInstance.SetData.InsertRecordSchedule(
                                int.Parse(row["RecordScheduleNumber"].ToString().Trim()),
                                row["RecordScheduleName"].ToString().Trim(),
                                row["RecordScheduleData"].ToString().Trim());

                        if (!string.IsNullOrWhiteSpace(resultString))
                        {
                            return CommonUtil.ResponseData(ResponseType.Failed, resultString);
                        }
                    }
                }

                return CommonUtil.ResponseData(ResponseType.Success);
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return CommonUtil.ResponseData(ResponseType.Failed, ex.Message);
            }
        }

        /// <summary>
        /// The import record schedule info.
        /// </summary>
        /// <param name="stream">
        /// The stream.
        /// </param>
        /// <returns>
        /// The <see cref="Stream"/>.
        /// </returns>
        /// <exception cref="Exception">
        /// </exception>
        public Stream ImportRecordScheduleInfo(Stream stream)
        {
            try
            {
                // 저장 위치에 있는 xaml 문자열을 가져와서 Stream으로 return.
                if (WebOperationContext.Current == null)
                {
                    throw new Exception("Can not get current WebOpreationContext.");
                }

                // Content는 무조건 xml임.
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/xml";

                using (var readStream = new StreamReader(stream))
                {
                    var recordScheduleDataSet = new DataSet();

                    recordScheduleDataSet.ReadXml(readStream);

                    var count = 0;

                    foreach (DataRow row in recordScheduleDataSet.Tables[0].Rows)
                    {
                        var resultString =
                            DataManager.GetInstance.SetData.ImportRecordSchedule(
                                int.Parse(row["RecordScheduleNumber"].ToString().Trim()),
                                row["RecordScheduleName"].ToString().Trim(),
                                row["RecordScheduleData"].ToString().Trim(),
                                count);

                        if (!string.IsNullOrWhiteSpace(resultString))
                        {
                            return CommonUtil.ResponseData(ResponseType.Failed, resultString);
                        }

                        count++;
                    }
                }

                return CommonUtil.ResponseData(ResponseType.Success);
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return CommonUtil.ResponseData(ResponseType.Failed, ex.Message);
            }
        }

        /// <summary>
        /// The update record schedule info.
        /// </summary>
        /// <param name="stream">
        /// The stream.
        /// </param>
        /// <returns>
        /// The response data.
        /// </returns>
        /// <exception cref="Exception">
        /// The exception.
        /// </exception>
        public Stream UpdateRecordScheduleInfo(Stream stream)
        {
            try
            {
                // 저장 위치에 있는 xaml 문자열을 가져와서 Stream으로 return.
                if (WebOperationContext.Current == null)
                {
                    throw new Exception("Can not get current WebOpreationContext.");
                }

                // Content는 무조건 xml임.
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/xml";

                Stream responseData = null;

                using (var readStream = new StreamReader(stream))
                {
                    var recordScheduleDataSet = new DataSet();

                    recordScheduleDataSet.ReadXml(readStream);

                    foreach (DataRow row in recordScheduleDataSet.Tables[0].Rows)
                    {
                        responseData =
                            DataManager.GetInstance.SetData.UpdateRecordSchedule(
                                int.Parse(row["RecordScheduleNumber"].ToString().Trim()),
                                row["RecordScheduleName"].ToString().Trim(),
                                row["RecordScheduleData"].ToString().Trim());
                    }
                }

                return responseData;
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return CommonUtil.ResponseData(ResponseType.Failed, ex.Message);
            }
        }

        /// <summary>
        /// The delete record schedule info.
        /// </summary>
        /// <param name="stream">
        /// The stream.
        /// </param>
        /// <returns>
        /// The response data.
        /// </returns>
        /// <exception cref="Exception">
        /// The exception.
        /// </exception>
        public Stream DeleteRecordScheduleInfo(Stream stream)
        {
            try
            {
                // 저장 위치에 있는 xaml 문자열을 가져와서 Stream으로 return.
                if (WebOperationContext.Current == null)
                {
                    throw new Exception("Can not get current WebOpreationContext.");
                }

                // Content는 무조건 xml임.
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/xml";

                Stream responseData = null;

                using (var readStream = new StreamReader(stream))
                {
                    var recordScheduleDataSet = new DataSet();

                    recordScheduleDataSet.ReadXml(readStream);

                    foreach (DataRow row in recordScheduleDataSet.Tables[0].Rows)
                    {
                        responseData =
                            DataManager.GetInstance.SetData.DeleteRecordSchedule(
                                int.Parse(row["RecordScheduleNumber"].ToString().Trim()));
                    }
                }

                return responseData;
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return CommonUtil.ResponseData(ResponseType.Failed, ex.Message);
            }
        }

        #endregion // Record Schedule

        #region Archive Schedule

        /// <summary>
        /// The add archive schedule info.
        /// </summary>
        /// <param name="stream">
        /// The stream.
        /// </param>
        /// <returns>
        /// The response data.
        /// </returns>
        /// <exception cref="Exception">
        /// The exception.
        /// </exception>
        public Stream AddArchiveScheduleInfo(Stream stream)
        {
            try
            {
                // 저장 위치에 있는 xaml 문자열을 가져와서 Stream으로 return.
                if (WebOperationContext.Current == null)
                {
                    throw new Exception("Can not get current WebOpreationContext.");
                }

                // Content는 무조건 xml임.
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/xml";

                using (var readStream = new StreamReader(stream))
                {
                    var archiveScheduleDataSet = new DataSet();

                    archiveScheduleDataSet.ReadXml(readStream);

                    foreach (DataRow row in archiveScheduleDataSet.Tables[0].Rows)
                    {
                        var resultString =
                            DataManager.GetInstance.SetData.InsertArchiveSchedule(
                                int.Parse(row["ArchiveScheduleNumber"].ToString().Trim()),
                                row["ArchiveScheduleName"].ToString().Trim(),
                                int.Parse(row["RetentionPeriod"].ToString().Trim()));

                        if (!string.IsNullOrWhiteSpace(resultString))
                        {
                            return CommonUtil.ResponseData(ResponseType.Failed, resultString);
                        }
                    }
                }

                return CommonUtil.ResponseData(ResponseType.Success);
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return CommonUtil.ResponseData(ResponseType.Failed, ex.Message);
            }
        }

        /// <summary>
        /// The update archive schedule info.
        /// </summary>
        /// <param name="stream">
        /// The stream.
        /// </param>
        /// <returns>
        /// The response data.
        /// </returns>
        /// <exception cref="Exception">
        /// The exception.
        /// </exception>
        public Stream UpdateArchiveScheduleInfo(Stream stream)
        {
            try
            {
                // 저장 위치에 있는 xaml 문자열을 가져와서 Stream으로 return.
                if (WebOperationContext.Current == null)
                {
                    throw new Exception("Can not get current WebOpreationContext.");
                }

                // Content는 무조건 xml임.
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/xml";

                Stream responseData = null;

                using (var readStream = new StreamReader(stream))
                {
                    var archiveScheduleDataSet = new DataSet();

                    archiveScheduleDataSet.ReadXml(readStream);

                    foreach (DataRow row in archiveScheduleDataSet.Tables[0].Rows)
                    {
                        responseData =
                            DataManager.GetInstance.SetData.UpdateArchiveSchedule(
                                int.Parse(row["ArchiveScheduleNumber"].ToString().Trim()),
                                row["ArchiveScheduleName"].ToString().Trim(),
                                int.Parse(row["RetentionPeriod"].ToString().Trim()));
                    }
                }

                return responseData;
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return CommonUtil.ResponseData(ResponseType.Failed, ex.Message);
            }
        }

        /// <summary>
        /// The import archive schedule info.
        /// </summary>
        /// <param name="stream">
        /// The stream.
        /// </param>
        /// <returns>
        /// The <see cref="Stream"/>.
        /// </returns>
        /// <exception cref="Exception">
        /// </exception>
        public Stream ImportArchiveScheduleInfo(Stream stream)
        {
            try
            {
                // 저장 위치에 있는 xaml 문자열을 가져와서 Stream으로 return.
                if (WebOperationContext.Current == null)
                {
                    throw new Exception("Can not get current WebOpreationContext.");
                }

                // Content는 무조건 xml임.
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/xml";

                Stream responseData = null;

                using (var readStream = new StreamReader(stream))
                {
                    var archiveScheduleDataSet = new DataSet();

                    archiveScheduleDataSet.ReadXml(readStream);

                    var count = 0;

                    foreach (DataRow row in archiveScheduleDataSet.Tables[0].Rows)
                    {
                        responseData =
                            DataManager.GetInstance.SetData.ImportArchiveSchedule(
                                int.Parse(row["ArchiveScheduleNumber"].ToString().Trim()),
                                row["ArchiveScheduleName"].ToString().Trim(),
                                int.Parse(row["RetentionPeriod"].ToString().Trim()),
                                count);

                        count++;
                    }
                }

                return responseData;
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return CommonUtil.ResponseData(ResponseType.Failed, ex.Message);
            }
        }

        /// <summary>
        /// The delete archive schedule info.
        /// </summary>
        /// <param name="stream">
        /// The stream.
        /// </param>
        /// <returns>
        /// The response data.
        /// </returns>
        /// <exception cref="Exception">
        /// The exception.
        /// </exception>
        public Stream DeleteArchiveScheduleInfo(Stream stream)
        {
            try
            {
                // 저장 위치에 있는 xaml 문자열을 가져와서 Stream으로 return.
                if (WebOperationContext.Current == null)
                {
                    throw new Exception("Can not get current WebOpreationContext.");
                }

                // Content는 무조건 xml임.
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/xml";

                Stream responseData = null;

                using (var readStream = new StreamReader(stream))
                {
                    var archiveScheduleDataSet = new DataSet();

                    archiveScheduleDataSet.ReadXml(readStream);

                    foreach (DataRow row in archiveScheduleDataSet.Tables[0].Rows)
                    {
                        responseData =
                            DataManager.GetInstance.SetData.DeleteArchiveSchedule(
                                int.Parse(row["ArchiveScheduleNumber"].ToString().Trim()));
                    }
                }

                return responseData;
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return CommonUtil.ResponseData(ResponseType.Failed, ex.Message);
            }
        }

        #endregion // Archive Schedule

        #region Record Camera

        /// <summary>
        /// The add record camera info.
        /// </summary>
        /// <param name="stream">
        /// The stream.
        /// </param>
        /// <returns>
        /// The response data.
        /// </returns>
        /// <exception cref="Exception">
        /// The exception.
        /// </exception>
        public Stream AddRecordCameraInfo(Stream stream)
        {
            try
            {
                // 저장 위치에 있는 xaml 문자열을 가져와서 Stream으로 return.
                if (WebOperationContext.Current == null)
                {
                    throw new Exception("Can not get current WebOpreationContext.");
                }

                // Content는 무조건 xml임.
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/xml";


                using (var readStream = new StreamReader(stream))
                {
                    var recordCameraDataSet = new DataSet();

                    recordCameraDataSet.ReadXml(readStream);

                    foreach (DataRow row in recordCameraDataSet.Tables[0].Rows)
                    {
                        var resultString =
                            DataManager.GetInstance.SetData.InsertRecordCamera(
                                int.Parse(row["RecordCameraNumber"].ToString().Trim()),
                                bool.Parse(row["UseLocalDisk"].ToString().Trim()),
                                row["RecordStorageList"].ToString().Trim(),
                                row["ArchiveStorageList"].ToString().Trim(),
                                row["RecordType"].ToString().Trim(),
                                int.Parse(row["RecordScheduleNumber"].ToString().Trim()),
                                int.Parse(row["ArchiveScheduleNumber"].ToString().Trim()),
                                int.Parse(row["RetentionPeriod"].ToString().Trim()),
                                bool.Parse(row["UseVoice"].ToString().Trim()),
                                row["RecorderID"].ToString().Trim(),
                                IsCheckedForNullableInt.IsCheckedData(row["Width"].ToString()),
                                IsCheckedForNullableInt.IsCheckedData(row["Height"].ToString()),
                                IsCheckedForNullableInt.IsCheckedData(row["FPS"].ToString()),
                                IsCheckedForNullableInt.IsCheckedData(row["Bitrate"].ToString()),
                                row["Codec"].ToString().Trim(),
                                bool.Parse(row["UseEvent"].ToString().Trim()),
                                IsCheckedForNullableInt.IsCheckedData(row["EventWidth"].ToString()),
                                IsCheckedForNullableInt.IsCheckedData(row["EventHeight"].ToString()),
                                IsCheckedForNullableInt.IsCheckedData(row["EventFPS"].ToString()),
                                IsCheckedForNullableInt.IsCheckedData(row["EventBitrate"].ToString()),
                                row["EventCodec"].ToString().Trim());

                        if (!string.IsNullOrWhiteSpace(resultString))
                        {
                            return CommonUtil.ResponseData(ResponseType.Failed, resultString);
                        }
                    }
                }

                return CommonUtil.ResponseData(ResponseType.Success);
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return CommonUtil.ResponseData(ResponseType.Failed, ex.Message);
            }
        }

        /// <summary>
        /// The import record camera info.
        /// </summary>
        /// <param name="stream">
        /// The stream.
        /// </param>
        /// <returns>
        /// The <see cref="Stream"/>.
        /// </returns>
        /// <exception cref="Exception">
        /// </exception>
        public Stream ImportRecordCameraInfo(Stream stream)
        {
            try
            {
                // 저장 위치에 있는 xaml 문자열을 가져와서 Stream으로 return.
                if (WebOperationContext.Current == null)
                {
                    throw new Exception("Can not get current WebOpreationContext.");
                }

                // Content는 무조건 xml임.
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/xml";


                using (var readStream = new StreamReader(stream))
                {
                    var recordCameraDataSet = new DataSet();

                    recordCameraDataSet.ReadXml(readStream);

                    var count = 0;

                    foreach (DataRow row in recordCameraDataSet.Tables[0].Rows)
                    {
                        var resultString =
                            DataManager.GetInstance.SetData.ImportRecordCamera(
                                int.Parse(row["RecordCameraNumber"].ToString().Trim()),
                                bool.Parse(row["UseLocalDisk"].ToString().Trim()),
                                row["RecordStorageList"].ToString().Trim(),
                                row["ArchiveStorageList"].ToString().Trim(),
                                row["RecordType"].ToString().Trim(),
                                int.Parse(row["RecordScheduleNumber"].ToString().Trim()),
                                int.Parse(row["ArchiveScheduleNumber"].ToString().Trim()),
                                int.Parse(row["RetentionPeriod"].ToString().Trim()),
                                bool.Parse(row["UseVoice"].ToString().Trim()),
                                row["RecorderID"].ToString().Trim(),
                                IsCheckedForNullableInt.IsCheckedData(row["Width"].ToString()),
                                IsCheckedForNullableInt.IsCheckedData(row["Height"].ToString()),
                                IsCheckedForNullableInt.IsCheckedData(row["FPS"].ToString()),
                                IsCheckedForNullableInt.IsCheckedData(row["Bitrate"].ToString()),
                                row["Codec"].ToString().Trim(),
                                string.IsNullOrEmpty(row["UseEvent"].ToString()) ? false : bool.Parse(row["UseEvent"].ToString().Trim()),
                                IsCheckedForNullableInt.IsCheckedData(row["EventWidth"].ToString()),
                                IsCheckedForNullableInt.IsCheckedData(row["EventHeight"].ToString()),
                                IsCheckedForNullableInt.IsCheckedData(row["EventFPS"].ToString()),
                                IsCheckedForNullableInt.IsCheckedData(row["EventBitrate"].ToString()),
                                row["EventCodec"].ToString().Trim(),
                                count);

                        if (!string.IsNullOrWhiteSpace(resultString))
                        {
                            return CommonUtil.ResponseData(ResponseType.Failed, resultString);
                        }

                        count++;
                    }
                }

                return CommonUtil.ResponseData(ResponseType.Success);
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return CommonUtil.ResponseData(ResponseType.Failed, ex.Message);
            }
        }

        /// <summary>
        /// The update record camera info.
        /// </summary>
        /// <param name="stream">
        /// The stream.
        /// </param>
        /// <returns>
        /// The response data.
        /// </returns>
        /// <exception cref="Exception">
        /// The exception.
        /// </exception>
        public Stream UpdateRecordCameraInfo(Stream stream)
        {
            try
            {
                // 저장 위치에 있는 xaml 문자열을 가져와서 Stream으로 return.
                if (WebOperationContext.Current == null)
                {
                    throw new Exception("Can not get current WebOpreationContext.");
                }

                // Content는 무조건 xml임.
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/xml";

                Stream responseData = null;

                using (var readStream = new StreamReader(stream))
                {
                    var recordCameraDataSet = new DataSet();

                    recordCameraDataSet.ReadXml(readStream);

                    foreach (DataRow row in recordCameraDataSet.Tables[0].Rows)
                    {
                        responseData =
                            DataManager.GetInstance.SetData.UpdateRecordCamera(
                                int.Parse(row["RecordCameraNumber"].ToString().Trim()),
                                bool.Parse(row["UseLocalDisk"].ToString().Trim()),
                                row["RecordStorageList"].ToString().Trim(),
                                row["ArchiveStorageList"].ToString().Trim(),
                                row["RecordType"].ToString().Trim(),
                                int.Parse(row["RecordScheduleNumber"].ToString().Trim()),
                                int.Parse(row["ArchiveScheduleNumber"].ToString().Trim()),
                                int.Parse(row["RetentionPeriod"].ToString().Trim()),
                                bool.Parse(row["UseVoice"].ToString().Trim()),
                                row["RecorderID"].ToString().Trim(),
                                IsCheckedForNullableInt.IsCheckedData(row["Width"].ToString()),
                                IsCheckedForNullableInt.IsCheckedData(row["Height"].ToString()),
                                IsCheckedForNullableInt.IsCheckedData(row["FPS"].ToString()),
                                IsCheckedForNullableInt.IsCheckedData(row["Bitrate"].ToString()),
                                row["Codec"].ToString().Trim(),
                                bool.Parse(row["UseEvent"].ToString().Trim()),
                                IsCheckedForNullableInt.IsCheckedData(row["EventWidth"].ToString()),
                                IsCheckedForNullableInt.IsCheckedData(row["EventHeight"].ToString()),
                                IsCheckedForNullableInt.IsCheckedData(row["EventFPS"].ToString()),
                                IsCheckedForNullableInt.IsCheckedData(row["EventBitrate"].ToString()),
                                row["EventCodec"].ToString().Trim());
                    }
                }

                return responseData;
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return CommonUtil.ResponseData(ResponseType.Failed, ex.Message);
            }
        }

        /// <summary>
        /// The delete record camera info.
        /// </summary>
        /// <param name="stream">
        /// The stream.
        /// </param>
        /// <returns>
        /// The response data.
        /// </returns>
        /// <exception cref="Exception">
        /// The exception.
        /// </exception>
        public Stream DeleteRecordCameraInfo(Stream stream)
        {
            try
            {
                // 저장 위치에 있는 xaml 문자열을 가져와서 Stream으로 return.
                if (WebOperationContext.Current == null)
                {
                    throw new Exception("Can not get current WebOpreationContext.");
                }

                // Content는 무조건 xml임.
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/xml";

                Stream responseData = null;

                using (var readStream = new StreamReader(stream))
                {
                    var recordCameraDataSet = new DataSet();

                    recordCameraDataSet.ReadXml(readStream);

                    foreach (DataRow row in recordCameraDataSet.Tables[0].Rows)
                    {
                        responseData =
                            DataManager.GetInstance.SetData.DeleteRecordCamera(
                                int.Parse(row["RecordCameraNumber"].ToString().Trim()));
                    }
                }

                return responseData;
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return CommonUtil.ResponseData(ResponseType.Failed, ex.Message);
            }
        }

        #endregion // Record Camera

        #region Configuration

        /// <summary>
        /// The add configuration info.
        /// </summary>
        /// <param name="stream">
        /// The stream.
        /// </param>
        /// <returns>
        /// The response data.
        /// </returns>
        /// <exception cref="Exception">
        /// The exception.
        /// </exception>
        public Stream AddConfigurationInfo(Stream stream)
        {
            try
            {
                // 저장 위치에 있는 xaml 문자열을 가져와서 Stream으로 return.
                if (WebOperationContext.Current == null)
                {
                    throw new Exception("Can not get current WebOpreationContext.");
                }

                // Content는 무조건 xml임.
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/xml";

                using (var readStream = new StreamReader(stream))
                {
                    var configurationDataSet = new DataSet();

                    configurationDataSet.ReadXml(readStream);

                    foreach (DataRow row in configurationDataSet.Tables[0].Rows)
                    {
                        var resultString = DataManager.GetInstance.SetData.InsertConfiguration(
                                        int.Parse(row["RESTServerPort"].ToString().Trim()),
                                        int.Parse(row["IMSPServerPort"].ToString().Trim()),
                                        row["EventRecordStorageList"].ToString().Trim());

                        if (!string.IsNullOrWhiteSpace(resultString))
                        {
                            return CommonUtil.ResponseData(ResponseType.Failed, resultString);
                        }
                    }
                }

                return CommonUtil.ResponseData(ResponseType.Success);
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return CommonUtil.ResponseData(ResponseType.Failed, ex.Message);
            }
        }

        /// <summary>
        /// The import configuration info.
        /// </summary>
        /// <param name="stream">
        /// The stream.
        /// </param>
        /// <returns>
        /// The <see cref="Stream"/>.
        /// </returns>
        /// <exception cref="Exception">
        /// </exception>
        public Stream ImportConfigurationInfo(Stream stream)
        {
            try
            {
                // 저장 위치에 있는 xaml 문자열을 가져와서 Stream으로 return.
                if (WebOperationContext.Current == null)
                {
                    throw new Exception("Can not get current WebOpreationContext.");
                }

                // Content는 무조건 xml임.
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/xml";

                using (var readStream = new StreamReader(stream))
                {
                    var configurationDataSet = new DataSet();

                    configurationDataSet.ReadXml(readStream);

                    var count = 0;

                    foreach (DataRow row in configurationDataSet.Tables[0].Rows)
                    {
                        var resultString = DataManager.GetInstance.SetData.ImportConfiguration(
                            int.Parse(row["Index"].ToString().Trim()),
                            int.Parse(row["RESTServerPort"].ToString().Trim()),
                            int.Parse(row["IMSPServerPort"].ToString().Trim()),
                            row["EventRecordStorageList"].ToString().Trim(),
                            count);

                        if (!string.IsNullOrWhiteSpace(resultString))
                        {
                            return CommonUtil.ResponseData(ResponseType.Failed, resultString);
                        }

                        count++;
                    }
                }

                return CommonUtil.ResponseData(ResponseType.Success);
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return CommonUtil.ResponseData(ResponseType.Failed, ex.Message);
            }
        }

        /// <summary>
        /// The update configuration info.
        /// </summary>
        /// <param name="stream">
        /// The stream.
        /// </param>
        /// <returns>
        /// The response data.
        /// </returns>
        /// <exception cref="Exception">
        /// The exception.
        /// </exception>
        public Stream UpdateConfigurationInfo(Stream stream)
        {
            try
            {
                // 저장 위치에 있는 xaml 문자열을 가져와서 Stream으로 return.
                if (WebOperationContext.Current == null)
                {
                    throw new Exception("Can not get current WebOpreationContext.");
                }

                // Content는 무조건 xml임.
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/xml";

                Stream responseData = null;

                using (var readStream = new StreamReader(stream))
                {
                    var configurationDataSet = new DataSet();

                    configurationDataSet.ReadXml(readStream);

                    foreach (DataRow row in configurationDataSet.Tables[0].Rows)
                    {
                        responseData = DataManager.GetInstance.SetData.UpdateConfiguration(
                            int.Parse(row["Index"].ToString().Trim()),
                            int.Parse(row["RESTServerPort"].ToString().Trim()),
                            int.Parse(row["IMSPServerPort"].ToString().Trim()),
                            row["EventRecordStorageList"].ToString().Trim());
                    }
                }

                return responseData;
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return CommonUtil.ResponseData(ResponseType.Failed, ex.Message);
            }
        }

        /// <summary>
        /// The delete configuration info.
        /// </summary>
        /// <param name="stream">
        /// The stream.
        /// </param>
        /// <returns>
        /// The response data.
        /// </returns>
        /// <exception cref="Exception">
        /// The exception.
        /// </exception>
        public Stream DeleteConfigurationInfo(Stream stream)
        {
            try
            {
                // 저장 위치에 있는 xaml 문자열을 가져와서 Stream으로 return.
                if (WebOperationContext.Current == null)
                {
                    throw new Exception("Can not get current WebOpreationContext.");
                }

                // Content는 무조건 xml임.
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/xml";

                Stream responseData = null;

                using (var readStream = new StreamReader(stream))
                {
                    var configurationDataSet = new DataSet();

                    configurationDataSet.ReadXml(readStream);

                    foreach (DataRow row in configurationDataSet.Tables[0].Rows)
                    {
                        responseData =
                            DataManager.GetInstance.SetData.DeleteConfiguration(
                                int.Parse(row["Index"].ToString().Trim()));
                    }
                }

                return responseData;
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return CommonUtil.ResponseData(ResponseType.Failed, ex.Message);
            }
        }

        #endregion // Configuration

        #region Delete Record Media Index

        /// <summary>
        /// The request delete to record media index.
        /// </summary>
        /// <param name="stream">
        /// The stream.
        /// </param>
        /// <param name="ip">
        /// The ip.
        /// </param>
        /// <param name="port">
        /// The port.
        /// </param>
        /// <returns>
        /// The <see cref="Stream"/>.
        /// </returns>
        public Stream RequestDeleteToRecordMediaIndex(Stream stream, string ip, string port)
        {
            using (var consolePrinter = new DataServiceMessage())
            {
                try
                {
                    using (var readStream = new StreamReader(stream))
                    {
                        var dataSet = new DataSet();
                        dataSet.ReadXml(readStream);

                        var stringWriteData = new StringWriter();
                        dataSet.WriteXml(stringWriteData, XmlWriteMode.WriteSchema);

                        var postData = stringWriteData.ToString();

                        var param = new RequestParameter()
                        {
                            Url = string.Format(@"http://{0}:{1}/rest/storage/deleterecordmediaindex", ip, port),
                            EncodingOption = "UTF8",
                            PostMessage = postData
                        };
                        
                        var response = DataServiceHandler.RequestToStreamResponse(param, true, true);

                        consolePrinter.PrintResponseMessage();
                        
                        return response;
                    }
                }
                catch (Exception ex)
                {
                    consolePrinter.PrintExceptionMessage(ex);
                    Logger.Write(Logger.Error, ex.ToString());

                    return CommonUtil.ResponseData(ResponseType.Failed, ex.ToString());
                }
            }
        }

        /// <summary>
        /// The request record type changed.
        /// </summary>
        /// <param name="recordCameraNumber">
        /// The record camera number.
        /// </param>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <param name="recorderId">
        /// The recorder Id.
        /// </param>
        /// <returns>
        /// The <see cref="Stream"/>.
        /// </returns>
        public Stream RequestRecordTypeChanged(string recordCameraNumber, string value, string recorderId)
        {
            using (var consolePrinter = new DataServiceMessage())
            {
                try
                {
                    var getData = DataManager.GetInstance.GetDataForConfiguration.GetRecorderInfo();
                    var bytes = Encoding.UTF8.GetBytes(getData);
                    var recorderInfo = new MemoryStream(bytes);
                    var dataSet = new DataSet();
                    dataSet.ReadXml(recorderInfo);

                    var recorderIp = string.Empty;
                    var port = string.Empty;

                    foreach (var dataRow in dataSet.Tables[0].Rows.Cast<DataRow>().Where(dataRow => dataRow["RecorderID"].ToString().Equals(recorderId)))
                    {
                        recorderIp = dataRow["RecorderIP"].ToString();
                        port = dataRow["RESTServerPort"].ToString();
                        break;
                    }

                    var param = new RequestParameter()
                    {
                        Url = string.Format(@"http://{0}:{1}/rest/data/setcamerainformation?recordcameranumber={2}&field=RecordType&value={3}", recorderIp, port, recordCameraNumber, value),
                        EncodingOption = "UTF8",
                        PostMessage = string.Empty
                    };

                    var response = DataServiceHandler.RequestToStreamResponse(param, true, true);

                    consolePrinter.PrintResponseMessage();

                    return response;
                }
                catch (Exception ex)
                {
                    consolePrinter.PrintExceptionMessage(ex);
                    Logger.Write(Logger.Error, ex.ToString());

                    return CommonUtil.ResponseData(ResponseType.Failed, ex.ToString());
                }
            }
        }

        /// <summary>
        /// The reserve recording index delete.
        /// </summary>
        /// <param name="recordCameraNumber">
        /// The record camera number.
        /// </param>
        /// <returns>
        /// The <see cref="Stream"/>.
        /// </returns>
        public Stream ReserveRecordingIndexDelete(string recordCameraNumber)
        {
            using (var consolePrinter = new DataServiceMessage())
            {
                try
                {
                    var recorderId = GetRecorderId(recordCameraNumber);
                    
                    var recorderList = DataManager.GetInstance.GetDataForConfiguration.GetRecorderInfo();
                    var bytes = Encoding.UTF8.GetBytes(recorderList);
                    var recorderInfo = new MemoryStream(bytes);
                    var recorderDataSet = new DataSet();
                    recorderDataSet.ReadXml(recorderInfo);

                    var recorderIp = string.Empty;
                    var port = string.Empty;

                    foreach (var dataRow in recorderDataSet.Tables[0].Rows.Cast<DataRow>().Where(dataRow => dataRow["RecorderID"].ToString().Equals(recorderId)))
                    {
                        recorderIp = dataRow["RecorderIP"].ToString();
                        port = dataRow["RESTServerPort"].ToString();
                        break;
                    }

                    var param = new RequestParameter()
                    {
                        Url = string.Format(@"http://{0}:{1}/rest/storage/readytodeleterecord?recordcameranumber={2}&reserve=true", recorderIp, port, recordCameraNumber),
                        EncodingOption = "UTF8",
                        PostMessage = string.Empty
                    };

                    var response = DataServiceHandler.RequestToStreamResponse(param, false, true);

                    consolePrinter.PrintResponseMessage();

                    if (response.Equals(null))
                    {
                        return CommonUtil.ResponseData(
                            ResponseType.Failed,
                            string.Format("{0} will not be able to connect to the recorder.", recorderIp));
                    }

                    return response;
                }
                catch (Exception ex)
                {
                    consolePrinter.PrintExceptionMessage(ex);
                    Logger.Write(Logger.Error, ex.ToString());

                    return CommonUtil.ResponseData(ResponseType.Failed, ex.ToString());
                }
            }
        }

        /// <summary>
        /// The reserve cancel recording index delete.
        /// </summary>
        /// <param name="recordCameraNumber">
        /// The record camera number.
        /// </param>
        /// <returns>
        /// The <see cref="Stream"/>.
        /// </returns>
        public Stream ReserveCancelRecordingIndexDelete(string recordCameraNumber)
        {
            using (var consolePrinter = new DataServiceMessage())
            {
                try
                {
                    var recorderId = GetRecorderId(recordCameraNumber);

                    var recorderList = DataManager.GetInstance.GetDataForConfiguration.GetRecorderInfo();
                    var bytes = Encoding.UTF8.GetBytes(recorderList);
                    var recorderInfo = new MemoryStream(bytes);
                    var recorderDataSet = new DataSet();
                    recorderDataSet.ReadXml(recorderInfo);

                    var recorderIp = string.Empty;
                    var port = string.Empty;

                    foreach (var dataRow in recorderDataSet.Tables[0].Rows.Cast<DataRow>().Where(dataRow => dataRow["RecorderID"].ToString().Equals(recorderId)))
                    {
                        recorderIp = dataRow["RecorderIP"].ToString();
                        port = dataRow["RESTServerPort"].ToString();
                        break;
                    }

                    var param = new RequestParameter()
                    {
                        Url = string.Format(@"http://{0}:{1}/rest/storage/readytodeleterecord?recordcameranumber={2}&reserve=false", recorderIp, port, recordCameraNumber),
                        EncodingOption = "UTF8",
                        PostMessage = string.Empty
                    };

                    var response = DataServiceHandler.RequestToStreamResponse(param, false, true);

                    consolePrinter.PrintResponseMessage();

                    if (response == null)
                    {
                        return CommonUtil.ResponseData(
                            ResponseType.Failed,
                            string.Format("{0} will not be able to connect to the recorder.", recorderIp));
                    }

                    return response;
                }
                catch (Exception ex)
                {
                    consolePrinter.PrintExceptionMessage(ex);
                    Logger.Write(Logger.Error, ex.ToString());

                    return CommonUtil.ResponseData(ResponseType.Failed, ex.ToString());
                }
            }
        }

        /// <summary>
        /// The reserve recording index delete by record camera.
        /// </summary>
        /// <param name="recordCameraNumber">
        /// The record camera number.
        /// </param>
        /// <param name="recordId">
        /// The record id.
        /// </param>
        /// <returns>
        /// The <see cref="Stream"/>.
        /// </returns>
        public Stream ReserveRecordingIndexDeleteByRecordCamera(string recordCameraNumber, string recordId)
        {
            using (var consolePrinter = new DataServiceMessage())
            {
                try
                {
                    var recorderList = DataManager.GetInstance.GetDataForConfiguration.GetRecorderInfo();
                    var bytes = Encoding.UTF8.GetBytes(recorderList);
                    var recorderInfo = new MemoryStream(bytes);
                    var recorderDataSet = new DataSet();
                    recorderDataSet.ReadXml(recorderInfo);

                    var recorderIp = string.Empty;
                    var port = string.Empty;

                    foreach (var dataRow in recorderDataSet.Tables[0].Rows.Cast<DataRow>().Where(dataRow => dataRow["RecorderID"].ToString().Equals(recordId)))
                    {
                        recorderIp = dataRow["RecorderIP"].ToString();
                        port = dataRow["RESTServerPort"].ToString();
                        break;
                    }

                    var param = new RequestParameter()
                    {
                        Url = string.Format(@"http://{0}:{1}/rest/storage/readytodeleterecord?recordcameranumber={2}&reserve=true", recorderIp, port, recordCameraNumber),
                        EncodingOption = "UTF8",
                        PostMessage = string.Empty
                    };

                    var response = DataServiceHandler.RequestToStreamResponse(param, false, true);

                    consolePrinter.PrintResponseMessage();

                    if (response.Equals(null))
                    {
                        return CommonUtil.ResponseData(
                            ResponseType.Failed,
                            string.Format("{0} will not be able to connect to the recorder.", recorderIp));
                    }

                    return response;
                }
                catch (Exception ex)
                {
                    consolePrinter.PrintExceptionMessage(ex);
                    Logger.Write(Logger.Error, ex.ToString());

                    return CommonUtil.ResponseData(ResponseType.Failed, ex.ToString());
                }
            }
        }

        /// <summary>
        /// The reserve cancel recording index delete by record camera.
        /// </summary>
        /// <param name="recordCameraNumber">
        /// The record camera number.
        /// </param>
        /// <param name="recordId">
        /// The record id.
        /// </param>
        /// <returns>
        /// The <see cref="Stream"/>.
        /// </returns>
        public Stream ReserveCancelRecordingIndexDeleteByRecordCamera(string recordCameraNumber, string recordId)
        {
            using (var consolePrinter = new DataServiceMessage())
            {
                try
                {
                    var recorderList = DataManager.GetInstance.GetDataForConfiguration.GetRecorderInfo();
                    var bytes = Encoding.UTF8.GetBytes(recorderList);
                    var recorderInfo = new MemoryStream(bytes);
                    var recorderDataSet = new DataSet();
                    recorderDataSet.ReadXml(recorderInfo);

                    var recorderIp = string.Empty;
                    var port = string.Empty;

                    foreach (var dataRow in recorderDataSet.Tables[0].Rows.Cast<DataRow>().Where(dataRow => dataRow["RecorderID"].ToString().Equals(recordId)))
                    {
                        recorderIp = dataRow["RecorderIP"].ToString();
                        port = dataRow["RESTServerPort"].ToString();
                        break;
                    }

                    var param = new RequestParameter()
                    {
                        Url = string.Format(@"http://{0}:{1}/rest/storage/readytodeleterecord?recordcameranumber={2}&reserve=false", recorderIp, port, recordCameraNumber),
                        EncodingOption = "UTF8",
                        PostMessage = string.Empty
                    };

                    var response = DataServiceHandler.RequestToStreamResponse(param, false, true);

                    consolePrinter.PrintResponseMessage();

                    if (response == null)
                    {
                        return CommonUtil.ResponseData(
                            ResponseType.Failed,
                            string.Format("{0} will not be able to connect to the recorder.", recorderIp));
                    }

                    return response;
                }
                catch (Exception ex)
                {
                    consolePrinter.PrintExceptionMessage(ex);
                    Logger.Write(Logger.Error, ex.ToString());

                    return CommonUtil.ResponseData(ResponseType.Failed, ex.ToString());
                }
            }
        }

        private static string GetRecorderId(string recordCameraNumber)
        {
            var recorderId = string.Empty;

            var recordCameraNumberList = DataManager.GetInstance.GetDataForConfiguration.GetRecordCameraInfo();
            var bytes = Encoding.UTF8.GetBytes(recordCameraNumberList);
            var recordCameraInfos = new MemoryStream(bytes);
            var recordCameraDataSet = new DataSet();
            recordCameraDataSet.ReadXml(recordCameraInfos);

            foreach (var dataRow in recordCameraDataSet.Tables[0].Rows.Cast<DataRow>().Where(dataRow => dataRow["RecordCameraNumber"].ToString().Equals(recordCameraNumber)))
            {
                recorderId = dataRow["RecorderID"].ToString();
                break;
            }

            return recorderId;
        }

        #endregion // Delete Record Media Index

        #endregion // SetData of Nvr

        #endregion // SetData

        #endregion // Configuration

        #region URL Capture

        public Stream GetUrlCaptureServerInfo()
        {
            try
            {
                if (WebOperationContext.Current == null)
                {
                    throw new Exception("Can not get current WebOpreationContext.");
                }

                // Content는 무조건 xml임.
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/xml";

                var resultString = DataManager.GetInstance.GetDataForClient.GetUrlCaptureServerInfo();
                if (string.IsNullOrWhiteSpace(resultString))
                {
                    return null;
                }

                // Test
                //resultString = new StringReader(DataSetXmlSample.UrlCaptureInfo);


                var bytes = Encoding.UTF8.GetBytes(resultString);

                return new MemoryStream(bytes);
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return CommonUtil.ResponseData(ResponseType.Failed, ex.Message);
            }
        }

        #endregion // URL Capture

        public Stream GetPingInfo(string host)
        {
            try
            {
                if (WebOperationContext.Current == null)
                {
                    throw new Exception("Can not get current WebOpreationContext.");
                }

                // Content는 무조건 xml임.
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/xml";

                if (string.IsNullOrWhiteSpace(host) == true) return null;

                var resultPingReply = Helpers.PingHelper.Ping(host);
                var resultString = Helpers.PingHelper.SerializeToString(resultPingReply.Status);

                if (string.IsNullOrWhiteSpace(resultString))
                {
                    return null;
                }

                var bytes = Encoding.UTF8.GetBytes(resultString);

                return new MemoryStream(bytes);
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return CommonUtil.ResponseData(ResponseType.Failed, ex.Message);
            }
        }

        #region Map
        public string SetKmlPlaceMaker(XmlDocument doc, string projectId, string userId, string type)
        {
            string ret = string.Empty;

            try
            {
                //using (var readStream = new StreamReader(stream))
                //{
                    //XmlDocument doc = new XmlDocument();
                    //doc.Load(stream);

                foreach (XmlNode node in doc.SelectSingleNode("Document").ChildNodes)
                {
                    ret = DataManager.GetInstance.SetData.SetKmlPlaceMaker(
                                                    node.Attributes["KpmId"].Value.ToString(),
                                                    node.Attributes["KpmNm"].Value.ToString(),
                                                    node.Attributes["KpmParentId"].Value.ToString(),
                                                    node.Attributes["KpmLat"].Value.ToString(),
                                                    node.Attributes["KpmLon"].Value.ToString(),
                                                    node.Attributes["KpmAlt"].Value.ToString(),
                                                    node.Attributes["KpmDesc"].Value.ToString(),
                                                    node.Attributes["KpmIsFolder"].Value.ToString(),
                                                    node.Attributes["KpmStyleUrl"].Value.ToString(),
                                                    userId
                                                    );

                    if (!string.IsNullOrWhiteSpace(ret))
                    {
                        return ret;
                    }
                }

                return ret;
            }
            catch(Exception ex)
            {
                return ex.Message;
            }
        }

        #region Monitoring

        #region GetCommonCode
        public Stream GetCommonCode(string code)
        {
            try
            {
                if (WebOperationContext.Current == null)
                {
                    throw new Exception("Can not get current WebOpreationContext.");
                }

                // Content는 무조건 xml임.
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/xml";

                var ds = DataManager.GetInstance.GetDataForClient.GetCommonCode(code);
                var bytes = Encoding.UTF8.GetBytes(ds.GetXml());

                return new MemoryStream(bytes);
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return CommonUtil.ResponseData(ResponseType.Failed, ex.Message);
            }
        }
        #endregion //GetCommonCode

        #region GetServerStatus
        public Stream GetServerStatus()
        {
            try
            {
                if (WebOperationContext.Current == null)
                {
                    throw new Exception("Can not get current WebOpreationContext.");
                }

                // Content는 무조건 xml임.
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/xml";

                var ds = DataManager.GetInstance.GetDataForClient.GetServerStatus();
                var bytes = Encoding.UTF8.GetBytes(ds.GetXml());
                
                return new MemoryStream(bytes);

            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return CommonUtil.ResponseData(ResponseType.Failed, ex.Message);
            }
        }

        public string SetKmlPlaceMakerDelete(string KpmID, string projectId, string userId, string type)
        {
            string ret = string.Empty;

            try
            {
                //using (var readStream = new StreamReader(stream))
                //{
                //XmlDocument doc = new XmlDocument();
                //doc.Load(stream);

                //foreach (XmlNode node in doc.SelectSingleNode("KmlPlaceMaker").ChildNodes)
                //{
                //    ret = DataManager.GetInstance.SetData.SetKmlPlaceMakerDelete(
                //                                    node.Attributes["KpmId"].Value
                //                                    );

                //    if (!string.IsNullOrWhiteSpace(ret))
                //    {
                //        return ret;
                //    }
                //}

                ret = DataManager.GetInstance.SetData.SetKmlPlaceMakerDelete(KpmID);

                if (!string.IsNullOrWhiteSpace(ret))
                {
                    return ret;
                }

                return ret;
            }catch(Exception ex)
            {
                return ex.Message;
            }
        }
        #endregion //GetServerStatus

        #region GetServerStatusHistory

        public Stream GetServerStatusHistory(string StartTime, string EndTime, string SystemId)
        {
            try
            {
                if (WebOperationContext.Current == null)
                {
                    throw new Exception("Can Not Get current WebOerationContext.");
                }
                // Content는 무조건 xml임.
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/xml";

                var ds = DataManager.GetInstance.GetDataForClient.GetServerStatusHistory(StartTime, EndTime, SystemId);
                var bytes = Encoding.UTF8.GetBytes(ds.GetXml());

                return new MemoryStream(bytes);
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return CommonUtil.ResponseData(ResponseType.Failed, ex.Message);
            }
        }

        #endregion //GetServerStatusHistory

        #region GetCameraStatus
        public Stream GetCameraStatus()
        {
            try
            {
                if (WebOperationContext.Current == null)
                {
                    throw new Exception("Can not get current WebOpreationContext.");
                }

                // Content는 무조건 xml임.
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/xml";

                var ds = DataManager.GetInstance.GetDataForClient.GetCameraStatus();
                var bytes = Encoding.UTF8.GetBytes(ds.GetXml());

                return new MemoryStream(bytes);
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return CommonUtil.ResponseData(ResponseType.Failed, ex.Message);
            }
        }
        #endregion //GetCameraStatus

        #endregion // Monitoring

        #region Alarm

        #region GetAlarmRuleList
        public Stream GetAlarmRuleList()
        {
            try
            {
                if (WebOperationContext.Current == null)
                {
                    throw new Exception("Can not get current WebOpreationContext.");
                }

                // Content는 무조건 xml임.
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/xml";

                var ds = DataManager.GetInstance.GetDataForClient.GetAlarmRuleList();
                var bytes = Encoding.UTF8.GetBytes(ds.GetXml());

                return new MemoryStream(bytes);
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return CommonUtil.ResponseData(ResponseType.Failed, ex.Message);
            }
        }

        public string SetMapXaml(XmlDocument doc, string projectId, string userId, string type)
        {
            string ret = string.Empty;

            try
            {
                //using (var readStream = new StreamReader(stream))
                //{
                    //XmlDocument doc = new XmlDocument();
                    //doc.Load(stream);

                foreach (XmlNode node in doc.SelectSingleNode("Document").ChildNodes)
                {
                    ret = DataManager.GetInstance.SetData.SetMapXaml(
                                                    node.Attributes["MxlId"].Value,
                                                    node.Attributes["MxlNm"].Value,
                                                    node.Attributes["MxlData"].Value,
                                                    userId
                                                    );

                    if (!string.IsNullOrWhiteSpace(ret))
                    {
                        return ret;
                    }
                }

                return ret;
            } 
            catch(Exception ex)
            {
                return ex.Message;
            }
        }
        #endregion //GetAlarmRuleList

        #region GetAlarmRuleMapping
        public Stream GetAlarmRuleMapping()
        {
            try
            {
                if (WebOperationContext.Current == null)
                {
                    throw new Exception("Can not get current WebOpreationContext.");
                }

                // Content는 무조건 xml임.
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/xml";

                var ds = DataManager.GetInstance.GetDataForClient.GetAlarmRuleMapping();
                var bytes = Encoding.UTF8.GetBytes(ds.GetXml());

                return new MemoryStream(bytes);
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return CommonUtil.ResponseData(ResponseType.Failed, ex.Message);
            }
        }
        #endregion //GetAlarmRuleMapping

        #region AddAlarmRule
        public Stream AddAlarmRule(Stream stream)
        {
            try
            {
                // 저장 위치에 있는 xaml 문자열을 가져와서 Stream으로 return.
                if (WebOperationContext.Current == null)
                {
                    throw new Exception("Can not get current WebOpreationContext.");
                }

                // Content는 무조건 xml임.
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/xml";
                using (var readStream = new StreamReader(stream))
                {
                    XDocument doc = XDocument.Load(readStream);

                    var ruleName = doc.Descendants("RuleName")
                                       .Select(el => el.Value)
                                       .ToList()[0];

                    string responseData = DataManager.GetInstance.SetData.AddAlarmRule(ruleName, doc.ToString());

                    var bytes = Encoding.UTF8.GetBytes(responseData);

                    return new MemoryStream(bytes);
                }

            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return CommonUtil.ResponseData(ResponseType.Failed, ex.Message);
            }
        }

        #endregion //AddAlarmRule

        #region DeleteAlarmRule
        public Stream DeleteAlarmRule(Stream stream)
        {
            try
            {
                // 저장 위치에 있는 xaml 문자열을 가져와서 Stream으로 return.
                if (WebOperationContext.Current == null)
                {
                    throw new Exception("Can not get current WebOpreationContext.");
                }

                // Content는 무조건 xml임.
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/xml";
                using (var readStream = new StreamReader(stream))
                {
                    XDocument doc = XDocument.Load(readStream);

                    var ruleId = doc.Descendants("RuleId")
                                       .Select(el => el.Value)
                                       .ToList()[0];

                    return DataManager.GetInstance.SetData.DeleteAlarmRule(Int32.Parse(ruleId));

                }

            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return CommonUtil.ResponseData(ResponseType.Failed, ex.Message);
            }
        }
        #endregion

        #endregion //DeleteAlarmRule

        #endregion // AlarmRule

        #region Map
        public string SetMapXamlDelete(string MxlId, string projectId, string userId, string type)
        {
            string ret = string.Empty;

            try
            {
                //using (var readStream = new StreamReader(stream))
                //{
                //XmlDocument doc = new XmlDocument();
                //doc.Load(stream);

                //foreach (XmlNode node in doc.SelectSingleNode("Document").ChildNodes)
                //{
                //    ret = DataManager.GetInstance.SetData.SetMapXaml(
                //                                    node.Attributes["MxlId"].Value,
                //                                    node.Attributes["MxlNm"].Value,
                //                                    node.Attributes["MxlData"].Value,
                //                                    userId
                //                                    );

                //    if (!string.IsNullOrWhiteSpace(ret))
                //    {
                //        return ret;
                //    }
                //}

                ret = DataManager.GetInstance.SetData.SetMapXamlDelete(MxlId);

                if (!string.IsNullOrWhiteSpace(ret))
                {
                    return ret;
                }

                return ret;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public Stream GetMapXaml(string projectId, string userId, string type)
        {
            using (var consolePrinter = new DataServiceMessage(projectId, userId, type))
            {
                DataSet ds = null;

                try
                {
                    // 저장 위치에 있는 xaml 문자열을 가져와서 Stream으로 return.
                    //if (WebOperationContext.Current == null)
                    //{
                    //    throw new Exception("Can not get current WebOpreationContext.");
                    //}

                    //// Content는 무조건 xml임.
                    //WebOperationContext.Current.OutgoingResponse.ContentType = "application/xml";

                    ds = DataManager.GetInstance.GetDataForClient.GetMapXamlList();

                    XmlDocument doc = MapXamlToXml(ds);

                    var bytes = Encoding.UTF8.GetBytes(doc.OuterXml);

                    return new MemoryStream(bytes);
                }
                catch (Exception ex)
                {
                    // 뭔가 잘못되면 다음과 같은 Element에 들어감.
                    string str = string.Format("<Error>{0}</Error>", ex.Message);
                    consolePrinter.PrintExceptionMessage(ex);
                    consolePrinter.PrintMessage(str);

                    var bytes = Encoding.UTF8.GetBytes(str);

                    return new MemoryStream(bytes);
                }
            }

        }

        private static XmlDocument MapXamlToXml(DataSet ds)
        {
            var xmlDocument = new XmlDocument();

            var xmlDeclaration = xmlDocument.CreateXmlDeclaration("1.0", "utf-8", null);
            xmlDocument.AppendChild(xmlDeclaration);

            var root = xmlDocument.CreateElement("Document");
            xmlDocument.AppendChild(root);

            if (ds.Tables[0].Rows.Count < 1)
                return null;

            for (int i = 0; i < 0; i++)
            {
                DataRow row = ds.Tables[0].Rows[i];

                var eData = xmlDocument.CreateElement("Data");

                root.AppendChild(eData);

                var attMxlId = xmlDocument.CreateAttribute("MxlId");
                var attMxlNm = xmlDocument.CreateAttribute("MxlNm");
                var attMxlData = xmlDocument.CreateAttribute("MxlData");

                attMxlId.Value = row["MxlId"].ToString();
                attMxlNm.Value = row["MxlNm"].ToString();
                attMxlData.Value = row["MxlData"].ToString();

                eData.Attributes.Append(attMxlId);
                eData.Attributes.Append(attMxlNm);
                eData.Attributes.Append(attMxlData);
            }

            return xmlDocument;
            
        }

        #endregion
    }
}