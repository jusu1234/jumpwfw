﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SearchRecordTimeRequestData.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Defines the SearchRecordTimeRequestData type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace InnowatchService.SearchRecordTime
{
    using System.Xml.Serialization;

    /// <summary>
    /// The search record time request data.
    /// </summary>
    public class SearchRecordTimeRequestData
    {
        /// <summary>
        /// Gets or sets MergeIntervalSeconds.
        /// </summary>
        [XmlElement("MergeIntervalSeconds")]
        public string MergeIntervalSeconds { get; set; }

        /// <summary>
        /// Gets or sets the search record camera info list.
        /// </summary>
        [XmlElement("RecordCameraInfoList")]
        public SearchRecordCameraInfoList SearchRecordCameraInfoList { get; set; }

        ///// <summary>
        ///// Gets or sets CameraIDList.
        ///// </summary>
        //[XmlArray("RecordCameraNumberList")]
        //[XmlArrayItem("RecordCameraNumber", typeof(string))]
        //public List<string> RecordCameraNumberList
        //{
        //    get
        //    {
        //        return this.recordCameraNumberList;
        //    }

        //    set
        //    {
        //        this.recordCameraNumberList = value;
        //    }
        //}
    }
}