﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SearchRecordCameraInfoList.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Defines the SearchRecordCameraInfoList type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace InnowatchService.SearchRecordTime
{
    using System.Collections.Generic;
    using System.Xml.Serialization;

    using InnowatchService.TimelineInfo;

    /// <summary>
    /// The search record camera info list.
    /// </summary>
    public class SearchRecordCameraInfoList
    {
        private List<RecordCameraInfo> recordCameraInfos = new List<RecordCameraInfo>();

        /// <summary>
        /// Gets or sets the record camera info.
        /// </summary>
        [XmlElement("RecordCameraInfo")]
        public List<RecordCameraInfo> RecordCameraInfo
        {
            get
            {
                return this.recordCameraInfos;
            }

            set
            {
                this.recordCameraInfos = value;
            }
        }
    }
}