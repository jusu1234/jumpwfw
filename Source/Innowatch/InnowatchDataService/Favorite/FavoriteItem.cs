﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FavoriteData.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Defines the FavoriteData type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace InnowatchService.Favorite
{
    /// <summary>
    /// iCommand에서 넘어온 Favorite에 관련된 데이타 관리.
    /// </summary>
    public class FavoriteItem
    {
        /// <summary>
        /// Gets or sets ParentID.
        /// 즐겨찾기의 부모 아이디.
        /// </summary>
        public int ParentID { get; set; }

        /// <summary>
        /// Gets or sets CurrentID.
        /// 즐겨찾기 아이디.
        /// </summary>
        public int CurrentID { get; set; }

        /// <summary>
        /// Gets or sets CurrentName.
        /// 즐겨찾기 이름.
        /// </summary>
        public string CurrentName { get; set; }

        /// <summary>
        /// Gets or sets ItemOrder.
        /// 그룹 내 즐겨찾기 순서.
        /// </summary>
        public int ItemOrder { get; set; }

        /// <summary>
        /// Gets or sets IsFolder.
        /// 그룹이면 true,  아이템이면 false.
        /// </summary>
        public bool IsFolder { get; set; }

        /// <summary>
        /// Gets or sets Data.
        /// Favorite 실제 Data.
        /// </summary>
        public string Data { get; set; }
    }
}