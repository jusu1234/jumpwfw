﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FavoriteManager.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Defines the FavoriteDataList type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace InnowatchService.Favorite
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;

    using InnowatchDataHandler;

    using InnowatchServiceLog;

    /// <summary>
    /// FavoriteData 목록 관리.
    /// </summary>
    public class FavoriteManager : List<FavoriteItem>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FavoriteManager"/> class.
        /// </summary>
        /// <param name="stream">
        /// The stream.
        /// </param>
        public FavoriteManager(Stream stream)
        {
            try
            {
                using (var readStream = new StreamReader(stream))
                {
                    var dataTable = new DataTable();
                    dataTable.ReadXml(readStream);

                    foreach (DataRow row in dataTable.Rows)
                    {
                        var favorite = new FavoriteItem
                            {
                                ParentID = Convert.ToInt32(row["ParentID"].ToString().Trim()),
                                CurrentID = Convert.ToInt32(row["CurrentID"].ToString().Trim()),
                                CurrentName = row["CurrentName"].ToString().Trim(),
                                ItemOrder = Convert.ToInt32(row["ItemOrder"].ToString().Trim()),
                                IsFolder = Convert.ToBoolean(row["IsFolder"].ToString().Trim()),
                                Data = row["Data"].ToString().Trim(),
                            };

                        this.Add(favorite);
                    }
                }

            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
            }
        }

        public List<FavoriteItem> GetFavoriteItem(FavoriteManager manager, bool isGroup)
        {
            var favorites = manager.Where(item => item.IsFolder == isGroup).ToList();
            return favorites;
        }
        
        /// <summary>
        /// 그룹 아이디를 충돌이 나지 않는 아이디로 교체한다.
        /// </summary>
        /// <param name="ids"></param>
        public void ChangeGroupID(List<int> ids)
        {
            foreach (var favoriteItem in this)
            {
                if (!favoriteItem.IsFolder)
                {
                    continue;
                }

                if (ids.Contains(favoriteItem.CurrentID))
                {
                    var id = GetNewFavoriteId(ids, this.Select(item => item.CurrentID).ToList());
                    Debug.Assert(id != null, "id != null");

                    var oldId = favoriteItem.CurrentID;
                    favoriteItem.CurrentID = (int)id;
                    foreach (var item in this.Where(item => item.ParentID == oldId))
                    {
                        item.ParentID = (int)id;
                    }
                }
            }
        }

        /// <summary>
        /// 그룹이 아닌 즐겨찾기 아이디 중 삭제할 목록을 반환한다.
        /// </summary>
        /// <param name="projectId">
        /// The project Id.
        /// </param>
        /// <param name="userId">
        /// The user Id.
        /// </param>
        /// <param name="type">
        /// The type.
        /// </param>
        /// <returns>
        /// 즐겨찾기 아이디 목록.
        /// </returns>
        public List<int> GetDeleteFavoriteIDList(string projectId, string userId, string type)
        {
            var idsFromDB = DataManager.GetInstance.GetDataForClient.GetFavoriteItemIDs(projectId, userId, type);
            var myIds = (from item in this where !item.IsFolder select Convert.ToInt32(item.CurrentID)).ToList();

            return GetDeletingList(idsFromDB, myIds);
        }

        /// <summary>
        /// 현재 목록에 없는 아이디중 가장 낮은 숫자를 반환한다.
        /// </summary>
        /// <param name="id1"></param>
        /// <param name="id2"> </param>
        /// <returns></returns>
        private static int? GetNewFavoriteId(List<int> id1, List<int> id2)
        {
            if (id1 == null)
            {
                id1 = new List<int>();
            }

            if (id2 == null)
            {
                id2 = new List<int>();
            }

            for (var i = 0; i < Int16.MaxValue; i++)
            {
                if (id1.Contains(i))
                {
                    continue;
                }

                if (id2.Contains(i))
                {
                    continue;
                }

                return i;
            }

            return null;
        }
        
        /// <summary>
        /// oldList에는 존재하지만 newList에는 존재하지 않는 id를 추출한다.
        /// </summary>
        /// <param name="oldList">
        /// DB에 저장되있는 아이디 목록.
        /// </param>
        /// <param name="newList">
        /// 삭제되지 않은 아이디 목록.
        /// </param>
        /// <returns>
        /// DB에서 삭제할 아이디 목록을 반환한다.
        /// </returns>
        private static List<int> GetDeletingList(List<int> oldList, List<int> newList)
        {
            var result = oldList.ToList();

            foreach (var oldID in oldList)
            {
                foreach (var newID in newList)
                {
                    if (oldID == newID)
                    {
                        result.Remove(newID);
                    }
                }
            }

            return result;
        }


        
    }
}