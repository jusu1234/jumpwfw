﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CreateToPlaybackVideoInformationData.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Defines the CreateToPlaybackVideoInformationData type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace InnowatchService.PlaybackVideoInformationInfo
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Xml;
    using System.Xml.Serialization;

    public enum MediaInfo
    {
        /// <summary>
        /// The media100.
        /// </summary>
        Media100,

        /// <summary>
        /// The media200.
        /// </summary>
        Media200,

        /// <summary>
        /// The none.
        /// </summary>
        None
    }

    /// <summary>
    /// The create to playback video information data.
    /// </summary>
    public class CreateToPlaybackVideoInformationData
    {
        private readonly DataSet cameraDataSet;
        private readonly DataSet resoultionDataSet;
        private readonly DataSet mediaDataSet;

        private VideoInformation videoInfomation;

        /// <summary>
        /// Initializes a new instance of the <see cref="CreateToPlaybackVideoInformationData"/> class.
        /// </summary>
        /// <param name="cameraInfo">
        /// The camera Info.
        /// </param>
        /// <param name="resolutionInfo">
        /// The resolution Info.
        /// </param>
        /// <param name="mediaInfo">
        /// The media Info.
        /// </param>
        public CreateToPlaybackVideoInformationData(string cameraInfo, string resolutionInfo, string mediaInfo)
        {
            this.videoInfomation = new VideoInformation();

            this.cameraDataSet = new DataSet();
            this.resoultionDataSet = new DataSet();
            this.mediaDataSet = new DataSet();

            using (var readStream = new StringReader(cameraInfo))
            {
                this.cameraDataSet.ReadXml(readStream);
            }

            using (var readStream = new StringReader(resolutionInfo))
            {
                this.resoultionDataSet.ReadXml(readStream);
            }

            using (var readStream = new StringReader(mediaInfo))
            {
                this.mediaDataSet.ReadXml(readStream);
            }
        }

        /// <summary>
        /// The create playback video information.
        /// </summary>
        /// <returns>
        /// The playback video information.
        /// </returns>
        public string CreatePlaybackVideoInformation()
        {
            this.CreateMediaInfo(this.mediaDataSet);

            this.CreateVideoInformation(this.cameraDataSet, this.resoultionDataSet);

            return this.CreateToXML(this.videoInfomation);
        }

        private void CreateVideoInformation(DataSet cameraInfo, DataSet resoultionInfo)
        {
            foreach (DataRow camera in cameraInfo.Tables[0].Rows)
            {
                var mediaServerType = this.GetMediaServerType(this.videoInfomation, camera);

                if (mediaServerType.Equals(MediaInfo.None) || mediaServerType.Equals(MediaInfo.Media100))
                {
                    var setResolutionInfo = this.SetResolutionInfo(camera["ProjectCameraID"].ToString(), resoultionInfo, mediaServerType);
                    var setCameraInfo = this.SetCameraInfo(camera, mediaServerType);

                    setCameraInfo.ResolutionInfos.AddRange(setResolutionInfo);
                    this.videoInfomation.CameraInfos.Add(setCameraInfo);
                }

                if (mediaServerType.Equals(MediaInfo.Media200))
                {
                    var setResolutionInfo = this.SetResolutionInfo(camera["ProjectCameraID"].ToString(), resoultionInfo, mediaServerType);
                    var setCameraInfo = this.SetCameraInfo(camera, mediaServerType);

                    setCameraInfo.ResolutionInfos.AddRange(setResolutionInfo);
                    this.videoInfomation.CameraInfos.Add(setCameraInfo);
                }
            }
        }

        private IEnumerable<ResolutionInfo> SetResolutionInfo(string projectCameraId, DataSet resoultionInfoDataSet, MediaInfo mediaType)
        {
            var resolutionInfoList = new List<ResolutionInfo>();

            if (!mediaType.Equals(MediaInfo.Media200))
            {
                resolutionInfoList.AddRange(resoultionInfoDataSet.Tables[0].Rows.Cast<DataRow>().Where(row => row["ProjectCameraID"].ToString().Equals(projectCameraId)).Select(row => new ResolutionInfo { Width = row["Width"].ToString(), Height = row["Height"].ToString(), Url = row["RtspUrl"].ToString() }));

                return resolutionInfoList;
            }

            for (var i = 0; i < 2; i++)
            {
                if (i >= resoultionInfoDataSet.Tables[0].Rows.Count)
                {
                    continue;
                }

                resolutionInfoList.AddRange(from DataRow row in resoultionInfoDataSet.Tables[0].Rows where row["ProjectCameraID"].ToString().Equals(projectCameraId) select new ResolutionInfo { Width = row["Width"].ToString(), Height = row["Height"].ToString(), Url = row["RtspUrl"].ToString() });
            }

            return resolutionInfoList;
        }

        private CameraInfo SetCameraInfo(DataRow camera, MediaInfo mediaInfoType)
        {
            CameraInfo cameraInfo = null;

            if (!mediaInfoType.Equals(MediaInfo.Media200))
            {
                cameraInfo = new CameraInfo
                    {
                        CameraId = camera["ProjectCameraID"].ToString(),
                        CameraName = camera["ProjectCameraName"].ToString(),
                        ConnectId = camera["RtspID"].ToString(),
                        ConnectPassword = camera["RtspPassword"].ToString(),
                        ControlType = camera["ControlType"].ToString(),
                        ConnectMode = camera["ConnectMode"].ToString(),
                        ImageClippingArea = camera["ImageClippingArea"].ToString(),
                        MediaServerId = camera["MediaServerID"].ToString(),
                        MediaServerIp = camera["MediaServerIP"].ToString(),
                        RecordCameraNumber = camera["RecordCameraNumber"].ToString(),
                        UseVoice = camera["UseVoice"].ToString(),
                        RecorderIp = camera["RecorderIP"].ToString(),
                        IMSPServerPort = camera["IMSPServerPort"].ToString()
                    };

                return cameraInfo;
            }

            cameraInfo = this.SetMedia200CameraInfo(camera);

            return cameraInfo;
        }

        private CameraInfo SetMedia200CameraInfo(DataRow camera)
        {
            var parentMedia =
                this.mediaDataSet.Tables[0].Rows.Cast<DataRow>().First(
                    item => string.Compare(item["MediaServerID"].ToString(), camera["MediaServerID"].ToString()) == 0);

            var index = int.Parse(camera["MediaServerPosition"].ToString()) - 1;

            var cameraInfo = new CameraInfo
                {
                    CameraId = camera["ProjectCameraID"].ToString(),
                    CameraName = camera["ProjectCameraName"].ToString(),
                    ConnectId = camera["RtspID"].ToString(),
                    ConnectPassword = camera["RtspPassword"].ToString(),
                    ControlType = camera["ControlType"].ToString(),
                    ConnectMode = camera["ConnectMode"].ToString(),
                    ImageClippingArea = camera["ImageClippingArea"].ToString(),
                    MediaServerId = camera["MediaServerID"].ToString(),
                    MediaServerIp = camera["MediaServerIP"].ToString(),
                    RecordCameraNumber = camera["RecordCameraNumber"].ToString(),
                    UseVoice = camera["UseVoice"].ToString(),
                    RecorderIp = camera["RecorderIP"].ToString(),
                    IMSPServerPort = camera["IMSPServerPort"].ToString(),
                    Row = (index / Convert.ToInt32(parentMedia["VideoBoardColumn"].ToString())).ToString(),
                    Column = (index % Convert.ToInt32(parentMedia["VideoBoardColumn"].ToString())).ToString()
                };

            return cameraInfo;
        }

        private MediaInfo GetMediaServerType(VideoInformation videoInformation, DataRow cameraInfo)
        {
            if (videoInformation != null)
            {
                if (string.IsNullOrWhiteSpace(cameraInfo["MediaServerID"].ToString()))
                {
                    return MediaInfo.None;
                }

                foreach (var mediaInfo in videoInformation.MergeVideoServerInfos.Where(mediaInfo => string.Compare(mediaInfo.MediaServerId, cameraInfo["MediaServerID"].ToString()) == 0))
                {
                    if ((string.Compare(mediaInfo.Width, "0") != 0) && string.Compare(mediaInfo.Height, "0") != 0)
                    {
                        return MediaInfo.Media200;
                    }

                    return MediaInfo.Media100;
                }
            }

            return MediaInfo.None;
        }

        private void CreateMediaInfo(DataSet mediaInfo)
        {
            foreach (var mergeInfo in from DataRow dr in mediaInfo.Tables[0].Rows 
                                      select new MergeVideoServerInfo
                                      {
                                          MediaServerId = dr["MediaServerId"].ToString(),
                                          Width = dr["VideoBoardWidth"].ToString(),
                                          Height = dr["VideoBoardHeight"].ToString(),
                                          ColumnCount = dr["VideoBoardColumn"].ToString(),
                                          RowCount = dr["VideoBoardRow"].ToString(),
                                          ConnectMode = dr["ConnectMode"].ToString(),
                                          Url = string.Format("rtsp://{0}/all.sdp", dr["MediaServerIP"])
                                      })
            {
                this.videoInfomation.MergeVideoServerInfos.Add(mergeInfo);
            }
        }

        private string CreateToXML(VideoInformation target)
        {
            var serializer = new XmlSerializer(typeof(VideoInformation));
            var memStream = new MemoryStream();

            var settings = new XmlWriterSettings
            {
                Indent = true,
                IndentChars = new string(' ', 4),
                NewLineOnAttributes = false,
                Encoding = Encoding.UTF8
            };

            var xmlWriter = XmlWriter.Create(memStream, settings);
            serializer.Serialize(xmlWriter, target);
            xmlWriter.Close();
            memStream.Close();

            var xmlData = Encoding.UTF8.GetString(memStream.GetBuffer());
            xmlData = xmlData.Substring(xmlData.IndexOf('<'));
            xmlData = xmlData.Substring(0, xmlData.LastIndexOf('>') + 1);

            return xmlData;
        }
    }
}