﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="VideoInformation.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Defines the VideoInformation type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace InnowatchService.PlaybackVideoInformationInfo
{
    using System.Collections.Generic;
    using System.Xml.Serialization;

    /// <summary>
    /// The video information.
    /// </summary>
    public class VideoInformation
    {
        private List<CameraInfo> cameraResolutions = new List<CameraInfo>();

        private List<MergeVideoServerInfo> mergerInfos = new List<MergeVideoServerInfo>();

        /// <summary>
        /// Gets or sets CameraInfos.
        /// </summary>
        [XmlElement("CameraInfo")]
        public List<CameraInfo> CameraInfos
        {
            get { return this.cameraResolutions; }
            set { this.cameraResolutions = value; }
        }

        /// <summary>
        /// Gets or sets MergeVideoServerInfos.
        /// </summary>
        [XmlElement("MergeVideoServerInfo")]
        public List<MergeVideoServerInfo> MergeVideoServerInfos
        {
            get { return this.mergerInfos; }
            set { this.mergerInfos = value; }
        }
    }
}