﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CameraInfo.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Defines the CameraInfo type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace InnowatchService.PlaybackVideoInformationInfo
{
    using System.Collections.Generic;
    using System.Xml.Serialization;

    /// <summary>
    /// The camera info.
    /// </summary>
    [XmlRoot("CameraInfo")]
    public class CameraInfo
    {
        private List<ResolutionInfo> resolutionInfos = new List<ResolutionInfo>();

        /// <summary>
        /// Gets or sets CameraId.
        /// </summary>
        [XmlAttribute("CameraId")]
        public string CameraId { get; set; }

        /// <summary>
        /// Gets or sets CameraName.
        /// </summary>
        [XmlAttribute("CameraName")]
        public string CameraName { get; set; }

        /// <summary>
        /// Gets or sets ConnectId.
        /// </summary>
        [XmlAttribute("ConnectId")]
        public string ConnectId { get; set; }

        /// <summary>
        /// Gets or sets ConnectPassword.
        /// </summary>
        [XmlAttribute("ConnectPassword")]
        public string ConnectPassword { get; set; }

        /// <summary>
        /// Gets or sets ControlType.
        /// </summary>
        [XmlAttribute("ControlType")]
        public string ControlType { get; set; }

        /// <summary>
        /// Gets or sets MediaServerId.
        /// </summary>
        [XmlAttribute("MediaServerId")]
        public string MediaServerId { get; set; }

        /// <summary>
        /// Gets or sets Row.
        /// </summary>
        [XmlAttribute("Row")]
        public string Row { get; set; }

        /// <summary>
        /// Gets or sets Column.
        /// </summary>
        [XmlAttribute("Column")]
        public string Column { get; set; }

        /// <summary>
        /// Gets or sets ConnectMode.
        /// </summary>
        [XmlAttribute("ConnectMode")]
        public string ConnectMode { get; set; }

        /// <summary>
        /// Gets or sets ImageClippingArea.
        /// </summary>
        [XmlAttribute("ImageClippingArea")]
        public string ImageClippingArea { get; set; }

        /// <summary>
        /// Gets or sets MediaServerIp.
        /// </summary>
        [XmlAttribute("MediaServerIp")]
        public string MediaServerIp { get; set; }

        /// <summary>
        /// Gets or sets RecordCameraNumber.
        /// </summary>
        [XmlAttribute("RecordCameraNumber")]
        public string RecordCameraNumber { get; set; }

        /// <summary>
        /// Gets or sets UseVoice.
        /// </summary>
        [XmlAttribute("UseVoice")]
        public string UseVoice { get; set; }

        /// <summary>
        /// Gets or sets RecorderIp.
        /// </summary>
        [XmlAttribute("RecorderIp")]
        public string RecorderIp { get; set; }

        /// <summary>
        /// Gets or sets IMSPServerPort.
        /// </summary>
        [XmlAttribute("IMSPServerPort")]
        public string IMSPServerPort { get; set; }

        /// <summary>
        /// Gets or sets ResolutionInfos.
        /// </summary>
        [XmlElement("ResolutionInfo")]
        public List<ResolutionInfo> ResolutionInfos
        {
            get { return this.resolutionInfos; }
            set { this.resolutionInfos = value; }
        }
    }
}