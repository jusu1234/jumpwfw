﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ResolutionInfo.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Defines the ResolutionInfo type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace InnowatchService.PlaybackVideoInformationInfo
{
    using System.Xml.Serialization;

    /// <summary>
    /// The resolution info.
    /// </summary>
    [XmlRoot("ResolutionInfo")]
    public class ResolutionInfo
    {
        /// <summary>
        /// Gets or sets Width.
        /// </summary>
        [XmlAttribute("Width")]
        public string Width { get; set; }

        /// <summary>
        /// Gets or sets Height.
        /// </summary>
        [XmlAttribute("Height")]
        public string Height { get; set; }

        /// <summary>
        /// Gets or sets Url.
        /// </summary>
        [XmlAttribute("Url")]
        public string Url { get; set; }
    }
}