﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MergeVideoServerInfo.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Defines the MergeVideoServerInfo type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace InnowatchService.PlaybackVideoInformationInfo
{
    using System.Xml.Serialization;

    /// <summary>
    /// The merge video server info.
    /// </summary> 
    [XmlRoot]
    public class MergeVideoServerInfo
    {
        /// <summary>
        /// Gets or sets MediaServerId.
        /// </summary>
        [XmlAttribute("MediaServerId")]
        public string MediaServerId { get; set; }

        /// <summary>
        /// Gets or sets Width.
        /// </summary>
        [XmlAttribute("Width")]
        public string Width { get; set; }

        /// <summary>
        /// Gets or sets Height.
        /// </summary>
        [XmlAttribute("Height")]
        public string Height { get; set; }

        /// <summary>
        /// Gets or sets ColumnCount.
        /// </summary>
        [XmlAttribute("ColumnCount")]
        public string ColumnCount { get; set; }

        /// <summary>
        /// Gets or sets RowCount.
        /// </summary>
        [XmlAttribute("RowCount")]
        public string RowCount { get; set; }

        /// <summary>
        /// Gets or sets ConnectMode.
        /// </summary>
        [XmlAttribute("ConnectMode")]
        public string ConnectMode { get; set; }

        /// <summary>
        /// Gets or sets Url.
        /// </summary>
        [XmlAttribute("Url")]
        public string Url { get; set; }
    }
}