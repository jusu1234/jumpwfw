﻿

namespace InnowatchService.Commons
{
    #region

    using System.IO;
    using System.Text;
    using System.Xml;
    using System.Xml.Serialization;

    #endregion

    /// <summary>
    /// The ptz infomation.
    /// </summary>
    [XmlRoot("PTZInformation")]
    public class PTZInformation
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PTZInformation"/> class.
        /// </summary>
        public PTZInformation()
        {
            this.ContentsDefineId = string.Empty;
            this.DllName = string.Empty;
            this.Address = string.Empty;
            this.Channel = string.Empty;
            this.AudioPort = string.Empty;
            this.IpPort = string.Empty;

            this.Username = string.Empty;
            this.Password = string.Empty;
            this.BaudRate = string.Empty;
            this.DataBits = string.Empty;
            this.Parity = string.Empty;

            this.StopBits = string.Empty;
            this.Intervaltime = string.Empty;
            this.State = string.Empty;
            this.Actiontime = string.Empty;
            this.TargetPoint = string.Empty;

            this.GroupSelectData = string.Empty;

            this.UsePreset = true;
            this.PresetCount = 0;

            // Initialize Power.
            this.UseAux1Power = true;
            this.UseAux2Power = true;
            this.UseLightPower = true;
            this.UseCameraPower = true;

            // Initialize PTZ.
            this.UsePanTilt = true;
            this.UsePTZSpeed = true;
            this.UseZoom = true;

            // Initialize Focus.
            this.UseFocus = true;
            //this.UseAutoFocus = true;

            // Initialize Iris.
            this.UseIris = true;
            this.UseAutoIris = true;

            // Initialize Audio.
            this.AudioMode = 0;

        }

        /// <summary>
        /// Gets or sets ContentsDefineId.
        /// </summary>
        [XmlAttribute("CameraID")]
        public string ContentsDefineId { get; set; }

        /// <summary>
        /// Gets or sets DllName.
        /// </summary>
        [XmlAttribute("ModuleName")]
        public string DllName { get; set; }

        /// <summary>
        /// Gets or sets Address.
        /// </summary>
        [XmlAttribute("Address")]
        public string Address { get; set; }

        /// <summary>
        /// Gets or sets Channel.
        /// </summary>
        [XmlAttribute("Channel")]
        public string Channel { get; set; }

        /// <summary>
        /// Gets or sets AudioPort.
        /// </summary>
        [XmlAttribute("AudioPort")]
        public string AudioPort { get; set; }

        /// <summary>
        /// Gets or sets Username.
        /// </summary>
        [XmlAttribute("Username")]
        public string Username { get; set; }

        /// <summary>
        /// Gets or sets Password.
        /// </summary>
        [XmlAttribute("Password")]
        public string Password { get; set; }

        /// <summary>
        /// Gets or sets BaudRate.
        /// </summary>
        [XmlAttribute("BaudRate")]
        public string BaudRate { get; set; }

        /// <summary>
        /// Gets or sets DataBits.
        /// </summary>
        [XmlAttribute("DataBits")]
        public string DataBits { get; set; }

        /// <summary>
        /// Gets or sets Parity.
        /// </summary>
        [XmlAttribute("Parity")]
        public string Parity { get; set; }

        /// <summary>
        /// Gets or sets StopBits.
        /// </summary>
        [XmlAttribute("StopBits")]
        public string StopBits { get; set; }

        /// <summary>
        /// Gets or sets Intervaltime.
        /// </summary>
        [XmlAttribute("Intervaltime")]
        public string Intervaltime { get; set; }

        /// <summary>
        /// Gets or sets State.
        /// </summary>
        [XmlAttribute("State")]
        public string State { get; set; }

        /// <summary>
        /// Gets or sets Actiontime.
        /// </summary>
        [XmlAttribute("Actiontime")]
        public string Actiontime { get; set; }

        /// <summary>
        /// Gets or sets TargetPoint.
        /// </summary>
        [XmlAttribute("TargetPoint")]
        public string TargetPoint { get; set; }

        /// <summary>
        /// Gets or sets GroupSelectData.
        /// </summary>
        [XmlAttribute("GroupSelectData")]
        public string GroupSelectData { get; set; }

        /// <summary>
        /// Gets or sets IpPort. (Use Http Port or TCP Port).
        /// </summary>
        [XmlAttribute("IpPort")]
        public string IpPort { get; set; }

        /// <summary>
        /// Gets or sets SerialPort.
        /// </summary>
        [XmlAttribute("SerialPort")]
        public string SerialPort { get; set; }

        /// <summary>
        /// Gets or sets UsePreset.
        /// </summary>
        [XmlAttribute("UsePreset")]
        public bool UsePreset { get; set; }

        /// <summary>
        /// Gets or sets PresetCount.
        /// </summary>
        [XmlAttribute("PresetCount")]
        public int PresetCount { get; set; }

        /// <summary>
        /// Gets or sets UseFocus.
        /// </summary>
        [XmlAttribute("UseFocus")]
        public bool UseFocus { get; set; }

        ///// <summary>
        ///// Gets or sets UseAutoFocus.
        ///// </summary>
        //[XmlAttribute("UseAutoFocus")]
        //public bool UseAutoFocus { get; set; }

        /// <summary>
        /// Gets or sets UseAux1.
        /// </summary>
        [XmlAttribute("UseAux1Power")]
        public bool UseAux1Power { get; set; }

        /// <summary>
        /// Gets or sets UseAux2.
        /// </summary>
        [XmlAttribute("UseAux2Power")]
        public bool UseAux2Power { get; set; }

        /// <summary>
        /// Gets or sets UseCameraLight.
        /// </summary>
        [XmlAttribute("UseLightPower")]
        public bool UseLightPower { get; set; }

        /// <summary>
        /// Gets or sets UseCameraPower.
        /// </summary>
        [XmlAttribute("UseCameraPower")]
        public bool UseCameraPower { get; set; }

        /// <summary>
        /// Gets or sets UseIris.
        /// </summary>
        [XmlAttribute("UseIris")]
        public bool UseIris { get; set; }

        /// <summary>
        /// Gets or sets UseAutoIris.
        /// </summary>
        [XmlAttribute("UseAutoIris")]
        public bool UseAutoIris { get; set; }

        /// <summary>
        /// Gets or sets UseAudioMode.
        /// </summary>
        [XmlAttribute("AudioMode")]
        public int AudioMode { get; set; }

        /// <summary>
        /// Gets or sets UsePanTilt.
        /// </summary>
        [XmlAttribute("UsePanTilt")]
        public bool UsePanTilt { get; set; }

        /// <summary>
        /// Gets or sets UsePanTiltSpeed.
        /// </summary>
        [XmlAttribute("UsePTZSpeed")]
        public bool UsePTZSpeed { get; set; }

        /// <summary>
        /// Gets or sets UseZoom.
        /// </summary>
        [XmlAttribute("UseZoom")]
        public bool UseZoom { get; set; }

        public string SaveDataToXml()
        {
            var serializer = new XmlSerializer(typeof(PTZInformation));
            var memStream = new MemoryStream();

            var settings = new XmlWriterSettings();
            settings.Indent = true;
            settings.IndentChars = new string(' ', 4);
            settings.NewLineOnAttributes = false;
            settings.Encoding = Encoding.UTF8;

            XmlWriter xmlWriter = XmlWriter.Create(memStream, settings);
            serializer.Serialize(xmlWriter, this);
            xmlWriter.Close();
            memStream.Close();

            string xmlData = Encoding.UTF8.GetString(memStream.GetBuffer());
            xmlData = xmlData.Substring(xmlData.IndexOf('<'));
            xmlData = xmlData.Substring(0, xmlData.LastIndexOf('>') + 1);

            return xmlData;
        }

        public static PTZInformation ReadDataFromXml(string xmlData)
        {
            var serializer = new XmlSerializer(typeof(PTZInformation));

            var stringReader = new StringReader(xmlData);
            var xmlReader = new XmlTextReader(stringReader);

            var ptzInfomationData = serializer.Deserialize(xmlReader) as PTZInformation;

            xmlReader.Close();
            stringReader.Close();

            return ptzInfomationData;
        }
    }
}