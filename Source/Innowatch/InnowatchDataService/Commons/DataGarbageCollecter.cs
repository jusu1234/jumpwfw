﻿
namespace InnowatchDataService.Commons
{
    using System;
    using System.Collections;
    using System.Data;
    using System.Threading;
    using InnowatchDataHandler;
    using InnowatchServiceLog;

    public enum DeleteType
    {
        Camera, CommandFavorite, Location
    }

    public static class DataGarbageCollecter
    {
        private static readonly Thread GarbageCollecter;
        private static readonly Queue Queue = new Queue();

        static DataGarbageCollecter()
        {
            GarbageCollecter = new Thread(Collect);
            GarbageCollecter.Start();
        }

        /// <summary>
        /// The collect.
        /// </summary>
        private static void Collect()
        {
            while (true)
            {
                if (Queue.Count <= 0)
                {
                    continue;
                }

                var deQueue = Queue.Dequeue();

                var workType = (DeleteType) deQueue;
                Logger.Write(
                    Logger.Info, 
                    GetMethodInfoStrings.GetMethodName(), 
                    string.Format("[DataGarbageCollector] Collect Garbage Type : {0}", workType));

                switch (workType)
                {
                    case DeleteType.Camera:
                        {
                            DeleteCameraGarbageData();
                            break;
                        }
                    case DeleteType.CommandFavorite:
                        {
                            DeleteCommandFavoriteGarbageData();
                            break;
                        }
                    case DeleteType.Location:
                        {
                            DeleteLocationGarbageData();
                            break;
                        }
                }
            }
        }

        /// <summary>
        /// The delete camera garbage data.
        /// </summary>
        private static void DeleteCameraGarbageData()
        {
            DeleteCameraGroup();
            DeleteCameraGroupMapping();
        }

        /// <summary>
        /// The delete favorite garbage data.
        /// </summary>
        private static void DeleteCommandFavoriteGarbageData()
        {
            DeleteCommandFavoriteGroup();
            DeleteCommandFavoriteGroupMapping();
        }

        /// <summary>
        /// The delete camera group mapping.
        /// </summary>
        private static void DeleteCameraGroupMapping()
        {
            try
            {
                var dataSet = DataManager.GetInstance.SetData.SelectLostChildeCameraGroupMapping();

                foreach (DataRow row in dataSet.Tables[0].Rows)
                {
                    DataManager.GetInstance.SetData.DeleteCameraGroupMapping(Convert.ToInt32(row["CameraGroupID"].ToString()), row["CameraID"].ToString());
                }

                dataSet = DataManager.GetInstance.GetDataForClient.SelectLostChildCameraGroup();

                if (dataSet != null)
                {
                    if (dataSet.Tables[0].Rows.Count > 0)
                    {
                        Logger.Write(Logger.Info, GetMethodInfoStrings.GetMethodName(), "CameraGroupMapping Garbage Enqueue.");
                        Queue.Enqueue(DeleteType.Camera);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, GetMethodInfoStrings.GetMethodName(), ex.Message);
            }
        }

        /// <summary>
        /// The delete camera group.
        /// </summary>
        private static void DeleteCameraGroup()
        {
            try
            {
                var dataSet = DataManager.GetInstance.GetDataForClient.SelectLostChildCameraGroup();

                foreach (DataRow row in dataSet.Tables[0].Rows)
                {
                    DataManager.GetInstance.SetData.DeleteCameraGroup(Convert.ToInt32(row["CameraGroupID"].ToString()));
                }

                dataSet = DataManager.GetInstance.GetDataForClient.SelectLostChildCameraGroup();

                if (dataSet != null)
                {
                    if (dataSet.Tables[0].Rows.Count > 0)
                    {
                        Logger.Write(Logger.Info, GetMethodInfoStrings.GetMethodName(), "CameraGroup Garbage Enqueue.");
                        Queue.Enqueue(DeleteType.Camera);    
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, GetMethodInfoStrings.GetMethodName(), ex.Message);
            }
        }
        
        /// <summary>
        /// The delete favorite group.
        /// </summary>
        private static void DeleteCommandFavoriteGroup()
        {
            try
            {
                var dataSet = DataManager.GetInstance.SetData.SelectLostChildFavoriteGroup();

                foreach (DataRow row in dataSet.Tables[0].Rows)
                {
                    DataManager.GetInstance.SetData.DeleteFavoriteGroup(Convert.ToInt32(row["FavoriteGRoupID"].ToString()), row["ProjectID"].ToString(), row["UserID"].ToString(), "command");
                }

                dataSet = DataManager.GetInstance.SetData.SelectLostChildFavoriteGroup();

                if (dataSet != null)
                {
                    if (dataSet.Tables[0].Rows.Count > 0)
                    {
                        Logger.Write(Logger.Info, GetMethodInfoStrings.GetMethodName(), "FavoriteGroup Garbage Enqueue.");
                        Queue.Enqueue(DeleteType.CommandFavorite);    
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, GetMethodInfoStrings.GetMethodName(), ex.Message);
            }
        }

        /// <summary>
        /// The delete favorite group mapping.
        /// </summary>
        private static void DeleteCommandFavoriteGroupMapping()
        {
            try
            {
                var dataSet = DataManager.GetInstance.SetData.SelectLostChildFavoriteGroupMapping();

                foreach (DataRow row in dataSet.Tables[0].Rows)
                {
                    DataManager.GetInstance.SetData.DeleteFavoriteGroupMappingByFavoriteGroupIdAndFavoriteId(Convert.ToInt32(row["FavoriteGroupID"].ToString()), Convert.ToInt32(row["FavoriteID"].ToString()));
                    DataManager.GetInstance.SetData.DeleteFavorite(Convert.ToInt32(row["FavoriteID"].ToString()), "command");
                }

                dataSet = DataManager.GetInstance.SetData.SelectLostChildFavoriteGroupMapping();

                if (dataSet != null)
                {
                    if(dataSet.Tables[0].Rows.Count > 0)
                    {
                        Logger.Write(Logger.Info, GetMethodInfoStrings.GetMethodName(), "FavoriteGroupMapping Garbage Enqueue.");
                        Queue.Enqueue(DeleteType.CommandFavorite);    
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, GetMethodInfoStrings.GetMethodName(), ex.Message);
            }
        }
        
        /// <summary>
        /// The delete location garbage data.
        /// </summary>
        private static void DeleteLocationGarbageData()
        {
            
        }

        /// <summary>
        /// The invoke.
        /// </summary>
        /// <param name="type">The delete type.</param>
        public static void Invoke(DeleteType type)
        {
            try
            {
                Logger.Write(Logger.Info, GetMethodInfoStrings.GetMethodName(), string.Format("{0} Invoke.", type));
                Queue.Enqueue(type);
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, GetMethodInfoStrings.GetMethodName(), ex.Message);
            }
        }
    }
}