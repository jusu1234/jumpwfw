﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IsCheckedForNullableInt.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Defines the IsCheckedForNullableInt type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace InnowatchService.Commons
{
    /// <summary>
    /// The is checked for nullable int.
    /// </summary>
    public static class IsCheckedForNullableInt
    {
        /// <summary>
        /// The is checked data.
        /// </summary>
        /// <param name="row">
        /// The row.
        /// </param>
        /// <returns>
        /// The nullable int dat.
        /// </returns>
        public static int? IsCheckedData(string row)
        {
            return string.IsNullOrEmpty(row) ? (int?)null : int.Parse(row);
        }
    }
}