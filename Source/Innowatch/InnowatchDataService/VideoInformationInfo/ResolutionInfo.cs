﻿// -----------------------------------------------------------------------
// <copyright file="ResolutionInfo.cs" company="Microsoft">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace InnowatchConsoleService.Commons
{
    using System.Xml.Serialization;

    [XmlRoot("ResolutionInfo")]
    public class ResolutionInfo
    {
        [XmlAttribute("Width")]
        public string Width { get; set; }

        [XmlAttribute("Height")]
        public string Height { get; set; }

        [XmlAttribute("Fps")]
        public string Fps { get; set; }

        [XmlAttribute("Url")]
        public string Url { get; set; }

        [XmlAttribute("IsPOD")]
        public bool IsPOD { get; set; }

        [XmlAttribute("Codec")]
        public string Codec { get; set; }

        [XmlAttribute("Bitrate")]
        public string Bitrate { get; set; }

        [XmlAttribute("ExtraParams")]
        public string ExtraParams { get; set; }
    }
}
