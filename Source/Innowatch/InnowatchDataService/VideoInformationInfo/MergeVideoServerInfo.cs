﻿

namespace InnowatchConsoleService.Commons
{
    using System.Xml.Serialization;

    [XmlRoot]
    public class MediaServerInfo
    {
        [XmlAttribute("MediaServerId")]
        public string MediaServerId { get; set; }

        [XmlAttribute("MediaServerIp")]
        public string MediaServerIp { get; set; }

        [XmlAttribute("Width")]
        public string Width { get; set; }

        [XmlAttribute("Height")]
        public string Height { get; set; }

        [XmlAttribute("ColumnCount")]
        public string ColumnCount { get; set; }

        [XmlAttribute("RowCount")]
        public string RowCount { get; set; }

        [XmlAttribute("ConnectMode")]
        public string ConnectMode { get; set; }

        [XmlAttribute("Url")]
        public string Url { get; set; }
    }
}
