﻿// -----------------------------------------------------------------------
// <copyright file="VideoInformation.cs" company="Microsoft">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

using InnowatchDataService.Commons;

namespace InnowatchConsoleService.Commons
{
    using System.Collections.Generic;
    using System.Xml.Serialization;

    public class VideoInformation
    {
        private List<CameraInfo> cameraResolutions = new List<CameraInfo>();

        private List<MediaServerInfo> mergerInfos = new List<MediaServerInfo>();

        [XmlElement("CameraInfo")]
        public List<CameraInfo> CameraInfos
        {
            get { return this.cameraResolutions; }
            set { this.cameraResolutions = value; }
        }

        [XmlElement("MediaServerInfo")]
        public List<MediaServerInfo> MediaServerInfos
        {
            get { return this.mergerInfos; }
            set { this.mergerInfos = value; }
        }
    }
}
