﻿// -----------------------------------------------------------------------
// <copyright file="CameraInfo.cs" company="Microsoft">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace InnowatchConsoleService.Commons
{
    using System.Xml.Serialization;
    using System.Collections.Generic;

    [XmlRoot("CameraInfo")]
    public class CameraInfo
    {
        private List<ResolutionInfo> resolutionInfos = new List<ResolutionInfo>();

        [XmlAttribute("CameraId")]
        public string CameraId { get; set; }

        [XmlAttribute("CameraName")]
        public string CameraName { get; set; }

        [XmlAttribute("ConnectId")]
        public string ConnectId { get; set; }

        [XmlAttribute("ConnectPassword")]
        public string ConnectPassword { get; set; }

        [XmlAttribute("ControlType")]
        public string ControlType { get; set; }

        [XmlAttribute("MediaServerId")]
        public string MediaServerId { get; set; }

        [XmlAttribute("MediaServerExtraParams")]
        public string MediaServerExtraParams { get; set; }

        [XmlAttribute("Row")]
        public string Row { get; set; }

        [XmlAttribute("Column")]
        public string Column { get; set; }

        /// <summary>
        /// Gets or sets RdsHost.
        /// </summary>
        [XmlAttribute("RdsHost")]
        public string RdsHost { get; set; }

        /// <summary>
        /// Gets or sets RdsPort.
        /// </summary>
        [XmlAttribute("RdsPort")]
        public int RdsPort { get; set; }

        [XmlAttribute("ConnectMode")]
        public string ConnectMode { get; set; }

        [XmlAttribute("ImageClippingArea")]
        public string ImageClippingArea { get; set; }

        [XmlAttribute("ResizingCodec")]
        public string ResizingCodec { get; set; }

        [XmlAttribute("ShowAudioLevel")]
        public bool ShowAudioLevel { get; set; }

        //[XmlAttribute("MediaServerIp")]
        //public string MediaServerIp { get; set; }

        [XmlElement("ResolutionInfo")]
        public List<ResolutionInfo> ResolutionInfos
        {
            get { return this.resolutionInfos; }
            set { this.resolutionInfos = value; }
        }
    }
}
