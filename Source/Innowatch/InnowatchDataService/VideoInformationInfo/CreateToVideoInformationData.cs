﻿
namespace InnowatchService.VideoInformationInfo
{
    using System;
    using System.Data;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Globalization;
    using System.Text;
    using System.Xml;
    using System.Xml.Serialization;

    using InnowatchConsoleService.Commons;

    /// <summary>
    /// 비디오 정보 데이터를 생성한다.
    /// </summary>
    public class CreateToVideoInformationData
    {
        /// <summary>
        /// VideoInformation을 생성한 후 Serialize 하여 반환한다.
        /// </summary>
        /// <param name="cameraDataSet">등록된 Camera DataSet.</param>
        /// <param name="mdDataSet">등록된 MediaServer DataSet.</param>
        /// <returns>The Videoinformation Serialized xml.</returns>
        public string CreateVideoInformation(DataSet cameraDataSet, DataSet mdDataSet)
        {
            var videoInfo = new VideoInformation();

            // MediaServer(MD200 포함) 정보 설정.
            this.SetMediaServerInfo(mdDataSet, videoInfo);

            // Camera 정보 설정.
            this.SetCameraInfo(cameraDataSet, videoInfo);
            var serialized = CreateToXML(videoInfo);

            videoInfo = null;

            return serialized;
        }

        private void SetCameraInfo(DataSet cameraDataSet, VideoInformation videoInfo)
        {
            // Camera 정보 설정.
            foreach (DataRow dr in cameraDataSet.Tables[0].Rows)
            {
                var rtspUrl = dr["RtspUrl"].ToString();
                var resolution = new ResolutionInfo
                {
                    Width = dr["Width"].ToString(),
                    Height = dr["Height"].ToString(),
                    Fps = dr["FPS"].ToString(),                    
                    Url = rtspUrl,
                    IsPOD = string.IsNullOrWhiteSpace(dr["IsPOD"].ToString()) ? false : bool.Parse(dr["IsPOD"].ToString()),
                    Codec = dr["Codec"].ToString(),
                    Bitrate = dr["Bitrate"].ToString(),
                    ExtraParams = dr["ExtraParams"].ToString(),
                };
                string tempUrl = string.Empty;
                if (videoInfo.CameraInfos.Any(cam => string.CompareOrdinal(cam.CameraId, dr["ProjectCameraID"].ToString()) == 0))
                {
                    var existCameraInfo = videoInfo.CameraInfos.First(
                        cam => string.CompareOrdinal(cam.CameraId, dr["ProjectCameraID"].ToString()) == 0);
                    tempUrl = resolution.Url.Replace("{IP}", dr["CameraIP"].ToString());
                    tempUrl = tempUrl.Replace("{ip}", dr["CameraIP"].ToString());
                    tempUrl = tempUrl.Replace("{ch}", dr["Channel"].ToString());
                    resolution.Url = tempUrl.Replace("{CH}", dr["Channel"].ToString());
                    existCameraInfo.ResolutionInfos.Add(resolution);
                    continue;
                }

                var cameraInfo = new CameraInfo
                {
                    CameraId = dr["ProjectCameraID"].ToString(),
                    CameraName = dr["ProjectCameraName"].ToString(),
                    ConnectId = dr["RtspID"].ToString(),
                    ConnectPassword = dr["RtspPassword"].ToString(),
                    ControlType = dr["ControlType"].ToString(),
                    ConnectMode = dr["ConnectMode"].ToString(),
                    ImageClippingArea = dr["ImageClippingArea"].ToString(),
                    MediaServerId = dr["MediaServerID"].ToString(),
                    RdsHost = dr["RdsHost"].ToString(),
                    ResizingCodec = dr["ResizingCodec"].ToString(),
                    RdsPort = string.IsNullOrWhiteSpace(dr["RdsPort"].ToString())
                                ? 0
                                : Convert.ToInt32(dr["RdsPort"].ToString()),
                    ShowAudioLevel = string.IsNullOrWhiteSpace(dr["ShowAudioLevel"].ToString()) ? false : bool.Parse(dr["ShowAudioLevel"].ToString()),
                    MediaServerExtraParams = dr["MediaServerExtraParams"].ToString()
                };

                // MD200 설정. (MD200 ByPass Url, 저화질 Row / Column)
                var mediaServer =
                    videoInfo.MediaServerInfos.FirstOrDefault(
                        md =>
                        string.Compare(cameraInfo.MediaServerId, md.MediaServerId, StringComparison.OrdinalIgnoreCase) == 0);

                if (mediaServer != null)
                {
                    var isMD200 = !(string.Compare(mediaServer.Width, string.Empty, StringComparison.OrdinalIgnoreCase) == 0 ||
                                    string.Compare(mediaServer.Width, "0", StringComparison.OrdinalIgnoreCase) == 0 ||
                                    string.Compare(mediaServer.Height, string.Empty, StringComparison.OrdinalIgnoreCase) == 0 ||
                                    string.Compare(mediaServer.Height, "0", StringComparison.OrdinalIgnoreCase) == 0);

                    if (isMD200)
                    {
                        // 떡판용 Row, Column 설정.
                        var index = dr["MediaServerPosition"].ToString();

                        if (!string.IsNullOrWhiteSpace(index))
                        {
                            rtspUrl = string.Format("rtsp://{0}/{1}.sdp?resolution=high", dr["MediaServerIP"], index);

                            var indexToInt = Convert.ToInt32(index) - 1;

                            cameraInfo.Row =
                                (indexToInt / Convert.ToInt32(mediaServer.ColumnCount)).ToString(CultureInfo.InvariantCulture);
                            cameraInfo.Column =
                                (indexToInt % Convert.ToInt32(mediaServer.ColumnCount)).ToString(CultureInfo.InvariantCulture);
                        }
                    }
                }

                tempUrl = rtspUrl.Replace("{IP}", dr["CameraIP"].ToString());
                tempUrl = tempUrl.Replace("{ip}", dr["CameraIP"].ToString());
                tempUrl = tempUrl.Replace("{ch}", dr["Channel"].ToString());
                resolution.Url = tempUrl.Replace("{CH}", dr["Channel"].ToString());
                cameraInfo.ResolutionInfos.Add(resolution);
                videoInfo.CameraInfos.Add(cameraInfo);
            }
        }

        private void SetMediaServerInfo(DataSet mdDataSet, VideoInformation videoInfo)
        {
            // 저화질 / MD 정보 설정.
            foreach (DataRow dr in mdDataSet.Tables[0].Rows)
            {
                var mergeInfo = new MediaServerInfo
                                    {
                                        MediaServerId = dr["MediaServerId"].ToString(),
                                        MediaServerIp = dr["MediaServerIp"].ToString(),
                                        Width = dr["VideoBoardWidth"].ToString(),
                                        Height = dr["VideoBoardHeight"].ToString(),
                                        ColumnCount = dr["VideoBoardColumn"].ToString(),
                                        RowCount = dr["VideoBoardRow"].ToString(),
                                        ConnectMode = dr["ConnectMode"].ToString(),
                                    };

                var isMD200 =
                    !(string.Compare(dr["VideoBoardWidth"].ToString(), string.Empty, StringComparison.OrdinalIgnoreCase) == 0 ||
                      string.Compare(dr["VideoBoardWidth"].ToString(), "0", StringComparison.OrdinalIgnoreCase) == 0 ||
                      string.Compare(dr["VideoBoardHeight"].ToString(), string.Empty, StringComparison.OrdinalIgnoreCase) == 0 ||
                      string.Compare(dr["VideoBoardHeight"].ToString(), "0", StringComparison.OrdinalIgnoreCase) == 0);

                if (isMD200)
                {
                    mergeInfo.Url = string.Format("rtsp://{0}/all.sdp", dr["MediaServerIP"]);
                }

                videoInfo.MediaServerInfos.Add(mergeInfo);
            }
        }

        /// <summary>
        /// </summary>
        /// <param name="cameraXmlData">
        /// The camera xml data.
        /// </param>
        /// <param name="mergerXmlData">
        /// The merger xml data.
        /// </param>
        /// <returns>
        /// </returns>
        public string CreateVideoInformation(string cameraXmlData, string mergerXmlData)
        {
            var videoInformation = new VideoInformation();
            var videoInfo = new VideoInformation();

            // 저화질 정보 설정.
            var merger = new DataSet();
            using (var mergerStreamXml = new MemoryStream(Encoding.UTF8.GetBytes(mergerXmlData)))
            {
                merger.ReadXml(mergerStreamXml, XmlReadMode.ReadSchema);
            }

            foreach (DataRow dr in merger.Tables[0].Rows)
            {
                var mergeInfo = new MediaServerInfo
                {
                    MediaServerId = dr["MediaServerId"].ToString(),
                    Width = dr["VideoBoardWidth"].ToString(),
                    Height = dr["VideoBoardHeight"].ToString(),
                    ColumnCount = dr["VideoBoardColumn"].ToString(),
                    RowCount = dr["VideoBoardRow"].ToString(),
                    ConnectMode = dr["ConnectMode"].ToString(),
                    Url = string.Format("rtsp://{0}/all.sdp", dr["MediaServerIP"]),                    
                };

                videoInfo.MediaServerInfos.Add(mergeInfo);
            }

            bool isMiddleChecked = false;
            var dataSet = new DataSet();
            var streamXml = new MemoryStream(Encoding.UTF8.GetBytes(cameraXmlData));
            dataSet.ReadXml(streamXml, XmlReadMode.ReadSchema);

            foreach (DataRow row in dataSet.Tables[0].Rows)
            {
                MediaServerInfo mediaServer = null;
                foreach (var mergeVideoServerInfo in videoInfo.MediaServerInfos)
                {
                    if (string.IsNullOrWhiteSpace(row["MediaServerID"].ToString()))
                    {
                        break;
                    }

                    if (string.Compare(mergeVideoServerInfo.MediaServerId, row["MediaServerID"].ToString()) == 0)
                    {
                        mediaServer = mergeVideoServerInfo;
                        break;
                    }
                }

                string mdType;
                string mediaServerIP;
                if (mediaServer == null)
                {
                    mdType = "none";
                    mediaServerIP = string.Empty;
                }
                else
                {
                    if (string.Compare(mediaServer.Width, string.Empty) == 0 || string.Compare(mediaServer.Width, "0") == 0)
                    {
                        mdType = "md100";
                        mediaServerIP = row["MediaServerIP"].ToString();
                    }
                    else
                    {
                        mdType = "md200";
                        mediaServerIP = string.Empty;
                    }
                }

                if (videoInformation.CameraInfos.Count.Equals(0))
                {
                    var cameraInfo = new CameraInfo
                        {
                            CameraId = row["ProjectCameraID"].ToString(),
                            CameraName = row["ProjectCameraName"].ToString(),
                            ConnectId = row["RtspID"].ToString(),
                            ConnectPassword = row["RtspPassword"].ToString(),
                            ControlType = row["ControlType"].ToString(),
                            ConnectMode = row["ConnectMode"].ToString(),
                            ImageClippingArea = row["ImageClippingArea"].ToString(),
                            MediaServerId = row["MediaServerID"].ToString(),
                            //MediaServerIp = mediaServerIP,
                            RdsHost = row["RdsHost"].ToString(),
                            RdsPort =
                                string.IsNullOrWhiteSpace(row["RdsPort"].ToString())
                                    ? 0
                                    : Convert.ToInt32(row["RdsPort"].ToString()),
                            ShowAudioLevel = string.IsNullOrWhiteSpace(row["ShowAudioLevel"].ToString()) ? false : bool.Parse(row["ShowAudioLevel"].ToString()),
                            MediaServerExtraParams = row["MediaServerExtraParams"].ToString()
                        };

                    // 떡판용 Row, Column 설정.
                    var index = row["MediaServerPosition"].ToString();
                    var URLString = row["RtspUrl"].ToString();
                    if (mdType == "md200" && !string.IsNullOrWhiteSpace(index))
                    {
                        URLString = string.Format("rtsp://{0}/{1}.sdp?resolution=high", row["MediaServerIP"], index);

                        var indexToInt = Convert.ToInt32(index) - 1;
                        var parentMerger = merger.Tables[0].Rows.Cast<DataRow>().First(
                            item => string.Compare(item["MediaServerID"].ToString(), cameraInfo.MediaServerId) == 0);

                        cameraInfo.Row = (indexToInt / Convert.ToInt32(parentMerger["VideoBoardColumn"].ToString())).ToString();
                        cameraInfo.Column = (indexToInt % Convert.ToInt32(parentMerger["VideoBoardColumn"].ToString())).ToString();
                    }

                    var resolution = new ResolutionInfo
                    {
                        Width = row["Width"].ToString(),
                        Height = row["Height"].ToString(),
                        Fps = row["FPS"].ToString(),
                        Url = URLString,
                        IsPOD = string.IsNullOrWhiteSpace(row["IsPOD"].ToString()) ? false : bool.Parse(row["IsPOD"].ToString()),
                        Codec = row["Codec"].ToString(),
                        Bitrate = row["Bitrate"].ToString(),
                        ExtraParams = row["ExtraParams"].ToString(),
                    };

                    cameraInfo.ResolutionInfos.Add(resolution);

                    videoInformation.CameraInfos.Add(cameraInfo);
                    videoInfo.CameraInfos.Add(cameraInfo);
                }
                else
                {
                    CameraInfo cameraInfo = null;
                    var cameraId = string.Empty;

                    foreach (var info in videoInformation.CameraInfos)
                    {
                        if (info.CameraId.Equals(row["ProjectCameraID"].ToString()))
                        {
                            cameraId = row["ProjectCameraID"].ToString();
                            break;
                        }

                        cameraId = info.CameraId;
                    }

                    foreach (var info in videoInformation.CameraInfos.Where(info => cameraId.Equals(info.CameraId)))
                    {
                        // 떡판용 Row, Column 설정.
                        var index = row["MediaServerPosition"].ToString();
                        var URLString = row["RtspUrl"].ToString();
                        if (!info.CameraId.Equals(row["ProjectCameraID"].ToString()))
                        {
                            cameraInfo = new CameraInfo
                                             {
                                                 CameraId = row["ProjectCameraID"].ToString(),
                                                 CameraName = row["ProjectCameraName"].ToString(),
                                                 ConnectId = row["RtspID"].ToString(),
                                                 ConnectPassword = row["RtspPassword"].ToString(),
                                                 ControlType = row["ControlType"].ToString(),
                                                 ConnectMode = row["ConnectMode"].ToString(),
                                                 ImageClippingArea = row["ImageClippingArea"].ToString(),
                                                 //MediaServerIp = mediaServerIP,
                                                 MediaServerId = row["MediaServerID"].ToString(),
                                                 RdsHost = row["RdsHost"].ToString(),
                                                 RdsPort =
                                                     string.IsNullOrWhiteSpace(row["RdsPort"].ToString())
                                                         ? 0
                                                         : Convert.ToInt32(row["RdsPort"].ToString()),
                                                 ShowAudioLevel = string.IsNullOrWhiteSpace(row["ShowAudioLevel"].ToString()) ? false : bool.Parse(row["ShowAudioLevel"].ToString()),
                                                 MediaServerExtraParams = row["MediaServerExtraParams"].ToString()
                                             };

                            if (mdType == "md200" && !string.IsNullOrWhiteSpace(index))
                            {
                                isMiddleChecked = false;
                                URLString = string.Format("rtsp://{0}/{1}.sdp?resolution=high", row["MediaServerIP"], index);
                                var indexToInt = Convert.ToInt32(index) - 1;
                                var parentMerger = merger.Tables[0].Rows.Cast<DataRow>().First(
                                    item => string.Compare(item["MediaServerID"].ToString(), cameraInfo.MediaServerId) == 0);


                                cameraInfo.Row = (indexToInt / Convert.ToInt32(parentMerger["VideoBoardColumn"].ToString())).ToString();
                                cameraInfo.Column = (indexToInt % Convert.ToInt32(parentMerger["VideoBoardColumn"].ToString())).ToString();
                            }

                            var resolution = new ResolutionInfo
                                                 {
                                                     Width = row["Width"].ToString(),
                                                     Height = row["Height"].ToString(),
                                                     Fps = row["FPS"].ToString(),
                                                     Url = URLString,
                                                     IsPOD = string.IsNullOrWhiteSpace(row["IsPOD"].ToString()) ? false : bool.Parse(row["IsPOD"].ToString()),
                                                     Codec = row["Codec"].ToString(),
                                                     Bitrate = row["Bitrate"].ToString(),
                                                     ExtraParams = row["ExtraParams"].ToString(),
                                                 };

                            cameraInfo.ResolutionInfos.Add(resolution);
                            videoInfo.CameraInfos.Add(cameraInfo);

                        }
                        else
                        {
                            if (isMiddleChecked) break;

                            if (mdType == "md200")
                            {
                                URLString = string.Format("rtsp://{0}/{1}.sdp?resolution=middle", row["MediaServerIP"], index);
                                isMiddleChecked = true;
                            }

                            var resolutions = new ResolutionInfo
                                                  {
                                                      Width = row["Width"].ToString(),
                                                      Height = row["Height"].ToString(),
                                                      Fps = row["FPS"].ToString(),
                                                      Url = URLString,
                                                      IsPOD = string.IsNullOrWhiteSpace(row["IsPOD"].ToString()) ? false : bool.Parse(row["IsPOD"].ToString()),
                                                      Codec = row["Codec"].ToString(),
                                                      Bitrate = row["Bitrate"].ToString(),
                                                      ExtraParams = row["ExtraParams"].ToString(),
                                                  };

                            info.ResolutionInfos.Add(resolutions);
                        }
                    }

                    if (cameraInfo != null)
                    {
                        videoInformation.CameraInfos.Add(cameraInfo);
                    }
                }
            }

            var result = CreateToXML(videoInfo);

            return result;
        }

        private static string CreateToXML(VideoInformation video)
        {
            var serializer = new XmlSerializer(typeof(VideoInformation));
            string xmlData;

            using (var memStream = new MemoryStream())
            {
                var settings = new XmlWriterSettings
                    {
                        Indent = true,
                        IndentChars = new string(' ', 4),
                        NewLineOnAttributes = false,
                        Encoding = Encoding.UTF8
                    };

                using (var xmlWriter = XmlWriter.Create(memStream, settings))
                {
                    serializer.Serialize(xmlWriter, video);
                }

                xmlData = Encoding.UTF8.GetString(memStream.GetBuffer());
            }

            xmlData = xmlData.Substring(xmlData.IndexOf('<'));
            xmlData = xmlData.Substring(0, xmlData.LastIndexOf('>') + 1);

            return xmlData;
        }
    }
}