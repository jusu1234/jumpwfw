﻿

namespace InnowatchDataService.Commons
{
    using System;
    using System.Collections.Generic;
    using System.Xml.Serialization;
    using System.Runtime.Serialization;

    using InnowatchService.Commons;

    [XmlRoot("CameraControlInfo")]
    [DataContract]
    [Serializable]
    public class CameraControlInfo
    {
        #region Constants and Fields

        private List<PTZInformation> _ptzInfomationList = new List<PTZInformation>();

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets DeviceTypeList.
        /// </summary>
        [XmlArray("PtzInfomationList")]
        [XmlArrayItem("PtzInfomation", typeof(PTZInformation))]
        [DataMember(IsRequired = false)]
        public List<PTZInformation> PtzInfomationList
        {
            get
            {
                return this._ptzInfomationList;
            }

            set
            {
                this._ptzInfomationList = value;
            }
        }

        #endregion
    }
}