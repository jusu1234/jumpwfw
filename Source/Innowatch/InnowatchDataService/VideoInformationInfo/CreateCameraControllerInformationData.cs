﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CreateCameraControllerInformationData.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Defines the CreateCameraControllerInformationData type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace InnowatchService.VideoInformationInfo
{
    using System;
    using System.Data;
    using System.IO;
    using System.Text;
    using System.Xml;
    using System.Xml.Serialization;

    using InnowatchDataService.Commons;

    using InnowatchService.Commons;

    /// <summary>
    /// The create camera controller information data.
    /// </summary>
    public class CreateCameraControllerInformationData
    {
        /// <summary>
        /// The create camera controller info.
        /// </summary>
        /// <param name="dataSet">
        /// The data set.
        /// </param>
        /// <returns>
        /// The return data.
        /// </returns>
        public static string CreateCameraControllerInfo(DataSet dataSet)
        {
            var cameraControllerInfo = new CameraControlInfo();

            if (dataSet == null)
            {
                return string.Empty;
            }

            if (dataSet.Tables.Count == 0)
            {
                return string.Empty;
            }

            if (dataSet.Tables[0].Rows.Count == 0)
            {
                return string.Empty;
            }

            foreach (DataRow dr in dataSet.Tables[0].Rows)
            {
                var ptzInfo = new PTZInformation();
                ptzInfo.Actiontime = string.Empty;
                ptzInfo.Address = dr["ConnectIP"].ToString();
                ptzInfo.AudioMode = Convert.ToInt32(dr["AudioMode"].ToString());
                ptzInfo.BaudRate = dr["BaudRate"].ToString();
                ptzInfo.Channel = dr["Channel"].ToString();
                ptzInfo.AudioPort = dr["AudioPort"].ToString();
                ptzInfo.ContentsDefineId = dr["ProjectCameraID"].ToString();
                ptzInfo.DataBits = dr["DataCount"].ToString();
                ptzInfo.DllName = dr["ModuleFileName"].ToString();
                ptzInfo.GroupSelectData = dr["GroupSelectData"].ToString();
                ptzInfo.Intervaltime = string.Empty;
                ptzInfo.IpPort = dr["ConnectPort"].ToString();
                ptzInfo.Parity = dr["ParityBit"].ToString();
                ptzInfo.Password = dr["ConnectPassword"].ToString();
                ptzInfo.PresetCount = Convert.ToInt32(dr["PresetCount"].ToString());
                ptzInfo.SerialPort = dr["SerialPort"].ToString();
                ptzInfo.State = string.Empty;
                ptzInfo.StopBits = dr["StopBit"].ToString();
                ptzInfo.TargetPoint = string.Empty;
                ptzInfo.UseAutoIris = true;
                ptzInfo.UseAux1Power = string.Compare(dr["UseAux1Power"].ToString(), "0") != 0;
                ptzInfo.UseAux2Power = string.Compare(dr["UseAux2Power"].ToString(), "0") != 0;
                ptzInfo.UseCameraPower = string.Compare(dr["UseCameraPower"].ToString(), "0") != 0;
                ptzInfo.UseFocus = string.Compare(dr["UseFocus"].ToString(), "0") != 0;
                ptzInfo.UseIris = string.Compare(dr["UseIris"].ToString(), "0") != 0;
                ptzInfo.UseLightPower = string.Compare(dr["UseLightPower"].ToString(), "0") != 0;
                ptzInfo.UsePTZSpeed = string.Compare(dr["UseSpeed"].ToString(), "0") != 0;
                ptzInfo.UsePanTilt = string.Compare(dr["UsePanTilt"].ToString(), "0") != 0;
                ptzInfo.UsePreset = string.Compare(dr["UsePreset"].ToString(), "0") != 0;
                ptzInfo.UseZoom = string.Compare(dr["UseZoom"].ToString(), "0") != 0;
                ptzInfo.Username = dr["ConnectID"].ToString();

                cameraControllerInfo.PtzInfomationList.Add(ptzInfo);
            }

            return CreateToXML(cameraControllerInfo);
        }

        private static string CreateToXML(CameraControlInfo target)
        {
            var serializer = new XmlSerializer(typeof(CameraControlInfo));
            var memStream = new MemoryStream();

            var settings = new XmlWriterSettings
                               {
                                   Indent = true,
                                   IndentChars = new string(' ', 4),
                                   NewLineOnAttributes = false,
                                   Encoding = Encoding.UTF8
                               };

            var xmlWriter = XmlWriter.Create(memStream, settings);
            serializer.Serialize(xmlWriter, target);
            xmlWriter.Close();
            memStream.Close();

            var xmlData = Encoding.UTF8.GetString(memStream.GetBuffer());
            xmlData = xmlData.Substring(xmlData.IndexOf('<'));
            xmlData = xmlData.Substring(0, xmlData.LastIndexOf('>') + 1);

            return xmlData;
        }
    }
}