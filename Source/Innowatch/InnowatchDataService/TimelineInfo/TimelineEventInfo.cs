﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TimelineEventInfo.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   The timeline event info.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace InnowatchService.TimelineInfo
{
    using System.Xml.Serialization;

    /// <summary>
    /// The timeline event info.
    /// </summary>
    [XmlRoot("Event")]
    public class TimelineEventInfo
    {
        /// <summary>
        /// Gets or sets EventType.
        /// </summary>
        [XmlAttribute("EventType")]
        public string EventType { get; set; }

        /// <summary>
        /// Gets or sets EventColor.
        /// </summary>
        [XmlAttribute("EventColor")]
        public string EventColor { get; set; }

        /// <summary>
        /// Gets or sets BeginDate.
        /// </summary>
        [XmlAttribute("BeginDate")]
        public string BeginDate { get; set; }

        /// <summary>
        /// Gets or sets BeginDate.
        /// </summary>
        [XmlAttribute("EndDate")]
        public string EndDate { get; set; }

        /// <summary>
        /// Gets or sets MergeEventCount.
        /// </summary>
        [XmlAttribute("MergeEventCount")]
        public string MergeEventCount { get; set; }
    }
}