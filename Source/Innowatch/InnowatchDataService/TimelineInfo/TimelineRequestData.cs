﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TimelineRequestData.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   The timeline request data.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace InnowatchService.TimelineInfo
{
    using System.Xml.Serialization;

    /// <summary>
    /// The timeline request data.
    /// </summary>
    public class TimelineRequestData
    {
        /// <summary>
        /// Gets or sets BeginDate.
        /// </summary>
        [XmlElement("BeginDate")]
        public string BeginDate { get; set; }

        /// <summary>
        /// Gets or sets EndDate.
        /// </summary>
        [XmlElement("EndDate")]
        public string EndDate { get; set; }

        /// <summary>
        /// Gets or sets RecordIntervalSeconds.
        /// </summary>
        [XmlElement("RecordIntervalSeconds")]
        public string RecordIntervalSeconds { get; set; }

        /// <summary>
        /// Gets or sets EventIntervalSeconds.
        /// </summary>
        [XmlElement("EventIntervalSeconds")]
        public string EventIntervalSeconds { get; set; }

        /// <summary>
        /// Gets or sets RecordCameraInfoList.
        /// </summary>
        [XmlElement("RecordCameraInfoList")]
        public RecordCameraInfoList RecordCameraInfoList { get; set; }
    }
}
