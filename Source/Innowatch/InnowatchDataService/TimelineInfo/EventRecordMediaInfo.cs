﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="EventRecordMediaInfo.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   The event record media info.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace InnowatchService.TimelineInfo
{
    using System.Collections.Generic;
    using System.Xml.Serialization;

    /// <summary>
    /// The event record media info.
    /// </summary>
    [XmlRoot("EventRecordMediaInfo")]
    public class EventRecordMediaInfo
    {
        private List<TimelineCameraInfo> timelineCameraInfos = new List<TimelineCameraInfo>();

        /// <summary>
        /// Gets or sets TimelineCameraInfos.
        /// </summary>
        [XmlElement("RecordCameras")]
        public List<TimelineCameraInfo> TimelineCameraInfos
        {
            get { return this.timelineCameraInfos; }
            set { this.timelineCameraInfos = value; }
        }
    }
}