﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TimelineCameraInfo.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   The timeline camera info.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace InnowatchService.TimelineInfo
{
    using System.Collections.Generic;
    using System.Xml.Serialization;

    /// <summary>
    /// The timeline camera info.
    /// </summary>
    [XmlRoot("RecordCamera")]
    public class TimelineCameraInfo
    {
        private List<TimelineDateInfo> timelineDateInfos = new List<TimelineDateInfo>();

        private List<TimelineEventInfo> timelineEventInfos = new List<TimelineEventInfo>();

        /// <summary>
        /// Gets or sets Id.
        /// </summary>
        [XmlAttribute("Id")]
        public string Id { get; set; }

        /// <summary>
        /// Gets or sets TimelineCameraInfos.
        /// </summary>
        [XmlElement("Date")]
        public List<TimelineDateInfo> TimelineDateInfos
        {
            get { return this.timelineDateInfos; }
            set { this.timelineDateInfos = value; }
        }

        /// <summary>
        /// Gets or sets TimelineEventInfos.
        /// </summary>
        [XmlElement("Event")]
        public List<TimelineEventInfo> TimelineEventInfos
        {
            get { return this.timelineEventInfos; }
            set { this.timelineEventInfos = value; }
        }
    }
}