﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="RecordCameraInfo.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Defines the RecordCameraInfo type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace InnowatchService.TimelineInfo
{
    using System.Xml.Serialization;

    /// <summary>
    /// The record camera info.
    /// </summary>
    [XmlRoot("RecordCameraInfo")]
    public class RecordCameraInfo
    {
        /// <summary>
        /// Gets or sets the record camera number.
        /// </summary>
        [XmlAttribute("RecordCameraNumber")]
        public string RecordCameraNumber { get; set; }

        /// <summary>
        /// Gets or sets the recorder ip.
        /// </summary>
        [XmlAttribute("RecorderIP")]
        public string RecorderIP { get; set; }
    }
}