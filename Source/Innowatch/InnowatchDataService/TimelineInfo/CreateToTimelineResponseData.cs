﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CreateToTimelineResponseData.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Defines the CreateToTimelineResponseData type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Linq;

namespace InnowatchService.TimelineInfo
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Data;
    using System.IO;
    using System.Text;
    using System.Xml;
    using System.Xml.Serialization;

    using Innotive.InnoWatch.CommonUtils.Log;

    /// <summary>
    /// The create to timeline response data.
    /// </summary>
    public class CreateToTimelineResponseData
    {
        private readonly TimelineResponseData timelineResponseData;

        private readonly List<RecordCameraInfo> recordCameraList;

        private readonly DataSet dateDataSet;

        private readonly int mergeIntervalSeconds;

        /// <summary>
        /// Initializes a new instance of the <see cref="CreateToTimelineResponseData"/> class.
        /// </summary>
        /// <param name="recordMediaCamera">
        /// The record media camera.
        /// </param>
        /// <param name="recordMediaCameraDate">
        /// The record media camera date.
        /// </param>
        /// <param name="intervalSeconds">
        /// The interval Seconds.
        /// </param>
        public CreateToTimelineResponseData(List<RecordCameraInfo> recordMediaCamera, DataSet recordMediaCameraDate, string intervalSeconds)
        {
            this.timelineResponseData = new TimelineResponseData();
            this.mergeIntervalSeconds = int.Parse(intervalSeconds);
            //this.dateDataSet = new DataSet();
            this.recordCameraList = recordMediaCamera;
            this.dateDataSet = recordMediaCameraDate;
        }

        /// <summary>
        /// The create response data.
        /// </summary>
        /// <returns>
        /// The xml to timeline response data.
        /// </returns>
        public string CreateResponseData()
        {
            InnotiveDebug.Trace("Reprocessed Date Start");

            var reprocessedDate = this.ReprocessedDate(this.recordCameraList, this.dateDataSet, this.mergeIntervalSeconds);

            InnotiveDebug.Trace("Reprocessed Date End");

            InnotiveDebug.Trace("Serialize Start");

            this.CreateRecordMediaInfo(this.recordCameraList, reprocessedDate);

            InnotiveDebug.Trace("Serialize End");

            // EventTimeLine 추가.
            this.CreateEventRecordInfo();

            return this.CreateToXML(this.timelineResponseData);
        }

        private void CreateEventRecordInfo()
        {
            var eventTable = this.dateDataSet.Tables["EventTimeLineTable"];

            if (eventTable == null)
                return;


            foreach (var recordMediaInfo in this.timelineResponseData.RecordMediaInfos)
            {
                foreach (var timeLineInfo in recordMediaInfo.TimelineCameraInfos)
                {
                    var rows =
                        eventTable.Rows.Cast<DataRow>().Where(
                            row => string.CompareOrdinal(row[0].ToString(), timeLineInfo.Id) == 0);

                    foreach (DataRow dataRow in rows)
                    {
                        var eventData = new TimelineEventInfo()
                        {
                            BeginDate = dataRow[1].ToString(),
                            EndDate = dataRow[2].ToString(),
                            EventType = dataRow[3].ToString(),
                            EventColor = dataRow[4].ToString(),
                            //MergeEventCount = dataRow[5].ToString()
                        };
                        timeLineInfo.TimelineEventInfos.Add(eventData);
                    }
                }
            }
        }

        private DataSet ReprocessedDate(List<RecordCameraInfo> recordCameraData, DataSet dateData, int interval)
        {
            var timelineDateList = new List<TimelineDateList>();

            foreach (DataTable dataTable in dateData.Tables)
            {
                if (dataTable.Rows.Count.Equals(0))
                {
                    continue;
                }

                var recordCameraNumber = 0;
                long postBeginDate = 0;
                long postEndDate = 0;

                recordCameraNumber = this.GetRequestRecordCameraNumber(recordCameraData, dataTable);

                if (recordCameraNumber.Equals(0))
                {
                    continue;
                }

                for (var rowCount = 0; rowCount < dataTable.Rows.Count; rowCount++)
                {
                    var timelineDate = new TimelineDateList {RecordCameraNumber = recordCameraNumber};

                    var currentBeginDate = long.Parse(dataTable.Rows[rowCount]["BeginDate"].ToString());
                    var currentEndDate = long.Parse(dataTable.Rows[rowCount]["EndDate"].ToString());

                    // 데이터 하나만 존재할 경우.
                    if (dataTable.Rows.Count.Equals(1))
                    {
                        timelineDate.BeginTime = currentBeginDate;
                        timelineDate.EndTime = currentEndDate;

                        timelineDateList.Add(timelineDate);

                        break;
                    }

                    // 데이터가 두개 이상 존재.
                    // 첫번째 데이터 처리.
                    if (rowCount.Equals(0))
                    {
                        postBeginDate = currentBeginDate;
                        postEndDate = currentEndDate;

                        continue;
                    }

                    // 현재 BeginDate 와 앞 데이터의 EndDate 가 Interval 보다 작으면, 
                    // Date 연장을 위한 PostEndDate 설정.
                    if (currentBeginDate - postEndDate <= interval)
                    {
                        if (dataTable.Rows.Count - rowCount == 1)
                        {
                            timelineDate.BeginTime = postBeginDate;
                            timelineDate.EndTime = currentEndDate;

                            timelineDateList.Add(timelineDate);
                        }

                        postEndDate = currentEndDate;

                        continue;
                    }

                    timelineDate.BeginTime = postBeginDate;
                    timelineDate.EndTime = postEndDate;

                    timelineDateList.Add(timelineDate);

                    if (dataTable.Rows.Count - rowCount == 1)
                    {
                        var lastTimelineDate = new TimelineDateList
                        {
                            RecordCameraNumber = recordCameraNumber,
                            BeginTime = currentBeginDate,
                            EndTime = currentEndDate
                        };

                        timelineDateList.Add(lastTimelineDate);
                    }

                    postBeginDate = currentBeginDate;
                    postEndDate = currentEndDate;
                }
            }

            return this.ReprocessedDateDataSet(recordCameraData, timelineDateList);
        }

        private int GetRequestRecordCameraNumber(List<RecordCameraInfo> recordCameraInfo, DataTable dateData)
        {
            var stringSeparators = new[] { "Table" };

            var number = dateData.TableName.Split(stringSeparators, StringSplitOptions.RemoveEmptyEntries);

            return recordCameraInfo.Any(cameraInfo => cameraInfo.RecordCameraNumber.Equals(number[0])) ? int.Parse(number[0]) : 0;
        }

        private DataSet ReprocessedDateDataSet(ICollection recordCameraData, IList<TimelineDateList> timelineDateList)
        {
            var dataSet = new DataSet();

            var cameraNumberListCount = recordCameraData.Count;

            for (var number = 1; number <= cameraNumberListCount; number++)
            {
                var dataTable = new DataTable(string.Format("Date{0}DataTable", number));

                var columnRecordCameraNumber = new DataColumn("RecordCameraNumber", typeof(int));
                var columnBeginDate = new DataColumn("BeginDate", typeof(long));
                var columnEndDate = new DataColumn("EndDate", typeof(long));
                dataTable.Columns.Add(columnRecordCameraNumber);
                dataTable.Columns.Add(columnBeginDate);
                dataTable.Columns.Add(columnEndDate);

                foreach (var timelineDate in timelineDateList)
                {
                    var newRow = dataTable.NewRow();
                    newRow["RecordCameraNumber"] = timelineDate.RecordCameraNumber;
                    newRow["BeginDate"] = timelineDate.BeginTime;
                    newRow["EndDate"] = timelineDate.EndTime;

                    dataTable.Rows.Add(newRow);
                }

                dataSet.Tables.Add(dataTable);
            }

            return dataSet;
        }

        #region RecordMedia

        private void CreateRecordMediaInfo(IList<RecordCameraInfo> recordCameras, DataSet dates)
        {
            var recordMediaInfo = new RecordMediaInfo();

            for (var i = 0; i < recordCameras.Count; i++)
            {
                var setRecordCamera = this.SetCameraInfo(recordCameras[i].RecordCameraNumber);
                var setDate = this.SetDate(recordCameras[i].RecordCameraNumber, dates, i);

                setRecordCamera.TimelineDateInfos.AddRange(setDate);
                recordMediaInfo.TimelineCameraInfos.Add(setRecordCamera);
            }

            this.timelineResponseData.RecordMediaInfos.Add(recordMediaInfo);
        }

        private TimelineCameraInfo SetCameraInfo(string recordCamera)
        {
            var cameraInfo = new TimelineCameraInfo
            {
                Id = recordCamera
            };

            return cameraInfo;
        }

        private IEnumerable<TimelineDateInfo> SetDate(string recordCamera, DataSet dates, int findTable)
        {
            var dateList = new List<TimelineDateInfo>();

            foreach (DataRow date in dates.Tables[findTable].Rows)
            {
                if (!recordCamera.Equals(date["RecordCameraNumber"].ToString()))
                {
                    continue;
                }

                var dateInfo = new TimelineDateInfo
                {
                    BeginDate = date["BeginDate"].ToString(),
                    EndDate = date["EndDate"].ToString()
                };

                dateList.Add(dateInfo);
            }

            return dateList;
        }

        #endregion // RecordMedia

        #region EventRecordMedia

        // TODO : EventRecordCamera 관련 작업은 NVR Alarm 작업 이후에 진행. by.ygsong

        #endregion // EventRecordMedia

        private string CreateToXML(TimelineResponseData target)
        {
            var serializer = new XmlSerializer(typeof(TimelineResponseData));
            var memStream = new MemoryStream();

            var settings = new XmlWriterSettings
            {
                Indent = true,
                IndentChars = new string(' ', 4),
                NewLineOnAttributes = false,
                Encoding = Encoding.UTF8
            };

            var xmlWriter = XmlWriter.Create(memStream, settings);
            serializer.Serialize(xmlWriter, target);
            xmlWriter.Close();
            memStream.Close();

            var xmlData = Encoding.UTF8.GetString(memStream.GetBuffer());
            xmlData = xmlData.Substring(xmlData.IndexOf('<'));
            xmlData = xmlData.Substring(0, xmlData.LastIndexOf('>') + 1);

            return xmlData;
        }
    }

    /// <summary>
    /// The timeline date list.
    /// </summary>
    public class TimelineDateList
    {
        /// <summary>
        /// Gets or sets RecordCameraNumber.
        /// </summary>
        public int RecordCameraNumber { get; set; }

        /// <summary>
        /// Gets or sets BeginTime.
        /// </summary>
        public long BeginTime { get; set; }

        /// <summary>
        /// Gets or sets EndTime.
        /// </summary>
        public long EndTime { get; set; }
    }
}