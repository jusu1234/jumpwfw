﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TimelineResponseData.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   The timeline response data.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace InnowatchService.TimelineInfo
{
    using System.Collections.Generic;
    using System.Xml.Serialization;

    /// <summary>
    /// The timeline response data.
    /// </summary>
    public class TimelineResponseData
    {
        private List<RecordMediaInfo> recordMediaInfos = new List<RecordMediaInfo>();

        //private List<EventRecordMediaInfo> eventRecordMediaInfos = new List<EventRecordMediaInfo>();

        /// <summary>
        /// Gets or sets RecordMediaInfos.
        /// </summary>
        [XmlElement("RecordMediaInfo")]
        public List<RecordMediaInfo> RecordMediaInfos
        {
            get { return this.recordMediaInfos; }
            set { this.recordMediaInfos = value; }
        }

        ///// <summary>
        ///// Gets or sets EventRecordMediaInfos.
        ///// </summary>
        //[XmlElement("EventRecordMediaInfo")]
        //public List<EventRecordMediaInfo> EventRecordMediaInfos
        //{
        //    get { return this.eventRecordMediaInfos; }
        //    set { this.eventRecordMediaInfos = value; }
        //}
    }
}