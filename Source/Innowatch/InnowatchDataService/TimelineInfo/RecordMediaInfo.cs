﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="RecordMediaInfo.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   The record media info.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace InnowatchService.TimelineInfo
{
    using System.Collections.Generic;
    using System.Xml.Serialization;

    /// <summary>
    /// The record media info.
    /// </summary>
    [XmlRoot("RecordMediaInfo")]
    public class RecordMediaInfo
    {
        private List<TimelineCameraInfo> timelineCameraInfos = new List<TimelineCameraInfo>();

        /// <summary>
        /// Gets or sets TimelineCameraInfos.
        /// </summary>
        [XmlElement("RecordCamera")]
        public List<TimelineCameraInfo> TimelineCameraInfos
        {
            get { return this.timelineCameraInfos; }
            set { this.timelineCameraInfos = value; }
        }
    }
}