﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="RecordCameraInfoList.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Defines the RecordCameraInfoList type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace InnowatchService.TimelineInfo
{
    using System.Collections.Generic;
    using System.Xml.Serialization;

    /// <summary>
    /// The record camera info list.
    /// </summary>
    [XmlRoot("RecordCameraInfoList")]
    public class RecordCameraInfoList
    {
        private List<RecordCameraInfo> recordCameraInfos = new List<RecordCameraInfo>();

        /// <summary>
        /// Gets or sets the record camera info.
        /// </summary>
        [XmlElement("RecordCameraInfo")]
        public List<RecordCameraInfo> RecordCameraInfo
        {
            get
            {
                return this.recordCameraInfos;
            }

            set
            {
                this.recordCameraInfos = value;
            }
        }
    }
}