﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TimelineDateInfo.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   The timeline date info.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace InnowatchService.TimelineInfo
{
    using System.Xml.Serialization;

    /// <summary>
    /// The timeline date info.
    /// </summary>
    [XmlRoot("Date")]
    public class TimelineDateInfo
    {
        /// <summary>
        /// Gets or sets BeginDate.
        /// </summary>
        [XmlAttribute("BeginDate")]
        public string BeginDate { get; set; }

        /// <summary>
        /// Gets or sets EndDate.
        /// </summary>
        [XmlAttribute("EndDate")]
        public string EndDate { get; set; }
    }
}