﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="GetDataForClient.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Defines the GetDataForClient type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace InnowatchDataHandler
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Xml;
    using System.Xml.Linq;

    using Commons;

    using Innotive.InnoWatch.CommonUtils.Log;

    /// <summary>
    /// The get data for client.
    /// </summary>
    public class GetDataForClient
    {
        #region GetData

        #region Client

        /// <summary>
        /// The get camera info for video information.
        /// </summary>
        /// <param name="projectId">
        /// The project id.
        /// </param>
        /// <returns>
        /// The data set of get data.
        /// </returns>
        public DataSet GetCameraInfoForVideoInformation(string projectId)
        {
            try
            {
                return DataManager.GetInstance.DataHandler.GetExecuteProcedureDataSet("SP_GetData_Client_CameraInforForVideoInformation", projectId);
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        public DataSet GetAllCameraInfo()
        {
            try
            {
                return DataManager.GetInstance.DataHandler.GetExecuteProcedureDataSet("SP_GetData_Client_AllCameraInfo");
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        /// <summary>
        /// The get media server info for video information.
        /// </summary>
        /// <param name="projectId">
        /// The project id.
        /// </param>
        /// <returns>
        /// The data set of get data.
        /// </returns>
        public DataSet GetMediaServerInfoForVideoInformation(string projectId)
        {
            try
            {
                return DataManager.GetInstance.DataHandler.GetExecuteProcedureDataSet("SP_GetData_Client_MediaServerInfoForVideoInformation", projectId);
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        /// <summary>
        /// The get favorite item ids.
        /// </summary>
        /// <param name="projectId">
        /// The project id.
        /// </param>
        /// <param name="userId">
        /// The user id.
        /// </param>
        /// <param name="type">
        /// The type.
        /// </param>
        /// <returns>
        /// The favoriet ids.
        /// </returns>
        public List<int> GetFavoriteItemIDs(string projectId, string userId, string type)
        {
            var dataSet = this.SelectFavoriteID(projectId, userId, type);

            var favoriteIDs = new List<int>();

            foreach (DataRow row in dataSet.Tables[0].Rows)
            {
                favoriteIDs.Add(Convert.ToInt32(row["FavoriteID"].ToString()));
            }

            return favoriteIDs;
        }

        /// <summary>
        /// The get user info.
        /// </summary>
        /// <param name="userId">The user id.</param>
        /// <param name="password">The password.</param>
        /// <returns>The result data.</returns>
        public string GetUserInfo(string userId, string password, string client)
        {
            try
            {
                if (string.Compare(client, "COMMAND", true) == 0)
                {
                    var userDataSet = DataManager.GetInstance.DataHandler.GetExecuteProcedureDataSet("SP_GetData_Client_UserInfo_ForCommand", userId, password);

                    return this.ExecuteGetUserInfoForPrivilege(userDataSet, userId, password);
                }

                if (string.Compare(client, "VIEWER", true) == 0)
                {
                    var userDataSet = DataManager.GetInstance.DataHandler.GetExecuteProcedureDataSet("SP_GetData_Client_UserInfo_ForViewer", userId, password);

                    return this.ExecuteGetUserInfoForPrivilege(userDataSet, userId, password);
                }

                if (string.Compare(client, "PLAYBACK", true) == 0)
                {
                    var userDataSet = DataManager.GetInstance.DataHandler.GetExecuteProcedureDataSet("SP_GetData_Client_UserInfo_ForPlayback", userId, password);

                    return this.ExecuteGetUserInfoForPrivilege(userDataSet, userId, password);
                }

                var userDataSetGeneral = DataManager.GetInstance.DataHandler.GetExecuteProcedureDataSet("SP_GetData_Client_UserInfo", userId, password);

                return this.ExecuteGetUserInfoForPrivilege(userDataSetGeneral, userId, password);
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, "GetUserInfo : " + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// The get camera group for client. (Folder 정보).
        /// </summary>
        /// <param name="projectId">
        /// The project id.
        /// </param>
        /// <param name="userId">
        /// The user id.
        /// </param>
        /// <returns>
        /// The dataset of camera group.
        /// </returns>
        public DataSet GetCameraGroupForClient(string projectId, string userId)
        {
            try
            {
                return DataManager.GetInstance.DataHandler.GetExecuteProcedureDataSet("SP_GetData_Client_UserInfo", projectId, userId);
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="projectId"></param>
        /// <param name="userId"></param>
        /// <param name="type"></param>
        /// <param name="group"></param>
        /// <returns></returns>
        public DataSet GetCameraGroupForClientByGrouping(string projectId, string group)
        {
            try
            {
                return 
                    DataManager.GetInstance.DataHandler.GetExecuteProcedureDataSet("SP_GetData_Client_CameraGroupByGrouping", projectId, group);
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        /// <summary>
        /// The get camera group mapping for client.
        /// </summary>
        /// <param name="projectId">The project id.</param>
        /// <param name="userId">The user id.</param>
        /// <returns>The dataset of camera group mapping.</returns>
        public DataSet GetCameraGroupMappingForClient(string projectId, string userId)
        {
            try
            {
                return DataManager.GetInstance.DataHandler.GetExecuteProcedureDataSet("SP_GetData_Client_CameraGroupMapping", projectId, userId);
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        /// <summary>
        /// The get favorite item order for client.
        /// </summary>
        /// <param name="from">The table.</param>
        /// <param name="whereQuery">The where query.</param>
        /// <param name="parentId">The parent id</param>
        /// <param name="type">The type</param>
        /// <param name="projectId">The project id.</param>
        /// <param name="userId">The user id.</param>
        /// <returns>The dataset of get favorte item order.</returns>
        public DataSet GetFavoriteItemOrderForClient(
            string from, string whereQuery, int parentId, string type, string projectId = null, string userId = null)
        {
            try
            {
                var identityQuery = string.Empty;
                var paramInfo = new List<ParamInfo>();
                if (!(string.IsNullOrWhiteSpace(projectId) && string.IsNullOrWhiteSpace(userId)))
                {
                    identityQuery = string.Format("AND ProjectID = @ProjectID AND UserID = @UserID AND Type = @Type");
                    paramInfo.Add(
                        new ParamInfo("@ProjectID", DbType.String, CommonUtil.ConvertToDbTypeString(projectId)));
                    paramInfo.Add(
                        new ParamInfo(
                            "@UserID", DbType.String, CommonUtil.ConvertToDbTypeString(userId).ToString().ToLower()));
                    paramInfo.Add(
                        new ParamInfo(
                            "@Type", DbType.String, CommonUtil.ConvertToDbTypeString(type).ToString().ToLower()));
                }

                var query =
                    string.Format(
                        "SELECT COUNT(FavoriteGroupID) AS ItemOrder FROM {0} WHERE {1} = @ParentID {2}",
                        from,
                        whereQuery,
                        identityQuery);

                paramInfo.Add(new ParamInfo("@ParentID", DbType.Int32, parentId));

                return DataManager.GetInstance.DataHandler.GetExecuteDataSet(query, paramInfo);
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        /// <summary>
        /// The get favorite group for client.
        /// </summary>
        /// <param name="projectId">The project id.</param>
        /// <param name="userId">The user id.</param>
        /// <param name="type">The type.</param>
        /// <returns>The dataset of get favorite group.</returns>
        public DataSet GetFavoriteGroupForClient(string projectId, string userId, string type)
        {
            try
            {
                return DataManager.GetInstance.DataHandler.GetExecuteProcedureDataSet("SP_GetData_Client_FavoriteGroup", projectId, userId, type);
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        /// <summary>
        /// The get favorite group for client.
        /// </summary>
        /// <returns>
        /// The dataset of get favorite group.
        /// </returns>
        public DataSet GetFavoriteGroupForClient()
        {
            try
            {
                return DataManager.GetInstance.DataHandler.GetExecuteProcedureDataSet("SP_GetDataClient_FavoriteGroup", CommonUtil.ConvertToDbTypeString(null), CommonUtil.ConvertToDbTypeString(null), CommonUtil.ConvertToDbTypeString(null));
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        /// <summary>
        /// The Get favorite group mapping for client.
        /// </summary>
        /// <param name="projectId">The project id.</param>
        /// <param name="userId">The user id.</param>
        /// <param name="type">The type.</param>
        /// <returns>The dataset of get favorite group mapping.</returns>
        public DataSet GetFavoriteGroupMappingForClient(string projectId, string userId, string type)
        {
            try
            {
                return DataManager.GetInstance.DataHandler.GetExecuteProcedureDataSet("SP_GetData_Client_FavoriteGroupMapping", projectId, userId, type);
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        /// <summary>
        /// The get last favorite group id.
        /// </summary>
        /// <returns>
        /// The data set of get data.
        /// </returns>
        public DataSet GetLastFavoriteGroupId()
        {
            try
            {
                return DataManager.GetInstance.DataHandler.GetExecuteProcedureDataSet("SP_GetData_Client_LastFavoriteGroupId");
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        public string GetCameraDataToMediaConfigXml(string mediaServerIp)
        {
            DataManager.GetInstance.DataHandler.InsertLogData(
                LogType.Info, string.Format("[Begin] Get Data Cast Server Ip: {0}", mediaServerIp));

            XElement settingsXml = null;

            // mediaServerIp에 해당하는 Media Server의 정보를 얻음
            try
            {
                DataManager.GetInstance.DataHandler.InsertLogData(
                    LogType.Info, string.Format("Cast Server IP: {0}", mediaServerIp));

                var mediaServer = this.GetMergerListByCastServerIp(mediaServerIp);

                if (mediaServer.Tables.Count == 0 || mediaServer.Tables[0].Rows.Count == 0)
                {
                    DataManager.GetInstance.DataHandler.InsertLogData(
                        LogType.Error,
                        string.Format("Cast Server Ip: {0} the information can not be found in Database", mediaServerIp));
                    return null;
                }

                var mediaServerId = mediaServer.Tables[0].Rows[0]["MediaServerID"].ToString();
                var mulitcastIp = mediaServer.Tables[0].Rows[0]["TransmitIP"].ToString();
                var multicastPort = mediaServer.Tables[0].Rows[0]["TransmitPort"].ToString();

                var rows = mediaServer.Tables[0].Rows[0]["VideoBoardRow"].ToString();
                var columns = mediaServer.Tables[0].Rows[0]["VideoBoardColumn"].ToString();

                if (string.IsNullOrWhiteSpace(rows) || string.IsNullOrWhiteSpace(columns))
                {
                    return string.Empty;
                }

                // Camera 정보를 가져오기 전에 Camera Table의 MediaServerPosition을 Update 한다.
                this.UpdateCameraMediaServerPosition(mediaServerId);

                settingsXml = new XElement("settings");

                // Create "config" Tag
                settingsXml.Add(
                    new XElement(
                        "config",
                        new XElement("mosaic", new XElement("rows", rows), new XElement("columns", columns)),
                        this.CreateCameraElement(mediaServerId),
                        new XElement("transmit", new XElement("ip", mulitcastIp), new XElement("port", multicastPort))));
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
            }

            if (settingsXml != null)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Info, settingsXml.ToString());
                return settingsXml.ToString();
            }

            return null;
        }

        /// <summary>
        /// Cast Server IP에 해당하는 Cast Server 정보 반환.
        /// </summary>
        /// <param name="mediaServerIp">Cast Server IP.</param>
        /// <returns>Cast Server DataSet.</returns>
        public DataSet GetMergerListByCastServerIp(string mediaServerIp)
        {
            try
            {
                return DataManager.GetInstance.DataHandler.GetExecuteProcedureDataSet("SP_GetData_Client_MergerList", mediaServerIp);
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        /// <summary>
        /// The get merge information.
        /// </summary>
        /// <returns>
        /// The xml to media server info data set.
        /// </returns>
        public string GetMergeInformation()
        {
            try
            {
                var xmlVideoInfo = CommonUtil.XmlToDataSet(DataManager.GetInstance.DataHandler.GetExecuteProcedureDataSet("SP_GetData_Client_MergeInformation"));

                return xmlVideoInfo;
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        /// <summary>
        /// The get license.
        /// </summary>
        /// <param name="ip">
        /// The ip.
        /// </param>
        /// <param name="type">
        /// The type.
        /// </param>
        /// <returns>
        /// The active code.
        /// </returns>
        public string GetLicense(string ip, string type)
        {
            DataManager.GetInstance.DataHandler.InsertLogData(
                LogType.Info, string.Format("[Begin] Get License client IP : {0}", ip));

            XElement licenseXml = null;
            try
            {
                if (string.IsNullOrEmpty(type))
                {
                    // MD에서 라이센스 찾음
                    var ac = this.GetMediaServerLicense(ip);

                    // MD에서 라이센스 없으면 View Server(CM/VW/DP)에서 찾음
                    if (string.IsNullOrEmpty(ac))
                    {
                        ac = this.GetViewServerLicense(ip, null);
                    }

                    licenseXml = new XElement("license");
                    licenseXml.Add(new XElement("activecode", ac));
                }
                else
                {
                    if (string.Compare("Media", type, true) == 0)
                    {
                        // MD에서 라이센스 찾음
                        var ac = this.GetMediaServerLicense(ip);

                        licenseXml = new XElement("license");
                        licenseXml.Add(new XElement("activecode", ac));
                    }
                    else if (string.Compare("Recorder", type, true) == 0)
                    {
                        // MD에서 라이센스 찾음
                        var ac = this.GetRecorderLicense(ip);

                        licenseXml = new XElement("license");
                        licenseXml.Add(new XElement("activecode", ac));
                    }
                    else if (string.Compare("Display", type, true) == 0)
                    {
                        var ac = this.GetDisplayLicense(ip);

                        licenseXml = new XElement("license");
                        licenseXml.Add(new XElement("activecode", ac));
                    }
                    else
                    {
                        var ac = this.GetViewServerLicense(ip, type);

                        licenseXml = new XElement("license");
                        licenseXml.Add(new XElement("activecode", ac));
                    }
                }
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
            }

            if (licenseXml != null)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(
                    LogType.Info, "<?xml version=\"1.0\" encoding=\"utf-8\"?> " + licenseXml);
                return licenseXml.ToString();
            }

            return null;
        }

        /// <summary>
        /// The get media server license.
        /// </summary>
        /// <param name="mediaServerIp">
        /// The media server ip.
        /// </param>
        /// <returns>
        /// The media server active code.
        /// </returns>
        public string GetMediaServerLicense(string mediaServerIp)
        {
            try
            {
                var ds = DataManager.GetInstance.DataHandler.GetExecuteProcedureDataSet("SP_GetData_Client_MediaServerLicense", mediaServerIp);

                if (ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        return ds.Tables[0].Rows[0]["ActiveCode"].ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return string.Empty;
            }

            return string.Empty;
        }

        /// <summary>
        /// The recorder license.
        /// </summary>
        /// <param name="mediaServerIp">
        /// The recorder ip.
        /// </param>
        /// <returns>
        /// The recorder active code.
        /// </returns>
        public string GetRecorderLicense(string RecorderIp)
        {
            try
            {
                var ds = DataManager.GetInstance.DataHandler.GetExecuteProcedureDataSet("SP_GetData_Client_RecorderLicense", RecorderIp);

                if (ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        return ds.Tables[0].Rows[0]["ActiveCode"].ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return string.Empty;
            }

            return string.Empty;
        }

        /// <summary>
        /// The recorder license.
        /// </summary>
        /// <param name="mediaServerIp">
        /// The recorder ip.
        /// </param>
        /// <returns>
        /// The recorder active code.
        /// </returns>
        public string GetDisplayLicense(string displayIp)
        {
            try
            {
                var ds = DataManager.GetInstance.DataHandler.GetExecuteProcedureDataSet("SP_GetData_Client_DisplayLicense", displayIp);

                if (ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        return ds.Tables[0].Rows[0]["ActiveCode"].ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return string.Empty;
            }

            return string.Empty;
        }


        /// <summary>
        /// The get view server license.
        /// </summary>
        /// <param name="viewServerIp">
        /// The view server ip.
        /// </param>
        /// <param name="type">
        /// The view server type.
        /// </param>
        /// <returns>
        /// The activate code.
        /// </returns>
        public string GetViewServerLicense(string viewServerIp, string type)
        {
            try
            {
                if (string.IsNullOrEmpty(type))
                {
                    var ds = DataManager.GetInstance.DataHandler.GetExecuteProcedureDataSet("SP_GetData_Client_License", viewServerIp, CommonUtil.ConvertToDbTypeString(null));

                    if (ds.Tables.Count > 0)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            return ds.Tables[0].Rows[0]["ActiveCode"].ToString();
                        }
                    }
                }
                else
                {
                    var ds = DataManager.GetInstance.DataHandler.GetExecuteProcedureDataSet("SP_GetData_Client_License", viewServerIp, type);

                    if (ds.Tables.Count > 0)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            return ds.Tables[0].Rows[0]["ActiveCode"].ToString();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return string.Empty;
            }

            return string.Empty;
        }

        /// <summary>
        /// The get location bookmark.
        /// </summary>
        /// <param name="projectId">
        /// The project id.
        /// </param>
        /// <param name="userId">
        /// The user id.
        /// </param>
        /// <returns>
        /// The location bookmark data.
        /// </returns>
        public string GetLocationBookmark(string projectId, string userId)
        {
            try
            {
                return CommonUtil.XmlToDataSet(DataManager.GetInstance.DataHandler.GetExecuteProcedureDataSet("SP_GetData_Client_LocationBookmark", projectId, userId));
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        /// <summary>
        /// The get favorite slide.
        /// </summary>
        /// <param name="projectId">
        /// The project id.
        /// </param>
        /// <param name="userId">
        /// The user id.
        /// </param>
        /// <param name="type">
        /// The type.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string GetFavoriteSlide(string projectId, string userId, string type)
        {
            try
            {
                return CommonUtil.XmlToDataSet(DataManager.GetInstance.DataHandler.GetExecuteProcedureDataSet("SP_GetData_Client_FavoriteSlide", projectId, userId, type));
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        /// <summary>
        /// The get favorite schedule.
        /// </summary>
        /// <param name="projectId">
        /// The project id.
        /// </param>
        /// <param name="userId">
        /// The user id.
        /// </param>
        /// <param name="type">
        /// The type.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string GetFavoriteSchedule(string projectId, string userId, string type)
        {
            try
            {
                var dataSet =
                    DataManager.GetInstance.DataHandler.GetExecuteProcedureDataSet("SP_GetData_Client_FavoriteSchedule", projectId, userId, type);

                if (dataSet == null)
                    return string.Empty;

                if (dataSet.Tables.Count == 0)
                    return string.Empty;

                if (dataSet.Tables[0].Rows.Count == 0)
                    return string.Empty;

                return dataSet.Tables[0].Rows[0][0].ToString();
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        /// <summary>
        /// The get map layout.
        /// MapLayout Data를 가져온다.
        /// </summary>
        /// <param name="projectId">
        /// 조회할 Projece ID.
        /// </param>
        /// <returns>
        /// The Map Data.
        /// </returns>
        public string GetMapLayout(string projectId)
        {
            try
            {
                var dataSet = this.ExecuteQueryToGetMapLayoutTable(projectId);
                var mapData = this.GetMapLayoutDataString(dataSet);

                return mapData;
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        /// <summary>
        /// ProjectId, UserId, ClientId로 CommandAlarmAction을 찾아온다.
        /// </summary>
        /// <param name="projectId">The project Id.</param>
        /// <param name="userId">The user Id.</param>
        /// <param name="clientId">The client Id.</param>
        /// <returns>The result DataSet.</returns>
        public DataSet GetCommandAlarmActionForClient(string projectId, string userId, string clientId)
        {
            try
            {
                return DataManager.GetInstance.DataHandler.GetExecuteProcedureDataSet("SP_GetData_Client_CommandAlarmAction", projectId, userId, clientId);
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        /// <summary>
        /// The get confirmed alarm pending list.
        /// </summary>
        /// <returns>
        /// The data set of get data.
        /// </returns>
        public DataSet GetConfirmedAlarmPendingList()
        {
            try
            {
                return DataManager.GetInstance.DataHandler.GetExecuteProcedureDataSet("SP_GetData_Client_ConfirmedAlarmPendingList");
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        /// <summary>
        /// The get pending list table.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The data set of get data.
        /// </returns>
        public DataSet GetPendingListTable(string id)
        {
            try
            {
                return DataManager.GetInstance.DataHandler.GetExecuteProcedureDataSet("SP_GetData_Client_PendingList", id);
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        /// <summary>
        /// The alarm data by pending list.
        /// </summary>
        /// <param name="pendingListId">
        /// The pending list id.
        /// </param>
        /// <returns>
        /// The data set of get data.
        /// </returns>
        public DataSet GetAlarmDataByPendingList(string pendingListId)
        {
            try
            {
                return DataManager.GetInstance.DataHandler.GetExecuteProcedureDataSet("SP_GetData_Client_AlarmDataByPendingList");
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        /// <summary>
        /// The get command alarm action table.
        /// </summary>
        /// <param name="alarmId">
        /// The alarm id.
        /// </param>
        /// <returns>
        /// The data set of get data.
        /// </returns>
        public DataSet GetCommandAlarmActionTable(string alarmId)
        {
            try
            {
                return DataManager.GetInstance.DataHandler.GetExecuteProcedureDataSet("SP_GetData_Client_CommandAlarmActionByAlarmId", alarmId);
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        /// <summary>
        /// The get alarm table.
        /// </summary>
        /// <param name="alarmId">
        /// The alarm id.
        /// </param>
        /// <returns>
        /// The data set of get data.
        /// </returns>
        public DataSet GetAlarmInfoByAlarmId(string alarmId)
        {
            try
            {
                return DataManager.GetInstance.DataHandler.GetExecuteProcedureDataSet("SP_GetData_Client_Alarm", alarmId);
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        /// <summary>
        /// The get all pending list count.
        /// </summary>
        /// <returns>
        /// The list count.
        /// </returns>
        public int GetAllPendingListCount()
        {
            try
            {
                var dataSet = DataManager.GetInstance.DataHandler.GetExecuteProcedureDataSet("SP_GetData_Client_AlarmPendingList");

                return dataSet == null ? 0 : dataSet.Tables[0].Rows.Count;
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return 0;
            }
        }

        /// <summary>
        /// The get unconfirmed pending list count.
        /// </summary>
        /// <returns>
        /// The unconfirmd pending list count.
        /// </returns>
        public int GetUnconfirmedPendingListCount()
        {
            try
            {
                var dataSet = DataManager.GetInstance.DataHandler.GetExecuteProcedureDataSet("SP_GetData_Client_UnconfirmedPendingListCount");

                return dataSet == null ? 0 : dataSet.Tables[0].Rows.Count;
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return 0;
            }
        }

        /// <summary>
        /// The get alarm received clients info.
        /// 알람을 수신할 Client의 정보를 가져온다.
        /// 알람은 CM과 VW만 수신한다.
        /// </summary>
        /// <returns>The result DataSet.</returns>
        public DataSet GetAlarmReceivedClientsInfo()
        {
            try
            {
                return DataManager.GetInstance.DataHandler.GetExecuteDataSet("SP_GetData_Client_AlarmReceivedClientsInfo");
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        /// <summary>
        /// The get pending info.
        /// 현재 Alarm으로 전송해야할 PendingList를 가져온다.
        /// </summary>
        /// <param name="clientId">
        /// The PendingList 의 ProjectID.
        /// </param>
        /// <param name="requestpage">
        /// The request page.
        /// </param>
        /// <param name="needcount">
        /// The need count (한 번에 표시해야하는 PendingList 개수).
        /// </param>
        /// <returns>
        /// The result DataSet.
        /// </returns>
        public DataSet GetPendingInfo(string clientId, int requestpage, int needcount)
        {
            try
            {
                return DataManager.GetInstance.DataHandler.GetExecuteProcedureDataSet("SP_GetData_Client_PendingListToAlarm", clientId, requestpage, needcount);
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        public DataSet GetExternalCommand(string executeType, string pendingListId)
        {

            var query = new StringBuilder();

            query.Append(string.Format("SELECT ET.{0}, EC.ExternalCommandText FROM AlarmPendingList AS AP   ", executeType ?? string.Empty));
            query.Append("  INNER JOIN AlarmExternalCommandMapping AS AEM                                   ");
            query.Append("  ON AP.AlarmID = AEM.AlarmID                                                     ");
            query.Append("  INNER JOIN ExternalCommandTemplate AS ET                                        ");
            query.Append("  ON AEM.ExternalCommandTemplateID = ET.ExternalCommandTemplateID                 ");
            query.Append("  INNER JOIN ExternalCommand AS EC                                                ");
            query.Append(string.Format("ON ET.{0} = EC.ExternalCommandID ", executeType));
            query.Append("WHERE AP.AlarmPendingListID = @PendingListId                                      ");

            var paramInfo = new List<ParamInfo>
                            {
                                new ParamInfo("@PendingListId", DbType.String, CommonUtil.ConvertToDbTypeString(pendingListId)),
                            };


            return DataManager.GetInstance.DataHandler.GetExecuteDataSet(query.ToString(), paramInfo);
        }

        /// <summary>
        /// The get client event service url.
        /// </summary>
        /// <param name="client">
        /// The client.
        /// </param>
        /// <returns>
        /// The data set of get data.
        /// </returns>
        public string GetClientEventServiceUrl(string client)
        {
            try
            {
                var dataSet = DataManager.GetInstance.DataHandler.GetExecuteProcedureDataSet("SP_GetData_Client_ClientEventServiceUrl", client);

                return dataSet.Tables[0].Rows[0]["ServiceUrl"].ToString();
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return string.Empty;
            }
        }

        /// <summary>
        /// The get ptz information list.
        /// </summary>
        /// <param name="projectId">
        /// The project id.
        /// </param>
        /// <returns>
        /// The datatable of ptz information list.
        /// </returns>
        public DataTable GetPtzInformationList(string projectId)
        {
            try
            {
                return DataManager.GetInstance.DataHandler.GetExecuteProcedureDataSet("SP_GetData_Client_PtzInformationList", projectId).Tables[0];
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        /// <summary>
        /// The get camera controller info.
        /// </summary>
        /// <param name="projectId">
        /// The project id.
        /// </param>
        /// <returns>
        /// The datatable of multi camera controller info.
        /// </returns>
        public DataTable GetCameraControllerInfo(string projectId)
        {
            try
            {
                return DataManager.GetInstance.DataHandler.GetExecuteProcedureDataSet("SP_GetData_Client_CameraControllerInfo", projectId).Tables[0];
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        /// <summary>
        /// The get multi external command address info.
        /// </summary>
        /// <param name="externalCommandId">
        /// The external Command Id.
        /// </param>
        /// <returns>
        /// The datatable of multi external command view server id.
        /// </returns>
        public DataTable GetMultiExternalCommandUrlInfo(string externalCommandId)
        {
            try
            {
                return DataManager.GetInstance.DataHandler.GetExecuteProcedureDataSet("SP_GetData_Client_MultiExternalCommandAddressInfo", externalCommandId).Tables[0];
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        /// <summary>
        /// The select lost child camera group.
        /// </summary>
        /// <returns>The dataset of lost child camera group.</returns>
        public DataSet SelectLostChildCameraGroup()
        {
            try
            {
                var query = new StringBuilder();

                query.Append("SELECT *                      ");
                query.Append("FROM CameraGroup              ");
                query.Append("WHERE ParentID                ");
                query.Append("  NOT IN(                     ");
                query.Append("      SELECT CameraGroupID    ");
                query.Append("      FROM CameraGroup		");
                query.Append("  ) AND ParentID <> -1        ");

                return DataManager.GetInstance.DataHandler.GetExecuteDataSet(query.ToString(), null);
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        /// <summary>
        /// The get nvr event data.
        /// </summary>
        /// <param name="alarmId">
        /// The alarm id.
        /// </param>
        /// <returns>
        /// The System.Data.DataTable.
        /// </returns>
        public DataTable GetNvrEventData(string alarmId)
        {
            try
            {
                return DataManager.GetInstance.DataHandler.GetExecuteProcedureDataSet("SP_GetData_Playback_NvrEventData", alarmId).Tables[0];
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.ToString());
                return null;
            }
        }

        /// <summary>
        /// The get recorder ip by view server type.
        /// </summary>
        /// <returns>
        /// The <see cref="DataTable"/>.
        /// </returns>
        public DataTable GetRecorderIpByViewServerType()
        {
            try
            {
                return
                    DataManager.GetInstance.DataHandler.GetExecuteProcedureDataSet(
                        "SP_GetData_Recorder_RecorderIpByViewServerType", "RECORDER").Tables[0];
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.ToString());
                return null;
            }
        }

        #region Methods

        /// <summary>
        /// The select favorite id.
        /// </summary>
        /// <param name="projectID">
        /// The project ID.
        /// </param>
        /// <param name="userID">
        /// The user ID.
        /// </param>
        /// <param name="type">
        /// The type.
        /// </param>
        /// <returns>
        /// The data set of get data.
        /// </returns>
        private DataSet SelectFavoriteID(string projectID, string userID, string type)
        {
            try
            {
                return DataManager.GetInstance.DataHandler.GetExecuteProcedureDataSet("SP_GetData_Client_FavoriteItemIds", projectID, userID, type);
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        private string ExecuteGetUserInfoForPrivilege(DataSet getExecuteDataSet, string userId, string password)
        {
            if (
                getExecuteDataSet.Tables[0].Rows.Cast<DataRow>().Any(
                    row =>
                    row["UserID"].ToString().Equals(userId.ToLower()) && row["Password"].ToString().Equals(password)))
            {
                var data = getExecuteDataSet.Tables[0].Rows.Cast<DataRow>().First(
                            row =>
                            row["UserID"].ToString().Equals(userId.ToLower()) && row["Password"].ToString().Equals(password));

                if (data != null)
                {
                    var sync = data["Sync"].ToString();
                    var rdsControl = data["RdsControl"].ToString();
                    var ptzControl = data["PtzControl"].ToString();
                    var favoriteSave = data["FavoriteSave"].ToString();
                    var resourceCamera = data["ResourceCamera"].ToString();
                    var resourceLayout = data["ResourceLayout"].ToString();
                    var resourceMap = data["ResourceMap"].ToString();
                    var resourceImage = data["ResourceImage"].ToString();
                    var playbackExport = data["PlaybackExport"].ToString();

                    return this.SetXmlToUserInfo("success", sync, rdsControl, ptzControl, favoriteSave, resourceCamera, resourceLayout, resourceMap, resourceImage, playbackExport).InnerXml;
                }
            }

            if (
                getExecuteDataSet.Tables[0].Rows.Cast<DataRow>().Any(
                    row =>
                    row["UserID"].ToString().Equals(userId.ToLower()) && !row["Password"].ToString().Equals(password)))
            {
                return this.SetXmlToUserInfo("incorrect password").InnerXml;
            }

            // 권한이 혹시 없으면
            const string query = "SELECT * FROM [User]";

            return this.ExecuteGetUserInfo(
                DataManager.GetInstance.DataHandler.GetExecuteDataSet(query), userId, password);
        }

        private string ExecuteGetUserInfo(DataSet getExecuteDataSet, string userId, string password)
        {
            if (
                getExecuteDataSet.Tables[0].Rows.Cast<DataRow>().Any(
                    row =>
                    row["UserID"].ToString().Equals(userId.ToLower()) && row["Password"].ToString().Equals(password)))
            {
                return this.SetXmlToUserInfo("invalid", "False", "False", "False", "False", "False", "False", "False", "False", "False").InnerXml;
            }

            if (getExecuteDataSet.Tables[0].Rows.Cast<DataRow>().Any(row => row["UserID"].ToString().Equals(userId.ToLower()) && !row["Password"].ToString().Equals(password)))
            {
                return this.SetXmlToUserInfo("incorrect password").InnerXml;
            }

            return this.SetXmlToUserInfo("none userid").InnerXml;
        }

        private XmlDocument SetXmlToUserInfo(string result, string sync, string rdsControl, string ptzControl, string favoriteSave, string resourceCamera, string resourceLayout,
            string resourceMap, string resourceImage, string playbackExport)
        {
            var xmlDocument = new XmlDocument();

            var xmlDeclaration = xmlDocument.CreateXmlDeclaration("1.0", "utf-8", null);
            xmlDocument.AppendChild(xmlDeclaration);

            var root = xmlDocument.CreateElement("Login");
            xmlDocument.AppendChild(root);

            var elementStatus = xmlDocument.CreateElement("Status");
            root.AppendChild(elementStatus);

            var attributeValue = xmlDocument.CreateAttribute("value");
            attributeValue.Value = result;
            elementStatus.Attributes.Append(attributeValue);

            var elementPrivilege = xmlDocument.CreateElement("Privilege");
            root.AppendChild(elementPrivilege);

            var attributePrivilege = xmlDocument.CreateAttribute("Sync");
            attributePrivilege.Value = sync;
            elementPrivilege.Attributes.Append(attributePrivilege);

            attributePrivilege = xmlDocument.CreateAttribute("RdsControl");
            attributePrivilege.Value = rdsControl;
            elementPrivilege.Attributes.Append(attributePrivilege);

            attributePrivilege = xmlDocument.CreateAttribute("PtzControl");
            attributePrivilege.Value = ptzControl;
            elementPrivilege.Attributes.Append(attributePrivilege);

            attributePrivilege = xmlDocument.CreateAttribute("FavoriteSave");
            attributePrivilege.Value = favoriteSave;
            elementPrivilege.Attributes.Append(attributePrivilege);

            attributePrivilege = xmlDocument.CreateAttribute("ResourceCamera");
            attributePrivilege.Value = resourceCamera;
            elementPrivilege.Attributes.Append(attributePrivilege);

            attributePrivilege = xmlDocument.CreateAttribute("ResourceLayout");
            attributePrivilege.Value = resourceLayout;
            elementPrivilege.Attributes.Append(attributePrivilege);

            attributePrivilege = xmlDocument.CreateAttribute("ResourceMap");
            attributePrivilege.Value = resourceMap;
            elementPrivilege.Attributes.Append(attributePrivilege);

            attributePrivilege = xmlDocument.CreateAttribute("ResourceImage");
            attributePrivilege.Value = resourceImage;
            elementPrivilege.Attributes.Append(attributePrivilege);

            attributePrivilege = xmlDocument.CreateAttribute("PlaybackExport");
            attributePrivilege.Value = playbackExport;
            elementPrivilege.Attributes.Append(attributePrivilege);

            return xmlDocument;
        }

        private XmlDocument SetXmlToUserInfo(string result)
        {
            var xmlDocument = new XmlDocument();

            var xmlDeclaration = xmlDocument.CreateXmlDeclaration("1.0", "utf-8", null);
            xmlDocument.AppendChild(xmlDeclaration);

            var root = xmlDocument.CreateElement("Login");
            xmlDocument.AppendChild(root);

            var elementStatus = xmlDocument.CreateElement("Status");
            root.AppendChild(elementStatus);

            var attributeValue = xmlDocument.CreateAttribute("value");
            attributeValue.Value = result;
            elementStatus.Attributes.Append(attributeValue);

            return xmlDocument;
        }

        #endregion // Methods

        #endregion // Client

        #region Playback

        /// <summary>
        /// The get camera info.
        /// </summary>
        /// <param name="projectId">
        /// The project id.
        /// </param>
        /// <returns>
        /// The xml to camera info data set.
        /// </returns>
        public string GetCameraInfo(string projectId)
        {
            try
            {
                return CommonUtil.XmlToDataSet(DataManager.GetInstance.DataHandler.GetExecuteProcedureDataSet("SP_GetData_Playback_CameraInfo", projectId));
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        /// <summary>
        /// The get resolution info.
        /// </summary>
        /// <param name="projectId">
        /// The project id.
        /// </param>
        /// <returns>
        /// The xml to resolution info.
        /// </returns>
        public string GetResolutionInfo(string projectId)
        {
            try
            {
                return CommonUtil.XmlToDataSet(DataManager.GetInstance.DataHandler.GetExecuteProcedureDataSet("SP_GetData_Playback_ResolutionInfo", projectId));
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Te get record media info.
        /// </summary>
        /// <param name="beginDate">
        /// The begin date.
        /// </param>
        /// <param name="endDate">
        /// The end date.
        /// </param>
        /// <param name="cameras">
        /// The cameras.
        /// </param>
        /// <returns>
        /// The xml to record media info.
        /// </returns>
        public DataSet GetRecordMediaInfo(string beginDate, string endDate, List<string> cameras)
        {
            try
            {
                var dataSet = new DataSet();

                foreach (var recordCameraNumber in cameras)
                {
                    var dataTable = DataManager.GetInstance.DataHandler.GetExecuteProcedureDataSet("SP_GetData_Playback_RecordMediaInfo", recordCameraNumber, beginDate, endDate).Tables[0].Copy();
                    dataTable.TableName = string.Format("RecordCamera{0}DataTable", recordCameraNumber);

                    dataSet.Tables.Add(dataTable);
                }

                return dataSet;
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        /// <summary>
        /// The get record media date info.
        /// </summary>
        /// <param name="beginDate">
        ///   The begin date.
        /// </param>
        /// <param name="endDate">
        ///   The end date.
        /// </param>
        /// <param name="cameras">
        ///   The cameras.
        /// </param>
        /// <param name="recordIntervalSeconds">
        /// The record interval seconds.
        /// </param>
        /// <param name="eventIntervalSeconds"> 
        /// The event interval seconds.
        /// </param>
        /// <returns>
        /// The xml to record media date info.
        /// </returns>
        public DataSet GetRecordMediaDateInfo(string beginDate, string endDate, List<string> cameras, string recordIntervalSeconds, string eventIntervalSeconds)
        {
            var dataSet = new DataSet();

            foreach (var recordCameraNumber in cameras)
            {
                var dataTable = DataManager.GetInstance.DataHandler.GetExecuteProcedureDataSet("SP_GetData_Playback_TimelineData", recordCameraNumber, beginDate, endDate, recordIntervalSeconds).Tables[0].Copy();
                dataTable.TableName = string.Format("RecordCamera{0}DateDataTable", recordCameraNumber);

                dataSet.Tables.Add(dataTable);
            }

            if (!string.IsNullOrWhiteSpace(eventIntervalSeconds))
            {
                // Get EventTimeLine .
                // CameraNumberList를 ","로 묶는다.
                var cameraNumberArray =
                    cameras.Aggregate(string.Empty, (current, camera) => current + string.Format("{0},", camera));

                if (!string.IsNullOrWhiteSpace(cameraNumberArray))
                {
                    var eventDataTable =
                        DataManager.GetInstance.DataHandler.GetExecuteProcedureDataSet(
                            "SP_GetData_Playback_EventTimelineData",
                            cameraNumberArray,
                            beginDate,
                            endDate,
                            eventIntervalSeconds).Tables[0].Copy();
                    eventDataTable.TableName = "EventTimeLineTable";
                    dataSet.Tables.Add(eventDataTable);
                }
            }

            return dataSet;
        }

        public DataTable GetEventRecordDateInfo(string beginDate, string endDate, List<string> cameras, string eventIntervalSeconds)
        {
            if (!string.IsNullOrWhiteSpace(eventIntervalSeconds))
            {
                // Get EventTimeLine .
                // CameraNumberList를 ","로 묶는다.
                var cameraNumberArray =
                    cameras.Aggregate(string.Empty, (current, camera) => current + string.Format("{0},", camera));

                if (!string.IsNullOrWhiteSpace(cameraNumberArray))
                {
                    var eventDataTable =
                        DataManager.GetInstance.DataHandler.GetExecuteProcedureDataSet(
                            "SP_GetData_Playback_EventTimelineData",
                            cameraNumberArray,
                            beginDate,
                            endDate,
                            eventIntervalSeconds).Tables[0].Copy();
                    eventDataTable.TableName = "EventTimeLineTable";
                    return eventDataTable;
                }
            }

            return null;
        }

        /// <summary>
        /// The get search record
        /// </summary>
        /// <param name="cameras">
        /// The cameras.
        /// </param>
        /// <param name="mergeIntervalSeconds">
        /// The merge interval seconds.
        /// </param>
        /// <returns>
        /// The xml to search record time info.
        /// </returns>
        public string GetSearchRecordTimeInfo(List<string> cameras, string mergeIntervalSeconds)
        {
            var dataSet = new DataSet();

            foreach (var recordCameraNumber in cameras)
            {
                var dataTable = DataManager.GetInstance.DataHandler.GetExecuteProcedureDataSet("SP_GetData_Playback_SearchRecordTimeData", recordCameraNumber, mergeIntervalSeconds).Tables[0].Copy();
                dataTable.TableName = string.Format("RecordCamera{0}DateDataTable", recordCameraNumber);

                dataSet.Tables.Add(dataTable);
            }

            return CommonUtil.XmlToDataSet(dataSet);
        }

        /// <summary>
        /// The get alarm type info.
        /// </summary>
        /// <returns>The xml to alarm type.</returns>
        public string GetAlarmTypeInfo()
        {
            try
            {
                return CommonUtil.XmlToDataSet(DataManager.GetInstance.DataHandler.GetExecuteProcedureDataSet("SP_GetData_Playback_AlarmTypeInfo"));
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return ex.Message;
            }
        }

        public DataSet GetAlarmTypeInfoForDataSet()
        {
            try
            {
                return DataManager.GetInstance.DataHandler.GetExecuteProcedureDataSet("SP_GetData_Playback_AlarmTypeInfo");
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        /// <summary>
        /// The get alarm list for playback.
        /// </summary>
        /// <param name="cameraName">
        /// The camera name.
        /// </param>
        /// <param name="alarmType">
        /// The alarm type.
        /// </param>
        /// <param name="level">
        /// The level.
        /// </param>
        /// <param name="fromDateTime">
        /// The from date time.
        /// </param>
        /// <param name="toDateTime">
        /// The to date time.
        /// </param>>
        /// <param name="index">
        /// The index.
        /// </param>
        /// <param name="pageCount">
        /// The page Count.
        /// </param>
        /// <returns>
        /// The xml string of alarm list.
        /// </returns>
        public DataTable GetAlarmListForPlayback(string cameraName, string alarmType, string level, string fromDateTime, string toDateTime, int index, int pageCount)
        {
            try
            {
                return
                DataManager.GetInstance.DataHandler.GetExecuteProcedureDataSet(
                    "SP_GetData_Playback_AlarmList",
                    CommonUtil.ConvertToDbTypeString(cameraName),
                    CommonUtil.ConvertToDbTypeString(alarmType),
                    CommonUtil.ConvertToDbTypeString(level),
                    CommonUtil.ConvertToDbTypeString(fromDateTime),
                    CommonUtil.ConvertToDbTypeString(toDateTime),
                    index,
                    pageCount).Tables[0];
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.ToString());
                return null;
            }
        }

        /// <summary>
        /// The get project camera if by record camera number.
        /// </summary>
        /// <param name="recordCameraNumber">
        /// The record camera number.
        /// </param>
        /// <returns>
        /// The project camera id.
        /// </returns>
        public DataTable GetProjectCameraIdByRecordCameraNumber(string recordCameraNumber)
        {
            try
            {
                return DataManager.GetInstance.DataHandler.GetExecuteProcedureDataSet("SP_GetData_Playback_ProjectCameraIdByRecordCameraNumber", int.Parse(recordCameraNumber)).Tables[0];
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        /// <summary>
        /// The get event record calendar data.
        /// </summary>
        /// <param name="camera">
        /// The camera.
        /// </param>
        /// <param name="beginDate">
        /// The begin date.
        /// </param>
        /// <param name="endDate">
        /// The end date.
        /// </param>
        /// <returns>
        /// The <see cref="DataSet"/>.
        /// </returns>
        public DataSet GetEventRecordCalendarData(int camera, string beginDate, string endDate)
        {
            try
            {
                return
                    DataManager.GetInstance.DataHandler.GetExecuteProcedureDataSet(
                        "SP_GetData_Playback_EventRecordCalendarData", camera, beginDate, endDate);
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.ToString());
                return null;
            }
        }

        #endregion // Playback

        #region Recorder

        /// <summary>
        /// The get recorder info.
        /// Alarm 발생 시 해당 Camera 와 연결 된 Recorder 에게 영상 저장 요청을 위한 필요한 정보를 가지고 온다.
        /// </summary>
        /// <param name="alarmId">
        /// The alarm id.
        /// </param>
        /// <returns>
        /// The get recorder info.
        /// </returns>
        public DataSet GetRecorderInfo(string alarmId)
        {
            try
            {
                return DataManager.GetInstance.DataHandler.GetExecuteProcedureDataSet("SP_GetData_Recorder_RecorderInfo", alarmId);
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        public string GetVideoInformationForNvr(string recorderId, string recordcameranumber)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(recordcameranumber) || string.Compare("-1", recordcameranumber) == 0)
                {
                    var resultData = DataManager.GetInstance.DataHandler.GetExecuteProcedureDataSet("SP_GetData_Recorder_VideoInformation", recorderId);

                    foreach (DataRow dataRow in resultData.Tables[0].Rows)
                    {
                        dataRow["RtspUrl"] = this.ReplaceRtspUrl(dataRow["RtspUrl"].ToString(), dataRow["CameraIP"].ToString(), dataRow["Channel"].ToString());
                    }

                    return CommonUtil.XmlToDataSet(resultData);
                }
                else
                {
                    var resultData = DataManager.GetInstance.DataHandler.GetExecuteProcedureDataSet("SP_GetData_Recorder_VideoInformationByRecordCameraNumber", recorderId, recordcameranumber);

                    foreach (DataRow dataRow in resultData.Tables[0].Rows)
                    {
                        dataRow["RtspUrl"] = this.ReplaceRtspUrl(dataRow["RtspUrl"].ToString(), dataRow["CameraIP"].ToString(), dataRow["Channel"].ToString());
                    }

                    return CommonUtil.XmlToDataSet(resultData);
                }

            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        private string ReplaceRtspUrl(string rtspUrl, string cameraIp, string channel)
        {
            var tempUrl = rtspUrl.Replace("{IP}", cameraIp);

            return tempUrl.Replace("{CH}", channel);
        }

        /// <summary>
        /// The get view server by view server ip.
        /// </summary>
        /// <param name="recorderIp">
        /// The recorder ip.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string GetRecorderByIp(string recorderIp)
        {
            try
            {
                return
                    CommonUtil.XmlToDataSet(DataManager.GetInstance.DataHandler.GetExecuteProcedureDataSet(
                        "SP_GetData_Recorder_RecorderByIp", recorderIp));
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.ToString());
                return null;
            }
        }

        /// <summary>
        /// The get record schedule by record schedule number.
        /// </summary>
        /// <param name="recordScheduleNumber">
        /// The record schedule number.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string GetRecordScheduleByRecordScheduleNumber(int recordScheduleNumber)
        {
            try
            {
                return
                    CommonUtil.XmlToDataSet(DataManager.GetInstance.DataHandler.GetExecuteProcedureDataSet(
                        "SP_GetData_Recorder_RecordScheduleByRecordScheduleNumbe", recordScheduleNumber));
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.ToString());
                return null;
            }
        }

        #endregion // Recorder

        #region Log

        /// <summary>
        /// The get used alarm log.
        /// </summary>
        /// <param name="programType">
        /// The program type.
        /// </param>
        /// <param name="logType">
        /// The log type.
        /// </param>
        /// <returns>
        /// The result bool type data.
        /// </returns>
        public bool GetUsedAlarmLog(string programType, string logType)
        {
            try
            {
                var usedAlarmLogDataSet = DataManager.GetInstance.DataHandler.GetExecuteProcedureDataSet("SP_GetData_Client_UsedAlarmLog", programType, logType);

                if (!usedAlarmLogDataSet.Tables[0].Rows.Count.Equals(0))
                {
                    return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return false;
            }
        }

        /// <summary>
        /// The get log external command.
        /// </summary>
        /// <param name="programType">
        /// The program type.
        /// </param>
        /// <param name="logType">
        /// The log type.
        /// </param>
        /// <returns>
        /// The mapping data set.
        /// </returns>
        public DataSet GetLogExternalCommand(string programType, string logType)
        {
            try
            {
                return DataManager.GetInstance.DataHandler.GetExecuteProcedureDataSet("SP_GetData_Client_LogExternalCommand", programType, logType);
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        #endregion // Log

        #region URL Capture

        /// <summary>
        /// The get url capture server info.
        /// </summary>
        /// <returns>
        /// The System.String.
        /// </returns>
        public string GetUrlCaptureServerInfo()
        {
            try
            {
                var dataSet = this.GetUrlCaptureServerInfoByViewServer();

                if (dataSet == null)
                {
                    return string.Empty;
                }

                using (var writer = new StringWriter())
                {
                    dataSet.WriteXml(writer, XmlWriteMode.WriteSchema);

                    return writer.ToString();
                }
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }

        }

        private DataSet GetUrlCaptureServerInfoByViewServer()
        {
            try
            {
                return DataManager.GetInstance.DataHandler.GetExecuteProcedureDataSet("SP_GetData_UrlCapture_UrlCaptureServerInfo");
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        #endregion

        #region
       
        #endregion
        #endregion // GetData



        #region Methods

        private DataSet ExecuteQueryToGetMapLayoutTable(string projectId)
        {
            try
            {
                return DataManager.GetInstance.DataHandler.GetExecuteProcedureDataSet("SP_GetData_Client_MapLayout", projectId);
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        private string GetMapLayoutDataString(DataSet dataSet)
        {
            try
            {
                return dataSet == null ? string.Empty : dataSet.Tables[0].Rows[0][1].ToString();
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return string.Empty;
            }
        }

        /// <summary>
        /// MD에서 Camera 정보를 가져가기 전에 Camera Table의 각 row index를 재정렬 한다.
        /// MediaServerPosition 순서로 정렬하고  제일 처음부터 다시 정렬하여 값을 할당한다.
        /// </summary>
        /// <param name="mediaServerId">
        /// 해당 MediaServerID.
        /// </param>
        private void UpdateCameraMediaServerPosition(string mediaServerId)
        {
            try
            {
                // 1. 등록된 카메라 중 CameraVideoMapping에서 VideoSpec.RtspUrl 이 하나도 없는 카메라는 걸러낸다.
                // 2. 걸러낸 카메라 중 MediaServerPosition이 Null 이 아닌 것을 먼저 가져온다.
                // 3. 걸러낸 카메라 중 MediaServerPosition이 Null 인것을 가져와 2번 아래에 붙인다.
                // 4. 2번과 3번을 Union 한 테이블의 Row를 순번대로 가져온다.
                // 5. Row 순번 대로 Camera의 MediaServerPostion을 Update 한다.
                DataManager.GetInstance.DataHandler.GetExecuteProcedureDataSet("SP_SetData_Client_UpdateCameraMediaServerPosition", mediaServerId);
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return;
            }
        }

        private XElement CreateCameraElement(string mediaServerId)
        {
            var cameras = new XElement("cameras");

            var dataSet = this.GetCameraInfoList(mediaServerId);

            // 중복된 Row는 검사하지 않는다. Linq로 한 번에 다 찾는다.
            var currentCamera = string.Empty;

            foreach (DataRow dr in dataSet.Tables[0].Rows)
            {
                // 1. 같은 카메라에 등록된 여러 개의 VideoSpec을 가진 Row를 찾아온다.
                var id = dr["CameraID"].ToString();

                if (string.Compare(currentCamera, id) == 0)
                {
                    continue;
                }

                // Index가 없는 데이터는 건너뛴다.
                if (string.Compare(dr["MediaServerPosition"].ToString(), string.Empty) == 0)
                {
                    continue;
                }

                currentCamera = id;

                var sameRows =
                    dataSet.Tables[0].Rows.Cast<DataRow>().Where(
                        dataRow => string.Compare(dataRow["CameraID"].ToString(), id) == 0);

                // 2. 동일한 CameraID에서 VideoSpec에 명시된 url을 추출한다. 추출 후 high,middleurl로 할당한다.
                XElement highSourceUrl = null;
                XElement middleSourceUrl = null;
                foreach (DataRow dataRow in sameRows)
                {
                    if (string.IsNullOrWhiteSpace(dataRow["RtspUrl"].ToString()))
                    {
                        continue;
                    }

                    if (highSourceUrl == null)
                    {
                        highSourceUrl = new XElement("highsourceurl", dataRow["RtspUrl"]);
                        continue;
                    }

                    middleSourceUrl = new XElement("middlesourceurl", dataRow["RtspUrl"]);
                    break;
                }

                if (highSourceUrl == null)
                {
                    continue;
                }

                if (middleSourceUrl == null)
                {
                    middleSourceUrl = new XElement("middlesourceurl");
                }

                var currentIndex = new XElement("index", dr["MediaServerPosition"].ToString());
                var port = new XElement("port", dr["CameraPort"].ToString());
                var vendor = new XElement("vendor", dr["Vendor"].ToString());

                var authentication = new XElement(
                    "authentication", new XElement("id", dr["RtspID"]), new XElement("password", dr["RtspPassword"]));

                var camera = new XElement(
                    "camera", currentIndex, highSourceUrl, middleSourceUrl, port, vendor, authentication);

                cameras.Add(camera);
            }

            var sortedCamers =
                (from camera in cameras.Elements("camera") orderby (int)camera.Element("index") select camera).ToList();

            var returned = new XElement("cameras");
            foreach (var sortedCamer in sortedCamers)
            {
                returned.Add(sortedCamer);
            }

            return returned;
        }

        private DataSet GetCameraInfoList(string mediaServerId)
        {
            try
            {
                return DataManager.GetInstance.DataHandler.GetExecuteProcedureDataSet("SP_GetData_Client_CameraInfoList", CommonUtil.ConvertToDbTypeString(mediaServerId));
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        #endregion // Methods



        //--------------------------------------------------------------------------------------By Heo Test


        public DataSet GetCommonCode(string code)
        {
            try
            {
                var query = new StringBuilder();

                query.Append("SELECT *                      ");
                query.Append("FROM StatusCode                   ");
                query.Append("WHERE GroupCode = @code            ");

                var paramInfo = new List<ParamInfo>
                            {

                                new ParamInfo("@code", DbType.String, CommonUtil.ConvertToDbTypeString(code)),
                            };

                return DataManager.GetInstance.DataHandler.GetExecuteDataSet(query.ToString(), paramInfo);
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        public DataSet GetServerStatus()
        {
            try
            {
                return DataManager.GetInstance.DataHandler.GetExecuteProcedureDataSet("SP_GetData_Client_ServerStatus");
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }


        public DataSet GetCameraStatus()
        {
            try
            {
                return DataManager.GetInstance.DataHandler.GetExecuteProcedureDataSet("SP_GetData_Client_CameraStatus");
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        public DataSet GetAlarmRuleList()
        {
            try
            {
                var query = new StringBuilder();

                query.Append("SELECT *                      ");
                query.Append("FROM AlarmRule                   ");

                return DataManager.GetInstance.DataHandler.GetExecuteDataSet(query.ToString());
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }


        public DataSet GetAlarmRuleMapping()
        {
            try
            {
                return DataManager.GetInstance.DataHandler.GetExecuteProcedureDataSet("SP_GetData_Client_AlarmRuleMapping");
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        public DataSet GetServerStatusHistory(string StartTime, string EndTime, string SystemId)
        {
            try
            {
                return DataManager.GetInstance.DataHandler.GetExecuteProcedureDataSet("SP_GetData_Client_ServerStatusHistory", StartTime, EndTime, SystemId);
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        #region
        public DataSet GetMapXamlList()
        {
            string query = "SELECT * FROM MapXaml";

            try
            {
                return DataManager.GetInstance.DataHandler.GetExecuteDataSet(query);
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }
        #endregion



    }
}