﻿// -----------------------------------------------------------------------
// <copyright file="ParamInfo.cs" company="Microsoft">
// 
// </copyright>
// -----------------------------------------------------------------------

namespace InnowatchDataHandler.Commons
{
    using System.Data;

    /// <summary>
    /// ParamInfo class.
    /// </summary>
    public class ParamInfo
    {
        public ParamInfo(string name, DbType dbtype, object data)
        {
            this.Name = name;
            this.DbType = dbtype;
            this.Data = data;
        }

        /// <summary>
        /// Gets Name.
        /// </summary>
        public string Name { get; private set; }

        /// <summary>
        /// Gets DBType.
        /// </summary>
        public DbType DbType { get; private set; }

        /// <summary>
        /// Gets or sets Data.
        /// </summary>
        public object Data { get; set; }
    }
}
