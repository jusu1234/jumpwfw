﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InnowatchDataHandler.Commons
{
    public class TimeConverter
    {
        public static double ConvertLocalDateTimeToUnixTimeMiliSeconds(DateTime localDateTime)
        {
            var unixStartTime = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            var utcDateTime = localDateTime.ToUniversalTime();

            return Convert.ToDouble((utcDateTime - unixStartTime).TotalMilliseconds);
        }

        public static double ConvertLocalDateTimeToUnixTimeSeconds(DateTime localDateTime)
        {
            var unixStartTime = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            var utcDateTime = localDateTime.ToUniversalTime();

            return Convert.ToDouble((utcDateTime - unixStartTime).TotalSeconds);
        }

        public static DateTime ConvertUnixTimeMiliSecondsToLocalDateTime(double unixTimeMiliSeconds)
        {
            var unixStartTime = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

            return unixStartTime.AddMilliseconds(unixTimeMiliSeconds).ToLocalTime();
        }

        public static DateTime ConvertUnixTimeSecondsToLocalDateTime(double unixTimeMiliSeconds)
        {
            var unixStartTime = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

            return unixStartTime.AddSeconds(unixTimeMiliSeconds).ToLocalTime();
        }

        public static string GetDayOfWeek(DateTime dateTime)
        {
            switch (dateTime.DayOfWeek)
            {
                case DayOfWeek.Sunday:
                    return "SUN";
                case DayOfWeek.Monday:
                    return "MON";
                case DayOfWeek.Tuesday:
                    return "TUE";
                case DayOfWeek.Wednesday:
                    return "WED";
                case DayOfWeek.Thursday:
                    return "THU";
                case DayOfWeek.Friday:
                    return "FRI";
                case DayOfWeek.Saturday:
                    return "SAT";
            }

            return string.Empty;
        }

        public static string GetKoreanAMorPM(DateTime dateTime)
        {
            string result = string.Format("{0:tt}", dateTime);

            if (result.ToUpper() == "AM".ToUpper())
                return "오전";
            else if (result.ToUpper() == "PM".ToUpper())
                return "오후";

            return result;
        }

        public static string GetEnglishAMorPM(DateTime dateTime)
        {
            return string.Format("{0:tt}", dateTime);
        }
    }
}
