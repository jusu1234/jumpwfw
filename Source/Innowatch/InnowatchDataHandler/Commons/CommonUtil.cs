﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CommonUtil.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Defines the ResponseType type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Diagnostics;
using InnowatchServiceLog;

namespace InnowatchDataHandler.Commons
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.IO;
    using System.Text;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// The response type.
    /// </summary>
    public enum ResponseType
    {
        /// <summary>
        /// The success.
        /// </summary>
        Success,

        /// <summary>
        /// The failed.
        /// </summary>
        Failed
    }

    /// <summary>
    /// The commonutil.
    /// </summary>
    public static class CommonUtil
    {
        #region public Methods

        public static string Check(string url)
        {
            string val = url;

            if (!url.EndsWith("/"))
            {
                val = val + "/";
            }

            if (!url.StartsWith("http://"))
            {
                val = "http://" + val;
            }

            return val;
        }

        /// <summary>
        /// String type 의 데이터가 null 일 경우, DBNull 로 변환.
        /// </summary>
        /// <param name="data">
        /// The data.
        /// </param>
        /// <returns>
        /// The DBNull.
        /// </returns>
        public static object ConvertToDbTypeString(string data)
        {
            if (string.IsNullOrWhiteSpace(data))
            {
                return DBNull.Value;
            }

            return data;
        }

        /// <summary>
        /// The xml to dataset.
        /// </summary>
        /// <param name="getExecuteDataSet">The dataset.</param>
        /// <returns>The xml to dataset.</returns>
        public static string XmlToDataSet(DataSet getExecuteDataSet)
        {
            var stringWriteData = new StringWriter();

            if (getExecuteDataSet != null)
            {
                getExecuteDataSet.WriteXml(stringWriteData, XmlWriteMode.WriteSchema);
            }

            return stringWriteData.ToString();
        }

        /// <summary>
        /// DB의 특정 테이블에 해당 값을 가진 Row가 있는 지 확인한다.
        /// </summary>
        /// <param name="tableName">
        /// 검색을 원하는 Table명.
        /// </param>
        /// <param name="columnName">
        /// 검색을 원하는 Column 명.
        /// </param>
        /// <param name="checkValue">
        /// 검색을 원하는 문자열.
        /// </param>
        /// <returns>
        /// 동일한 값을 가진 Row가 있으면 true.
        /// </returns>
        public static bool CheckExistTuple(string tableName, string columnName, string checkValue)
        {
            try
            {
                var query = CreateSelectQuery(tableName, columnName);
                var paramInfo = CreateSelectQueryParam(DbType.String, checkValue);

                return CheckTuples(query, paramInfo);
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return false;
            }
        }

        /// <summary>
        /// DB의 특정 테이블에 해당 값을 가진 Row가 있는 지 확인한다.
        /// </summary>
        /// <param name="tableName">검색을 원하는 Table명.</param>
        /// <param name="columnName">검색을 원하는 Column 명.</param>
        /// <param name="checkValue">검색을 원하는 Boolean.</param>
        /// <returns>동일한 값을 가진 Row가 있으면 true.</returns>
        public static bool CheckExistTuple(string tableName, string columnName, bool checkValue)
        {
            try
            {
                var query = CreateSelectQuery(tableName, columnName);
                var paramInfo = CreateSelectQueryParam(DbType.Boolean, checkValue);

                return CheckTuples(query, paramInfo);
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return false;
            }
        }

        /// <summary>
        /// DB의 특정 테이블에 해당 값을 가진 Row가 있는 지 확인한다.
        /// </summary>
        /// <param name="tableName">검색을 원하는 Table명.</param>
        /// <param name="columnName">검색을 원하는 Column 명.</param>
        /// <param name="checkValue">검색을 원하는 정수값.</param>
        /// <returns>동일한 값을 가진 Row가 있으면 true.</returns>
        public static bool CheckExistTuple(string tableName, string columnName, int checkValue)
        {
            try
            {
                var query = CreateSelectQuery(tableName, columnName);
                var paramInfo = CreateSelectQueryParam(DbType.Int32, checkValue);

                return CheckTuples(query, paramInfo);
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return false;
            }
        }

        public static Stream ResponseData(ResponseType responseType, string message = null)
        {
            switch (responseType)
            {
                case ResponseType.Success:
                    {
                        return SetResponseData("success");
                    }

                case ResponseType.Failed:
                    {
                        return SetResponseData("failed", message);
                    }
            }

            return null;
        }

        /// <summary>
        /// 해당 객체를 Xml로 Serialize 한다.
        /// </summary>
        /// <typeparam name="T">Serialize 할 객체의 타입.</typeparam>
        /// <param name="param">Serialize 할 객체.</param>
        /// <returns>The result Xml string.</returns>
        public static string SerializeToXml<T>(T param)
        {
            try
            {
                var serializer = new XmlSerializer(typeof(T));
                var memStream = new MemoryStream();

                var settings = new XmlWriterSettings();
                settings.Indent = true;
                settings.IndentChars = new string(' ', 4);
                settings.NewLineOnAttributes = false;
                settings.Encoding = Encoding.UTF8;

                XmlWriter xmlWriter = XmlWriter.Create(memStream, settings);
                serializer.Serialize(xmlWriter, param);
                xmlWriter.Close();
                memStream.Close();

                string xmlData = Encoding.UTF8.GetString(memStream.GetBuffer());
                xmlData = xmlData.Substring(xmlData.IndexOf('<'));
                xmlData = xmlData.Substring(0, xmlData.LastIndexOf('>') + 1);

                return xmlData;
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
            
        }

        /// <summary>
        /// 해당 Xml을 지정된 객체로 Deserialize 한다.
        /// </summary>
        /// <typeparam name="T">Deserialize 할 타입.</typeparam>
        /// <param name="xmlData">Deserialize 할 Xml 문자열.</param>
        /// <returns>Deserialize 된 객체.</returns>
        public static T DeserializeFromXml<T>(string xmlData) where T : class
        {
            try
            {
                var serializer = new XmlSerializer(typeof(T));

                var stringReader = new StringReader(xmlData);
                var xmlReader = new XmlTextReader(stringReader);

                var deserializedObject = serializer.Deserialize(xmlReader) as T;

                xmlReader.Close();
                stringReader.Close();

                return deserializedObject;
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        #endregion

        #region private Methods

        private static bool CheckTuples(string query, List<ParamInfo> paramInfo)
        {
            var dataSet = DataManager.GetInstance.DataHandler.GetExecuteDataSet(query, paramInfo);

            if (dataSet.Tables.Count == 0)
                return false;

            return dataSet.Tables[0].Rows.Count > 0;
        }

        private static string CreateSelectQuery(string tableName, string columnName)
        {
            var query = new StringBuilder();
            query.Append("SELECT *   ");
            query.Append(string.Format("FROM {0} ", tableName));
            query.Append(string.Format("WHERE {0} = @CheckValue", columnName));

            return query.ToString();
        }

        private static List<ParamInfo> CreateSelectQueryParam(DbType type, object checkValue)
        {
            var paramInfo = new List<ParamInfo>
                                {
                                    new ParamInfo("@CheckValue", type, checkValue),
                                };

            return paramInfo;
        }

        private static Stream SetResponseData(string value, string message = null)
        {
            var xmlDocument = new XmlDocument();
            var xmlDec = xmlDocument.CreateXmlDeclaration("1.0", "utf-8", null);
            xmlDocument.AppendChild(xmlDec);

            var elementResponseData = xmlDocument.CreateElement("ResponseData");
            xmlDocument.AppendChild(elementResponseData);

            var elementResult = xmlDocument.CreateElement("Result");
            elementResponseData.AppendChild(elementResult);

            var attributeValue = xmlDocument.CreateAttribute("value");
            attributeValue.Value = value;
            elementResult.Attributes.Append(attributeValue);

            if (!string.IsNullOrEmpty(message))
            {
                var attributeMessage = xmlDocument.CreateAttribute("message");
                attributeMessage.Value = message;
                elementResult.Attributes.Append(attributeMessage);
                Logger.Write(Logger.Error, message);
            }

            var bytes = Encoding.UTF8.GetBytes(xmlDocument.InnerXml);

            return new MemoryStream(bytes);
        }

        #endregion
    }

    public class PerformanceTimer : IDisposable
    {
        private double _startStemp;

        private string _message;

        public PerformanceTimer(string message)
        {
            this._message = message;
            this._startStemp = Stopwatch.GetTimestamp();
        }

        public void Dispose()
        {
            var elapsed = (Stopwatch.GetTimestamp() - _startStemp) / Stopwatch.Frequency;
            var result = string.Format("{0} : Elapsed Time {1}", this._message, elapsed);
            Debug.WriteLine(result);
        }
    }
 
}
