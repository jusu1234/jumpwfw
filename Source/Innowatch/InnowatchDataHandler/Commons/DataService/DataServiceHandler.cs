﻿// -----------------------------------------------------------------------
// <copyright file="DataServiceHandler.cs" company="Innotive Inc. Korea">
// Data Service Handler.
// </copyright>
// -----------------------------------------------------------------------

namespace InnowatchDataHandler.Commons.DataService
{
    using System;
    using System.Data;
    using System.IO;
    using System.Net;
    using System.Net.Cache;
    using System.Text;
    using System.Xml;
    using InnowatchServiceLog;

    /// <summary>
    /// The data service handler class.
    /// </summary>
    public static class DataServiceHandler
    {
        public static string Request(RequestParameter parameter, bool isPost = false)
        {
            var url = parameter.Url;

            var encodingOption = GetEncodingOption(parameter.EncodingOption);

            if (string.IsNullOrEmpty(url))
                return string.Empty;

            var responseString = string.Empty;

            try
            {
                // 로컬 파일 읽기
                if (url.Contains("http://") == false)
                {
                    var reader = new StreamReader(url, encodingOption);
                    responseString = reader.ReadToEnd();
                    reader.Close();
                    return responseString;
                }

                var httpWebRequest = WebRequestOnly(url);

                if (httpWebRequest == null)
                    return string.Empty;

                if (isPost)
                {
                    httpWebRequest.Method = "POST";

                    var postData = parameter.PostMessage;
                    var byteArray = encodingOption.GetBytes(postData);

                    // Set the ContentType property of the WebRequest.
                    httpWebRequest.ContentType = "application/x-www-form-urlencoded";
                    httpWebRequest.ContentLength = byteArray.Length;

                    var dataStream = httpWebRequest.GetRequestStream();

                    dataStream.Write(byteArray, 0, byteArray.Length);

                    dataStream.Close();
                }

                using (var webResponse = httpWebRequest.GetResponse())
                {
                    using (var stream = webResponse.GetResponseStream())
                    {
                        if (stream == null)
                        {
                            return string.Empty;
                        }

                        using (var streamReader = new StreamReader(stream, encodingOption))
                        {
                            responseString = streamReader.ReadToEnd();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, string.Format("[Web request Error] : {0}", ex.Message));
            }

            return responseString;
        }

        /// <summary>
        /// Response string에 REST가 붙여주는 xml 노드를 제거해서 반환한다. (필요한 순수 string만 반환)
        /// </summary>
        /// <param name="parameter">The PollingParameter.</param>
        /// <param name="isPost">The isPost.</param>
        /// <returns>The result string.</returns>
        public static string RequestToRestTypeResponse(RequestParameter parameter, bool isPost = false)
        {
            var result = string.Empty;
            var xmlResponse = Request(parameter, isPost);

            if (string.IsNullOrWhiteSpace(xmlResponse))
            {
                return string.Empty;
            }

            try
            {
                var doc = new XmlDocument();
                doc.LoadXml(xmlResponse);

                if (doc.DocumentElement == null)
                {
                    return string.Empty;
                }

                result = doc.DocumentElement.InnerText;
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, ex.Message);
            }

            return result;
        }

        /// <summary>
        /// REST 요청으로 응답온 DataSet 을 반환한다.
        /// </summary>
        /// <param name="parameter">The PollingParameter.</param>
        /// <param name="isPost">The isPost.</param>
        /// <returns>The DataSet.</returns>
        public static DataSet RequestToDataSetResponseByRest(RequestParameter parameter, bool isPost = false)
        {
            DataSet dataSet = null;
            try
            {
                dataSet = new DataSet();

                var dataString = RequestToRestTypeResponse(parameter, isPost);
                var encodingOption = GetEncodingOption(parameter.EncodingOption);

                using (var stream = new MemoryStream(encodingOption.GetBytes(dataString)))
                {
                    dataSet.ReadXml(stream);
                }
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, ex.Message);
            }

            return dataSet;
        }

        /// <summary>
        /// The request to stream response.
        /// </summary>
        /// <param name="parameter">
        /// The parameter.
        /// </param>
        /// <param name="isPost">
        /// The is post.
        /// </param>
        /// <param name="isRecord">
        /// The is record.
        /// </param>
        /// <returns>
        /// The System.IO.Stream.
        /// </returns>
        public static Stream RequestToStreamResponse(RequestParameter parameter, bool isPost = false, bool isRecord = false)
        {
            Stream stream = null;

            try
            {
                var webResponse = GetResponseStream(parameter, isPost, isRecord);
                stream = webResponse.GetResponseStream();
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, ex.Message);
            }

            return stream;
        }

        private static WebResponse GetResponseStream(RequestParameter parameter, bool isPost = false, bool isRecord = false)
        {
            WebResponse response = null;
            var url = parameter.Url;
            var encodingOption = GetEncodingOption(parameter.EncodingOption);

            if (string.IsNullOrEmpty(url))
            {
                return null;
            }

            try
            {
                var req = isRecord ? WebRequestOnly(url, true) : WebRequestOnly(url);

                if (req == null)
                {
                    return null;
                }

                if (isPost)
                {
                    req.Method = "POST";

                    var postData = parameter.PostMessage;
                    var byteArray = encodingOption.GetBytes(postData);

                    // Set the ContentType property of the WebRequest.
                    req.ContentType = "application/x-www-form-urlencoded";
                    req.ContentLength = byteArray.Length;

                    var dataStream = req.GetRequestStream();

                    dataStream.Write(byteArray, 0, byteArray.Length);

                    dataStream.Close();
                }

                response = req.GetResponse();
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, ex.Message);
            }

            return response;
        }

        public static void BeginGetResponseOnlyGetMethod(RequestParameter parameter, Action<Stream> callback, bool isRecord = false)
        {
            var url = parameter.Url;

            if (string.IsNullOrEmpty(url))
                return;

            try
            {
                var req = isRecord ? WebRequestOnly(url, true) : WebRequestOnly(url);
                if (req == null)
                {
                    return;
                }
                var state = new object[] { req, callback };
                req.Timeout = 5000;
                req.BeginGetResponse(ReceiveCallback, state);
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, ex.Message);
            }
        }

        private static void ReceiveCallback(IAsyncResult asyncResult)
        {
            try
            {
                var state = asyncResult.AsyncState as object[];
                if (state == null)
                    return;

                var req = state[0] as HttpWebRequest;
                var callback = state[1] as Action<Stream>;

                if (req == null || callback == null)
                    return;

                using (var response = req.EndGetResponse(asyncResult))
                {
                    using (var stream = response.GetResponseStream())
                    {
                        callback(stream);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, ex);
                Console.WriteLine(ex);
            }
        }

        /// <summary>
        /// The web request only.
        /// </summary>
        /// <param name="url">
        /// The url.
        /// </param>
        /// <param name="isRecord">
        /// The is record.
        /// </param>
        /// <returns>
        /// The System.Net.HttpWebRequest.
        /// </returns>
        public static HttpWebRequest WebRequestOnly(string url, bool isRecord = false)
        {
            try
            {
                var req = WebRequest.Create(url) as HttpWebRequest;

                if (req != null)
                {
                    req.Timeout = isRecord ? 5000 : 100000;

                    req.ContentType = "multipart/form-data";
                    req.Method = "GET";
                    req.SendChunked = false;
                    req.CachePolicy = new RequestCachePolicy(RequestCacheLevel.NoCacheNoStore);

                    return req;
                }

                return null;
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, string.Format("HttpRequest Error : {0}", ex.Message));
                return null;
            }
        }

        private static Encoding GetEncodingOption(string encondingOption)
        {
            Encoding encodingOption;

            // Contents에 지정된 EncodingOption을 가져와서 설정한다.
            switch (encondingOption)
            {
                case "ASCII":
                    encodingOption = Encoding.ASCII;
                    break;
                case "BigEndianUnicode":
                    encodingOption = Encoding.BigEndianUnicode;
                    break;
                case "Unicode":
                    encodingOption = Encoding.Unicode;
                    break;
                case "UTF32":
                    encodingOption = Encoding.UTF32;
                    break;
                case "UTF7":
                    encodingOption = Encoding.UTF7;
                    break;
                case "UTF8":
                    encodingOption = Encoding.UTF8;
                    break;
                case "Default":
                    encodingOption = Encoding.Default;
                    break;
                default:
                    encodingOption = Encoding.UTF8;
                    break;
            }

            return encodingOption;
        }
    }
}
