﻿// -----------------------------------------------------------------------
// <copyright file="RequestParameter.cs" company="Microsoft">
// Request Parameter.
// </copyright>
// -----------------------------------------------------------------------

namespace InnowatchDataHandler.Commons.DataService
{
    /// <summary>
    /// The request parameter.
    /// </summary>
    public class RequestParameter
    {
        public string Url { get; set; }
        public string EncodingOption { get; set; }
        public string PostMessage { get; set; }

        public enum CharacterSetEncodingOption
        {
            ASCII = 0,
            BigEndianUnicode = 1,
            Unicode = 2,
            UTF32 = 3,
            UTF7 = 4,
            UTF8 = 5,
            Default = 6,
        }
    }
}
