﻿
namespace InnowatchDataHandler.Commons
{
    using System.Data;
    using System.IO;
    using System.Xml;
    
    public static class XmlConverter
    {
        /// <summary>
        /// The xml to dataset.
        /// </summary>
        /// <param name="getExecuteDataSet">The dataset.</param>
        /// <returns>The xml to dataset.</returns>
        public static string DataSetToXml(DataSet getExecuteDataSet)
        {
            var stringWriteData = new StringWriter();
            getExecuteDataSet.WriteXml(stringWriteData, XmlWriteMode.WriteSchema);

            return stringWriteData.ToString();
        }

        /// <summary>
        /// The xml to favoriet response data.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <param name="itemOrder">The item order.</param>
        /// <returns>The xml to add favoriet response data.</returns>
        public static string XmlToAddFavoriteResponseData(string id, string itemOrder)
        {
            var xmlDoc = new XmlDocument();

            var xmlDec = xmlDoc.CreateXmlDeclaration("1.0", "utf-8", null);
            xmlDoc.AppendChild(xmlDec);

            var responseFavoriteData = xmlDoc.CreateElement("addfavorite");
            xmlDoc.AppendChild(responseFavoriteData);

            var elementId = xmlDoc.CreateElement("id");
            elementId.InnerText = id;
            responseFavoriteData.AppendChild(elementId);

            var elementItemOrder = xmlDoc.CreateElement("itemorder");
            elementItemOrder.InnerText = itemOrder;
            responseFavoriteData.AppendChild(elementItemOrder);

            return xmlDoc.InnerXml;
        }

        /// <summary>
        /// The xml to add camera group response data.
        /// </summary>
        /// <param name="result">카메라 그룹 추가 성공여부 결과 값.</param>
        /// <returns>The xml to add camera group response data.</returns>
        public static string XmlToAddCameraGroupResponseData(string result)
        {
            var xmlDoc = new XmlDocument();

            var xmlDec = xmlDoc.CreateXmlDeclaration("1.0", "utf-8", null);
            xmlDoc.AppendChild(xmlDec);

            var responseFavoriteData = xmlDoc.CreateElement("AddCameraGroup");
            xmlDoc.AppendChild(responseFavoriteData);

            var elementSuccess = xmlDoc.CreateElement("SuccessfulResult");
            elementSuccess.InnerText = result;
            responseFavoriteData.AppendChild(elementSuccess);

            return xmlDoc.InnerXml;
        }
    }
}