﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InnowatchDataHandler.Commons
{
    /// <summary>
    /// RPN 추가 2013-06-21
    /// </summary>
    public class QueryInfo
    {
        public string Query { get; private set; }
        public IEnumerable<ParamInfo> ParamInfos { get; private set; }


        public QueryInfo(string query, IEnumerable<ParamInfo> paramInfos)
        {
            Query = query;
            ParamInfos = paramInfos;
        }
    }
}
