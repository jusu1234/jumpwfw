﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DatabaseHandler.cs" company="Innotive">
//   Innotive Inc Korea
// </copyright>
// <summary>
//   Defines the DatabaseHandler type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace InnowatchDataHandler
{
    /// <summary>
    /// The log mode.
    /// </summary>
    public enum LogType
    {
        /// <summary>
        /// The debug.
        /// </summary>
        Debug,

        /// <summary>
        /// The info.
        /// </summary>
        Info,

        /// <summary>
        /// The warning.
        /// </summary>
        Warning,

        /// <summary>
        /// The error.
        /// </summary>
        Error,

        /// <summary>
        /// The fatal.
        /// </summary>
        Fatal
    }

    /// <summary>
    /// The data manager.
    /// </summary>
    public class DataManager
    {
        #region Member Fildes

        private readonly SetData setData;
        
        private readonly DataHandler dataHandler;

        private readonly GetDataForClient getDataForClient;

        private readonly GetDataForConfiguration getDataForConfiguration;

        private readonly BindingProviderManager bindingProviderManager;

        private static DataManager instance;

        #endregion // Member Fildes

        #region Properties

        /// <summary>
        /// Gets DataHandler.
        /// </summary>
        public DataHandler DataHandler
        {
            get { return this.dataHandler; }
        }

        /// <summary>
        /// Gets GetDataForClient.
        /// </summary>
        public GetDataForClient GetDataForClient
        {
            get { return this.getDataForClient; }
        }

        /// <summary>
        /// Gets GetDataForConfiguration.
        /// </summary>
        public GetDataForConfiguration GetDataForConfiguration
        {
            get { return this.getDataForConfiguration; }
        }

        /// <summary>
        /// Gets SetData.
        /// </summary>
        public SetData SetData
        {
            get { return this.setData; }
        }

        /// <summary>
        /// Gets BindingProviderManager.
        /// </summary>
        public BindingProviderManager BindingProviderManager
        {
            get { return this.bindingProviderManager; }
        }

        #endregion // Properties

        #region Methods

        /// <summary>
        /// Initializes a new instance of the <see cref="DataManager"/> class.
        /// </summary>
        public DataManager()
        {
            this.dataHandler = new DataHandler();
            this.getDataForClient = new GetDataForClient();
            this.getDataForConfiguration = new GetDataForConfiguration();
            this.setData = new SetData();
            this.bindingProviderManager = new BindingProviderManager();
        }

        /// <summary>
        /// Gets GetInstance.
        /// </summary>
        public static DataManager GetInstance
        {
            get
            {
                if (instance != null)
                {
                    return instance;
                }

                instance = new DataManager();

                return instance;
            }
        }

        #endregion // Methods
    }
}
