﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="BindingProviderManager.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Defines the BindingProviderManager type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace InnowatchDataHandler
{
    using System;
    using System.Data;

    /// <summary>
    /// The binding provider manager.
    /// </summary>
    public class BindingProviderManager
    {
        /// <summary>
        /// The set binding provider.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <param name="data">
        /// The data.
        /// </param>
        public void SetBindingProvider(string id, string data)
        {
            try
            {
                DataManager.GetInstance.DataHandler.GetExecuteProcedureDataSet("SP_SetData_Client_BindingProvider", id, data);
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return;
            }
        }

        /// <summary>
        /// The get binding provider.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The get data.
        /// </returns>
        public DataSet GetBindingProvider(string id)
        {
            try
            {
                return DataManager.GetInstance.DataHandler.GetExecuteProcedureDataSet("SP_GetData_Client_BindingProvider", id);
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }
    }
}
