﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DataHandler.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   The data handler.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace InnowatchDataHandler
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;

    using Commons;

    using InnowatchServiceLog.ConsoleMessage;

    using Microsoft.Practices.EnterpriseLibrary.Data;
    using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

    /// <summary>
    /// The data handler.
    /// </summary>
    public class DataHandler
    {
        /// <summary>
        /// The set execute dataset.
        /// </summary>
        /// <param name="query">
        /// The query.
        /// </param>
        /// <returns>
        /// The set execute data set.
        /// </returns>
        public string SetExecuteDataSet(string query)
        {
            return this.SetExecuteDataSet(query, null);
        }

        /// <summary>
        /// The set execute procedure data set.
        /// </summary>
        /// <param name="procedureName">
        /// The procedure name.
        /// </param>
        /// <returns>
        /// The System.String.
        /// </returns>
        public string SetExecuteProcedureDataSet(string procedureName)
        {
            return this.SetExecuteProcedureDataSet(procedureName, null);
        }

        /// <summary>
        /// The set execute dataset.
        /// </summary>
        /// <param name="query">
        /// The query.
        /// </param>
        /// <param name="paramInfo">
        /// The IEnumerable paraminfo.
        /// </param>
        /// <returns>
        /// The set execute data set.
        /// </returns>
        public string SetExecuteDataSet(string query, IEnumerable<ParamInfo> paramInfo)
        {
            try
            {
                var database = DatabaseFactory.CreateDatabase("InnowatchData") as SqlDatabase;

                if (database == null)
                {
                    return "None sql databse!";
                }

                var command = database.GetSqlStringCommand(query);

                if (paramInfo != null)
                {
                    foreach (var parma in paramInfo)
                    {
                        database.AddInParameter(command, parma.Name, parma.DbType, parma.Data);
                    }
                }

                using (var connection = database.CreateConnection())
                {
                    connection.Open();

                    var transaction = connection.BeginTransaction();

                    try
                    {
                        // Credit the first account
                        database.ExecuteNonQuery(command, transaction);

                        // Commit the transaction
                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        this.InsertLogData(LogType.Error, ex.Message);
                        transaction.Rollback();
                        connection.Close();

                        return ex.Message;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }

                return string.Empty;
            }
            catch (Exception ex)
            {
                DataHandlerMessage.PrintExceptionMessage(ex);
                return ex.Message;
            }
        }

        /// <summary>
        /// The set execute procedure data set.
        /// </summary>
        /// <param name="procedureName">
        /// The procedure name.
        /// </param>
        /// <param name="parameterValues">
        /// The parameter Values.
        /// </param>
        /// <returns>
        /// The System.String.
        /// </returns>
        public string SetExecuteProcedureDataSet(string procedureName, params object[] parameterValues)
        {
            try
            {
                var database = DatabaseFactory.CreateDatabase("InnowatchData") as SqlDatabase;

                if (database == null)
                {
                    return "None sql databse!";
                }

                var command = parameterValues == null
                                        ? database.GetStoredProcCommand(procedureName)
                                        : database.GetStoredProcCommand(procedureName, parameterValues);

                using (var connection = database.CreateConnection())
                {
                    connection.Open();

                    var transaction = connection.BeginTransaction();

                    try
                    {
                        database.ExecuteDataSet(command);

                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        this.InsertLogData(LogType.Error, "SetExecuteProcedureDataSet : " + ex);
                        transaction.Rollback();
                        connection.Close();

                        return ex.Message;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }

                return null;
            }
            catch (Exception ex)
            {
                DataHandlerMessage.PrintExceptionMessage(ex);
                return ex.Message;
            }
        }

        /// <summary>
        /// The get execute dataset.
        /// </summary>
        /// <param name="query">The query.</param>
        /// <returns>The dataset.</returns>
        public DataSet GetExecuteDataSet(string query)
        {
            return this.GetExecuteDataSet(query, null);
        }

        /// <summary>
        /// The get procedure dataset.
        /// </summary>
        /// <param name="query">
        /// The query.
        /// </param>
        /// <returns>
        /// The dataset.
        /// </returns>
        public DataSet GetExecuteProcedureDataSet(string query)
        {
            return this.GetExecuteProcedureDataSet(query, null);
        }

        /// <summary>
        /// The get execute dataset.
        /// </summary>
        /// <param name="query">The query.</param>
        /// <param name="parameters">The parameters.</param>
        /// <returns>The dataset of get data.</returns>
        public DataSet GetExecuteDataSet(string query, object parameters)
        {
            try
            {
                var database = DatabaseFactory.CreateDatabase("InnowatchData") as SqlDatabase;

                if (null == database)
                {
                    return null;
                }

                var command = database.GetSqlStringCommand(query) as SqlCommand;

                var paramInfos = parameters as List<ParamInfo>;

                if (paramInfos != null)
                {
                    foreach (var param in paramInfos)
                    {
                        database.AddInParameter(command, param.Name, param.DbType, param.Data);
                    }
                }

               

                return database.ExecuteDataSet(command);
            }
            catch (Exception ex)
            {
                this.InsertLogData(LogType.Error, "GetExecuteDataSet : " + ex.Message);
                DataHandlerMessage.PrintExceptionMessage(ex);
                return null;
            }
        }

        /// <summary>
        /// The get rpocedure dataset.
        /// </summary>
        /// <param name="query">
        /// The query.
        /// </param>
        /// <param name="parameterValues">
        /// The parameter values.
        /// </param>
        /// <returns>
        /// The dataset of get data.
        /// </returns>
        public DataSet GetExecuteProcedureDataSet(string query, params object[] parameterValues)
        {
            try
            {
                var database = DatabaseFactory.CreateDatabase("InnowatchData");

                if (database == null)
                {
                    return null;
                }

                var command = parameterValues == null ? 
                                    database.GetStoredProcCommand(query) : 
                                    database.GetStoredProcCommand(query, parameterValues);

                return database.ExecuteDataSet(command);
            }
            catch (Exception ex)
            {
                this.InsertLogData(LogType.Error, "GetExecuteProcedureDataSet : " + ex.Message);
                System.Console.WriteLine("SQL Query Exception : " + query);
                DataHandlerMessage.PrintExceptionMessage(ex);
                return null;
            }
        }

        /// <summary>
        /// The insert log data.
        /// </summary>
        /// <param name="logType">
        /// The log type.
        /// </param>
        /// <param name="message">
        /// The message.
        /// </param>
        public void InsertLogData(LogType logType, string message)
        {
            const string Query = "INSERT INTO [Log](LogType, LogMessage, LogDate, UserID) VALUES (@LogType, @LogMessage, getDate(), @UserId)";

            var paramInfo = new List<ParamInfo>
                                {
                                    new ParamInfo("@LogType", DbType.String, this.GetLogLevelName(logType)),
                                    new ParamInfo("@LogMessage", DbType.String, CommonUtil.ConvertToDbTypeString(message)),
                                    new ParamInfo("@UserId", DbType.String, CommonUtil.ConvertToDbTypeString(null))
                                };

            SetExecuteDataSet(Query, paramInfo);
        }

        /// <summary>
        /// The get log level name.
        /// </summary>
        /// <param name="logType">
        /// The log type.
        /// </param>
        /// <returns>
        /// The get log level name.
        /// </returns>
        private string GetLogLevelName(LogType logType)
        {
            var logLevelName = string.Empty;

            switch (logType)
            {
                case LogType.Debug:
                    logLevelName = "DEBUG";
                    break;
                case LogType.Info:
                    logLevelName = "INFORMATION";
                    break;
                case LogType.Warning:
                    logLevelName = "WARNING";
                    break;
                case LogType.Error:
                    logLevelName = "ERROR";
                    break;
                case LogType.Fatal:
                    logLevelName = "FATAL";
                    break;
            }

            return logLevelName;
        }


        #region RPN 추가
        /// <summary>
        /// The set execute dataset.
        /// </summary>
        /// <param name="queryInfos">
        /// The IEnumerable QueryInfo.
        /// </param>
        /// <returns>
        /// The set execute data set.
        /// </returns>
        public string SetExecuteDataSet( IEnumerable<QueryInfo> queryInfos)
        {
            
            try
            {
                var database = DatabaseFactory.CreateDatabase("InnowatchData") as SqlDatabase;

                if (database == null)
                {
                    return "None sql databse!";
                }

                if(queryInfos == null)
                {
                    return "None Query!";
                }


                using (var connection = database.CreateConnection())
                {
                    connection.Open();

                    var transaction = connection.BeginTransaction();

                    try
                    {
                        // ExceuteNonQuery Loop

                        foreach (QueryInfo queryInfo in queryInfos)
                        {
                            var command = database.GetSqlStringCommand(queryInfo.Query);


                            if (queryInfo.ParamInfos != null)
                            {
                                foreach (var parma in queryInfo.ParamInfos)
                                {
                                    database.AddInParameter(command, parma.Name, parma.DbType, parma.Data);
                                }
                            }

                            database.ExecuteNonQuery(command, transaction);
                        }



                        // Commit the transaction
                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        this.InsertLogData(LogType.Error, ex.Message);
                        transaction.Rollback();
                        connection.Close();

                        return ex.Message;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }

                return string.Empty;
            }
            catch (Exception ex)
            {
                DataHandlerMessage.PrintExceptionMessage(ex);
                return ex.Message;
            }
        }

        #endregion
    }
}
