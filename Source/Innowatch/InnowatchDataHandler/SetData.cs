﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SetData.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   TODO: Update summary.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace InnowatchDataHandler
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Runtime.CompilerServices;
    using System.Text;
    using Commons;
    using InnowatchServiceLog;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class SetData
    {
        #region User

        /// <summary>
        /// The insert user.
        /// </summary>
        /// <param name="userId">
        /// The user id.
        /// </param>
        /// <param name="password">
        /// The password.
        /// </param>
        /// <param name="name">
        /// The name.
        /// </param>
        /// <param name="priv">
        /// The Privilege.
        /// </param>
        /// <param name="mobileNumber">
        /// The mobile number.
        /// </param>
        /// <param name="officeTelNumber">
        /// The office tel number.
        /// </param>
        /// <param name="email">
        /// The email.
        /// </param>
        /// <param name="useCommand">
        /// The useCommand.
        /// </param>
        /// <param name="usePlayback">
        /// The usePlayback.
        /// </param>
        /// <returns>
        /// The System.String.
        /// </returns>
        public string InsertUser(string userId, string password, string name, int priv, string mobileNumber, string officeTelNumber, string email, bool useCommand, bool useViewer, bool usePlayback)
        {
            try
            {
                return DataManager.GetInstance.DataHandler.SetExecuteProcedureDataSet(
                    "SP_SetData_User_AddUser",
                    CommonUtil.ConvertToDbTypeString(userId),
                    CommonUtil.ConvertToDbTypeString(password),
                    CommonUtil.ConvertToDbTypeString(name),
                    CommonUtil.ConvertToDbTypeString(mobileNumber),
                    CommonUtil.ConvertToDbTypeString(officeTelNumber),
                    CommonUtil.ConvertToDbTypeString(email),
                    priv,
                    useCommand,
                    usePlayback,
                    useViewer);
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.ToString());
                return ex.Message;
            }
        }

        public string ImportUser(string userId, string password, string name, int priv, string mobileNumber, string officeTelNumber, string email, bool useCommand, bool useViewer, bool usePlayback, int count)
        {
            try
            {
                return DataManager.GetInstance.DataHandler.SetExecuteProcedureDataSet(
                    "SP_SetData_Manager_ImportUser",
                    CommonUtil.ConvertToDbTypeString(userId),
                    CommonUtil.ConvertToDbTypeString(password),
                    CommonUtil.ConvertToDbTypeString(name),
                    CommonUtil.ConvertToDbTypeString(mobileNumber),
                    CommonUtil.ConvertToDbTypeString(officeTelNumber),
                    CommonUtil.ConvertToDbTypeString(email),
                    priv,
                    useCommand,
                    usePlayback,
                    useViewer,
                    count);
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.ToString());
                return ex.Message;
            }
        }

        /// <summary>
        /// The insert user privilege.
        /// </summary>
        /// <param name="priv">
        /// The priv.
        /// </param>
        /// <param name="grade">
        /// The grade.
        /// </param>
        /// <param name="sync">
        /// The sync.
        /// </param>
        /// <param name="rdsControl">
        /// The rds Control.
        /// </param>
        /// <param name="ptzControl">
        /// The ptz Control.
        /// </param>
        /// <param name="favoriteSave">
        /// The favorite Save.
        /// </param>
        /// <param name="resourceCamera">
        /// The resource Camera.
        /// </param>
        /// <param name="resourceLayout">
        /// The resource Layout.
        /// </param>
        /// <param name="resourceMap">
        /// The resource Map.
        /// </param>
        /// <param name="resourceImage">
        /// The resource Image.
        /// </param>
        /// <param name="playbackExport"> </param>
        /// <returns>
        /// The System.String.
        /// </returns>
        public string InsertUserPrivilege(int priv, string grade, bool sync, bool rdsControl, bool ptzControl, bool favoriteSave, 
            bool resourceCamera, bool resourceLayout, bool resourceMap, bool resourceImage, bool playbackExport)
        {
            try
            {
                return DataManager.GetInstance.DataHandler.SetExecuteProcedureDataSet(
                    "SP_SetData_User_AddUserPrivilege",
                    priv,
                    CommonUtil.ConvertToDbTypeString(grade),
                    sync,
                    rdsControl,
                    ptzControl,
                    favoriteSave,
                    resourceCamera,
                    resourceLayout,
                    resourceMap,
                    resourceImage,
                    playbackExport);
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.ToString());
                return ex.Message;
            }
        }

        public string ImportUserPrivilege(int priv, string grade, bool sync, bool rdsControl, bool ptzControl, bool favoriteSave, bool resourceCamera, bool resourceLayout, bool resourceMap, 
            bool resourceImage, bool playbackExport, int count)
        {
            try
            {
                return DataManager.GetInstance.DataHandler.SetExecuteProcedureDataSet(
                    "SP_SetData_Manager_ImportUserPrivilege",
                    priv,
                    CommonUtil.ConvertToDbTypeString(grade),
                    sync,
                    rdsControl,
                    ptzControl,
                    favoriteSave,
                    resourceCamera,
                    resourceLayout,
                    resourceMap,
                    resourceImage,
                    playbackExport,
                    count);
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.ToString());
                return ex.Message;
            }
        }

        /// <summary>
        /// The update user.
        /// </summary>
        /// <param name="userId">
        /// The user id.
        /// </param>
        /// <param name="password">
        /// The password.
        /// </param>
        /// <param name="name">
        /// The name.
        /// </param>
        /// <param name="privilege">
        /// The privilege.
        /// </param>
        /// <param name="mobileNumber">
        /// The mobile number.
        /// </param>
        /// <param name="officeTelNumber">
        /// The office tel number.
        /// </param>
        /// <param name="email">
        /// The email.
        /// </param>
        /// <param name="useCommand">
        /// The useCommand.
        /// </param>
        /// <param name="usePlayback">
        /// The usePlayback.
        /// </param>
        /// <returns>
        /// The System.String.
        /// </returns>
        public string UpdateUser(string userId, string password, string name, int privilege, string mobileNumber, string officeTelNumber, string email, bool useCommand, bool useViewer, bool usePlayback)
        {
            try
            {
                return DataManager.GetInstance.DataHandler.SetExecuteProcedureDataSet(
                    "SP_SetData_User_UpdateUser",
                    CommonUtil.ConvertToDbTypeString(userId),
                    CommonUtil.ConvertToDbTypeString(password),
                    CommonUtil.ConvertToDbTypeString(name),
                    CommonUtil.ConvertToDbTypeString(mobileNumber),
                    CommonUtil.ConvertToDbTypeString(officeTelNumber),
                    CommonUtil.ConvertToDbTypeString(email),
                    privilege,
                    useCommand ? 1 : 0,
                    usePlayback ? 1 : 0,
                    useViewer ? 1 : 0);
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.ToString());
                return ex.Message;
            }
        }

        /// <summary>
        /// The update user.
        /// </summary>
        /// <param name="priv">
        /// The priv.
        /// </param>
        /// <param name="grade">
        /// The grade.
        /// </param>
        /// <param name="sync">
        /// The sync.
        /// </param>
        /// <param name="rdsControl">
        /// The rds Control.
        /// </param>
        /// <param name="ptzControl">
        /// The ptz Control.
        /// </param>
        /// <param name="favoriteSave">
        /// The favorite Save.
        /// </param>
        /// <param name="resourceCamera">
        /// The resource Camera.
        /// </param>
        /// <param name="resourceLayout">
        /// The resource Layout.
        /// </param>
        /// <param name="resourceMap">
        /// The resource Map.
        /// </param>
        /// <param name="resourceImage">
        /// The resource Image.
        /// </param>
        /// <param name="playbackExport"> </param>
        /// <returns>
        /// The System.String.
        /// </returns>
        public string UpdateUserPrivilege(int priv, string grade, bool sync, bool rdsControl, bool ptzControl, bool favoriteSave, 
            bool resourceCamera, bool resourceLayout, bool resourceMap, bool resourceImage, bool playbackExport)
        {
            try
            {
                return DataManager.GetInstance.DataHandler.SetExecuteProcedureDataSet(
                        "SP_SetData_User_UpdateUserPrivilege",
                        priv,
                        CommonUtil.ConvertToDbTypeString(grade),
                        sync,
                        rdsControl,
                        ptzControl,
                        favoriteSave,
                        resourceCamera,
                        resourceLayout,
                        resourceMap,
                        resourceImage,
                        playbackExport);
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.ToString());
                return ex.Message;
            }
        }

        /// <summary>
        /// Teh delete user.
        /// </summary>
        /// <param name="userId">
        /// The user id.
        /// </param>
        /// <returns>
        /// The System.String.
        /// </returns>
        public string DeleteUser(string userId)
        {
            try
            {
                return DataManager.GetInstance.DataHandler.SetExecuteProcedureDataSet("SP_SetData_User_DeleteUser", CommonUtil.ConvertToDbTypeString(userId));
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.ToString());
                return ex.Message;
            }
        }

        /// <summary>
        /// Teh delete user.
        /// </summary>
        /// <param name="privilege">
        /// The privilege.
        /// </param>
        /// <returns>
        /// The System.String.
        /// </returns>
        public string DeleteUserPrivilege(int privilege)
        {
            try
            {
                return DataManager.GetInstance.DataHandler.SetExecuteProcedureDataSet("SP_SetData_User_DeleteUserPrivilege", privilege);
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.ToString());
                return ex.Message;
            }
        }

        #endregion // User

        #region CameraGroup

        /// <summary>
        /// The insert camera group.
        /// </summary>
        /// <param name="parentId">
        /// The parent id.
        /// </param>
        /// <param name="cameraGroupName">
        /// The camera group name.
        /// </param>
        /// <param name="projectId">
        /// The project id.
        /// </param>
        /// <param name="itemOrder">
        /// The item order.
        /// </param>
        /// <param name="userId">
        /// The user id.
        /// </param>
        /// <param name="type">
        /// The type.
        /// </param>
        /// <returns>
        /// The System.Data.DataTable.
        /// </returns>
        public DataTable InsertCameraGroup(int parentId, string cameraGroupName, string projectId, int itemOrder, string userId, string type)
        {
            try
            {
                return
                    DataManager.GetInstance.DataHandler.GetExecuteProcedureDataSet(
                        "SP_SetData_CameraGroup_AddCameraGroup",
                        parentId,
                        CommonUtil.ConvertToDbTypeString(cameraGroupName),
                        CommonUtil.ConvertToDbTypeString(projectId),
                        itemOrder,
                        CommonUtil.ConvertToDbTypeString(userId).ToString().ToLower(),
                        CommonUtil.ConvertToDbTypeString(type).ToString().ToLower()).Tables[0];
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.ToString());
                return null;
            }
        }

        /// <summary>
        /// The insert camera group.
        /// </summary>
        /// <param name="cameraGroupId">
        /// The camera group id.
        /// </param>
        /// <param name="parentId">
        /// The parent id.
        /// </param>
        /// <param name="cameraGroupName">
        /// The camera group name.
        /// </param>
        /// <param name="projectId">
        /// The project id.
        /// </param>
        /// <param name="itemOrder">
        /// The item order.
        /// </param>
        /// <param name="userId">
        /// The user id.
        /// </param>
        /// <param name="type">
        /// The type.
        /// </param>
        public void InsertCameraGroupTable(int cameraGroupId, int parentId, string cameraGroupName, string projectId, int itemOrder, string userId, string type)
        {
            try
            {
                DataManager.GetInstance.DataHandler.SetExecuteProcedureDataSet(
                    "SP_SetData_CameraGroup_AddCameraGroupTable",
                    cameraGroupId,
                    parentId,
                    CommonUtil.ConvertToDbTypeString(cameraGroupName),
                    CommonUtil.ConvertToDbTypeString(projectId),
                    itemOrder,
                    CommonUtil.ConvertToDbTypeString(userId).ToString().ToLower(),
                    CommonUtil.ConvertToDbTypeString(type).ToString().ToLower());
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.ToString());
                return;
            }
        }

        /// <summary>
        /// The update camera group.
        /// </summary>
        /// <param name="cameraGroupId">
        /// The camera group id.
        /// </param>
        /// <param name="parentId">
        /// The parent id.
        /// </param>
        /// <param name="cameraGroupName">
        /// The camera group name.
        /// </param>
        /// <param name="projectId">
        /// The project id.
        /// </param>
        /// <param name="itemOrder">
        /// The item order.
        /// </param>
        /// <param name="userId">
        /// The user id.
        /// </param>
        /// <param name="type">
        /// The type.
        /// </param>
        public void UpdateCameraGroup(int cameraGroupId, int parentId, string cameraGroupName, string projectId, int itemOrder, string userId, string type)
        {
            try
            {
                DataManager.GetInstance.DataHandler.SetExecuteProcedureDataSet(
                    "SP_SetData_CameraGroup_UpdateCameraGroup",
                    cameraGroupId,
                    parentId,
                    CommonUtil.ConvertToDbTypeString(cameraGroupName),
                    CommonUtil.ConvertToDbTypeString(projectId),
                    itemOrder,
                    CommonUtil.ConvertToDbTypeString(userId).ToString().ToLower(),
                    CommonUtil.ConvertToDbTypeString(type).ToString().ToLower());
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return;
            }
        }

        /// <summary>
        /// The delete camera group.
        /// </summary>
        /// <param name="cameraGroupId">The camera group id.</param>
        public void DeleteCameraGroup(int cameraGroupId)
        {
            try
            {
                DataManager.GetInstance.DataHandler.SetExecuteProcedureDataSet(
                    "SP_SetData_CameraGroup_DeleteCameraGroupByCameraGroupId", cameraGroupId);
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.ToString());
                return;
            }
        }

        /// <summary>
        /// The delete camera group.
        /// </summary>
        /// <param name="cameraGroupId">
        /// The camera group id.
        /// </param>
        /// <param name="projectId">
        /// The project id.
        /// </param>
        /// <param name="userId">
        /// The user id.
        /// </param>
        public void DeleteCameraGroup(int cameraGroupId, string projectId, string userId)
        {
            try
            {
                DataManager.GetInstance.DataHandler.SetExecuteProcedureDataSet(
                    "SP_SetData_CameraGroup_DeleteCameraGroup",
                    cameraGroupId,
                    CommonUtil.ConvertToDbTypeString(projectId),
                    CommonUtil.ConvertToDbTypeString(userId),
                    null);
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.ToString());
                return;
            }
        }

        /// <summary>
        /// The delete camera group table.
        /// </summary>
        public void DeleteCameraGroupTable()
        {
            try
            {
                DataManager.GetInstance.DataHandler.SetExecuteProcedureDataSet("SP_SetData_CameraGroup_DeleteCameraGroupTable");
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.ToString());
                return;
            }
        }

        /// <summary>
        /// The delete camera group by project id, type.
        /// </summary>
        /// <param name="projectId">
        /// The project id.
        /// </param>
        /// <param name="type">
        /// The type.
        /// </param>
        public void DeleteCameraGroupByProjectId(string projectId, string type)
        {
            try
            {
                DataManager.GetInstance.DataHandler.SetExecuteProcedureDataSet(
                    "SP_SetData_CameraGroup_DeleteCameraGroupByProjectId",
                    CommonUtil.ConvertToDbTypeString(projectId),
                    CommonUtil.ConvertToDbTypeString(type));
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.ToString());
                return;
            }
        }

        /// <summary>
        /// 프로젝트 아이디, 사용자 아이디, 프로그램 타입을 검사하여 카메라 그룹을 삭제한다.
        /// </summary>
        /// <param name="projectId">
        /// The project id.
        /// </param>
        /// <param name="userId">
        /// The user Id.
        /// </param>
        /// <param name="type">
        /// The type.
        /// </param>
        public void DeleteCameraGroup(string projectId, string userId, string type)
        {
            try
            {
                DataManager.GetInstance.DataHandler.SetExecuteProcedureDataSet(
                    "SP_SetData_CameraGroup_DeleteCameraGroup",
                    null,
                    CommonUtil.ConvertToDbTypeString(projectId),
                    CommonUtil.ConvertToDbTypeString(userId),
                    CommonUtil.ConvertToDbTypeString(type));
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.ToString());
                return;
            }
        }

        /// <summary>
        /// The select camera group for client.
        /// </summary>
        /// <param name="projectId">The project id.</param>
        /// <param name="userId">The user id.</param>
        /// <param name="type">The type.</param>
        /// <returns>The dataset of parent camera group.</returns>
        public DataSet SelectParentCameraGroupForClient(string projectId, string userId, string type)
        {
            try
            {
                var query = new StringBuilder();

                query.Append("SELECT *                                                                              ");
                query.Append("FROM CameraGroup                                                                      ");
                query.Append("WHERE ProjectID = @ProjectId AND UserID = @UserId AND ParentID = -1 AND Type = @Type  ");
                query.Append("ORDER BY CameraGroupID ASC                                                            ");

                var paramInfo = new List<ParamInfo>
                                      {
                                          new ParamInfo("@ProjectId", DbType.String, CommonUtil.ConvertToDbTypeString(projectId)),
                                          new ParamInfo("@UserId", DbType.String, CommonUtil.ConvertToDbTypeString(userId).ToString().ToLower()),
                                          new ParamInfo("@Type", DbType.String, CommonUtil.ConvertToDbTypeString(type).ToString().ToLower())
                                      };

                return DataManager.GetInstance.DataHandler.GetExecuteDataSet(query.ToString(), paramInfo);
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        /// <summary>
        /// The select childe camera group for client.
        /// </summary>
        /// <param name="projectId">The project id.</param>
        /// <param name="userId">The user id.</param>
        /// <param name="type">The type.</param>
        /// <returns>The dataset of child camera group.</returns>
        public DataSet SelectChildCameraGroupForClient(string projectId, string userId, string type)
        {
            try
            {
                var query = new StringBuilder();

                query.Append("SELECT *                                                                                      ");
                query.Append("FROM CameraGroup                                                                              ");
                query.Append("WHERE ProjectID = @ProjectId AND UserID = @UserId AND ParentID NOT IN (-1) AND Type = @Type   ");
                query.Append("ORDER BY CameraGroupID ASC                                                                    ");

                var paramInfo = new List<ParamInfo>
                                      {
                                          new ParamInfo("@ProjectId", DbType.String, CommonUtil.ConvertToDbTypeString(projectId)),
                                          new ParamInfo("@UserId", DbType.String, CommonUtil.ConvertToDbTypeString(userId).ToString().ToLower()),
                                          new ParamInfo("@Type", DbType.String, CommonUtil.ConvertToDbTypeString(type).ToString().ToLower())
                                      };

                return DataManager.GetInstance.DataHandler.GetExecuteDataSet(query.ToString(), paramInfo);
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        #endregion // CameraGroup

        #region CameraGroupMapping

        /// <summary>
        /// The insert camera group mapping.
        /// </summary>
        /// <param name="cameraGroupId">The camera group id.</param>
        /// <param name="cameraId">The camera id.</param>
        /// <param name="itemOrder">The item order.</param>
        public void InsertCameraGroupMapping(int cameraGroupId, string cameraId, int itemOrder)
        {
            try
            {
                const string query = "INSERT INTO CameraGroupMapping VALUES (@CameraGroupID, @CameraID, @ItemOrder)";

                var paramInfo = new List<ParamInfo>
                                    {
                                        new ParamInfo("@CameraGroupID", DbType.Int32, cameraGroupId),
                                        new ParamInfo("@CameraID", DbType.String, CommonUtil.ConvertToDbTypeString(cameraId)),
                                        new ParamInfo("@ItemOrder", DbType.Int32, itemOrder)
                                    };

                DataManager.GetInstance.DataHandler.SetExecuteDataSet(query, paramInfo);
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return;
            }
        }

        /// <summary>
        /// The update camera group mapping.
        /// </summary>
        /// <param name="currentCameraGroupId">The current camera group id.</param>
        /// <param name="cameraGroupId">The camera group id.</param>
        /// <param name="cameraId">The camera id.</param>
        /// <param name="itemOrder">The item order.</param>
        public void UpdateCameraGroupMapping(int currentCameraGroupId, int cameraGroupId, string cameraId, int itemOrder)
        {
            try
            {
                const string query = "UPDATE CameraGroupMapping SET CameraGroupID = @CameraGroupId, ItemOrder = @ItemOrder WHERE CameraGroupID = @CurrentCameraGroupId AND CameraID = @CameraId";

                var paramInfo = new List<ParamInfo>
                                    {
                                        new ParamInfo("@CurrentCameraGroupId", DbType.Int32, currentCameraGroupId),
                                        new ParamInfo("@CameraGroupId", DbType.Int32, cameraGroupId),
                                        new ParamInfo("@CameraId", DbType.String, CommonUtil.ConvertToDbTypeString(cameraId)),
                                        new ParamInfo("@ItemOrder", DbType.Int32, itemOrder)
                                    };

                DataManager.GetInstance.DataHandler.SetExecuteDataSet(query, paramInfo);
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return;
            }
        }

        /// <summary>
        /// The delete camera group mapping.
        /// </summary>
        /// <param name="cameraGroupId">The camera group id.</param>
        /// <param name="cameraId">The camera id.</param>
        public void DeleteCameraGroupMapping(int cameraGroupId, string cameraId)
        {
            try
            {
                const string query = "DELETE CameraGroupMapping WHERE CameraGroupID = @CameraGroupId AND CameraID = @CameraId";

                var paramInfo = new List<ParamInfo>
                                    {
                                        new ParamInfo("@CameraGroupId", DbType.Int32, cameraGroupId),
                                        new ParamInfo("@CameraId", DbType.String, CommonUtil.ConvertToDbTypeString(cameraId))
                                    };

                DataManager.GetInstance.DataHandler.SetExecuteDataSet(query, paramInfo);
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return;
            }
        }

        public void DeleteCameraGroupMappingTable()
        {
            try
            {
                var query = new StringBuilder();

                query.Append("DELETE FROM CameraGroupMapping            ");
                query.Append("DBCC CHECKIDENT(CameraGroup, RESEED, 0)   ");

                DataManager.GetInstance.DataHandler.SetExecuteDataSet(query.ToString());
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return;
            }
        }

        /// <summary>
        /// The delete camera group mapping by camera group id.
        /// </summary>
        /// <param name="cameraGroupId">The camera group id.</param>
        public void DeleteCameraGroupMappingByCameraGroupId(int cameraGroupId)
        {
            try
            {
                const string query = "DELETE CameraGroupMapping WHERE CameraGroupID = @CameraGroupId";

                var paramInfo = new List<ParamInfo>
                                    {
                                        new ParamInfo("@CameraGroupId", DbType.Int32, cameraGroupId)
                                    };

                DataManager.GetInstance.DataHandler.SetExecuteDataSet(query, paramInfo);
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return;
            }
        }

        /// <summary>
        /// The select all camera group mapping.
        /// </summary>
        /// <returns>The dataset of all camera group mapping.</returns>
        public DataSet SelectAllCameraGroupMapping()
        {
            try
            {
                const string query = "SELECT * FROM CameraGroupMapping";

                return DataManager.GetInstance.DataHandler.GetExecuteDataSet(query);
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        /// <summary>
        /// The select camera group mapping.
        /// </summary>
        /// <param name="cameraGroupId">The camera group id.</param>
        /// <param name="cameraId">The camera id.</param>
        /// <returns>The dataset of camera group mapping.</returns>
        public DataSet SelectCameraGroupMapping(int cameraGroupId, string cameraId)
        {
            try
            {
                const string query = "SELECT * FROM CameraGroupMapping WHERE CameraGroupID = @CameraGroupId AND CameraID = @CameraId";

                var paramInfo = new List<ParamInfo>
                                {
                                    new ParamInfo("@CameraGroupId", DbType.Int32, cameraGroupId),
                                    new ParamInfo("@CameraId", DbType.String, CommonUtil.ConvertToDbTypeString(cameraId))
                                };

                return DataManager.GetInstance.DataHandler.GetExecuteDataSet(query, paramInfo);
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        /// <summary>
        /// The select camera group mapping for client.
        /// </summary>
        /// <param name="projectId">The project id.</param>
        /// <param name="userId">The user id.</param>
        /// <param name="type">The type.</param>
        /// <returns>The dataset of camera group mapping for client.</returns>
        public DataSet SelectCameraGroupMappingForClient(string projectId, string userId, string type)
        {
            try
            {
                var query = new StringBuilder();

                query.Append("SELECT CGM.CameraGroupID, PM.ProjectCameraID, PM.ProjectCameraName, CGM.ItemOrder ");
                query.Append("FROM CameraGroupMapping AS CGM                                                    ");
                query.Append("  INNER JOIN CameraGroup AS CG                                                    ");
                query.Append("  ON CGM.CameraGroupID = CG.CameraGroupID                                         ");
                query.Append("  INNER JOIN ProjectMapping AS PM                                                 ");
                query.Append("  ON CG.ProjectID = PM.ProjectID AND CGM.CameraID = PM.CameraID                   ");
                query.Append("WHERE CG.ProjectID = @ProjectId AND CG.UserID = @UserId AND CG.Type = @Type       ");
                query.Append("ORDER BY CGM.CameraGroupID ASC, CGM.ItemOrder ASC                                 ");

                var paramInfo = new List<ParamInfo>
                                {
                                    new ParamInfo("@ProjectId", DbType.String, CommonUtil.ConvertToDbTypeString(projectId)),
                                    new ParamInfo("@UserId", DbType.String, CommonUtil.ConvertToDbTypeString(userId).ToString().ToLower()),
                                    new ParamInfo("@Type", DbType.String, CommonUtil.ConvertToDbTypeString(type).ToString().ToLower())
                                };

                return DataManager.GetInstance.DataHandler.GetExecuteDataSet(query.ToString(), paramInfo);
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        /// <summary>
        /// The select lost childe camera group mapping.
        /// </summary>
        /// <returns>The dataset of select lost childe camera group mapping.</returns>
        public DataSet SelectLostChildeCameraGroupMapping()
        {
            try
            {
                var query = new StringBuilder();

                query.Append("SELECT *                                          ");
                query.Append("FROM CameraGroupMapping                           ");
                query.Append("WHERE CameraGroupID                               ");
                query.Append("  NOT IN(                                         ");
                query.Append("      SELECT cgm.CameraGroupID                    ");
                query.Append("      FROM CameraGroupMapping AS cgm              ");
                query.Append("          INNER JOIN CameraGroup AS cg            ");
                query.Append("          ON cgm.CameraGroupID = cg.CameraGroupID ");
                query.Append("  )                                               ");

                return DataManager.GetInstance.DataHandler.GetExecuteDataSet(query.ToString(), null);
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        #endregion // CameraGroupMapping

        #region Favorite

        /// <summary>
        /// The insert favorite.
        /// </summary>
        /// <param name="favoriteName">The favorite name.</param>
        /// <param name="data">The data.</param>
        /// <param name="type">The type.</param>
        /// <returns>The favorit id.</returns>
        public DataSet InsertFavorite(string favoriteName, string data, string type)
        {
            try
            {
                const string query = "INSERT INTO Favorite(FavoriteName, Data, Type) VALUES (@FavoriteName, @Data, @Type) SELECT @@IDENTITY AS FavoriteID";

                var parmaInfo = new List<ParamInfo>
                                    {
                                        new ParamInfo("@Favoritename", DbType.String, CommonUtil.ConvertToDbTypeString(favoriteName)),
                                        new ParamInfo("@Data", DbType.String, CommonUtil.ConvertToDbTypeString(data)),
                                        new ParamInfo("@Type", DbType.String, CommonUtil.ConvertToDbTypeString(type))
                                    };

                return DataManager.GetInstance.DataHandler.GetExecuteDataSet(query, parmaInfo);
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        /// <summary>
        /// The insert favorite table.
        /// </summary>
        /// <param name="favoriteId">The favorite id.</param>
        /// <param name="favoriteName">The favorite name.</param>
        /// <param name="data">The data.</param>
        public void InsertFavoriteTable(int favoriteId, string favoriteName, string data, string type)
        {
            try
            {
                var query = new StringBuilder();

                query.Append("SET IDENTITY_INSERT Favorite ON                                                                   ");
                query.Append("INSERT INTO Favorite(FavoriteID, FavoriteName, Data, Type) VALUES (@FavoriteId, @FavoriteName, @Data, @Type)   ");
                query.Append("SET IDENTITY_INSERT Favorite OFF                                                                  ");

                var parmaInfo = new List<ParamInfo>
                                    {
                                        new ParamInfo("@FavoriteId", DbType.Int32, favoriteId),
                                        new ParamInfo("@FavoriteName", DbType.String, CommonUtil.ConvertToDbTypeString(favoriteName)),
                                        new ParamInfo("@Data", DbType.String, CommonUtil.ConvertToDbTypeString(data)),
                                        new ParamInfo("@Type", DbType.String, CommonUtil.ConvertToDbTypeString(type))
                                    };

                DataManager.GetInstance.DataHandler.SetExecuteDataSet(query.ToString(), parmaInfo);
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return;
            }
        }

        /// <summary>
        /// The update favorite.
        /// </summary>
        /// <param name="favoriteId">The favorite id.</param>
        /// <param name="favoriteName">The favorite name.</param>
        /// <param name="data">The data.</param>
        public void UpdateFavorite(int favoriteId, string favoriteName, string data, string type)
        {
            try
            {
                var query = new StringBuilder();

                query.Append("UPDATE Favorite                                   ");
                query.Append("SET FavoriteName = @FavoriteName, Data = @Data    ");
                query.Append("WHERE FavoriteID = @FavoriteId AND Type = @Type   ");

                var parmaInfo = new List<ParamInfo>
                                    {
                                        new ParamInfo("@FavoriteId", DbType.Int32, favoriteId),
                                        new ParamInfo("@Favoritename", DbType.String, CommonUtil.ConvertToDbTypeString(favoriteName)),
                                        new ParamInfo("@Data", DbType.String, CommonUtil.ConvertToDbTypeString(data)),
                                        new ParamInfo("@Type", DbType.String, CommonUtil.ConvertToDbTypeString(type))
                                    };

                DataManager.GetInstance.DataHandler.SetExecuteDataSet(query.ToString(), parmaInfo);
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return;
            }
        }

        /// <summary>
        /// The delete favorite.
        /// </summary>
        /// <param name="favoriteId">The favorite id.</param>
        /// <param name="type">The type.</param>
        public void DeleteFavorite(int favoriteId, string type)
        {
            try
            {
                const string query = "DELETE Favorite WHERE FavoriteID = @FavoriteId AND Type = @Type";

                var parmaInfo = new List<ParamInfo>
                                    {
                                        new ParamInfo("@FavoriteId", DbType.Int32, favoriteId),
                                        new ParamInfo("@Type", DbType.String, type)
                                    };

                DataManager.GetInstance.DataHandler.SetExecuteDataSet(query, parmaInfo);
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return;
            }
        }

        public void DeleteFavoriteTable()
        {
            try
            {
                var query = new StringBuilder();

                query.Append("DELETE FROM Favorite                  ");
                query.Append("DBCC CHECKIDENT(Favorite, RESEED, 0)  ");

                DataManager.GetInstance.DataHandler.SetExecuteDataSet(query.ToString());
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return;
            }
        }

        /// <summary>
        /// The select favorite.
        /// </summary>
        /// <param name="favoriteId">The favorite id.</param>
        /// <returns>The dataset of favorite.</returns>
        public DataSet SelectFavorite(int favoriteId)
        {
            try
            {
                const string query = "SELECT * FROM Favorite WHERE FavoriteID = @FavoriteId";

                var parmaInfo = new List<ParamInfo>
                                    {
                                        new ParamInfo("@FavoriteId", DbType.Int32, favoriteId)
                                    };

                return DataManager.GetInstance.DataHandler.GetExecuteDataSet(query, parmaInfo);
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }


        /// <summary>
        /// 즐겨 찾기 그룹 아이디 목록을 가지고 있는 데이타 셋을 가져온다.
        /// </summary>
        /// <param name="projectID">
        /// The project ID.
        /// </param>
        /// <param name="userID">
        /// The user ID.
        /// </param>
        /// <param name="type">
        /// The type.
        /// </param>
        /// <returns>
        /// 데이타 셋을 반환한다.
        /// </returns>
        public DataSet SelectFavoriteGroupID(string projectID, string userID, string type)
        {
            const string Query =
                " SELECT     FavoriteGroupMapping.FavoriteID"
                + " FROM       FavoriteGroupMapping INNER JOIN FavoriteGroup "
                + "ON             FavoriteGroupMapping.FavoriteGroupID = FavoriteGroup.FavoriteGroupID"
                + " WHERE    (FavoriteGroup.ProjectID = @ProjectID) "
                + " AND          (FavoriteGroup.UserID = @UserID)"
                + " AND         (FavoriteGroup.Type = @Type)";
            try
            {

                var parmaInfo = new List<ParamInfo>
                                    {
                                        new ParamInfo("@ProjectID", DbType.String, projectID),
                                        new ParamInfo("@UserID", DbType.String, userID),
                                        new ParamInfo("@Type", DbType.String, type)
                                    };

                return DataManager.GetInstance.DataHandler.GetExecuteDataSet(Query, parmaInfo);
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }


        #endregion // Favorite

        #region FavoriteGroup

        /// <summary>
        /// The insert favorite group.
        /// </summary>
        /// <param name="parentId">The parent id.</param>
        /// <param name="favoriteGroupName">The favorite group name.</param>
        /// <param name="projectId">The project id.</param>
        /// <param name="itemOrder">The item order.</param>
        /// <param name="userId">The user id.</param>
        /// <param name="type">The type.</param>
        /// <returns>The favorite id.</returns>
        public DataSet InsertFavoriteGroup(int parentId, string favoriteGroupName, string projectId, int itemOrder, string userId, string type)
        {
            try
            {
                var query = new StringBuilder();

                query.Append("INSERT INTO FavoriteGroup(ParentID, FavoriteGroupName, ProjectID, ItemOrder, UserID, Type)                          ");
                query.Append("VALUES (@ParentId, @FavoriteGroupName, @ProjectId, @ItemOrder, @UserId, @Type) SELECT @@IDENTITY AS FavoriteGroupID ");

                var paramInfo = new List<ParamInfo>
                                    {
                                        new ParamInfo("@ParentId", DbType.Int32, parentId),
                                        new ParamInfo("@FavoriteGroupName", DbType.String, CommonUtil.ConvertToDbTypeString(favoriteGroupName)),
                                        new ParamInfo("@ProjectId", DbType.String, CommonUtil.ConvertToDbTypeString(projectId)),
                                        new ParamInfo("@ItemOrder", DbType.Int32, itemOrder),
                                        new ParamInfo("@UserId", DbType.String, CommonUtil.ConvertToDbTypeString(userId)),
                                        new ParamInfo("@Type", DbType.String, CommonUtil.ConvertToDbTypeString(type))
                                    };

                return DataManager.GetInstance.DataHandler.GetExecuteDataSet(query.ToString(), paramInfo);
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        public void InsertFavoriteGroupTable(int favoriteGroupId, int parentId, string favoriteGroupName, string projectId, int itemOrder, string userId, string type)
        {
            try
            {
                var query = new StringBuilder();

                query.Append("SET IDENTITY_INSERT FavoriteGroup ON                                                                  ");
                query.Append("INSERT INTO FavoriteGroup(FavoriteGroupID, ParentID, FavoriteGroupName, ProjectID, ItemOrder, UserID, Type) ");
                query.Append("VALUES (@FavoriteGroupId, @ParentId, @FavoriteGroupName, @ProjectId, @ItemOrder, @UserId, @Type)             ");
                query.Append("SET IDENTITY_INSERT FavoriteGroup OFF                                                                 ");

                var paramInfo = new List<ParamInfo>
                                    {
                                        new ParamInfo("@FavoriteGroupId", DbType.Int32, favoriteGroupId),
                                        new ParamInfo("@ParentId", DbType.Int32, parentId),
                                        new ParamInfo("@FavoriteGroupName", DbType.String, CommonUtil.ConvertToDbTypeString(favoriteGroupName)),
                                        new ParamInfo("@ProjectId", DbType.String, CommonUtil.ConvertToDbTypeString(projectId)),
                                        new ParamInfo("@ItemOrder", DbType.Int32, itemOrder),
                                        new ParamInfo("@UserId", DbType.String, CommonUtil.ConvertToDbTypeString(userId)),
                                        new ParamInfo("@Type", DbType.String, CommonUtil.ConvertToDbTypeString(type))
                                    };

                DataManager.GetInstance.DataHandler.SetExecuteDataSet(query.ToString(), paramInfo);
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                throw;
            }
        }

        /// <summary>
        /// The update favorite group.
        /// </summary>
        /// <param name="favoriteGroupId">The favorite group id.</param>
        /// <param name="parentId">The parent id.</param>
        /// <param name="favoriteGroupName">The favorite group name.</param>
        /// <param name="projectId">The prject id.</param>
        /// <param name="itemOrder">The item order.</param>
        /// <param name="userId">The user id.</param>
        /// <param name="type">The type.</param>
        public void UpdateFavoriteGroup(int favoriteGroupId, int parentId, string favoriteGroupName, string projectId, int itemOrder, string userId, string type)
        {
            try
            {
                var query = new StringBuilder();

                query.Append("UPDATE FavoriteGroup                                                                                      ");
                query.Append("SET ParentID = @ParentId, FavoriteGroupName = @FavoriteGroupName, ItemOrder = @ItemOrder                  ");
                query.Append("WHERE FavoriteGroupID = @FavoriteGroupId AND ProjectID = @ProjectId AND UserID = @UserId AND Type = @Type ");

                var paramInfo = new List<ParamInfo>
                                    {
                                        new ParamInfo("@FavoriteGroupId", DbType.Int32, favoriteGroupId),
                                        new ParamInfo("@ProjectId", DbType.String, CommonUtil.ConvertToDbTypeString(projectId)),
                                        new ParamInfo("@UserId", DbType.String, CommonUtil.ConvertToDbTypeString(userId)),
                                        new ParamInfo("@ParentId", DbType.Int32, parentId),
                                        new ParamInfo("@FavoriteGroupName", DbType.String, CommonUtil.ConvertToDbTypeString(favoriteGroupName)),
                                        new ParamInfo("@ItemOrder", DbType.Int32, itemOrder),
                                        new ParamInfo("@Type", DbType.String, CommonUtil.ConvertToDbTypeString(type))
                                    };

                DataManager.GetInstance.DataHandler.SetExecuteDataSet(query.ToString(), paramInfo);
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return;
            }
        }

        /// <summary>
        /// The delete favorite group.
        /// </summary>
        /// <param name="favoriteGroupId">The favorite group id.</param>
        /// <param name="projectId">The proejct id.</param>
        /// <param name="userId">The user id.</param>
        /// <param name="type">The type.</param>
        public void DeleteFavoriteGroup(int favoriteGroupId, string projectId, string userId, string type)
        {
            try
            {
                const string query = "DELETE FavoriteGroup WHERE FavoriteGroupId = @FavoriteGroupId AND ProjectID = @ProjectId AND UserID = @UserId AND Type = @Type";

                var paramInfo = new List<ParamInfo>
                                    {
                                        new ParamInfo("@FavoriteGroupId", DbType.Int32, favoriteGroupId),
                                        new ParamInfo("@ProjectId", DbType.String, CommonUtil.ConvertToDbTypeString(projectId)),
                                        new ParamInfo("@UserId", DbType.String, CommonUtil.ConvertToDbTypeString(userId)),
                                        new ParamInfo("@Type", DbType.String, CommonUtil.ConvertToDbTypeString(type))
                                    };

                DataManager.GetInstance.DataHandler.SetExecuteDataSet(query, paramInfo);
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return;
            }
        }

        /// <summary>
        /// The delete favorite group.
        /// </summary>
        /// <param name="favoriteGroupId">The favorite group id.</param>
        /// <param name="projectId">The proejct id.</param>
        /// <param name="userId">The user id.</param>
        /// <param name="type">The type.</param>
        public void DeleteFavoriteGroup(string projectId, string userId, string type)
        {
            const string Query = "DELETE FavoriteGroup WHERE ProjectID = @ProjectId AND UserID = @UserId AND Type = @Type";
            var paramInfo = new List<ParamInfo>
                                    {
                                        new ParamInfo("@ProjectId", DbType.String, CommonUtil.ConvertToDbTypeString(projectId)),
                                        new ParamInfo("@UserId", DbType.String, CommonUtil.ConvertToDbTypeString(userId)),
                                        new ParamInfo("@Type", DbType.String, CommonUtil.ConvertToDbTypeString(type))
                                    };
            try
            {
                DataManager.GetInstance.DataHandler.SetExecuteDataSet(Query, paramInfo);
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return;
            }
        }

        public void DeleteFavoriteGroupTable()
        {
            try
            {
                var query = new StringBuilder();

                query.Append("DELETE FROM FavoriteGroup                 ");
                query.Append("DBCC CHECKIDENT(FavoriteGroup, RESEED, 0) ");

                DataManager.GetInstance.DataHandler.SetExecuteDataSet(query.ToString());
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return;
            }
        }

        /// <summary>
        /// The delete favorite group by project id.
        /// </summary>
        /// <param name="projectId">The proejct id.</param>
        public void DeleteFavoriteGroupByProjectId(string projectId)
        {
            try
            {
                const string query = "DELETE FavoriteGroup WHERE ProjectID = @ProjectId";

                var paramInfo = new List<ParamInfo>
                                    {
                                        new ParamInfo("@ProjectId", DbType.String, CommonUtil.ConvertToDbTypeString(projectId)),
                                    };

                DataManager.GetInstance.DataHandler.SetExecuteDataSet(query, paramInfo);
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return;
            }
        }

        /// <summary>
        /// The select childeren favorite group by favorite group id.
        /// </summary>
        /// <param name="favoriteGroupId">The favorite group id.</param>
        /// <param name="projectId">The project id.</param>
        /// <param name="userId">The user id.</param>
        /// <returns>The dataset of select children favorite gorup by favorite group id.</returns>
        public DataSet SelectChildrenFavoriteGroupByFavoriteGroupId(int favoriteGroupId, string projectId, string userId)
        {
            try
            {
                const string query = "SELECT * FROM FavoriteGroup WHERE ParentID = @FavoriteGroupId AND ProjectID = @ProjectId AND UserID = @UserId";

                var paramInfo = new List<ParamInfo>
                                    {
                                        new ParamInfo("@FavoriteGroupId", DbType.Int32, favoriteGroupId),
                                        new ParamInfo("@ProjectId", DbType.String, CommonUtil.ConvertToDbTypeString(projectId)),
                                        new ParamInfo("@UserId", DbType.String, CommonUtil.ConvertToDbTypeString(userId).ToString().ToLower())
                                    };

                return DataManager.GetInstance.DataHandler.GetExecuteDataSet(query, paramInfo);
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        /// <summary>
        /// The select lost child favorite group.
        /// </summary>
        /// <returns>The dataset of lost child favorite group.</returns>
        public DataSet SelectLostChildFavoriteGroup()
        {
            try
            {
                var query = new StringBuilder();

                query.Append("SELECT *                      ");
                query.Append("FROM FavoriteGroup            ");
                query.Append("WHERE ParentID                ");
                query.Append("  NOT IN(                     ");
                query.Append("      SELECT FavoriteGroupID  ");
                query.Append("      FROM FavoriteGroup		");
                query.Append("  ) AND ParentID <> -1        ");

                return DataManager.GetInstance.DataHandler.GetExecuteDataSet(query.ToString(), null);
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        #endregion // FavoriteGroup

        #region FavoriteGroupMapping

        /// <summary>
        /// The insert favorite group mapping.
        /// </summary>
        /// <param name="favoriteGroupId">The favorite group id.</param>
        /// <param name="favoriteId">The favorite id.</param>
        /// <param name="itemOrder">The item order.</param>
        /// <returns>The favorite group id.</returns>
        public void InsertFavoriteGroupMapping(int favoriteGroupId, int favoriteId, int itemOrder)
        {
            try
            {
                const string query = "INSERT INTO FavoriteGroupMapping VALUES (@FavoriteGroupID, @FavoriteID, @ItemOrder)";

                var paramInfo = new List<ParamInfo>
                                    {
                                        new ParamInfo("@FavoriteGroupID", DbType.Int32, favoriteGroupId),
                                        new ParamInfo("@FavoriteID", DbType.Int32, favoriteId),
                                        new ParamInfo("@ItemOrder", DbType.Int32, itemOrder)
                                    };

                DataManager.GetInstance.DataHandler.SetExecuteDataSet(query, paramInfo);
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return;
            }
        }

        /// <summary>
        /// The update favorite group mapping.
        /// </summary>
        /// <param name="favoriteGroupId">The favorite group id.</param>
        /// <param name="favoriteId">The favorite id.</param>
        /// <param name="itemOrder">The item order.</param>
        public void UpdateFavoriteGroupMapping(int favoriteGroupId, int favoriteId, int itemOrder)
        {
            try
            {
                var query = new StringBuilder();

                query.Append("UPDATE FavoriteGroupMapping                                           ");
                query.Append("SET FavoriteGroupID = @FavoriteGroupId, ItemOrder = @ItemOrder        ");
                query.Append("WHERE FavoriteGroupID = @FavoriteGroupId AND FavoriteID = @FavoriteId ");

                var paramInfo = new List<ParamInfo>
                                    {
                                        new ParamInfo("@FavoriteGroupId", DbType.Int32, favoriteGroupId),
                                        new ParamInfo("@FavoriteId", DbType.Int32, favoriteId),
                                        new ParamInfo("@ItemOrder", DbType.Int32, itemOrder)
                                    };

                DataManager.GetInstance.DataHandler.SetExecuteDataSet(query.ToString(), paramInfo);
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return;
            }
        }

        /// <summary>
        /// The update favorite group mapping.
        /// </summary>
        /// <param name="currentFavoriteGroupId">The current favorite group id.</param>
        /// <param name="favoriteGroupId">The favorite group id.</param>
        /// <param name="favoriteId">The favorite id.</param>
        /// <param name="itemOrder">The item order.</param>
        public void UpdateFavoriteGroupMappingForConfiguration(int currentFavoriteGroupId, int favoriteGroupId, int favoriteId, int itemOrder)
        {
            try
            {
                var query = new StringBuilder();

                query.Append("UPDATE FavoriteGroupMapping                                                  ");
                query.Append("SET FavoriteGroupID = @FavoriteGroupId, ItemOrder = @ItemOrder               ");
                query.Append("WHERE FavoriteGroupID = @CurrentFavoriteGroupID AND FavoriteID = @FavoriteId ");

                var paramInfo = new List<ParamInfo>
                                    {
                                        new ParamInfo("@CurrentFavoriteGroupID", DbType.Int32, currentFavoriteGroupId),
                                        new ParamInfo("@FavoriteGroupId", DbType.Int32, favoriteGroupId),
                                        new ParamInfo("@FavoriteId", DbType.Int32, favoriteId),
                                        new ParamInfo("@ItemOrder", DbType.Int32, itemOrder)
                                    };

                DataManager.GetInstance.DataHandler.SetExecuteDataSet(query.ToString(), paramInfo);
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return;
            }
        }

        /// <summary>
        /// The delete favorite group mapping by favorite id.
        /// </summary>
        /// <param name="favoriteId">The faovirte id.</param>
        public void DeleteFavoriteGroupMappingByFavoriteId(int favoriteId)
        {
            try
            {
                const string query = "DELETE FavoriteGroupMapping WHERE FavoriteID = @FavoriteId";

                var paramInfo = new List<ParamInfo>
                                    {
                                        new ParamInfo("@FavoriteId", DbType.Int32, favoriteId)
                                    };

                DataManager.GetInstance.DataHandler.SetExecuteDataSet(query, paramInfo);
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return;
            }
        }

        /// <summary>
        /// The delete favorite group mapping by favorite id.
        /// </summary>
        /// <param name="favoriteId">The faovirte id.</param>
        /// <param name="favoriteGroupId">The favorite Group id.</param>
        public void DeleteFavoriteGroupMappingByFavoriteGroupIdAndFavoriteId(int favoriteGroupId, int favoriteId)
        {
            try
            {
                const string query = "DELETE FavoriteGroupMapping WHERE FavoriteID = @FavoriteId AND FavoriteGroupID = @FavoriteGroupId";

                var paramInfo = new List<ParamInfo>
                                    {
                                        new ParamInfo("@FavoriteId", DbType.Int32, favoriteId),
                                        new ParamInfo("@FavoriteGroupId", DbType.Int32, favoriteGroupId)
                                    };

                DataManager.GetInstance.DataHandler.SetExecuteDataSet(query, paramInfo);
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return;
            }
        }

        public void DeleteFavoriteGroupMappingTable()
        {
            try
            {
                const string query = "DELETE FROM FavoriteGroupMapping";

                DataManager.GetInstance.DataHandler.SetExecuteDataSet(query);
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return;
            }
        }


        public void DeleteFavoriteGroupMapping(List<int> favoriteIDs)
        {
            foreach (var id in favoriteIDs)
            {
                this.DeleteFavoriteGroupMappingByFavoriteId(id);
            }
        }

        /// <summary>
        /// The select favorite group mapping by project id and user id.
        /// </summary>
        /// <param name="projectId">The project id.</param>
        /// <param name="userId">The user id.</param>
        /// <returns>The dataset of favorite group mapping by project id and user id.</returns>
        public DataSet SelectFavoriteGroupMapping(string projectId, string userId)
        {
            try
            {
                const string query = "SELECT FM.FavoriteGroupID, FM.FavoriteID, FM.ItemOrder FROM FavoriteGroup AS F INNER JOIN FavoriteGroupMapping AS FM ON F.FavoriteGroupID = FM.FavoriteGroupID WHERE ProjectID = @ProjectId AND UserID = @UserId";

                var paramInfo = new List<ParamInfo>
                                    {
                                        new ParamInfo("@ProjectId", DbType.String, CommonUtil.ConvertToDbTypeString(projectId)),
                                        new ParamInfo("@UserId", DbType.String, CommonUtil.ConvertToDbTypeString(userId))
                                    };

                return DataManager.GetInstance.DataHandler.GetExecuteDataSet(query, paramInfo);
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        /// <summary>
        /// The delete favorite group mapping by favorite group id.
        /// </summary>
        /// <param name="favoriteGroupId">The favorite group id.</param>
        public void DeleteFavoriteGroupMappingByFavoriteGroupId(int favoriteGroupId)
        {
            try
            {
                const string query = "DELETE FavoriteGroupMapping WHERE FavoriteGroupID = @FavoriteGroupId";

                var paramInfo = new List<ParamInfo>
                                    {
                                        new ParamInfo("@FavoriteGroupId", DbType.Int32, favoriteGroupId)
                                    };

                DataManager.GetInstance.DataHandler.SetExecuteDataSet(query, paramInfo);
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return;
            }
        }

        /// <summary>
        /// The select favorite group mapping by favorite group id.
        /// </summary>
        /// <param name="favoriteGroupId">The favorite group id.</param>
        /// <returns>The dataset of select favorite group by favorite group id.</returns>
        public DataSet SelectFavoriteGroupMappingByFavoriteGroupId(int favoriteGroupId)
        {
            try
            {
                const string query = "SELECT * FROM FavoriteGroupMapping WHERE FavoriteGroupID = @FavoriteGroupId";

                var paramInfo = new List<ParamInfo>
                                    {
                                        new ParamInfo("@FavoriteGroupId", DbType.Int32, favoriteGroupId)
                                    };

                return DataManager.GetInstance.DataHandler.GetExecuteDataSet(query, paramInfo);
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        /// <summary>
        /// The select lost childe favorite group mapping.
        /// </summary>
        /// <returns>The dataset of select lost childe favorite group mapping.</returns>
        public DataSet SelectLostChildFavoriteGroupMapping()
        {
            try
            {
                var query = new StringBuilder();

                query.Append("SELECT *                                              ");
                query.Append("FROM FavoriteGroupMapping                             ");
                query.Append("WHERE FavoriteGroupID                                 ");
                query.Append("  NOT IN(                                             ");
                query.Append("      SELECT fgm.FavoriteGroupID                      ");
                query.Append("      FROM FavoriteGroupMapping AS fgm                ");
                query.Append("          INNER JOIN FavoriteGroup AS fg              ");
                query.Append("          ON fgm.FavoriteGroupID = fg.FavoriteGroupID ");
                query.Append("  )                                                   ");

                return DataManager.GetInstance.DataHandler.GetExecuteDataSet(query.ToString(), null);
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        #endregion // FavoriteGroupMapping

        #region FavoriteSlide

        /// <summary>
        /// The add favorite slide.
        /// </summary>
        /// <param name="slideName">
        /// The slide name.
        /// </param>
        /// <param name="slideInfo">
        /// The slide info.
        /// </param>
        /// <param name="projectId">
        /// The project id.
        /// </param>
        /// <param name="userId">
        /// The user id.
        /// </param>
        /// <param name="type">
        /// The type.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string AddFavoriteSlide(string slideName, string slideInfo, string projectId, string userId, string type)
        {
            try
            {
                return DataManager.GetInstance.DataHandler.SetExecuteProcedureDataSet(
                        "SP_SetData_Client_AddFavoriteSlide", slideName, slideInfo, projectId, userId, type);
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.ToString());
                return ex.Message;
            }
        }

        /// <summary>
        /// The update favorite slide.
        /// </summary>
        /// <param name="slideName">
        /// The slide name.
        /// </param>
        /// <param name="slideInfo">
        /// The slide info.
        /// </param>
        /// <param name="projectId">
        /// The project id.
        /// </param>
        /// <param name="userId">
        /// The user id.
        /// </param>
        /// <param name="type">
        /// The type.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string UpdateFavoriteSlide(string slideName, string slideInfo, string projectId, string userId, string type)
        {
            try
            {
                return DataManager.GetInstance.DataHandler.SetExecuteProcedureDataSet(
                        "SP_SetData_Client_UpdateFavoriteSlide", slideName, slideInfo, projectId, userId, type);
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.ToString());
                return ex.Message;
            }
        }

        /// <summary>
        /// The delete favorite slide.
        /// </summary>
        /// <param name="projectId">
        /// The project id.
        /// </param>
        /// <param name="userId">
        /// The user id.
        /// </param>
        /// <param name="type">
        /// The type.
        /// </param>
        /// <param name="name">
        /// The name.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string DeleteFavoriteSlide(string projectId, string userId, string type, string name)
        {
            try
            {
                return DataManager.GetInstance.DataHandler.SetExecuteProcedureDataSet(
                        "SP_SetData_Client_DeleteFavoriteSlide", projectId, userId, type, name);
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.ToString());
                return ex.Message;
            }
        }

        #endregion // FavoriteSlide

        #region FavoriteSchedule

        public string AddOrUpdateFavoriteSchedule(string projectId, string userId, string type, string scheduleInfo)
        {
            try
            {
                return DataManager.GetInstance.DataHandler.SetExecuteProcedureDataSet(
                        "SP_SetData_Client_AddOrUpdateFavoriteSchedule", projectId, userId, type, scheduleInfo);
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return ex.Message;
            }
        }

        #endregion //FavoriteSchedule

        #region MediaServer

        /// <summary>
        /// The insert media server.
        /// </summary>
        /// <param name="mediaServerId">The media server id.</param>
        /// <param name="mediaServerIp">The media server ip.</param>
        /// <param name="description">The media server description.</param>
        /// <param name="vbWidth">The video board width.</param>
        /// <param name="vbHeight">The video board height.</param>
        /// <param name="vbRow">The video board row.</param>
        /// <param name="vbColumn">The video board column.</param>
        /// <param name="transmitIp">The transmit ip.</param>
        /// <param name="transmitPort">The transmit port.</param>
        /// <param name="connectMode">The connect mode.</param>
        /// <param name="activeCode">The active code.</param>
        public string InsertMediaServer(string mediaServerId, string mediaServerIp, string description, int vbWidth, int vbHeight, int vbRow, int vbColumn, string transmitIp, int? transmitPort, string connectMode, string statusServiceUrl, string activeCode)
        {
            try
            {
                const string query = "INSERT INTO MediaServer VALUES (@MediaServerId, @MediaServerIp, @Description, @VideoBoardWidth, @VideoBoardHeight, @VideoBoardRow, @VideoBoardColumn, @TransmitIp, @TransmitPort, @ConnectMode, @ActiveCode, @StatusServiceUrl)";

                var port = string.IsNullOrEmpty(transmitPort.ToString()) ? null : transmitPort;

                var paramInfo = new List<ParamInfo>
                               {
                                   new ParamInfo("@MediaServerId", DbType.String, CommonUtil.ConvertToDbTypeString(mediaServerId)),
                                   new ParamInfo("@MediaServerIp", DbType.String, CommonUtil.ConvertToDbTypeString(mediaServerIp)),
                                   new ParamInfo("@Description", DbType.String, CommonUtil.ConvertToDbTypeString(description)),
                                   new ParamInfo("@VideoBoardWidth", DbType.Int32, vbWidth),
                                   new ParamInfo("@VideoBoardHeight", DbType.Int32, vbHeight),
                                   new ParamInfo("@VideoBoardRow", DbType.Int32, vbRow),
                                   new ParamInfo("@VideoBoardColumn", DbType.Int32, vbColumn),
                                   new ParamInfo("@TransmitIp", DbType.String, CommonUtil.ConvertToDbTypeString(transmitIp)),
                                   new ParamInfo("@TransmitPort", DbType.Int32, port),
                                   new ParamInfo("@ConnectMode", DbType.String, CommonUtil.ConvertToDbTypeString(connectMode)),
                                   new ParamInfo("@ActiveCode", DbType.String, CommonUtil.ConvertToDbTypeString(activeCode)),
                                   new ParamInfo("@StatusServiceUrl", DbType.String, CommonUtil.ConvertToDbTypeString(statusServiceUrl))
                               };

                var result = DataManager.GetInstance.DataHandler.SetExecuteDataSet(query, paramInfo);
                return result;
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return ex.Message;
            }
        }

        public string ImportMediaServer(string mediaServerId, string mediaServerIp, string description, int vbWidth, int vbHeight, int vbRow, int vbColumn, string transmitIp, int? transmitPort, string connectMode, string activeCode, string statusServiceUrl, int count)
        {
            try
            {
                var port = string.IsNullOrEmpty(transmitPort.ToString()) ? null : transmitPort;

                var result = DataManager.GetInstance.DataHandler.SetExecuteProcedureDataSet(
                    "SP_SetData_Manager_ImportMediaServerInfo",
                    CommonUtil.ConvertToDbTypeString(mediaServerId),
                    CommonUtil.ConvertToDbTypeString(mediaServerIp),
                    CommonUtil.ConvertToDbTypeString(description),
                    vbWidth,
                    vbHeight,
                    vbRow,
                    vbColumn,
                    CommonUtil.ConvertToDbTypeString(transmitIp),
                    port,
                    CommonUtil.ConvertToDbTypeString(connectMode),
                    CommonUtil.ConvertToDbTypeString(activeCode),
                    CommonUtil.ConvertToDbTypeString(statusServiceUrl),
                    count);

                return result;
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return ex.Message;
            }
        }

        /// <summary>
        /// The update media server.
        /// </summary>
        /// <param name="mediaServerId">The media server id.</param>
        /// <param name="mediaServerIp">The media server ip.</param>
        /// <param name="description">The media server description.</param>
        /// <param name="vbWidth">The video board width.</param>
        /// <param name="vbHeight">The video board height.</param>
        /// <param name="vbRow">The video board row.</param>
        /// <param name="vbColumn">The video board column.</param>
        /// <param name="transmitIp">The transmit ip.</param>
        /// <param name="transmitPort">The transmit port.</param>
        /// <param name="connectMode">The connect mode.</param>
        /// <param name="activeCode">The active code.</param>
        public string UpdateMediaServer(string mediaServerId, string mediaServerIp, string description, int vbWidth, int vbHeight, int vbRow, int vbColumn, string transmitIp, int? transmitPort, string connectMode, string statusServiceUrl, string activeCode)
        {
            try
            {
                const string query = "UPDATE MediaServer SET MediaServerIP = @MediaServerIp, MediaServerDescription = @Description, VideoBoardWidth = @VideoBoardWidth, VideoBoardHeight = @VideoBoardHeight, VideoBoardRow = @VideoBoardRow, VideoBoardColumn = @VideoBoardColumn, TransmitIP = @TransmitIp, TransmitPort = @TransmitPort, ConnectMode = @ConnectMode, ActiveCode = @ActiveCode, StatusServiceUrl = @StatusServiceUrl WHERE MediaServerID = @MediaServerId";

                var port = string.IsNullOrEmpty(transmitPort.ToString()) ? null : transmitPort;

                var paramInfo = new List<ParamInfo>
                                {
                                    new ParamInfo("@MediaServerIp", DbType.String, CommonUtil.ConvertToDbTypeString(mediaServerIp)),
                                    new ParamInfo("@Description", DbType.String, CommonUtil.ConvertToDbTypeString(description)),
                                    new ParamInfo("@MediaServerId", DbType.String, CommonUtil.ConvertToDbTypeString(mediaServerId)),
                                    new ParamInfo("@VideoBoardWidth", DbType.Int32, vbWidth),
                                    new ParamInfo("@VideoBoardHeight", DbType.Int32, vbHeight),
                                    new ParamInfo("@VideoBoardRow", DbType.Int32, vbRow),
                                    new ParamInfo("@VideoBoardColumn", DbType.Int32, vbColumn),
                                    new ParamInfo("@TransmitIp", DbType.String, CommonUtil.ConvertToDbTypeString(transmitIp)),
                                    new ParamInfo("@TransmitPort", DbType.Int32, port),
                                    new ParamInfo("@ConnectMode", DbType.String, CommonUtil.ConvertToDbTypeString(connectMode)),
                                    new ParamInfo("@StatusServiceUrl", DbType.String, CommonUtil.ConvertToDbTypeString(statusServiceUrl)),
                                    new ParamInfo("@ActiveCode", DbType.String, CommonUtil.ConvertToDbTypeString(activeCode))
                                };

                var result = DataManager.GetInstance.DataHandler.SetExecuteDataSet(query, paramInfo);
                return result;
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return ex.Message;
            }
        }

        public string DeleteMediaServer(string mediaServerId)
        {
            try
            {
                const string query = "DELETE MediaServer WHERE MediaServerID = @MediaServerId";

                var paramInfo = new List<ParamInfo>
                                {
                                    new ParamInfo("@MediaServerId", DbType.String, CommonUtil.ConvertToDbTypeString(mediaServerId))
                                };

                var result = DataManager.GetInstance.DataHandler.SetExecuteDataSet(query, paramInfo);
                return result;
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return ex.Message;
            }
        }

        public DataSet SelectMediaServer(string mediaServerId)
        {
            try
            {
                const string query = "SELECT * FROM MediaServer WHERE MediaServerID = @MediaServerId";

                var paramInfo = new List<ParamInfo>
                                {
                                    new ParamInfo("@MediaServerId", DbType.String, CommonUtil.ConvertToDbTypeString(mediaServerId))
                                };

                return DataManager.GetInstance.DataHandler.GetExecuteDataSet(query, paramInfo);
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        #endregion // MediaServer

        #region Recorder

        /// <summary>
        /// The insert recorder.
        /// </summary>
        public string InsertRecorder(
            string recorderId, 
            string recorderIp, 
            string description, 
            int restServerPort, 
            int imspServerPort,             
            string eventRecordStorageList,             
            string statusServiceUrl, 
            string activeCode)
        {
            try
            {
                const string query = "INSERT INTO Recorder VALUES (@RecorderId, @RecorderIp, @Description, @RESTServerPort, @IMSPServerPort, @EventRecordStorageList, @StatusServiceUrl, @ActiveCode)";

                var paramInfo = new List<ParamInfo>
                               {
                                   new ParamInfo("@RecorderId", DbType.String, CommonUtil.ConvertToDbTypeString(recorderId)),
                                   new ParamInfo("@RecorderIp", DbType.String, CommonUtil.ConvertToDbTypeString(recorderIp)),
                                   new ParamInfo("@Description", DbType.String, CommonUtil.ConvertToDbTypeString(description)),
                                   new ParamInfo("@RESTServerPort", DbType.Int32, restServerPort),
                                   new ParamInfo("@IMSPServerPort", DbType.Int32, imspServerPort),
                                   new ParamInfo("@EventRecordStorageList", DbType.String, CommonUtil.ConvertToDbTypeString(eventRecordStorageList)),                                   
                                   new ParamInfo("@StatusServiceUrl", DbType.String, CommonUtil.ConvertToDbTypeString(statusServiceUrl)),
                                   new ParamInfo("@ActiveCode", DbType.String, CommonUtil.ConvertToDbTypeString(activeCode))
                               };

                var result = DataManager.GetInstance.DataHandler.SetExecuteDataSet(query, paramInfo);
                return result;
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return ex.Message;
            }
        }

        public string ImportRecorder(
            string recorderId,
            string recorderIp,
            string description,
            int restServerPort,
            int imspServerPort,
            string eventRecordStorageList,
            string statusServiceUrl,
            string activeCode,
            int count)
        {
            try
            {                
                var result = DataManager.GetInstance.DataHandler.SetExecuteProcedureDataSet(
                    "SP_SetData_Manager_ImportRecorderInfo",
                    CommonUtil.ConvertToDbTypeString(recorderId),
                    CommonUtil.ConvertToDbTypeString(recorderIp),
                    CommonUtil.ConvertToDbTypeString(description),
                    restServerPort,
                    imspServerPort,
                    CommonUtil.ConvertToDbTypeString(eventRecordStorageList),
                    CommonUtil.ConvertToDbTypeString(statusServiceUrl),
                    CommonUtil.ConvertToDbTypeString(activeCode),
                    count);

                return result;
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return ex.Message;
            }
        }

        /// <summary>
        /// The update recorder.
        /// </summary>
        public string UpdateRecorder(
            string recorderId,
            string recorderIp,
            string description,
            int restServerPort,
            int imspServerPort,
            string eventRecordStorageList,
            string statusServiceUrl,
            string activeCode)
        {
            try
            {
                const string query = "UPDATE Recorder SET RecorderIP = @RecorderIp, RecorderDescription = @Description, RESTServerPort = @RESTServerPort, IMSPServerPort = @IMSPServerPort, EventRecordStorageList = @EventRecordStorageList, StatusServiceUrl = @StatusServiceUrl, ActiveCode = @ActiveCode WHERE RecorderID = @RecorderId";

                var paramInfo = new List<ParamInfo>
                                {
                                    new ParamInfo("@RecorderId", DbType.String, CommonUtil.ConvertToDbTypeString(recorderId)),
                                    new ParamInfo("@RecorderIp", DbType.String, CommonUtil.ConvertToDbTypeString(recorderIp)),
                                    new ParamInfo("@Description", DbType.String, CommonUtil.ConvertToDbTypeString(description)),                                    
                                    new ParamInfo("@RESTServerPort", DbType.Int32, restServerPort),
                                    new ParamInfo("@IMSPServerPort", DbType.Int32, imspServerPort),
                                    new ParamInfo("@EventRecordStorageList", DbType.String, CommonUtil.ConvertToDbTypeString(eventRecordStorageList)),
                                    new ParamInfo("@StatusServiceUrl", DbType.String, CommonUtil.ConvertToDbTypeString(statusServiceUrl)),
                                    new ParamInfo("@ActiveCode", DbType.String, CommonUtil.ConvertToDbTypeString(activeCode))
                                };

                var result = DataManager.GetInstance.DataHandler.SetExecuteDataSet(query, paramInfo);
                return result;
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return ex.Message;
            }
        }

        public string DeleteRecorder(string recorderId)
        {
            try
            {
                const string query = "DELETE Recorder WHERE RecorderID = @RecorderId";

                var paramInfo = new List<ParamInfo>
                                {
                                    new ParamInfo("@RecorderId", DbType.String, CommonUtil.ConvertToDbTypeString(recorderId))
                                };

                var result = DataManager.GetInstance.DataHandler.SetExecuteDataSet(query, paramInfo);
                return result;
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return ex.Message;
            }
        }

        public DataSet SelectRecorder(string recorderId)
        {
            try
            {
                const string query = "SELECT * FROM Recorder WHERE RecorderID = @RecorderId";

                var paramInfo = new List<ParamInfo>
                                {
                                    new ParamInfo("@RecorderId", DbType.String, CommonUtil.ConvertToDbTypeString(recorderId))
                                };

                return DataManager.GetInstance.DataHandler.GetExecuteDataSet(query, paramInfo);
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        #endregion // Recorder

        #region Camera

        /// <summary>
        /// The insert camera.
        /// </summary>
        /// <param name="cameraId">
        /// The camera id.
        /// </param>
        /// <param name="cameraDescription">
        /// The camera description.
        /// </param>
        /// <param name="cameraIp">
        /// The camera ip.
        /// </param>
        /// <param name="cameraPort">
        /// The camera port.
        /// </param>
        /// <param name="connectMode">
        /// The connect mode.
        /// </param>
        /// <param name="mediaServerId">
        /// The media server i.
        /// </param>
        /// <param name="rtspId">
        /// The rtsp id.
        /// </param>
        /// <param name="rtspPassword">
        /// The rtsp password.
        /// </param>
        /// <param name="controlType">
        /// The control type.
        /// </param>
        /// <param name="cameraControlSettingId">
        /// The camera control setting id.
        /// </param>
        /// <param name="ptzConnectionInfoId">
        /// The ptz connection info id.
        /// </param>
        /// <param name="imageClippingArea">
        /// The image clipping area.
        /// </param>
        /// <param name="enable">
        /// The enable.
        /// </param>
        /// <param name="analogCameraMemo">
        /// The analog camera memo.
        /// </param>
        /// <param name="mediaServerPosition">
        /// The media server position.
        /// </param>
        /// <param name="vendor">
        /// The vendor.
        /// </param>
        /// <param name="recordCameraNumber">
        /// The record camera number.
        /// </param>
        /// <param name="rdsHost">
        /// The rds host.
        /// </param>
        /// <param name="rdsPort">
        /// The rds port.
        /// </param>
        /// <param name="multiCameraControllerId">
        /// The multi camera controller id.
        /// </param>
        /// <param name="resizingCodec">
        /// The resizing codec.
        /// </param>
        /// <returns>
        /// The result data.
        /// </returns>
        public string InsertCamera(
            string cameraId,
            string cameraDescription,
            string cameraIp,
            int cameraPort,
            string connectMode,
            string mediaServerId,
            string mediaServerExtraParams,
            string rtspId,
            string rtspPassword,
            string controlType,
            string cameraControlSettingId,
            string ptzConnectionInfoId,
            string imageClippingArea,
            bool enable,
            bool showAudioLevel,
            string analogCameraMemo,
            int? mediaServerPosition,
            int? vendor,
            int? recordCameraNumber,
            string rdsHost,
            int? rdsPort,
            string multiCameraControllerId,
            string resizingCodec,
            string channel)
        {
            try
            {
                var query = new StringBuilder();

                query.Append("INSERT INTO Camera ");
                query.Append("VALUES (@CameraID, @CameraDescription, @CameraIP, @CameraPort, @ConnectMode, @MediaServerID, @RtspID,         ");
                query.Append("        @RtspPassword, @ControlType, @CameraControlSettingID, @PtzConnectionInfoID, @ImageClippingArea,       ");
                query.Append("        @Enable, @AnalogCameraMemo, @MediaServerPosition, @Vendor, @RecordCameraNumber, @RdsHost, @RdsPort,   ");
                query.Append("        @MultiCameraControllerId, @ResizingCodec, @Channel, @ShowAudioLevel, @MediaServerExtraParams)         ");

                if (string.IsNullOrEmpty(multiCameraControllerId))
                {
                    multiCameraControllerId = null;
                }

                var paramInfo = new List<ParamInfo>
                                {
                                    new ParamInfo("@CameraID", DbType.String, CommonUtil.ConvertToDbTypeString(cameraId)),
                                    new ParamInfo("@CameraDescription", DbType.String, CommonUtil.ConvertToDbTypeString(cameraDescription)),
                                    new ParamInfo("@CameraIP", DbType.String, CommonUtil.ConvertToDbTypeString(cameraIp)),
                                    new ParamInfo("@CameraPort", DbType.Int32, cameraPort),
                                    new ParamInfo("@ConnectMode", DbType.String, CommonUtil.ConvertToDbTypeString(connectMode)),
                                    new ParamInfo("@MediaServerID", DbType.String, CommonUtil.ConvertToDbTypeString(mediaServerId)),
                                    new ParamInfo("@RtspID", DbType.String, CommonUtil.ConvertToDbTypeString(rtspId)),
                                    new ParamInfo("@RtspPassword", DbType.String, CommonUtil.ConvertToDbTypeString(rtspPassword)),
                                    new ParamInfo("@ControlType", DbType.String, CommonUtil.ConvertToDbTypeString(controlType)),
                                    new ParamInfo("@CameraControlSettingID", DbType.String, CommonUtil.ConvertToDbTypeString(cameraControlSettingId)),
                                    new ParamInfo("@PtzConnectionInfoID", DbType.String, CommonUtil.ConvertToDbTypeString(ptzConnectionInfoId)),
                                    new ParamInfo("@ImageClippingArea", DbType.String, CommonUtil.ConvertToDbTypeString(imageClippingArea)),
                                    new ParamInfo("@Enable", DbType.Byte, enable ? 1 : 0),
                                    new ParamInfo("@AnalogCameraMemo", DbType.String, CommonUtil.ConvertToDbTypeString(analogCameraMemo)),
                                    new ParamInfo("@MediaServerPosition", DbType.Int32, mediaServerPosition),
                                    new ParamInfo("@Vendor", DbType.Int32, vendor),
                                    new ParamInfo("@RecordCameraNumber", DbType.Int32, recordCameraNumber),
                                    new ParamInfo("@RdsHost", DbType.String, CommonUtil.ConvertToDbTypeString(rdsHost)),
                                    new ParamInfo("@RdsPort", DbType.Int32, rdsPort),
                                    new ParamInfo("@MultiCameraControllerId", DbType.String, multiCameraControllerId),
                                    new ParamInfo("@ResizingCodec", DbType.String, resizingCodec),
                                    new ParamInfo("@Channel", DbType.String, channel),
                                    new ParamInfo("@ShowAudioLevel", DbType.Byte, showAudioLevel ? 1 : 0),
                                    new ParamInfo("@MediaServerExtraParams", DbType.String, CommonUtil.ConvertToDbTypeString(mediaServerExtraParams)),
                                };

                var result = DataManager.GetInstance.DataHandler.SetExecuteDataSet(query.ToString(), paramInfo);
                return result;
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return ex.Message;
            }
        }

        /// <summary>
        /// The insert camera.
        /// </summary>
        /// <param name="cameraId">
        /// The camera id.
        /// </param>
        /// <param name="cameraDescription">
        /// The camera description.
        /// </param>
        /// <param name="cameraIp">
        /// The camera ip.
        /// </param>
        /// <param name="cameraPort">
        /// The camera port.
        /// </param>
        /// <param name="connectMode">
        /// The connect mode.
        /// </param>
        /// <param name="mediaServerId">
        /// The media server id.
        /// </param>
        /// <param name="rtspId">
        /// The rtsp id.
        /// </param>
        /// <param name="rtspPassword">
        /// The rtsp password.
        /// </param>
        /// <param name="controlType">
        /// The control type.
        /// </param>
        /// <param name="cameraControlSettingId">
        /// The camera control setting id.
        /// </param>
        /// <param name="ptzConnectionInfoId">
        /// The ptz connection info id.
        /// </param>
        /// <param name="imageClippingArea">
        /// The image clipping area.
        /// </param>
        /// <param name="enable">
        /// The enable.
        /// </param>
        /// <param name="analogCameraMemo">
        /// The analog camera memo.
        /// </param>
        /// <param name="mediaServerPosition">
        /// The media server position.
        /// </param>
        /// <param name="vendor">
        /// The vendor.
        /// </param>
        /// <param name="recordCameraNumber">
        /// The record camera number.
        /// </param>
        /// <param name="rdsHost">
        /// The rds host.
        /// </param>
        /// <param name="rdsPort">
        /// The rds port.
        /// </param>
        /// <param name="multiCameraControllerId">
        /// The multi camera controller id.
        /// </param>
        /// <param name="resizingCodec">
        /// The resizing codec.
        /// </param>
        /// <returns>
        /// The result data.
        /// </returns>
        public string ImportCamera(
            string cameraId,
            string cameraDescription,
            string cameraIp,
            int cameraPort,
            string connectMode,
            string mediaServerId,
            string mediaServerExtraParams,
            string rtspId,
            string rtspPassword,
            string controlType,
            string cameraControlSettingId,
            string ptzConnectionInfoId,
            string imageClippingArea,
            bool enable,
            bool showAudioLevel,
            string analogCameraMemo,
            int? mediaServerPosition,
            int? vendor,
            int? recordCameraNumber,
            string rdsHost,
            int? rdsPort,
            string multiCameraControllerId,
            string resizingCodec,
            string channel,
            int count)
        {
            try
            {
                if (string.IsNullOrEmpty(multiCameraControllerId))
                {
                    multiCameraControllerId = null;
                }

                var result = DataManager.GetInstance.DataHandler.SetExecuteProcedureDataSet(
                    "SP_SetData_Manager_ImportCameraInfo",
                    CommonUtil.ConvertToDbTypeString(cameraId),
                    CommonUtil.ConvertToDbTypeString(cameraDescription),
                    CommonUtil.ConvertToDbTypeString(cameraIp),
                    cameraPort,                    
                    CommonUtil.ConvertToDbTypeString(connectMode),
                    CommonUtil.ConvertToDbTypeString(mediaServerId),
                    CommonUtil.ConvertToDbTypeString(rtspId),
                    CommonUtil.ConvertToDbTypeString(rtspPassword),
                    CommonUtil.ConvertToDbTypeString(controlType),
                    CommonUtil.ConvertToDbTypeString(cameraControlSettingId),
                    CommonUtil.ConvertToDbTypeString(ptzConnectionInfoId),
                    CommonUtil.ConvertToDbTypeString(imageClippingArea),
                    enable ? 1 : 0,
                    showAudioLevel ? 1 : 0,
                    CommonUtil.ConvertToDbTypeString(mediaServerExtraParams),
                    CommonUtil.ConvertToDbTypeString(analogCameraMemo),
                    mediaServerPosition,
                    vendor,
                    recordCameraNumber,
                    CommonUtil.ConvertToDbTypeString(rdsHost),
                    rdsPort,
                    multiCameraControllerId,
                    resizingCodec,
                    channel,
                    count);

                return result;
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return ex.Message;
            }
        }

        /// <summary>
        /// The update camera.
        /// </summary>
        /// <param name="cameraId">
        /// The camera id.
        /// </param>
        /// <param name="cameraDescription">
        /// The camera description.
        /// </param>
        /// <param name="cameraIp">
        /// The camera ip.
        /// </param>
        /// <param name="cameraPort">
        /// The camera port.
        /// </param>
        /// <param name="connectMode">
        /// The connect mode.
        /// </param>
        /// <param name="mediaServerId">
        /// The media server id.
        /// </param>
        /// <param name="rtspId">
        /// The rtsp id.
        /// </param>
        /// <param name="rtspPassword">
        /// The rtsp password.
        /// </param>
        /// <param name="controlType">
        /// The control type.
        /// </param>
        /// <param name="cameraControlSettingId">
        /// The camera control setting id.
        /// </param>
        /// <param name="ptzConnectionInfoId">
        /// The ptz connection info id.
        /// </param>
        /// <param name="imageClippingArea">
        /// The image clipping area.
        /// </param>
        /// <param name="enable">
        /// The enable.
        /// </param>
        /// <param name="analogCameraMemo">
        /// The analog camera memo.
        /// </param>
        /// <param name="mediaServerPosition">
        /// The media server position.
        /// </param>
        /// <param name="vendor">
        /// The vendor.
        /// </param>
        /// <param name="recordCameraNumber">
        /// The record camera number.
        /// </param>
        /// <param name="rdsHost">
        /// The rds host.
        /// </param>
        /// <param name="rdsPort">
        /// The rds port.
        /// </param>
        /// <param name="multiCameraControllerId">
        /// The multi camera controller id.
        /// </param>
        /// <returns>
        /// The result data.
        /// </returns>
        public string UpdateCamera(
            string cameraId,
            string cameraDescription,
            string cameraIp,
            int cameraPort,
            string connectMode,
            string mediaServerId,
            string mediaServerExtraParams,
            string rtspId,
            string rtspPassword,
            string controlType,
            string cameraControlSettingId,
            string ptzConnectionInfoId,
            string imageClippingArea,
            bool enable,
            bool showAudioLevel,
            string analogCameraMemo,
            int? mediaServerPosition,
            int? vendor,
            int? recordCameraNumber,
            string rdsHost,
            int? rdsPort,
            string multiCameraControllerId,
            string resizingCodec,
            string channel)
        {
            try
            {
                var query = new StringBuilder();

                query.Append("UPDATE Camera                                                                                                                     ");
                query.Append("SET CameraDescription = @CameraDescription, CameraIP = @CameraIP, CameraPort = @CameraPort, ConnectMode = @ConnectMode,           ");
                query.Append("    MediaServerID = @MediaServerID, RtspID = @RtspID, RtspPassword = @RtspPassword, ControlType = @ControlType,                   ");
                query.Append("    CameraControlSettingID = @CameraControlSettingID, PtzConnectionInfoID = @PtzConnectionInfoID,                                 ");
                query.Append("    ImageClippingArea = @ImageClippingArea, Enable = @Enable, AnalogCameraMemo = @AnalogCameraMemo,                               ");
                query.Append("    MediaServerPosition = @MediaServerPosition, Vendor = @Vendor, RecordCameraNumber = @RecordCameraNumber,                       ");
                query.Append("    RdsHost = @RdsHost, RdsPort = @RdsPort, MultiCameraControllerId = @MultiCameraControllerId, ResizingCodec = @ResizingCodec,   ");
                query.Append("    Channel = @Channel, ShowAudioLevel = @ShowAudioLevel, MediaServerExtraParams = @MediaServerExtraParams                        ");
                query.Append("WHERE CameraID = @CameraID                                                                                                        ");

                if (string.IsNullOrEmpty(multiCameraControllerId))
                {
                    multiCameraControllerId = null;
                }

                var paramInfo = new List<ParamInfo>
                                {
                                    new ParamInfo("@CameraID", DbType.String, CommonUtil.ConvertToDbTypeString(cameraId)),
                                    new ParamInfo("@CameraDescription", DbType.String, CommonUtil.ConvertToDbTypeString(cameraDescription)),
                                    new ParamInfo("@CameraIP", DbType.String, CommonUtil.ConvertToDbTypeString(cameraIp)),
                                    new ParamInfo("@CameraPort", DbType.Int32, cameraPort),
                                    new ParamInfo("@ConnectMode", DbType.String, CommonUtil.ConvertToDbTypeString(connectMode)),
                                    new ParamInfo("@MediaServerID", DbType.String, CommonUtil.ConvertToDbTypeString(mediaServerId)),
                                    new ParamInfo("@MediaServerExtraParams", DbType.String, CommonUtil.ConvertToDbTypeString(mediaServerExtraParams)),
                                    new ParamInfo("@RtspID", DbType.String, CommonUtil.ConvertToDbTypeString(rtspId)),
                                    new ParamInfo("@RtspPassword", DbType.String, CommonUtil.ConvertToDbTypeString(rtspPassword)),
                                    new ParamInfo("@ControlType", DbType.String, CommonUtil.ConvertToDbTypeString(controlType)),
                                    new ParamInfo("@CameraControlSettingID", DbType.String, CommonUtil.ConvertToDbTypeString(cameraControlSettingId)),
                                    new ParamInfo("@PtzConnectionInfoID", DbType.String, CommonUtil.ConvertToDbTypeString(ptzConnectionInfoId)),
                                    new ParamInfo("@ImageClippingArea", DbType.String, CommonUtil.ConvertToDbTypeString(imageClippingArea)),
                                    new ParamInfo("@Enable", DbType.Byte, enable ? 1 : 0),
                                    new ParamInfo("@ShowAudioLevel", DbType.Byte, showAudioLevel ? 1 : 0),
                                    new ParamInfo("@AnalogCameraMemo", DbType.String, CommonUtil.ConvertToDbTypeString(analogCameraMemo)),
                                    new ParamInfo("@MediaServerPosition", DbType.Int32, mediaServerPosition),
                                    new ParamInfo("@Vendor", DbType.Int32, vendor),
                                    new ParamInfo("@RecordCameraNumber", DbType.Int32, recordCameraNumber),
                                    new ParamInfo("@RdsHost", DbType.String, CommonUtil.ConvertToDbTypeString(rdsHost)),
                                    new ParamInfo("@RdsPort", DbType.Int32, rdsPort),
                                    new ParamInfo("@MultiCameraControllerId", DbType.String, multiCameraControllerId),
                                    new ParamInfo("@ResizingCodec", DbType.String, resizingCodec),
                                    new ParamInfo("@Channel", DbType.String, channel)
                                };

                var result = DataManager.GetInstance.DataHandler.SetExecuteDataSet(query.ToString(), paramInfo);
                return result;
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return ex.Message;
            }
        }

        /// <summary>
        /// The update camera for delete camera control setting.
        /// </summary>
        /// <param name="cameraControlSettingId">
        /// The camera control setting id.
        /// </param>
        /// <returns>
        /// The result data.
        /// </returns>
        public string UpdateCameraForDeleteCameraControlSetting(string cameraControlSettingId)
        {
            try
            {
                var query = new StringBuilder();

                query.Append("UPDATE Camera SET CameraControlSettingID =                                                                                                                         ");
                query.Append("     CASE WHEN CameraControlSettingID = @CameraControlSettingID THEN NULL ELSE CameraControlSettingID END         ");

                var paramInfo = new List<ParamInfo>
                {
                    new ParamInfo("@CameraControlSettingID", DbType.String, CommonUtil.ConvertToDbTypeString(cameraControlSettingId)),
                };

                var result = DataManager.GetInstance.DataHandler.SetExecuteDataSet(query.ToString(), paramInfo);
                return result;
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return ex.Message;
            }
        }

        public string UpdateCameraForDeletePtzConnectionInfo(string ptzConnectionInfoId)
        {
            try
            {
                var query = new StringBuilder();

                query.Append("UPDATE Camera SET PtzConnectionInfoID =                                                                                                              ");
                query.Append("     CASE WHEN PtzConnectionInfoID = @PtzConnectionInfoID THEN NULL ELSE PtzConnectionInfoID END         ");

                var paramInfo = new List<ParamInfo>
                {
                    new ParamInfo("@PtzConnectionInfoID", DbType.String, CommonUtil.ConvertToDbTypeString(ptzConnectionInfoId)),
                };

                var result = DataManager.GetInstance.DataHandler.SetExecuteDataSet(query.ToString(), paramInfo);
                return result;
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return ex.Message;
            }
        }

        public string DeleteCamera(string cameraId)
        {
            try
            {
                const string query = "DELETE Camera WHERE CameraID = @CameraID";

                var paramInfo = new List<ParamInfo>
                                {
                                    new ParamInfo("@CameraID", DbType.String, CommonUtil.ConvertToDbTypeString(cameraId))
                                };

                var result = DataManager.GetInstance.DataHandler.SetExecuteDataSet(query, paramInfo);
                return result;
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return ex.Message;
            }
        }

        public string DeleteCameraTable()
        {
            try
            {
                const string query = "DELETE FROM Camera";

                var result = DataManager.GetInstance.DataHandler.SetExecuteDataSet(query);
                return result;
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return ex.Message;
            }
        }

        public DataSet SelectCamera(string cameraId)
        {
            try
            {
                const string query = "SELECT * FROM Camera WHERE CameraID = @CameraID";

                var paramInfo = new List<ParamInfo>
                                {
                                    new ParamInfo("@CameraID", DbType.String, CommonUtil.ConvertToDbTypeString(cameraId))
                                };

                return DataManager.GetInstance.DataHandler.GetExecuteDataSet(query, paramInfo);
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        /// <summary>
        /// The select all camera with project mapping.
        /// </summary>
        /// <returns>The dataset of select all camera with project mapping.</returns>
        public DataSet SelectAllCameraWithProjectMapping()
        {
            try
            {
                const string query = "SELECT * FROM Camera AS C LEFT OUTER JOIN ProjectMapping AS PM ON C.CameraID = PM.CameraID";

                return DataManager.GetInstance.DataHandler.GetExecuteDataSet(query);
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        #endregion // Camera

        #region Vendor

        public DataSet SelectAllVendor()
        {
            try
            {
                const string query = "SELECT * FROM VendorInfo";

                return DataManager.GetInstance.DataHandler.GetExecuteDataSet(query);
            }
            catch
            {
                return null;
            }
        }

        #endregion // Vendor

        #region CameraVideoMapping

        public string InsertCameraVideoMapping(string cameraId, string videoSpecId)
        {
            try
            {
                const string query = "INSERT INTO CameraVideoMapping VALUES(@CameraId, @VideoSpecId)";

                var paramInfo = new List<ParamInfo>
                                {
                                    new ParamInfo("@CameraId", DbType.String, CommonUtil.ConvertToDbTypeString(cameraId)),
                                    new ParamInfo("@VideoSpecId", DbType.String, CommonUtil.ConvertToDbTypeString(videoSpecId))
                                };

                var result = DataManager.GetInstance.DataHandler.SetExecuteDataSet(query, paramInfo);
                return result;
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return ex.Message;
            }
        }


        public void DeleteCameraVideoMapping(string cameraId, string videoSpecId)
        {
            try
            {
                const string query = "DELETE CameraVideoMapping WHERE CameraID = @CameraId AND VideoSpecID = @VideoSpecId";

                var paramInfo = new List<ParamInfo>
                                {
                                    new ParamInfo("@CameraId", DbType.String, CommonUtil.ConvertToDbTypeString(cameraId)),
                                    new ParamInfo("@VideoSpecId", DbType.String, CommonUtil.ConvertToDbTypeString(videoSpecId))
                                };

                DataManager.GetInstance.DataHandler.SetExecuteDataSet(query, paramInfo);
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return;
            }
        }

        public string DeleteCameraVideoMappingForDeleteVideoSpec(string videoSpecId)
        {
            try
            {
                const string query = "DELETE FROM CameraVideoMapping WHERE VideoSpecID = @VideoSpecId";

                var paramInfo = new List<ParamInfo>
                {
                    new ParamInfo("@VideoSpecId", DbType.String, CommonUtil.ConvertToDbTypeString(videoSpecId))
                };

                var result = DataManager.GetInstance.DataHandler.SetExecuteDataSet(query, paramInfo);
                return result;
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return ex.Message;
            }
        }

        public string DeleteCameraVideoMapping(string cameraId)
        {
            try
            {
                const string query = "DELETE CameraVideoMapping WHERE CameraID = @CameraId";

                var paramInfo = new List<ParamInfo>
                                {
                                    new ParamInfo("@CameraId", DbType.String, CommonUtil.ConvertToDbTypeString(cameraId))
                                };

                var result = DataManager.GetInstance.DataHandler.SetExecuteDataSet(query, paramInfo);
                return result;
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return ex.Message;
            }
        }

        public string DeleteCameraVideoMappingTable()
        {
            try
            {
                const string query = "DELETE FROM CameraVideoMapping";

                var result = DataManager.GetInstance.DataHandler.SetExecuteDataSet(query);
                return result;
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return ex.Message;
            }
        }

        public DataSet SelectAllCameraVideoMapping()
        {
            try
            {
                const string query = "SELECT * FROM CameraVideoMapping";

                return DataManager.GetInstance.DataHandler.GetExecuteDataSet(query);
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        #endregion // CameraVideoMapping

        #region Project

        public string InsertProjcet(string projectId, string projectName, string projectDescription)
        {
            try
            {
                const string query = "INSERT INTO Project VALUES(@ProjectId, @ProjectName, @ProjectDescription)";

                var paramInfo = new List<ParamInfo>
                                {
                                    new ParamInfo("@ProjectId", DbType.String, CommonUtil.ConvertToDbTypeString(projectId)),
                                    new ParamInfo("@ProjectName", DbType.String, CommonUtil.ConvertToDbTypeString(projectName)),
                                    new ParamInfo("@ProjectDescription", DbType.String, CommonUtil.ConvertToDbTypeString(projectDescription))
                                };

                var result = DataManager.GetInstance.DataHandler.SetExecuteDataSet(query, paramInfo);
                return result;
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return ex.Message;
            }
        }

        public string ImportProjcet(string projectId, string projectName, string projectDescription, int count)
        {
            try
            {
                var result = DataManager.GetInstance.DataHandler.SetExecuteProcedureDataSet("SP_SetData_Manager_ImportProjectInfo", CommonUtil.ConvertToDbTypeString(projectId), CommonUtil.ConvertToDbTypeString(projectName), CommonUtil.ConvertToDbTypeString(projectDescription), count);

                return result;
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return ex.Message;
            }
        }

        public string UpdateProjcet(string projectId, string projectName, string projectDescription)
        {
            try
            {
                const string query = "UPDATE Project SET ProjectName = @ProjectName, ProjectDescription = @ProjectDescription WHERE ProjectID = @ProjectId";

                var paramInfo = new List<ParamInfo>
                {
                    new ParamInfo("@ProjectId", DbType.String, CommonUtil.ConvertToDbTypeString(projectId)),
                    new ParamInfo("@ProjectName", DbType.String, CommonUtil.ConvertToDbTypeString(projectName)),
                    new ParamInfo("@ProjectDescription", DbType.String, CommonUtil.ConvertToDbTypeString(projectDescription))
                };

                var result = DataManager.GetInstance.DataHandler.SetExecuteDataSet(query, paramInfo);
                return result;
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return ex.Message;
            }
        }

        public string DeleteProjcet(string projectId)
        {
            try
            {
                const string query = "DELETE Project WHERE ProjectID = @ProjectId";

                var paramInfo = new List<ParamInfo>
                {
                    new ParamInfo("@ProjectId", DbType.String, CommonUtil.ConvertToDbTypeString(projectId))
                };

                var result = DataManager.GetInstance.DataHandler.SetExecuteDataSet(query, paramInfo);
                return result;
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return ex.Message;
            }
        }

        public DataSet SelectProject(string projectId)
        {
            try
            {
                const string query = "SELECT * FROM Project WHERE ProjectID = @ProjectId";

                var paramInfo = new List<ParamInfo>
                                {
                                    new ParamInfo("@ProjectId", DbType.String, CommonUtil.ConvertToDbTypeString(projectId))
                                };

                return DataManager.GetInstance.DataHandler.GetExecuteDataSet(query, paramInfo);
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        #endregion // Project

        #region VideoSpec

        public string InsertVideoSpec(string videoSpecId, string videoSpecName, int width, int height, int fps, string codec, string rtspUrl, bool isPOD, int bitrate, string extraParams)
        {
            try
            {
                const string query = "INSERT INTO VideoSpec VALUES(@VideoSpecId, @VideoSpecName, @Width, @Height, @Fps, @Codec, @RtspUrl, @IsPOD, @Bitrate, @ExtraParams)";

                var paramInfo = new List<ParamInfo>
                                {
                                    new ParamInfo("@VideoSpecId", DbType.String, CommonUtil.ConvertToDbTypeString(videoSpecId)),
                                    new ParamInfo("@VideoSpecName", DbType.String, CommonUtil.ConvertToDbTypeString(videoSpecName)),
                                    new ParamInfo("@Width", DbType.Int32, width),
                                    new ParamInfo("@Height", DbType.Int32, height),
                                    new ParamInfo("@Fps", DbType.Int32, fps),
                                    new ParamInfo("@Codec", DbType.String, CommonUtil.ConvertToDbTypeString(codec)),
                                    new ParamInfo("@RtspUrl", DbType.String, CommonUtil.ConvertToDbTypeString(rtspUrl)),
                                    new ParamInfo("@IsPOD", DbType.Byte, isPOD ? 1 : 0),
                                    new ParamInfo("@Bitrate", DbType.Int32, bitrate),
                                    new ParamInfo("@ExtraParams", DbType.String, CommonUtil.ConvertToDbTypeString(extraParams)),
                                };

                var result = DataManager.GetInstance.DataHandler.SetExecuteDataSet(query, paramInfo);
                return result;
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return ex.Message;
            }
        }

        public string ImportVideoSpec(string videoSpecId, string videoSpecName, int width, int height, int fps, string codec, string rtspUrl, bool isPOD, int bitrate, string extraParams, int count)
        {
            try
            {
                var result = DataManager.GetInstance.DataHandler.SetExecuteProcedureDataSet("SP_SetData_Manager_ImportVideoSpecInfo", videoSpecId, videoSpecName, width, height, fps, codec, rtspUrl, isPOD, bitrate, extraParams, count);

                return result;
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return ex.Message;
            }
        }

        public string UpdateVideoSpec(string videoSpecId, string videoSpecName, int width, int height, int fps, string codec, string rtspUrl, bool isPOD, int bitrate, string extraParams)
        {
            try
            {
                var query = new StringBuilder();

                query.Append("UPDATE VideoSpec                                                                                                      ");
                query.Append("SET VideoSpecName = @VideoSpecName, Width = @Width, Height = @Height, FPS = @Fps, Codec = @Codec, RtspUrl = @RtspUrl, IsPOD = @IsPOD,  ");
                query.Append("Bitrate = @Bitrate, ExtraParams = @ExtraParams  ");
                query.Append("WHERE VideoSpecID = @VideoSpecId                                                                                      ");

                var paramInfo = new List<ParamInfo>
                                {
                                    new ParamInfo("@VideoSpecId", DbType.String, CommonUtil.ConvertToDbTypeString(videoSpecId)),
                                    new ParamInfo("@VideoSpecName", DbType.String, CommonUtil.ConvertToDbTypeString(videoSpecName)),
                                    new ParamInfo("@Width", DbType.Int32, width),
                                    new ParamInfo("@Height", DbType.Int32, height),
                                    new ParamInfo("@Fps", DbType.Int32, fps),
                                    new ParamInfo("@Codec", DbType.String, CommonUtil.ConvertToDbTypeString(codec)),
                                    new ParamInfo("@RtspUrl", DbType.String, CommonUtil.ConvertToDbTypeString(rtspUrl)),
                                    new ParamInfo("@IsPOD", DbType.Byte, isPOD ? 1 : 0),
                                    new ParamInfo("@Bitrate", DbType.Int32, bitrate),
                                    new ParamInfo("@ExtraParams", DbType.String, CommonUtil.ConvertToDbTypeString(extraParams)),
                                };

                var result = DataManager.GetInstance.DataHandler.SetExecuteDataSet(query.ToString(), paramInfo);
                return result;
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return ex.Message;
            }
        }

        public string DeleteVideoSpec(string videoSpecId)
        {
            try
            {
                const string query = "DELETE VideoSpec WHERE VideoSpecID = @VideoSpecId";

                var paramInfo = new List<ParamInfo>
                                {
                                    new ParamInfo("@VideoSpecId", DbType.String, CommonUtil.ConvertToDbTypeString(videoSpecId)),
                                };

                var result = DataManager.GetInstance.DataHandler.SetExecuteDataSet(query, paramInfo);
                return result;
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return ex.Message;
            }
        }

        public string DeleteVideoSpecTable()
        {
            try
            {
                const string query = "DELETE FROM VideoSpec";

                var result = DataManager.GetInstance.DataHandler.SetExecuteDataSet(query);
                return result;
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return ex.Message;
            }
        }

        public DataSet SelectVideoSpec(string videoSpecId)
        {
            try
            {
                const string query = "SELECT * FROM VideoSpec WHERE VideoSpecID = @VideoSpecId";

                var paramInfo = new List<ParamInfo>
                                {
                                    new ParamInfo("@VideoSpecId", DbType.String, CommonUtil.ConvertToDbTypeString(videoSpecId)),
                                };

                return DataManager.GetInstance.DataHandler.GetExecuteDataSet(query, paramInfo);
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        #endregion // VideoSpec

        #region PtzConnectionInfo

        public string InsertPtzConnectionInfo(string ptzConnectionInfoId, string connectIp, int? connectPort, string connectId, string connectPassword, string serialPort, int? baudRate, int? parityBit, int? dataCount, int? stopBit, string channel, int? audioPort, string groupSelectData)
        {
            try
            {
                const string query = "INSERT INTO PtzConnectionInfo VALUES (@PtzConnectionInfoID, @ConnectIP, @ConnectPort, @AudioPort, @ConnectID, @ConnectPassword, @SerialPort, @BaudRate, @ParityBit, @DataCount, @StopBit, @Channel, @GroupSelectData)";

                var paramInfo = new List<ParamInfo>
                                {
                                    new ParamInfo("@PtzConnectionInfoID", DbType.String, CommonUtil.ConvertToDbTypeString(ptzConnectionInfoId)),
                                    new ParamInfo("@ConnectIP", DbType.String, CommonUtil.ConvertToDbTypeString(connectIp)),
                                    new ParamInfo("@ConnectPort", DbType.Int32, connectPort),
                                    new ParamInfo("@ConnectID", DbType.String, CommonUtil.ConvertToDbTypeString(connectId)),
                                    new ParamInfo("@ConnectPassword", DbType.String, CommonUtil.ConvertToDbTypeString(connectPassword)),
                                    new ParamInfo("@SerialPort", DbType.String, CommonUtil.ConvertToDbTypeString(serialPort)),
                                    new ParamInfo("@BaudRate", DbType.Int32, baudRate),
                                    new ParamInfo("@ParityBit", DbType.Int32, parityBit),
                                    new ParamInfo("@DataCount", DbType.Int32, dataCount),
                                    new ParamInfo("@StopBit", DbType.Int32, stopBit),
                                    new ParamInfo("@Channel", DbType.String, channel),
                                    new ParamInfo("@AudioPort", DbType.Int32, audioPort),
                                    new ParamInfo("@GroupSelectData", DbType.String, CommonUtil.ConvertToDbTypeString(groupSelectData)),
                                };

                var result = DataManager.GetInstance.DataHandler.SetExecuteDataSet(query, paramInfo);
                return result;
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return ex.Message;
            }
        }

        public string ImportPtzConnectionInfo(string ptzConnectionInfoId, string connectIp, int? connectPort, string connectId, string connectPassword, string serialPort, int? baudRate, int? parityBit, int? dataCount, int? stopBit, string channel, int? audioPort, string groupSelectData, int count)
        {
            try
            {
                var result = DataManager.GetInstance.DataHandler.SetExecuteProcedureDataSet("SP_SetData_Manager_ImportPtzConnectionInfo", ptzConnectionInfoId, connectIp, connectPort, audioPort, connectId, connectPassword, serialPort, baudRate, parityBit, dataCount, stopBit, channel, groupSelectData, count);
                return result;
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return ex.Message;
            }
        }

        public string UpdatePtzConnectionInfo(
            string ptzConnectionInfoId,
            string connectIp,
            int? connectPort,
            string connectId,
            string connectPassword,
            string serialPort,
            int? baudRate,
            int? parityBit,
            int? dataCount,
            int? stopBit,
            string channel,
            int? audioPort,
            string groupSelectData)
        {
            try
            {
                var query = new StringBuilder();

                query.Append("UPDATE PtzConnectionInfo                                                                                              ");
                query.Append("SET ConnectIP = @ConnectIP, ConnectPort = @ConnectPort, ConnectID = @ConnectID, ConnectPassword = @ConnectPassword,   ");
                query.Append("    SerialPort = @SerialPort, BaudRate = @BaudRate, ParityBit = @ParityBit, DataCount = @DataCount,                   ");
                query.Append("    StopBit = @StopBit, Channel = @Channel, AudioPort = @AudioPort, GroupSelectData = @GroupSelectData                ");
                query.Append("WHERE PtzConnectionInfoID = @PtzConnectionInfoID                                                                      ");

                var paramInfo = new List<ParamInfo>
                                {
                                    new ParamInfo("@PtzConnectionInfoID", DbType.String, CommonUtil.ConvertToDbTypeString(ptzConnectionInfoId)),
                                    new ParamInfo("@ConnectIP", DbType.String, CommonUtil.ConvertToDbTypeString(connectIp)),
                                    new ParamInfo("@ConnectPort", DbType.Int32, connectPort),
                                    new ParamInfo("@ConnectID", DbType.String, CommonUtil.ConvertToDbTypeString(connectId)),
                                    new ParamInfo("@ConnectPassword", DbType.String, CommonUtil.ConvertToDbTypeString(connectPassword)),
                                    new ParamInfo("@SerialPort", DbType.String, CommonUtil.ConvertToDbTypeString(serialPort)),
                                    new ParamInfo("@BaudRate", DbType.Int32, baudRate),
                                    new ParamInfo("@ParityBit", DbType.Int32, parityBit),
                                    new ParamInfo("@DataCount", DbType.Int32, dataCount),
                                    new ParamInfo("@StopBit", DbType.Int32, stopBit),
                                    new ParamInfo("@Channel", DbType.String, channel),
                                    new ParamInfo("@AudioPort", DbType.Int32, audioPort),
                                    new ParamInfo("@GroupSelectData", DbType.String, CommonUtil.ConvertToDbTypeString(groupSelectData)),
                                };

                var result = DataManager.GetInstance.DataHandler.SetExecuteDataSet(query.ToString(), paramInfo);
                return result;
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return ex.Message;
            }
        }

        public string DeletePtzConnectionInfo(string ptzConnectionInfoId)
        {
            try
            {
                const string query = "DELETE PtzConnectionInfo WHERE PtzConnectionInfoID = @PtzConnectionInfoId";

                var paramInfo = new List<ParamInfo>
                                {
                                    new ParamInfo("@PtzConnectionInfoID", DbType.String, CommonUtil.ConvertToDbTypeString(ptzConnectionInfoId)),
                                };

                var result = DataManager.GetInstance.DataHandler.SetExecuteDataSet(query, paramInfo);
                return result;
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return ex.Message;
            }
        }

        #endregion // PtzConnectionInfo

        #region CameraControlSetting

        public string InsertCameraControlSetting(
            string cameraControlSettingId,
            string moduleFileName,
            bool usePanTilt,
            bool useSpeed,
            bool useZoom,
            bool useFocus,
            bool useIris,
            bool usePreset,
            int presetCount,
            bool useCameraPower,
            bool useLightPower,
            bool useAux1Power,
            bool useAux2Power,
            int audioMode,
            string description)
        {
            try
            {
                const string query = "INSERT INTO CameraControlSetting VALUES (@CameraControlSettingID, @ModuleFileName, @UsePanTilt, @UseSpeed, @UseZoom, @UseFocus, @UseIris, @UsePreset, @PresetCount, @UseCameraPower, @UseLightPower, @UseAux1Power, @UseAux2Power, @AudioMode, @Description)";

                var paramInfo = new List<ParamInfo>
                                {
                                    new ParamInfo("@CameraControlSettingID", DbType.String, CommonUtil.ConvertToDbTypeString(cameraControlSettingId)),
                                    new ParamInfo("@ModuleFileName", DbType.String, CommonUtil.ConvertToDbTypeString(moduleFileName)),
                                    new ParamInfo("@UsePanTilt", DbType.Byte, usePanTilt ? 1 : 0),
                                    new ParamInfo("@UseSpeed", DbType.Byte, useSpeed ? 1 : 0),
                                    new ParamInfo("@UseZoom", DbType.Byte, useZoom ? 1 : 0),
                                    new ParamInfo("@UseFocus", DbType.Byte, useFocus ? 1 : 0),
                                    new ParamInfo("@UseIris", DbType.Byte, useIris ? 1 : 0),
                                    new ParamInfo("@UsePreset", DbType.Byte, usePreset ? 1 : 0),
                                    new ParamInfo("@PresetCount", DbType.Int32, presetCount),
                                    new ParamInfo("@UseCameraPower", DbType.Byte, useCameraPower ? 1 : 0),
                                    new ParamInfo("@UseLightPower", DbType.Byte, useLightPower ? 1 : 0),
                                    new ParamInfo("@UseAux1Power", DbType.Byte, useAux1Power ? 1 : 0),
                                    new ParamInfo("@UseAux2Power", DbType.Byte, useAux2Power ? 1 : 0),
                                    new ParamInfo("@AudioMode", DbType.Int16, audioMode),
                                    new ParamInfo("@Description", DbType.String, CommonUtil.ConvertToDbTypeString(description)),
                                };

                var result = DataManager.GetInstance.DataHandler.SetExecuteDataSet(query, paramInfo);
                return result;
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return ex.Message;
            }
        }

        public string ImportCameraControlSetting(
            string cameraControlSettingId,
            string moduleFileName,
            bool usePanTilt,
            bool useSpeed,
            bool useZoom,
            bool useFocus,
            bool useIris,
            bool usePreset,
            int presetCount,
            bool useCameraPower,
            bool useLightPower,
            bool useAux1Power,
            bool useAux2Power,
            int audioMode,
            string description,
            int count)
        {
            try
            {
                var result = DataManager.GetInstance.DataHandler.SetExecuteProcedureDataSet(
                    "SP_SetData_Manager_ImportCameraControlSettingInfo",
                    CommonUtil.ConvertToDbTypeString(cameraControlSettingId),
                    CommonUtil.ConvertToDbTypeString(moduleFileName),
                    usePanTilt ? 1 : 0,
                    useSpeed ? 1 : 0,
                    useZoom ? 1 : 0,
                    useFocus ? 1 : 0,
                    useIris ? 1 : 0,
                    usePreset ? 1 : 0,
                    presetCount,
                    useCameraPower ? 1 : 0,
                    useLightPower ? 1 : 0,
                    useAux1Power ? 1 : 0,
                    useAux2Power ? 1 : 0,
                    audioMode,
                    CommonUtil.ConvertToDbTypeString(description),
                    count);

                return result;
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return ex.Message;
            }
        }

        public string UpdateCameraControlSetting(
            string cameraControlSettingId,
            string moduleFileName,
            bool usePanTilt,
            bool useSpeed,
            bool useZoom,
            bool useFocus,
            bool useIris,
            bool usePreset,
            int presetCount,
            bool useCameraPower,
            bool useLightPower,
            bool useAux1Power,
            bool useAux2Power,
            int audioMode,
            string description)
        {
            try
            {
                var query = new StringBuilder();

                query.Append("UPDATE CameraControlSetting                                                                                                      ");
                query.Append("SET CameraControlSettingID = @CameraControlSettingID, ModuleFileName = @ModuleFileName, UsePanTilt = @UsePanTilt, UseSpeed = @UseSpeed, UseZoom = @UseZoom, UseFocus = @UseFocus, UseIris = @UseIris, UsePreset = @UsePreset, PresetCount = @PresetCount, UseCameraPower = @UseCameraPower, UseLightPower = @UseLightPower, UseAux1Power = @UseAux1Power, UseAux2Power = @UseAux2Power, AudioMode = @AudioMode, Description = @Description  ");
                query.Append("WHERE CameraControlSettingID = @CameraControlSettingID                                                                                      ");

                var paramInfo = new List<ParamInfo>
                                {
                                    new ParamInfo("@CameraControlSettingID", DbType.String, CommonUtil.ConvertToDbTypeString(cameraControlSettingId)),
                                    new ParamInfo("@ModuleFileName", DbType.String, CommonUtil.ConvertToDbTypeString(moduleFileName)),
                                    new ParamInfo("@UsePanTilt", DbType.Byte, usePanTilt ? 1 : 0),
                                    new ParamInfo("@UseSpeed", DbType.Byte, useSpeed ? 1 : 0),
                                    new ParamInfo("@UseZoom", DbType.Byte, useZoom ? 1 : 0),
                                    new ParamInfo("@UseFocus", DbType.Byte, useFocus ? 1 : 0),
                                    new ParamInfo("@UseIris", DbType.Byte, useIris ? 1 : 0),
                                    new ParamInfo("@UsePreset", DbType.Byte, usePreset ? 1 : 0),
                                    new ParamInfo("@PresetCount", DbType.Int32, presetCount),
                                    new ParamInfo("@UseCameraPower", DbType.Byte, useCameraPower ? 1 : 0),
                                    new ParamInfo("@UseLightPower", DbType.Byte, useLightPower ? 1 : 0),
                                    new ParamInfo("@UseAux1Power", DbType.Byte, useAux1Power ? 1 : 0),
                                    new ParamInfo("@UseAux2Power", DbType.Byte, useAux2Power ? 1 : 0),
                                    new ParamInfo("@AudioMode", DbType.Int16, audioMode),
                                    new ParamInfo("@Description", DbType.String, CommonUtil.ConvertToDbTypeString(description)),
                                };

                var result = DataManager.GetInstance.DataHandler.SetExecuteDataSet(query.ToString(), paramInfo);
                return result;
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return ex.Message;
            }
        }

        public string DeleteCameraControlSetting(string cameraControlSettingId)
        {
            try
            {
                const string query = "DELETE CameraControlSetting WHERE CameraControlSettingID = @CameraControlSettingID";

                var paramInfo = new List<ParamInfo>
                                {
                                    new ParamInfo("@CameraControlSettingID", DbType.String, CommonUtil.ConvertToDbTypeString(cameraControlSettingId)),
                                };

                var result = DataManager.GetInstance.DataHandler.SetExecuteDataSet(query, paramInfo);
                return result;
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return ex.Message;
            }
        }

        #endregion // CameraControlSetting

        #region CameraControllerService

        /// <summary>
        /// 카메라 컨트롤러 서비스에서 모듈 목록 가져오기
        /// </summary>
        /// <returns></returns>
        public List<string> GetCameraControllerModules()
        {
            return new List<string>
            {
                "Axis",
                "사랑해",
                "미워해"
            };
        }

        #endregion // CameraControllerService

        #region ProjectMapping

        public string InsertProjectMapping(string projectId, string cameraId, string projectCameraId, string projectCameraName)
        {
            try
            {
                const string query = "INSERT INTO ProjectMapping VALUES(@ProjectId, @CameraId, @ProjectCameraId, @ProjectCameraName)";

                var paramInfo = new List<ParamInfo>
                                {
                                    new ParamInfo("@ProjectId", DbType.String, CommonUtil.ConvertToDbTypeString(projectId)),
                                    new ParamInfo("@CameraId", DbType.String, CommonUtil.ConvertToDbTypeString(cameraId)),
                                    new ParamInfo("@ProjectCameraId", DbType.String, CommonUtil.ConvertToDbTypeString(projectCameraId)),
                                    new ParamInfo("@ProjectCameraName", DbType.String, CommonUtil.ConvertToDbTypeString(projectCameraName))
                                };

                var result = DataManager.GetInstance.DataHandler.SetExecuteDataSet(query, paramInfo);
                return result;
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return ex.Message;
            }
        }

        public void UpdateProjectMapping(string projectId, string cameraId, string projectCameraId, string projectCameraName)
        {
            try
            {
                var query = new StringBuilder();

                query.Append("UPDATE ProjectMapping                                                                                 ");
                query.Append("SET CameraID = @CameraId, ProjectCameraID = @ProjectCameraId, ProjectCameraName = @ProjectCameaName   ");
                query.Append("WHERE ProjectID = @ProjectId                                                                          ");

                var paramInfo = new List<ParamInfo>
                                {
                                    new ParamInfo("@ProjectId", DbType.String, CommonUtil.ConvertToDbTypeString(projectId)),
                                    new ParamInfo("@CameraId", DbType.String, CommonUtil.ConvertToDbTypeString(cameraId)),
                                    new ParamInfo("@ProjectCameraId", DbType.String, CommonUtil.ConvertToDbTypeString(projectCameraId)),
                                    new ParamInfo("@ProjectCameraName", DbType.String, CommonUtil.ConvertToDbTypeString(projectCameraName))
                                };

                DataManager.GetInstance.DataHandler.SetExecuteDataSet(query.ToString(), paramInfo);
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return;
            }
        }

        public void DeleteProjectMapping(string projectId)
        {
            try
            {
                const string query = "DELETE ProjectMapping WHERE ProjectID = @ProjectID";

                var paramInfo = new List<ParamInfo>
                                {
                                    new ParamInfo("@ProjectID", DbType.String, CommonUtil.ConvertToDbTypeString(projectId))
                                };

                DataManager.GetInstance.DataHandler.SetExecuteDataSet(query, paramInfo);
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return;
            }
        }

        public string DeleteProjectMapping(string projectId, string cameraId)
        {
            try
            {
                const string query = "DELETE ProjectMapping WHERE ProjectID = @ProjectID AND CameraID = @CameraID";

                var paramInfo = new List<ParamInfo>
                                {
                                    new ParamInfo("@ProjectID", DbType.String, CommonUtil.ConvertToDbTypeString(projectId)),
                                    new ParamInfo("@CameraID", DbType.String, CommonUtil.ConvertToDbTypeString(cameraId)),
                                };

                var result = DataManager.GetInstance.DataHandler.SetExecuteDataSet(query, paramInfo);
                return result;
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return ex.Message;
            }
        }

        public string DeleteProjectMappingTable()
        {
            try
            {
                const string query = "DELETE FROM ProjectMapping";

                var result = DataManager.GetInstance.DataHandler.SetExecuteDataSet(query);
                return result;
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return ex.Message;
            }
        }

        public DataSet SelectAllProjectMapping()
        {
            try
            {
                const string query = "SELECT * FROM ProjectMapping";

                return DataManager.GetInstance.DataHandler.GetExecuteDataSet(query);
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        public DataSet SelectProjectMappingWithoutProjectIdColumn(string projectId)
        {
            try
            {
                const string query = "SELECT CameraID, ProjectCameraID, ProjectCameraName FROM ProjectMapping WHERE ProjectID = @ProjectID";

                var paramInfo = new List<ParamInfo>
                                {
                                    new ParamInfo("@ProjectID", DbType.String, CommonUtil.ConvertToDbTypeString(projectId))
                                };

                return DataManager.GetInstance.DataHandler.GetExecuteDataSet(query, paramInfo);
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        /// <summary>
        /// The select camera id by project camera id.
        /// </summary>
        /// <param name="cameraId">The project camera id.(CM에서 사용하는 camera id)</param>
        /// <returns>The dataset of camera id by project camera id.</returns>
        public DataSet SelectCameraIdByProjectCameraId(string cameraId)
        {
            try
            {
                const string query = "SELECT * FROM ProjectMapping WHERE ProjectCameraID = @CameraId";

                var paramInfo = new List<ParamInfo>
                                    {
                                        new ParamInfo("@CameraId", DbType.String, CommonUtil.ConvertToDbTypeString(cameraId))
                                    };

                return DataManager.GetInstance.DataHandler.GetExecuteDataSet(query, paramInfo);
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        #endregion // ProjectMapping

        #region ViewServer

        /// <summary>
        /// The insert view server.
        /// </summary>
        /// <param name="viewServerId">
        /// The view Server Id.
        /// </param>
        /// <param name="viewServerIp">
        /// The view Server Ip.
        /// </param>
        /// <param name="viewServerPort">
        /// The view Server Port.
        /// </param>
        /// <param name="serviceUrl">
        /// The event Service Url.
        /// </param>
        /// <param name="viewServerType">
        /// The view Server Type.
        /// </param>
        /// <param name="description">
        /// The description.
        /// </param>
        /// <param name="activeCode">
        /// The active Code.
        /// </param>
        /// <returns>
        /// The insert view server.
        /// </returns>
        public string InsertViewServer(
            string viewServerId,
            string viewServerIp,
            int? viewServerPort,
            string serviceUrl,
            string viewServerType,
            string description,
            string activeCode)
        {
            try
            {
                var query = new StringBuilder();
                query.Append("INSERT INTO ");
                query.Append("ViewServer(ViewServerID, ViewServerIP, ViewServerPort, ServiceUrl, ViewServerType, ViewServerDescription, ActiveCode, DefaultConfig, Config) ");
                query.Append("VALUES (@ViewServerId, @ViewServerIp, @ViewServerPort, @ServiceUrl, @ViewServerType, @Description, @ActiveCode, @DefaultConfig, @Config) ");

                var paramInfo = new List<ParamInfo>
                {
                    new ParamInfo("@ViewServerId", DbType.String, CommonUtil.ConvertToDbTypeString(viewServerId)),
                    new ParamInfo("@ViewServerIp", DbType.String, CommonUtil.ConvertToDbTypeString(viewServerIp)),
                    new ParamInfo("@ViewServerPort", DbType.Int32, viewServerPort),
                    new ParamInfo("@ServiceUrl", DbType.String, CommonUtil.ConvertToDbTypeString(serviceUrl)),
                    new ParamInfo("@ViewServerType", DbType.String, CommonUtil.ConvertToDbTypeString(viewServerType)),
                    new ParamInfo("@Description", DbType.String, CommonUtil.ConvertToDbTypeString(description)),                                   
                    new ParamInfo("@ActiveCode", DbType.String, CommonUtil.ConvertToDbTypeString(activeCode)),
                    new ParamInfo("@DefaultConfig", DbType.String, DBNull.Value),
                    new ParamInfo("@Config", DbType.String, DBNull.Value)
                };

                var result = DataManager.GetInstance.DataHandler.SetExecuteDataSet(query.ToString(), paramInfo);
                return result;
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return ex.Message;
            }
        }

        public string ImportViewServer(
            string viewServerId,
            string viewServerIp,
            int? viewServerPort,
            string serviceUrl,
            string viewServerType,
            string description,
            string activeCode,
            int count)
        {
            try
            {
                var result = DataManager.GetInstance.DataHandler.SetExecuteProcedureDataSet(
                    "SP_SetData_Manager_ImportViewServerInfo", viewServerId, viewServerIp, viewServerPort, serviceUrl, viewServerType, description, activeCode, DBNull.Value, DBNull.Value, count);

                return result;
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return ex.Message;
            }
        }

        /// <summary>
        /// The update view server.
        /// </summary>
        /// <param name="viewServerId">
        /// The view Server Id.
        /// </param>
        /// <param name="viewServerIp">
        /// The view Server Ip.
        /// </param>
        /// <param name="viewServerPort">
        /// The view Server Port.
        /// </param>
        /// <param name="serviceUrl">
        /// The event Service Url.
        /// </param>
        /// <param name="viewServerType">
        /// The view Server Type.
        /// </param>
        /// <param name="description">
        /// The description.
        /// </param>
        /// <param name="activeCode">
        /// The active Code.
        /// </param>
        /// <returns>
        /// The update view server.
        /// </returns>
        public string UpdateViewServer(
            string viewServerId,
            string viewServerIp,
            int? viewServerPort,
            string serviceUrl,
            string viewServerType,
            string description,
            string activeCode)
        {
            try
            {
                var query = new StringBuilder();
                query.Append("UPDATE ViewServer SET ");
                query.Append("ViewServerIP = @ViewServerIp, ViewServerPort = @ViewServerPort, ViewServerType = @ViewServerType, ServiceUrl = @ServiceUrl, ");
                query.Append("ViewServerDescription = @Description, ActiveCode = @ActiveCode WHERE ViewServerID = @ViewServerId");

                var paramInfo = new List<ParamInfo>
                                {
                                    new ParamInfo("@ViewServerId", DbType.String, CommonUtil.ConvertToDbTypeString(viewServerId)),
                                    new ParamInfo("@ViewServerIp", DbType.String, CommonUtil.ConvertToDbTypeString(viewServerIp)),
                                    new ParamInfo("@ViewServerPort", DbType.Int32, viewServerPort),
                                    new ParamInfo("@ServiceUrl", DbType.String, CommonUtil.ConvertToDbTypeString(serviceUrl)),
                                    new ParamInfo("@ViewServerType", DbType.String, CommonUtil.ConvertToDbTypeString(viewServerType)),
                                    new ParamInfo("@Description", DbType.String, CommonUtil.ConvertToDbTypeString(description)),
                                    new ParamInfo("@ActiveCode", DbType.String, CommonUtil.ConvertToDbTypeString(activeCode))
                                };

                var result = DataManager.GetInstance.DataHandler.SetExecuteDataSet(query.ToString(), paramInfo);
                return result;
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return ex.Message;
            }
        }

        /// <summary>
        /// The get view server default confg
        /// </summary>
        /// <param name="ip">
        /// The ip.
        /// </param>
        /// <returns>
        /// The response data.
        /// </returns>
        public string GetViewServerDefaultConfg(string ip)
        {
            try
            {
                const string query = "SELECT DefaultConfig FROM ViewServer WHERE ViewServerIP = @ViewServerIp";

                var paramInfo = new List<ParamInfo>
                                {
                                    new ParamInfo("@ViewServerIp", DbType.String, CommonUtil.ConvertToDbTypeString(ip)),
                                };

                var ds = DataManager.GetInstance.DataHandler.GetExecuteDataSet(query, paramInfo);

                if (ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        return ds.Tables[0].Rows[0]["DefaultConfig"].ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return string.Empty;
            }

            return string.Empty;
        }

        /// <summary>
        /// The get view server confg.
        /// </summary>
        /// <param name="ip">
        /// The ip.
        /// </param>
        /// <returns>
        /// The response data.
        /// </returns>
        public string GetViewServerConfg(string ip)
        {
            try
            {
                const string Query = "SELECT Config FROM ViewServer WHERE ViewServerIP = @ViewServerIp";

                var paramInfo = new List<ParamInfo>
                                {
                                    new ParamInfo("@ViewServerIp", DbType.String, CommonUtil.ConvertToDbTypeString(ip)),
                                };

                var ds = DataManager.GetInstance.DataHandler.GetExecuteDataSet(Query, paramInfo);

                if (ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        return ds.Tables[0].Rows[0]["Config"].ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return string.Empty;
            }

            return string.Empty;
        }

        /// <summary>
        /// The update view server default confg.
        /// </summary>
        /// <param name="ip">
        /// The ip.
        /// </param>
        /// <param name="content">
        /// The content.
        /// </param>
        public void UpdateViewServerDefaultConfg(string ip, string content)
        {
            try
            {
                const string Query = "UPDATE ViewServer SET DefaultConfig = @DefaultConfig WHERE ViewServerIP = @ViewServerIp";

                var paramInfo = new List<ParamInfo>
                                {
                                    new ParamInfo("@ViewServerIp", DbType.String, CommonUtil.ConvertToDbTypeString(ip)),
                                    new ParamInfo("@DefaultConfig", DbType.String, CommonUtil.ConvertToDbTypeString(content))
                                };

                DataManager.GetInstance.DataHandler.SetExecuteDataSet(Query, paramInfo);
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return;
            }
        }

        /// <summary>
        /// The update view server confg.
        /// </summary>
        /// <param name="ip">
        /// The ip.
        /// </param>
        /// <param name="content">
        /// The content.
        /// </param>
        public void UpdateViewServerConfg(string ip, string content)
        {
            try
            {
                const string Query = "UPDATE ViewServer SET Config = @Config WHERE ViewServerIP = @ViewServerIp";

                var paramInfo = new List<ParamInfo>
                                {
                                    new ParamInfo("@ViewServerIp", DbType.String, CommonUtil.ConvertToDbTypeString(ip)),
                                    new ParamInfo("@Config", DbType.String, CommonUtil.ConvertToDbTypeString(content))
                                };

                DataManager.GetInstance.DataHandler.SetExecuteDataSet(Query, paramInfo);
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return;
            }
        }

        /// <summary>
        /// The delete view server.
        /// </summary>
        /// <param name="viewServerId">
        /// The view server id.
        /// </param>
        /// <returns>
        /// The response data.
        /// </returns>
        public string DeleteViewServer(string viewServerId)
        {
            try
            {
                const string Query = "DELETE ViewServer WHERE ViewServerID = @ViewServerId";

                var paramInfo = new List<ParamInfo>
                {
                    new ParamInfo("@ViewServerId", DbType.String, CommonUtil.ConvertToDbTypeString(viewServerId))
                };

                var result = DataManager.GetInstance.DataHandler.SetExecuteDataSet(Query, paramInfo);
                return result;
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return ex.Message;
            }
        }

        /// <summary>
        /// The set license.
        /// </summary>
        /// <param name="activeCode">
        /// The active code.
        /// </param>
        /// <param name="ip">
        /// The ip.
        /// </param>
        /// <param name="type">
        /// The type.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string SetLicense(string activeCode, string ip, string type)
        {
            try
            {
                const string Query = "UPDATE ViewServer SET ActiveCode = @activeCode WHERE ViewServerIP = @ip AND ViewServerType = @type";

                var paramInfo = new List<ParamInfo>
                {
                    new ParamInfo("@activeCode", DbType.String, CommonUtil.ConvertToDbTypeString(activeCode)),
                    new ParamInfo("@ip", DbType.String, CommonUtil.ConvertToDbTypeString(ip)),
                    new ParamInfo("@type", DbType.String, CommonUtil.ConvertToDbTypeString(type))
                };

                var result = DataManager.GetInstance.DataHandler.SetExecuteDataSet(Query, paramInfo);
                return result;
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return ex.Message;
            }
        }

        /// <summary>
        /// 공통 Default Config 업데이트.
        /// </summary>
        /// <param name="config">Default Config 내용.</param>
        public void UpdateCommonDefaultConfig(string config)
        {

        }

        /// <summary>
        /// 공통 Default Config 내용 가져오기.
        /// </summary>
        /// <returns>Default Config 내용.</returns>
        public string GetCommonDefaultConfig()
        {
            return string.Empty;
        }

        /// <summary>
        /// 공통 Config 업데이트.
        /// </summary>
        /// <param name="config">Config 내용.</param>
        public void UpdateCommonConfig(string config)
        {

        }

        /// <summary>
        /// 공통 Config 내용 가져오기.
        /// </summary>
        /// <returns>Config 내용.</returns>
        public string GetCommonConfig()
        {
            return string.Empty;
        }

        #endregion // ViewServer

        #region Display

        /// <summary>
        /// The insert display.
        /// </summary>
        public string InsertDisplay(
            string displayId,
            string displayIp,
            string serviceUrl,
            string description,
            string activeCode)
        {
            try
            {
                var query = new StringBuilder();
                query.Append("INSERT INTO ");
                query.Append("Display(DisplayID, DisplayIP, ServiceUrl, DisplayDescription, ActiveCode) ");
                query.Append("VALUES (@DisplayId, @DisplayIp, @ServiceUrl, @Description, @ActiveCode) ");

                var paramInfo = new List<ParamInfo>
                {
                    new ParamInfo("@DisplayId", DbType.String, CommonUtil.ConvertToDbTypeString(displayId)),
                    new ParamInfo("@DisplayIp", DbType.String, CommonUtil.ConvertToDbTypeString(displayIp)),
                    new ParamInfo("@ServiceUrl", DbType.String, CommonUtil.ConvertToDbTypeString(serviceUrl)),
                    new ParamInfo("@Description", DbType.String, CommonUtil.ConvertToDbTypeString(description)),                                   
                    new ParamInfo("@ActiveCode", DbType.String, CommonUtil.ConvertToDbTypeString(activeCode))
                };

                var result = DataManager.GetInstance.DataHandler.SetExecuteDataSet(query.ToString(), paramInfo);
                return result;
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return ex.Message;
            }
        }

        public string ImportDisplay(
            string displayId,
            string displayIp,
            string serviceUrl,
            string description,
            string activeCode,
            int count)
        {
            try
            {
                var result = DataManager.GetInstance.DataHandler.SetExecuteProcedureDataSet(
                    "SP_SetData_Manager_ImportDisplayInfo", displayId, displayIp, serviceUrl, description, activeCode, count);

                return result;
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return ex.Message;
            }
        }

        /// <summary>
        /// The update display.
        /// </summary>
        public string UpdateDisplay(
            string displayId,
            string displayIp,
            string serviceUrl,
            string description,
            string activeCode)
        {
            try
            {
                var query = new StringBuilder();
                query.Append("UPDATE Display SET ");
                query.Append("DisplayIP = @DisplayIp, ServiceUrl = @ServiceUrl, ");
                query.Append("DisplayDescription = @Description, ActiveCode = @ActiveCode WHERE DisplayID = @DisplayId");

                var paramInfo = new List<ParamInfo>
                                {
                                    new ParamInfo("@DisplayId", DbType.String, CommonUtil.ConvertToDbTypeString(displayId)),
                                    new ParamInfo("@DisplayIp", DbType.String, CommonUtil.ConvertToDbTypeString(displayIp)),                                    
                                    new ParamInfo("@ServiceUrl", DbType.String, CommonUtil.ConvertToDbTypeString(serviceUrl)),
                                    new ParamInfo("@Description", DbType.String, CommonUtil.ConvertToDbTypeString(description)),
                                    new ParamInfo("@ActiveCode", DbType.String, CommonUtil.ConvertToDbTypeString(activeCode))
                                };

                var result = DataManager.GetInstance.DataHandler.SetExecuteDataSet(query.ToString(), paramInfo);
                return result;
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return ex.Message;
            }
        }
        
        /// <summary>
        /// The delete display.
        /// </summary>
        public string DeleteDisplay(string displayId)
        {
            try
            {
                const string Query = "DELETE Display WHERE DisplayID = @DisplayId";

                var paramInfo = new List<ParamInfo>
                {
                    new ParamInfo("@DisplayId", DbType.String, CommonUtil.ConvertToDbTypeString(displayId))
                };

                var result = DataManager.GetInstance.DataHandler.SetExecuteDataSet(Query, paramInfo);
                return result;
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return ex.Message;
            }
        }

        #endregion // Display

        #region CameraControllerService

        /// <summary>
        /// The insert camera controller service.
        /// </summary>
        public string InsertCameraControllerService(
            string cameraControllerServiceId,
            string cameraControllerServiceIp,
            string serviceUrl,
            string cameraControllerServiceDescription,
            string activeCode)
        {
            try
            {
                var query = new StringBuilder();
                query.Append("INSERT INTO ");
                query.Append("CameraControllerService(CameraControllerServiceID, CameraControllerServiceIP, ServiceUrl, CameraControllerServiceDescription, ActiveCode) ");
                query.Append("VALUES (@CameraControllerServiceId, @CameraControllerServiceIp, @ServiceUrl, @Description, @ActiveCode) ");

                var paramInfo = new List<ParamInfo>
                {
                    new ParamInfo("@CameraControllerServiceId", DbType.String, CommonUtil.ConvertToDbTypeString(cameraControllerServiceId)),
                    new ParamInfo("@CameraControllerServiceIp", DbType.String, CommonUtil.ConvertToDbTypeString(cameraControllerServiceIp)),
                    new ParamInfo("@ServiceUrl", DbType.String, CommonUtil.ConvertToDbTypeString(serviceUrl)),
                    new ParamInfo("@Description", DbType.String, CommonUtil.ConvertToDbTypeString(cameraControllerServiceDescription)),                                   
                    new ParamInfo("@ActiveCode", DbType.String, CommonUtil.ConvertToDbTypeString(activeCode)),
                };

                var result = DataManager.GetInstance.DataHandler.SetExecuteDataSet(query.ToString(), paramInfo);
                return result;
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return ex.Message;
            }
        }

        public string ImportCameraControllerService(
            string cameraControllerServiceId,
            string cameraControllerServiceIp,
            string serviceUrl,
            string cameraControllerServiceDescription,
            string activeCode,
            int count)
        {
            try
            {
                var result = DataManager.GetInstance.DataHandler.SetExecuteProcedureDataSet(
                    "SP_SetData_Manager_ImportCameraControllerServiceInfo", cameraControllerServiceId, cameraControllerServiceIp, cameraControllerServiceDescription, serviceUrl, activeCode, count);

                return result;
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return ex.Message;
            }
        }

        /// <summary>
        /// The update camera controller service.
        /// </summary>
        public string UpdateCameraControllerService(
            string cameraControllerServiceId,
            string cameraControllerServiceIp,
            string serviceUrl,
            string cameraControllerServiceDescription,
            string activeCode)
        {
            try
            {
                var query = new StringBuilder();
                query.Append("UPDATE CameraControllerService SET ");
                query.Append("CameraControllerServiceIP = @CameraControllerServiceIp, ServiceUrl = @ServiceUrl, ");
                query.Append("CameraControllerServiceDescription = @Description, ActiveCode = @ActiveCode WHERE CameraControllerServiceID = @CameraControllerServiceId");

                var paramInfo = new List<ParamInfo>
                                {
                                    new ParamInfo("@CameraControllerServiceId", DbType.String, CommonUtil.ConvertToDbTypeString(cameraControllerServiceId)),
                                    new ParamInfo("@CameraControllerServiceIp", DbType.String, CommonUtil.ConvertToDbTypeString(cameraControllerServiceIp)),
                                    new ParamInfo("@ServiceUrl", DbType.String, CommonUtil.ConvertToDbTypeString(serviceUrl)),
                                    new ParamInfo("@Description", DbType.String, CommonUtil.ConvertToDbTypeString(cameraControllerServiceDescription)),
                                    new ParamInfo("@ActiveCode", DbType.String, CommonUtil.ConvertToDbTypeString(activeCode))
                                };

                var result = DataManager.GetInstance.DataHandler.SetExecuteDataSet(query.ToString(), paramInfo);
                return result;
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return ex.Message;
            }
        }

        /// <summary>
        /// The delete camera controller service.
        /// </summary>
        public string DeleteCameraControllerService(string cameraControllerServiceId)
        {
            try
            {
                const string Query = "DELETE CameraControllerService WHERE CameraControllerServiceID = @CameraControllerServiceId";

                var paramInfo = new List<ParamInfo>
                {
                    new ParamInfo("@CameraControllerServiceId", DbType.String, CommonUtil.ConvertToDbTypeString(cameraControllerServiceId))
                };

                var result = DataManager.GetInstance.DataHandler.SetExecuteDataSet(Query, paramInfo);
                return result;
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return ex.Message;
            }
        }
        
        #endregion // CameraControllerService

        #region ExternalCommandService

        /// <summary>
        /// The insert external command service.
        /// </summary>
        public string InsertExternalCommandService(
            string externalCommandServiceId,
            string externalCommandServiceIp,
            string serviceUrl,
            string externalCommandServiceDescription,
            string activeCode)
        {
            try
            {
                var query = new StringBuilder();
                query.Append("INSERT INTO ");
                query.Append("ExternalCommandService(ExternalCommandServiceID, ExternalCommandServiceIP, ServiceUrl, ExternalCommandServiceDescription, ActiveCode) ");
                query.Append("VALUES (@ExternalCommandServiceId, @ExternalCommandServiceIp, @ServiceUrl, @Description, @ActiveCode) ");

                var paramInfo = new List<ParamInfo>
                {
                    new ParamInfo("@ExternalCommandServiceId", DbType.String, CommonUtil.ConvertToDbTypeString(externalCommandServiceId)),
                    new ParamInfo("@ExternalCommandServiceIp", DbType.String, CommonUtil.ConvertToDbTypeString(externalCommandServiceIp)),
                    new ParamInfo("@ServiceUrl", DbType.String, CommonUtil.ConvertToDbTypeString(serviceUrl)),
                    new ParamInfo("@Description", DbType.String, CommonUtil.ConvertToDbTypeString(externalCommandServiceDescription)),                                   
                    new ParamInfo("@ActiveCode", DbType.String, CommonUtil.ConvertToDbTypeString(activeCode)),
                };

                var result = DataManager.GetInstance.DataHandler.SetExecuteDataSet(query.ToString(), paramInfo);
                return result;
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return ex.Message;
            }
        }

        public string ImportExternalCommandService(
            string externalCommandServiceId,
            string externalCommandServiceIp,
            string serviceUrl,
            string externalCommandServiceDescription,
            string activeCode,
            int count)
        {
            try
            {
                var result = DataManager.GetInstance.DataHandler.SetExecuteProcedureDataSet(
                    "SP_SetData_Manager_ImportExternalCommandServiceInfo", externalCommandServiceId, externalCommandServiceIp, externalCommandServiceDescription, serviceUrl, activeCode, count);

                return result;
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return ex.Message;
            }
        }

        /// <summary>
        /// The update external command service.
        /// </summary>
        public string UpdateExternalCommandService(
            string externalCommandServiceId,
            string externalCommandServiceIp,
            string serviceUrl,
            string externalCommandServiceDescription,
            string activeCode)
        {
            try
            {
                var query = new StringBuilder();
                query.Append("UPDATE ExternalCommandService SET ");
                query.Append("ExternalCommandServiceIP = @ExternalCommandServiceIp, ServiceUrl = @ServiceUrl, ");
                query.Append("ExternalCommandServiceDescription = @Description, ActiveCode = @ActiveCode WHERE ExternalCommandServiceID = @ExternalCommandServiceId");

                var paramInfo = new List<ParamInfo>
                                {
                                    new ParamInfo("@ExternalCommandServiceId", DbType.String, CommonUtil.ConvertToDbTypeString(externalCommandServiceId)),
                                    new ParamInfo("@ExternalCommandServiceIp", DbType.String, CommonUtil.ConvertToDbTypeString(externalCommandServiceIp)),
                                    new ParamInfo("@ServiceUrl", DbType.String, CommonUtil.ConvertToDbTypeString(serviceUrl)),
                                    new ParamInfo("@Description", DbType.String, CommonUtil.ConvertToDbTypeString(externalCommandServiceDescription)),
                                    new ParamInfo("@ActiveCode", DbType.String, CommonUtil.ConvertToDbTypeString(activeCode))
                                };

                var result = DataManager.GetInstance.DataHandler.SetExecuteDataSet(query.ToString(), paramInfo);
                return result;
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return ex.Message;
            }
        }

        /// <summary>
        /// The delete external command service.
        /// </summary>
        public string DeleteExternalCommandService(string externalCommandServiceId)
        {
            try
            {
                const string Query = "DELETE ExternalCommandService WHERE ExternalCommandServiceID = @ExternalCommandServiceId";

                var paramInfo = new List<ParamInfo>
                {
                    new ParamInfo("@ExternalCommandServiceId", DbType.String, CommonUtil.ConvertToDbTypeString(externalCommandServiceId))
                };

                var result = DataManager.GetInstance.DataHandler.SetExecuteDataSet(Query, paramInfo);
                return result;
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return ex.Message;
            }
        }
        
        #endregion // ExternalCommandService

        #region MapLayout

        /// <summary>
        /// 입력받은 Project ID에 해당하는 MapLayout Data를 저장 한다.
        /// </summary>
        /// <param name="projectId">저장할 Project ID.</param>
        /// <param name="data">저장할 Map Layout Data.</param>
        public void SetMapLayout(string projectId, string data)
        {
            // 입력받은 Project ID가 없으면 Insert, 있으면 Update.
            var isUpdate = this.CheckMapLayoutExists(projectId);

            try
            {
                if (isUpdate)
                {
                    this.UpdateMapLayoutData(projectId, data);
                }
                else
                {
                    this.InsertMapLayoutData(projectId, data);
                }
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
            }
        }

        private bool CheckMapLayoutExists(string projectId)
        {
            var dataSet = this.ExecuteQueryToGetMapLayoutTable(projectId);

            if (dataSet == null)
            {
                return false;
            }

            if (dataSet.Tables.Count == 0)
            {
                return false;
            }

            if (dataSet.Tables[0].Rows.Count != 1)
            {
                return false;
            }

            return true;
        }

        private DataSet ExecuteQueryToGetMapLayoutTable(string projectId)
        {
            var query = new StringBuilder();

            query.Append("SELECT * ");
            query.Append("FROM MapLayout ");
            query.Append("WHERE ProjectID = @ProjectId");

            var paramInfo = new List<ParamInfo>
                            {
                                new ParamInfo("@ProjectId", DbType.String, CommonUtil.ConvertToDbTypeString(projectId)),
                            };


            return DataManager.GetInstance.DataHandler.GetExecuteDataSet(query.ToString(), paramInfo);
        }

        private void UpdateMapLayoutData(string projectId, string data)
        {
            try
            {
                var query = new StringBuilder();

                query.Append("UPDATE MapLayout ");
                query.Append("SET LayoutData = @LayoutData ");
                query.Append("WHERE ProjectID = @ProjectId");

                var paramInfo = new List<ParamInfo>
                                    {
                                        new ParamInfo("@ProjectId", DbType.String, CommonUtil.ConvertToDbTypeString(projectId)),
                                        new ParamInfo("@LayoutData", DbType.String, CommonUtil.ConvertToDbTypeString(data))
                                    };

                DataManager.GetInstance.DataHandler.SetExecuteDataSet(query.ToString(), paramInfo);
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
            }
        }

        private void InsertMapLayoutData(string projectId, string data)
        {
            try
            {
                var query = new StringBuilder();

                query.Append("INSERT INTO MapLayout (ProjectID, LayoutData) ");
                query.Append("VALUES(@ProjectId, @LayoutData)");

                var paramInfo = new List<ParamInfo>
                                    {
                                        new ParamInfo("@ProjectId", DbType.String, CommonUtil.ConvertToDbTypeString(projectId)),
                                        new ParamInfo("@LayoutData", DbType.String, CommonUtil.ConvertToDbTypeString(data))
                                    };

                DataManager.GetInstance.DataHandler.SetExecuteDataSet(query.ToString(), paramInfo);
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
            }
        }

        #endregion // MapLayout

        #region LocationBookmark

        public void SetLocationBookmark(string data, string projectId, string userId)
        {
            var locationBookmarkDataSet = LocationBookmarkExist(projectId, userId);

            if (CheckedLocationBookmardData(locationBookmarkDataSet))
            {
                UpdateLocationBookmark(data, projectId, userId);
            }
            else
            {
                InsertLocationBookmark(data, projectId, userId);
            }
        }

        private void UpdateLocationBookmark(string data, string projectId, string userId)
        {
            try
            {
                var query = new StringBuilder();

                query.Append("UPDATE LocationBookmark                           ");
                query.Append("SET LocationBookmarkData = @LocationBookmarkData  ");
                query.Append("WHERE ProjectID = @ProjectId AND UserID = @UserId ");

                var paramInfo = new List<ParamInfo>
                                    {
                                        new ParamInfo("@ProjectId", DbType.String, CommonUtil.ConvertToDbTypeString(projectId)),
                                        new ParamInfo("@UserId", DbType.String, CommonUtil.ConvertToDbTypeString(userId)),
                                        new ParamInfo("@LocationBookmarkData", DbType.String, CommonUtil.ConvertToDbTypeString(data))
                                    };

                DataManager.GetInstance.DataHandler.SetExecuteDataSet(query.ToString(), paramInfo);
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return;
            }
        }

        private void InsertLocationBookmark(string data, string projectId, string userId)
        {
            try
            {
                var query = new StringBuilder();

                query.Append("INSERT INTO LocationBookmark (ProjectID, UserID, LocationBookmarkData)    ");
                query.Append("VALUES(@ProjectId, @UserId, @LocationBookmarkData)                        ");

                var paramInfo = new List<ParamInfo>
                                    {
                                        new ParamInfo("@ProjectId", DbType.String, CommonUtil.ConvertToDbTypeString(projectId)),
                                        new ParamInfo("@UserId", DbType.String, CommonUtil.ConvertToDbTypeString(userId)),
                                        new ParamInfo("@LocationBookmarkData", DbType.String, CommonUtil.ConvertToDbTypeString(data))
                                    };

                DataManager.GetInstance.DataHandler.SetExecuteDataSet(query.ToString(), paramInfo);
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return;
            }
        }

        private bool CheckedLocationBookmardData(DataSet locationBookmarkDataSet)
        {
            if (locationBookmarkDataSet == null)
            {
                return false;
            }

            if (locationBookmarkDataSet.Tables.Count == 0)
            {
                return false;
            }

            if (locationBookmarkDataSet.Tables[0].Rows.Count != 1)
            {
                return false;
            }

            return true;
        }

        private DataSet LocationBookmarkExist(string projectId, string userId)
        {
            try
            {
                const string query = "SELECT * FROM LocationBookmark WHERE ProjectID = @ProjectID AND UserID = @UserId";

                var paramInfo = new List<ParamInfo>
                                {
                                    new ParamInfo("@ProjectId", DbType.String, CommonUtil.ConvertToDbTypeString(projectId)),
                                    new ParamInfo("@UserId", DbType.String, CommonUtil.ConvertToDbTypeString(userId))
                                };

                return DataManager.GetInstance.DataHandler.GetExecuteDataSet(query, paramInfo);
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        #endregion // LocationBookmark

        #region Alarm

        #region CommandAlarmAction

        /// <summary>
        /// CommandAlarmAction에 전달된 내용을 저장한다.
        /// </summary>
        /// <param name="actionId">The actionId.</param>
        /// <param name="actionName">The actionName.</param>
        /// <param name="actionData">The actionData.</param>
        /// <param name="projectId">The projectId.</param>
        /// <param name="userId">The userId.</param>
        /// <param name="clientId">The clientId.</param>
        public string InsertCommandAlarmAction(
            string actionId,
            string actionName,
            string actionData,
            string projectId,
            string userId,
            string clientId)
        {
            try
            {
                var query = new StringBuilder();

                query.Append("INSERT INTO CommandAlarmAction                                                                                                                            ");
                query.Append("(CommandAlarmActionID, CommandAlarmActionName, CommandAlarmActionData, ProjectID, UserID, ClientID)     ");
                query.Append("VALUES(@ActionID, @ActionName, @ActionData, @ProjectId, @UserId, @ClientID)                                                 ");

                var paramInfo = new List<ParamInfo>
                                    {
                                        new ParamInfo("@ActionID", DbType.String, CommonUtil.ConvertToDbTypeString(actionId)),
                                        new ParamInfo("@ActionName", DbType.String, CommonUtil.ConvertToDbTypeString(actionName)),
                                        new ParamInfo("@ActionData", DbType.String, CommonUtil.ConvertToDbTypeString(actionData)),
                                        new ParamInfo("@ProjectId", DbType.String, CommonUtil.ConvertToDbTypeString(projectId)),
                                        new ParamInfo("@UserId", DbType.String, CommonUtil.ConvertToDbTypeString(userId)),
                                        new ParamInfo("@ClientID", DbType.String, CommonUtil.ConvertToDbTypeString(clientId))
                                    };

                var result = DataManager.GetInstance.DataHandler.SetExecuteDataSet(query.ToString(), paramInfo);
                return result;

            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return ex.Message;
            }
        }

        /// <summary>
        /// CommandAlarmAction의 내용을 Update 한다.
        /// </summary>
        /// <param name="actionId">The actionId.</param>
        /// <param name="actionName">The actionName.</param>
        /// <param name="actionData">The actionData.</param>
        /// <param name="projectId">The projectId.</param>
        /// <param name="userId">The userId.</param>
        /// <param name="clientId">The clientId.</param>
        public string UpdateCommandAlarmAction(
            string actionId,
            string actionName,
            string actionData,
            string projectId,
            string userId,
            string clientId)
        {
            try
            {
                var query = new StringBuilder();

                query.Append("UPDATE CommandAlarmAction  SET                                                                                            ");
                query.Append("CommandAlarmActionName = @ActionName, CommandAlarmActionData = @ActionData,        ");
                query.Append("ProjectID = @ProjectId, UserID = @UserId, ClientID = @ClientID                                                  ");
                query.Append("WHERE  CommandAlarmActionID = @ActionID                                                                             ");

                var paramInfo = new List<ParamInfo>
                                    {
                                        new ParamInfo("@ActionID", DbType.String, CommonUtil.ConvertToDbTypeString(actionId)),
                                        new ParamInfo("@ActionName", DbType.String, CommonUtil.ConvertToDbTypeString(actionName)),
                                        new ParamInfo("@ActionData", DbType.String, CommonUtil.ConvertToDbTypeString(actionData)),
                                        new ParamInfo("@ProjectId", DbType.String, CommonUtil.ConvertToDbTypeString(projectId)),
                                        new ParamInfo("@UserId", DbType.String, CommonUtil.ConvertToDbTypeString(userId)),
                                        new ParamInfo("@ClientID", DbType.String, CommonUtil.ConvertToDbTypeString(clientId))
                                    };

                var result = DataManager.GetInstance.DataHandler.SetExecuteDataSet(query.ToString(), paramInfo);
                return result;
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return ex.Message;
            }
        }

        public Stream DeleteCommandAlarmAction(string actionGuid, string actionName)
        {
            try
            {
                var query = new StringBuilder();

                query.Append("DELETE FROM CommandAlarmAction  ");
                query.Append("WHERE  CommandAlarmActionID = @CommandAlarmActionID"); // Guid만 확인하여 삭제.(Name이 Update이 된 경우 삭제 되지 않음.)
                //query.Append("WHERE  CommandAlarmActionID = @CommandAlarmActionID AND CommandAlarmActionName = @CommandAlarmActionName");

                var paramInfo = new List<ParamInfo>
                {
                    new ParamInfo("@CommandAlarmActionID", DbType.String, CommonUtil.ConvertToDbTypeString(actionGuid)),
                    //new ParamInfo("@CommandAlarmActionName", DbType.String, CommonUtil.ConvertToDbTypeString(actionName)),
                };

                var result = DataManager.GetInstance.DataHandler.SetExecuteDataSet(query.ToString(), paramInfo);

                return !(string.IsNullOrWhiteSpace(result)) ? CommonUtil.ResponseData(ResponseType.Failed, result) : CommonUtil.ResponseData(ResponseType.Success);
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return CommonUtil.ResponseData(ResponseType.Failed, ex.Message);
            }
        }

        #endregion //CommandAlarmAction

        #region AlarmInfo

        /// <summary>
        /// The insert alarm info.
        /// </summary>
        /// <param name="alarmId">
        /// The alarm id.
        /// </param>
        /// <param name="alarmName">
        /// The alarm name.
        /// </param>
        /// <param name="autoTerminate">
        /// The auto terminate.
        /// </param>
        /// <param name="alarmLeve">
        /// The alarm leve.
        /// </param>
        /// <param name="alarmTypeId">
        /// The alarm type.
        /// </param>
        /// <param name="description">
        /// The description.
        /// </param>
        /// <param name="cameraList">
        /// The camera list.
        /// </param>
        /// <param name="cameraNameList">
        /// The camera name list.
        /// </param>
        /// <param name="alarmPreTime">
        /// The alarm pre time.
        /// </param>
        /// <param name="alarmPostTime">
        /// The alarm post time.
        /// </param>
        /// <param name="useRecording">
        /// The use recording.
        /// </param>
        /// <param name="useIgnoreSameEvent">
        /// The use ignore same event.
        /// </param>
        /// <param name="historyDataDeletePeriod">
        /// The history data delete period.
        /// </param>
        /// <returns>
        /// The result data.
        /// </returns>
        public string InsertAlarmInfo(string alarmId, string alarmName, bool autoTerminate, string alarmLeve, int alarmTypeId, string description, string cameraList, string cameraNameList, int? alarmPreTime, int? alarmPostTime, bool? useRecording, bool? useIgnoreSameEvent, int? historyDataDeletePeriod)
        {
            try
            {
                var query = new StringBuilder();

                query.Append("INSERT INTO Alarm VALUES(@AlarmId, @AlarmName, @AutoTerminate, @AlarmLevel, @AlarmTypeId, @Description, @CameraList, @CameraNameList, @AlarmPreTime, @AlarmPostTime, @UseRecording, @UseIgnoreSameEvent, @HistoryDataDeletePeriod)");

                var paramInfo = new List<ParamInfo>
                {
                    new ParamInfo("@AlarmId", DbType.String, CommonUtil.ConvertToDbTypeString(alarmId)),
                    new ParamInfo("@AlarmName", DbType.String, CommonUtil.ConvertToDbTypeString(alarmName)),
                    new ParamInfo("@AutoTerminate", DbType.Byte, autoTerminate ? 1 : 0),
                    new ParamInfo("@AlarmLevel", DbType.String, CommonUtil.ConvertToDbTypeString(alarmLeve)),
                    new ParamInfo("@AlarmTypeId", DbType.Int32, alarmTypeId),
                    new ParamInfo("@Description", DbType.String, CommonUtil.ConvertToDbTypeString(description)),
                    new ParamInfo("@CameraList", DbType.String, CommonUtil.ConvertToDbTypeString(cameraList)),
                    new ParamInfo("@CameraNameList", DbType.String, CommonUtil.ConvertToDbTypeString(cameraNameList)),
                    new ParamInfo("@AlarmPreTime", DbType.Int32, alarmPreTime),
                    new ParamInfo("@AlarmPostTime", DbType.Int32, alarmPostTime),
                    new ParamInfo("@UseRecording", DbType.Boolean, useRecording),
                    new ParamInfo("@UseIgnoreSameEvent", DbType.Boolean, useIgnoreSameEvent),
                    new ParamInfo("@HistoryDataDeletePeriod", DbType.Int32, historyDataDeletePeriod)
                };

                var result = DataManager.GetInstance.DataHandler.SetExecuteDataSet(query.ToString(), paramInfo);

                return result;
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return ex.Message;
            }
        }

        public string ImportAlarmInfo(string alarmId, string alarmName, bool autoTerminate, string alarmLevel, int alarmTypeId, string description, string cameraList, string cameraNameList, int? alarmPreTime, int? alarmPostTime, bool? useRecording, bool? useIgnoreSameEvent, int? historyDataDeletePeriod, int count)
        {
            try
            {
                var result = DataManager.GetInstance.DataHandler.SetExecuteProcedureDataSet("SP_SetData_Manager_ImportAlarmInfo", alarmId, alarmName, autoTerminate, alarmLevel, alarmTypeId, description, cameraList, cameraNameList, alarmPreTime, alarmPostTime, useRecording, useIgnoreSameEvent, historyDataDeletePeriod, count);

                return result;
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return ex.Message;
            }
        }

        public string UpdateAlarmInfo(string alarmId, string alarmName, bool autoTerminate, string alarmLeve, int alarmType, string description, string cameraList, string cameraNameList, int? alarmPreTime, int? alarmPostTime, bool? useRecording, bool? useIgnoreSameEvent, int? historyDataDeletePeriod)
        {
            try
            {
                var query = new StringBuilder();

                query.Append("UPDATE Alarm SET                                                                                                                                          ");
                query.Append("AlarmName = @AlarmName, AutoTerminate = @AutoTerminate, AlarmLevel = @AlarmLevel, AlarmTypeId = @AlarmTypeId, Description = @Description,                 ");
                query.Append("CameraList = @CameraList, CameraNameList = @CameraNameList, AlarmPreTime = @AlarmPreTime, AlarmPostTime = @AlarmPostTime, UseRecording = @UseRecording,   ");
                query.Append("UseIgnoreSameEvent = @UseIgnoreSameEvent, HistoryDataDeletePeriod = @HistoryDataDeletePeriod                                                              ");
                query.Append("WHERE  AlarmID = @AlarmID                                                                                                                                 ");

                var paramInfo = new List<ParamInfo>
                {
                    new ParamInfo("@AlarmID", DbType.String, CommonUtil.ConvertToDbTypeString(alarmId)),
                    new ParamInfo("@AlarmName", DbType.String, CommonUtil.ConvertToDbTypeString(alarmName)),
                    new ParamInfo("@AutoTerminate", DbType.Byte, autoTerminate ? 1: 0),
                    new ParamInfo("@AlarmLevel", DbType.String, CommonUtil.ConvertToDbTypeString(alarmLeve)),
                    new ParamInfo("@AlarmTypeId", DbType.Int32, alarmType),
                    new ParamInfo("@Description", DbType.String, CommonUtil.ConvertToDbTypeString(description)),
                    new ParamInfo("@CameraList", DbType.String, CommonUtil.ConvertToDbTypeString(cameraList)),
                    new ParamInfo("@CameraNameList", DbType.String, CommonUtil.ConvertToDbTypeString(cameraNameList)),
                    new ParamInfo("@AlarmPreTime", DbType.Int32, alarmPreTime),
                    new ParamInfo("@AlarmPostTime", DbType.Int32, alarmPostTime),
                    new ParamInfo("@UseRecording", DbType.Boolean, useRecording),
                    new ParamInfo("@UseIgnoreSameEvent", DbType.Boolean, useIgnoreSameEvent),
                    new ParamInfo("@HistoryDataDeletePeriod", DbType.Int32, historyDataDeletePeriod)
                };

                var result = DataManager.GetInstance.DataHandler.SetExecuteDataSet(query.ToString(), paramInfo);
                return result;
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return ex.Message;
            }
        }

        public string DeleteAlarmInfo(string alarmId)
        {
            try
            {
                var query = new StringBuilder();

                query.Append("DELETE FROM Alarm  ");
                query.Append("WHERE  AlarmID = @AlarmId");

                var paramInfo = new List<ParamInfo>
                {
                    new ParamInfo("@AlarmId", DbType.String, CommonUtil.ConvertToDbTypeString(alarmId)),
                };

                var result = DataManager.GetInstance.DataHandler.SetExecuteDataSet(query.ToString(), paramInfo);
                return result;
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return ex.Message;
            }
        }

        public string DeleteAlarmInfoTable()
        {
            try
            {
                var query = "DELETE FROM Alarm";

                return DataManager.GetInstance.DataHandler.SetExecuteDataSet(query, null); ;
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return ex.Message;
            }
        }

        #endregion

        #region AlarmMapping

        #region CommandAlarmActionMapping

        public Stream InsertCommandAlarmActionMapping(string commandActionId, string alarmId)
        {
            try
            {
                var query = new StringBuilder();

                query.Append("INSERT INTO CommandActionAlarmMapping VALUES(@CommandActionId, @AlarmId)   ");

                var paramInfo = new List<ParamInfo>
                {
                    new ParamInfo("@CommandActionId", DbType.String, CommonUtil.ConvertToDbTypeString(commandActionId)),
                    new ParamInfo("@AlarmId", DbType.String, CommonUtil.ConvertToDbTypeString(alarmId)),
                    
                };

                var result = DataManager.GetInstance.DataHandler.SetExecuteDataSet(query.ToString(), paramInfo);

                return !(string.IsNullOrWhiteSpace(result)) ? CommonUtil.ResponseData(ResponseType.Failed, result) : CommonUtil.ResponseData(ResponseType.Success);

            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return CommonUtil.ResponseData(ResponseType.Failed, ex.Message);
            }
        }

        public string ImportCommandAlarmActionMappingInfo(int mappingId, string commandId, string alarmId, int count)
        {
            try
            {
                var query = new StringBuilder();
                query.Append("SET IDENTITY_INSERT CommandActionAlarmMapping ON                                                                  ");
                query.Append("INSERT INTO CommandActionAlarmMapping(MappingID, CommandAlarmActionID, AlarmID) VALUES(@MappingId, @CommandActionId, @AlarmId)   ");
                query.Append("SET IDENTITY_INSERT CommandActionAlarmMapping OFF                                                                   ");

                var paramInfo = new List<ParamInfo>
                {
                    new ParamInfo("@MappingId", DbType.Int32, mappingId),
                    new ParamInfo("@CommandActionId", DbType.String, CommonUtil.ConvertToDbTypeString(commandId)),
                    new ParamInfo("@AlarmId", DbType.String, CommonUtil.ConvertToDbTypeString(alarmId)),
                    
                };

                return DataManager.GetInstance.DataHandler.SetExecuteDataSet(query.ToString(), paramInfo);
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return ex.Message;
            }
        }

        public string DeleteCommandAlarmActionMapping(string commandActionId, string alarmId = null)
        {
            try
            {
                var query = new StringBuilder();

                query.Append("DELETE FROM CommandActionAlarmMapping WHERE  CommandAlarmActionID = @CommandAlarmActionID ");
                var paramInfo = new List<ParamInfo>
                {
                    new ParamInfo("@CommandAlarmActionID", DbType.String, CommonUtil.ConvertToDbTypeString(commandActionId)),
                };

                if (!string.IsNullOrWhiteSpace(alarmId))
                {
                    query.Append("AND AlarmID = @AlarmId ");
                    paramInfo.Add(new ParamInfo("@AlarmId", DbType.String, CommonUtil.ConvertToDbTypeString(alarmId)));
                }

                return DataManager.GetInstance.DataHandler.SetExecuteDataSet(query.ToString(), paramInfo);
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return ex.Message;
            }
        }

        #endregion

        #region ExternalCommandTemplateMapping

        public bool InsertExternalCommandTemplateMapping(string alarmId, string externalCommandTemplateId)
        {
            try
            {
                var query = new StringBuilder();

                query.Append("INSERT INTO AlarmExternalCommandMapping VALUES(@AlarmId, @ExternalCommandTemplateId)   ");

                var paramInfo = new List<ParamInfo>
                {
                    new ParamInfo("@AlarmId", DbType.String, CommonUtil.ConvertToDbTypeString(alarmId)),
                    new ParamInfo("@ExternalCommandTemplateId", DbType.String, CommonUtil.ConvertToDbTypeString(externalCommandTemplateId)),
                    
                };

                var result = DataManager.GetInstance.DataHandler.SetExecuteDataSet(query.ToString(), paramInfo);

                if (!string.IsNullOrWhiteSpace(result))
                {
                    DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, result);
                    return false;
                }

                return true;
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return false;
            }
        }

        public string InsertExternalCommandTemplateMappingTable(int mappingId, string alarmId, string externalCommandTemplateId)
        {
            try
            {
                var query = new StringBuilder();
                query.Append("SET IDENTITY_INSERT AlarmExternalCommandMapping ON                                                                  ");
                query.Append("INSERT INTO AlarmExternalCommandMapping(MappingID, AlarmID, ExternalCommandTemplateID) VALUES(@MappingId, @AlarmId, @ExternalCommandTemplateID)   ");
                query.Append("SET IDENTITY_INSERT AlarmExternalCommandMapping OFF                                                                   ");

                var paramInfo = new List<ParamInfo>
                {
                    new ParamInfo("@MappingId", DbType.Int32, mappingId),
                    new ParamInfo("@AlarmId", DbType.String, CommonUtil.ConvertToDbTypeString(alarmId)),
                    new ParamInfo("@ExternalCommandTemplateID", DbType.String, CommonUtil.ConvertToDbTypeString(externalCommandTemplateId)),
                };

                var result = DataManager.GetInstance.DataHandler.SetExecuteDataSet(query.ToString(), paramInfo);

                return result;

            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return ex.Message;
            }
        }

        public string ImportExternalCommandTemplateMappingInfo(int mappingId, string alarmId, string externalCommandTemplateId, int count)
        {
            try
            {
                var result = DataManager.GetInstance.DataHandler.SetExecuteProcedureDataSet("SP_SetData_Manager_ImportExternalCommandTemplateMappingInfo", mappingId, CommonUtil.ConvertToDbTypeString(alarmId), CommonUtil.ConvertToDbTypeString(externalCommandTemplateId), count);

                return result;

            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return ex.Message;
            }
        }

        public string DeleteExternalCommandTemplateMapping(string externalCommandTemplateId, string alarmId)
        {
            try
            {
                var query = new StringBuilder();

                query.Append("DELETE FROM AlarmExternalCommandMapping  ");
                query.Append("WHERE  AlarmID = @AlarmId AND ExternalCommandTemplateID = @ExternalCommandTemplateID");

                var paramInfo = new List<ParamInfo>
                {
                    new ParamInfo("@ExternalCommandTemplateID", DbType.String, CommonUtil.ConvertToDbTypeString(externalCommandTemplateId)),
                    new ParamInfo("@AlarmId", DbType.String, CommonUtil.ConvertToDbTypeString(alarmId)),
                };

                var result = DataManager.GetInstance.DataHandler.SetExecuteDataSet(query.ToString(), paramInfo);

                return result;
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return ex.Message;
            }
        }

        /// <summary>
        /// ExternalCommandTemplate 삭제로 인해 연결되어 있는 Mapping을 지운다.
        /// </summary>
        /// <param name="externalCommandTemplateId">The externalCommandTemplateId.</param>
        /// <returns>The result string.</returns>
        public string DeleteExternalCommandTemplateMappingForDeleteExternalCommandTemplate(string externalCommandTemplateId)
        {
            try
            {
                var query = new StringBuilder();

                query.Append("DELETE FROM AlarmExternalCommandMapping  ");
                query.Append("WHERE  ExternalCommandTemplateID = @ExternalCommandTemplateID");

                var paramInfo = new List<ParamInfo>
                {
                    new ParamInfo("@ExternalCommandTemplateID", DbType.String, CommonUtil.ConvertToDbTypeString(externalCommandTemplateId)),
                };

                var result = DataManager.GetInstance.DataHandler.SetExecuteDataSet(query.ToString(), paramInfo);

                return result;
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return ex.Message;
            }
        }

        #endregion

        public string DeleteAlarmMapping(string alarmId)
        {
            var deleteExternalCommandTemplateResult = this.DeleteExternalCommandTemplateMappingForAlarmDelete(alarmId);
            var deleteCommandAlarmActionResult = this.DeleteCommandAlarmActionMappingForAlarmDelete(alarmId);

            var returnString = string.Empty;
            if (!string.IsNullOrWhiteSpace(deleteCommandAlarmActionResult))
            {
                returnString = string.Format("Delete CommandAlarmActionMapping Error : {0}", deleteCommandAlarmActionResult);
            }

            if (!string.IsNullOrWhiteSpace(deleteExternalCommandTemplateResult))
            {
                returnString = string.IsNullOrWhiteSpace(returnString)
                                   ? deleteExternalCommandTemplateResult
                                   : string.Format("\r\nDelete ExternalCommandTemplateMapping Error : {0}",
                                                   deleteExternalCommandTemplateResult);
            }

            return returnString;
        }

        private string DeleteExternalCommandTemplateMappingForAlarmDelete(string alarmId)
        {
            try
            {
                var query = new StringBuilder();

                query.Append("DELETE FROM AlarmExternalCommandMapping  ");
                query.Append("WHERE  AlarmID = @AlarmId ");

                var paramInfo = new List<ParamInfo>
                {
                    new ParamInfo("@AlarmId", DbType.String, CommonUtil.ConvertToDbTypeString(alarmId)),
                };

                return DataManager.GetInstance.DataHandler.SetExecuteDataSet(query.ToString(), paramInfo);
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return ex.Message;
            }
        }

        private string DeleteCommandAlarmActionMappingForAlarmDelete(string alarmId)
        {
            try
            {
                var query = new StringBuilder();

                query.Append("DELETE FROM CommandActionAlarmMapping  ");
                query.Append("WHERE  AlarmID = @AlarmId ");

                var paramInfo = new List<ParamInfo>
                {
                    new ParamInfo("@AlarmId", DbType.String, CommonUtil.ConvertToDbTypeString(alarmId)),
                };

                return DataManager.GetInstance.DataHandler.SetExecuteDataSet(query.ToString(), paramInfo);
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return ex.Message;
            }
        }

        #endregion


        #region EventNotify

        public string InsertAlarmPendingList(string alarmPendingListID, string alarmID, DateTime registTime, int confirmStatus, string alarmParameter)
        {
            try
            {
                var query = new StringBuilder();

                query.Append("INSERT INTO AlarmPendingList (AlarmPendingListID, AlarmID, RegistTime, ConfirmStatus, AlarmParameter)    ");
                query.Append("VALUES(@AlarmPendingListID, @AlarmID, @RegistTime, @ConfirmStatus, @AlarmParameter)                        ");

                var paramInfo = new List<ParamInfo>
                {
                    new ParamInfo("@AlarmPendingListID", DbType.String, CommonUtil.ConvertToDbTypeString(alarmPendingListID)),
                    new ParamInfo("@AlarmID", DbType.String, CommonUtil.ConvertToDbTypeString(alarmID)),
                    new ParamInfo("@RegistTime", DbType.DateTime, CommonUtil.ConvertToDbTypeString(registTime.ToString())),
                    new ParamInfo("@ConfirmStatus", DbType.Byte, confirmStatus),
                    new ParamInfo("@AlarmParameter", DbType.String, CommonUtil.ConvertToDbTypeString(alarmParameter))
                };

                var result = DataManager.GetInstance.DataHandler.SetExecuteDataSet(query.ToString(), paramInfo);
                return result;
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return ex.Message;
            }
        }

        public string InsertAlarmHistory(string alarmPendingListId, bool confirmStatus, string userId)
        {
            try
            {
                var query = new StringBuilder();

                query.Append("INSERT INTO AlarmHistory (RegistTime, ConfirmStatus, UserID, AlarmPendingListID)    ");
                query.Append("VALUES(@RegistTime, @ConfirmStatus, @UserID, @AlarmPendingListID)                     ");

                var paramInfo = new List<ParamInfo>
                {
                    new ParamInfo("@RegistTime", DbType.DateTime, DateTime.Now),
                    new ParamInfo("@ConfirmStatus", DbType.Byte, confirmStatus ? 1 :0),
                    new ParamInfo("@UserID", DbType.String, CommonUtil.ConvertToDbTypeString(userId)),
                    new ParamInfo("@AlarmPendingListID", DbType.String, CommonUtil.ConvertToDbTypeString(alarmPendingListId)),
                };

                var result = DataManager.GetInstance.DataHandler.SetExecuteDataSet(query.ToString(), paramInfo);
                return result;
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return ex.Message;
            }
        }


        public string InserAllConfirmAlarmHistory(string clientId)
        {
            try
            {
                var query = new StringBuilder();

                query.Append("INSERT INTO AlarmHistory                                                                          ");
                query.Append("      SELECT GETDATE(),  1, UserID, AlarmPendingListID                           ");
                query.Append("      FROM CommandAlarmAction AS CA                                                     ");
                query.Append("      INNER JOIN CommandActionAlarmMapping AS CAAM                         ");
                query.Append("      ON CA.CommandAlarmActionID = CAAM.CommandAlarmActionID      ");
                query.Append("      INNER JOIN AlarmPendingList AS AM                                                   ");
                query.Append("      ON AM.AlarmID = CAAM.AlarmID                                                           ");
                query.Append("      WHERE CA.ClientID = @ClientID                                                             ");

                var paramInfo = new List<ParamInfo>
                {
                    new ParamInfo("@ClientID", DbType.String, CommonUtil.ConvertToDbTypeString(clientId))
                };

                var result = DataManager.GetInstance.DataHandler.SetExecuteDataSet(query.ToString(), paramInfo);
                return result;
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return ex.Message;
            }
        }

        public string UpdateAlarmPendingList(string pendingListId, bool confirmStatus)
        {
            try
            {
                var query = new StringBuilder();

                query.Append("UPDATE AlarmPendingList SET ConfirmStatus = @ConfirmStatus        ");
                query.Append("WHERE AlarmPendingListID = @PendingListId                                      ");

                var paramInfo = new List<ParamInfo>
                {
                    new ParamInfo("@PendingListId", DbType.String, CommonUtil.ConvertToDbTypeString(pendingListId)),
                    new ParamInfo("@ConfirmStatus", DbType.Byte, confirmStatus ? 1 :0),
                };

                var result = DataManager.GetInstance.DataHandler.SetExecuteDataSet(query.ToString(), paramInfo);
                return result;
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return ex.Message;
            }
        }

        public string UpdateAllPendingList(string clientId)
        {
            try
            {
                var query = new StringBuilder();

                query.Append(" UPDATE AM SET AM.ConfirmStatus = 1                                                ");
                query.Append(" FROM CommandAlarmAction AS CA                                                      ");
                query.Append(" INNER JOIN CommandActionAlarmMapping AS CAAM                          ");
                query.Append(" ON CA.CommandAlarmActionID = CAAM.CommandAlarmActionID       ");
                query.Append(" INNER JOIN AlarmPendingList AS AM                                                    ");
                query.Append(" ON AM.AlarmID = CAAM.AlarmID                                                            ");
                query.Append(" WHERE CA.ClientID = @ClientID                                                             ");

                var paramInfo = new List<ParamInfo>
                {
                    new ParamInfo("@ClientID", DbType.String, CommonUtil.ConvertToDbTypeString(clientId)),
                };

                var result = DataManager.GetInstance.DataHandler.SetExecuteDataSet(query.ToString(), paramInfo);
                return result;
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return ex.Message;
            }
        }

        #endregion //EventNotify

        #region ExternalCommand

        /// <summary>
        /// The insert external command for configuration.
        /// </summary>
        /// <param name="externalCommandId">
        /// The external command id.
        /// </param>
        /// <param name="externalCommandName">
        /// The external command name.
        /// </param>
        /// <param name="externalCommandType">
        /// The external command type.
        /// </param>
        /// <param name="externalCommandText">
        /// The external command text.
        /// </param>
        /// <param name="fileName">
        /// The file name.
        /// </param>
        /// <param name="description">
        /// The description.
        /// </param>
        /// <param name="multiExternalCommandId">
        /// The multi External Command Id.
        /// </param>
        /// <returns>
        /// The response data.
        /// </returns>
        public string InsertExternalCommand(string externalCommandId, string externalCommandName, string externalCommandType, string externalCommandText, string fileName, string description, string multiExternalCommandId)
        {
            try
            {
                const string Query = "INSERT INTO ExternalCommand VALUES(@ExternalCommandId, @ExternalCommandName, @ExternalCommandType, @ExternalCommandText, @FileName, @Description, @MultiExternalCommandId)";

                var paramInfo = new List<ParamInfo>
                                    {
                                        new ParamInfo("@ExternalCommandId", DbType.String, CommonUtil.ConvertToDbTypeString(externalCommandId)),
                                        new ParamInfo("@ExternalCommandName", DbType.String, CommonUtil.ConvertToDbTypeString(externalCommandName)),
                                        new ParamInfo("@ExternalCommandType", DbType.String, CommonUtil.ConvertToDbTypeString(externalCommandType)),
                                        new ParamInfo("@ExternalCommandText", DbType.String, CommonUtil.ConvertToDbTypeString(externalCommandText)),
                                        new ParamInfo("@FileName", DbType.String, CommonUtil.ConvertToDbTypeString(fileName)),
                                        new ParamInfo("@Description", DbType.String, CommonUtil.ConvertToDbTypeString(description)),
                                        new ParamInfo("@MultiExternalCommandId", DbType.String, CommonUtil.ConvertToDbTypeString(multiExternalCommandId))
                                    };

                var result = DataManager.GetInstance.DataHandler.SetExecuteDataSet(Query, paramInfo);

                return result;
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return ex.Message;
            }
        }

        public string ImportExternalCommand(string externalCommandId, string externalCommandName, string externalCommandType, string externalCommandText, string fileName, string description, string multiExternalCommandId, int count)
        {
            try
            {
                var result = DataManager.GetInstance.DataHandler.SetExecuteProcedureDataSet(
                    "SP_SetData_Manager_ImportExternalCommand",
                    CommonUtil.ConvertToDbTypeString(externalCommandId),
                    CommonUtil.ConvertToDbTypeString(externalCommandName),
                    CommonUtil.ConvertToDbTypeString(externalCommandType),
                    CommonUtil.ConvertToDbTypeString(externalCommandText),
                    CommonUtil.ConvertToDbTypeString(fileName),
                    CommonUtil.ConvertToDbTypeString(description),
                    CommonUtil.ConvertToDbTypeString(multiExternalCommandId),
                    count);

                return result;
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return ex.Message;
            }
        }

        /// <summary>
        /// The update external command for configuration.
        /// </summary>
        /// <param name="externalCommandId">
        /// The external command id.
        /// </param>
        /// <param name="externalCommandName">
        /// The external command name.
        /// </param>
        /// <param name="externalCommandType">
        /// The external command type.
        /// </param>
        /// <param name="externalCommandText">
        /// The external command text.
        /// </param>
        /// <param name="fileName">
        /// The file name.
        /// </param>
        /// <param name="description">
        /// The description.
        /// </param>
        /// <param name="multiExternalCommandId">
        /// The multi External Command Id.
        /// </param>
        /// <returns>
        /// The response data.
        /// </returns>
        public string UpdateExternalCommand(string externalCommandId, string externalCommandName, string externalCommandType, string externalCommandText, string fileName, string description, string multiExternalCommandId)
        {
            try
            {
                var query = new StringBuilder();

                query.Append("UPDATE ExternalCommand                                                                                                                            ");
                query.Append("SET ExternalCommandName = @ExternalCommandName, ExternalCommandType = @ExternalCommandType,                                                       ");
                query.Append(" ExternalCommandText = @ExternalCommandText,   FileName = @FileName, Description = @Description, MultiExternalCommandId = @MultiExternalCommandId ");
                query.Append("WHERE ExternalCommandID = @ExternalCommandId                                                                                                      ");

                var paramInfo = new List<ParamInfo>
                                    {
                                        new ParamInfo("@ExternalCommandId", DbType.String, CommonUtil.ConvertToDbTypeString(externalCommandId)),
                                        new ParamInfo("@ExternalCommandName", DbType.String, CommonUtil.ConvertToDbTypeString(externalCommandName)),
                                        new ParamInfo("@ExternalCommandType", DbType.String, CommonUtil.ConvertToDbTypeString(externalCommandType)),
                                        new ParamInfo("@ExternalCommandText", DbType.String, CommonUtil.ConvertToDbTypeString(externalCommandText)),
                                        new ParamInfo("@FileName", DbType.String, CommonUtil.ConvertToDbTypeString(fileName)),
                                        new ParamInfo("@Description", DbType.String, CommonUtil.ConvertToDbTypeString(description)),
                                        new ParamInfo("@MultiExternalCommandId", DbType.String, CommonUtil.ConvertToDbTypeString(multiExternalCommandId))
                                    };

                var result = DataManager.GetInstance.DataHandler.SetExecuteDataSet(query.ToString(), paramInfo);

                return result;
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return ex.Message;
            }
        }

        /// <summary>
        /// The delete external command for configuration.
        /// </summary>
        /// <param name="externalCommandId">The external command id.</param>
        /// <returns>The response data.</returns>
        public string DeleteExternalCommand(string externalCommandId)
        {
            try
            {
                const string query = "DELETE FROM ExternalCommand WHERE ExternalCommandID = @ExternalCommandId";

                var paramInfo = new List<ParamInfo>
                {
                    new ParamInfo("@ExternalCommandId", DbType.String, CommonUtil.ConvertToDbTypeString(externalCommandId))
                };

                var result = DataManager.GetInstance.DataHandler.SetExecuteDataSet(query, paramInfo);

                return result;
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return ex.Message;
            }
        }

        public string DeleteExternalCommandTable()
        {
            try
            {
                const string query = "DELETE FROM ExternalCommand ";

                var result = DataManager.GetInstance.DataHandler.SetExecuteDataSet(query, null);
                return result;
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return ex.Message;
            }
        }

        #endregion // ExternalCommand

        #region ExternalCommandTemplate

        public string InsertExternalCommandTemplate(
            string externalCommandTemplateId,
            string templateName,
            string externalCommandId,
            string startExternalCommandId,
            string endExternalCommandId,
            string description)
        {
            try
            {
                const string query = "INSERT INTO ExternalCommandTemplate VALUES(@ExternalCommandTemplateId, @TemplateName, @ExternalCommandId, @StartExternalCommand, @EndExternalCommand, @Description)";

                var paramInfo = new List<ParamInfo>
                {
                    new ParamInfo("@ExternalCommandTemplateId", DbType.String, CommonUtil.ConvertToDbTypeString(externalCommandTemplateId)),
                    new ParamInfo("@TemplateName", DbType.String, CommonUtil.ConvertToDbTypeString(templateName)),
                    new ParamInfo("@ExternalCommandId", DbType.String, CommonUtil.ConvertToDbTypeString(externalCommandId)),
                    new ParamInfo("@StartExternalCommand", DbType.String, CommonUtil.ConvertToDbTypeString(startExternalCommandId)),
                    new ParamInfo("@EndExternalCommand", DbType.String, CommonUtil.ConvertToDbTypeString(endExternalCommandId)),
                    new ParamInfo("@Description", DbType.String, CommonUtil.ConvertToDbTypeString(description))
                };

                var result = DataManager.GetInstance.DataHandler.SetExecuteDataSet(query, paramInfo);

                return result;
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return ex.Message;
            }
        }

        public string ImportExternalCommandTemplate(
            string externalCommandTemplateId,
            string templateName,
            string externalCommandId,
            string startExternalCommandId,
            string endExternalCommandId,
            string description,
            int count)
        {
            try
            {
                var result = DataManager.GetInstance.DataHandler.SetExecuteProcedureDataSet(
                    "SP_SetData_Manager_ImportExternalCommandTemplate",
                    CommonUtil.ConvertToDbTypeString(externalCommandTemplateId),
                    CommonUtil.ConvertToDbTypeString(templateName),
                    CommonUtil.ConvertToDbTypeString(externalCommandId),
                    CommonUtil.ConvertToDbTypeString(startExternalCommandId),
                    CommonUtil.ConvertToDbTypeString(endExternalCommandId),
                    CommonUtil.ConvertToDbTypeString(description),
                    count
                    );

                return result;
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return ex.Message;
            }
        }

        public string UpdateExternalCommandTemplate(
            string externalCommandTemplateId,
            string externalCommandTemplateName,
            string externalCommandId,
            string startExternalCommand,
            string endExternalCommand,
            string description)
        {
            try
            {
                var query = new StringBuilder();

                query.Append("UPDATE ExternalCommandTemplate                                                                                           ");
                query.Append(" SET TemplateName = @TemplateName, ExternalCommandID = @ExternalCommandID,        ");
                query.Append(" StartExternalCommandID = @StartExternalCommandID,                                                            ");
                query.Append(" EndExternalCommandID= @EndExternalCommandID, Description = @Description                   ");
                query.Append("WHERE ExternalCommandTemplateID = @ExternalCommandTemplateID                                  ");

                var paramInfo = new List<ParamInfo>
                {
                    new ParamInfo("@ExternalCommandTemplateID", DbType.String, CommonUtil.ConvertToDbTypeString(externalCommandTemplateId)),
                    new ParamInfo("@TemplateName", DbType.String, CommonUtil.ConvertToDbTypeString(externalCommandTemplateName)),
                    new ParamInfo("@ExternalCommandID", DbType.String, CommonUtil.ConvertToDbTypeString(externalCommandId)),
                    new ParamInfo("@StartExternalCommandID", DbType.String, CommonUtil.ConvertToDbTypeString(startExternalCommand)),
                    new ParamInfo("@EndExternalCommandID", DbType.String, CommonUtil.ConvertToDbTypeString(endExternalCommand)),
                    new ParamInfo("@Description", DbType.String, CommonUtil.ConvertToDbTypeString(description))
                };

                var result = DataManager.GetInstance.DataHandler.SetExecuteDataSet(query.ToString(), paramInfo);
                return result;
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return ex.Message;
            }
        }

        public string UpdateExternalCommandTemplateForDeleteExternalCommand(string externalCommandId)
        {
            try
            {
                var query = new StringBuilder();

                query.Append("UPDATE ExternalCommandTemplate                                                                                                                                                ");
                query.Append("      SET ExternalCommandID =                                                                                                                                                          ");
                query.Append("              CASE WHEN ExternalCommandID = @ExternalCommandID THEN NULL ELSE ExternalCommandID END,                    ");
                query.Append("      StartExternalCommandID =                                                                                                                                                          ");
                query.Append("              CASE WHEN StartExternalCommandID = @ExternalCommandID THEN NULL ELSE StartExternalCommandID  END,    ");
                query.Append("      EndExternalCommandID =                                                                                                                                                            ");
                query.Append("              CASE WHEN EndExternalCommandID = @ExternalCommandID THEN NULL ELSE EndExternalCommandID END         ");

                var paramInfo = new List<ParamInfo>
                {
                    new ParamInfo("@ExternalCommandID", DbType.String, CommonUtil.ConvertToDbTypeString(externalCommandId)),
                };

                var result = DataManager.GetInstance.DataHandler.SetExecuteDataSet(query.ToString(), paramInfo);
                return result;
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return ex.Message;
            }
        }

        public string DeleteExternalCommandTemplate(string externalCommandTemplateId)
        {
            try
            {
                const string query = "DELETE FROM ExternalCommandTemplate WHERE ExternalCommandTemplateID = @ExternalCommandTemplateId";

                var paramInfo = new List<ParamInfo>
                {
                    new ParamInfo("@ExternalCommandTemplateId", DbType.String, CommonUtil.ConvertToDbTypeString(externalCommandTemplateId))
                };

                var result = DataManager.GetInstance.DataHandler.SetExecuteDataSet(query, paramInfo);
                return result;
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return ex.Message;
            }
        }

        public string DeleteExternalCommandTemplateTable()
        {
            try
            {
                const string query = "DELETE FROM ExternalCommandTemplate";

                var result = DataManager.GetInstance.DataHandler.SetExecuteDataSet(query, null);
                return result;
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return ex.Message;
            }
        }

        #endregion //ExternalCommandTemplate

        #endregion //Alarm

        #region Storage

        /// <summary>
        /// The insert storage.
        /// </summary>
        /// <param name="storageNumber">
        /// The storage number.
        /// </param>
        /// <param name="storageName">
        /// The storage name.
        /// </param>
        /// <param name="storagePath">
        /// The storage path.
        /// </param>
        /// <param name="storageType">
        /// The storage Type.
        /// </param>
        /// <param name="storageCapacity">
        /// The storage capacity.
        /// </param>
        /// <param name="storageRemain">
        /// The storage remain.
        /// </param>
        /// <param name="lowerLimit">
        /// The lower limit.
        /// </param>
        /// <param name="erasingMethod">
        /// The erasing method.
        /// </param>
        /// <returns>
        /// The result.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// The exception.
        /// </exception>
        public string InsertStorage(int storageNumber, string storageName, string storagePath, int storageType, int storageCapacity, int storageRemain, int lowerLimit, int erasingMethod)
        {
            try
            {
                const string Query = "INSERT INTO Storage VALUES(@StorageNumber, @StorageName, @StoragePath, @StorageCapacity, @StorageRemain, @LowerLimit, @ErasingMethod, @StorageType)";

                var paramInfo = new List<ParamInfo>
                                    {
                                        new ParamInfo("@StorageNumber", DbType.Int32, storageNumber),
                                        new ParamInfo("@StorageName", DbType.String, CommonUtil.ConvertToDbTypeString(storageName)),
                                        new ParamInfo("@StoragePath", DbType.String, CommonUtil.ConvertToDbTypeString(storagePath)),
                                        new ParamInfo("@StorageCapacity", DbType.Int32, storageCapacity),
                                        new ParamInfo("@StorageRemain", DbType.Int32, storageRemain),
                                        new ParamInfo("@LowerLimit", DbType.Int32, lowerLimit),
                                        new ParamInfo("@ErasingMethod", DbType.Int32, erasingMethod),
                                        new ParamInfo("@StorageType", DbType.Int32, storageType)
                                    };

                return DataManager.GetInstance.DataHandler.SetExecuteDataSet(Query, paramInfo);
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return ex.Message;
            }
        }

        /// <summary>
        /// The import storage info.
        /// </summary>
        /// <param name="storageNumber">
        /// The storage number.
        /// </param>
        /// <param name="storageName">
        /// The storage name.
        /// </param>
        /// <param name="storagePath">
        /// The storage path.
        /// </param>
        /// <param name="storageType">
        /// The storage type.
        /// </param>
        /// <param name="storageCapacity">
        /// The storage capacity.
        /// </param>
        /// <param name="storageRemain">
        /// The storage remain.
        /// </param>
        /// <param name="lowerLimit">
        /// The lower limit.
        /// </param>
        /// <param name="erasingMethod">
        /// The erasing method.
        /// </param>
        /// <param name="count">
        /// The count.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string ImportStorageInfo(int storageNumber, string storageName, string storagePath, int storageType, int storageCapacity, int storageRemain, int lowerLimit, int erasingMethod, int count)
        {
            try
            {
                return DataManager.GetInstance.DataHandler.SetExecuteProcedureDataSet(
                    "SP_SetData_Manager_ImportStorageInfo",
                    storageNumber,
                    CommonUtil.ConvertToDbTypeString(storageName),
                    CommonUtil.ConvertToDbTypeString(storagePath),
                    storageCapacity,
                    storageRemain,
                    lowerLimit,
                    erasingMethod,
                    storageType,
                    count);
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return ex.Message;
            }
        }

        /// <summary>
        /// The insert storage.
        /// </summary>
        /// <param name="storageNumber">
        /// The storage number.
        /// </param>
        /// <param name="storageName">
        /// The storage name.
        /// </param>
        /// <param name="storagePath">
        /// The storage path.
        /// </param>
        /// <param name="storateType">
        /// The storage type.
        /// </param>
        /// <param name="storageCapacity">
        /// The storage capacity.
        /// </param>
        /// <param name="storageRemain">
        /// The storage remain.
        /// </param>
        /// <param name="lowerLimit">
        /// The lower limit.
        /// </param>
        /// <param name="erasingMethod">
        /// The erasing method.
        /// </param>
        /// <returns>
        /// The result.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// The exception.
        /// </exception>
        public Stream UpdateStorage(int storageNumber, string storageName, string storagePath, int storageType, int storageCapacity, int storageRemain, int lowerLimit, int erasingMethod)
        {
            try
            {
                var query = new StringBuilder();

                query.Append("UPDATE Storage                                                                                                            ");
                query.Append("SET StorageName = @StorageName, StoragePath = @StoragePath, StorageCapacity = @StorageCapacity,                           ");
                query.Append("    StorageRemain = @StorageRemain, LowerLimit = @LowerLimit, ErasingMethod = @ErasingMethod, StorageType = @StorageType  ");
                query.Append("WHERE StorageNumber = @StorageNumber                                                                                      ");

                var paramInfo = new List<ParamInfo>
                                    {
                                        new ParamInfo("@StorageNumber", DbType.Int32, storageNumber),
                                        new ParamInfo("@StorageName", DbType.String, CommonUtil.ConvertToDbTypeString(storageName)),
                                        new ParamInfo("@StoragePath", DbType.String, CommonUtil.ConvertToDbTypeString(storagePath)),
                                        new ParamInfo("@StorageCapacity", DbType.Int32, storageCapacity),
                                        new ParamInfo("@StorageRemain", DbType.Int32, storageRemain),
                                        new ParamInfo("@LowerLimit", DbType.Int32, lowerLimit),
                                        new ParamInfo("@ErasingMethod", DbType.Int32, erasingMethod),
                                        new ParamInfo("@StorageType", DbType.Int32, storageType)
                                    };

                var result = DataManager.GetInstance.DataHandler.SetExecuteDataSet(query.ToString(), paramInfo);

                return !string.IsNullOrWhiteSpace(result) ? CommonUtil.ResponseData(ResponseType.Failed, result) : CommonUtil.ResponseData(ResponseType.Success);
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return CommonUtil.ResponseData(ResponseType.Failed, ex.Message);
            }
        }

        /// <summary>
        /// The delete storage.
        /// </summary>
        /// <param name="storageNumber">
        /// The storage number.
        /// </param>
        /// <returns>
        /// The result.
        /// </returns>
        public Stream DeleteStorage(int storageNumber)
        {
            try
            {
                const string Query = "DELETE FROM Storage WHERE StorageNumber = @StorageNumber";

                var paramInfo = new List<ParamInfo>
                                    {
                                        new ParamInfo("@StorageNumber", DbType.Int32, storageNumber)
                                    };

                var result = DataManager.GetInstance.DataHandler.SetExecuteDataSet(Query, paramInfo);

                return !string.IsNullOrWhiteSpace(result) ? CommonUtil.ResponseData(ResponseType.Failed, result) : CommonUtil.ResponseData(ResponseType.Success);
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return CommonUtil.ResponseData(ResponseType.Failed, ex.Message);
            }
        }

        /// <summary>
        /// The set stroage update process.
        /// </summary>
        /// <param name="capacity">
        /// The capacity.
        /// </param>
        /// <param name="remain">
        /// The remain.
        /// </param>
        /// <param name="storageNumber">
        /// The storage number.
        /// </param>
        /// <returns>
        /// The <see cref="Stream"/>.
        /// </returns>
        public Stream SetStroageUpdateProcess(int capacity, int remain, int storageNumber)
        {
            try
            {
                var result = DataManager.GetInstance.DataHandler.SetExecuteProcedureDataSet("SP_SetData_Recorder_StorageUpdateProcess", capacity, remain, storageNumber);

                return !string.IsNullOrWhiteSpace(result) ? CommonUtil.ResponseData(ResponseType.Failed, result) : CommonUtil.ResponseData(ResponseType.Success);
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return CommonUtil.ResponseData(ResponseType.Failed, ex.Message);
            }
        }

        #endregion // Storage

        #region Record Schedule

        /// <summary>
        /// The insert record schedule.
        /// </summary>
        /// <param name="recordScheduleNumber">
        /// The record schedule number.
        /// </param>
        /// <param name="recordScheduleName">
        /// The record schedule name.
        /// </param>
        /// <param name="recordScheduleData">
        /// The record schedule data.
        /// </param>
        /// <returns>
        /// The result.
        /// </returns>
        public string InsertRecordSchedule(int recordScheduleNumber, string recordScheduleName, string recordScheduleData)
        {
            try
            {
                const string Query = "INSERT INTO RecordSchedule VALUES(@RecordScheduleNumber, @RecordScheduleName, @RecordScheduleData)";

                var paramInfo = new List<ParamInfo>
                                    {
                                        new ParamInfo("@RecordScheduleNumber", DbType.Int32, recordScheduleNumber),
                                        new ParamInfo("@RecordScheduleName", DbType.String, CommonUtil.ConvertToDbTypeString(recordScheduleName)),
                                        new ParamInfo("@RecordScheduleData", DbType.String, CommonUtil.ConvertToDbTypeString(recordScheduleData))
                                    };

                return DataManager.GetInstance.DataHandler.SetExecuteDataSet(Query, paramInfo);
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return ex.Message;
            }
        }

        public string ImportRecordSchedule(int recordScheduleNumber, string recordScheduleName, string recordScheduleData, int count)
        {
            try
            {
                return DataManager.GetInstance.DataHandler.SetExecuteProcedureDataSet(
                    "SP_SetData_Manager_ImportRecordSchedule",
                    recordScheduleNumber,
                    CommonUtil.ConvertToDbTypeString(recordScheduleName),
                    CommonUtil.ConvertToDbTypeString(recordScheduleData),
                    count);
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return ex.Message;
            }
        }

        /// <summary>
        /// The update record schedule.
        /// </summary>
        /// <param name="recordScheduleNumber">
        /// The record schedule number.
        /// </param>
        /// <param name="recordScheduleName">
        /// The record schedule name.
        /// </param>
        /// <param name="recordScheduleData">
        /// The record schedule data.
        /// </param>
        /// <returns>
        /// The result.
        /// </returns>
        public Stream UpdateRecordSchedule(int recordScheduleNumber, string recordScheduleName, string recordScheduleData)
        {
            try
            {
                var query = new StringBuilder();

                query.Append("UPDATE RecordSchedule                                                                     ");
                query.Append("SET RecordScheduleName = @RecordScheduleName, RecordScheduleData = @RecordScheduleData    ");
                query.Append("WHERE RecordScheduleNumber = @RecordScheduleNumber                                        ");

                var paramInfo = new List<ParamInfo>
                                    {
                                        new ParamInfo("@RecordScheduleNumber", DbType.Int32, recordScheduleNumber),
                                        new ParamInfo("@RecordScheduleName", DbType.String, CommonUtil.ConvertToDbTypeString(recordScheduleName)),
                                        new ParamInfo("@RecordScheduleData", DbType.String, CommonUtil.ConvertToDbTypeString(recordScheduleData))
                                    };

                var result = DataManager.GetInstance.DataHandler.SetExecuteDataSet(query.ToString(), paramInfo);

                return !string.IsNullOrWhiteSpace(result) ? CommonUtil.ResponseData(ResponseType.Failed, result) : CommonUtil.ResponseData(ResponseType.Success);
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return CommonUtil.ResponseData(ResponseType.Failed, ex.Message);
            }
        }

        /// <summary>
        /// The delete record schedule.
        /// </summary>
        /// <param name="recordScheduleNumber">
        /// The record schedule number.
        /// </param>
        /// <returns>
        /// The result.
        /// </returns>
        public Stream DeleteRecordSchedule(int recordScheduleNumber)
        {
            try
            {
                const string Query = "DELETE FROM RecordSchedule WHERE RecordScheduleNumber = @RecordScheduleNumber";

                var paramInfo = new List<ParamInfo>
                                    {
                                        new ParamInfo("@RecordScheduleNumber", DbType.Int32, recordScheduleNumber)
                                    };

                var result = DataManager.GetInstance.DataHandler.SetExecuteDataSet(Query, paramInfo);

                return !string.IsNullOrWhiteSpace(result) ? CommonUtil.ResponseData(ResponseType.Failed, result) : CommonUtil.ResponseData(ResponseType.Success);
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return CommonUtil.ResponseData(ResponseType.Failed, ex.Message);
            }
        }

        #endregion // Record Schedule

        #region Archive Schedule

        /// <summary>
        /// The insert archive schedule.
        /// </summary>
        /// <param name="archiveScheduleNumber">
        /// The archive schedule number.
        /// </param>
        /// <param name="archiveScheduleName">
        /// The archive schedule name.
        /// </param>
        /// <param name="retentionPeriod">
        /// The retention period.
        /// </param>
        /// <returns>
        /// The result.
        /// </returns>
        public string InsertArchiveSchedule(int archiveScheduleNumber, string archiveScheduleName, int retentionPeriod)
        {
            try
            {
                const string Query = "INSERT INTO ArchiveSchedule VALUES(@ArchiveScheduleNumber, @ArchiveScheduleName, @RetentionPeriod)";

                var paramInfo = new List<ParamInfo>
                {
                    new ParamInfo("@ArchiveScheduleNumber", DbType.Int32, archiveScheduleNumber),
                    new ParamInfo("@ArchiveScheduleName", DbType.String, CommonUtil.ConvertToDbTypeString(archiveScheduleName)),
                    new ParamInfo("@RetentionPeriod", DbType.Int32, retentionPeriod)
                };

                return DataManager.GetInstance.DataHandler.SetExecuteDataSet(Query, paramInfo);
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return ex.Message;
            }
        }

        /// <summary>
        /// The update archive schedule.
        /// </summary>
        /// <param name="archiveScheduleNumber">
        /// The archive schedule number.
        /// </param>
        /// <param name="archiveScheduleName">
        /// The archive schedule name.
        /// </param>
        /// <param name="retentionPeriod">
        /// The retention period.
        /// </param>
        /// <returns>
        /// The result.
        /// </returns>
        public Stream UpdateArchiveSchedule(int archiveScheduleNumber, string archiveScheduleName, int retentionPeriod)
        {
            try
            {
                var query = new StringBuilder();

                query.Append("UPDATE ArchiveSchedule                                                                ");
                query.Append("SET ArchiveScheduleName = @ArchiveScheduleName, RetentionPeriod = @RetentionPeriod    ");
                query.Append("WHERE ArchiveScheduleNumber = @ArchiveScheduleNumber                                  ");

                var paramInfo = new List<ParamInfo>
                                    {
                                        new ParamInfo("@ArchiveScheduleNumber", DbType.Int32, archiveScheduleNumber),
                                        new ParamInfo("@ArchiveScheduleName", DbType.String, CommonUtil.ConvertToDbTypeString(archiveScheduleName)),
                                        new ParamInfo("@RetentionPeriod", DbType.Int32, retentionPeriod)
                                    };

                var result = DataManager.GetInstance.DataHandler.SetExecuteDataSet(query.ToString(), paramInfo);

                return !string.IsNullOrWhiteSpace(result) ? CommonUtil.ResponseData(ResponseType.Failed, result) : CommonUtil.ResponseData(ResponseType.Success);
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return CommonUtil.ResponseData(ResponseType.Failed, ex.Message);
            }
        }

        public Stream ImportArchiveSchedule(int archiveScheduleNumber, string archiveScheduleName, int retentionPeriod, int count)
        {
            var result = DataManager.GetInstance.DataHandler.SetExecuteProcedureDataSet("SP_SetData_Manager_ImportArchiveSchedule", archiveScheduleNumber, archiveScheduleName, retentionPeriod, count);

            return !string.IsNullOrWhiteSpace(result) ? CommonUtil.ResponseData(ResponseType.Failed, result) : CommonUtil.ResponseData(ResponseType.Success);
        }

        /// <summary>
        /// The delete archive schedule.
        /// </summary>
        /// <param name="archiveScheduleNumber">
        /// The archive schedule number.
        /// </param>
        /// <returns>
        /// The result.
        /// </returns>
        public Stream DeleteArchiveSchedule(int archiveScheduleNumber)
        {
            try
            {
                const string Query = "DELETE FROM ArchiveSchedule WHERE ArchiveScheduleNumber = @ArchiveScheduleNumber";

                var paramInfo = new List<ParamInfo>
                                    {
                                        new ParamInfo("@ArchiveScheduleNumber", DbType.Int32, archiveScheduleNumber)
                                    };

                var result = DataManager.GetInstance.DataHandler.SetExecuteDataSet(Query, paramInfo);

                return !string.IsNullOrWhiteSpace(result) ? CommonUtil.ResponseData(ResponseType.Failed, result) : CommonUtil.ResponseData(ResponseType.Success);
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return CommonUtil.ResponseData(ResponseType.Failed, ex.Message);
            }
        }

        #endregion // Archive Schedule

        #region Record Camera

        /// <summary>
        /// The insert record camera.
        /// </summary>
        /// <param name="recordCameraNumber">
        /// The record camera number.
        /// </param>
        /// <param name="useLocalDisk">
        /// The use Local Disk.
        /// </param>
        /// <param name="recordStorageList">
        /// The record storage list.
        /// </param>
        /// <param name="archiveStorageList">
        /// The archive storage list.
        /// </param>
        /// <param name="recordType">
        /// The record type.
        /// </param>
        /// <param name="recordScheduleNumber">
        /// The record schedule number.
        /// </param>
        /// <param name="archiveScheduleNumber">
        /// The archive schedule number.
        /// </param>
        /// <param name="retentionPeriod">
        /// The retention period.
        /// </param>
        /// <param name="useVoice">
        /// The use voice.
        /// </param>
        /// <param name="recorderId">
        /// The recorder id.
        /// </param>
        /// <param name="width">
        /// The width.
        /// </param>
        /// <param name="height">
        /// The height.
        /// </param>
        /// <param name="fps">
        /// The fps.
        /// </param>
        /// <param name="codec">
        /// The codec.
        /// </param>
        /// <param name="eventWidth">
        /// The event Width.
        /// </param>
        /// <param name="eventHeight">
        /// The event Height.
        /// </param>
        /// <param name="eventFps">
        /// The event Fps.
        /// </param>
        /// <param name="eventCodec">
        /// The event Codec.
        /// </param>
        /// <returns>
        /// The result.
        /// </returns>
        public string InsertRecordCamera(
            int recordCameraNumber,
            bool useLocalDisk,
            string recordStorageList,
            string archiveStorageList,
            string recordType,
            int recordScheduleNumber,
            int archiveScheduleNumber,
            int retentionPeriod,
            bool useVoice,
            string recorderId,
            int? width,
            int? height,
            int? fps,
            int? bitrate,
            string codec,
            bool? useEvent,
            int? eventWidth,
            int? eventHeight,
            int? eventFps,
            int? eventBitrate,
            string eventCodec)
        {
            try
            {
                const string Query = "INSERT INTO RecordCamera VALUES(@RecordCameraNumber, @RecordStorageList, @ArchiveStorageList, @RecordType, @RecordScheduleNumber, @ArchiveScheduleNumber, @RetentionPeriod, @UseVoice, @RecorderId, @Width, @Height, @FPS, @Codec, @EventWidth, @EventHeight, @EventFPS, @EventCodec, @UseLocalDisk, @Bitrate, @UseEvent, @EventBitrate)";

                var paramInfo = new List<ParamInfo>
                                    {
                                        new ParamInfo("@RecordCameraNumber", DbType.Int32, recordCameraNumber),
                                        new ParamInfo("@RecordStorageList", DbType.String, CommonUtil.ConvertToDbTypeString(recordStorageList)),
                                        new ParamInfo("@ArchiveStorageList", DbType.String, CommonUtil.ConvertToDbTypeString(archiveStorageList)),
                                        new ParamInfo("@RecordType", DbType.String, CommonUtil.ConvertToDbTypeString(recordType)),
                                        new ParamInfo("@RecordScheduleNumber", DbType.Int32, recordScheduleNumber),
                                        new ParamInfo("@ArchiveScheduleNumber", DbType.Int32, archiveScheduleNumber),
                                        new ParamInfo("@RetentionPeriod", DbType.Int32, retentionPeriod),
                                        new ParamInfo("@UseVoice", DbType.Boolean, useVoice),
                                        new ParamInfo("@RecorderId", DbType.String, CommonUtil.ConvertToDbTypeString(recorderId)),
                                        new ParamInfo("@Width", DbType.Int32, width),
                                        new ParamInfo("@Height", DbType.Int32, height),
                                        new ParamInfo("@FPS", DbType.Int32, fps),
                                        new ParamInfo("@Codec", DbType.String, CommonUtil.ConvertToDbTypeString(codec)),
                                        new ParamInfo("@EventWidth", DbType.Int32, eventWidth),
                                        new ParamInfo("@EventHeight", DbType.Int32, eventHeight),
                                        new ParamInfo("@EventFPS", DbType.Int32, eventFps),
                                        new ParamInfo("@EventCodec", DbType.String, CommonUtil.ConvertToDbTypeString(eventCodec)),
                                        new ParamInfo("@UseLocalDisk", DbType.Boolean, useLocalDisk),
                                        new ParamInfo("@Bitrate", DbType.Int32, bitrate),
                                        new ParamInfo("@UseEvent", DbType.Boolean, useEvent),
                                        new ParamInfo("@EventBitrate", DbType.Int32, eventBitrate)
                                    };

                return DataManager.GetInstance.DataHandler.SetExecuteDataSet(Query, paramInfo);
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return ex.Message;
            }
        }

        public string ImportRecordCamera(
            int recordCameraNumber,
            bool useLocalDisk,
            string recordStorageList,
            string archiveStorageList,
            string recordType,
            int recordScheduleNumber,
            int archiveScheduleNumber,
            int retentionPeriod,
            bool useVoice,
            string recorderId,
            int? width,
            int? height,
            int? fps,
            int? bitrate,
            string codec,
            bool useEvent,
            int? eventWidth,
            int? eventHeight,
            int? eventFps,
            int? eventBitrate,
            string eventCodec,
            int count)
        {
            try
            {
                return DataManager.GetInstance.DataHandler.SetExecuteProcedureDataSet(
                    "SP_SetData_Manager_ImportRecordCamera",
                    recordCameraNumber,
                    CommonUtil.ConvertToDbTypeString(recordStorageList),
                    CommonUtil.ConvertToDbTypeString(archiveStorageList),
                    CommonUtil.ConvertToDbTypeString(recordType),
                    recordScheduleNumber,
                    archiveScheduleNumber,
                    retentionPeriod,
                    useVoice,
                    CommonUtil.ConvertToDbTypeString(recorderId),
                    width,
                    height,
                    fps,
                    CommonUtil.ConvertToDbTypeString(codec),
                    eventWidth,
                    eventHeight,
                    eventFps,
                    CommonUtil.ConvertToDbTypeString(eventCodec),
                    useLocalDisk,
                    bitrate,
                    useEvent,
                    eventBitrate,
                    count);
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return ex.Message;
            }
        }

        /// <summary>
        /// The update record camera.
        /// </summary>
        /// <param name="recordCameraNumber">
        /// The record camera number.
        /// </param>
        /// <param name="useLocalDisk">
        /// The use Local Disk.
        /// </param>
        /// <param name="recordStorageList">
        /// The record storage list.
        /// </param>
        /// <param name="archiveStorageList">
        /// The archive storage list.
        /// </param>
        /// <param name="recordType">
        /// The record type.
        /// </param>
        /// <param name="recordScheduleNumber">
        /// The record schedule number.
        /// </param>
        /// <param name="archiveScheduleNumber">
        /// The archive schedule number.
        /// </param>
        /// <param name="retentionPeriod">
        /// The retention period.
        /// </param>
        /// <param name="useVoice">
        /// The use voice.
        /// </param>
        /// <param name="recorderId">
        /// The recorder id.
        /// </param>
        /// <param name="width">
        /// The width.
        /// </param>
        /// <param name="height">
        /// The height.
        /// </param>
        /// <param name="fps">
        /// The fps.
        /// </param>
        /// <param name="codec">
        /// The codec.
        /// </param>
        /// <param name="eventWidth">
        /// The event Width.
        /// </param>
        /// <param name="eventHeight">
        /// The event Height.
        /// </param>
        /// <param name="eventFps">
        /// The event Fps.
        /// </param>
        /// <param name="eventCodec">
        /// The event Codec.
        /// </param>
        /// <returns>
        /// The result.
        /// </returns>
        public Stream UpdateRecordCamera(
            int recordCameraNumber,
            bool useLocalDisk,
            string recordStorageList,
            string archiveStorageList,
            string recordType,
            int recordScheduleNumber,
            int archiveScheduleNumber,
            int retentionPeriod,
            bool useVoice,
            string recorderId,
            int? width,
            int? height,
            int? fps,
            int? bitrate,
            string codec,
            bool? useEvent,
            int? eventWidth,
            int? eventHeight,
            int? eventFps,
            int? eventBitrate,
            string eventCodec)
        {
            try
            {
                var query = new StringBuilder();

                query.Append("UPDATE RecordCamera                                                                                               ");
                query.Append("SET RecordStorageList = @recordStorageList, ArchiveStorageList = @ArchiveStorageList, RecordType = @RecordType,   ");
                query.Append("    RecordScheduleNumber = @RecordScheduleNumber, ArchiveScheduleNumber = @ArchiveScheduleNumber,                 ");
                query.Append("    RetentionPeriod = @RetentionPeriod, UseVoice = @UseVoice, RecorderID = @RecorderId, Width = @Width,           ");
                query.Append("    Height = @Height, FPS = @FPS, Codec = @Codec, EventWidth = @EventWidth, EventHeight = @EventHeight,           ");
                query.Append("    EventFPS = @EventFPS, EventCodec = @EventCodec, UseLocalDisk = @UseLocalDisk,                                  ");
                query.Append("    Bitrate = @Bitrate, UseEvent = @UseEvent, EventBitrate = @EventBitrate                                        ");
                query.Append("WHERE RecordCameraNumber = @RecordCameraNumber                                                                    ");

                var paramInfo = new List<ParamInfo>
                                    {
                                        new ParamInfo("@RecordCameraNumber", DbType.Int32, recordCameraNumber),
                                        new ParamInfo("@RecordStorageList", DbType.String, CommonUtil.ConvertToDbTypeString(recordStorageList)),
                                        new ParamInfo("@ArchiveStorageList", DbType.String, CommonUtil.ConvertToDbTypeString(archiveStorageList)),
                                        new ParamInfo("@RecordType", DbType.String, CommonUtil.ConvertToDbTypeString(recordType)),
                                        new ParamInfo("@RecordScheduleNumber", DbType.Int32, recordScheduleNumber),
                                        new ParamInfo("@ArchiveScheduleNumber", DbType.Int32, archiveScheduleNumber),
                                        new ParamInfo("@RetentionPeriod", DbType.Int32, retentionPeriod),
                                        new ParamInfo("@UseVoice", DbType.Boolean, useVoice),
                                        new ParamInfo("@RecorderId", DbType.String, CommonUtil.ConvertToDbTypeString(recorderId)),
                                        new ParamInfo("@Width", DbType.Int32, width),
                                        new ParamInfo("@Height", DbType.Int32, height),
                                        new ParamInfo("@FPS", DbType.Int32, fps),
                                        new ParamInfo("@Codec", DbType.String, CommonUtil.ConvertToDbTypeString(codec)),
                                        new ParamInfo("@EventWidth", DbType.Int32, eventWidth),
                                        new ParamInfo("@EventHeight", DbType.Int32, eventHeight),
                                        new ParamInfo("@EventFPS", DbType.Int32, eventFps),
                                        new ParamInfo("@EventCodec", DbType.String, CommonUtil.ConvertToDbTypeString(eventCodec)),
                                        new ParamInfo("@UseLocalDisk", DbType.Boolean, useLocalDisk),
                                        new ParamInfo("@Bitrate", DbType.Int32, bitrate),
                                        new ParamInfo("@UseEvent", DbType.Boolean, useEvent),
                                        new ParamInfo("@EventBitrate", DbType.Int32, eventBitrate)
                                    };

                var result = DataManager.GetInstance.DataHandler.SetExecuteDataSet(query.ToString(), paramInfo);

                return !string.IsNullOrWhiteSpace(result) ? CommonUtil.ResponseData(ResponseType.Failed, result) : CommonUtil.ResponseData(ResponseType.Success);
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return CommonUtil.ResponseData(ResponseType.Failed, ex.Message);
            }
        }

        /// <summary>
        /// The delete record camera.
        /// </summary>
        /// <param name="recordCameraNumber">
        /// The record camera number.
        /// </param>
        /// <returns>
        /// The result.
        /// </returns>
        public Stream DeleteRecordCamera(int recordCameraNumber)
        {
            try
            {
                const string Query = "DELETE FROM RecordCamera WHERE RecordCameraNumber = @RecordCameraNumber";

                var paramInfo = new List<ParamInfo>
                {
                    new ParamInfo("@RecordCameraNumber", DbType.Int32, recordCameraNumber)
                };

                var result = DataManager.GetInstance.DataHandler.SetExecuteDataSet(Query, paramInfo);

                // Camera Table에 해당되는 RecordCameraNumber 삭제
                const string Query2 = "UPDATE Camera SET RecordCameraNumber = NULL WHERE RecordCameraNumber = @RecordCameraNumber";

                var result2 = DataManager.GetInstance.DataHandler.SetExecuteDataSet(Query2, paramInfo);

                return !string.IsNullOrWhiteSpace(result) ? CommonUtil.ResponseData(ResponseType.Failed, result) : CommonUtil.ResponseData(ResponseType.Success);
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return CommonUtil.ResponseData(ResponseType.Failed, ex.Message);
            }
        }

        #endregion //Record Camera

        #region Configuration

        /// <summary>
        /// The add configuration info.
        /// </summary>
        /// <param name="restServerPort">
        /// The rest server port.
        /// </param>
        /// <param name="imspServerPort">
        /// The imsp server port.
        /// </param>
        /// <param name="eventRecordStorageList">
        /// The event record storage list.
        /// </param>
        /// <returns>
        /// The result.
        /// </returns>
        public string InsertConfiguration(int restServerPort, int imspServerPort, string eventRecordStorageList)
        {
            try
            {
                const string Query = "INSERT INTO Configuration VALUES(@RESTServerPort, @IMSPServerPort, @EventRecordStorageList)";

                var paramInfo = new List<ParamInfo>
                                    {
                                        new ParamInfo("@RESTServerPort", DbType.Int32, restServerPort),
                                        new ParamInfo("@IMSPServerPort", DbType.Int32, imspServerPort),
                                        new ParamInfo("@EventRecordStorageList", DbType.String, CommonUtil.ConvertToDbTypeString(eventRecordStorageList))
                                    };

                return DataManager.GetInstance.DataHandler.SetExecuteDataSet(Query, paramInfo);
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return ex.Message;
            }
        }

        public string ImportConfiguration(int index, int restServerPort, int imspServerPort, string eventRecordStorageList, int count)
        {
            try
            {
                return DataManager.GetInstance.DataHandler.SetExecuteProcedureDataSet(
                    "SP_SetData_Manager_ImportConfigurationInfo",
                    index,
                    restServerPort,
                    imspServerPort,
                    eventRecordStorageList,
                    count);
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return ex.Message;
            }
        }

        /// <summary>
        /// The update configruration info.
        /// </summary>
        /// <param name="index">
        /// The index.
        /// </param>
        /// <param name="restServerPort">
        /// The rest server port.
        /// </param>
        /// <param name="imspServerPort">
        /// The imsp server port.
        /// </param>
        /// <param name="eventRecordStorageList">
        /// The event record storage list.
        /// </param>
        /// <returns>
        /// The result.
        /// </returns>
        public Stream UpdateConfiguration(int index, int restServerPort, int imspServerPort, string eventRecordStorageList)
        {
            try
            {
                var query = new StringBuilder();

                query.Append("UPDATE Configuration                                                                                                      ");
                query.Append("SET RESTServerPort = @RESTServerPort, IMSPServerPort = @IMSPServerPort, EventRecordStorageList = @EventRecordStorageList  ");
                query.Append("WHERE [Index] = @Index                                                                                                    ");

                var paramInfo = new List<ParamInfo>
                                    {
                                        new ParamInfo("@Index", DbType.Int32, index),
                                        new ParamInfo("@RESTServerPort", DbType.Int32, restServerPort),
                                        new ParamInfo("@IMSPServerPort", DbType.Int32, imspServerPort),
                                        new ParamInfo("@EventRecordStorageList", DbType.String, CommonUtil.ConvertToDbTypeString(eventRecordStorageList))
                                    };

                var result = DataManager.GetInstance.DataHandler.SetExecuteDataSet(query.ToString(), paramInfo);

                return !string.IsNullOrWhiteSpace(result) ? CommonUtil.ResponseData(ResponseType.Failed, result) : CommonUtil.ResponseData(ResponseType.Success);
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return CommonUtil.ResponseData(ResponseType.Failed, ex.Message);
            }
        }

        /// <summary>
        /// The delete configuration.
        /// </summary>
        /// <param name="index">
        /// The index.
        /// </param>
        /// <returns>
        /// The result.
        /// </returns>
        public Stream DeleteConfiguration(int index)
        {
            try
            {
                const string Query = "DELETE FROM Configuration WHERE [Index] = @Index";

                var paramInfo = new List<ParamInfo>
                {
                    new ParamInfo("@Index", DbType.Int32, index)
                };

                var result = DataManager.GetInstance.DataHandler.SetExecuteDataSet(Query, paramInfo);

                return !string.IsNullOrWhiteSpace(result) ? CommonUtil.ResponseData(ResponseType.Failed, result) : CommonUtil.ResponseData(ResponseType.Success);
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return CommonUtil.ResponseData(ResponseType.Failed, ex.Message);
            }
        }

        /// <summary>
        /// The delete configuration table.
        /// </summary>
        /// <returns>
        /// The result.
        /// </returns>
        public string DeleteConfiguraionTable()
        {
            try
            {
                const string Query = "DELETE FROM Configuration DBCC CHECKIDENT([Configuration], RESEED, 0)";

                return DataManager.GetInstance.DataHandler.SetExecuteDataSet(Query);
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return ex.Message;
            }
        }

        #endregion // Configuration

        #region RecordEventHistory

        [MethodImpl(MethodImplOptions.Synchronized)]
        public int InsertRecordEventHistory(
            string alarmId,
            string alarmName,
            string alarmType,
            int? logId,
            string eventData,
            DateTime registTime,
            DateTime beginTime,
            DateTime endTime,
            int recordCameraNumber,
            string description,
            int deletePeriod)
        {
            var convertRegistTime = (int)TimeConverter.ConvertLocalDateTimeToUnixTimeSeconds(registTime);
            var convertBeginTime = (int)TimeConverter.ConvertLocalDateTimeToUnixTimeSeconds(beginTime);
            var convertEndTime = (int)TimeConverter.ConvertLocalDateTimeToUnixTimeSeconds(endTime);

            var recordEventHistoryDataSet =
                DataManager.GetInstance.DataHandler.GetExecuteProcedureDataSet(
                            "SP_SetData_Client_InsertRecordEventHistory",
                            CommonUtil.ConvertToDbTypeString(alarmId),
                            CommonUtil.ConvertToDbTypeString(alarmName),
                            alarmType,
                            logId,
                            eventData,
                            convertRegistTime,
                            convertBeginTime,
                            convertEndTime,
                            recordCameraNumber,
                            description,
                            deletePeriod);

            if (recordEventHistoryDataSet == null)
                return 0;

            var lastRow =
                recordEventHistoryDataSet.Tables[0].Rows.Cast<DataRow>().LastOrDefault();

            return lastRow == null ? 0 : int.Parse(lastRow["RecordEventHistoryID"].ToString());
        }

        public void UpdateRecordEventHistory(
            int recordId,
            int recordCameraId,
            double beginTime,
            double endTime)
        {
            DataManager.GetInstance.DataHandler.GetExecuteProcedureDataSet(
                "SP_SetData_Client_UpdateRecordEventHistory",
                recordId,
                recordCameraId,
                beginTime,
                endTime);
        }

        /// <summary>
        /// The set record event history.
        /// </summary>
        /// <param name="alarmId">
        /// The alarm id.
        /// </param>
        /// <param name="logId">
        /// The log Id.
        /// </param>
        /// <param name="eventData">
        /// The event Data.
        /// </param>
        /// <returns>
        /// The record event history id.
        /// </returns>
        public int SetRecordEventHistory(string alarmId, int? logId, string eventData)
        {
            var recordEventHistoryDataSet = DataManager.GetInstance.DataHandler.GetExecuteProcedureDataSet("SP_SetData_Client_RecordEventHistory", CommonUtil.ConvertToDbTypeString(alarmId), logId, eventData);
            var recordEventHistoryId = 0;

            if (recordEventHistoryDataSet == null)
            {
                return recordEventHistoryId;
            }

            foreach (DataRow row in recordEventHistoryDataSet.Tables[0].Rows)
            {
                recordEventHistoryId = int.Parse(row["RecordEventHistoryID"].ToString());
            }

            return recordEventHistoryId;
        }

        #endregion // recordEventHistory

        #region InnowatchDataLog

        /// <summary>
        /// The set log history.
        /// </summary>
        /// <param name="programType">
        /// The program type.
        /// </param>
        /// <param name="logType">
        /// The log type.
        /// </param>
        /// <param name="viewServerId">
        /// The view server id.
        /// </param>
        /// <param name="registTime">
        /// The regist time.
        /// </param>
        /// <param name="message">
        /// The message.
        /// </param>
        /// <returns>
        /// The log history id.
        /// </returns>
        public int? SetLogHistory(string programType, string logType, string viewServerId, string registTime, string message)
        {
            DataSet dataSet;

            if (string.IsNullOrWhiteSpace(registTime))
            {
                dataSet = DataManager.GetInstance.DataHandler.GetExecuteProcedureDataSet(
                "SP_SetLogHistory",
                CommonUtil.ConvertToDbTypeString(programType.ToUpper()),
                CommonUtil.ConvertToDbTypeString(logType.ToUpper()),
                CommonUtil.ConvertToDbTypeString(viewServerId),
                DateTime.Now,
                CommonUtil.ConvertToDbTypeString(message));
            }
            else
            {
                dataSet = DataManager.GetInstance.DataHandler.GetExecuteProcedureDataSet(
                "SP_SetLogHistory",
                CommonUtil.ConvertToDbTypeString(programType.ToUpper()),
                CommonUtil.ConvertToDbTypeString(logType.ToUpper()),
                CommonUtil.ConvertToDbTypeString(viewServerId),
                DateTime.Parse(registTime),
                CommonUtil.ConvertToDbTypeString(message));
            }

            var logId = 0;

            foreach (DataRow row in dataSet.Tables[0].Rows)
            {
                if (string.IsNullOrWhiteSpace(row["LogID"].ToString()))
                {
                    return null;
                }

                logId = int.Parse(row["LogID"].ToString());
            }

            return logId;
        }

        /// <summary>
        /// The set use log alarm info.
        /// </summary>
        /// <param name="logType">
        /// The log type.
        /// </param>
        /// <param name="programType">
        /// The program type.
        /// </param>
        /// <param name="count">
        /// The count.
        /// </param>
        /// <returns>
        /// The used log alarm info.
        /// </returns>
        public string SetUseLogAlarmInfo(string logType, string programType, int count)
        {
            var logAlarmDataSet = DataManager.GetInstance.DataHandler.GetExecuteProcedureDataSet("SP_SetUseLogAlarmInfo", logType, programType, count);

            return CommonUtil.XmlToDataSet(logAlarmDataSet);
        }

        /// <summary>
        /// The set unused log alarm info.
        /// </summary>
        /// <param name="logType">
        /// The log type.
        /// </param>
        /// <param name="programType">
        /// The program type.
        /// </param>
        /// <param name="count">
        /// The count.
        /// </param>
        /// <returns>
        /// The unused log alarm info.
        /// </returns>
        public string SetUnusedLogAlarmInfo(string logType, string programType, int count)
        {
            try
            {
                var logDataSet = DataManager.GetInstance.DataHandler.GetExecuteProcedureDataSet("SP_SetUnusedLogAlarmInfo", logType, programType, count);

                return CommonUtil.XmlToDataSet(logDataSet);
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return null;
            }
        }

        /// <summary>
        /// The set program log import data.
        /// </summary>
        /// <param name="logType">
        /// The log type.
        /// </param>
        /// <param name="programType">
        /// The program type.
        /// </param>
        /// <param name="useAlarm">
        /// The use alarm.
        /// </param>
        public string SetProgramLogImportData(string logType, string programType, int useAlarm)
        {
            try
            {
                var result = DataManager.GetInstance.DataHandler.GetExecuteProcedureDataSet("SP_SetProgramLogImportData", logType, programType, useAlarm);
                if (result == null)
                    return string.Empty;

                if (result.Tables.Count == 0)
                    return string.Empty;

                if (result.Tables[0].Rows.Count == 0)
                    return string.Empty;

                throw new Exception("LogType update failed.");
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
                return ex.Message;
            }
        }

        /// <summary>
        /// The insert log type external command mapping.
        /// </summary>
        /// <param name="logType">
        /// The log type.
        /// </param>
        /// <param name="programType">
        /// The program type.
        /// </param>
        /// <param name="externalCommandId">
        /// The external command id.
        /// </param>
        /// <returns>
        /// The result.
        /// </returns>
        public bool InsertLogTypeExternalCommandMapping(string logType, string programType, string externalCommandId)
        {
            try
            {
                DataManager.GetInstance.DataHandler.GetExecuteProcedureDataSet("SP_SetLogTypeExternalCommandMapping", logType, programType, externalCommandId);

                return true;
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);

                return false;
            }
        }

        /// <summary>
        /// The delete log external command mapping.
        /// </summary>
        /// <param name="logType">
        /// The log type.
        /// </param>
        /// <param name="programType">
        /// The program type.
        /// </param>
        /// <param name="externalCommandId">
        /// The external command id.
        /// </param>
        /// <returns>
        /// The result.
        /// </returns>
        public string DeleteLogExternalCommandMapping(string logType, string programType, string externalCommandId)
        {
            try
            {
                DataManager.GetInstance.DataHandler.GetExecuteProcedureDataSet("SP_DeleteLogTypeExternalCommandMapping", logType, programType, externalCommandId);

                return "true";
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);

                return ex.Message;
            }
        }

        /// <summary>
        /// The set log external command mapping import data.
        /// </summary>
        /// <param name="mappingId"> </param>
        /// <param name="logType">
        /// The log type.
        /// </param>
        /// <param name="programType">
        /// The program type.
        /// </param>
        /// <param name="externalCommandId">
        /// The external command id.
        /// </param>
        /// <param name="count"> </param>
        public void SetLogExternalCommandMappingImportData(int mappingId, string logType, string programType, string externalCommandId, int count)
        {
            try
            {
                DataManager.GetInstance.DataHandler.GetExecuteProcedureDataSet("SP_SetData_Manager_ImportLogExternalCommandMappingInfo", mappingId, logType, programType, externalCommandId, count);
            }
            catch (Exception ex)
            {
                Logger.Write(Logger.Error, this, GetMethodInfoStrings.GetMethodName(), ex.Message);
            }
        }

        /// <summary>
        /// The delete log external command mapping table.
        /// </summary>
        /// <param name="programType">
        /// The program type.
        /// </param>
        /// <returns>
        /// The response data.
        /// </returns>
        public string DeleteLogExternalCommandMappingTable(string programType)
        {
            try
            {
                DataManager.GetInstance.DataHandler.GetExecuteProcedureDataSet("SP_DeleteLogExternalCommandMappingTable", programType);

                return "true";
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);

                return ex.Message;
            }
        }

        #endregion // InnowatchDataLog

        #region Map
        public String SetKmlPlaceMaker(string KpmId, string KpmNm, string KpmParentId, string KpmLat, string KpmLon, string KpmAlt, string KpmDesc, string KpmIsFolder, string userid, string KpmStyleUrl)
        {
            try
            {
                return DataManager.GetInstance.DataHandler.SetExecuteProcedureDataSet("SP_SetData_KmlPlaceMaker", KpmId, KpmNm, KpmParentId, KpmLat, KpmLon, KpmAlt, KpmDesc, KpmIsFolder, userid, KpmStyleUrl);
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        public String SetMapXaml(string MxlId, string MxlNm, string MxlData, string userid)
        {
            try
            {
                return DataManager.GetInstance.DataHandler.SetExecuteProcedureDataSet("SP_SetData_MapXaml",  MxlId, MxlNm,  MxlData, userid);
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        public String SetKmlPlaceMakerDelete(string KpmId)
        {
            try
            {
                return DataManager.GetInstance.DataHandler.SetExecuteProcedureDataSet("SP_SetData_KmlPlaceMaker_Delete", KpmId);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public String SetMapXamlDelete(string MxlId)
        {
            try
            {
                return DataManager.GetInstance.DataHandler.SetExecuteProcedureDataSet("SP_SetData_MapXaml_Delete", MxlId);
                
            }catch(Exception ex)
            {
                return ex.Message;
            }
        }
        #endregion

        #region RPN
        public string AddAlarmRule(string alarmRuleName, string alarmRuleObj)
        {
            try
            {
                var query = new StringBuilder();

                query.Append("INSERT INTO AlarmRule (AlarmRuleName, AlarmRuleObj, LastUpdateDate) VALUES(@AlarmRuleName, @AlarmRule, DEFAULT) SELECT SCOPE_IDENTITY()");

                var paramInfo = new List<ParamInfo>
                {
                    new ParamInfo("@AlarmRuleName", DbType.String, CommonUtil.ConvertToDbTypeString(alarmRuleName)),
                    new ParamInfo("@AlarmRule", DbType.String, CommonUtil.ConvertToDbTypeString(alarmRuleObj))
                };

                DataSet ds = DataManager.GetInstance.DataHandler.GetExecuteDataSet(query.ToString(), paramInfo);

                return ds.Tables[0].Rows[0][0].ToString();
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

       

        public Stream DeleteAlarmRule(int? alarmRuleId)
        {
            try
            {
                var query = new StringBuilder();

                query.Append("DELETE FROM AlarmRuleMapping WHERE AlarmRuleId = @AlarmRuleId1; DELETE FROM AlarmRule WHERE AlarmRuleId = @AlarmRuleId2");

                var paramInfo = new List<ParamInfo>
                {
                    new ParamInfo("@AlarmRuleId1", DbType.Int32, alarmRuleId),
                    new ParamInfo("@AlarmRuleId2", DbType.Int32, alarmRuleId)
                };

                string result = DataManager.GetInstance.DataHandler.SetExecuteDataSet(query.ToString(), paramInfo);

                return !string.IsNullOrWhiteSpace(result) ? CommonUtil.ResponseData(ResponseType.Failed, result) : CommonUtil.ResponseData(ResponseType.Success);

            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        #endregion // RPN
    }
}
