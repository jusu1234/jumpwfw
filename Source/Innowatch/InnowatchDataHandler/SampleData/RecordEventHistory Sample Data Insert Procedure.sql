USE [InnowatchData]
GO
/****** Object:  StoredProcedure [dbo].[SP_SetData_Client_RecordEventHistory_Test]    Script Date: 08/17/2012 11:50:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SP_SetData_Client_RecordEventHistory_Test]
(
	@alarmId nvarchar(50),
	@logId int,
	@date datetime,
	@eventData nvarchar(max)
)
AS
	BEGIN
		DECLARE @alarmName nvarchar(50)
		DECLARE @alarmLevel nvarchar(10)
		DECLARE @alarmTypeId int
		DECLARE @alarmType nvarchar(20)
		DECLARE @cameraList nvarchar(max)
		DECLARE @recordingTime int
		DECLARE @description nvarchar(500)
		DECLARE @registTime bigint
		DECLARE @selectLastTime datetime
		DECLARE @lastTime bigint
		DECLARE @count int
		DECLARE @id int
		DECLARE @mergeCount int
		
		-- RegistTime 설정
		SET @registTime = DATEDIFF(second, '1970-01-01 09:00:00.000', @date)
		
		-- RecordEventHistory 에 저장할 Alarm 정보 조회 후 변수에 저장
		
		SELECT @alarmName = AlarmName, @alarmLevel = AlarmLevel, @alarmTypeId = AlarmTypeID, @cameraList = CameraList, @recordingTime = RecordingTime, @description = Description
		FROM Alarm
		WHERE AlarmID = @alarmId
		
		-- AlarmTeyp 정보를 가져오기
		
		SELECT @alarmType = AlarmType
		FROM AlarmType
		WHERE AlarmTypeID = @alarmTypeId
		
		INSERT INTO RecordEventHistory(RecordEventHistory.AlarmID, RecordEventHistory.LogID, RecordEventHistory.AlarmName, RecordEventHistory.AlarmType, RecordEventHistory.AlarmLevel, RecordEventHistory.CameraList, RecordEventHistory.RecordingTime, RecordEventHistory.RegistTime, RecordEventHistory.Description, RecordEventHistory.EventData)
		VALUES (@alarmId, @logId, @alarmName, @alarmType, @alarmLevel, @cameraList, @recordingTime, @date, @description, @eventData)
		SELECT @@IDENTITY AS RecordEventHistoryID	
		
		-- Interval : 20
		BEGIN
			SELECT @count = count(*)
			FROM RecordEventHistoryAsTwoHour
			
			IF (@count = 0)
				BEGIN
					INSERT INTO RecordEventHistoryAsTwoHour(RecordEventHistoryAsTwoHour.AlarmID, RecordEventHistoryAsTwoHour.LogID, RecordEventHistoryAsTwoHour.AlarmName, RecordEventHistoryAsTwoHour.AlarmType, RecordEventHistoryAsTwoHour.AlarmLevel, RecordEventHistoryAsTwoHour.CameraList, RecordEventHistoryAsTwoHour.RecordingTime, RecordEventHistoryAsTwoHour.RegistTime, RecordEventHistoryAsTwoHour.Description, RecordEventHistoryAsTwoHour.EventData, RecordEventHistoryAsTwoHour.MergeHistoryDataCount)
					VALUES (@alarmId, @logId, @alarmName, @alarmType, @alarmLevel, @cameraList, @recordingTime, @date, @description, @eventData, 1)
				END
			ELSE
				BEGIN
					-- 마지막 시간을 조회
					SELECT TOP(1) @selectLastTime = RegistTime
					FROM RecordEventHistoryAsTwoHour
					ORDER BY RecordEventHistoryID DESC

					-- 마지막 시간을 UNIXTIME 으로 변환
					SET @lastTime = DATEDIFF(second, '1970-01-01 09:00:00.000', @selectLastTime)
					
					-- 변환 된 값의 구간 정보 확인
					IF (@registTime / 20 != @lastTime / 20)
						BEGIN
							INSERT INTO RecordEventHistoryAsTwoHour(RecordEventHistoryAsTwoHour.AlarmID, RecordEventHistoryAsTwoHour.LogID, RecordEventHistoryAsTwoHour.AlarmName, RecordEventHistoryAsTwoHour.AlarmType, RecordEventHistoryAsTwoHour.AlarmLevel, RecordEventHistoryAsTwoHour.CameraList, RecordEventHistoryAsTwoHour.RecordingTime, RecordEventHistoryAsTwoHour.RegistTime, RecordEventHistoryAsTwoHour.Description, RecordEventHistoryAsTwoHour.EventData, RecordEventHistoryAsTwoHour.MergeHistoryDataCount)
							VALUES (@alarmId, @logId, @alarmName, @alarmType, @alarmLevel, @cameraList, @recordingTime, @date, @description, @eventData, 1)
						END
					ELSE
						BEGIN
							SELECT TOP(1) @id = RecordEventHistoryID, @mergeCount = MergeHistoryDataCount
							FROM RecordEventHistoryAsTwoHour
							WHERE RegistTime = @selectLastTime
							ORDER BY RecordEventHistoryID DESC
							
							UPDATE RecordEventHistoryAsTwoHour SET MergeHistoryDataCount = @mergeCount + 1 WHERE RecordEventHistoryID = @id
						END
				END
		END
		
		-- Interval : 60
		BEGIN
			SELECT @count = count(*)
			FROM RecordEventHistoryAsSixHour
			
			IF (@count = 0)
				BEGIN
					INSERT INTO RecordEventHistoryAsSixHour(RecordEventHistoryAsSixHour.AlarmID, RecordEventHistoryAsSixHour.LogID, RecordEventHistoryAsSixHour.AlarmName, RecordEventHistoryAsSixHour.AlarmType, RecordEventHistoryAsSixHour.AlarmLevel, RecordEventHistoryAsSixHour.CameraList, RecordEventHistoryAsSixHour.RecordingTime, RecordEventHistoryAsSixHour.RegistTime, RecordEventHistoryAsSixHour.Description, RecordEventHistoryAsSixHour.EventData, RecordEventHistoryAsSixHour.MergeHistoryDataCount)
					VALUES (@alarmId, @logId, @alarmName, @alarmType, @alarmLevel, @cameraList, @recordingTime, @date, @description, @eventData, 1)
				END
			ELSE
				BEGIN
					-- 마지막 시간을 조회
					SELECT TOP(1) @selectLastTime = RegistTime
					FROM RecordEventHistoryAsSixHour
					ORDER BY RecordEventHistoryID DESC

					-- 마지막 시간을 UNIXTIME 으로 변환
					SET @lastTime = DATEDIFF(second, '1970-01-01 09:00:00.000', @selectLastTime)
					
					-- 변환 된 값의 구간 정보 확인
					IF (@registTime / 60 != @lastTime / 60)
						BEGIN
							INSERT INTO RecordEventHistoryAsSixHour(RecordEventHistoryAsSixHour.AlarmID, RecordEventHistoryAsSixHour.LogID, RecordEventHistoryAsSixHour.AlarmName, RecordEventHistoryAsSixHour.AlarmType, RecordEventHistoryAsSixHour.AlarmLevel, RecordEventHistoryAsSixHour.CameraList, RecordEventHistoryAsSixHour.RecordingTime, RecordEventHistoryAsSixHour.RegistTime, RecordEventHistoryAsSixHour.Description, RecordEventHistoryAsSixHour.EventData, RecordEventHistoryAsSixHour.MergeHistoryDataCount)
							VALUES (@alarmId, @logId, @alarmName, @alarmType, @alarmLevel, @cameraList, @recordingTime, @date, @description, @eventData, 1)
						END
					ELSE
						BEGIN
							SELECT TOP(1) @id = RecordEventHistoryID, @mergeCount = MergeHistoryDataCount
							FROM RecordEventHistoryAsSixHour
							WHERE RegistTime = @selectLastTime
							ORDER BY RecordEventHistoryID DESC
							
							UPDATE RecordEventHistoryAsSixHour SET MergeHistoryDataCount = @mergeCount + 1 WHERE RecordEventHistoryID = @id
						END
				END
		END
		
		-- Interval : 120 (1 minute)
		BEGIN
			SELECT @count = count(*)
			FROM RecordEventHistoryAsTwelveHour
			
			IF (@count = 0)
				BEGIN
					INSERT INTO RecordEventHistoryAsTwelveHour(RecordEventHistoryAsTwelveHour.AlarmID, RecordEventHistoryAsTwelveHour.LogID, RecordEventHistoryAsTwelveHour.AlarmName, RecordEventHistoryAsTwelveHour.AlarmType, RecordEventHistoryAsTwelveHour.AlarmLevel, RecordEventHistoryAsTwelveHour.CameraList, RecordEventHistoryAsTwelveHour.RecordingTime, RecordEventHistoryAsTwelveHour.RegistTime, RecordEventHistoryAsTwelveHour.Description, RecordEventHistoryAsTwelveHour.EventData, RecordEventHistoryAsTwelveHour.MergeHistoryDataCount)
					VALUES (@alarmId, @logId, @alarmName, @alarmType, @alarmLevel, @cameraList, @recordingTime, @date, @description, @eventData, 1)
				END
			ELSE
				BEGIN
					-- 마지막 시간을 조회
					SELECT TOP(1) @selectLastTime = RegistTime
					FROM RecordEventHistoryAsTwelveHour
					ORDER BY RecordEventHistoryID DESC

					-- 마지막 시간을 UNIXTIME 으로 변환
					SET @lastTime = DATEDIFF(second, '1970-01-01 09:00:00.000', @selectLastTime)
					
					-- 변환 된 값의 구간 정보 확인
					IF (@registTime / 120 != @lastTime / 120)
						BEGIN
							INSERT INTO RecordEventHistoryAsTwelveHour(RecordEventHistoryAsTwelveHour.AlarmID, RecordEventHistoryAsTwelveHour.LogID, RecordEventHistoryAsTwelveHour.AlarmName, RecordEventHistoryAsTwelveHour.AlarmType, RecordEventHistoryAsTwelveHour.AlarmLevel, RecordEventHistoryAsTwelveHour.CameraList, RecordEventHistoryAsTwelveHour.RecordingTime, RecordEventHistoryAsTwelveHour.RegistTime, RecordEventHistoryAsTwelveHour.Description, RecordEventHistoryAsTwelveHour.EventData, RecordEventHistoryAsTwelveHour.MergeHistoryDataCount)
							VALUES (@alarmId, @logId, @alarmName, @alarmType, @alarmLevel, @cameraList, @recordingTime, @date, @description, @eventData, 1)
						END
					ELSE
						BEGIN
							SELECT TOP(1) @id = RecordEventHistoryID, @mergeCount = MergeHistoryDataCount
							FROM RecordEventHistoryAsTwelveHour
							WHERE RegistTime = @selectLastTime
							ORDER BY RecordEventHistoryID DESC
							
							UPDATE RecordEventHistoryAsTwelveHour SET MergeHistoryDataCount = @mergeCount + 1 WHERE RecordEventHistoryID = @id
						END
				END
		END
		
		-- Interval : 240 (2 minute)
		BEGIN
			SELECT @count = count(*)
			FROM RecordEventHistoryAsOneDay
			
			IF (@count = 0)
				BEGIN
					INSERT INTO RecordEventHistoryAsOneDay(RecordEventHistoryAsOneDay.AlarmID, RecordEventHistoryAsOneDay.LogID, RecordEventHistoryAsOneDay.AlarmName, RecordEventHistoryAsOneDay.AlarmType, RecordEventHistoryAsOneDay.AlarmLevel, RecordEventHistoryAsOneDay.CameraList, RecordEventHistoryAsOneDay.RecordingTime, RecordEventHistoryAsOneDay.RegistTime, RecordEventHistoryAsOneDay.Description, RecordEventHistoryAsOneDay.EventData, RecordEventHistoryAsOneDay.MergeHistoryDataCount)
					VALUES (@alarmId, @logId, @alarmName, @alarmType, @alarmLevel, @cameraList, @recordingTime, @date, @description, @eventData, 1)
				END
			ELSE
				BEGIN
					-- 마지막 시간을 조회
					SELECT TOP(1) @selectLastTime = RegistTime
					FROM RecordEventHistoryAsOneDay
					ORDER BY RecordEventHistoryID DESC

					-- 마지막 시간을 UNIXTIME 으로 변환
					SET @lastTime = DATEDIFF(second, '1970-01-01 09:00:00.000', @selectLastTime)
					
					-- 변환 된 값의 구간 정보 확인
					IF (@registTime / 240 != @lastTime / 240)
						BEGIN
							INSERT INTO RecordEventHistoryAsOneDay(RecordEventHistoryAsOneDay.AlarmID, RecordEventHistoryAsOneDay.LogID, RecordEventHistoryAsOneDay.AlarmName, RecordEventHistoryAsOneDay.AlarmType, RecordEventHistoryAsOneDay.AlarmLevel, RecordEventHistoryAsOneDay.CameraList, RecordEventHistoryAsOneDay.RecordingTime, RecordEventHistoryAsOneDay.RegistTime, RecordEventHistoryAsOneDay.Description, RecordEventHistoryAsOneDay.EventData, RecordEventHistoryAsOneDay.MergeHistoryDataCount)
							VALUES (@alarmId, @logId, @alarmName, @alarmType, @alarmLevel, @cameraList, @recordingTime, @date, @description, @eventData, 1)
						END
					ELSE
						BEGIN
							SELECT TOP(1) @id = RecordEventHistoryID, @mergeCount = MergeHistoryDataCount
							FROM RecordEventHistoryAsOneDay
							WHERE RegistTime = @selectLastTime
							ORDER BY RecordEventHistoryID DESC
							
							UPDATE RecordEventHistoryAsOneDay SET MergeHistoryDataCount = @mergeCount + 1 WHERE RecordEventHistoryID = @id
						END
				END
		END
	END
