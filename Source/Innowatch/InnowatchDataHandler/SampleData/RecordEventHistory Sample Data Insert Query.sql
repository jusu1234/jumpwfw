USE InnowatchData
GO
	
TRUNCATE TABLE RecordEventHistory
GO
TRUNCATE TABLE RecordEventHistoryAsTwoHour
GO
TRUNCATE TABLE RecordEventHistoryAsSixHour
GO
TRUNCATE TABLE RecordEventHistoryAsTwelveHour
GO
TRUNCATE TABLE RecordEventHistoryAsOneDay
GO

DECLARE @count int
DECLARE @date datetime

SET @count = 1
SET @date = dateadd(dd, -1, getdate())

while (@count < 30000)
	BEGIN		
		exec SP_SetData_Client_RecordEventHistory_Test 'NVR_Event', null, @date, '<?xml version="1.0" encoding="utf-8"?>  <EventDatas>    <EventData CameraID="CAM06" RecordCameraNumber="9" PreRecordingTime="10" PostRecordingTime="10" />    <EventData CameraID="CAM07" RecordCameraNumber="10" PreRecordingTime="10" PostRecordingTime="10" />    <EventData CameraID="CAM08" RecordCameraNumber="11" PreRecordingTime="10" PostRecordingTime="10" />    <EventData CameraID="CAM09" RecordCameraNumber="12" PreRecordingTime="10" PostRecordingTime="10" />    <EventData CameraID="CAM10" RecordCameraNumber="13" PreRecordingTime="10" PostRecordingTime="10" />    <EventData CameraID="CAM11" RecordCameraNumber="14" PreRecordingTime="10" PostRecordingTime="10" />    <EventData CameraID="CAM12" RecordCameraNumber="15" PreRecordingTime="10" PostRecordingTime="10" />    <EventData CameraID="CAM13" RecordCameraNumber="16" PreRecordingTime="10" PostRecordingTime="10" />    <EventData CameraID="CAM14" RecordCameraNumber="17" PreRecordingTime="10" PostRecordingTime="10" />    <EventData CameraID="CAM15" RecordCameraNumber="18" PreRecordingTime="10" PostRecordingTime="10" />    <EventData CameraID="CAM16" RecordCameraNumber="19" PreRecordingTime="10" PostRecordingTime="10" />    <EventData CameraID="CAM17" RecordCameraNumber="20" PreRecordingTime="10" PostRecordingTime="10" />    <EventData CameraID="CAM18" RecordCameraNumber="21" PreRecordingTime="10" PostRecordingTime="10" />    <EventData CameraID="CAM19" RecordCameraNumber="22" PreRecordingTime="10" PostRecordingTime="10" />    <EventData CameraID="CAM20" RecordCameraNumber="23" PreRecordingTime="10" PostRecordingTime="10" />    <EventData CameraID="CAM21" RecordCameraNumber="24" PreRecordingTime="10" PostRecordingTime="10" />    <EventData CameraID="CAM22" RecordCameraNumber="25" PreRecordingTime="10" PostRecordingTime="10" />    <EventData CameraID="CAM23" RecordCameraNumber="26" PreRecordingTime="10" PostRecordingTime="10" />    <EventData CameraID="CAM24" RecordCameraNumber="27" PreRecordingTime="10" PostRecordingTime="10" />    <EventData CameraID="CAM25" RecordCameraNumber="28" PreRecordingTime="10" PostRecordingTime="10" />    <EventData CameraID="CAM26" RecordCameraNumber="29" PreRecordingTime="10" PostRecordingTime="10" />    <EventData CameraID="CAM27" RecordCameraNumber="30" PreRecordingTime="10" PostRecordingTime="10" />    <EventData CameraID="CAM28" RecordCameraNumber="31" PreRecordingTime="10" PostRecordingTime="10" />    <EventData CameraID="CAM29" RecordCameraNumber="32" PreRecordingTime="10" PostRecordingTime="10" />    <EventData CameraID="CAM30" RecordCameraNumber="33" PreRecordingTime="10" PostRecordingTime="10" />    <EventData CameraID="CAM31" RecordCameraNumber="34" PreRecordingTime="10" PostRecordingTime="10" />    <EventData CameraID="CAM32" RecordCameraNumber="35" PreRecordingTime="10" PostRecordingTime="10" />    <EventData CameraID="CAM33" RecordCameraNumber="36" PreRecordingTime="10" PostRecordingTime="10" />  </EventDatas>'
		SET @count = @count + 1
		SET @date = dateadd(second, 7, @date)
	END
GO
 
select *
from RecordEventHistory
go

select *
from RecordEventHistoryAsTwoHour
go

select *
from RecordEventHistoryAsSixHour
go

select *
from RecordEventHistoryAsTwelveHour
go

select *
FROM RecordEventHistoryAsOneDay
go