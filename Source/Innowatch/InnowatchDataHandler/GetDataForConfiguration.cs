﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="GetDataForConfiguration.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   The get data for configuration.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace InnowatchDataHandler
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Text;
    using Commons;

    /// <summary>
    /// The get data for configuration.
    /// </summary>
    public class GetDataForConfiguration
    {
        #region GetData

        /// <summary>
        /// The get user info.
        /// </summary>
        /// <param name="userId">The user id.</param>
        /// <param name="password">The password.</param>
        /// <returns>The result data.</returns>
        public string GetUserInfo(string userId, string password, string client)
        {
            try
            {
                if (string.Compare(client, "COMMAND", true) == 0)
                {
                    return CommonUtil.XmlToDataSet(DataManager.GetInstance.DataHandler.GetExecuteProcedureDataSet("SP_GetData_Manager_UserInfo_ForCommand", CommonUtil.ConvertToDbTypeString(userId).ToString().ToLower(), CommonUtil.ConvertToDbTypeString(password)));
                }

                if (string.Compare(client, "VIEWER", true) == 0)
                {
                    return CommonUtil.XmlToDataSet(DataManager.GetInstance.DataHandler.GetExecuteProcedureDataSet("SP_GetData_Manager_UserInfo_ForViewer", CommonUtil.ConvertToDbTypeString(userId).ToString().ToLower(), CommonUtil.ConvertToDbTypeString(password)));
                }

                if (string.Compare(client, "PLAYBACK", true) == 0)
                {
                    return CommonUtil.XmlToDataSet(DataManager.GetInstance.DataHandler.GetExecuteProcedureDataSet("SP_GetData_Manager_UserInfo_ForPlayback", CommonUtil.ConvertToDbTypeString(userId).ToString().ToLower(), CommonUtil.ConvertToDbTypeString(password)));
                }

                return CommonUtil.XmlToDataSet(DataManager.GetInstance.DataHandler.GetExecuteProcedureDataSet("SP_GetData_Manager_UserInfo", CommonUtil.ConvertToDbTypeString(userId).ToString().ToLower(), CommonUtil.ConvertToDbTypeString(password)));
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, "GetUserInfo : " + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// The get user info.
        /// </summary>
        /// <param name="privilege">
        /// The privilege.
        /// </param>
        /// <returns>
        /// The xml of user info.
        /// </returns>
        public string GetUserInfo(int privilege)
        {
            try
            {
                return CommonUtil.XmlToDataSet(DataManager.GetInstance.DataHandler.GetExecuteProcedureDataSet("SP_GetData_Manager_UserInfoByPrivilege", privilege));
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        /// <summary>
        /// The get user info.
        /// </summary>
        /// <param name="privilege">
        /// The privilege.
        /// </param>
        /// <returns>
        /// The xml of user info.
        /// </returns>
        public string GetUserPrivilegeInfo(int privilege)
        {
            try
            {
                return CommonUtil.XmlToDataSet(DataManager.GetInstance.DataHandler.GetExecuteProcedureDataSet("SP_GetData_Manager_UserPrivilegeInfo", privilege));
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Teh get project info.
        /// </summary>
        /// <returns>The xml of project info.</returns>
        public string GetProjectInfo()
        {
            try
            {
                const string ProcedureName = "SP_GetData_Manager_AllProject";

                return this.ExecuteProcedureToSerializedDataSet(ProcedureName, "ProjectInfo");
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        /// <summary>
        /// The get camera group. (Folder 정보)
        /// </summary>
        /// <param name="projectId">The project id.</param>
        /// <param name="userId">The user id.</param>
        /// <returns>The dataset of camera group.</returns>
        public DataSet GetCameraGroup(string projectId, string userId)
        {
            try
            {
                return DataManager.GetInstance.DataHandler.GetExecuteProcedureDataSet("SP_GetData_Manager_CameraGroupByProjectIdAndUserId", projectId, userId);
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        /// <summary>
        /// 카메라 컨트롤러 정보를 받아온다.
        /// </summary>
        /// <returns>
        /// 카메라 컨트롤러 정보.
        /// </returns>
        public string GetCameraControlSettingForConfiguration()
        {
            try
            {
                const string ProcedureName = "SP_GetData_Manager_AllCameraControlSetting";

                return this.ExecuteProcedureToSerializedDataSet(ProcedureName, "CameraControlSetting");
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        /// <summary>
        /// 카메라 그룹 전체 정보를 받아온다.
        /// </summary>
        /// <returns>
        /// 전체 카메라 그룹.
        /// </returns>
        public string GetAllCameraGroup()
        {
            try
            {
                return CommonUtil.XmlToDataSet(DataManager.GetInstance.DataHandler.GetExecuteProcedureDataSet("SP_GetData_Manager_AllCameraGroup"));
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        /// <summary>
        /// The get camera group mapping.
        /// </summary>
        /// <param name="projectId">The project id.</param>
        /// <param name="userId">The user id.</param>
        /// <returns>The dataset of camera group mapping.</returns>
        public DataSet GetCameraGroupMapping(string projectId, string userId)
        {
            try
            {
                return DataManager.GetInstance.DataHandler.GetExecuteProcedureDataSet("SP_GetData_Manager_CameraGroupMappingByProjectIdAndUserId", projectId, userId);
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        /// <summary>
        /// 전체 카메라 그룹 맵핑 정보를 가지고 온다.
        /// </summary>
        /// <returns>
        /// 전체 카메라 그룹 맵핑 정보.
        /// </returns>
        public string GetAllCameraGroupMapping()
        {
            try
            {
                return CommonUtil.XmlToDataSet(DataManager.GetInstance.DataHandler.GetExecuteProcedureDataSet("SP_GetData_Manager_AllCameraGroupMapping"));
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        /// <summary>
        /// The get favorite group mapping.
        /// </summary>
        /// <param name="projectId">
        /// The project id.
        /// </param>
        /// <param name="userId">
        /// The user id.
        /// </param>
        /// <param name="type">
        /// The type.
        /// </param>
        /// <returns>
        /// The dataset of favorite group mapping.
        /// </returns>
        public DataSet GetFavoriteGroupMapping(string projectId, string userId, string type)
        {
            try
            {
                return DataManager.GetInstance.DataHandler.GetExecuteProcedureDataSet("SP_GetData_Manager_FavoriteGroupMappingByProjectIdAndUserIdAndType", projectId, userId, type);
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        /// <summary>
        /// The get media server info.
        /// </summary>
        /// <returns>The xml of media server dataset info.</returns>
        public string GetMediaServerInfo()
        {
            try
            {
                const string ProcedureName = "SP_GetData_Manager_AllMediaServer";
                return this.ExecuteProcedureToSerializedDataSet(ProcedureName, "MediaServerInfo");
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        /// <summary>
        /// The get recorder info.
        /// </summary>
        /// <returns>The xml of recorder dataset info.</returns>
        public string GetRecorderInfo()
        {
            try
            {
                const string ProcedureName = "SP_GetData_Manager_AllRecorder";
                return this.ExecuteProcedureToSerializedDataSet(ProcedureName, "RecorderInfo");
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        /// <summary>
        /// The get view server info.
        /// </summary>
        /// <returns>The xml of view server dataset info.</returns>
        public string GetViewServerInfo()
        {
            try
            {
                const string ProcedureName = "SP_GetData_Manager_ViewServerInfo";
                return this.ExecuteProcedureToSerializedDataSet(ProcedureName, "ViewServerInfo");
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        /// <summary>
        /// The get display info.
        /// </summary>
        /// <returns>The xml of display dataset info.</returns>
        public string GetDisplayInfo()
        {
            try
            {
                const string ProcedureName = "SP_GetData_Manager_DisplayInfo";
                return this.ExecuteProcedureToSerializedDataSet(ProcedureName, "DisplayInfo");
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        /// <summary>
        /// The get camera controller service info.
        /// </summary>
        /// <returns>The xml of camera controller service dataset info.</returns>
        public string GetCameraControllerServiceInfo()
        {
            try
            {
                const string ProcedureName = "SP_GetData_Manager_CameraControllerServiceInfo";
                return this.ExecuteProcedureToSerializedDataSet(ProcedureName, "CameraControllerServiceInfo");
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        /// <summary>
        /// The get external command service info.
        /// </summary>
        /// <returns>The xml of external command service dataset info.</returns>
        public string GetExternalCommandServiceInfo()
        {
            try
            {
                const string ProcedureName = "SP_GetData_Manager_ExternalCommandServiceInfo";
                return this.ExecuteProcedureToSerializedDataSet(ProcedureName, "ExternalCommandServiceInfo");
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        /// <summary>
        /// The get multi camera controller info.
        /// </summary>
        /// <returns>
        /// The xml of multi camera controller dataset info.
        /// </returns>
        public string GetMultiCameraControllerInfo()
        {
            try
            {
                return CommonUtil.XmlToDataSet(DataManager.GetInstance.DataHandler.GetExecuteProcedureDataSet("SP_GetData_Manager_MultiCameraControllerInfo"));
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        /// <summary>
        /// The get video spec info.
        /// </summary>
        /// <returns>
        /// The xml of video spec dataset info.
        /// </returns>
        public string GetVideoSpecInfo()
        {
            try
            {
                const string ProcedureName = "SP_GetData_Manager_AllVideoSpec";

                return this.ExecuteProcedureToSerializedDataSet(ProcedureName, "VideoSpecInfo");
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        /// <summary>
        /// The get ptz connection info.
        /// </summary>
        /// <returns>
        /// The xml of ptz connection dataset info.
        /// </returns>
        public string GetPtzConnectionInfo()
        {
            try
            {
                const string ProcedureName = "SP_GetData_Manager_AllPtzConnection";
                return this.ExecuteProcedureToSerializedDataSet(ProcedureName, "PtzConnectionInfo");
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        /// <summary>
        /// The get camera info.
        /// </summary>
        /// <returns>
        /// The xml to camera dataset info.
        /// </returns>
        public DataSet GetCameraInfo(string projectId)
        {
            try
            {
                return DataManager.GetInstance.DataHandler.GetExecuteProcedureDataSet("SP_GetData_Manager_AllMappedCamera", projectId);

                // 삭제 대기 - 2013. 05. 09 - by shwlee.
                //return DataManager.GetInstance.DataHandler.GetExecuteProcedureDataSet("SP_GetData_Manager_AllCamera");
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        /// <summary>
        /// The get camera video mapping by camera id.
        /// </summary>
        /// <param name="cameraId">
        /// The camera id.
        /// </param>
        /// <returns>
        /// The System.String.
        /// </returns>
        public string GetCameraVideoMappingByCameraId(string cameraId)
        {
            try
            {
                return CommonUtil.XmlToDataSet(DataManager.GetInstance.DataHandler.GetExecuteProcedureDataSet("SP_GetData_Manager_CameraVideoMappingByCameraId", cameraId));
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        /// <summary>
        /// The get all camera video spec mapping.
        /// </summary>
        /// <returns>
        /// The System.String.
        /// </returns>
        public string GetAllCameraVideoSpecMapping()
        {
            try
            {
                return CommonUtil.XmlToDataSet(DataManager.GetInstance.DataHandler.GetExecuteProcedureDataSet("SP_GetData_Manager_AllCameraVideoMapping"));
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        /// <summary>
        /// 카메라 그룹 부분에 사용 할 카메라 아디를 받아온다.
        /// </summary>
        /// <param name="projectId">
        /// The project id.
        /// </param>
        /// <returns>
        /// 카메라 아이디 정보.
        /// </returns>
        public string GetCameraIdForConfiguration(string projectId)
        {
            try
            {
                return CommonUtil.XmlToDataSet(DataManager.GetInstance.DataHandler.GetExecuteProcedureDataSet("SP_GetData_Manager_CameraIdUsedInTheCameraGroup", projectId));
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        /// <summary>
        /// 카메라 그룹 부분에 사용 할 카메라 그룹 아이디를 받아온다.
        /// </summary>
        /// <param name="projectId">The project id.</param>
        /// <param name="userId">The user id.</param>
        /// <returns>카메라 그룹 아이디 정보</returns>
        public string GetCameraGroupIdForConfiguration(string projectId, string userId)
        {
            try
            {
                return CommonUtil.XmlToDataSet(DataManager.GetInstance.DataHandler.GetExecuteProcedureDataSet("SP_GetData_Manager_CameraGroupIdUsedInTheCameraGroup", projectId, userId));
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        /// <summary>
        /// 즐겨찾기 정보를 받아온다.
        /// </summary>
        /// <param name="type">
        /// The type.
        /// </param>
        /// <returns>
        /// 즐겨찾기 정보.
        /// </returns>
        public string GetFavoriteInfo(string type)
        {
            try
            {
                return CommonUtil.XmlToDataSet(DataManager.GetInstance.DataHandler.GetExecuteProcedureDataSet("SP_GetData_Manager_AllFavorite", type));
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        /// <summary>
        /// The get favorite group by project id and user id.
        /// </summary>
        /// <param name="projectId">The project id.</param>
        /// <param name="userId">The user id.</param>
        /// <param name="type">The type.</param>
        /// <returns>The xml of favorite group by project id and user id.</returns>
        public string GetFavoriteGroupByProjectIdAndUserId(string projectId, string userId, string type)
        {
            try
            {
                return CommonUtil.XmlToDataSet(DataManager.GetInstance.DataHandler.GetExecuteProcedureDataSet("SP_GetData_Manager_FavoriteGroupByProjectIdAndUserId", projectId, userId, type));
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        /// <summary>
        /// 즐겨찾기 아이디를 가지고 온다.
        /// </summary>
        /// <param name="type">
        /// The type.
        /// </param>
        /// <returns>
        /// The System.String.
        /// </returns>
        public string GetFavoriteIdList(string type)
        {
            try
            {
                return CommonUtil.XmlToDataSet(DataManager.GetInstance.DataHandler.GetExecuteProcedureDataSet("SP_GetData_Manager_FavoriteId", type));
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        /// <summary>
        /// 즐겨찾기 그룹 아이디를 가지고 온다.
        /// </summary>
        /// <param name="projectId">
        /// The project id.
        /// </param>
        /// <param name="userId">
        /// The user id.
        /// </param>
        /// <param name="type">
        /// The type.
        /// </param>
        /// <returns>
        /// 즐겨찾기 그룹 아이디.
        /// </returns>
        public string GetFavoriteGroupIdList(string projectId, string userId, string type)
        {
            try
            {
                return CommonUtil.XmlToDataSet(DataManager.GetInstance.DataHandler.GetExecuteProcedureDataSet("SP_GetData_Manager_FavoriteGroupId", projectId, userId, type));
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        /// <summary>
        /// 미디어 서버 아이디를 가지고 온다.
        /// </summary>
        /// <returns>
        /// 미디어 서버 아이디.
        /// </returns>
        public string GetMediaServerId()
        {
            try
            {   
                return CommonUtil.XmlToDataSet(DataManager.GetInstance.DataHandler.GetExecuteProcedureDataSet("SP_GetData_Manager_AllMediaServer"));
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        /// <summary>
        /// 카메라 컨트롤 아이디를 가지고 온다.
        /// </summary>
        /// <returns>
        /// 카메라 컨트롤 아이디
        /// </returns>
        public string GetCameraControlSettingId()
        {
            try
            {
                return CommonUtil.XmlToDataSet(DataManager.GetInstance.DataHandler.GetExecuteProcedureDataSet("SP_GetData_Manager_CameraControlSettingId"));
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        /// <summary>
        /// PTZ 연결 정보 아이디를 가지고 온다.
        /// </summary>
        /// <returns>PTZ 연결 정보 아이디.</returns>
        public string GetPtzConnectionInfoId()
        {
            try
            {
                return CommonUtil.XmlToDataSet(DataManager.GetInstance.DataHandler.GetExecuteProcedureDataSet("SP_GetData_Manager_PtzConnectionInfoId"));
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Camera 와 연결되지 않은 RecordCameraNumber 를 가지고 온다.
        /// </summary>
        /// <returns>RecordCameraNumber.</returns>
        public string GetRecordCameraNumber()
        {
            try
            {
                return CommonUtil.XmlToDataSet(DataManager.GetInstance.DataHandler.GetExecuteProcedureDataSet("SP_GetData_Manager_RecordCameraNumberisNull"));
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        /// <summary>
        /// 벤더 정보를 가지고 온다.
        /// </summary>
        /// <returns>
        /// 벤더 정보.
        /// </returns>
        public string GetVendorList()
        {
            try
            {
                return CommonUtil.XmlToDataSet(DataManager.GetInstance.DataHandler.GetExecuteProcedureDataSet("SP_GetData_Manager_VendorInfo"));
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        /// <summary>
        /// 사용자 전체 정보를 가지고 온다.
        /// </summary>
        /// <returns>
        /// 사용자 전체 정보.
        /// </returns>
        public string GetAllUserInfo()
        {
            try
            {
                const string ProcedureName = "SP_GetData_Manager_AllUser";

                return this.ExecuteProcedureToSerializedDataSet(ProcedureName, "UserInfo");
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        /// <summary>
        /// 사용자 전체 정보를 가지고 온다.
        /// </summary>
        /// <returns>
        /// 사용자 전체 정보.
        /// </returns>
        public string GetAllUserPrivilegeInfo()
        {
            try
            {
                const string ProcedureName = "SP_GetData_Manager_AllUserPrivilege";

                return this.ExecuteProcedureToSerializedDataSet(ProcedureName, "UserPrivilegeInfo");
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        /// <summary>
        /// 즐겨찾기 그룹 전체 정보를 가지고 온다.
        /// </summary>
        /// <returns>
        /// 즐겨찾기 전체 그룹.
        /// </returns>
        public string GetAllFavoriteGroupInfo()
        {
            try
            {
                return CommonUtil.XmlToDataSet(DataManager.GetInstance.DataHandler.GetExecuteProcedureDataSet("SP_GetData_Manager_AllFavoriteGroup"));
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        /// <summary>
        /// 즐겨찾기 그룹 맵핑 전체 정보를 가지고 온다.
        /// </summary>
        /// <returns>
        /// 즐겨찾기 그룹 맵핑 전체 정보.
        /// </returns>
        public string GetAllFavoriteGroupMappingInfo()
        {
            try
            {
                return CommonUtil.XmlToDataSet(DataManager.GetInstance.DataHandler.GetExecuteProcedureDataSet("SP_GetData_Manager_AllFavoriteGroupMapping"));
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        /// <summary>
        /// 전체 카메라 정보를 가지고 온다.
        /// </summary>
        /// <returns>
        /// 전체 카메라 정보
        /// </returns>
        public string GetAllCameraInfo()
        {
            try
            {
                const string ProcedureName = "SP_GetData_Manager_AllCamera";

                return this.ExecuteProcedureToSerializedDataSet(ProcedureName, "CameraInfo");
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        /// <summary>
        /// 전체 프로젝트 맵핑 정보를 가지고 온다.
        /// </summary>
        /// <returns>
        /// 전체 프로젝트 맵핑 정보.
        /// </returns>
        public string GetAllProjectMappingInfo()
        {
            try
            {
                const string ProcedureName = "SP_GetData_Manager_AllProjectMapping";

                return this.ExecuteProcedureToSerializedDataSet(ProcedureName, "ProjectMapping");
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        /// <summary>
        /// 전체 카메라 비디오 맵핑 정보를 가지고 온다.
        /// </summary>
        /// <returns>
        /// 전체 카메라 비디오 맵핑 정보.
        /// </returns>
        public string GetAllCameraVideoMappingInfo()
        {
            try
            {
                const string ProcedureName = "SP_GetData_Manager_AllCameraVideoMapping";

                return this.ExecuteProcedureToSerializedDataSet(ProcedureName, "CameraVideoMapping");
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        /// <summary>
        /// The get all alarm info.
        /// </summary>
        /// <returns>
        /// The System.String.
        /// </returns>
        public string GetAllAlarmInfo()
        {
            try
            {
                const string ProcedureName = "SP_GetData_Manager_AllAlarm";

                return this.ExecuteProcedureToSerializedDataSet(ProcedureName, "AlarmInfo");
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        /// <summary>
        /// The get all external command template mapping.
        /// </summary>
        /// <returns>
        /// The System.String.
        /// </returns>
        public string GetAllExternalCommandTemplateMapping()
        {
            try
            {
                const string ProcedureName = "SP_GetData_Manager_AllAlarmExternalCommandMapping";

                return this.ExecuteProcedureToSerializedDataSet(ProcedureName, "ExternalCommandSetMappingInfo");
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        /// <summary>
        /// The get all external command template mapping to data set.
        /// </summary>
        /// <returns>
        /// The System.Data.DataSet.
        /// </returns>
        public DataSet GetAllExternalCommandTemplateMappingToDataSet()
        {
            try
            {
                return DataManager.GetInstance.DataHandler.GetExecuteProcedureDataSet("SP_GetData_Manager_AllAlarmExternalCommandMapping");
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        /// <summary>
        /// The get external command template of alarm mapping.
        /// </summary>
        /// <param name="alarmId">
        /// The alarm id.
        /// </param>
        /// <returns>
        /// The System.String.
        /// </returns>
        public string GetExternalCommandTemplateOfAlarmMapping(string alarmId)
        {
            try
            {
                return CommonUtil.XmlToDataSet(DataManager.GetInstance.DataHandler.GetExecuteProcedureDataSet("SP_GetData_Manager_ExternalCommandTemplateOfAlarmMapping", alarmId));
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        /// <summary>
        /// The get command alarm action of alarm mapping.
        /// </summary>
        /// <param name="alarmId">
        /// The alarm id.
        /// </param>
        /// <returns>
        /// The System.String.
        /// </returns>
        public string GetCommandAlarmActionOfAlarmMapping(string alarmId)
        {
            try
            {
                return CommonUtil.XmlToDataSet(DataManager.GetInstance.DataHandler.GetExecuteProcedureDataSet("SP_GetData_Manager_CommandAlarmActionOfAlarmMapping", alarmId));
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        /// <summary>
        /// The get external command template mapping.
        /// </summary>
        /// <returns>
        /// The System.String.
        /// </returns>
        public string GetExternalCommandTemplateMapping()
        {
            try
            {
                return CommonUtil.XmlToDataSet(DataManager.GetInstance.DataHandler.GetExecuteProcedureDataSet("SP_GetData_Manager_AllAlarmExternalCommandMapping"));
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        /// <summary>
        /// The get command alarm action mapping.
        /// </summary>
        /// <returns>
        /// The System.String.
        /// </returns>
        public string GetCommandAlarmActionMapping()
        {
            try
            {
                return CommonUtil.XmlToDataSet(DataManager.GetInstance.DataHandler.GetExecuteProcedureDataSet("SP_GetData_Manager_AllCommandActionAlarmMapping"));
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        /// <summary>
        /// The get all command alarm action infos.
        /// </summary>
        /// <returns>
        /// The System.String.
        /// </returns>
        public string GetAllCommandAlarmActionInfos()
        {
            try
            {
                return CommonUtil.XmlToDataSet(DataManager.GetInstance.DataHandler.GetExecuteProcedureDataSet("SP_GetData_Manager_AllCommandAlarmAction"));
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        /// <summary>
        /// The get all command alarm action info mapping.
        /// </summary>
        /// <returns>
        /// The System.String.
        /// </returns>
        public string GetAllCommandAlarmActionInfoMapping()
        {
            try
            {
                const string ProcedureName = "SP_GetData_Manager_AllCommandActionAlarmMapping";

                return this.ExecuteProcedureToSerializedDataSet(ProcedureName, "CommandActionAlarmMappingInfo");
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        /// <summary>
        /// The get alarm history.
        /// </summary>
        /// <param name="pageNumber">
        /// The page Number.
        /// </param>
        /// <param name="itemCount">
        /// The item Count.
        /// </param>
        /// <returns>
        /// The result data.
        /// </returns>
        public string GetAlarmHistory(int pageNumber, int itemCount)
        {
            try
            {
                return CommonUtil.XmlToDataSet(DataManager.GetInstance.DataHandler.GetExecuteProcedureDataSet("SP_GetData_Manager_AlarmHistory", pageNumber, itemCount));
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        /// <summary>
        /// The get all external command.
        /// </summary> 
        /// <returns>
        /// The response data.
        /// </returns>
        public string GetAllExternalCommand()
        {
            try
            {
                const string ProcedureName = "SP_GetData_Manager_AllExternalCommand";

                return this.ExecuteProcedureToSerializedDataSet(ProcedureName, "ExternalCommandInfo");
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        /// <summary>
        /// The get external command by id.
        /// </summary>
        /// <param name="externalCommandId">
        /// The id.
        /// </param>
        /// <returns>
        /// The System.Data.DataSet.
        /// </returns>
        public DataSet GetExternalCommandById(string externalCommandId)
        {
            try
            {
                return DataManager.GetInstance.DataHandler.GetExecuteProcedureDataSet("SP_GetData_Manager_ExternalCommandByExternalCommandId", externalCommandId);
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        /// <summary>
        /// The get external command template by external command id.
        /// </summary>
        /// <param name="externalCommandId">
        /// The id.
        /// </param>
        /// <returns>
        /// The System.Data.DataSet.
        /// </returns>
        public DataSet GetExternalCommandTemplateByExternalCommandId(string externalCommandId)
        {
            try
            {
                return DataManager.GetInstance.DataHandler.GetExecuteProcedureDataSet("SP_GetData_Manager_ExternalCommandTemplateByExternalCommandId", CommonUtil.ConvertToDbTypeString(externalCommandId));
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        /// <summary>
        /// The get all external command template.
        /// </summary>
        /// <returns>
        /// The System.String.
        /// </returns>
        public string GetAllExternalCommandTemplate()
        {
            try
            {
                const string ProcedureName = "SP_GetData_Manager_AllExternalCommandTemplate";

                return this.ExecuteProcedureToSerializedDataSet(ProcedureName, "ExternalCommandSetInfo");
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        /// <summary>
        /// The get all external command id list.
        /// </summary>
        /// <returns>
        /// The System.String.
        /// </returns>
        public string GetAllExternalCommandIDList()
        {
            try
            {
                return CommonUtil.XmlToDataSet(DataManager.GetInstance.DataHandler.GetExecuteProcedureDataSet("SP_GetData_Manager_ExternalCommandIdList"));
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        /// <summary>
        /// The get multi external command.
        /// </summary>
        /// <returns>
        /// The result data.
        /// </returns>
        public string GetMultiExternalCommandId()
        {
            try
            {
                return CommonUtil.XmlToDataSet(DataManager.GetInstance.DataHandler.GetExecuteProcedureDataSet("SP_GetData_Manager_AllMultiExternalCommand"));
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        /// <summary>
        /// The get external command ip info.
        /// </summary>
        /// <param name="viewServerId">
        /// The external command id.
        /// </param>
        /// <returns>
        /// The result data.
        /// </returns>
        public string GetExternalCommandIpInfo(string viewServerId)
        {
            try
            {
                return
                    CommonUtil.XmlToDataSet(
                        DataManager.GetInstance.DataHandler.GetExecuteProcedureDataSet("SP_GetData_Manager_ExternalCommandIpInfo", viewServerId));
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        #region Get Data Of Nvr

        /// <summary>
        /// The get storage info.
        /// </summary>
        /// <returns>
        /// The xml to storage info data set.
        /// </returns>
        public string GetStorageInfo()
        {
            try
            {
                const string ProcedureName = "SP_GetData_Manager_AllStorage";

                return this.ExecuteProcedureToSerializedDataSet(ProcedureName, "StorageInfo");
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        /// <summary>
        /// The get record schedule info.
        /// </summary>
        /// <returns>
        /// The xml to record schedule info data set.
        /// </returns>
        public string GetRecordScheduleInfo()
        {
            try
            {
                const string ProcedureName = "SP_GetData_Manager_AllRecordSchedule";

                return this.ExecuteProcedureToSerializedDataSet(ProcedureName, "RecordScheduleInfo");
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        /// <summary>
        /// The get archive schedule info.
        /// </summary>
        /// <returns>
        /// The xml to archive schedule info data set.
        /// </returns>
        public string GetArchiveScheduleInfo()
        {
            try
            {
                const string ProcedureName = "SP_GetData_Manager_AllArchiveSchedule";

                return this.ExecuteProcedureToSerializedDataSet(ProcedureName, "ArchiveScheduleInfo");
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        /// <summary>
        /// The get record camera info.
        /// </summary>
        /// <returns>
        /// The xml to record camera info data set.
        /// </returns>
        public string GetRecordCameraInfo()
        {
            try
            {
                const string ProcedureName = "SP_GetData_Manager_AllRecordCamera";

                return this.ExecuteProcedureToSerializedDataSet(ProcedureName, "RecordCameraInfo");
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        /// <summary>
        /// The get record storage list.
        /// </summary>
        /// <returns>
        /// The xml to record storage list data set.
        /// </returns>
        public string GetRecordStorageList()
        {
            try
            {
                return CommonUtil.XmlToDataSet(DataManager.GetInstance.DataHandler.GetExecuteProcedureDataSet("SP_GetData_Manager_RecordStorageList"));
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        /// <summary>
        /// The get record schedule list.
        /// </summary>
        /// <returns>
        /// The xml to record schedule list data.
        /// </returns>
        public string GetRecordScheduleList()
        {
            try
            {
                return CommonUtil.XmlToDataSet(DataManager.GetInstance.DataHandler.GetExecuteProcedureDataSet("SP_GetData_Manager_RecordScheduleList"));
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        /// <summary>
        /// The get archive schedule list.
        /// </summary>
        /// <returns>
        /// The xml to archvie schedule list data.
        /// </returns>
        public string GetArchiveScheduleList()
        {
            try
            {
                return CommonUtil.XmlToDataSet(DataManager.GetInstance.DataHandler.GetExecuteProcedureDataSet("SP_GetData_Manager_ArchiveScheduleList"));
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        /// <summary>
        /// The get recorder list.
        /// </summary> 
        /// <returns>
        /// The xml to recorder list data.
        /// </returns>
        public string GetRecorderList()
        {
            try
            {
                return CommonUtil.XmlToDataSet(DataManager.GetInstance.DataHandler.GetExecuteProcedureDataSet("SP_GetData_Manager_RecorderList"));
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        /// <summary>
        /// The get camera to record info.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string GetCameraToRecordInfo()
        {
            try
            {
                return CommonUtil.XmlToDataSet(DataManager.GetInstance.DataHandler.GetExecuteProcedureDataSet("SP_GetData_Manager_CameraToRecordList"));
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }
        
        /// <summary>
        /// The get alarm type list.
        /// </summary>
        /// <returns>
        /// The xml to alarm type list data.
        /// </returns>
        public string GetAlarmTypeList()
        {
            try
            {
                return CommonUtil.XmlToDataSet(DataManager.GetInstance.DataHandler.GetExecuteProcedureDataSet("SP_GetData_Manager_AllAlarmType"));
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        /// <summary>
        /// The get project camer list.
        /// </summary>
        /// <param name="projectId">
        /// The project Id.
        /// </param>
        /// <returns>
        /// The xml to project camera list data.
        /// </returns>
        public string GetProjectCameraList(string projectId)
        {
            try
            {
                return CommonUtil.XmlToDataSet(DataManager.GetInstance.DataHandler.GetExecuteProcedureDataSet("SP_GetData_Manager_ProjectCameraList", CommonUtil.ConvertToDbTypeString(projectId)));
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        #endregion // Get Data Of Nvr

        #region Get Data of Log

        /// <summary>
        /// The get program log list info.
        /// </summary>
        /// <param name="programType">
        /// The program Type.
        /// </param>
        /// <returns>
        /// The xml to program log list info.
        /// </returns>
        public string GetProgramLogListInfo(string programType)
        {
            try
            {
                return CommonUtil.XmlToDataSet(DataManager.GetInstance.DataHandler.GetExecuteProcedureDataSet("SP_GetData_Manager_ProgramLogList", programType));
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        /// <summary>
        /// The get program log alarm list info.
        /// </summary>
        /// <param name="programType">
        /// The program type.
        /// </param>
        /// <returns>
        /// The xml to program log alarm list info.
        /// </returns>
        public string GetProgramLogAlarmListInfo(string programType)
        {
            try
            {
                return CommonUtil.XmlToDataSet(DataManager.GetInstance.DataHandler.GetExecuteProcedureDataSet("SP_GetData_Manager_ProgramLogAlarmList", programType));
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        /// <summary>
        /// The get program log all list info.
        /// </summary>
        /// <param name="programType">
        /// The program type.
        /// </param>
        /// <returns>
        /// The xml to program log all list info.
        /// </returns>
        public string GetProgramLogAllListInfo(string programType)
        {
            try
            {
                const string ProcedureName = "SP_GetData_Manager_AllProgramLogList";

                return this.ExecuteProcedureToSerializedDataSet(
                                ProcedureName,
                                "AllProgramLogListInfo",
                                programType);
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        /// <summary>
        /// The get log type external command mapping info.
        /// </summary>
        /// <param name="programType">
        /// The program Type.
        /// </param>
        /// <returns>
        /// The xml to log type external command mapping info.
        /// </returns>
        public string GetLogTypeExternalCommandMappingInfo(string programType)
        {
            try
            {
                const string ProcedureName = "SP_GetData_Manager_LogTypeExternalCommandMappingInfoByProgramType";

                return this.ExecuteProcedureToSerializedDataSet(
                                ProcedureName, 
                                "LogTypeExternalCommandMappingInfo",
                                programType);
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        /// <summary>
        /// The get log type external command mapping data set info.
        /// </summary>
        /// <param name="programType">
        /// The program Type.
        /// </param>
        /// <returns>
        /// The data set.
        /// </returns>
        public DataSet GetLogTypeExternalCommandMappingDataSetInfoByProgramType(string programType)
        {
            try
            {
                return DataManager.GetInstance.DataHandler.GetExecuteProcedureDataSet("SP_GetData_Manager_LogTypeExternalCommandMappingInfoByProgramType", programType);
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        /// <summary>
        /// The get external command info of log alarm mapping.
        /// </summary>
        /// <param name="programType">
        /// The program type.
        /// </param>
        /// <param name="logType">
        /// The log Type.
        /// </param>
        /// <returns>
        /// The xml to external command info of log alarm.
        /// </returns>
        public string GetExternalCommandInfoOfLogAlarmMapping(string programType, string logType)
        {
            try
            {
                return CommonUtil.XmlToDataSet(DataManager.GetInstance.DataHandler.GetExecuteProcedureDataSet("SP_GetData_Manager_ExternalCommandOfLogAlarmMappingInfo", programType, logType));
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        /// <summary>
        /// The get log history.
        /// </summary>
        /// <param name="pageNumber">
        /// The page number.
        /// </param>
        /// <param name="itemCount">
        /// The item count.
        /// </param>
        /// <returns>
        /// The response data.
        /// </returns>
        public string GetLogHitory(int pageNumber, int itemCount)
        {
            try
            {
                return CommonUtil.XmlToDataSet(DataManager.GetInstance.DataHandler.GetExecuteProcedureDataSet("SP_GetData_Manager_LogHistory", pageNumber, itemCount));
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        #endregion // Get Data Of Log

        #region DataSet Handle

        private string ExecuteProcedureToSerializedDataSet(string procedureName, string tableName, params object[] parameterValues)
        {
            try
            {
                var resultDataSet = DataManager.GetInstance.DataHandler.GetExecuteProcedureDataSet(procedureName, parameterValues);
                this.SetDataSetTableName(resultDataSet, tableName);

                return CommonUtil.XmlToDataSet(resultDataSet);
            }
            catch (Exception ex)
            {
                DataManager.GetInstance.DataHandler.InsertLogData(LogType.Error, ex.Message);
                return null;
            }
        }

        private void SetDataSetTableName(DataSet resultDataSet, string name)
        {
            if (resultDataSet == null)
            {
                return;
            }

            if (resultDataSet.Tables.Count <= 0)
            {
                return;
            }

            resultDataSet.DataSetName = name;
            resultDataSet.Tables[0].TableName = name;
        }

        #endregion

        #endregion // GetData
    }
}
