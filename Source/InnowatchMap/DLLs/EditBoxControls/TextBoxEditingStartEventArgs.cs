﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EditBoxControls
{
    public class TextBoxEditingStartEventArgs : EventArgs
    {
        public TextBoxEditingStartEventArgs(string oldValue)
        {
            this.OldValue = oldValue;
        }

        public string OldValue { get; set; }
    }
}
