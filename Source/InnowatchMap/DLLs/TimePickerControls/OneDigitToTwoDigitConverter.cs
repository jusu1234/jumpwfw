﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Windows.Data;
using System.Windows;

namespace Innotive.InnoWatch.DLLs.TimePickerControls
{
    /// <summary>
    /// One Digit => Two Digit Converter
    /// </summary>
    public class OneDigitToTwoDigitConverter : IValueConverter
    {
        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            string returnvalue = "00";

            if (value != null)
            {
                if (value is int)
                {
                    int current = (int)value;

                    returnvalue = current.ToString("00");
                }
            }

            return returnvalue;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            int returnvalue = 0;

            if (value != null)
            {
                if (value is string)
                {
                    string currentString = (string)value;

                    try
                    {
                        returnvalue = Int32.Parse(currentString);
                    }
                    catch
                    {
                    }
                }
            }

            return returnvalue;
        }

        #endregion
    }
}
