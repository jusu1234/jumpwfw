﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace Innotive.InnoWatch.DLLs.TimePickerControls
{
    /// <summary>
    /// Delegate for the TimeSelectedChanged event
    /// </summary>
    /// <param name="sender">The object raising the event</param>
    /// <param name="e">The routed event arguments</param>
    public delegate void TimeSelectedChangedEventHandler(object sender, TimeSelectedChangedRoutedEventArgs e);

    /// <summary>
    /// Routed event arguments for the TimeSelectedChanged event
    /// </summary>
    public class TimeSelectedChangedRoutedEventArgs : RoutedEventArgs
    {
        /// <summary>
        /// Gets or sets the new time
        /// </summary>
        public TimeSpan NewTime { get; set; }

        /// <summary>
        /// Gets or sets the old time
        /// </summary>
        public TimeSpan OldTime { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="routedEvent">The event that is raised </param>
        public TimeSelectedChangedRoutedEventArgs(RoutedEvent routedEvent)
            : base(routedEvent) { }
    }
}
