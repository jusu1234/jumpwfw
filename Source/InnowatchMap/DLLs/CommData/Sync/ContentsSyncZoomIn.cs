﻿
namespace Innotive.InnoWatch.DLLs.CommData.Sync
{
    using System;
    using System.Windows;
    using System.Xml.Serialization;

    [XmlRoot("ZoomIn")]
    public class ZoomInData : BaseSync
    {
        [XmlAttribute("LayoutGuid")]
        public Guid LayoutGuid;

        [XmlAttribute("DesireRect")]
        public Rect DesireRect;

        //public string SaveDataToXML()
        //{
        //    XmlSerializer serializer = new XmlSerializer(typeof(ZoomInData));
        //    MemoryStream memStream = new MemoryStream();
        //    XmlTextWriter xmlWriter = new XmlTextWriter(memStream, Encoding.UTF8);
        //    serializer.Serialize(xmlWriter, this);
        //    xmlWriter.Close();
        //    memStream.Close();

        //    string xmlData = Encoding.UTF8.GetString(memStream.GetBuffer());
        //    xmlData = xmlData.Substring(xmlData.IndexOf('<'));
        //    xmlData = xmlData.Substring(0, (xmlData.LastIndexOf('>') + 1));

        //    return xmlData;
        //}

        //public static ZoomInData ReadDataFromXML(string xmlData)
        //{
        //    XmlSerializer serializer = null;

        //    serializer = new XmlSerializer(typeof(ZoomInData));

        //    StringReader stringReader = new StringReader(xmlData);
        //    XmlTextReader xmlReader = new XmlTextReader(stringReader);

        //    ZoomInData zoomInData = serializer.Deserialize(xmlReader) as ZoomInData;

        //    xmlReader.Close();
        //    stringReader.Close();

        //    return zoomInData;
        //}
    }
}