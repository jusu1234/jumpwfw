
namespace Innotive.InnoWatch.DLLs.CommData.Sync
{
    using System;
    using System.Xml.Serialization;

    [XmlRoot("ChangeElementProperty")]
    public class ChangeElementPropertyData : BaseSync
    {
        [XmlAttribute("ElementGuid")]
        public Guid ElementGuid;     

        //DependencyProperty를 Network으로 보내는게 안되서 string으로 보냄 !!
        //받는쪽에서 string값을 parsing해서 DependencyProperty로 복원함 !!
        //string propertyName = property.OwnerType.Name + "." + property.Name;
        //string propertyValue = value.ToString();

        [XmlAttribute("PropertyName")]
        //public DependencyProperty PropertyName;
        public string PropertyName;

        [XmlAttribute("PropertyValue")]
        //public object PropertyValue;
        public string PropertyValue;

        [XmlAttribute("Effect")]
        public string Effect;

        [XmlAttribute("StartTime")]
        public double StartTime;

        [XmlAttribute("Duration")]
        public double Duration;

        //public string SaveDataToXML()
        //{
        //    XmlSerializer serializer = new XmlSerializer(typeof(ChangeElementPropertyData));
        //    MemoryStream memStream = new MemoryStream();
        //    XmlTextWriter xmlWriter = new XmlTextWriter(memStream, Encoding.UTF8);
        //    serializer.Serialize(xmlWriter, this);
        //    xmlWriter.Close();
        //    memStream.Close();

        //    string xmlData = Encoding.UTF8.GetString(memStream.GetBuffer());
        //    xmlData = xmlData.Substring(xmlData.IndexOf('<'));
        //    xmlData = xmlData.Substring(0, (xmlData.LastIndexOf('>') + 1));

        //    return xmlData;
        //}

        //public static ChangeElementPropertyData ReadDataFromXML(string xmlData)
        //{
        //    XmlSerializer serializer = null;

        //    serializer = new XmlSerializer(typeof(ChangeElementPropertyData));

        //    StringReader stringReader = new StringReader(xmlData);
        //    XmlTextReader xmlReader = new XmlTextReader(stringReader);

        //    ChangeElementPropertyData changeElementPropertyData = serializer.Deserialize(xmlReader) as ChangeElementPropertyData;

        //    xmlReader.Close();
        //    stringReader.Close();

        //    return changeElementPropertyData;
        //}
    }
}