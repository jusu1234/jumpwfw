﻿
namespace Innotive.InnoWatch.DLLs.CommData.Sync
{
    using System.Xml.Serialization;

    [XmlRoot("DisplayMode")]
    public class DisplayModeData : BaseSync
    {
        [XmlAttribute("DisplayMode")]
        public ConsoleDisplayMode DisplayMode;

        //public string SaveDataToXML()
        //{
        //    XmlSerializer serializer = new XmlSerializer(typeof(DisplayModeData));
        //    MemoryStream memStream = new MemoryStream();
        //    XmlTextWriter xmlWriter = new XmlTextWriter(memStream, Encoding.UTF8);
        //    serializer.Serialize(xmlWriter, this);
        //    xmlWriter.Close();
        //    memStream.Close();

        //    string xmlData = Encoding.UTF8.GetString(memStream.GetBuffer());
        //    xmlData = xmlData.Substring(xmlData.IndexOf('<'));
        //    xmlData = xmlData.Substring(0, (xmlData.LastIndexOf('>') + 1));

        //    return xmlData;
        //}

        //public static DisplayModeData ReadDataFromXML(string xmlData)
        //{
        //    XmlSerializer serializer = null;

        //    serializer = new XmlSerializer(typeof(DisplayModeData));

        //    StringReader stringReader = new StringReader(xmlData);
        //    XmlTextReader xmlReader = new XmlTextReader(stringReader);

        //    DisplayModeData displayModeData = serializer.Deserialize(xmlReader) as DisplayModeData;

        //    xmlReader.Close();
        //    stringReader.Close();

        //    return displayModeData;
        //}
    }
}