﻿
namespace Innotive.InnoWatch.DLLs.CommData.Sync
{
    using System;
    using System.Xml.Serialization;

    [XmlRoot("GoToLocation")]
    public class GoToLocationData : BaseSync
    {       
        //[XmlElement("LayoutGuid")]
        //public Guid LayoutGuid;

        [XmlAttribute("BookmarkLayoutGuid")]
        public Guid BookmarkLayoutGuid;

        [XmlAttribute("BookmarkGuid")]
        public Guid BookmarkGuid;

        [XmlAttribute("Effect")]
        public string Effect;

        [XmlAttribute("StartTime")]
        public double StartTime;

        [XmlAttribute("Duration")]
        public double Duration;

        //public string SaveDataToXML()
        //{
        //    XmlSerializer serializer = new XmlSerializer(typeof(GoToLocationData));
        //    MemoryStream memStream = new MemoryStream();
        //    XmlTextWriter xmlWriter = new XmlTextWriter(memStream, Encoding.UTF8);
        //    serializer.Serialize(xmlWriter, this);
        //    xmlWriter.Close();
        //    memStream.Close();

        //    string xmlData = Encoding.UTF8.GetString(memStream.GetBuffer());
        //    xmlData = xmlData.Substring(xmlData.IndexOf('<'));
        //    xmlData = xmlData.Substring(0, (xmlData.LastIndexOf('>') + 1));

        //    return xmlData;
        //}

        //public static GoToLocationData ReadDataFromXML(string xmlData)
        //{
        //    XmlSerializer serializer = null;

        //    serializer = new XmlSerializer(typeof(GoToLocationData));

        //    StringReader stringReader = new StringReader(xmlData);
        //    XmlTextReader xmlReader = new XmlTextReader(stringReader);

        //    GoToLocationData goToLocationData = serializer.Deserialize(xmlReader) as GoToLocationData;

        //    xmlReader.Close();
        //    stringReader.Close();

        //    return goToLocationData;
        //}
    }
}