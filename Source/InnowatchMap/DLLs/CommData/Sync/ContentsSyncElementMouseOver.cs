
namespace Innotive.InnoWatch.DLLs.CommData.Sync
{
    using System;
    using System.Xml.Serialization;

    [XmlRoot("ElementMouseOver")]
    public class ElementMouseOverData : BaseSync
    {
        [XmlAttribute("ElementGuid")]
        public Guid ElementGuid;

        [XmlAttribute("IsMouseOver")]
        public bool IsMouseOver;

        //public string SaveDataToXML()
        //{
        //    XmlSerializer serializer = new XmlSerializer(typeof(ElementMouseOverData));
        //    MemoryStream memStream = new MemoryStream();
        //    XmlTextWriter xmlWriter = new XmlTextWriter(memStream, Encoding.UTF8);
        //    serializer.Serialize(xmlWriter, this);
        //    xmlWriter.Close();
        //    memStream.Close();

        //    string xmlData = Encoding.UTF8.GetString(memStream.GetBuffer());
        //    xmlData = xmlData.Substring(xmlData.IndexOf('<'));
        //    xmlData = xmlData.Substring(0, (xmlData.LastIndexOf('>') + 1));

        //    return xmlData;
        //}

        //public static ElementMouseOverData ReadDataFromXML(string xmlData)
        //{
        //    XmlSerializer serializer = null;

        //    serializer = new XmlSerializer(typeof(ElementMouseOverData));

        //    StringReader stringReader = new StringReader(xmlData);
        //    XmlTextReader xmlReader = new XmlTextReader(stringReader);

        //    ElementMouseOverData elementMouseOverData = serializer.Deserialize(xmlReader) as ElementMouseOverData;

        //    xmlReader.Close();
        //    stringReader.Close();

        //    return elementMouseOverData;
        //}
    }
}