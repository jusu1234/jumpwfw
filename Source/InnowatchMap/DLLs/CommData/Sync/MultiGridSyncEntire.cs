﻿
namespace Innotive.InnoWatch.DLLs.CommData.Sync
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Text;
    using System.Windows;
    using System.Xml;
    using System.Xml.Serialization;

    #region MultiGrid Element 정보

    public enum MultiGridCellElementType {Empty, CameraControl = 1, RdsViewerControl, LayoutControl };

    //Camera 정보
    [XmlRoot("MultiGridCameraData")]
    public class MultiGridCameraData
    {
        [XmlAttribute("CameraSyncGuid")]
        public Guid CameraSyncGuid;

        [XmlAttribute("CameraID")]
        public string CameraID;
        
        //기본 생성자가 없으면 Serialize가 안됨 !! 왜 그런지는 잘...
        public MultiGridCameraData()
        {
        }

        public MultiGridCameraData(Guid cameraSyncGuid, string cameraID)
        {
            this.CameraSyncGuid = cameraSyncGuid;
            this.CameraID = cameraID;
        }
    }

    //Rds 정보
    [XmlRoot("MultiGridRdsData")]
    public class MultiGridRdsData
    {
        [XmlAttribute("RdsSyncGuid")]
        public Guid RdsSyncGuid;

        [XmlAttribute("RdsID")]
        public string RdsID;

        [XmlAttribute("RdsEnabledDisplay")]
        public bool RdsEnabledDisplay;

        [XmlAttribute("RdsControlEnabled")]
        public bool RdsControlEnabled;

        //기본 생성자가 없으면 Serialize가 안됨 !! 왜 그런지는 잘...
        public MultiGridRdsData()
        {
        }

        public MultiGridRdsData(Guid rdsSyncGuid, string rdsID, bool rdsEnabledDisplay, bool rdsControlEnabled)
        {
            this.RdsSyncGuid = rdsSyncGuid;
            this.RdsID = rdsID;
            this.RdsEnabledDisplay = rdsEnabledDisplay;
            this.RdsControlEnabled = rdsControlEnabled;
        }
    }

    //Layout 정보
    [XmlRoot("MultiGridLayoutData")]
    public class MultiGridLayoutData
    {
        [XmlAttribute("LayoutSyncGuid")]
        public Guid LayoutSyncGuid;

        [XmlAttribute("LayoutName")]
        public string LayoutName;

        //기본 생성자가 없으면 Serialize가 안됨 !! 왜 그런지는 잘...
        public MultiGridLayoutData()
        {
        }

        public MultiGridLayoutData(Guid layoutSyncGuid, string layoutName)
        {
            this.LayoutSyncGuid = layoutSyncGuid;
            this.LayoutName = layoutName;
        }
    }

    #endregion MultiGrid Element 정보

    #region MultiGrid FullScreen Data

    [XmlRoot("MultiGridFullScreenDataForSync")]
    public class MultiGridFullScreenDataForSync : BaseSync
    {
        [XmlAttribute("GridControlSyncGuid")]
        public Guid GridControlSyncGuid;

        [XmlAttribute("CellSyncGuid")]
        public Guid CellSyncGuid;

        public MultiGridFullScreenDataForSync()
        {
        }

        public MultiGridFullScreenDataForSync(Guid gridControlSyncGuid, Guid cellSyncGuid)
        {
            GridControlSyncGuid = gridControlSyncGuid;
            CellSyncGuid = cellSyncGuid;
        }

        public static MultiGridFullScreenDataForSync ReadDataFromXML(string xmlData)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(MultiGridFullScreenDataForSync));
            StringReader stringReader = new StringReader(xmlData);
            XmlTextReader xmlReader = new XmlTextReader(stringReader);

            MultiGridFullScreenDataForSync data = serializer.Deserialize(xmlReader) as MultiGridFullScreenDataForSync;

            xmlReader.Close();
            stringReader.Close();

            return data;
        }

        public static MultiGridFullScreenDataForSync ReadDataFromXMLFile(string fileName)
        {
            string xmlData = string.Empty;

            TextReader streamReader = new StreamReader(fileName);
            xmlData = streamReader.ReadToEnd();

            return MultiGridFullScreenDataForSync.ReadDataFromXML(xmlData);
        }

        public string SaveDataToXML()
        {
            XmlSerializer serializer = new XmlSerializer(typeof(MultiGridFullScreenDataForSync));
            MemoryStream memStream = new MemoryStream();

            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Indent = true;
            settings.IndentChars = new string(' ', 4);
            settings.NewLineOnAttributes = false;
            settings.Encoding = Encoding.UTF8;

            XmlWriter xmlWriter = XmlWriter.Create(memStream, settings);
            serializer.Serialize(xmlWriter, this);
            xmlWriter.Close();
            memStream.Close();

            string xmlData = Encoding.UTF8.GetString(memStream.GetBuffer());
            xmlData = xmlData.Substring(xmlData.IndexOf('<'));
            xmlData = xmlData.Substring(0, (xmlData.LastIndexOf('>') + 1));

            return xmlData;
        }
    }

    #endregion MultiGrid FullScreen Data

    #region MultiGrid Cell Data

    //TODO -blackRoot : XmlRootAttribute와 XmlRoot의 차이를 알아봐야 할듯..
    //[XmlRootAttribute("MultiGridCellDataForSync")]
    [XmlRoot("MultiGridCellDataForSync")]
    public class MultiGridCellDataForSync : BaseSync
    {
        [XmlAttribute("GridControlSyncGuid")]
        public Guid GridControlSyncGuid;

        [XmlAttribute("CellSyncGuid")]
        public Guid CellSyncGuid;

        //XmlElement로 Rect값을 넣을 경우 xml data의 길이가 길어지는 문제가 발생함 !!
        //data를 줄이기 위해서 아래 left, top, width, height로 구분해서 추가함 !!
        [XmlElement("RectForCanvas")]
        public Rect RectForCanvas;

        /*
        //XmlElement로 할 경우 <Left>10</Left>
        //XmlAttribute로 할 경우 <MultiGridCellData Left="10"/> 이 됨
        //XmlAttribute의 data가 더 짧기 때문에 이걸 사용함 !!
        //[XmlElement("Left")]
        [XmlAttribute("Left")]
        public double Left;

        [XmlAttribute("Top")]
        public double Top;

        [XmlAttribute("Width")]
        public double Width;

        [XmlAttribute("Height")]
        public double Height;

        [XmlElement("RectForCanvas")]
        public Rect RectForCanvas;
        */

        ///////////////////////////////
        //ElementType이 CameraControl이면 아래 CameraInfo를 사용
        //ElementType이 RdsControl이면 아래 RdsInfo를 사용
        [XmlAttribute("ElementType")]
        public string ElementType;

        //[XmlAttribute("CameraInfo")]
        [XmlElement("CameraInfo")]
        public MultiGridCameraData CameraInfo;

        //[XmlAttribute("RdsInfo")]
        [XmlElement("RdsInfo")]
        public MultiGridRdsData RdsInfo;

        //[XmlAttribute("LayoutInfo")]
        [XmlElement("LayoutInfo")]
        public MultiGridLayoutData LayoutInfo;
        ///////////////////////////////

        
        public MultiGridCellDataForSync()
        {
        }
        
        public MultiGridCellDataForSync(Guid gridControlSyncGuid, Guid cellSyncGuid, Rect rectForCanvas, Guid visibleCameraSyncGuid, string visibleCameraID)
        {
            this.GridControlSyncGuid = gridControlSyncGuid;
            this.CellSyncGuid = cellSyncGuid;
            this.RectForCanvas = new Rect(rectForCanvas.X, rectForCanvas.Y, rectForCanvas.Width, rectForCanvas.Height);
            this.ElementType = MultiGridCellElementType.CameraControl.ToString();
            this.CameraInfo = new MultiGridCameraData(visibleCameraSyncGuid, visibleCameraID);
        }

        public MultiGridCellDataForSync(Guid gridControlSyncGuid, Guid cellSyncGuid, Rect rectForCanvas,
                                        Guid visibleRdsSyncGuid, string visibleRdsID, string visibleRdsServerIP, int visibleRdsPort, 
                                        bool visibleRdsEnabledDisplay, bool visibleRdsControlEnabled)
        {
            this.GridControlSyncGuid = gridControlSyncGuid;
            this.CellSyncGuid = cellSyncGuid;
            this.RectForCanvas = new Rect(rectForCanvas.X, rectForCanvas.Y, rectForCanvas.Width, rectForCanvas.Height);
            this.ElementType = MultiGridCellElementType.RdsViewerControl.ToString();
            this.RdsInfo = new MultiGridRdsData(visibleRdsSyncGuid, visibleRdsID, visibleRdsEnabledDisplay, visibleRdsControlEnabled);
        }
        
        public MultiGridCellDataForSync(Guid gridControlSyncGuid, Guid cellSyncGuid, Rect rectForCanvas, MultiGridCameraData visibleCameraInfo)
        {
            this.GridControlSyncGuid = gridControlSyncGuid;
            this.CellSyncGuid = cellSyncGuid;
            this.RectForCanvas = new Rect(rectForCanvas.X, rectForCanvas.Y, rectForCanvas.Width, rectForCanvas.Height);
            this.ElementType = MultiGridCellElementType.CameraControl.ToString();
            this.CameraInfo = new MultiGridCameraData(visibleCameraInfo.CameraSyncGuid, visibleCameraInfo.CameraID);
        }

        public MultiGridCellDataForSync(Guid gridControlSyncGuid, Guid cellSyncGuid, Rect rectForCanvas, MultiGridRdsData visibleRdsInfo)
        {
            this.GridControlSyncGuid = gridControlSyncGuid;
            this.CellSyncGuid = cellSyncGuid;
            this.RectForCanvas = new Rect(rectForCanvas.X, rectForCanvas.Y, rectForCanvas.Width, rectForCanvas.Height);
            this.ElementType = MultiGridCellElementType.RdsViewerControl.ToString();
            this.RdsInfo = new MultiGridRdsData(visibleRdsInfo.RdsSyncGuid, visibleRdsInfo.RdsID, visibleRdsInfo.RdsEnabledDisplay, visibleRdsInfo.RdsControlEnabled);
        }

        public MultiGridCellDataForSync(Guid gridControlSyncGuid, Guid cellSyncGuid, Rect rectForCanvas, MultiGridLayoutData visibleLayoutInfo)
        {
            this.GridControlSyncGuid = gridControlSyncGuid;
            this.CellSyncGuid = cellSyncGuid;
            this.RectForCanvas = new Rect(rectForCanvas.X, rectForCanvas.Y, rectForCanvas.Width, rectForCanvas.Height);
            this.ElementType = MultiGridCellElementType.LayoutControl.ToString();
            this.LayoutInfo = new MultiGridLayoutData(visibleLayoutInfo.LayoutSyncGuid, visibleLayoutInfo.LayoutName);
        }

        public MultiGridCellDataForSync(Guid gridControlSyncGuid, Guid cellSyncGuid, Rect rectForCanvas)
        {
            this.GridControlSyncGuid = gridControlSyncGuid;
            this.CellSyncGuid = cellSyncGuid;
            this.RectForCanvas = new Rect(rectForCanvas.X, rectForCanvas.Y, rectForCanvas.Width, rectForCanvas.Height);
            this.ElementType = MultiGridCellElementType.Empty.ToString();
        }

        public static MultiGridCellDataForSync ReadDataFromXML(string xmlData)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(MultiGridCellDataForSync));
            StringReader stringReader = new StringReader(xmlData);
            XmlTextReader xmlReader = new XmlTextReader(stringReader);

            MultiGridCellDataForSync data = serializer.Deserialize(xmlReader) as MultiGridCellDataForSync;

            xmlReader.Close();
            stringReader.Close();

            return data;
        }

        public static MultiGridCellDataForSync ReadDataFromXMLFile(string fileName)
        {
            string xmlData = string.Empty;

            TextReader streamReader = new StreamReader(fileName);
            xmlData = streamReader.ReadToEnd();

            return MultiGridCellDataForSync.ReadDataFromXML(xmlData);
        }

        public string SaveDataToXML()
        {
            XmlSerializer serializer = new XmlSerializer(typeof(MultiGridCellDataForSync));
            MemoryStream memStream = new MemoryStream();

            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Indent = true;
            settings.IndentChars = new string(' ', 4);
            settings.NewLineOnAttributes = false;
            settings.Encoding = Encoding.UTF8;

            XmlWriter xmlWriter = XmlWriter.Create(memStream, settings);
            serializer.Serialize(xmlWriter, this);
            xmlWriter.Close();
            memStream.Close();

            string xmlData = Encoding.UTF8.GetString(memStream.GetBuffer());
            xmlData = xmlData.Substring(xmlData.IndexOf('<'));
            xmlData = xmlData.Substring(0, (xmlData.LastIndexOf('>') + 1));

            return xmlData;
        }
    }

    #endregion MultiGrid Cell Data

    #region MultiGrid 전체 Data

    [XmlRoot("MultiGridDataForSync")]
    public class MultiGridDataForSync : BaseSync
    {
        [XmlElement("GridControlSyncGuid")]
        public Guid GridControlSyncGuid;

        [XmlArray("MultiGridCellDataForSyncList")]
        [XmlArrayItem("MultiGridCellDataForSync", typeof(MultiGridCellDataForSync))]
        public List<MultiGridCellDataForSync> MultiGridCellDataForSyncList;

        public MultiGridDataForSync()
        {
            MultiGridCellDataForSyncList = new List<MultiGridCellDataForSync>();
        }

        public void AddCellData(Guid gridControlSyncGuid, Guid cellSyncGuid, Rect rectForCanvas, MultiGridCameraData visibleCameraInfo)
        {
            MultiGridCellDataForSync data = new MultiGridCellDataForSync(gridControlSyncGuid, cellSyncGuid, rectForCanvas, visibleCameraInfo);
            MultiGridCellDataForSyncList.Add(data);
        }

        public void AddCellData(Guid gridControlSyncGuid, Guid cellSyncGuid, Rect rectForCanvas, MultiGridRdsData visibleRdsInfo)
        {
            MultiGridCellDataForSync data = new MultiGridCellDataForSync(gridControlSyncGuid, cellSyncGuid, rectForCanvas, visibleRdsInfo);
            MultiGridCellDataForSyncList.Add(data);
        }

        public void AddCellData(Guid gridControlSyncGuid, Guid cellSyncGuid, Rect rectForCanvas, MultiGridLayoutData visibleLayoutInfo)
        {
            MultiGridCellDataForSync data = new MultiGridCellDataForSync(gridControlSyncGuid, cellSyncGuid, rectForCanvas, visibleLayoutInfo);
            MultiGridCellDataForSyncList.Add(data);
        }

        public void AddCellData(Guid gridControlSyncGuid, Guid cellSyncGuid, Rect rectForCanvas)
        {
            MultiGridCellDataForSync data = new MultiGridCellDataForSync(gridControlSyncGuid, cellSyncGuid, rectForCanvas);
            MultiGridCellDataForSyncList.Add(data);
        }

        public static MultiGridDataForSync ReadDataFromXML(string xmlData)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(MultiGridDataForSync));
            StringReader stringReader = new StringReader(xmlData);
            XmlTextReader xmlReader = new XmlTextReader(stringReader);

            MultiGridDataForSync data = serializer.Deserialize(xmlReader) as MultiGridDataForSync;

            xmlReader.Close();
            stringReader.Close();

            return data;
        }

        public static MultiGridDataForSync ReadDataFromXMLFile(string fileName)
        {
            string xmlData = string.Empty;

            TextReader streamReader = new StreamReader(fileName);
            xmlData = streamReader.ReadToEnd();

            return MultiGridDataForSync.ReadDataFromXML(xmlData);
        }

        public string SaveDataToXML()
        {
            XmlSerializer serializer = new XmlSerializer(typeof(MultiGridDataForSync));
            MemoryStream memStream = new MemoryStream();

            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Indent = true;
            settings.IndentChars = new string(' ', 4);
            settings.NewLineOnAttributes = false;
            settings.Encoding = Encoding.UTF8;

            XmlWriter xmlWriter = XmlWriter.Create(memStream, settings);
            serializer.Serialize(xmlWriter, this);
            xmlWriter.Close();
            memStream.Close();

            string xmlData = Encoding.UTF8.GetString(memStream.GetBuffer());
            xmlData = xmlData.Substring(xmlData.IndexOf('<'));
            xmlData = xmlData.Substring(0, (xmlData.LastIndexOf('>') + 1));

            return xmlData;
        }
    }

    #endregion MultiGrid 전체 Data
}