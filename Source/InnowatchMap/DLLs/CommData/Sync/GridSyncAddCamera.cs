﻿
namespace Innotive.InnoWatch.DLLs.CommData.Sync
{
    using System;
    using System.Collections.Generic;
    using System.Xml.Serialization;

    [XmlRoot("GridAddCameraData")]
    public class GridAddCameraData : BaseSync
    {
        private List<CameraInfo> _cameraList;

        [XmlAttribute("GridSyncGuid")]
        public Guid GridSyncGuid;
        
        [XmlArray("CameraList")]
        [XmlArrayItem("Camera", typeof(CameraInfo))]
        public List<CameraInfo> CameraList
        {
            get { return _cameraList; }
            set { _cameraList = value; }
        }

        public GridAddCameraData()
        {
            _cameraList = new List<CameraInfo>();
        }

        //public string SaveDataToXML()
        //{
        //    XmlSerializer serializer = new XmlSerializer(typeof(GridAddCameraData));
        //    MemoryStream memStream = new MemoryStream();
        //    XmlTextWriter xmlWriter = new XmlTextWriter(memStream, Encoding.UTF8);
        //    serializer.Serialize(xmlWriter, this);
        //    xmlWriter.Close();
        //    memStream.Close();

        //    string xmlData = Encoding.UTF8.GetString(memStream.GetBuffer());
        //    xmlData = xmlData.Substring(xmlData.IndexOf('<'));
        //    xmlData = xmlData.Substring(0, (xmlData.LastIndexOf('>') + 1));

        //    return xmlData;
        //}

        //public static GridAddCameraData ReadDataFromXML(string xmlData)
        //{
        //    XmlSerializer serializer = null;

        //    serializer = new XmlSerializer(typeof(GridAddCameraData));

        //    StringReader stringReader = new StringReader(xmlData);
        //    XmlTextReader xmlReader = new XmlTextReader(stringReader);

        //    GridAddCameraData gridAddCameraData = serializer.Deserialize(xmlReader) as GridAddCameraData;

        //    xmlReader.Close();
        //    stringReader.Close();

        //    return gridAddCameraData;
        //}
    }
}