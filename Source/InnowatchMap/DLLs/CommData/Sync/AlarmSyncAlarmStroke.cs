﻿
namespace Innotive.InnoWatch.DLLs.CommData.Sync
{
    using System.Xml.Serialization;

    [XmlRoot("AlarmStroke")]
    public class AlarmStroke : BaseSync
    {
        private bool _isShow;

        public AlarmStroke()
        {
            _isShow = false;
        }

        [XmlAttribute("IsShow")]
        public bool IsShow
        {
            get { return _isShow; }
            set { _isShow = value; }
        }

        //public string SaveDataToXML()
        //{
        //    XmlSerializer serializer = new XmlSerializer(typeof(AlarmStroke));
        //    MemoryStream memStream = new MemoryStream();
        //    XmlTextWriter xmlWriter = new XmlTextWriter(memStream, Encoding.UTF8);
        //    serializer.Serialize(xmlWriter, this);
        //    xmlWriter.Close();
        //    memStream.Close();

        //    string xmlData = Encoding.UTF8.GetString(memStream.GetBuffer());
        //    xmlData = xmlData.Substring(xmlData.IndexOf('<'));
        //    xmlData = xmlData.Substring(0, (xmlData.LastIndexOf('>') + 1));

        //    return xmlData;
        //}

        //public static AlarmStroke ReadDataFromXML(string xmlData)
        //{
        //    XmlSerializer serializer = null;

        //    serializer = new XmlSerializer(typeof(AlarmStroke));

        //    StringReader stringReader = new StringReader(xmlData);
        //    XmlTextReader xmlReader = new XmlTextReader(stringReader);

        //    AlarmStroke alarmStroke = serializer.Deserialize(xmlReader) as AlarmStroke;

        //    xmlReader.Close();
        //    stringReader.Close();

        //    return alarmStroke;
        //}
    }   
}
