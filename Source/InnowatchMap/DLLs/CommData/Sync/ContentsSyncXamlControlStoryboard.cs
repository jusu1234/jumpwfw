﻿
namespace Innotive.InnoWatch.DLLs.CommData.Sync
{
    using System;
    using System.Xml.Serialization;

    [XmlRoot("XamlControlStoryboard")]
    public class XamlControlStoryboardData : BaseSync
    {
        [XmlAttribute("TargetLayoutGuid")]
        public Guid TargetLayoutGuid;

        [XmlAttribute("TargetElementGuid")] 
        public Guid TargetElementGuid;

        [XmlAttribute("TargetStoryboardName")]
        public string TargetStoryboardName;

        //public string SaveDataToXML()
        //{
        //    XmlSerializer serializer = new XmlSerializer(typeof(XamlControlStoryboardData));
        //    MemoryStream memStream = new MemoryStream();
        //    XmlTextWriter xmlWriter = new XmlTextWriter(memStream, Encoding.UTF8);
        //    serializer.Serialize(xmlWriter, this);
        //    xmlWriter.Close();
        //    memStream.Close();

        //    string xmlData = Encoding.UTF8.GetString(memStream.GetBuffer());
        //    xmlData = xmlData.Substring(xmlData.IndexOf('<'));
        //    xmlData = xmlData.Substring(0, (xmlData.LastIndexOf('>') + 1));

        //    return xmlData;
        //}

        //public static XamlControlStoryboardData ReadDataFromXML(string xmlData)
        //{
        //    XmlSerializer serializer = null;

        //    serializer = new XmlSerializer(typeof(ZoomInData));

        //    StringReader stringReader = new StringReader(xmlData);
        //    XmlTextReader xmlReader = new XmlTextReader(stringReader);

        //    XamlControlStoryboardData zoomInData = serializer.Deserialize(xmlReader) as XamlControlStoryboardData;

        //    xmlReader.Close();
        //    stringReader.Close();

        //    return zoomInData;
        //}
    }
}