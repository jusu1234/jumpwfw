﻿
namespace Innotive.InnoWatch.DLLs.CommData.Sync
{
    using System;
    using System.Xml.Serialization;

    [XmlRoot("ZoomEnd")]
    public class ZoomEndData : BaseSync
    {
        [XmlAttribute("LayoutGuid")]
        public Guid LayoutGuid;
        
        //public string SaveDataToXML()
        //{
        //    XmlSerializer serializer = new XmlSerializer(typeof(ZoomEndData));
        //    MemoryStream memStream = new MemoryStream();
        //    XmlTextWriter xmlWriter = new XmlTextWriter(memStream, Encoding.UTF8);
        //    serializer.Serialize(xmlWriter, this);
        //    xmlWriter.Close();
        //    memStream.Close();

        //    string xmlData = Encoding.UTF8.GetString(memStream.GetBuffer());
        //    xmlData = xmlData.Substring(xmlData.IndexOf('<'));
        //    xmlData = xmlData.Substring(0, (xmlData.LastIndexOf('>') + 1));

        //    return xmlData;
        //}

        //public static ZoomEndData ReadDataFromXML(string xmlData)
        //{
        //    XmlSerializer serializer = null;

        //    serializer = new XmlSerializer(typeof(ZoomEndData));

        //    StringReader stringReader = new StringReader(xmlData);
        //    XmlTextReader xmlReader = new XmlTextReader(stringReader);

        //    ZoomEndData zoomEndData = serializer.Deserialize(xmlReader) as ZoomEndData;

        //    xmlReader.Close();
        //    stringReader.Close();

        //    return zoomEndData;
        //}
    }
}