﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FolderOpen.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Folder Open Class.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace MsdnMag
{
    using System.Windows.Forms;

    /// <summary>
    /// Folder Open Class.
    /// </summary>
    public class FolderOpen
    {
        /// <summary>
        /// Show Folder Select Dialog.
        /// </summary>
        /// <param name="message">Message of Dialog.</param>
        /// <param name="startFolder">Starting Folder.</param>
        /// <returns>Selected Folder.</returns>
        public static string Show(string message, string startFolder)
        {
            var dlg = new FolderBrowserDialog
            {
                Description = message,
                SelectedPath = startFolder,
            };

            if (dlg.ShowDialog() == DialogResult.OK)
            {
                return dlg.SelectedPath;
            }

            return string.Empty;
        }
    }
}
