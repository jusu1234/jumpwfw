﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Innotive.InnoWatch.Commons.Models;

namespace Innotive.InnoWatch.DLLs.TreeViewControls
{
    /// <summary>
    /// StackedTreeView Control
    /// </summary>
    public class StackedTreeView : TreeView
    {
        /// <summary>
        /// 클래스 정적 생성자
        /// </summary>
        static StackedTreeView()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(StackedTreeView), new FrameworkPropertyMetadata(typeof(TreeView)));
        }

        #region 사용자 정의 트리 아이템 사용

        protected override DependencyObject GetContainerForItemOverride()
        {
            return new StackedTreeViewItem();
        }

        protected override bool IsItemItsOwnContainerOverride(object item)
        {
            return item is StackedTreeViewItem;
        }

        #endregion // 사용자 정의 트리 아이템 사용

        /// <summary>
        /// 클래스 생성자
        /// </summary>
        public StackedTreeView()
        {
            this.ItemContainerGenerator.StatusChanged += new EventHandler(ItemContainerGenerator_StatusChanged);
        }

        /// <summary>
        /// 클래스 소멸자
        /// </summary>
        ~StackedTreeView()
        {
            this.ItemContainerGenerator.StatusChanged -= new EventHandler(ItemContainerGenerator_StatusChanged);
        }

        /// <summary>
        /// 아이템 생성기 상태 변환 이벤트 핸들러
        /// </summary>
        protected void ItemContainerGenerator_StatusChanged(object sender, EventArgs e)
        {
            ItemContainerGenerator generatedItemContainer = sender as ItemContainerGenerator;
            if (generatedItemContainer.Status == System.Windows.Controls.Primitives.GeneratorStatus.ContainersGenerated)
            {
                int i = 0;
                StackedTreeViewItem currentTreeViewItem = (StackedTreeViewItem)generatedItemContainer.ContainerFromIndex(i);
                while (currentTreeViewItem != null)
                {
                    currentTreeViewItem.ItemContainerGenerator.StatusChanged += new EventHandler(ItemContainerGenerator_StatusChanged);
                    //currentTreeViewItem.ApplyItemDepth(0);

                    if (currentTreeViewItem.Header is IHasDepth)
                    {
                        currentTreeViewItem.ApplyItemDepth((currentTreeViewItem.Header as IHasDepth).Depth);
                    }
                    currentTreeViewItem.ApplyItemContanerSource(this.ItemContainerStyle);

                    currentTreeViewItem = (StackedTreeViewItem)generatedItemContainer.ContainerFromIndex(++i);
                }
            }
        }
    }
}
