﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Innotive.InnoWatch.DLLs.ActionEffects
{
    /// <summary>
    /// Action Effects Enums
    /// </summary>
    public enum ActionEffects 
    { 
        None, 

        FadeOutIn,

        ParrallelFadeOutIn,

        ShrinkTransition,

        BlindsTransition,

        CloudRevealTransition,

        RandomCircleRevealTransition,

        FadeTransition,

        WaveTransition,

        RadialWiggleTransition,

        BloodTransition,

        CircleStretchTransition,

        DisolveTransition,

        DropFadeTransition,

        RotateCrumbleTransition,

        WaterTransition,

        CrumbleTransition,

        RadialBlurTransition,

        CircularBlurTransition,

        PixelateTransition,

        PixelateInTransition,

        PixelateOutTransition,

        SwirlGridTransition4,

        SwirlGridTransition16,

        SmoothSwirlGridTransition4,

        SmoothSwirlGridTransition16,

        SmoothSwirlGridTransitionM8,

        SmoothSwirlGridTransitionM6,

        MostBrightTransition,

        LeastBrightTransition,

        SaturateTransition,

        BandedSwirlTransition1,

        BandedSwirlTransition2,

        BandedSwirlTransition3,

        CircleRevealTransition1,

        CircleRevealTransition2,

        CircleRevealTransition3,

        LineRevealTransition1,

        LineRevealTransition2,

        LineRevealTransition3,

        LineRevealTransition4,

        LineRevealTransition5,

        LineRevealTransition6,

        LineRevealTransition7,

        LineRevealTransition8,

        RippleTransition,

        SlideInTransition1,

        SlideInTransition2,

        SlideInTransition3,

        SlideInTransition4,

        SwirlTransition1,

        SwirlTransition2,
    };
}
