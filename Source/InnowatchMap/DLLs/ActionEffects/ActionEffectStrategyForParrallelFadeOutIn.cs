﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Media.Animation;

namespace Innotive.InnoWatch.DLLs.ActionEffects
{
    public class ActionEffectStrategyForParrallelFadeOutIn : IActionEffectStrategy
    {
        private FrameworkElement _newElement = null;
        private FrameworkElement _oldElement = null;
        private double _startTime = 0.0;
        private double _duration = 0.0;

        public ActionEffectStrategyForParrallelFadeOutIn(FrameworkElement newElement, FrameworkElement oldElement, double startTime, double duration)
        {
            this._newElement = newElement;
            this._oldElement = oldElement;
            this._startTime = startTime;
            this._duration = duration;
        }

        #region IActionEffectStrategy Members

        
        private Action _endAction = null;
        private DoubleAnimationUsingKeyFrames _animationForFadeIn = null;
        private DoubleAnimationUsingKeyFrames _animationForFadeOut = null;

        public void Begin(Action endAction)
        {
            LinearDoubleKeyFrame keyFrameForFadeIn = new LinearDoubleKeyFrame();
            keyFrameForFadeIn.Value = 1.0;
            keyFrameForFadeIn.KeyTime = KeyTime.FromTimeSpan(TimeSpan.FromSeconds(this._duration));

            _animationForFadeIn = new DoubleAnimationUsingKeyFrames();
            _animationForFadeIn.BeginTime = TimeSpan.FromSeconds(this._startTime);
            _animationForFadeIn.KeyFrames.Add(keyFrameForFadeIn);
            _animationForFadeIn.FillBehavior = FillBehavior.HoldEnd;
            
            LinearDoubleKeyFrame keyFrameForFadeOut = new LinearDoubleKeyFrame();
            keyFrameForFadeOut.Value = 0.0;
            keyFrameForFadeOut.KeyTime = KeyTime.FromTimeSpan(TimeSpan.FromSeconds(this._duration));

            _animationForFadeOut = new DoubleAnimationUsingKeyFrames();
            _animationForFadeOut.BeginTime = TimeSpan.FromSeconds(this._startTime);
            _animationForFadeOut.KeyFrames.Add(keyFrameForFadeOut);
            _animationForFadeOut.FillBehavior = FillBehavior.HoldEnd;
                        

            this._endAction = endAction;

            if (_newElement != null)
            {
                _newElement.Opacity = 0.0;
            }
            if (_oldElement != null)
            {
                _oldElement.Opacity = 1.0;
            }

            if (_oldElement != null)
            {
                _animationForFadeOut.Completed += new EventHandler(AnimationForFadeOut_Completed);
                _oldElement.BeginAnimation(FrameworkElement.OpacityProperty, _animationForFadeOut);
            }

            if (_newElement != null)
            {
                _animationForFadeIn.Completed += new EventHandler(AnimationForFadeIn_Completed);
                _newElement.BeginAnimation(FrameworkElement.OpacityProperty, _animationForFadeIn);
            }
            
        }

        private void AnimationForFadeOut_Completed(object sender, EventArgs e)
        {
            _newElement = null;
            _oldElement = null;

            _animationForFadeOut.Completed -= new EventHandler(AnimationForFadeOut_Completed);
            _animationForFadeOut = null;

            if (this._endAction != null)
            {
                this._endAction.Invoke();
            }
        }

        private void AnimationForFadeIn_Completed(object sender, EventArgs e)
        {
            _newElement = null;
            _oldElement = null;

            _animationForFadeIn.Completed -= new EventHandler(AnimationForFadeIn_Completed);
            _animationForFadeIn = null;

            if (this._endAction != null)
            {
                this._endAction.Invoke();
            }
        }

        #endregion
    }
}
