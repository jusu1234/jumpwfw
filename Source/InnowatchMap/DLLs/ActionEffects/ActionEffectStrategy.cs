﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace Innotive.InnoWatch.DLLs.ActionEffects
{
    /// <summary>
    /// Action Effect Strategy Interface
    /// </summary>
    interface IActionEffectStrategy
    {
        void Begin(Action endAction);
    }

    /// <summary>
    /// Action Effect Strategy Manager Class
    /// </summary>
    public class ActionEffectStrategy
    {
        private IActionEffectStrategy _interface = null;

        public ActionEffectStrategy(string effectName, FrameworkElement newElement, FrameworkElement oldElement, double startTime, double duration)
        {
            if (string.IsNullOrEmpty(effectName))
            {
                effectName = "None";
            }

            if (string.Compare(effectName, ActionEffects.None.ToString(), true) == 0)
            {
                _interface = new ActionEffectStrategyForNone(newElement, oldElement, startTime, duration);
            }

            else if (string.Compare(effectName, ActionEffects.FadeOutIn.ToString(), true) == 0)
            {
                _interface = new ActionEffectStrategyForFadeOutIn(newElement, oldElement, startTime, duration);
            }

            else if (string.Compare(effectName, ActionEffects.ParrallelFadeOutIn.ToString(), true) == 0)
            {
                _interface = new ActionEffectStrategyForParrallelFadeOutIn(newElement, oldElement, startTime, duration);
            }

            else if (string.Compare(effectName, ActionEffects.ShrinkTransition.ToString(), true) == 0)
            {
                _interface = new ActionEffectStrategyForShrinkTransition(newElement, oldElement, startTime, duration);
            }

            else if (string.Compare(effectName, ActionEffects.BlindsTransition.ToString(), true) == 0)
            {
                _interface = new ActionEffectStrategyForBlindsTransition(newElement, oldElement, startTime, duration);
            }

            else if (string.Compare(effectName, ActionEffects.CloudRevealTransition.ToString(), true) == 0)
            {
                _interface = new ActionEffectStrategyForCloudRevealTransition(newElement, oldElement, startTime, duration);
            }

            else if (string.Compare(effectName, ActionEffects.RandomCircleRevealTransition.ToString(), true) == 0)
            {
                _interface = new ActionEffectStrategyForRandomCircleRevealTransition(newElement, oldElement, startTime, duration);
            }

            else if (string.Compare(effectName, ActionEffects.FadeTransition.ToString(), true) == 0)
            {
                _interface = new ActionEffectStrategyForFadeTransition(newElement, oldElement, startTime, duration);
            }

            else if (string.Compare(effectName, ActionEffects.WaveTransition.ToString(), true) == 0)
            {
                _interface = new ActionEffectStrategyForWaveTransition(newElement, oldElement, startTime, duration);
            }

            else if (string.Compare(effectName, ActionEffects.RadialWiggleTransition.ToString(), true) == 0)
            {
                _interface = new ActionEffectStrategyForRadialWiggleTransition(newElement, oldElement, startTime, duration);
            }

            else if (string.Compare(effectName, ActionEffects.BloodTransition.ToString(), true) == 0)
            {
                _interface = new ActionEffectStrategyForBloodTransition(newElement, oldElement, startTime, duration);
            }

            else if (string.Compare(effectName, ActionEffects.CircleStretchTransition.ToString(), true) == 0)
            {
                _interface = new ActionEffectStrategyForCircleStretchTransition(newElement, oldElement, startTime, duration);
            }

            else if (string.Compare(effectName, ActionEffects.DisolveTransition.ToString(), true) == 0)
            {
                _interface = new ActionEffectStrategyForDisolveTransition(newElement, oldElement, startTime, duration);
            }

            else if (string.Compare(effectName, ActionEffects.DropFadeTransition.ToString(), true) == 0)
            {
                _interface = new ActionEffectStrategyForDropFadeTransition(newElement, oldElement, startTime, duration);
            }

            else if (string.Compare(effectName, ActionEffects.SwirlGridTransition4.ToString(), true) == 0)
            {
                _interface = new ActionEffectStrategyForSwirlGridTransition(newElement, oldElement, startTime, duration, Math.PI * 4);
            }

            else if (string.Compare(effectName, ActionEffects.SwirlGridTransition16.ToString(), true) == 0)
            {
                _interface = new ActionEffectStrategyForSwirlGridTransition(newElement, oldElement, startTime, duration, Math.PI * 16);
            }

            else if (string.Compare(effectName, ActionEffects.SmoothSwirlGridTransition4.ToString(), true) == 0)
            {
                _interface = new ActionEffectStrategyForSmoothSwirlGridTransition(newElement, oldElement, startTime, duration, Math.PI * 4);
            }

            else if (string.Compare(effectName, ActionEffects.SmoothSwirlGridTransition16.ToString(), true) == 0)
            {
                _interface = new ActionEffectStrategyForSmoothSwirlGridTransition(newElement, oldElement, startTime, duration, Math.PI * 16);
            }

            else if (string.Compare(effectName, ActionEffects.SmoothSwirlGridTransitionM8.ToString(), true) == 0)
            {
                _interface = new ActionEffectStrategyForSmoothSwirlGridTransition(newElement, oldElement, startTime, duration, -Math.PI * 8);
            }

            else if (string.Compare(effectName, ActionEffects.SmoothSwirlGridTransitionM6.ToString(), true) == 0)
            {
                _interface = new ActionEffectStrategyForSmoothSwirlGridTransition(newElement, oldElement, startTime, duration, -Math.PI * 6);
            }

            else if (string.Compare(effectName, ActionEffects.MostBrightTransition.ToString(), true) == 0)
            {
                _interface = new ActionEffectStrategyForMostBrightTransition(newElement, oldElement, startTime, duration);
            }

            else if (string.Compare(effectName, ActionEffects.LeastBrightTransition.ToString(), true) == 0)
            {
                _interface = new ActionEffectStrategyForLeastBrightTransition(newElement, oldElement, startTime, duration);
            }

            else if (string.Compare(effectName, ActionEffects.SaturateTransition.ToString(), true) == 0)
            {
                _interface = new ActionEffectStrategyForSaturateTransition(newElement, oldElement, startTime, duration);
            }

            else if (string.Compare(effectName, ActionEffects.BandedSwirlTransition1.ToString(), true) == 0)
            {
                _interface = new ActionEffectStrategyForBandedSwirlTransition(newElement, oldElement, startTime, duration, Math.PI / 5.0, 50.0);
            }

            else if (string.Compare(effectName, ActionEffects.BandedSwirlTransition2.ToString(), true) == 0)
            {
                _interface = new ActionEffectStrategyForBandedSwirlTransition(newElement, oldElement, startTime, duration, Math.PI, 10.0);
            }

            else if (string.Compare(effectName, ActionEffects.BandedSwirlTransition3.ToString(), true) == 0)
            {
                _interface = new ActionEffectStrategyForBandedSwirlTransition(newElement, oldElement, startTime, duration, -Math.PI, 10.0);
            }

            else if (string.Compare(effectName, ActionEffects.CircleRevealTransition1.ToString(), true) == 0)
            {
                _interface = new ActionEffectStrategyForCircleRevealTransition(newElement, oldElement, startTime, duration, 0.0);
            }

            else if (string.Compare(effectName, ActionEffects.CircleRevealTransition2.ToString(), true) == 0)
            {
                _interface = new ActionEffectStrategyForCircleRevealTransition(newElement, oldElement, startTime, duration, 0.1);
            }

            else if (string.Compare(effectName, ActionEffects.CircleRevealTransition3.ToString(), true) == 0)
            {
                _interface = new ActionEffectStrategyForCircleRevealTransition(newElement, oldElement, startTime, duration, 0.5);
            }

            else if (string.Compare(effectName, ActionEffects.LineRevealTransition1.ToString(), true) == 0)
            {
                _interface = new ActionEffectStrategyForLineRevealTransition(newElement, oldElement, startTime, duration, new Vector(-0.2, -0.2), new Vector(1, 0), new Vector(1.4, 0), 0.2);
            }

            else if (string.Compare(effectName, ActionEffects.LineRevealTransition2.ToString(), true) == 0)
            {
                _interface = new ActionEffectStrategyForLineRevealTransition(newElement, oldElement, startTime, duration, new Vector(1.2, -0.2), new Vector(-1, 0), new Vector(-1.4, 0), 0.2);
            }

            else if (string.Compare(effectName, ActionEffects.LineRevealTransition3.ToString(), true) == 0)
            {
                _interface = new ActionEffectStrategyForLineRevealTransition(newElement, oldElement, startTime, duration, new Vector(-0.2, 0.2), new Vector(0, 1), new Vector(0, 1.4), 0.2);
            }

            else if (string.Compare(effectName, ActionEffects.LineRevealTransition4.ToString(), true) == 0)
            {
                _interface = new ActionEffectStrategyForLineRevealTransition(newElement, oldElement, startTime, duration, new Vector(-0.2, 1.2), new Vector(0, -1), new Vector(0, -1.4), 0.2);
            }

            else if (string.Compare(effectName, ActionEffects.LineRevealTransition5.ToString(), true) == 0)
            {
                _interface = new ActionEffectStrategyForLineRevealTransition(newElement, oldElement, startTime, duration, new Vector(-0.2, -0.2), new Vector(1, 1), new Vector(1.4, 1.4), 0.2);
            }

            else if (string.Compare(effectName, ActionEffects.LineRevealTransition6.ToString(), true) == 0)
            {
                _interface = new ActionEffectStrategyForLineRevealTransition(newElement, oldElement, startTime, duration, new Vector(1.2, 1.2), new Vector(-1, -1), new Vector(-1.4, -1.4), 0.2);
            }

            else if (string.Compare(effectName, ActionEffects.LineRevealTransition7.ToString(), true) == 0)
            {
                _interface = new ActionEffectStrategyForLineRevealTransition(newElement, oldElement, startTime, duration, new Vector(1.2, -0.2), new Vector(-1, 1), new Vector(-1.4, 1.4), 0.2);
            }

            else if (string.Compare(effectName, ActionEffects.LineRevealTransition8.ToString(), true) == 0)
            {
                _interface = new ActionEffectStrategyForLineRevealTransition(newElement, oldElement, startTime, duration, new Vector(-0.2, 1.2), new Vector(1, -1), new Vector(1.4, -1.4), 0.2);
            }

            else if (string.Compare(effectName, ActionEffects.RippleTransition.ToString(), true) == 0)
            {
                _interface = new ActionEffectStrategyForRippleTransition(newElement, oldElement, startTime, duration);
            }

            else if (string.Compare(effectName, ActionEffects.SlideInTransition1.ToString(), true) == 0)
            {
                _interface = new ActionEffectStrategyForSlideInTransition(newElement, oldElement, startTime, duration, new Vector(1, 0));
            }

            else if (string.Compare(effectName, ActionEffects.SlideInTransition2.ToString(), true) == 0)
            {
                _interface = new ActionEffectStrategyForSlideInTransition(newElement, oldElement, startTime, duration, new Vector(0, 1));
            }

            else if (string.Compare(effectName, ActionEffects.SlideInTransition3.ToString(), true) == 0)
            {
                _interface = new ActionEffectStrategyForSlideInTransition(newElement, oldElement, startTime, duration, new Vector(-1, 0));
            }

            else if (string.Compare(effectName, ActionEffects.SlideInTransition4.ToString(), true) == 0)
            {
                _interface = new ActionEffectStrategyForSlideInTransition(newElement, oldElement, startTime, duration, new Vector(0, -1));
            }

            else if (string.Compare(effectName, ActionEffects.SwirlTransition1.ToString(), true) == 0)
            {
                _interface = new ActionEffectStrategyForSwirlTransition(newElement, oldElement, startTime, duration, Math.PI * 4);
            }

            else if (string.Compare(effectName, ActionEffects.SwirlTransition2.ToString(), true) == 0)
            {
                _interface = new ActionEffectStrategyForSwirlTransition(newElement, oldElement, startTime, duration, -Math.PI * 4);
            }
        }

        public void Begin(Action endAction)
        {
            if (this._interface != null)
            {
                this._interface.Begin(endAction);
            }
        }
    }
}
