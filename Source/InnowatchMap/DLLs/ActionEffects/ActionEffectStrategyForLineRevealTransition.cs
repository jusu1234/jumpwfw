﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media.Animation;
using System.Windows;
using System.Windows.Media;

namespace Innotive.InnoWatch.DLLs.ActionEffects
{
    public class ActionEffectStrategyForLineRevealTransition : ActionEffectBaseStrategyForTransition
    {
        private Vector _origin = new Vector(-0.2, -0.2);
        private Vector _normal = new Vector(1, 0);
        private Vector _offset = new Vector(1.4, 0);
        private double _fuzzyAmount = 0.2;

        public ActionEffectStrategyForLineRevealTransition(FrameworkElement newElement, FrameworkElement oldElement, double startTime, double duration, Vector origin, Vector normal, Vector offset, double fuzzyAmount)
            : base(newElement, oldElement, startTime, duration)
        {
            this._origin = origin;
            this._normal = normal;
            this._offset = offset;
            this._fuzzyAmount = fuzzyAmount;
        }

        public override void Begin(Action endAction)
        {
            Effect = new TransitionEffects.LineRevealTransitionEffect() { LineOrigin = this._origin, LineNormal = this._normal, LineOffset = this._offset, FuzzyAmount = this._fuzzyAmount };

            base.Begin(endAction);
        }
    }
}
