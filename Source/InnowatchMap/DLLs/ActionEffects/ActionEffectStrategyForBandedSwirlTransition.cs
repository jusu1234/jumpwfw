﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media.Animation;
using System.Windows;
using System.Windows.Media;

namespace Innotive.InnoWatch.DLLs.ActionEffects
{
    public class ActionEffectStrategyForBandedSwirlTransition : ActionEffectBaseStrategyForTransition
    {
        private double _param1 = Math.PI / 5.0;
        private double _param2 = 50.0;

        public ActionEffectStrategyForBandedSwirlTransition(FrameworkElement newElement, FrameworkElement oldElement, double startTime, double duration, double param1, double param2)
            : base(newElement, oldElement, startTime, duration)
        {
            this._param1 = param1;
            this._param2 = param2;
        }

        public override void Begin(Action endAction)
        {            
            Effect = new TransitionEffects.BandedSwirlTransitionEffect(this._param1, this._param2);

            base.Begin(endAction);
        }
    }
}
