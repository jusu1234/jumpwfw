﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AlarmBorderControl.xaml.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Interaction logic for UserControl1.xaml
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.DLLs.AlarmBorderControls
{
    using System;
    using System.Windows;
    using System.Windows.Media;
    using System.Windows.Media.Animation;

    /// <summary>
    /// Interaction logic for UserControl1.xaml .
    /// </summary>
    public partial class AlarmBorderControl
    {
        private readonly Storyboard blinkAnimationStoryboard;
        private BorderActionType borderAction;
        private string borderColor;

        /// <summary>
        /// Initializes a new instance of the <see cref="AlarmBorderControl"/> class.
        /// </summary>
        public AlarmBorderControl()
        {
            InitializeComponent();
            this.blinkAnimationStoryboard = new Storyboard();

            // RegisterAnimation
            NameScope.SetNameScope(this, new NameScope());
            this.RegisterName("BlinkAnimation", xRectangle);

            var opacityKeyFrame = new DoubleAnimationUsingKeyFrames();

            var startKeyFrame = new SplineDoubleKeyFrame
            {
                KeyTime = KeyTime.FromTimeSpan(new TimeSpan(0, 0, 0)),
                Value = 0
            };

            opacityKeyFrame.KeyFrames.Add(startKeyFrame);

            var endKeyFrame = new SplineDoubleKeyFrame
            {
                KeySpline = new KeySpline(0.5, 0, 1, 1),
                KeyTime = KeyTime.FromTimeSpan(new TimeSpan(0, 0, 0, 0, 800)),
                Value = 1
            };

            opacityKeyFrame.KeyFrames.Add(endKeyFrame);

            Storyboard.SetTargetName(opacityKeyFrame, "BlinkAnimation");
            Storyboard.SetTargetProperty(opacityKeyFrame, new PropertyPath(OpacityProperty));

            this.blinkAnimationStoryboard.Children.Add(opacityKeyFrame);
            this.blinkAnimationStoryboard.RepeatBehavior = RepeatBehavior.Forever;
        }

        /// <summary>
        /// Border action type.
        /// </summary>
        public enum BorderActionType : byte
        {
            /// <summary>
            /// Alarm Status.
            /// </summary>
            Enable = 0,

            /// <summary>
            /// Alert Status.
            /// </summary>
            Disable = 1,

            /// <summary>
            /// Unset Status.
            /// </summary>
            Blink = 2,
        }

        /// <summary>
        /// Gets or sets a value indicating whether IsBorderBlink.
        /// </summary>
        public BorderActionType BorderAction
        {
            get
            {
                return this.borderAction;
            }

            set
            {
                this.borderAction = value;

                switch (this.borderAction)
                {
                    case BorderActionType.Enable:
                        this.Visibility = Visibility.Visible;
                        this.StopBlinkAnimation();
                        break;
                    case BorderActionType.Disable:
                        this.Visibility = Visibility.Hidden;
                        this.StopBlinkAnimation();
                        break;
                    case BorderActionType.Blink:
                        this.Visibility = Visibility.Visible;
                        this.BeginBlinkAnimation();
                        break;
                }
            }
        }

        /// <summary>
        /// Gets or sets BorderColor.
        /// </summary>
        public string BorderColor
        {
            get
            {
                return this.borderColor;
            }

            set
            {
                this.borderColor = value;

                //by blackRoot : borderColor가 empty일 경우(ex.AlarmEnd) 아래 BrushConverter에서 exception이 발생함 !!
                if (string.IsNullOrEmpty(this.borderColor))
                {
                    this.xRectangle.Stroke = null;
                    return;
                }

                var conv = new BrushConverter();
                var brush = conv.ConvertFromString(this.borderColor) as SolidColorBrush;
                this.xRectangle.Stroke = brush;
            }
        }

        /// <summary>
        /// Blink Action Start.
        /// </summary>
        public void BeginBlinkAnimation()
        {
            this.blinkAnimationStoryboard.Begin(xRectangle, true);
        }

        /// <summary>
        /// Border Action Stop.
        /// </summary>
        public void StopBlinkAnimation()
        {
            this.blinkAnimationStoryboard.Stop(xRectangle);
        }
    }
}
