﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="BookmarkControl.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Defines the BookmarkControl type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.DLLs.BookmarkControls
{
    using System;
    using System.Windows;
    using System.Windows.Input;
    using System.Windows.Media;
    using Innotive.InnoWatch.Commons.BaseControls;

    /// <summary>
    /// 선택된 컨테이너의 인텍스를 전달하는 메세지 아규먼트.
    /// </summary>
    public class BookmarkControlEventArgs : EventArgs
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="BookmarkControlEventArgs"/> class.
        /// </summary>
        /// <param name="propertyName">
        /// The property name.
        /// </param>
        public BookmarkControlEventArgs(string propertyName)
        {
            this.PropertyName = propertyName;
        }

        /// <summary>
        /// Gets SelectedIndex.
        /// </summary>
        public string PropertyName { get; private set; }
    }

    /// <summary>
    /// 북마크를 시각적으로 보여주기위한 오브젝트.
    /// </summary>
    public class BookmarkControl : BaseShapeControl
    {
        private static readonly Brush NULL_BRUSH = Brushes.Transparent;

        private Brush _drawBrush = NULL_BRUSH;
        private Pen _drawPen = new Pen();

        /// <summary>
        /// Initializes a new instance of the <see cref="BookmarkControl"/> class.
        /// </summary>
        public BookmarkControl()
        {
            this.NormalFill = System.Windows.Media.Brushes.Transparent;
            this.MouseOverFill = new SolidColorBrush(Color.FromArgb(80, 255, 255, 255));
            this.StrokeThickness = 1;
            this.NormalStroke = new SolidColorBrush(Color.FromRgb( 255, 90, 0));
            this.MouseOverStroke = new SolidColorBrush(Color.FromRgb(255, 90, 0));
            this.MouseDownStroke = new SolidColorBrush(Color.FromRgb(255, 90, 0));

            this.Visibility = Visibility.Visible;
        }

        /// <summary>
        /// 북마크 이벤트 .
        /// </summary>
        /// <param name="obj">
        /// The obj.
        /// </param>
        /// <param name="bookmarkControlEventArgs">
        /// The bookmark control event.
        /// </param>
        public delegate void EventHandler(object obj, BookmarkControlEventArgs bookmarkControlEventArgs);

        /// <summary>
        /// 북마크 이벤트 핸들러.
        /// </summary>
        public event EventHandler BookmarkControlPropertyChanged;

        /// <summary>
        /// Gets or sets BookmarkGuid.
        /// </summary>
        public Guid BookmarkGuid { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether IsHome.
        /// </summary>
        public bool IsHome { get; set; }

        /// <summary>
        /// Gets or sets BookmarkSizeRatio.
        /// 북마크 컨트롤의 가로와 세로의 비율. (비율 = 세로 / 가로)
        /// </summary>
        public double BookmarkSizeRatio { get; set; }

        /// <summary>
        /// 속성 변경 이벤트.
        /// </summary>
        /// <param name="e">
        /// The e.
        /// </param>
        protected override void OnPropertyChanged(DependencyPropertyChangedEventArgs e)
        {
            base.OnPropertyChanged(e);

            if (this.BookmarkControlPropertyChanged != null)
            {
                if (string.Compare(e.Property.Name, "Name", true) == 0)
                {
                    this.BookmarkControlPropertyChanged(this, new BookmarkControlEventArgs(e.Property.Name));  
                }
                else if (string.Compare(e.Property.Name, "Description", true) == 0)
                {
                    this.BookmarkControlPropertyChanged(this, new BookmarkControlEventArgs("Description"));
                }
                else if (string.Compare(e.Property.Name, "Left", true) == 0)
                {
                    this.BookmarkControlPropertyChanged(this, new BookmarkControlEventArgs("Left"));
                }
                else if (string.Compare(e.Property.Name, "Top", true) == 0)
                {
                    this.BookmarkControlPropertyChanged(this, new BookmarkControlEventArgs("Top"));
                }
                else if (string.Compare(e.Property.Name, "Width", true) == 0)
                {
                    this.BookmarkControlPropertyChanged(this, new BookmarkControlEventArgs("Width"));
                }
                else if (string.Compare(e.Property.Name, "Height", true) == 0)
                {
                    this.BookmarkControlPropertyChanged(this, new BookmarkControlEventArgs("Height"));
                }
            }
        }

        private static void OnChangedForDrawing(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            var self = o as BookmarkControl;

            if (self != null)
            {
                self.InvalidateVisual();
            }
        }

        protected override void OnMouseEnter(MouseEventArgs e)
        {
            base.OnMouseEnter(e);

            this.InvalidateVisual();
        }

        protected override void OnMouseLeave(MouseEventArgs e)
        {
            base.OnMouseLeave(e);

            this.InvalidateVisual();
        }

        protected override void OnMouseDown(MouseButtonEventArgs e)
        {
            base.OnMouseDown(e);

            this.InvalidateVisual();
        }

        protected override void OnMouseUp(MouseButtonEventArgs e)
        {
            base.OnMouseUp(e);

            this.InvalidateVisual();
        }

        protected override void OnRender(DrawingContext drawingContext)
        {
            base.OnRender(drawingContext);

            if (this.IsFill)
            {
                this._drawBrush = this.IsMouseOver ? this.MouseOverFill : this.NormalFill;
            }
            else
            {
                this._drawBrush = NULL_BRUSH;
            }

            if (this.IsStroke)
            {
                this._drawPen.Brush = this.IsMouseOver ? this.MouseOverStroke : this.NormalStroke;
            }
            else
            {
                this._drawPen.Brush = NULL_BRUSH;
            }


            this._drawPen.Thickness = this.StrokeThickness;

            var rect = new Rect(0, 0, this.Width, this.Height);

            if (this.IsStroke)
            {
                rect.Inflate(-this.StrokeThickness / 2, -this.StrokeThickness / 2);
            }

            drawingContext.DrawRoundedRectangle(this._drawBrush, this._drawPen, rect, 0, 0);
        }

        //public override FrameworkElement GetIcon()
        //{
        //    return new IconUserControl();
        //}
    }
}
