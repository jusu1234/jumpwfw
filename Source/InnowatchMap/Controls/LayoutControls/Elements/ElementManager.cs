﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright company="Innotive Inc. Korea" file="ElementManager.cs">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Normal : XAML 에 기록되는 Element
//   Foundation : Zoom 기능등을 구현하기 위한 뼈대 Element
//   Unknown : 초기화 되지 않은 Element.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.DLLs.LayoutControls.Elements
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Windows;
    using System.Windows.Controls;
    using Innotive.InnoWatch.Commons.BaseControls;
    using Innotive.InnoWatch.Commons.Configs;
    using Innotive.InnoWatch.Commons.Utils;
    using Innotive.InnoWatch.DLLs.BookmarkControls;
    using Innotive.InnoWatch.DLLs.CameraControls;

    /// <summary>
    /// The element manager.
    /// </summary>
    internal class ElementManager
    {
        /// <summary>
        /// Layout에 올라와 있는 비쥬얼엘리먼트들의 목록을 가지고 있습니다.
        /// </summary>
        private readonly List<FrameworkElement> visualElementList = new List<FrameworkElement>();

        /// <summary>
        /// Layout 컨트롤.
        /// </summary>
        private readonly LayoutControl layoutControl;

        /// <summary>
        /// Initializes a new instance of the <see cref="ElementManager"/> class.
        /// </summary>
        /// <param name="layoutControl">
        /// The layout control.
        /// </param>
        public ElementManager(LayoutControl layoutControl)
        {
            this.layoutControl = layoutControl;
        }
        
        /// <summary>
        /// 엘리먼트가 현재 어느 위치에 있는지 지정한다.
        /// OutsideCanvas: 현재 OutsideCanvas에 있는 엘리먼트
        /// InsideCanvas: 현재 InsideCanvas에 있는 엘리먼트
        /// UnknownPosition: 아직 Layout에 올라오지 않았거나 LayoutCanvas에 있는 엘리먼트.
        /// </summary>
        public enum AllocatedPosition
        {
            /// <summary>
            /// The outside canvas.
            /// </summary>
            OutsideCanvas,

            /// <summary>
            /// The inside canvas.
            /// </summary>
            InsideCanvas,

            /// <summary>
            /// The unknown position.
            /// </summary>
            UnknownPosition
        }



        /// <summary>
        /// Gets Elements.
        /// </summary>
        public List<FrameworkElement> Elements
        {
            get { return GetElements(); }
        }

        /// <summary>
        /// Gets LockedElements.
        /// </summary>
        public UIElementCollection LockedElements
        {
            get { return layoutControl.OutsideCanvas.Children; }
        }

        /// <summary>
        /// Gets UnLockedElements.
        /// </summary>
        public UIElementCollection UnLockedElements
        {
            get { return layoutControl.InsideCanvas.Children; }
        }

        /// <summary>
        /// 레이아웃에 엘리먼트를 추가하는 함수입니다.
        /// </summary>
        /// <param name="element">
        /// The element.
        /// </param>
        public void AddElement(FrameworkElement element)
        {
            AllocationCanvas(element);
            AddControlElementList(element);
            AssignElementEventHandler(element);
            
            // mhkim : 엘리먼트 추가시  레이아웃 갱신
            layoutControl.UpdateViewArea(Rect.Empty, null);
        }

        public int GetObjectCount()
        {
            if (layoutControl == null)
            {
                return 0;
            }

            int counts = 0;

            foreach (FrameworkElement element in layoutControl.OutsideCanvas.Children)
            {
                if (IsFoundationElement(element) == false)
                {
                    if (element is LayoutControl)
                    {
                        counts += (element as LayoutControl).GetObjectCount();
                    }
                    else if (element is BaseControl)
                    {
                        counts += (element as BaseControl).GetObjectCount();
                    }
                }
            }

            foreach (FrameworkElement element in layoutControl.InsideCanvas.Children)
            {
                if (IsFoundationElement(element) == false)
                {
                    if (element is LayoutControl)
                    {
                        counts += (element as LayoutControl).GetObjectCount();
                    }
                    else if (element is BaseControl)
                    {
                        counts += (element as BaseControl).GetObjectCount();
                    }
                }
            }

            return counts;
        }

        public void ChildrenSetFocused(bool focused)
        {
            if (layoutControl == null)
            {
                return;
            }

            foreach (FrameworkElement element in layoutControl.OutsideCanvas.Children)
            {
                if (IsFoundationElement(element) == false)
                {
                    if (element is LayoutControl)
                    {
                        (element as LayoutControl).ChildrenSetFocused(focused);
                    }
                }
            }

            foreach (FrameworkElement element in layoutControl.InsideCanvas.Children)
            {
                if (IsFoundationElement(element) == false)
                {
                    if (element is LayoutControl)
                    {
                        (element as LayoutControl).ChildrenSetFocused(focused);
                    }
                }
            }
        }

        public void SetProcess(ProcessMode processMode)
        {
            if (layoutControl == null)
            {
                return;
            }

            foreach (FrameworkElement element in layoutControl.OutsideCanvas.Children)
            {
                if (IsFoundationElement(element) == false)
                {
                    if (element is LayoutControl)
                    {
                        (element as LayoutControl).SetProcess(processMode);
                    }
                }
            }

            foreach (FrameworkElement element in layoutControl.InsideCanvas.Children)
            {
                if (IsFoundationElement(element) == false)
                {
                    if (element is LayoutControl)
                    {
                        (element as LayoutControl).SetProcess(processMode);
                    }
                }
            }
        }

        /// <summary>
        /// LayoutControl 에 바로 붙어 있는 자식 Element 들을 종류에 따라 하위 Canvas 에 재배열한다.
        /// </summary>
        public void AdjustElementLocation()
        {
            var tempElement = new List<FrameworkElement>();

            foreach (FrameworkElement element in layoutControl.Children)
            {
                if (element is BaseLayoutControl || element is BaseControl)
                {
                    tempElement.Add(element);
                }
            }

            foreach (FrameworkElement element in tempElement)
            {
                AllocationCanvas(element);
                AssignElementEventHandler(element);
            }

            InitControlElementList();
        }

        /// <summary>
        /// The arrange element.
        /// </summary>
        /// <param name="targetElement">
        /// The target Element.
        /// </param>
        /// <param name="arrangeType">
        /// The arrange Type.
        /// </param>
        public void ArrangeElement(FrameworkElement targetElement, ElementArrangeType arrangeType)
        {
            var ratio = layoutControl.LayoutActualRect.Width / layoutControl.ActualWidth;

            switch (arrangeType)
            {
                case ElementArrangeType.LeftTop:
                    break;

                case ElementArrangeType.Center:
                    Canvas.SetLeft(targetElement, GetTargetElementLeft(targetElement, ratio));
                    Canvas.SetTop(targetElement, GetTargetElementTop(targetElement, ratio));
                    break;

                case ElementArrangeType.UserDefine:
                    break;
            }
        }

        /// <summary>
        /// Layout에 이벤트를 등록한다.
        /// </summary>
        /// <param name="element">
        /// The element.
        /// </param>
        public void AssignElementEventHandler(FrameworkElement element)
        {
            if (element == null)
            {
                return;
            }

            if (layoutControl.MainProcess == null)
            {
                return;
            }

            lock (element)
            {
                element.MouseEnter += layoutControl.MainProcess.ElementMouseEnter;
                element.MouseLeave += layoutControl.MainProcess.ElementMouseLeave;

                if (element is BaseLayoutControl)
                {
                    var control = element as BaseLayoutControl;
                    control.LockTypeChanged += ElementManager_LayoutLockTypeChanged;
                    control.Process = layoutControl.Process;
                }
                else if (element is BaseControl)
                {
                    var control = element as BaseControl;
                    control.LockTypeChanged += ElementManager_ControlLockTypeChanged;
                }
            }
        }

        /// <summary>
        /// The assign element event handler.
        /// </summary>
        public void AssignElementEventHandler()
        {
            if (layoutControl == null)
            {
                return;
            }

            foreach (FrameworkElement element in layoutControl.OutsideCanvas.Children)
            {
                if (IsFoundationElement(element) == false)
                {
                    if (element is BaseLayoutControl || element is BaseControl)
                    {
                        AssignElementEventHandler(element);
                    }
                }
            }

            foreach (FrameworkElement element in layoutControl.InsideCanvas.Children)
            {
                if (IsFoundationElement(element) == false)
                {
                    if (element is BaseLayoutControl || element is BaseControl)
                    {
                        AssignElementEventHandler(element);
                    }
                }
            }
        }

        /// <summary>
        /// 해당 엘리먼트의 ZIdnex를 한단계 올린다.
        /// </summary>
        /// <param name="element">
        /// The element.
        /// </param>
        public void BringForward(FrameworkElement element)
        {
            InitControlElementList();

            var zIndex = Panel.GetZIndex(element);
            if (zIndex >= visualElementList.Count)
            {
                return;
            }

            visualElementList.Remove(element);

            if (visualElementList.Count > zIndex)
            {
                visualElementList.Insert(zIndex + 1, element);
            }
            else
            {
                visualElementList.Add(element);
            }

            SetControlElementZIndex();
        }

        /// <summary>
        /// 해당 엘리먼트의 ZIndex를 최상위로 올린다.
        /// </summary>
        /// <param name="element">
        /// The element.
        /// </param>
        public void BringToFront(FrameworkElement element)
        {
            InitControlElementList();
            visualElementList.Remove(element);
            visualElementList.Add(element);
            SetControlElementZIndex();
        }

        /// <summary>
        /// The clear element event handler.
        /// </summary>
        /// <param name="element">
        /// The element.
        /// </param>
        public void ClearElementEventHandler(FrameworkElement element)
        {
            if (element == null)
            {
                return;
            }

            lock (element)
            {
                if (layoutControl.MainProcess != null)
                {
                    element.MouseEnter -= layoutControl.MainProcess.ElementMouseEnter;
                    element.MouseLeave -= layoutControl.MainProcess.ElementMouseLeave;
                }

                if (element is BaseLayoutControl)
                {
                    var control = element as BaseLayoutControl;
                    control.LockTypeChanged -= ElementManager_LayoutLockTypeChanged;
                }
                else if (element is BaseControl)
                {
                    var control = element as BaseControl;
                    control.LockTypeChanged -= ElementManager_ControlLockTypeChanged;
                }
            }
        }

        /// <summary>
        /// The clear element event handler.
        /// </summary>
        public void ClearElementEventHandler()
        {
            if (layoutControl == null)
            {
                return;
            }

            foreach (FrameworkElement element in layoutControl.OutsideCanvas.Children)
            {
                if (IsFoundationElement(element) == false)
                {
                    if (element is BaseLayoutControl || element is BaseControl)
                    {
                        ClearElementEventHandler(element);
                    }
                }
            }

            foreach (FrameworkElement element in layoutControl.InsideCanvas.Children)
            {
                if (IsFoundationElement(element) == false)
                {
                    if (element is BaseLayoutControl || element is BaseControl)
                    {
                        ClearElementEventHandler(element);
                    }
                }
            }
        }

        /// <summary>
        /// 레이아웃에 엘리먼트를 추가하는 함수입니다. 
        /// ObjectListUserControl에서 ZIndex를 조절할때 사용합니다.
        /// </summary>
        /// <param name="index">
        /// The index.
        /// </param>
        /// <param name="element">
        /// 추가할 엘리먼트.
        /// </param>
        public void InsertElement(int index, FrameworkElement element)
        {
            AllocationCanvas(index, element);
            AssignElementEventHandler(element);
        }

        /// <summary>
        /// The is foundation element.
        /// </summary>
        /// <param name="element">
        /// The element.
        /// </param>
        /// <returns>
        /// True 또는 False를 반환한다.
        /// </returns>
        public bool IsFoundationElement(FrameworkElement element)
        {
            return element is IFoundationElement;
        }

        /// <summary>
        /// ZoomControl의 Child Element를 제거한다.
        /// </summary>
        /// <param name="element">
        /// 제거할 엘리먼트.
        /// </param>
        public void RemoveElement(FrameworkElement element)
        {
            ClearElementEventHandler(element);

            var index = layoutControl.InsideCanvas.Children.IndexOf(element);

            if (index > -1)
            {
                layoutControl.InsideCanvas.Children.Remove(element);
            }

            index = layoutControl.OutsideCanvas.Children.IndexOf(element);

            if (index > -1)
            {
                layoutControl.OutsideCanvas.Children.Remove(element);
            }

            InitControlElementList();
            SetControlElementZIndex();
        }

        public void RemoveElements(Type type, bool includeSubLayout)
        {
            if (layoutControl == null)
            {
                return;
            }

            var list = GetElements();

            foreach (var element in list)
            {
                if (element.GetType() == type)
                {
                    RemoveElement(element);
                }

                if (element is LayoutControl && includeSubLayout == true)
                {
                    (element as LayoutControl).RemoveElements(type, true);
                }
            }
        }

        /// <summary>
        /// 해당 엘리먼트의 ZIdnex를 한단계 내린다.
        /// </summary>
        /// <param name="element">
        /// 해당 엘리먼트.
        /// </param>
        public void SendBackward(FrameworkElement element)
        {
            InitControlElementList();

            var zIndex = Panel.GetZIndex(element);

            if (zIndex < 1)
            {
                return;
            }

            visualElementList.Remove(element);
            visualElementList.Insert(zIndex - 1, element);
            SetControlElementZIndex();
        }

        /// <summary>
        /// 해당 엘리먼트의 ZIdnex를 최하위로 내린다.
        /// </summary>
        /// <param name="element">
        /// 해당 엘리먼트.
        /// </param>
        public void SendToBack(FrameworkElement element)
        {
            InitControlElementList();
            visualElementList.Remove(element);
            visualElementList.Insert(0, element);
            SetControlElementZIndex();
        }

        /// <summary>
        /// ZIndex 최대 크기를 반환한다.
        /// </summary>
        /// <returns>
        /// 최대 ZIndex.
        /// </returns>
        public int GetMaxZIndex()
        {
            int count = 0;

            if (layoutControl != null)
            {
                foreach (FrameworkElement element in layoutControl.OutsideCanvas.Children)
                {
                    if (IsFoundationElement(element) == false)
                    {
                        if (element is BookmarkControl)
                        {

                        }
                        else
                        {
                            count++;
                        }
                    }
                }

                foreach (FrameworkElement element in layoutControl.InsideCanvas.Children)
                {
                    if (IsFoundationElement(element) == false)
                    {
                        if (element is BookmarkControl)
                        {

                        }
                        else
                        {
                            count++;
                        }
                    }
                }    
            }

            return count;
        }

        /// <summary>
        /// 해당 엘리먼트의 ZIdnex를 반환한다.
        /// </summary>
        /// <param name="element">
        /// 해당 엘리먼트.
        /// </param>
        /// <returns>
        /// The get z index.
        /// </returns>
        public int GetZIndex(FrameworkElement element)
        {
            return Panel.GetZIndex(element);
        }

        /// <summary>
        /// 해당 엘리먼트의 ZIdnex를 설정한다..
        /// </summary>
        /// <param name="element">
        /// 해당 엘리먼트.
        /// </param>
        /// <param name="zIndex">
        /// The ZIdnex. 
        /// </param>
        public void SetZIndex(FrameworkElement element, int zIndex)
        {
            InitControlElementList();

            visualElementList.Remove(element);
            visualElementList.Insert(zIndex, element);
            SetControlElementZIndex();
        }

        internal void Clear()
        {
            for (var i = this.visualElementList.Count - 1; i >= 0; i--)
            {
                var element = this.visualElementList[i];
                if (element is IDisposable)
                {
                    (element as IDisposable).Dispose();
                }
            }

            if (this.visualElementList != null)
            {
                this.visualElementList.Clear();
            }
        }

        /// <summary>
        /// 엘리먼트를 InCanvas로 이동시키킬수 있게하기 위한 준비작업
        /// 1. 부모가 OutsideCanvas인 경우 Canvas 이동 후에도 눈에 보이는 위치와 크기가 같아 보이도록 크기와 위치 속성을 변경한다.
        /// 2. 엘리먼트가 부모를 가지고 있으면 부모로 부터 분리 시킨다.
        /// </summary>
        /// <param name="element">
        /// 해당 엘리먼트.
        /// </param>
        private void PerpareAllocateInsideCanvas(FrameworkElement element)
        {
            if (GetAllocatedPosition(element) == AllocatedPosition.OutsideCanvas)
            {
                //Outside 기준 element의 Rect값을 Inside 기준으로 변경함 !!
                var absoluteRect = InnoConvertUtil.GetAbsoluteRect(element, layoutControl.OutsideCanvas, layoutControl.InsideCanvas);
                Canvas.SetLeft(element, absoluteRect.Left);
                Canvas.SetTop(element, absoluteRect.Top);
                element.Width = absoluteRect.Width;
                element.Height = absoluteRect.Height;
            }

            if (element.Parent != null)
            {
                if (element.Parent is Canvas)
                {
                    (element.Parent as Canvas).Children.Remove(element);
                }
                else if (element.Parent is ListBox)
                {
                    (element.Parent as ListBox).Items.Remove(element);
                }
                else if (element.Parent is BaseLayoutControl)
                {
                    (element.Parent as BaseLayoutControl).Children.Remove(element);
                }
            }
        }

        /// <summary>
        /// BaseLayoutControl을 상속받은 엘리먼트 속성인 LockType에 변경이 생긴 경우
        /// 해당 엘리먼트의 부모(InsideCanvas, OutsideCanvas)를 속성에 맞게 변경한다.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private static void ElementManager_LayoutLockTypeChanged(object sender, RoutedPropertyChangedEventArgs<LockTypes> e)
        {
            var element = sender as BaseControl;

            var parentLayout = element.ParentBaseLayoutControl as IBaseLayoutControl;
            if (parentLayout != null)
            {
                parentLayout.AllocationCanvas(element);
            }

            e.Handled = true;
        }

        /// <summary>
        /// The get elements.
        /// </summary>
        /// <returns>
        /// 엘리먼트목록을 반환한다.
        /// </returns>
        private List<FrameworkElement> GetElements()
        {
            var elementList = new List<FrameworkElement>();

            if(layoutControl == null)
            {
                return elementList;
            }

            foreach (FrameworkElement element in layoutControl.OutsideCanvas.Children)
            {
                if (IsFoundationElement(element) == false)
                {
                    elementList.Add(element);
                }
            }

            foreach (FrameworkElement element in layoutControl.InsideCanvas.Children)
            {
                if (IsFoundationElement(element) == false)
                {
                    elementList.Add(element);
                }
            }

            return elementList;
        }

        public FrameworkElement GetFirstElement()
        {
            if (layoutControl != null)
            {
                foreach (FrameworkElement element in layoutControl.OutsideCanvas.Children)
                {
                    if (IsFoundationElement(element) == false)
                    {
                        return element;
                    }
                }

                foreach (FrameworkElement element in layoutControl.InsideCanvas.Children)
                {
                    if (IsFoundationElement(element) == false)
                    {
                        return element;
                    }
                }
            }

            return null;
        }

        public int GetElementsCounts()
        {
            int count = 0;

            if (layoutControl != null)
            {
                foreach (FrameworkElement element in layoutControl.OutsideCanvas.Children)
                {
                    if (IsFoundationElement(element) == false)
                    {
                        count++;
                    }
                }

                foreach (FrameworkElement element in layoutControl.InsideCanvas.Children)
                {
                    if (IsFoundationElement(element) == false)
                    {
                        count++;
                    }
                }    
            }

            return count;            
        }

        private double GetTargetElementTop(FrameworkElement targetElement, double ratio)
        {
            var logicalTop = (layoutControl.ActualHeight - (targetElement.ActualHeight * ratio)) / 2;
            return (logicalTop - layoutControl.LayoutActualRect.Y) / ratio;
        }

        private double GetTargetElementLeft(FrameworkElement targetElement, double ratio)
        {
            var logicalLeft = (layoutControl.ActualWidth - (targetElement.ActualWidth * ratio)) / 2;
            return (logicalLeft - layoutControl.LayoutActualRect.X) / ratio;
        }

        /// <summary>
        /// _controlElementList를 초기화한다. Element가 Add된 경우 사용한다.
        /// </summary>
        /// <param name="element">
        /// 해당 엘리먼트.
        /// </param>
        private void AddControlElementList(UIElement element)
        {
            var count = GetElements().Count;
            Panel.SetZIndex(element, count);
            InitControlElementList();
        }

        /// <summary>
        /// 엘리먼트를 InsideCanvas에 추가한다.
        /// </summary>
        /// <param name="element">
        /// The element.
        /// </param>
        private void AllocateInsideCanvas(FrameworkElement element)
        {
            PerpareAllocateInsideCanvas(element);
            layoutControl.InsideCanvas.Children.Add(element);
        }

        /// <summary>
        /// 엘리먼트를 OutsideCanvas에 인서트한다.
        /// </summary>
        /// <param name="index">
        /// the index.
        /// </param>
        /// <param name="element">
        /// The element.
        /// </param>
        private void AllocateInsideCanvas(int index, FrameworkElement element)
        {
            PerpareAllocateInsideCanvas(element);
            layoutControl.InsideCanvas.Children.Insert(index, element);
        }

        /// <summary>
        /// 엘리먼트를 OutsideCanvas에 추가한다.
        /// </summary>
        /// <param name="element">
        /// The element.
        /// </param>
        private void AllocateOutsideCanvas(FrameworkElement element)
        {
            PrepareAllocateOutsideCanvas(element);
            layoutControl.OutsideCanvas.Children.Add(element);
        }

        /// <summary>
        /// 엘리먼트를 OutsideCanvas에 인서트한다.
        /// </summary>
        /// <param name="index">
        /// The Index.
        /// </param>
        /// <param name="element">
        /// the element.
        /// </param>
        private void AllocateOutsideCanvas(int index, FrameworkElement element)
        {
            PrepareAllocateOutsideCanvas(element);
            layoutControl.OutsideCanvas.Children.Insert(index, element);
        }

        /// <summary>
        /// 해당 Element 를 Canvas 타입에 따라서, 내부 Canvas 에 재배열 한다.
        /// </summary>
        /// <param name="element">
        /// The element.
        /// </param>
        public void AllocationCanvas(FrameworkElement element)
        {
            // by jhlee (BaseLayoutControl.LockType)
            if (BaseLayoutControl.GetLockType(element) == LockTypes.None)
            {
                AllocateInsideCanvas(element);
            }
            else
            {
                AllocateOutsideCanvas(element);
            }
        }

        /// <summary>
        /// 해당 Element 를 Canvas 타입에 따라서, 내부 Canvas 에 재배열 한다.
        /// </summary>
        /// <param name="index">
        /// The index.
        /// </param>
        /// <param name="element">
        /// The element.
        /// </param>
        private void AllocationCanvas(int index, FrameworkElement element)
        {
            if (element.Name == "InfoDisplay")
            {
                return;
            }

            // 내부 Canvas 의 Children 으로 지정
            // by jhlee (BaseLayoutControl.LockType)
            if (BaseLayoutControl.GetLockType(element) == LockTypes.None)
            {
                AllocateInsideCanvas(index, element);
            }
            else
            {
                AllocateOutsideCanvas(index, element);
            }

            InitControlElementList();
        }
        
        /// <summary>
        /// BaseControl을 상속받은 엘리먼트 속성인 LockType에 변경이 생긴 경우
        /// 해당 엘리먼트의 부모(InsideCanvas, OutsideCanvas)를 속성에 맞게 변경한다.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void ElementManager_ControlLockTypeChanged(object sender, RoutedPropertyChangedEventArgs<LockTypes> e)
        {
            var element = sender as FrameworkElement;
            AllocationCanvas(element);
            e.Handled = true;
        }

        /// <summary>
        /// 해당 엘리먼트가 Layout 내부 캔버스 중 어느 위치에 있는지 알려준다.
        /// </summary>
        /// <param name="element">
        /// 해당 엘리먼트.
        /// </param>
        /// <returns>
        /// The AllocatedPosition.
        /// </returns>
        private AllocatedPosition GetAllocatedPosition(UIElement element)
        {
            if (layoutControl.InsideCanvas.Children.Contains(element))
            {
                return AllocatedPosition.InsideCanvas;
            }

            if (layoutControl.OutsideCanvas.Children.Contains(element))
            {
                return AllocatedPosition.OutsideCanvas;
            }

            return AllocatedPosition.UnknownPosition;
        }

        /// <summary>
        /// _controlElementList를 초기화한다.
        /// </summary>
        private void InitControlElementList()
        {
            var list = GetElements();
            visualElementList.Clear();
            foreach (var element in list)
            {
                // 북마크 컨트롤을 최상위로 - by Alvin (110127)
                if (element.GetType() == typeof(BookmarkControl))
                {
                    Panel.SetZIndex(element, list.Count + 10);
                }

                InsertControlElementZIndex(element);
            }

            SetControlElementZIndex();
        }

        public void UpdateLayoutSizeChangeForVisibility(LayoutControl layout)
        {
            if (layoutControl == null)
            {
                return;
            }

            foreach (FrameworkElement childElement in layoutControl.OutsideCanvas.Children)
            {
                if (IsFoundationElement(childElement) == false)
                {
                    if (!(childElement is BaseControl))
                    {
                        continue;
                    }

                    var baseControl = childElement as BaseControl;

                    /*
                    if (childElement is LayoutControl)
                    {
                        UpdateLayoutSizeChangeForVisibility(childElement as LayoutControl);
                    }
                    */

                    if (baseControl.IsAppearInAllLevel || !baseControl.PlayVisible)
                    {
                        continue;
                    }

                    var zoomRatioPercent = layout.ActualZoomRatio * 100;

                    if (zoomRatioPercent >= baseControl.AppearanceMinLevel &&
                        zoomRatioPercent <= baseControl.AppearanceMaxLevel)
                    {
                        baseControl.Visibility = Visibility.Visible;
                    }
                    else
                    {
                        baseControl.Visibility = Visibility.Hidden;
                    }
                }
            }

            foreach (FrameworkElement childElement in layoutControl.InsideCanvas.Children)
            {
                if (IsFoundationElement(childElement) == false)
                {
                    if (!(childElement is BaseControl))
                    {
                        continue;
                    }

                    var baseControl = childElement as BaseControl;

                    /*
                    if (childElement is LayoutControl)
                    {
                        UpdateLayoutSizeChangeForVisibility(childElement as LayoutControl);
                    }
                    */

                    if (baseControl.IsAppearInAllLevel || !baseControl.PlayVisible)
                    {
                        continue;
                    }

                    var zoomRatioPercent = layout.ActualZoomRatio * 100;

                    if (zoomRatioPercent >= baseControl.AppearanceMinLevel &&
                        zoomRatioPercent <= baseControl.AppearanceMaxLevel)
                    {
                        baseControl.Visibility = Visibility.Visible;
                    }
                    else
                    {
                        baseControl.Visibility = Visibility.Hidden;
                    }
                }
            }
        }

        public void UpdateLayoutSizeChange(LayoutControl layout)
        {
            if (layoutControl == null)
            {
                return;
            }

            foreach (FrameworkElement childElement in layoutControl.OutsideCanvas.Children)
            {
                if (IsFoundationElement(childElement) == false)
                {
                    // CameraControl 이 아닌 경우는 PTZ가 생성되지 않으므로 리턴한다.
                    if (!(childElement is CameraControl))
                    {
                        return;
                    }

                    var camControl = childElement as CameraControl;

                    // Config의 CameraPtzEnable이 False이면 무조건 PTZ를 보여주지 않는다.
                    if (!CommonsConfig.Instance.CameraPtzEnable)
                    {
                        camControl.IsEnableToggleButtonShowControlUI = false;
                        return;
                    }

                    // PTZEnableState가 ZoomLevel인 경우에만 사이즈 변경에 따른 PTZ의 Visibility가 변경된다.
                    if (camControl.PTZEnabled != PTZEnableState.ZoomLevel)
                    {
                        return;
                    }

                    if (camControl.PTZLockToParentsMinLevel)
                    {
                        var zoomRatio = layout.ZoomRatio;

                        bool isVisible = zoomRatio <= layout.MaxRatio && zoomRatio >= layout.MinRatio;
                        camControl.ZoomLevelChanged(isVisible);
                    }
                    else
                    {
                        var zoomRatioPercent = layout.ZoomRatio * 100;

                        bool isVisible = zoomRatioPercent >= camControl.PTZMinLevel;
                        camControl.ZoomLevelChanged(isVisible);
                    }


                    if (!(childElement is BaseControl))
                    {
                        continue;
                    }

                    var baseControl = childElement as BaseControl;

                    /*
                    if (baseControl is LayoutControl)
                    {
                        UpdateLayoutSizeChange(baseControl as LayoutControl);
                    }
                    */

                    if (baseControl.IsAppearInAllLevel || !baseControl.PlayVisible)
                    {
                        continue;
                    }

                    var zoomRatioPercent1 = layout.ZoomRatio * 100;

                    if (zoomRatioPercent1 >= baseControl.AppearanceMinLevel &&
                        zoomRatioPercent1 <= baseControl.AppearanceMaxLevel)
                    {
                        baseControl.Visibility = Visibility.Visible;
                    }
                    else
                    {
                        baseControl.Visibility = Visibility.Hidden;
                    }
                }
            }

            foreach (FrameworkElement childElement in layoutControl.InsideCanvas.Children)
            {
                if (IsFoundationElement(childElement) == false)
                {
                    // CameraControl 이 아닌 경우는 PTZ가 생성되지 않으므로 리턴한다.
                    if (!(childElement is CameraControl))
                    {
                        return;
                    }

                    var camControl = childElement as CameraControl;

                    // Config의 CameraPtzEnable이 False이면 무조건 PTZ를 보여주지 않는다.
                    if (!CommonsConfig.Instance.CameraPtzEnable)
                    {
                        camControl.IsEnableToggleButtonShowControlUI = false;
                        return;
                    }

                    // PTZEnableState가 ZoomLevel인 경우에만 사이즈 변경에 따른 PTZ의 Visibility가 변경된다.
                    if (camControl.PTZEnabled != PTZEnableState.ZoomLevel)
                    {
                        return;
                    }

                    if (camControl.PTZLockToParentsMinLevel)
                    {
                        var zoomRatio = layout.ZoomRatio;

                        bool isVisible = zoomRatio <= layout.MaxRatio && zoomRatio >= layout.MinRatio;
                        camControl.ZoomLevelChanged(isVisible);
                    }
                    else
                    {
                        var zoomRatioPercent = layout.ZoomRatio * 100;

                        bool isVisible = zoomRatioPercent >= camControl.PTZMinLevel;
                        camControl.ZoomLevelChanged(isVisible);
                    }


                    if (!(childElement is BaseControl))
                    {
                        continue;
                    }

                    var baseControl = childElement as BaseControl;

                    /*
                    if (baseControl is LayoutControl)
                    {
                        UpdateLayoutSizeChange(baseControl as LayoutControl);
                    }
                    */

                    if (baseControl.IsAppearInAllLevel || !baseControl.PlayVisible)
                    {
                        continue;
                    }

                    var zoomRatioPercent1 = layout.ZoomRatio * 100;

                    if (zoomRatioPercent1 >= baseControl.AppearanceMinLevel &&
                        zoomRatioPercent1 <= baseControl.AppearanceMaxLevel)
                    {
                        baseControl.Visibility = Visibility.Visible;
                    }
                    else
                    {
                        baseControl.Visibility = Visibility.Hidden;
                    }
                }
            }
        }

        /// <summary>
        /// ZIndex를 비교하여 _controlElementList에 엘리먼트 순서를 배치한다.
        /// 000 에 (0) 을 추가하면 000(0);
        /// 0001 에 (2) 을 추가하면 0001(2);
        /// 0123 에 (2) 를 추가하면 012(2)3; 
        /// 0357 에 (2) 를 추가하면 0(2)357;.
        /// </summary>
        /// <param name="element">
        /// 해당 엘리먼트.
        /// </param>
        private void InsertControlElementZIndex(FrameworkElement element)
        {
            var elementZIndex = Panel.GetZIndex(element);

            for (var index = 0; index < visualElementList.Count; index++)
            {
                var listZIndex = Panel.GetZIndex(visualElementList[index]);
                if (elementZIndex < listZIndex)
                {
                    visualElementList.Insert(index, element);
                    return;
                }
            }

            visualElementList.Add(element);
        }

        /// <summary>
        /// 엘리먼트를 OutCanvas로 이동시키킬수 있게하기 위한 준비작업
        /// 1. 부모가 InsideCanvas인 경우 Canvas 이동 후에도 눈에 보이는 위치와 크기가 같아 보이도록 크기와 위치 속성을 변경한다.
        /// 2. 엘리먼트가 부모를 가지고 있으면 부모로 부터 분리 시킨다.
        /// </summary>
        /// <param name="element">
        /// 해당 엘리먼트.
        /// </param>
        private void PrepareAllocateOutsideCanvas(FrameworkElement element)
        {
            if (GetAllocatedPosition(element) == AllocatedPosition.InsideCanvas)
            {
                //Inside 기준 element의 Rect값을 Outside 기준으로 변경함 !!
                var absoluteRect = InnoConvertUtil.GetAbsoluteRect(element, layoutControl.InsideCanvas, layoutControl.OutsideCanvas);
                Canvas.SetLeft(element, absoluteRect.Left);
                Canvas.SetTop(element, absoluteRect.Top);
                element.Width = absoluteRect.Width;
                element.Height = absoluteRect.Height;
            }

            if (element.Parent != null)
            {
                if (element.Parent is Canvas)
                {
                    (element.Parent as Canvas).Children.Remove(element);
                }
                else if (element.Parent is ListBox)
                {
                    (element.Parent as ListBox).Items.Remove(element);
                }
                else if (element.Parent is BaseLayoutControl)
                {
                    (element.Parent as BaseLayoutControl).Children.Remove(element);
                }
            }
        }

        /// <summary>
        /// 엘리먼트에 ZIdnex를 재설정한다.
        /// </summary>
        private void SetControlElementZIndex()
        {
            for (var index = 0; index < visualElementList.Count; index++)
            {
                Panel.SetZIndex(visualElementList[index], index);
            }
        }
    }
}