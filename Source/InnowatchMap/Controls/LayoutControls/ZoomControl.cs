﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ZoomControl.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Defines the ZoomControl type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.DLLs.LayoutControls
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Media;
    using System.Windows.Media.Animation;
    using Innotive.InnoWatch.Commons.BaseControls;
    using Innotive.InnoWatch.Commons.Configs;
    using Innotive.InnoWatch.Commons.EventArguments;
    using Innotive.InnoWatch.Commons.Publics;

    /// <summary>
    /// Layout의 Zoom 관련된 기본 기능들.
    /// </summary>
    public class ZoomControl : BaseLayoutControl
    {
        public static readonly DependencyProperty IsZoomEnabledProperty =
            DependencyProperty.Register(
                "IsZoomEnabled",
                typeof(bool),
                typeof(ZoomControl),
                new FrameworkPropertyMetadata(true));

        public static readonly DependencyProperty LayoutActualRectProperty =
            DependencyProperty.Register(
                "LayoutActualRect",
                typeof(Rect),
                typeof(ZoomControl),
                new FrameworkPropertyMetadata(new Rect(0, 0, 800, 600), null));

        public static readonly DependencyProperty MaxRatioProperty =
            DependencyProperty.Register(
                "MaxRatio",
                typeof(double),
                typeof(ZoomControl),
                new FrameworkPropertyMetadata(double.NaN));

        /// <summary>
        /// ZoomOut의 최대 범위 지정 속성.
        /// </summary>
        public static readonly DependencyProperty MinRatioProperty =
            DependencyProperty.Register(
                "MinRatio",
                typeof(double),
                typeof(ZoomControl),
                new FrameworkPropertyMetadata(double.NaN));

        public static readonly DependencyProperty OriginHeightProperty =
            DependencyProperty.Register(
                "OriginHeight",
                typeof(double),
                typeof(ZoomControl),
                new FrameworkPropertyMetadata(600.0, new PropertyChangedCallback(OnChangeOriginHeight)));

        public static readonly DependencyProperty OriginWidthProperty =
            DependencyProperty.Register(
                "OriginWidth",
                typeof(double),
                typeof(ZoomControl),
                new FrameworkPropertyMetadata(800.0, new PropertyChangedCallback(OnChangeOriginWidth)));

        public static readonly DependencyProperty ZoomInRatioProperty =
            DependencyProperty.Register(
                "ZoomInRatio",
                typeof(double),
                typeof(ZoomControl),
                new FrameworkPropertyMetadata(1.50));

        public static readonly DependencyProperty ZoomOutRatioProperty =
            DependencyProperty.Register(
                "ZoomOutRatio",
                typeof(double),
                typeof(ZoomControl),
                new FrameworkPropertyMetadata(1.50));

        public static readonly RoutedEvent eChangedEvent =
            EventManager.RegisterRoutedEvent(
                "eChanged", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(ZoomControl));

        public static readonly RoutedEvent eVisibilityEvent =
            EventManager.RegisterRoutedEvent(
                "eVisibility", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(ZoomControl));

        public static readonly RoutedEvent eZoomEndedEvent =
            EventManager.RegisterRoutedEvent(
                "eZoomEnded", RoutingStrategy.Bubble, typeof(ZoomEndedRoutedEventHandler), typeof(ZoomControl));

        public static readonly RoutedEvent ePanningEndedEvent =
            EventManager.RegisterRoutedEvent(
                "ePanningEnded", RoutingStrategy.Bubble, typeof(PanningEndedRoutedEventHandler), typeof(ZoomControl));

        public static readonly RoutedEvent ePanningStartedEvent =
            EventManager.RegisterRoutedEvent(
                "ePanningStarted", RoutingStrategy.Bubble, typeof(PanningStartedRoutedEventHandler), typeof(ZoomControl));

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public double CurrentZoomPercentage
        {
            get
            {
                return this.GetCurrentZoomPercentage();
            }
            set
            {
                this.SetCurrentZoomPercentage(value);
            }
        }

        private void SetCurrentZoomPercentage(double value)
        {
            var ratio = value / 100;
            if (ratio >= ZoomRatio)
            {
                ZoomIn(ratio / ZoomRatio, double.NaN);
            }
            else
            {
                ZoomOut(ZoomRatio / ratio, double.NaN);
            }
        }

        private double GetCurrentZoomPercentage()
        {
            double ratio;

            if (this.InsideViewbox.DesireWidth <= 0)
            {
                ratio = this.InsideViewbox.Width / this.InsideCanvas.Width;
            }
            else
            {
                ratio = this.InsideViewbox.DesireWidth / this.InsideCanvas.Width;
            }

            var percentage = ratio * 100;

            return Math.Round(percentage, 3, MidpointRounding.AwayFromZero);
        }

        private LayoutFrame LayoutFrame;

        /// <summary>
        /// Initializes a new instance of the <see cref="ZoomControl"/> class.
        /// </summary>
        public ZoomControl()
            : base()
        {
            this.UpdateCameraControl = true;

            this.DoCreate();

            this.Width = this.OriginWidth;
            this.Height = this.OriginHeight;

            this.InsideViewbox.DesireWidth = 0.0;

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ZoomControl"/> class.
        /// </summary>
        /// <param name="layoutSize">
        /// The layout size.
        /// </param>
        public ZoomControl(Size layoutSize)
            : base()
        {
            this.UpdateCameraControl = true;

            this.DoCreate();

            this.OriginWidth = layoutSize.Width;
            this.OriginHeight = layoutSize.Height;

            this.Width = this.OriginWidth;
            this.Height = this.OriginHeight;

            this.LayoutActualRect = new Rect(0, 0, this.OriginWidth, this.OriginHeight);
        }

        /// <summary>
        /// The eChanged.
        /// </summary>
        public event RoutedEventHandler eChanged
        {
            add
            {
                this.AddHandler(eChangedEvent, value);
            }

            remove
            {
                this.RemoveHandler(eChangedEvent, value);
            }
        }

        /// <summary>
        /// The eVisibility.
        /// </summary>
        public event RoutedEventHandler eVisibility
        {
            add
            {
                this.AddHandler(eVisibilityEvent, value);
            }

            remove
            {
                this.RemoveHandler(eVisibilityEvent, value);
            }
        }

        /// <summary>
        /// The ePanningStarted.
        /// </summary>
        public event PanningStartedRoutedEventHandler ePanningStarted
        {
            add
            {
                this.AddHandler(ePanningStartedEvent, value);
            }

            remove
            {
                this.RemoveHandler(ePanningStartedEvent, value);
            }
        }

        /// <summary>
        /// The ePanningEnded.
        /// </summary>
        public event PanningEndedRoutedEventHandler ePanningEnded
        {
            add
            {
                this.AddHandler(ePanningEndedEvent, value);
            }

            remove
            {
                this.RemoveHandler(ePanningEndedEvent, value);
            }
        }

        /// <summary>
        /// The eZoomEnded.
        /// </summary>
        public event ZoomEndedRoutedEventHandler eZoomEnded
        {
            add
            {
                this.AddHandler(eZoomEndedEvent, value);
            }

            remove
            {
                this.RemoveHandler(eZoomEndedEvent, value);
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether IsZoomEnabled.
        /// </summary>
        public bool IsZoomEnabled
        {
            get
            {
                return (bool)this.GetValue(IsZoomEnabledProperty);
            }

            set
            {
                this.SetValue(IsZoomEnabledProperty, value);
            }
        }

        /// <summary>
        /// Gets or sets LayoutActualRect.
        /// 실제 Layout이 화면상에 표출되고 있는 영역을 반환한다.
        /// 내부적으로 _InsideViewbox의 BoundRect와 동일하다고 보면 된다.
        /// </summary>
        public Rect LayoutActualRect
        {
            get
            {
                // 혹시라도 동기화가 틀어졌을 경우에 대비해 다시 세팅한다.
                this.SetValue(LayoutActualRectProperty, this.InsideViewbox.CanvasBound);

                return this.InsideViewbox.CanvasBound;
            }

            set
            {
                //InnoTrace.Trace(1, "[blackRoot10] LayoutActualRect값 변경 시작 !!");

                //움직임이 없는 경우 처리하지 않음 !! (ex. 마지막 Level에서 Zoom In/Out, Panning을 할 경우)
                if (this.InsideViewbox.CanvasBound.Equals(this.MakeRectInnerArea(value)))
                    return;

                Commons.Utils.ZoomPanSmoothManager.Start(this);

                this.InsideViewbox.ClearZoomAnimation();
                var rect = this.SetInsideViewboxRect(value);

                this.InsideViewbox.DesireWidth = rect.Width;

                //LayoutActualRect를 get하는 부분에서 BoundRect값으로 다시 세팅을 해줌 !!
                //해주면 좋고.. 안해줘도 큰 상관없음 !!);
                this.SetValue(LayoutActualRectProperty, rect);

                // Panning 시 너무 많은 UI 갱신 발생함. ZoomPanningEnd 에서 한 번만 호출하도록 수정. - by shwlee.
                //this.RaiseEvent(new RoutedEventArgs(eChangedEvent, null));

                this.RaiseEvent(new ZoomEndedRoutedEventArgs(eZoomEndedEvent, this, false));
            }
        }

        /// <summary>
        /// Gets or sets MaxRatio.
        /// </summary>
        public double MaxRatio
        {
            get
            {
                return (double)this.GetValue(MaxRatioProperty);
            }

            set
            {
                this.SetValue(MaxRatioProperty, value);
            }
        }

        /// <summary>
        /// 줌아웃 최대 지정 범위 속성.
        /// Gets or sets MinRatio.
        /// </summary>
        public double MinRatio
        {
            get
            {
                return (double)this.GetValue(MinRatioProperty);
            }

            set
            {
                this.SetValue(MinRatioProperty, value);
            }
        }

        /// <summary>
        /// Gets or sets OriginHeight.
        /// </summary>
        public double OriginHeight
        {
            get
            {
                return (double)this.GetValue(OriginHeightProperty);
            }

            set
            {
                this.SetValue(OriginHeightProperty, value);
            }
        }

        /// <summary>
        /// Gets or sets OriginWidth.
        /// </summary>
        public double OriginWidth
        {
            get
            {
                return (double)this.GetValue(OriginWidthProperty);
            }

            set
            {
                this.SetValue(OriginWidthProperty, value);
            }
        }

        /// <summary>
        /// Gets or sets ZoomInRatio.
        /// </summary>
        public double ZoomInRatio
        {
            get
            {
                return (double)this.GetValue(ZoomInRatioProperty);
            }

            set
            {
                this.SetValue(ZoomInRatioProperty, value);
            }
        }

        /// <summary>
        /// Gets or sets ZoomOutRatio.
        /// </summary>
        public double ZoomOutRatio
        {
            get
            {
                return (double)this.GetValue(ZoomOutRatioProperty);
            }

            set
            {
                this.SetValue(ZoomOutRatioProperty, value);
            }
        }

        /// <summary>
        /// Gets ZoomRatio ( 현재 ZoomRatio 를 구한다. 애니메이션 동작중에도 중간값이 아닌 최종 결과값을 반환함. ).
        /// </summary>
        public double ZoomRatio
        {
            get
            {
                double ratio;

                if (this.InsideViewbox.DesireWidth <= 0)
                {
                    ratio = this.InsideViewbox.Width / this.InsideCanvas.Width;
                }
                else
                {
                    ratio = this.InsideViewbox.DesireWidth / this.InsideCanvas.Width;
                }

                if(double.IsNaN(ratio))
                {
                    ratio = 1.0;
                }

                return Math.Round(ratio, 3, MidpointRounding.AwayFromZero);
            }
        }

        public double ActualZoomRatio
        {
            get
            {
                var ratio = this.InsideViewbox.ActualWidth / this.InsideCanvas.ActualWidth;

                return Math.Round(ratio, 3, MidpointRounding.AwayFromZero);
            }
        }

        /// <summary>
        /// Gets or sets main canvas  기본 구성 요소.
        /// </summary>
        public InnoCanvas InsideCanvas { get; internal set; }

        /// <summary>
        /// Gets or sets Zoom 비율 조정 기본 구성 요소.
        /// </summary>
        public InsideViewbox InsideViewbox { get; internal set; }

        /// <summary>
        /// Gets or sets overlay canvas (LockToLayout Object)  기본 구성 요소.
        /// </summary>
        public InnoCanvas OutsideCanvas { get; internal set; }

        /// <summary>
        /// Gets or sets 외부 비율 조정 기본 구성 요소.
        /// </summary>
        public OutsideViewbox OutsideViewbox { get; internal set; }

        /// <summary>
        /// Gets or Sets If Camera Control has too Update.
        /// </summary>
        public bool UpdateCameraControl { get; set; }

        internal double OverlayRatioX
        {
            get
            {
                return this.OutsideViewbox.ActualWidth / this.OriginWidth;
            }
        }

        internal double OverlayRatioY
        {
            get
            {
                return this.OutsideViewbox.ActualHeight / this.OriginHeight;
            }
        }

        /// <summary>
        /// OriginSize를 재설정한다. ( 내부 Canvas 실제크기 포함 ).
        /// </summary>
        /// <param name="newSize">
        /// 새롭게 설정할 Size 값.
        /// </param>
        public virtual void OriginResize(Size newSize)
        {
            this.OriginWidth = newSize.Width;
            this.OriginHeight = newSize.Height;

            this.RaiseEvent(new RoutedEventArgs(eChangedEvent, null));
        }

        /// <summary>
        /// The go to location animation.
        /// </summary>
        /// <param name="desireRect">
        /// The desire rect.
        /// </param>
        /// <param name="startTime">
        /// The start time.
        /// </param>
        /// <param name="duration">
        /// The duration.
        /// </param>
        public void GoToLocationAnimation(Rect desireRect, double startTime, double duration, KeySpline keySpline)
        {
            //by blackRoot : LayoutActualRect를 직접호출할 경우 처음 한번은 IPQ Redraw가 되지만 두번째부터 IPQ Redraw가 안되는 문제가 발생함 !!
            //아래쪽 DoGoToLocationZoom()을 탈 경우 정상동작을 함. 그래서 이쪽 부분을 주석처리함 !!
            /*
                               // Storyboard의 Duration이 0 인 경우 Storyboard Complete 이벤트를 타지 않는 경우가 있어 
                               // Redrawbasecontrol을 호출하지 않을 때가 있어서 임시처리함. 
                               if (startTime <= 0 && duration <= 0)
                               {
                                   //desireRect = this.MakeRectInnerArea(desireRect);
                                   this.LayoutActualRect = desireRect;
                                   return;
                               }
                               */
            
            if (double.IsNaN(duration))
            {
                duration = CommonsConfig.Instance.LayoutZoomAnimationSpeed;
            }

            if (duration <= 0)
                duration = 0.1;

            desireRect = this.MakeRectInnerArea(desireRect);

            // 만약 마지막 GotoLocation이 처음 LayoutActualRect와 같으면 실행이 안되는 문제가 발생함.(by jhlee)
            //if (!this.LayoutActualRect.Equals(desireRect))
            //{
                //animation이 끝나는 시점에서 외부로 event를 발생시킴 !! 외부에서 ZoomRatio값을 가져갈때 이 값이 사용됨 !!
                this.InsideViewbox.DesireWidth = desireRect.Width;

                //실제 InsideViewbox의 left, top, width, height를 변경해줌 !! animation 내부에서 이 값을 바꿔주기 때문에 호출하면 안됨 !!
                //  zoom ratio 적용 안되는 문제 때문에 추가함. 
                // this.InsideViewbox.CanvasBound = desireRect;

                this.InsideViewbox.DoZoom(desireRect, false, keySpline, duration, startTime);
            //}
        }

        /// <summary>
        /// 줌 애니메이션 동작
        /// iDisplay에서 Sync Message를 받은 경우. iCommand 내부 Sync를 진행하는 경우 !!
        /// </summary>
        /// <param name="layoutOriginGuid"></param>
        /// <param name="desireRect"></param>
        public void ZoomAnimationSync(Guid layoutOriginGuid, Rect desireRect)
        {
            var action = new Action(() =>
            {
                if (this.OriginGUID == Guid.Empty || layoutOriginGuid == Guid.Empty || this.OriginGUID != layoutOriginGuid)
                {
                    this.ZoomAnimation(desireRect, double.NaN);
                }
            });

            if (this.Dispatcher.CheckAccess())
            {
                action.Invoke();
            }
            else
            {
                this.Dispatcher.BeginInvoke(action);
            }
        }

        /// <summary>
        /// 내부 줌 에니메이션 동작. 
        /// iDisplay에서 Sync Message를 받은 경우. iCommand 내부 Sync를 진행하는 경우 !!
        /// </summary>
        /// <param name="desireRect">
        /// The desire rect.
        /// </param>
        /// <param name="duration">
        /// The duration.
        /// </param>
        private void ZoomAnimation(Rect desireRect, double duration)
        {
            //MouseClick으로 Zoom을 할경우 NaN값이 넘어옴 !!
            if (double.IsNaN(duration))
            {
                duration = CommonsConfig.Instance.LayoutZoomAnimationSpeed;
            }

            if (duration <= 0)
            {
                this.LayoutActualRect = desireRect;

                this.OnChangedCurrentZoomPercentage(new EventArgs());
                return;
            }

            desireRect = this.MakeRectInnerArea(desireRect);

            System.Diagnostics.Debug.WriteLine("Zoom : " + desireRect.ToString());

            if (!this.LayoutActualRect.Equals(desireRect))
            {
                this.InsideViewbox.DesireWidth = desireRect.Width;
                this.InsideViewbox.DoZoom(desireRect, true, null, duration);
            }

            this.OnChangedCurrentZoomPercentage(new EventArgs());
        }

        public event EventHandler<EventArgs> ChangedCurrentZoomPercentage;

        public void OnChangedCurrentZoomPercentage(EventArgs e)
        {
            var handler = this.ChangedCurrentZoomPercentage;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        /// <summary>
        /// 사용자가 Click 해서 ZoomIn 을 실행하는 경우.
        /// </summary>
        /// <param name="mousePoint">
        /// </param>
        public Rect ZoomIn(Point mousePoint)
        {
            if (this.IsZoomEnabled == false)
            {
                return Rect.Empty;
            }

            // 현재 대비 ZoomInRatio한 결과의 비율을 MaxRatio와 비교하자
            double maxArea = (this.OriginWidth * this.MaxRatio) * (this.OriginHeight * this.MaxRatio);

            double currentZoomInRatio;
            if (this.InsideViewbox.CalculateRatioArea(this.ZoomInRatio) > maxArea)
            {
                currentZoomInRatio = Math.Sqrt(maxArea / (this.InsideViewbox.ActualWidth * this.InsideViewbox.ActualHeight));
            }
            else
            {
                currentZoomInRatio = this.ZoomInRatio;
            }

            var desireRect = new Rect();

            // Viewbox 위치
            var zoomOutedLayoutLeft = mousePoint.X - mousePoint.X * currentZoomInRatio;
            var zoomOutedLayoutTop = mousePoint.Y - mousePoint.Y * currentZoomInRatio;

            desireRect.X = zoomOutedLayoutLeft + Canvas.GetLeft(this.InsideViewbox) * currentZoomInRatio;
            desireRect.Y = zoomOutedLayoutTop + Canvas.GetTop(this.InsideViewbox) * currentZoomInRatio;

            // Viewbox 넓이
            desireRect.Width = this.InsideViewbox.ActualWidth * currentZoomInRatio;
            desireRect.Height = this.InsideViewbox.ActualHeight * currentZoomInRatio;

            this.ZoomAnimation(desireRect, double.NaN);

            return desireRect;
        }

        /// <summary>
        /// 사용자가 지정한 ZoomRatio 로  ZoomIn 기능을 동작 시키는 경우. Action을 실행했을 경우임 !!
        /// </summary>
        /// <param name="ratio">
        /// zoom ratio.
        /// </param>
        /// <param name="duration">
        /// duration.
        /// </param>
        public Rect ZoomIn(double ratio, double duration)
        {
            if (this.IsZoomEnabled == false)
            {
                return Rect.Empty;
            }

            if (ratio > this.MaxRatio)
            {
                ratio = this.MaxRatio;
            }

            var centerPoint = new Point(this.Width / 2, this.Height / 2);
            var desireRect = new Rect();

            // 현재 대비 ratio 결과의 비율을 MaxRatio와 비교하자
            var maxArea = (this.OriginWidth * this.MaxRatio) * (this.OriginHeight * this.MaxRatio);

            if (this.InsideViewbox.CalculateRatioArea(ratio) > maxArea)
            {
                ratio = Math.Sqrt(maxArea / (this.InsideViewbox.ActualWidth * this.InsideViewbox.ActualHeight));
            }

            // Viewbox 위치
            var zoomOutedLayoutLeft = centerPoint.X - centerPoint.X * ratio;
            var zoomOutedLayoutTop = centerPoint.Y - centerPoint.Y * ratio;

            desireRect.X = zoomOutedLayoutLeft + Canvas.GetLeft(this.InsideViewbox) * ratio;
            desireRect.Y = zoomOutedLayoutTop + Canvas.GetTop(this.InsideViewbox) * ratio;

            // Viewbox 넓이
            desireRect.Width = this.InsideViewbox.ActualWidth * ratio;
            desireRect.Height = this.InsideViewbox.ActualHeight * ratio;

            this.ZoomAnimation(desireRect, duration);

            return desireRect;
        }

        /// <summary>
        /// 사용자가 Click 해서 ZoomOut 을 실행하는 경우.
        /// </summary>
        /// <param name="mousePoint">
        /// </param>
        public Rect ZoomOut(Point mousePoint)
        {
            if (this.IsZoomEnabled == false)
            {
                return Rect.Empty;
            }

            // 현재 대비 ZoomOutRatio한 결과의 비율을 MinRatio와 비교하자
            double minArea = (this.OriginWidth * this.MinRatio) * (this.OriginHeight * this.MinRatio);

            double currentZoomOutRatio;

            if (this.InsideViewbox.CalculateRatioArea(1 / this.ZoomOutRatio) < minArea)
            {
                currentZoomOutRatio = Math.Sqrt((this.InsideViewbox.ActualWidth * this.InsideViewbox.ActualHeight) / minArea);
            }
            else
            {
                currentZoomOutRatio = this.ZoomOutRatio;
            }

            var desireRect = new Rect();

            // Viewbox 위치
            var zoomOutedLayoutLeft = mousePoint.X - (mousePoint.X / currentZoomOutRatio);
            var zoomOutedLayoutTop = mousePoint.Y - (mousePoint.Y / currentZoomOutRatio);

            desireRect.X = zoomOutedLayoutLeft + (Canvas.GetLeft(this.InsideViewbox) / currentZoomOutRatio);
            desireRect.Y = zoomOutedLayoutTop + (Canvas.GetTop(this.InsideViewbox) / currentZoomOutRatio);

            // Viewbox 넓이
            desireRect.Width = this.InsideViewbox.ActualWidth / currentZoomOutRatio;
            desireRect.Height = this.InsideViewbox.ActualHeight / currentZoomOutRatio;

            this.ZoomAnimation(desireRect, double.NaN);

            return desireRect;
        }

        /// <summary>
        /// 사용자가 지정한 ZoomRatio 로  ZoomOut 기능을 동작 시키는 경우.
        /// </summary>
        /// <param name="ratio">
        /// The ratio.
        /// </param>
        /// <param name="duration">
        /// The duration.
        /// </param>
        public Rect ZoomOut(double ratio, double duration)
        {
            if (this.IsZoomEnabled == false)
            {
                return Rect.Empty;
            }

            if (ratio < this.MinRatio)
            {
                ratio = this.MinRatio;
            }

            var centerPoint = new Point(this.Width / 2, this.Height / 2);
            var desireRect = new Rect();

            // 현재 대비 ratio 결과의 비율을 MinRatio와 비교하자
            var minArea = (this.OriginWidth * this.MinRatio) * (this.OriginHeight * this.MinRatio);

            if (this.InsideViewbox.CalculateRatioArea(1 / ratio) < minArea)
            {
                ratio = Math.Sqrt((this.InsideViewbox.ActualWidth * this.InsideViewbox.ActualHeight) / minArea);
            }

            // Viewbox 위치
            var zoomOutedLayoutLeft = centerPoint.X - (centerPoint.X / ratio);
            var zoomOutedLayoutTop = centerPoint.Y - (centerPoint.Y / ratio);

            desireRect.X = zoomOutedLayoutLeft + (Canvas.GetLeft(this.InsideViewbox) / ratio);
            desireRect.Y = zoomOutedLayoutTop + (Canvas.GetTop(this.InsideViewbox) / ratio);

            // Viewbox 넓이
            desireRect.Width = this.InsideViewbox.ActualWidth / ratio;
            desireRect.Height = this.InsideViewbox.ActualHeight / ratio;

            this.ZoomAnimation(desireRect, duration);

            return desireRect;
        }

        /// <summary>
        /// The zoom reset.
        /// </summary>
        public void ZoomReset()
        {
            this.OriginFitSizePosition(this.InsideViewbox, 0);

            this.RaiseEvent(new RoutedEventArgs(eChangedEvent, null));
            this.RaiseEvent(new ZoomEndedRoutedEventArgs(eZoomEndedEvent, null));
        }

        /// <summary>
        /// 속성 변경 알림 이벤트 핸들러.
        /// </summary>
        /// <param name="e">Dependency Property Changed Event Args.</param>
        protected override void OnPropertyChanged(DependencyPropertyChangedEventArgs e)
        {
            base.OnPropertyChanged(e);

            // 구지 부모와 연결되어 있지 않으면 계산할 필요 없다고 생각됨 (by jhlee)
            if (PresentationSource.FromVisual(this) != null)
            {
                if (string.Compare(e.Property.Name, "Left", true) == 0 ||
                    string.Compare(e.Property.Name, "Top", true) == 0 ||
                    string.Compare(e.Property.Name, "ActualWidth", true) == 0 ||
                    string.Compare(e.Property.Name, "ActualHeight", true) == 0 ||
                    string.Compare(e.Property.Name, "IsVisible", true) == 0)
                {
                    this.OutsideViewbox.UpdateViewAreaAsync();
                    this.InsideViewbox.UpdateViewAreaAsync();
                }
            }
        }

        /// <summary>
        /// Canvas의 Children을 Update함 (주로 IPQ, Camera쪽)
        /// </summary>
        public override void UpdateViewArea(Rect parentCanvasScreenRegion, List<Rect> parentsScreenRegion)
        {
            // 부모와 연결되어 있지 않으면 처리하지 않음.
            if (PresentationSource.FromVisual(this) == null)
            {
                return;
            }

            // 영역 리스트 다시 계산
            var historyScreenRegion = new List<Rect>();

            if (parentsScreenRegion != null)
            {
                historyScreenRegion.AddRange(parentsScreenRegion);
            }

            if (parentCanvasScreenRegion != Rect.Empty)
            {
                historyScreenRegion.Add(parentCanvasScreenRegion);
            }

            if (historyScreenRegion.Count != 0)
            {
                // OutsideViewbox 화면 영역 계산 및 추가
                var outsideViewboxScreenRegion = new Rect(
                    this.PointToScreen(new Point(0, 0)),
                    this.PointToScreen(new Point(this.OutsideViewbox.ActualWidth, this.OutsideViewbox.ActualHeight)));
                historyScreenRegion.Add(outsideViewboxScreenRegion);

                // OutsideCanvas 화면 갱신
                this.OutsideCanvas.UpdateViewArea(historyScreenRegion);

                // OutsideCanvas 화면 영역 계산 및 추가
                var outsideCanvasScreenRegion = new Rect(
                    this.PointToScreen(new Point(0, 0)),
                    this.PointToScreen(new Point(this.OutsideCanvas.ActualWidth, this.OutsideCanvas.ActualHeight)));
                historyScreenRegion.Add(outsideCanvasScreenRegion);

                // InsideViewbox 화면 영역 계산 및 추가
                var insideViewboxScreenRegion = new Rect(
                    this.PointToScreen(new Point(0, 0)),
                    this.PointToScreen(new Point(this.InsideViewbox.ActualWidth, this.InsideViewbox.ActualHeight)));
                historyScreenRegion.Add(insideViewboxScreenRegion);

                // InsideCanvas 화면 갱신
                this.InsideCanvas.UpdateViewArea(historyScreenRegion);
            }
            else
            {
                this.OutsideCanvas.UpdateViewArea();
                this.InsideCanvas.UpdateViewArea();
            }
        }

        /// <summary>
        /// The arrange override.
        /// </summary>
        /// <param name="arrangeBounds">
        /// The arrange bounds.
        /// </param>
        /// <returns>
        /// </returns>
        protected override Size ArrangeOverride(Size arrangeBounds)
        {
            foreach (UIElement child in this.Children)
            {
                if (child == null)
                {
                    continue;
                }

                double x = 0;
                double y = 0;

                var left = Canvas.GetLeft(child);
                if (!double.IsNaN(left))
                {
                    x = left;
                }
                else
                {
                    var right = Canvas.GetRight(child);
                    if (!double.IsNaN(right))
                    {
                        x = arrangeBounds.Width - child.DesiredSize.Width - right;
                    }
                }

                var top = Canvas.GetTop(child);
                if (!double.IsNaN(top))
                {
                    y = top;
                }
                else
                {
                    var bottom = Canvas.GetBottom(child);
                    if (!double.IsNaN(bottom))
                    {
                        y = arrangeBounds.Height - child.DesiredSize.Height - bottom;
                    }
                }

                child.Arrange(new Rect(new Point(x, y), child.DesiredSize));
            }

            return arrangeBounds;
        }

        /// <summary>
        /// The do dispose.
        /// </summary>
        /// <param name="isManage">
        /// The is manage.
        /// </param>
        protected override void DoDispose(bool isManage)
        {
            if (isManage)
            {
                for (var i = this.InsideCanvas.Children.Count - 1; i >= 0; i--)
                {
                    var element = this.InsideCanvas.Children[i];
                    if (element is IDisposable)
                    {
                        (element as IDisposable).Dispose();
                    }
                }

                this.LayoutFrame = null;


                for (var i = this.OutsideCanvas.Children.Count - 1; i >= 0; i--)
                {
                    var element = this.OutsideCanvas.Children[i];
                    if (element is IDisposable)
                    {
                        (element as IDisposable).Dispose();
                    }
                }

                this.Children.Clear();
            }

            base.DoDispose(isManage);
        }

        /// <summary>
        /// The measure override.
        /// </summary>
        /// <param name="constraint">
        /// The constraint.
        /// </param>
        /// <returns>
        /// </returns>
        protected override Size MeasureOverride(Size constraint)
        {
            var childConstraint = new Size(Double.PositiveInfinity, Double.PositiveInfinity);

            foreach (UIElement child in this.Children)
            {
                if (child != null)
                {
                    child.Measure(childConstraint);
                }
            }

            return new Size();
        }

        /// <summary>
        /// element 를 Object ( Element ) 크기 위치와 동일하게 만든다. 
        /// 주로 사용되는 곳은 OutsideViewbox 가 Stretch 되길 원할때.
        /// </summary>
        /// <param name="element">
        /// ZoomControl내부 Foundation Element.
        /// </param>
        /// <param name="margin">
        /// </param>
        protected void ObjectFitSizePosition(FrameworkElement element, double margin)
        {
            if (element == null)
            {
                return;
            }

            element.Width = this.Width - margin * 2;
            element.Height = this.Height - margin * 2;

            Canvas.SetLeft(element, 0 + margin);
            Canvas.SetTop(element, 0 + margin);
        }

        /// <summary>
        /// element 를 Origin ( OriginWidth, Height ) 크기와 동일하게 하고, 위치를 초기화 한다. 
        /// </summary>
        /// <param name="element">
        /// ZoomControl내부 Foundation Element.
        /// </param>
        /// <param name="margin">
        /// </param>
        protected void OriginFitSizePosition(FrameworkElement element, double margin)
        {
            if (this.OriginWidth <= 0 || this.OriginHeight <= 0)
            {
                return;
            }

            if (element == null)
            {
                return;
            }

            element.Width = this.OriginWidth - margin * 2;
            element.Height = this.OriginHeight - margin * 2;

            Canvas.SetLeft(element, 0 + margin);
            Canvas.SetTop(element, 0 + margin);
        }

        protected void RedrawLayoutFrame()
        {
            if (Public.IsEditingType())
            {
                if (this.LayoutFrame != null)
                {
                    this.LayoutFrame.Redraw(this);
                }
            }
        }

        /// <summary>
        /// The on render.
        /// </summary>
        /// <param name="drawingContext">
        /// The drawing context.
        /// </param>
        protected override void OnRender(DrawingContext drawingContext)
        {
            base.OnRender(drawingContext);

            drawingContext.DrawRectangle(
                this.Background,
                null,
                new Rect(0, 0, this.Width, this.Height));
        }

        /// <summary>
        /// The on render size changed.
        /// </summary>
        /// <param name="sizeInfo">
        /// The size info.
        /// </param>
        protected override void OnRenderSizeChanged(SizeChangedInfo sizeInfo)
        {
            base.OnRenderSizeChanged(sizeInfo);

            this.OutsideViewbox.Width = sizeInfo.NewSize.Width;
            this.OutsideViewbox.Height = sizeInfo.NewSize.Height;
        }

        private void DoCreate()
        {
            this.MinRatio = 0.0001;
            this.MaxRatio = 10000;

            // Set Outside Viewbox
            this.OutsideViewbox = new OutsideViewbox {Stretch = Stretch.Fill, Name = "OutsideViewbox"};

            // Set Overlay Canvas (lock to Layout)
            this.OutsideCanvas = new InnoCanvas {Name = "OutsideCanvas", Background = Brushes.White, ClipToBounds = false};

            // Set Zoom Viewbox
            this.InsideViewbox = new InsideViewbox {Name = "InsideViewbox", DesireWidth = this.OriginWidth};
            this.InsideViewbox.StoryboardCompleted += this.InsideViewbox_StoryboardCompleted;
            this.InsideViewbox.UpdateViewAreaForVisibility += this.InsideViewbox_UpdateViewAreaForVisibility;

            // Set Main Canvas
            this.InsideCanvas = new InnoCanvas
            {
                Name = "InsideCanvas",
                Background = Brushes.White,
                ClipToBounds = false
            };

            this.Children.Add(this.OutsideViewbox);
            this.OutsideViewbox.Child = this.OutsideCanvas;
            this.OutsideCanvas.Children.Add(this.InsideViewbox);
            this.InsideViewbox.Child = this.InsideCanvas;

            // ClipToBounds가 true이면 그래픽 성능에 영향을 줌.
            this.ClipToBounds = true;

            this.Loaded += this.ZoomControl_Loaded;

            this.LayoutFrame = new LayoutFrame();
            this.RedrawLayoutFrame();
            this.Children.Add(this.LayoutFrame);
        }

        private void ZoomControl_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= this.ZoomControl_Loaded;

            this.RefreshOriginResize();

            //by blackRoot : PlayerLayoutFileData.HandleLayoutControlLoaded() 내에서 GoHome()을 하는중 이쪽 Loaded가 발생하면 GoHome이 멈추는 증상이 발생함 !!
            //GoHome()이 완료된 결과가 LayoutActualRect값을 세팅한것과 같기 때문에 여기서 안해줘도 됨 !!
            //this.LayoutActualRect = new Rect(0, 0, this.OriginWidth, this.OriginHeight);
        }

        private static void OnChangeOriginHeight(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            var zoomControl = o as ZoomControl;

            if (zoomControl == null)
            {
                return;
            }

            zoomControl.InsideCanvas.Height = (double)e.NewValue;
            zoomControl.OutsideCanvas.Height = (double)e.NewValue;

            if (zoomControl.IsVisible)
            {
                zoomControl.RefreshOriginResize();
            }
        }

        private static void OnChangeOriginWidth(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            var zoomControl = o as ZoomControl;

            if (zoomControl == null)
            {
                return;
            }

            zoomControl.InsideCanvas.Width = (double) e.NewValue;
            zoomControl.OutsideCanvas.Width = (double) e.NewValue;

            if (zoomControl.IsVisible)
            {
                zoomControl.RefreshOriginResize();
            }
        }

        /// <summary>
        /// Layout을 Zoom/Panning시 화면밖의 미지의 영역으로 벗어나지 않도록 제한한다.
        /// </summary>
        /// <param name="rect">
        /// </param>
        /// <returns>
        /// </returns>
        private Rect MakeRectInnerArea(Rect rect)
        {
            if (rect.Width < this.OriginWidth)
            {
                if (rect.Left < 0)
                {
                    rect.X = 0;
                }
                else if (rect.Right > this.OriginWidth)
                {
                    rect.X = this.OriginWidth - rect.Width;
                }
            }
            else
            {
                if (rect.Left > 0)
                {
                    rect.X = 0;
                }
                else if (rect.Right < this.OriginWidth)
                {
                    rect.X = this.OriginWidth - rect.Width;
                }
            }

            if (rect.Height < this.OriginHeight)
            {
                if (rect.Top < 0)
                {
                    rect.Y = 0;
                }
                else if (rect.Bottom > this.OriginHeight)
                {
                    rect.Y = this.OriginHeight - rect.Height;
                }
            }
            else
            {
                if (rect.Top > 0)
                {
                    rect.Y = 0;
                }
                else if (rect.Bottom < this.OriginHeight)
                {
                    rect.Y = this.OriginHeight - rect.Height;
                }
            }

            return rect;
        }

        private void RefreshOriginResize()
        {
            this.ObjectFitSizePosition(this.OutsideViewbox, 0);
            this.OriginFitSizePosition(this.OutsideCanvas, 0);
            this.OriginFitSizePosition(this.InsideViewbox, 0); // 100% 상태
            this.OriginFitSizePosition(this.InsideCanvas, 0);
        }

        /// <summary>
        /// LayoutControl 기준으로 Layout 의 실제 위치를 설정한다.
        /// </summary>
        /// <param name="value">
        /// 설정할 위치값.
        /// </param>
        /// <returns>
        /// 최종 설정된 위치.
        /// </returns>
        private Rect SetInsideViewboxRect(Rect value)
        {
            if (this.InsideViewbox == null)
            {
                return Rect.Empty;
            }

            var destRect = this.MakeRectInnerArea(value);

            this.InsideViewbox.CanvasBound = destRect;

            return destRect;
        }

        public void SetSelectedRectangle(FrameType type)
        {
            switch (type)
            {
                case FrameType.Normal:
                    this.LayoutFrame.Stroke = new SolidColorBrush(Colors.PaleVioletRed);
                    break;
                default:
                    this.LayoutFrame.Stroke = new SolidColorBrush(Colors.YellowGreen);
                    break;
            }
        }

        private void InsideViewbox_StoryboardCompleted(object sender, EventArgs e)
        {
            /*
            //IPQ Redraw가 제대로 안되는 문제가 발생해서 최종값을 다시 한번 세팅해줌 !!
            //Duration을 0으로 하고 실행한것과 같음 !!
            this.LayoutActualRect = this.InsideViewbox.CanvasBound;
            */


            this.RaiseEvent(new RoutedEventArgs(eChangedEvent, null));
            this.RaiseEvent(new ZoomEndedRoutedEventArgs(eZoomEndedEvent, this, true));
        }

        private void InsideViewbox_UpdateViewAreaForVisibility(object sender, EventArgs e)
        {
            this.RaiseEvent(new RoutedEventArgs(eVisibilityEvent, null));
        }
    }
}