﻿using System.Windows.Controls;

namespace Innotive.InnoWatch.DLLs.LayoutControls
{
    using System;
    using System.Globalization;
    using System.Windows;
    using System.Windows.Data;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Shapes;
    
    /// <summary>
    /// Interaction logic for LayoutControlEditor.xaml
    /// </summary>
    public partial class LayoutControlEditor
    {
        public LayoutControlEditorViewModel ViewModel = new LayoutControlEditorViewModel();

        public LayoutControlEditor()
        {
            InitializeComponent();
            this.DataContext = ViewModel;
        }

        private void MouseLeftButtonDown_ChangeColor(object sender, MouseButtonEventArgs e)
        {

        }
    }

    /// <summary>
    /// CurrentZoomPercentage 유효성 규칙
    /// </summary>
    public class CurrentZoomPercentageValidationRule : ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            double doubleValue;
            if (value == null) return new ValidationResult(false, null);
            var result = double.TryParse(value.ToString(), out doubleValue );
            if (doubleValue.Equals(0)) return new ValidationResult(false, "0값은 처리할 수 없습니다.");

            return new ValidationResult(result, null);
        }
    }

    /// <summary>
    /// 문자열로 변경된 색상값에서 #의 제거하거나 붙여넣습니다.
    /// </summary>
    public class BackgroundCoverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null) return string.Empty;
            var result = value.ToString();
            result = result.Substring(1, result.Length - 1);

            return result;
        }
        
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var result = (string)value;
            result = "#" + result;

            var converter = new BrushConverter();

            try
            {
                return converter.ConvertFromString(result) as Brush;
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
