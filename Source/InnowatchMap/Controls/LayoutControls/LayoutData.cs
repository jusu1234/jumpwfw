﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LayoutData.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Defines the LayoutData type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.DLLs.LayoutControls
{
    using System.Windows.Media;

    /// <summary>
    /// 레이아웃 정보 클래스.
    /// </summary>
    public class LayoutData
    {
        private readonly Brush insideBackground = new SolidColorBrush(Colors.White);
        private readonly Brush outsideBackground = new SolidColorBrush(Colors.White);

        /// <summary>
        /// Initializes a new instance of the <see cref="LayoutData"/> class.
        /// </summary>
        public LayoutData()
        {
            this.IsShowToList = true;
            this.OriginHeight = 768;
            this.OriginWidth = 1024;
            this.Name = "Layout01";
        }

        /// <summary>
        /// Gets or sets Name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets OriginWidth.
        /// </summary>
        public double OriginWidth { get; set; }

        /// <summary>
        /// Gets or sets OriginHeight.
        /// </summary>
        public double OriginHeight { get; set; }

        /// <summary>
        /// Gets or sets Description.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether IsShowToList.
        /// </summary>
        public bool IsShowToList { get; set; }

        /// <summary>
        /// Gets or sets OutsideBackground.
        /// </summary>
        public Brush InsideBackground
        {
            get
            {
                return this.insideBackground;
            }

            set
            {
                ((SolidColorBrush)this.insideBackground).Color = ((SolidColorBrush)value).Color;
            }
        }
        
        /// <summary>
        /// Gets or sets OutsideBackground.
        /// </summary>
        public Brush OutsideBackground
        {
            get
            {
                return this.outsideBackground;
            }

            set
            {
                ((SolidColorBrush)this.outsideBackground).Color = ((SolidColorBrush)value).Color;
            }
        }
    }
}
