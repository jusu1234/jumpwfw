﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PlayProcess.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   The play process.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.DLLs.LayoutControls.Processes
{
    using System;
    using System.Windows;
    using System.Windows.Input;
    using Innotive.InnoWatch.Commons.EventArguments;
    using Innotive.InnoWatch.Commons.GuidManagers;
    using Innotive.InnoWatch.Commons.Publics;

    /// <summary>
    /// The play process.
    /// </summary>
    public class PlayProcess : Process
    {
        #region Constants and Fields

        protected bool isMouseLButtonDowned;

        protected bool isMouseRButtonDowned;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="PlayProcess"/> class.
        /// </summary>
        /// <param name="layoutControl">
        /// The layout control.
        /// </param>
        public PlayProcess(LayoutControl layoutControl)
        {
            this.OwnerLayout = layoutControl;
        }

        #endregion

        #region Methods

        /// <summary>
        /// ElementMouseEnter: 20081129.
        /// </summary>
        /// <param name="sender">
        /// </param>
        /// <param name="e">
        /// </param>
        internal override void ElementMouseEnter(object sender, MouseEventArgs e)
        {
            base.ElementMouseEnter(sender, e);
            e.Handled = true;

            this.ActionRaise(sender, TriggerEventType.MouseEnter);
        }

        /// <summary>
        /// eleement_MouseLeave: 20081129.
        /// </summary>
        /// <param name="sender">
        /// </param>
        /// <param name="e">
        /// </param>
        internal override void ElementMouseLeave(object sender, MouseEventArgs e)
        {
            base.ElementMouseLeave(sender, e);
            e.Handled = true;

            this.ActionRaise(sender, TriggerEventType.MouseLeave);
        }
        
        internal override void LayoutControl_MouseWheel(object sender, MouseWheelEventArgs e)
        {
            base.LayoutControl_MouseWheel(sender, e);
            e.Handled = true;

            // MouseWheel 의 경우는 Action 으로 처리할 이유가 없어서, ZoomIn/Out Action 으로 처리함.
            Point clickPoint = this.OwnerLayout.GetMousePosition();
            if (e.Delta > 0)
            {
                Rect desireRect = this.OwnerLayout.ZoomIn(clickPoint);
            }
            else if (e.Delta < 0)
            {
                Rect desireRect = this.OwnerLayout.ZoomOut(clickPoint);

                if (Rect.Empty != desireRect)
                {
                    this.OwnerLayout.RaiseEvent(
                        new RectRoutedEventArgs(LayoutControl.eZoomOutEvent, this.OwnerLayout, desireRect));
                }
            }
        }

        #endregion

        /// <summary>
        /// ActionRaise: 20081129.
        /// </summary>
        /// <param name="sender">
        /// </param>
        /// <param name="triggerEventType">
        /// </param>
        /// <returns>
        /// The action raise.
        /// </returns>
        protected bool ActionRaise(object sender, TriggerEventType triggerEventType)
        {
            if (!(sender is FrameworkElement))
            {
                return false;
            }

            var trigerElement = sender as FrameworkElement;

            Guid guid = GuidManager.GetSyncGuid(trigerElement);

            if (guid == Guid.Empty)
            {
                return false;
            }

            var args = new ActionEventArgs(triggerEventType);
            this.OwnerLayout.ActionRaise(sender, args);

            return args.IsExcuted;
        }
    }
}