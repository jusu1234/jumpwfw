﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="RecordProcess.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   The record process.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.DLLs.LayoutControls.Processes
{
    /// <summary>
    /// The record process.
    /// </summary>
    public class RecordProcess : Process
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="RecordProcess"/> class.
        /// </summary>
        /// <param name="layoutControl">
        /// The layout control.
        /// </param>
        public RecordProcess(LayoutControl layoutControl)
        {
            this.OwnerLayout = layoutControl;
        }

        #endregion
    }
}