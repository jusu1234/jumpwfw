﻿// -----------------------------------------------------------------------
// <copyright file="LayoutControlEditorViewModel.cs" company="Microsoft">
// Innotive Inc. Korea
// </copyright>
// -----------------------------------------------------------------------

namespace Innotive.InnoWatch.DLLs.LayoutControls
{
    using System.ComponentModel;
    using System.Windows.Media;

    using Innotive.InnoWatch.Commons.BaseControls;

    /// <summary>
    /// 레이아웃에 관련된 속성 창 뷰 모델.
    /// </summary>
    public class LayoutControlEditorViewModel: INotifyPropertyChanged
    {
        /// <summary>
        /// INotifyPropertyChanged 기본 이벤트.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// SelectedElements 인터페이스.
        /// </summary>
        public ISelectedElements SelectedElements;
        
        public bool? IsZoomEnabled
        {
            get
            {
                if (this.SelectedElements == null) return null;

                return this.SelectedElements.IsZoomEnabled;
            }
            set
            {
                if (this.SelectedElements == null) return;

                if (value == null)
                {
                    this.OnPropertyChanged("IsZoomEnabled");
                    return;
                }

                this.SelectedElements.IsZoomEnabled = value;
                this.OnPropertyChanged("IsZoomEnabled");
            }
        }

        public double? CurrentZoomPercentage
        {
            get
            {
                if (this.SelectedElements == null) return null;

                return this.SelectedElements.CurrentZoomPercentage;
            }
            set
            {
                if (this.SelectedElements == null) return;

                if (value == null)
                {
                    this.OnPropertyChanged("CurrentZoomPercentage");
                    return;
                }

                this.SelectedElements.CurrentZoomPercentage = value;
                this.OnPropertyChanged("CurrentZoomPercentage");
            }
        }

        public double? MinZoomPercentage
        {
            get
            {
                if (this.SelectedElements == null) return null;
                return this.SelectedElements.MinZoomPercentage;
            }
            set
            {
                if (this.SelectedElements == null) return;

                if (value == null)
                {
                    this.OnPropertyChanged("MinZoomPercentage");
                    return;
                }

                this.SelectedElements.MinZoomPercentage = value;
                this.OnPropertyChanged("MinZoomPercentage");
            }
        }

        public double? MaxZoomPercentage
        {
            get
            {
                if (this.SelectedElements == null) return null;
                return this.SelectedElements.MaxZoomPercentage;
            }
            set
            {
                if (this.SelectedElements == null) return;

                if (value == null)
                {
                    this.OnPropertyChanged("MaxZoomPercentage");
                    return;
                }

                this.SelectedElements.MaxZoomPercentage = value;
                this.OnPropertyChanged("MaxZoomPercentage");
            }
        }

        public double? ZoomInRatio
        {
            get
            {
                if (this.SelectedElements == null) return null;
                return this.SelectedElements.ZoomInRatio;
            }
            set
            {
                if (this.SelectedElements == null) return;

                if (value == null)
                {
                    this.OnPropertyChanged("ZoomInRatio");
                    return;
                }

                this.SelectedElements.ZoomInRatio = value;
                this.OnPropertyChanged("ZoomInRatio");
            }
        }
        
        public double? ZoomOutRatio
        {
            get
            {
                if (this.SelectedElements == null) return null;
                return this.SelectedElements.ZoomOutRatio;
            }
            set
            {
                if (this.SelectedElements == null) return;

                if (value == null)
                {
                    this.OnPropertyChanged("ZoomOutRatio");
                    return;
                }

                this.SelectedElements.ZoomOutRatio = value;
                this.OnPropertyChanged("ZoomOutRatio");
            }
        }

        public Brush InsideBackground
        {
            get
            {
                if (this.SelectedElements == null) return null;
                return this.SelectedElements.InsideBackground;
            }
            set
            {
                if (this.SelectedElements == null) return;

                if (value == null)
                {
                    this.OnPropertyChanged("InsideBackground");
                    return;
                }

                this.SelectedElements.InsideBackground = value;
                this.OnPropertyChanged("InsideBackground");
            }
        }

        public Brush OutsideBackground
        {
            get
            {
                if (this.SelectedElements == null) return null;
                return this.SelectedElements.OutsideBackground;
            }
            set
            {
                if (this.SelectedElements == null) return;

                if (value == null)
                {
                    this.OnPropertyChanged("OutsideBackground");
                    return;
                }

                this.SelectedElements.OutsideBackground = value;
                this.OnPropertyChanged("OutsideBackground");
            }
        }

        public double? OriginWidth
        {
            get
            {
                if (this.SelectedElements == null) return null;
                return this.SelectedElements.OriginWidth;
            }
            set
            {
                if (this.SelectedElements == null) return;

                if (value == null)
                {
                    this.OnPropertyChanged("OriginWidth");
                    return;
                }

                this.SelectedElements.OriginWidth = value;
                this.OnPropertyChanged("OriginWidth");
            }
        }

        public double? OriginHeight
        {
            get
            {
                if (this.SelectedElements == null) return null;
                return this.SelectedElements.OriginHeight;
            }
            set
            {
                if (this.SelectedElements == null) return;

                if (value == null)
                {
                    this.OnPropertyChanged("OriginHeight");
                    return;
                }

                this.SelectedElements.OriginHeight = value;
                this.OnPropertyChanged("OriginHeight");
            }
        }

        /// <summary>
        /// 바인딩 시작.
        /// </summary>
        /// <param name="selected"></param>
        public void SetBindings(ISelectedElements selected)
        {
            if (selected == null) return;

            this.SelectedElements = selected;
            this.SelectedElements.PropertyChanged += this.SelectedElements_PropertyChanged;

            this.IsZoomEnabled = this.SelectedElements.IsZoomEnabled;
            this.CurrentZoomPercentage = this.SelectedElements.CurrentZoomPercentage;
            this.MinZoomPercentage = this.SelectedElements.MinZoomPercentage;
            this.MaxZoomPercentage = this.SelectedElements.MaxZoomPercentage;
            this.ZoomInRatio = this.SelectedElements.ZoomInRatio;
            this.ZoomOutRatio = this.SelectedElements.ZoomOutRatio;
            this.InsideBackground = this.SelectedElements.InsideBackground;
            this.OutsideBackground = this.SelectedElements.OutsideBackground;
            this.OriginWidth = this.SelectedElements.OriginWidth;
            this.OriginHeight = this.SelectedElements.OriginHeight;
        }

        /// <summary>
        /// 속성 변경시 이벤트 발생.
        /// </summary>
        /// <param name="propertyName"></param>
        public void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        private void SelectedElements_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            this.OnPropertyChanged(e.PropertyName);
        }
    }
}
