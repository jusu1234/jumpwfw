﻿// -----------------------------------------------------------------------
// <copyright file="BaseControlEditorOnMapcs.cs" company="Microsoft">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace Innotive.InnoWatch.DLLs.BaseControlEditors
{
    using System.ComponentModel;

    using Innotive.InnoWatch.Commons.BaseControls;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class BaseControlEditorOnMapViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged(string propertyName)
        {
            if (propertyName != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        public ISelectedElements SelectedElements;

        public void SetBindings(ISelectedElements selected)
        {
            if (selected == null) return;

            this.SelectedElements = selected;
            this.SelectedElements.PropertyChanged += this.SelectedElements_PropertyChanged;

            this.LockType = SelectedElements.LockType;

            //this.CenterLatitude = this.SelectedElements.Latitude;
            //this.CenterLongitude = this.SelectedElements.Longitude;

            //this.LeftGis = this.SelectedElements.LeftGis;
            //this.TopGis = this.SelectedElements.TopGis;
            //this.RightGis = this.SelectedElements.RightGis;
            //this.BottomGis = this.SelectedElements.BottomGis;
        }

        void SelectedElements_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            this.OnPropertyChanged(e.PropertyName);
        }
        
        public double? CenterLatitude
        {
            get
            {
                if (this.SelectedElements == null) return null;

                return this.SelectedElements.CenterLatitude;
            }
            set
            {
                if (this.SelectedElements == null) return;

                this.SelectedElements.CenterLatitude = value;
                this.OnPropertyChanged("CenterLatitude");
            }
        }

        public double? CenterLongitude
        {
            get
            {
                if (this.SelectedElements == null) return null;

                return this.SelectedElements.CenterLongitude;
            }
            set
            {
                if (this.SelectedElements == null) return;

                this.SelectedElements.CenterLongitude = value;
                this.OnPropertyChanged("CenterLongitude");
            }
        }

        public double? LeftGis
        {
            get
            {
                if (this.SelectedElements == null) return null;

                return this.SelectedElements.LeftGis;
            }
            set
            {
                if (this.SelectedElements == null) return;

                this.SelectedElements.LeftGis = value;
                this.OnPropertyChanged("LeftGis");
            }
        }

        public double? TopGis
        {
            get
            {
                if (this.SelectedElements == null) return null;

                return this.SelectedElements.TopGis;
            }
            set
            {
                if (this.SelectedElements == null) return;

                this.SelectedElements.TopGis = value;
                this.OnPropertyChanged("TopGis");
            }
        }

        public double? RightGis
        {
            get
            {
                if (this.SelectedElements == null) return null;

                return this.SelectedElements.RightGis;
            }
            set
            {
                if (this.SelectedElements == null) return;

                this.SelectedElements.RightGis = value;
                this.OnPropertyChanged("RightGis");
            }
        }

        public double? BottomGis
        {
            get
            {
                if (this.SelectedElements == null) return null;

                return this.SelectedElements.BottomGis;
            }
            set
            {
                if (this.SelectedElements == null) return;

                this.SelectedElements.BottomGis = value;
                this.OnPropertyChanged("BottomGis");
            }
        }

        public double? Left
        {
            get
            {
                if (this.SelectedElements == null) return null;

                return this.SelectedElements.Left;
            }
            set
            {
                if (this.SelectedElements == null) return;

                this.SelectedElements.Left = value;
                this.OnPropertyChanged("Left");
            }
        }

        public double? Top
        {
            get
            {
                if (this.SelectedElements == null) return null;

                return this.SelectedElements.Top;
            }
            set
            {
                if (this.SelectedElements == null) return;

                this.SelectedElements.Top = value;
                this.OnPropertyChanged("Top");
            }
        }

        public double? Width
        {
            get
            {
                if (this.SelectedElements == null) return null;

                return this.SelectedElements.Width;
            }
            set
            {
                if (this.SelectedElements == null) return;
                if (value == null || value == this.Width)
                {
                    this.OnPropertyChanged("Width");
                    return;
                }

                this.SelectedElements.Width = value;
                this.OnPropertyChanged("Width");
            }
        }

        public double? Height
        {
            get
            {
                if (this.SelectedElements == null) return null;

                return this.SelectedElements.Height;
            }
            set
            {
                if (this.SelectedElements == null) return;
                if (value == null || value == this.Height)
                {
                    this.OnPropertyChanged("Height");
                    return;
                }

                this.SelectedElements.Height = value;
                this.OnPropertyChanged("Height");
            }
        }

        public LockTypes? LockType
        {
            get
            {
                if (this.SelectedElements == null) return null;

                var lockType = this.SelectedElements.LockType;
                if (lockType == null)
                {
                    return null;
                }
                return (LockTypes)lockType;
                
            }
            set
            {
                if (this.SelectedElements == null) return;
                if (value == null)
                {
                    return;
                }

                this.SelectedElements.LockType = (LockTypes)value;
                this.OnPropertyChanged("LockType");
            }
        }


    }
}
