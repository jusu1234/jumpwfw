﻿namespace Innotive.InnoWatch.DLLs.BaseControlEditors
{
    using System;
    using System.Globalization;
    using System.Windows.Data;

    /// <summary>
    /// Interaction logic for BaseControlEditor.xaml
    /// </summary>
    public partial class BaseControlEditorOnMap
    {
        public BaseControlEditorOnMapViewModel ViewModel = new BaseControlEditorOnMapViewModel();

        public BaseControlEditorOnMap()
        {
            InitializeComponent();
            this.DataContext = this.ViewModel;
        }
    }
}
