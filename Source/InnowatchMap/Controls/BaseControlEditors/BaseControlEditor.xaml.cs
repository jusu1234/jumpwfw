﻿namespace Innotive.InnoWatch.DLLs.BaseControlEditors
{
    using System;
    using System.Globalization;
    using System.Windows.Data;

    /// <summary>
    /// Interaction logic for BaseControlEditor.xaml
    /// </summary>
    public partial class BaseControlEditor
    {
        public BaseControlEditorViewModel ViewModel = new BaseControlEditorViewModel();

        public BaseControlEditor()
        {
            InitializeComponent();
            this.DataContext = this.ViewModel;
        }
    }

    public class IsAppearanceInAllLevelConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null) return null;
            
            var param = bool.Parse(parameter.ToString());
            var result = !((bool)value ^ param);
            return result;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null) return null;
            
            var param = bool.Parse(parameter.ToString());
            var result = !((bool)value ^ param);
            return result;
        }
    }
}
