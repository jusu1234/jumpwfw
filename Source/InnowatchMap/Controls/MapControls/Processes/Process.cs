﻿namespace Innotive.InnoWatch.DLLs.MapControls.Processes
{
    using System.Windows.Input;
    using Innotive.InnoWatch.DLLs.LayoutControls;

    /// <summary>
    /// The process.
    /// </summary>
    public class Process
    {
        /// <summary>
        /// Gets or sets _layoutControl.
        /// </summary>
        protected MapControl OwnMapControl { get; set; }

        internal virtual void ElementMouseEnter(object sender, MouseEventArgs e)
        {
        }

        internal virtual void ElementMouseLeave(object sender, MouseEventArgs e)
        {
        }

        internal virtual void MapControl_MouseEnter(object sender, MouseEventArgs e)
        {
        }

        internal virtual void MapControl_MouseLeave(object sender, MouseEventArgs e)
        {
        }

        internal virtual void MapControl_MouseWheel(object sender, MouseWheelEventArgs e)
        {
        }
    }
}