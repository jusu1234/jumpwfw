﻿namespace Innotive.InnoWatch.DLLs.MapControls.Processes
{
    using Innotive.InnoWatch.DLLs.LayoutControls;

    /// <summary>
    /// The record process.
    /// </summary>
    public class RecordProcess : Process
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="RecordProcess"/> class.
        /// </summary>
        /// <param name="layoutControl">
        /// The layout control.
        /// </param>
        public RecordProcess(MapControl mapControl)
        {
            this.OwnMapControl = mapControl;
        }

        #endregion
    }
}