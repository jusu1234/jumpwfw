﻿using System;
using System.Windows;
using System.Windows.Input;
using Innotive.InnoWatch.Commons.EventArguments;
using Innotive.InnoWatch.Commons.GuidManagers;
using Innotive.InnoWatch.Commons.Publics;
using Innotive.InnoWatch.DLLs.LayoutControls;

namespace Innotive.InnoWatch.DLLs.MapControls.Processes
{
    /// <summary>
    /// The play process.
    /// </summary>
    public class PlayProcess : Process
    {
        #region Constants and Fields

        protected bool isMouseLButtonDowned;

        protected bool isMouseRButtonDowned;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="PlayProcess"/> class.
        /// </summary>
        /// <param name="layoutControl">
        /// The layout control.
        /// </param>
        public PlayProcess(MapControl mapControl)
        {
            OwnMapControl = mapControl;
        }

        #endregion

        #region Methods

        /// <summary>
        /// ElementMouseEnter: 20081129.
        /// </summary>
        /// <param name="sender">
        /// </param>
        /// <param name="e">
        /// </param>
        internal override void ElementMouseEnter(object sender, MouseEventArgs e)
        {
            base.ElementMouseEnter(sender, e);
            e.Handled = true;

            ActionRaise(sender, TriggerEventType.MouseEnter);
        }

        /// <summary>
        /// eleement_MouseLeave: 20081129.
        /// </summary>
        /// <param name="sender">
        /// </param>
        /// <param name="e">
        /// </param>
        internal override void ElementMouseLeave(object sender, MouseEventArgs e)
        {
            base.ElementMouseLeave(sender, e);
            e.Handled = true;

            ActionRaise(sender, TriggerEventType.MouseLeave);
        }

        internal override void MapControl_MouseWheel(object sender, MouseWheelEventArgs e)
        {
            base.MapControl_MouseWheel(sender, e);

            e.Handled = true;

            //// MouseWheel 의 경우는 Action 으로 처리할 이유가 없어서, ZoomIn/Out Action 으로 처리함.
            //Point clickPoint = OwnMapControl.GetMousePosition();
            //if (e.Delta > 0)
            //{
            //    Rect desireRect = OwnMapControl.ZoomIn(clickPoint);
            //}
            //else if (e.Delta < 0)
            //{
            //    Rect desireRect = OwnMapControl.ZoomOut(clickPoint);

            //    if (Rect.Empty != desireRect)
            //    {
            //        OwnerLayout.RaiseEvent(
            //            new RectRoutedEventArgs(LayoutControl.eZoomOutEvent, OwnerLayout, desireRect));
            //    }
            //}
        }

        #endregion

        /// <summary>
        /// ActionRaise: 20081129.
        /// </summary>
        /// <param name="sender">
        /// </param>
        /// <param name="triggerEventType">
        /// </param>
        /// <returns>
        /// The action raise.
        /// </returns>
        protected bool ActionRaise(object sender, TriggerEventType triggerEventType)
        {
            //if (!(sender is FrameworkElement))
            //{
            //    return false;
            //}

            //var trigerElement = sender as FrameworkElement;

            //Guid guid = GuidManager.GetSyncGuid(trigerElement);

            //if (guid == Guid.Empty)
            //{
            //    return false;
            //}

            //var args = new ActionEventArgs(triggerEventType);
            //OwnMapControl.ActionRaise(sender, args);

            //return args.IsExcuted;

            return false;
        }
    }
}