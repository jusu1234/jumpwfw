﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FullViewCameraEventArgs.cs" company="Innotive Inc. Korea">
//   Copyright (c) 2012 Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   FullViewCamera 시 발생하는 이벤트
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.DLLs.MapControls.Event
{
    using System;

    /// <summary>
    /// FullViewCamera 시 발생하는 이벤트
    /// </summary>
    public class FullViewCameraEventArgs : EventArgs
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FullViewCameraEventArgs"/> class. 
        /// </summary>
        /// <param name="cameraId">
        /// The cameraId.
        /// </param>
        /// <param name="isFull">
        /// The is Full.
        /// </param>
        public FullViewCameraEventArgs(string cameraId, bool isFull)
        {
            this.CameraId = cameraId;
            this.IsFull = isFull;
        }

        /// <summary>
        /// Gets or sets CameraId.
        /// </summary>
        public string CameraId { get; set; }

        /// <summary>
        /// Gets or sets isFull.
        /// </summary>
        public bool IsFull { get; set; }
    }
}
