﻿namespace Innotive.InnoWatch.DLLs.MapControls
{
    using Innotive.InnoWatch.Commons.PropertyEditor;

    /// <summary>
    /// Interaction logic for MapControlEditor.xaml
    /// </summary>
    public partial class MapControlEditor : BaseEditor
    {
        public MapControlEditorViewModel ViewModel = new MapControlEditorViewModel();

        public MapControlEditor()
        {
            InitializeComponent();
            this.DataContext = ViewModel;
        }
    }
}