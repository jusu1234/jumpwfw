﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PTZControl.xaml.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Interaction logic for PTZControl.xaml.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Threading;

namespace Innotive.InnoWatch.DLLs.CameraControls
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.ComponentModel;
    using System.Drawing;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Controls.Primitives;
    using System.Windows.Input;
    using System.Windows.Threading;
    using Innotive.InnoWatch.Commons.CameraController;
    using Innotive.InnoWatch.Commons.CameraController.CameraControlInfos;
    using Innotive.InnoWatch.Commons.CameraController.PresetSequence;
    using Innotive.InnoWatch.Commons.Utils;
    using Innotive.InnoWatch.CommonUtils.Log;
    using Innotive.InnoWatch.DLLs.CameraControls.PTZOperation;
    using Point = System.Windows.Point;

    /// <summary>
    /// Interaction logic for PTZControl.xaml.
    /// </summary>
    public partial class PTZControl : IDisposable, INotifyPropertyChanged
    {
        #region Constants and Fields

        public static readonly RoutedEvent MousePressedEvent = EventManager.RegisterRoutedEvent(
            "MousePressed", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(PTZControl));

        private readonly List<ToggleButton> _toggleButtonGroupList = new List<ToggleButton>();

        private DispatcherTimer _mousePressTimer;

        private static Cursor _pantilt_down;
        private static Cursor _pantilt_down_click;
        private static Cursor _pantilt_left;
        private static Cursor _pantilt_left_click;
        private static Cursor _pantilt_leftDown;
        private static Cursor _pantilt_leftDown_click;
        private static Cursor _pantilt_leftUp;
        private static Cursor _pantilt_leftUp_click;
        private static Cursor _pantilt_right;
        private static Cursor _pantilt_right_click;
        private static Cursor _pantilt_rightDown;
        private static Cursor _pantilt_rightDown_click;
        private static Cursor _pantilt_rightUp;
        private static Cursor _pantilt_rightUp_click;
        private static Cursor _pantilt_up;
        private static Cursor _pantilt_up_click;

        private static Cursor _zoom_normal;
        private static Cursor _zoom_zoomIn_click;
        private static Cursor _zoom_zoomOut_click;

        private static Cursor _focus_far_click;
        private static Cursor _focus_near_click;
        private static Cursor _focus_normal;

        private static Cursor _iris_open_normal;
        private static Cursor _iris_close_normal;
        private static Cursor _iris_open_click;
        private static Cursor _iris_close_click;

        //Power, Preset, Audio는 Cursor가 필요없음 !!

        private string _parentCameraID = string.Empty;

        private PTZStates _ptzState = PTZStates.None;
        private PtzControlCommand _panTiltState;
        private PtzControlCommand _zoomState;
        private PtzControlCommand _focusState;
        private PtzControlCommand _irisState;

        private OperatingManager operatingManager = null;

        /// <summary>
        /// The selected Camera Id.
        /// </summary>
        private string selectedCameraId = string.Empty;

        /// <summary>
        /// The whether to use preset.
        /// </summary>
        private bool usePreset = false;

        /// <summary>
        /// The wheather to use Iris.
        /// </summary>
        private bool useIris = false;

        /// <summary>
        /// The whether to use CameraPower.
        /// </summary>
        private bool useCameraPower = false;

        /// <summary>
        /// The whether to use CameraLight.
        /// </summary>
        private bool useCameraLight = false;

        /// <summary>
        /// The whether to use Aux1.
        /// </summary>
        private bool useAux1 = false;

        /// <summary>
        /// The whether to use Aux2.
        /// </summary>
        private bool useAux2 = false;

        /// <summary>
        /// The whether to use Focus.
        /// </summary>
        private bool useFocus = false;

        /// <summary>
        /// The whether to use PanTilt.
        /// </summary>
        private bool usePanTilt = false;

        /// <summary>
        /// The whether to use Zoom.
        /// </summary>
        private bool useZoom = false;

        /// <summary>
        /// The whether to use PanTiltSpeed.
        /// </summary>
        private bool usePanTiltSpeed = false;

        /// <summary>
        /// The whether to use Mic.
        /// </summary>
        private bool useMic = false;

        /// <summary>
        /// The whether to use Speaker.
        /// </summary>
        private bool useSpeaker = false;

        /// <summary>
        /// The whether to use Audio Panel.
        /// </summary>
        private bool useAudio = false;

        /// <summary>
        /// The whether to use Power.
        /// </summary>
        private bool usePower = false;

        /// <summary>
        /// Current PTZInformation.
        /// </summary>
        private PTZInformation ptzInformation;

        /// <summary>
        /// MouseWheel Zoom을 자동으로 멈추기 위한 Timer.
        /// </summary>
        private Timer autostopTimer;

        /// <summary>
        /// Current Preset Source.
        /// </summary>
        private ObservableCollection<string> presetSource = new ObservableCollection<string>();

        //PTZControl쪽 UI가 눌려졌음. MultiGrid에서 Selection을 처리하기 위함 !!
        internal event EventHandler<EventArgs> ePTZControlUIClicked;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="PTZControl"/> class.
        /// </summary>
        /// <param name="parentCameraID">
        /// The parent camera id.
        /// </param>
        public PTZControl(string parentCameraID)
        {
            this.InitializeComponent();

            this._parentCameraID = parentCameraID;

            // Toggle버튼을 하나의 그룹으로 묶어서 RadioButton 처럼 하나만 클릭이 될 수 있도록 한다. 
            this._toggleButtonGroupList.Clear();

            this._toggleButtonGroupList.Add(this.xPTZ_PanTiltButton);
            this._toggleButtonGroupList.Add(this.xPTZ_ZoomButton);
            this._toggleButtonGroupList.Add(this.xPTZ_FocusButton);
            this._toggleButtonGroupList.Add(this.xPTZ_IrisButton);
            this._toggleButtonGroupList.Add(this.xPTZ_PowerButton);
            this._toggleButtonGroupList.Add(this.xPTZ_PresetButton);
            this._toggleButtonGroupList.Add(this.xPTZ_AudioButton);

            this.SetCursors();

            this.Loaded += this.PTZControl_Loaded;
            this.Unloaded += this.PTZControl_Unloaded;
            this.SizeChanged += this.PTZControl_SizeChanged;

            this.operatingManager = new OperatingManager(this._parentCameraID, this);

            this.InitPTZInformation();

            this.autostopTimer = new Timer(OpticalZoomStopFromMouseWheel);
        }

        private void InitPTZInformation()
        {
            this.PtzInformation = null;

            if (CameraControlInfoExecution.Instance.CameraControlInfo != null)
                this.PtzInformation = CameraControlInfoExecution.Instance.CameraControlInfo.GetPtzInformation(this._parentCameraID);

            if (this.PtzInformation == null)
            {
                InnotiveDebug.Log.Error(string.Format("[PTZ Setting Error] CameraController 정보에 등록되지 않은 카메라입니다."));
                return;
            }

            this.SelectedCameraId = this.PtzInformation.ContentsDefineId;

            this.UseAux1 = this.PtzInformation.UseAux1Power;
            this.UseAux2 = this.PtzInformation.UseAux2Power;
            this.UseCameraLight = this.PtzInformation.UseLightPower;
            this.UseCameraPower = this.PtzInformation.UseCameraPower;
            this.UsePower = this.UseAux1 || this.UseAux2 || this.UseCameraLight || this.UseCameraPower;

            this.UsePreset = this.PtzInformation.UsePreset;

            this.UseIris = this.PtzInformation.UseIris;

            this.UseFocus = this.PtzInformation.UseFocus;

            this.UsePanTilt = this.PtzInformation.UsePanTilt;
            this.UsePanTiltSpeed = this.PtzInformation.UsePTZSpeed;

            this.UseZoom = this.PtzInformation.UseZoom;

            this.UseAudio = this.PtzInformation.AudioMode != 5;
            this.UseMic = this.PtzInformation.AudioMode != 4;
            this.xToggleButtonMicOnOff.IsEnabled = this.UseMic;
            this.UseSpeaker = this.PtzInformation.AudioMode != 3;
            this.xToggleButtonSpeakerOnOff.IsEnabled = this.UseSpeaker;
        }

        void PTZControl_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            this.UpdatePtzControlSize();
        }

        private void PTZControl_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= this.PTZControl_Loaded;

            this.xPTZ_MainGrid.MouseLeftButtonDown += xPTZ_MainGrid_MouseLeftButtonDown;
            //this.xPTZ_MainGrid.PreviewMouseLeftButtonDown += xPTZ_MainGrid_PreviewMouseLeftButtonDown;
            this.PreviewMouseLeftButtonDown += this.PTZControl_PreviewMouseLeftButtonDown;
            this.PreviewMouseLeftButtonUp += this.PTZControl_PreviewMouseLeftButtonUp;

            this.PreviewStylusUp += this.PTZControl_PreviewStylusUp;

            this.MousePressed += this.PTZControl_MousePressed;
            this.StylusUp += this.PTZControl_StylusUp;
            this.StylusLeave += this.PTZControl_StylusLeave;
            this.MouseEnter += this.PTZControl_MouseEnter;
            this.MouseLeave += this.PTZControl_MouseLeave;

            this.xPTZ_PanTiltButton.Click += this.xPTZ_MainControlButton_Clicked;
            this.xPTZ_ZoomButton.Click += this.xPTZ_MainControlButton_Clicked;
            this.xPTZ_FocusButton.Click += this.xPTZ_MainControlButton_Clicked;
            this.xPTZ_IrisButton.Click += this.xPTZ_MainControlButton_Clicked;
            this.xPTZ_PowerButton.Click += this.xPTZ_MainControlButton_Clicked;
            this.xPTZ_PresetButton.Click += this.xPTZ_MainControlButton_Clicked;
            this.xPTZ_AudioButton.Click += this.xPTZ_MainControlButton_Clicked;

            this.xPTZ_PanTiltButton.StylusUp += this.xPTZ_MainControlButton_StylusUp;
            this.xPTZ_ZoomButton.StylusUp += this.xPTZ_MainControlButton_StylusUp;
            this.xPTZ_FocusButton.StylusUp += this.xPTZ_MainControlButton_StylusUp;
            this.xPTZ_IrisButton.StylusUp += this.xPTZ_MainControlButton_StylusUp;
            this.xPTZ_PowerButton.StylusUp += this.xPTZ_MainControlButton_StylusUp;
            this.xPTZ_PresetButton.StylusUp += this.xPTZ_MainControlButton_StylusUp;
            this.xPTZ_AudioButton.StylusUp += this.xPTZ_MainControlButton_StylusUp;

            this.xPTZ_Region_LeftUp.MouseEnter += this.xPTZ_Region_MouseEnter;
            this.xPTZ_Region_Up.MouseEnter += this.xPTZ_Region_MouseEnter;
            this.xPTZ_Region_RightUp.MouseEnter += this.xPTZ_Region_MouseEnter;
            this.xPTZ_Region_Left.MouseEnter += this.xPTZ_Region_MouseEnter;
            this.xPTZ_Region_Right.MouseEnter += this.xPTZ_Region_MouseEnter;
            this.xPTZ_Region_LeftDown.MouseEnter += this.xPTZ_Region_MouseEnter;
            this.xPTZ_Region_Down.MouseEnter += this.xPTZ_Region_MouseEnter;
            this.xPTZ_Region_RightDown.MouseEnter += this.xPTZ_Region_MouseEnter;
            this.xPTZ_Region_Center_Left.MouseEnter += this.xPTZ_Region_MouseEnter;
            this.xPTZ_Region_Center_Up.MouseEnter += this.xPTZ_Region_MouseEnter;
            this.xPTZ_Region_Center_Right.MouseEnter += this.xPTZ_Region_MouseEnter;
            this.xPTZ_Region_Center_Down.MouseEnter += this.xPTZ_Region_MouseEnter;
            this.xPTZ_Region_UpDown_Up.MouseEnter += this.xPTZ_Region_UpDown_MouseEnter;
            this.xPTZ_Region_UpDown_Down.MouseEnter += this.xPTZ_Region_UpDown_MouseEnter;

            this.xPTZ_Region_LeftUp.StylusEnter += this.xPTZ_Region_StylusEnter;
            this.xPTZ_Region_Up.StylusEnter += this.xPTZ_Region_StylusEnter;
            this.xPTZ_Region_RightUp.StylusEnter += this.xPTZ_Region_StylusEnter;
            this.xPTZ_Region_Left.StylusEnter += this.xPTZ_Region_StylusEnter;
            this.xPTZ_Region_Right.StylusEnter += this.xPTZ_Region_StylusEnter;
            this.xPTZ_Region_LeftDown.StylusEnter += this.xPTZ_Region_StylusEnter;
            this.xPTZ_Region_Down.StylusEnter += this.xPTZ_Region_StylusEnter;
            this.xPTZ_Region_RightDown.StylusEnter += this.xPTZ_Region_StylusEnter;
            this.xPTZ_Region_Center_Left.StylusEnter += this.xPTZ_Region_StylusEnter;
            this.xPTZ_Region_Center_Up.StylusEnter += this.xPTZ_Region_StylusEnter;
            this.xPTZ_Region_Center_Right.StylusEnter += this.xPTZ_Region_StylusEnter;
            this.xPTZ_Region_Center_Down.StylusEnter += this.xPTZ_Region_StylusEnter;
            this.xPTZ_Region_UpDown_Up.StylusEnter += this.xPTZ_Region_UpDown_StylusEnter;
            this.xPTZ_Region_UpDown_Down.StylusEnter += this.xPTZ_Region_UpDown_StylusEnter;

            this.xSelectPresetButton.MouseEnter += this.xSelectPresetSequence_MouseEnter;
            this.xSelectSequenceButton.MouseEnter += this.xSelectPresetSequence_MouseEnter;
            this.xSelectPresetButton.StylusUp += this.xSelectPresetButton_StylusUp;
            this.xSelectSequenceButton.StylusUp += this.xSelectSequenceButton_StylusUp;
            this.xComboBoxPreset.StylusUp += this.xComboBoxPreset_StylusUp;
            this.xComboBoxSequence.StylusUp += this.xComboBoxSequence_StylusUp;
            
            //첫번째 Button이 Click된 상태로 보이게 함 !!
            this.InitUI();

            // Mic/Speaker 이벤트 연결.
            this.SetAudioEventHandler();
        }

        /// <summary>
        /// Mic/Speaker 버튼 클릭 시 발생하는 Event를 연결한다.
        /// </summary>
        /// <param name="mic">Mic Toggle button.</param>
        /// <param name="speaker">Speaker Toggle Button.</param>
        private void SetAudioEventHandler()
        {
            AudioManager.Instance.PrepareRecord += AudioManagerInstance_PrepareRecord;
            AudioManager.Instance.Recording += AudioManagerInstance_Recording;
            AudioManager.Instance.PrepareStopRecording += AudioManagerInstance_PrepareStopRecording;
            AudioManager.Instance.StopRecording += AudioManagerInstance_StopRecording;
            AudioManager.Instance.PreparePlay += AudioManagerInstance_PreparePlay;
            AudioManager.Instance.Playing += AudioManagerInstance_Playing;
            AudioManager.Instance.PrepareStopPlaying += AudioManagerInstance_PrepareStopPlaying;
            AudioManager.Instance.StopPlaying += AudioManagerInstance_StopPlaying;

            /*
            // Mic 버튼 클릭 시.
            AudioManager.Instance.PrepareRecord += (send, ea) => this.Dispatcher.BeginInvoke(new Action(() =>
            {
                SetAudioToggleButton(mic, false, false);
                speaker.IsEnabled = this.PtzInformation.AudioMode == 2 ? false : this.UseSpeaker;
            }));
            AudioManager.Instance.Recording += (send, ea) => this.Dispatcher.BeginInvoke(new Action(() => SetAudioToggleButton(mic, true, true)));
            AudioManager.Instance.PrepareStopRecording += (send, ea) => this.Dispatcher.BeginInvoke(new Action(() => SetAudioToggleButton(mic, false, true)));
            AudioManager.Instance.StopRecording += (send, ea) => this.Dispatcher.BeginInvoke(new Action(() =>
            {
                SetAudioToggleButton(mic, true, false);
                speaker.IsEnabled = this.PtzInformation.AudioMode == 2 ? true : this.UseSpeaker;
            }));

            // Speaker 버튼 클릭 시.
            AudioManager.Instance.PreparePlay += (send, ea) => this.Dispatcher.BeginInvoke(new Action(() =>
            {
                SetAudioToggleButton(speaker, false, false);
                mic.IsEnabled = this.PtzInformation.AudioMode == 2 ? false : this.UseMic;
            }));
            AudioManager.Instance.Playing += (send, ea) => this.Dispatcher.BeginInvoke(new Action(() => SetAudioToggleButton(speaker, true, true)));
            AudioManager.Instance.PrepareStopPlaying += (send, ea) => this.Dispatcher.BeginInvoke(new Action(() => SetAudioToggleButton(speaker, false, true)));
            AudioManager.Instance.StopPlaying += (send, ea) => this.Dispatcher.BeginInvoke(new Action(() =>
            {
                SetAudioToggleButton(speaker, true, false);
                mic.IsEnabled = this.PtzInformation.AudioMode == 2 ? true : this.UseMic;
            }));
            */
        }

        // Mic 버튼 클릭 시.
        private void AudioManagerInstance_PrepareRecord(object sender, EventArgs e)
        {
            ToggleButton mic = this.xToggleButtonMicOnOff;
            ToggleButton speaker = this.xToggleButtonSpeakerOnOff;
            this.Dispatcher.BeginInvoke(new Action(() =>
            {
                SetAudioToggleButton(this.xToggleButtonMicOnOff, false, false);
                speaker.IsEnabled = this.PtzInformation.AudioMode == 2 ? false : this.UseSpeaker;
            }));
        }
        private void AudioManagerInstance_Recording(object sender, EventArgs e)
        {
            ToggleButton mic = this.xToggleButtonMicOnOff;
            ToggleButton speaker = this.xToggleButtonSpeakerOnOff;
            this.Dispatcher.BeginInvoke(new Action(() => SetAudioToggleButton(mic, true, true)));
        }
        private void AudioManagerInstance_PrepareStopRecording(object sender, EventArgs e)
        {
            ToggleButton mic = this.xToggleButtonMicOnOff;
            ToggleButton speaker = this.xToggleButtonSpeakerOnOff;
            this.Dispatcher.BeginInvoke(new Action(() => SetAudioToggleButton(mic, false, true)));
        }
        private void AudioManagerInstance_StopRecording(object sender, EventArgs e)
        {
            ToggleButton mic = this.xToggleButtonMicOnOff;
            ToggleButton speaker = this.xToggleButtonSpeakerOnOff;
            this.Dispatcher.BeginInvoke(new Action(() =>
            {
                SetAudioToggleButton(mic, true, false);
                speaker.IsEnabled = this.PtzInformation.AudioMode == 2 ? true : this.UseSpeaker;
            }));
        }
        // Speaker 버튼 클릭 시.
        private void AudioManagerInstance_PreparePlay(object sender, EventArgs e)
        {
            ToggleButton mic = this.xToggleButtonMicOnOff;
            ToggleButton speaker = this.xToggleButtonSpeakerOnOff;
            this.Dispatcher.BeginInvoke(new Action(() =>
            {
                SetAudioToggleButton(speaker, false, false);
                mic.IsEnabled = this.PtzInformation.AudioMode == 2 ? false : this.UseMic;
            }));
        }
        private void AudioManagerInstance_Playing(object sender, EventArgs e)
        {
            ToggleButton mic = this.xToggleButtonMicOnOff;
            ToggleButton speaker = this.xToggleButtonSpeakerOnOff;
            this.Dispatcher.BeginInvoke(new Action(() => SetAudioToggleButton(speaker, true, true)));
        }
        private void AudioManagerInstance_PrepareStopPlaying(object sender, EventArgs e)
        {
            ToggleButton mic = this.xToggleButtonMicOnOff;
            ToggleButton speaker = this.xToggleButtonSpeakerOnOff;
            this.Dispatcher.BeginInvoke(new Action(() => SetAudioToggleButton(speaker, false, true)));
        }
        private void AudioManagerInstance_StopPlaying(object sender, EventArgs e)
        {
            ToggleButton mic = this.xToggleButtonMicOnOff;
            ToggleButton speaker = this.xToggleButtonSpeakerOnOff;
            this.Dispatcher.BeginInvoke(new Action(() =>
            {
                SetAudioToggleButton(speaker, true, false);
                mic.IsEnabled = this.PtzInformation.AudioMode == 2 ? true : this.UseMic;
            }));
        }

        private void ReleaseAudioEventHandler()
        {
            AudioManager.Instance.PrepareRecord -= AudioManagerInstance_PrepareRecord;
            AudioManager.Instance.Recording -= AudioManagerInstance_Recording;
            AudioManager.Instance.PrepareStopRecording -= AudioManagerInstance_PrepareStopRecording;
            AudioManager.Instance.StopRecording -= AudioManagerInstance_StopRecording;
            AudioManager.Instance.PreparePlay -= AudioManagerInstance_PreparePlay;
            AudioManager.Instance.Playing -= AudioManagerInstance_Playing;
            AudioManager.Instance.PrepareStopPlaying -= AudioManagerInstance_PrepareStopPlaying;
            AudioManager.Instance.StopPlaying -= AudioManagerInstance_StopPlaying;
        }

        //전체 UI 초기화. 제어버튼이 눌린 경우 전체 UI를 무조건 초기화 함 !!
        public void InitUI()
        {
            this.SetToggleButtonGroupState(this.xPTZ_PanTiltButton, false);
            this.SetToggleButtonGroupState(this.xPTZ_ZoomButton, false);
            this.SetToggleButtonGroupState(this.xPTZ_FocusButton, false);
            this.SetToggleButtonGroupState(this.xPTZ_IrisButton, false);
            this.SetToggleButtonGroupState(this.xPTZ_PowerButton, false);
            this.SetToggleButtonGroupState(this.xPTZ_PresetButton, false);
            this.SetToggleButtonGroupState(this.xPTZ_AudioButton, false);
        }

        // Audio Toggle 버튼의 상태를 변경함.
        private void SetAudioToggleButton(ToggleButton button, bool isEnabled, bool isChecked)
        {
            button.IsEnabled = isEnabled;
            button.IsChecked = isChecked;
        }

        private void xComboBoxSequence_StylusUp(object sender, StylusEventArgs e)
        {
            this.xComboBoxSequence.IsDropDownOpen = true;
        }

        private void xComboBoxPreset_StylusUp(object sender, StylusEventArgs e)
        {
            this.xComboBoxPreset.IsDropDownOpen = true;
        }

        private void xSelectSequenceButton_StylusUp(object sender, StylusEventArgs e)
        {
            var button = sender as RadioButton;

            if (button == null)
                return;

            button.Focus();
            button.IsChecked = true;
        }

        private void xSelectPresetButton_StylusUp(object sender, StylusEventArgs e)
        {
            var button = sender as RadioButton;

            if (button == null)
                return;

            button.Focus();
            button.IsChecked = true;
        }

        private void xSelectPresetSequence_MouseEnter(object sender, MouseEventArgs e)
        {
            var button = sender as RadioButton;

            if (button == null)
                return;

            button.Focus();
        }

        //PTZControl에서 MouseDown시 무조건 Cell의 빨간색 테두리가 보여지게 함
        void PTZControl_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            // ControlPanel을 클릭 했을 경우 return.
            var hitControl = e.OriginalSource as FrameworkElement;
            if (hitControl == null)
                return;

            var speed = ((int)this.xSliderPtzSpeed.Value).ToString();
            if (this._ptzState == PTZStates.PanTilt)
            {
                if (!(string.CompareOrdinal(hitControl.Name, "xPTZ_Region_LeftUp") == 0 ||
                    string.CompareOrdinal(hitControl.Name, "xPTZ_Region_Up") == 0 ||
                    string.CompareOrdinal(hitControl.Name, "xPTZ_Region_RightUp") == 0 ||
                    string.CompareOrdinal(hitControl.Name, "xPTZ_Region_Left") == 0 ||
                    string.CompareOrdinal(hitControl.Name, "xPTZ_Region_Right") == 0 ||
                    string.CompareOrdinal(hitControl.Name, "xPTZ_Region_LeftDown") == 0 ||
                    string.CompareOrdinal(hitControl.Name, "xPTZ_Region_Down") == 0 ||
                    string.CompareOrdinal(hitControl.Name, "xPTZ_Region_RightDown") == 0 ||
                    string.CompareOrdinal(hitControl.Name, "xPTZ_Region_Center_Up") == 0 ||
                    string.CompareOrdinal(hitControl.Name, "xPTZ_Region_Center_Left") == 0 ||
                    string.CompareOrdinal(hitControl.Name, "xPTZ_Region_Center_Right") == 0 ||
                    string.CompareOrdinal(hitControl.Name, "xPTZ_Region_Center_Down") == 0))
                {
                    InnotiveDebug.Trace(3, "[shwlee] PTZ Up Source : {0}", e.OriginalSource);
                    return;
                }

                InnotiveDebug.Trace(3, "[shwlee] PTZ Down Source : {0}", e.OriginalSource);
                // region 을 체크 해서 마우스 커서를 바꾸어 주고 카메라가 저장하고 있는 위치 타입을 결정해준다. 
                switch (this._panTiltState)
                {
                    case PtzControlCommand.LeftUp:
                        this.operatingManager.RequestPTZCommand(PtzControlCommand.LeftUp_Start, speed);
                        break;
                    case PtzControlCommand.LeftDown:
                        this.operatingManager.RequestPTZCommand(PtzControlCommand.LeftDown_Start, speed);
                        break;
                    case PtzControlCommand.RightDown:
                        this.operatingManager.RequestPTZCommand(PtzControlCommand.RightDown_Start, speed);
                        break;
                    case PtzControlCommand.RightUp:
                        this.operatingManager.RequestPTZCommand(PtzControlCommand.RightUp_Start, speed);
                        break;
                    case PtzControlCommand.Left:
                        this.operatingManager.RequestPTZCommand(PtzControlCommand.Left_Start, speed);
                        break;
                    case PtzControlCommand.Right:
                        this.operatingManager.RequestPTZCommand(PtzControlCommand.Right_Start, speed);
                        break;
                    case PtzControlCommand.Up:
                        this.operatingManager.RequestPTZCommand(PtzControlCommand.Up_Start, speed);
                        break;
                    case PtzControlCommand.Down:
                        this.operatingManager.RequestPTZCommand(PtzControlCommand.Down_Start, speed);
                        break;
                }

                this.RaisePTZControlUIClickedEvent();
            }
            else if (this._ptzState == PTZStates.Zoom)
            {
                if (!(string.CompareOrdinal(hitControl.Name, "xPTZ_Region_UpDown_Up") == 0 ||
                    string.CompareOrdinal(hitControl.Name, "xPTZ_Region_UpDown_Down") == 0))
                {
                    return;
                }

                switch (this._zoomState)
                {
                    case PtzControlCommand.ZoomIn:
                        this.operatingManager.RequestPTZCommand(PtzControlCommand.ZoomIn_Start, speed);
                        break;
                    case PtzControlCommand.ZoomOut:
                        this.operatingManager.RequestPTZCommand(PtzControlCommand.ZoomOut_Start, speed);
                        break;
                }

                this.RaisePTZControlUIClickedEvent();
            }
            else if (this._ptzState == PTZStates.Focus)
            {
                if (!(string.CompareOrdinal(hitControl.Name, "xPTZ_Region_UpDown_Up") == 0 ||
                    string.CompareOrdinal(hitControl.Name, "xPTZ_Region_UpDown_Down") == 0))
                {
                    return;
                }

                switch (this._focusState)
                {
                    case PtzControlCommand.FocusFar:
                        this.operatingManager.RequestPTZCommand(PtzControlCommand.FocusFar_Start, speed);
                        break;
                    case PtzControlCommand.FocusNear:
                        this.operatingManager.RequestPTZCommand(PtzControlCommand.FocusNear_Start, speed);
                        break;
                }

                this.RaisePTZControlUIClickedEvent();
            }
            else if (this._ptzState == PTZStates.Iris)
            {
                if (!(string.CompareOrdinal(hitControl.Name, "xPTZ_Region_UpDown_Up") == 0 ||
                    string.CompareOrdinal(hitControl.Name, "xPTZ_Region_UpDown_Down") == 0))
                {
                    return;
                }

                switch (this._irisState)
                {
                    case PtzControlCommand.IrisOpen:
                        this.operatingManager.RequestPTZCommand(PtzControlCommand.IrisOpen_Start, speed);
                        break;
                    case PtzControlCommand.IrisClose:
                        this.operatingManager.RequestPTZCommand(PtzControlCommand.IrisClose_Start, speed);
                        break;
                }

                this.RaisePTZControlUIClickedEvent();
            }
        }
        
        void PTZControl_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            // ControlPanel을 클릭 했을 경우 return.
            var hitControl = e.OriginalSource as FrameworkElement;
            if (hitControl == null)
                return;

            var speed = ((int)this.xSliderPtzSpeed.Value).ToString();
            if (this._ptzState == PTZStates.PanTilt)
            {
                if (!(string.CompareOrdinal(hitControl.Name, "xPTZ_Region_LeftUp") == 0 ||
                    string.CompareOrdinal(hitControl.Name, "xPTZ_Region_Up") == 0 ||
                    string.CompareOrdinal(hitControl.Name, "xPTZ_Region_RightUp") == 0 ||
                    string.CompareOrdinal(hitControl.Name, "xPTZ_Region_Left") == 0 ||
                    string.CompareOrdinal(hitControl.Name, "xPTZ_Region_Right") == 0 ||
                    string.CompareOrdinal(hitControl.Name, "xPTZ_Region_LeftDown") == 0 ||
                    string.CompareOrdinal(hitControl.Name, "xPTZ_Region_Down") == 0 ||
                    string.CompareOrdinal(hitControl.Name, "xPTZ_Region_RightDown") == 0 ||
                    string.CompareOrdinal(hitControl.Name, "xPTZ_Region_Center_Up") == 0 ||
                    string.CompareOrdinal(hitControl.Name, "xPTZ_Region_Center_Left") == 0 ||
                    string.CompareOrdinal(hitControl.Name, "xPTZ_Region_Center_Right") == 0 ||
                    string.CompareOrdinal(hitControl.Name, "xPTZ_Region_Center_Down") == 0))
                {
                    InnotiveDebug.Trace(3, "[shwlee] PTZ Up Source : {0}", e.OriginalSource);
                    return;
                }

                InnotiveDebug.Trace(3, "[shwlee] PTZ Up Source : {0}", e.OriginalSource);

                // region 을 체크 해서 마우스 커서를 바꾸어 주고 카메라가 저장하고 있는 위치 타입을 결정해준다. 
                switch (this._panTiltState)
                {
                    case PtzControlCommand.LeftUp:
                        this.operatingManager.RequestPTZCommand(PtzControlCommand.LeftUp_End, speed);
                        break;
                    case PtzControlCommand.LeftDown:
                        this.operatingManager.RequestPTZCommand(PtzControlCommand.LeftDown_End, speed);
                        break;
                    case PtzControlCommand.RightDown:
                        this.operatingManager.RequestPTZCommand(PtzControlCommand.RightDown_End, speed);
                        break;
                    case PtzControlCommand.RightUp:
                        this.operatingManager.RequestPTZCommand(PtzControlCommand.RightUp_End, speed);
                        break;
                    case PtzControlCommand.Left:
                        this.operatingManager.RequestPTZCommand(PtzControlCommand.Left_End, speed);
                        break;
                    case PtzControlCommand.Right:
                        this.operatingManager.RequestPTZCommand(PtzControlCommand.Right_End, speed);
                        break;
                    case PtzControlCommand.Up:
                        this.operatingManager.RequestPTZCommand(PtzControlCommand.Up_End, speed);
                        break;
                    case PtzControlCommand.Down:
                        this.operatingManager.RequestPTZCommand(PtzControlCommand.Down_End, speed);
                        break;
                }
            }
            else if (this._ptzState == PTZStates.Zoom)
            {
                if (!(string.CompareOrdinal(hitControl.Name, "xPTZ_Region_UpDown_Up") == 0 ||
                    string.CompareOrdinal(hitControl.Name, "xPTZ_Region_UpDown_Down") == 0))
                {
                    return;
                }

                switch (this._zoomState)
                {
                    case PtzControlCommand.ZoomIn:
                        this.operatingManager.RequestPTZCommand(PtzControlCommand.ZoomIn_End, speed);
                        break;
                    case PtzControlCommand.ZoomOut:
                        this.operatingManager.RequestPTZCommand(PtzControlCommand.ZoomOut_End, speed);
                        break;
                }
            }
            else if (this._ptzState == PTZStates.Focus)
            {
                switch (this._focusState)
                {
                    case PtzControlCommand.FocusFar:
                        this.operatingManager.RequestPTZCommand(PtzControlCommand.FocusFar_End, speed);
                        break;
                    case PtzControlCommand.FocusNear:
                        this.operatingManager.RequestPTZCommand(PtzControlCommand.FocusNear_End, speed);
                        break;
                }
            }
            else if (this._ptzState == PTZStates.Iris)
            {

                if (!(string.CompareOrdinal(hitControl.Name, "xPTZ_Region_UpDown_Up") == 0 ||
                    string.CompareOrdinal(hitControl.Name, "xPTZ_Region_UpDown_Down") == 0))
                {
                    return;
                }

                switch (this._irisState)
                {
                    case PtzControlCommand.IrisOpen:
                        this.operatingManager.RequestPTZCommand(PtzControlCommand.IrisOpen_End, speed);
                        break;
                    case PtzControlCommand.IrisClose:
                        this.operatingManager.RequestPTZCommand(PtzControlCommand.IrisClose_End, speed);
                        break;
                }
            }
        }

        private void PTZControl_PreviewStylusUp(object sender, StylusEventArgs e)
        {
            // ControlPanel을 클릭 했을 경우 return.
            var hitControl = e.OriginalSource as FrameworkElement;
            if (hitControl == null)
                return;

            var speed = ((int)this.xSliderPtzSpeed.Value).ToString();
            if (this._ptzState == PTZStates.PanTilt)
            {
                if (!(string.CompareOrdinal(hitControl.Name, "xPTZ_Region_LeftUp") == 0 ||
                    string.CompareOrdinal(hitControl.Name, "xPTZ_Region_Up") == 0 ||
                    string.CompareOrdinal(hitControl.Name, "xPTZ_Region_RightUp") == 0 ||
                    string.CompareOrdinal(hitControl.Name, "xPTZ_Region_Left") == 0 ||
                    string.CompareOrdinal(hitControl.Name, "xPTZ_Region_Right") == 0 ||
                    string.CompareOrdinal(hitControl.Name, "xPTZ_Region_LeftDown") == 0 ||
                    string.CompareOrdinal(hitControl.Name, "xPTZ_Region_Down") == 0 ||
                    string.CompareOrdinal(hitControl.Name, "xPTZ_Region_RightDown") == 0 ||
                    string.CompareOrdinal(hitControl.Name, "xPTZ_Region_Center_Up") == 0 ||
                    string.CompareOrdinal(hitControl.Name, "xPTZ_Region_Center_Left") == 0 ||
                    string.CompareOrdinal(hitControl.Name, "xPTZ_Region_Center_Right") == 0 ||
                    string.CompareOrdinal(hitControl.Name, "xPTZ_Region_Center_Down") == 0))
                {
                    InnotiveDebug.Trace(3, "[shwlee] PTZ Up Source : {0}", e.OriginalSource);
                    return;
                }

                InnotiveDebug.Trace(3, "[shwlee] PTZ Up Source : {0}", e.OriginalSource);

                // region 을 체크 해서 마우스 커서를 바꾸어 주고 카메라가 저장하고 있는 위치 타입을 결정해준다. 
                switch (this._panTiltState)
                {
                    case PtzControlCommand.LeftUp:
                        this.operatingManager.RequestPTZCommand(PtzControlCommand.LeftUp_End, speed);
                        break;
                    case PtzControlCommand.LeftDown:
                        this.operatingManager.RequestPTZCommand(PtzControlCommand.LeftDown_End, speed);
                        break;
                    case PtzControlCommand.RightDown:
                        this.operatingManager.RequestPTZCommand(PtzControlCommand.RightDown_End, speed);
                        break;
                    case PtzControlCommand.RightUp:
                        this.operatingManager.RequestPTZCommand(PtzControlCommand.RightUp_End, speed);
                        break;
                    case PtzControlCommand.Left:
                        this.operatingManager.RequestPTZCommand(PtzControlCommand.Left_End, speed);
                        break;
                    case PtzControlCommand.Right:
                        this.operatingManager.RequestPTZCommand(PtzControlCommand.Right_End, speed);
                        break;
                    case PtzControlCommand.Up:
                        this.operatingManager.RequestPTZCommand(PtzControlCommand.Up_End, speed);
                        break;
                    case PtzControlCommand.Down:
                        this.operatingManager.RequestPTZCommand(PtzControlCommand.Down_End, speed);
                        break;
                }
            }
            else if (this._ptzState == PTZStates.Zoom)
            {
                if (!(string.CompareOrdinal(hitControl.Name, "xPTZ_Region_UpDown_Up") == 0 ||
                    string.CompareOrdinal(hitControl.Name, "xPTZ_Region_UpDown_Down") == 0))
                {
                    return;
                }

                switch (this._zoomState)
                {
                    case PtzControlCommand.ZoomIn:
                        this.operatingManager.RequestPTZCommand(PtzControlCommand.ZoomIn_End, speed);
                        break;
                    case PtzControlCommand.ZoomOut:
                        this.operatingManager.RequestPTZCommand(PtzControlCommand.ZoomOut_End, speed);
                        break;
                }
            }
            else if (this._ptzState == PTZStates.Focus)
            {
                switch (this._focusState)
                {
                    case PtzControlCommand.FocusFar:
                        this.operatingManager.RequestPTZCommand(PtzControlCommand.FocusFar_End, speed);
                        break;
                    case PtzControlCommand.FocusNear:
                        this.operatingManager.RequestPTZCommand(PtzControlCommand.FocusNear_End, speed);
                        break;
                }
            }
            else if (this._ptzState == PTZStates.Iris)
            {

                if (!(string.CompareOrdinal(hitControl.Name, "xPTZ_Region_UpDown_Up") == 0 ||
                    string.CompareOrdinal(hitControl.Name, "xPTZ_Region_UpDown_Down") == 0))
                {
                    return;
                }

                switch (this._irisState)
                {
                    case PtzControlCommand.IrisOpen:
                        this.operatingManager.RequestPTZCommand(PtzControlCommand.IrisOpen_End, speed);
                        break;
                    case PtzControlCommand.IrisClose:
                        this.operatingManager.RequestPTZCommand(PtzControlCommand.IrisClose_End, speed);
                        break;
                }
            }
        }

        //PTZControl UI에서 MouseDown시 Cell의 빨간색 테두리가 Toggle이 안되게 함
        void xPTZ_MainGrid_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;
        }

        public void OpticalZoomStopFromMouseWheel(object sender)
        {
            this.operatingManager.RequestPTZCommand(PtzControlCommand.ZoomStop, "0");
            lastdelta = 0;
        }

        private int lastdelta = -9999;
        public void OpticalZoomFromMouseWheel(int delta)
        {
            if (lastdelta == delta) // 중복이면 Stop 만 지연시키고 리턴.
            {
                autostopTimer.Change(400, Timeout.Infinite);
                return;
            }

            var speed = (int)Math.Sqrt(Math.Abs(delta)) / 5;

            var Zoomtype = delta > 0 ? PtzControlCommand.ZoomIn_Start : PtzControlCommand.ZoomOut_Start;

            this.operatingManager.RequestPTZCommand(Zoomtype, speed.ToString());

            autostopTimer.Change(400, Timeout.Infinite);

            lastdelta = delta;
        }

        //PTZControl에서 MouseDown시 무조건 Cell의 빨간색 테두리가 보여지게 함
        private void RaisePTZControlUIClickedEvent()
        {
            var tempEvent = this.ePTZControlUIClicked;
            if (tempEvent != null)
                tempEvent(this, new EventArgs());
        }

        private void PTZControl_Unloaded(object sender, RoutedEventArgs e)
        {
            this.Unloaded -= this.PTZControl_Unloaded;

            this.xPTZ_MainGrid.MouseLeftButtonDown -= xPTZ_MainGrid_MouseLeftButtonDown;
            this.PreviewMouseLeftButtonDown -= PTZControl_PreviewMouseLeftButtonDown;
            this.PreviewMouseLeftButtonUp -= PTZControl_PreviewMouseLeftButtonUp;

            this.PreviewStylusUp -= this.PTZControl_PreviewStylusUp;

            this.MousePressed -= this.PTZControl_MousePressed;
            this.StylusUp -= this.PTZControl_StylusUp;
            this.MouseEnter -= this.PTZControl_MouseEnter;
            this.MouseLeave -= this.PTZControl_MouseLeave;

            this.xPTZ_PanTiltButton.Click -= this.xPTZ_MainControlButton_Clicked;
            this.xPTZ_ZoomButton.Click -= this.xPTZ_MainControlButton_Clicked;
            this.xPTZ_FocusButton.Click -= this.xPTZ_MainControlButton_Clicked;
            this.xPTZ_IrisButton.Click -= this.xPTZ_MainControlButton_Clicked;
            this.xPTZ_PowerButton.Click -= this.xPTZ_MainControlButton_Clicked;
            this.xPTZ_PresetButton.Click -= this.xPTZ_MainControlButton_Clicked;
            this.xPTZ_AudioButton.Click -= this.xPTZ_MainControlButton_Clicked;

            this.xPTZ_PanTiltButton.StylusUp -= this.xPTZ_MainControlButton_StylusUp;
            this.xPTZ_ZoomButton.StylusUp -= this.xPTZ_MainControlButton_StylusUp;
            this.xPTZ_FocusButton.StylusUp -= this.xPTZ_MainControlButton_StylusUp;
            this.xPTZ_IrisButton.StylusUp -= this.xPTZ_MainControlButton_StylusUp;
            this.xPTZ_PowerButton.StylusUp -= this.xPTZ_MainControlButton_StylusUp;
            this.xPTZ_PresetButton.StylusUp -= this.xPTZ_MainControlButton_StylusUp;
            this.xPTZ_AudioButton.StylusUp -= this.xPTZ_MainControlButton_StylusUp;

            this.xPTZ_Region_LeftUp.MouseEnter -= this.xPTZ_Region_MouseEnter;
            this.xPTZ_Region_Up.MouseEnter -= this.xPTZ_Region_MouseEnter;
            this.xPTZ_Region_RightUp.MouseEnter -= this.xPTZ_Region_MouseEnter;
            this.xPTZ_Region_Left.MouseEnter -= this.xPTZ_Region_MouseEnter;
            this.xPTZ_Region_Right.MouseEnter -= this.xPTZ_Region_MouseEnter;
            this.xPTZ_Region_LeftDown.MouseEnter -= this.xPTZ_Region_MouseEnter;
            this.xPTZ_Region_Down.MouseEnter -= this.xPTZ_Region_MouseEnter;
            this.xPTZ_Region_RightDown.MouseEnter -= this.xPTZ_Region_MouseEnter;
            this.xPTZ_Region_Center_Left.MouseEnter -= this.xPTZ_Region_MouseEnter;
            this.xPTZ_Region_Center_Up.MouseEnter -= this.xPTZ_Region_MouseEnter;
            this.xPTZ_Region_Center_Right.MouseEnter -= this.xPTZ_Region_MouseEnter;
            this.xPTZ_Region_Center_Down.MouseEnter -= this.xPTZ_Region_MouseEnter;
            this.xPTZ_Region_UpDown_Up.MouseEnter -= this.xPTZ_Region_UpDown_MouseEnter;
            this.xPTZ_Region_UpDown_Down.MouseEnter -= this.xPTZ_Region_UpDown_MouseEnter;
            
            this.xPTZ_Region_LeftUp.StylusEnter -= this.xPTZ_Region_StylusEnter;
            this.xPTZ_Region_Up.StylusEnter -= this.xPTZ_Region_StylusEnter;
            this.xPTZ_Region_RightUp.StylusEnter -= this.xPTZ_Region_StylusEnter;
            this.xPTZ_Region_Left.StylusEnter -= this.xPTZ_Region_StylusEnter;
            this.xPTZ_Region_Right.StylusEnter -= this.xPTZ_Region_StylusEnter;
            this.xPTZ_Region_LeftDown.StylusEnter -= this.xPTZ_Region_StylusEnter;
            this.xPTZ_Region_Down.StylusEnter -= this.xPTZ_Region_StylusEnter;
            this.xPTZ_Region_RightDown.StylusEnter -= this.xPTZ_Region_StylusEnter;
            this.xPTZ_Region_Center_Left.StylusEnter -= this.xPTZ_Region_StylusEnter;
            this.xPTZ_Region_Center_Up.StylusEnter -= this.xPTZ_Region_StylusEnter;
            this.xPTZ_Region_Center_Right.StylusEnter -= this.xPTZ_Region_StylusEnter;
            this.xPTZ_Region_Center_Down.StylusEnter -= this.xPTZ_Region_StylusEnter;
            this.xPTZ_Region_UpDown_Up.StylusEnter -= this.xPTZ_Region_UpDown_StylusEnter;
            this.xPTZ_Region_UpDown_Down.StylusEnter -= this.xPTZ_Region_UpDown_StylusEnter;

            this.xSelectPresetButton.MouseEnter -= xSelectPresetSequence_MouseEnter;
            this.xSelectSequenceButton.MouseEnter -= xSelectPresetSequence_MouseEnter;

            // Mic/Speaker 이벤트 해제.
            this.ReleaseAudioEventHandler();
        }

        #endregion

        #region Delegates and Events

        /// <summary>
        /// mouse pressed.
        /// </summary>
        public event RoutedEventHandler MousePressed
        {
            add
            {
                this.AddHandler(MousePressedEvent, value);
            }

            remove
            {
                this.RemoveHandler(MousePressedEvent, value);
            }
        }

        #endregion

        #region Enums

        /// <summary>
        /// ptz states.
        /// </summary>
        public enum PTZStates
        {
            /// <summary>
            /// pan tilt.
            /// </summary>
            PanTilt,

            /// <summary>
            /// zoom.
            /// </summary>
            Zoom,

            /// <summary>
            /// focus.
            /// </summary>
            Focus,

            /// <summary>
            /// Iris.
            /// </summary>
            Iris,

            /// <summary>
            /// Audio.
            /// </summary>
            Audio,

            /// <summary>
            /// Power.
            /// </summary>
            Power,

            /// <summary>
            /// Preset.
            /// </summary>
            Preset,

            /// <summary>
            /// none.
            /// </summary>
            None,
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets PanTiltButton.
        /// </summary>
        public ToggleButton PanTiltButton
        {
            get
            {
                return this.xPTZ_PanTiltButton;
            }
        }

        /// <summary>
        /// Gets ZoomButton.
        /// </summary>
        public ToggleButton ZoomButton
        {
            get
            {
                return this.xPTZ_ZoomButton;
            }
        }

        /// <summary>
        /// Gets FocusButton.
        /// </summary>
        public ToggleButton FocusButton
        {
            get
            {
                return this.xPTZ_FocusButton;
            }
        }

        /// <summary>
        /// Gets IrisButton.
        /// </summary>
        public ToggleButton IrisButton
        {
            get
            {
                return this.xPTZ_IrisButton;
            }
        }

        /// <summary>
        /// Gets PowerButton.
        /// </summary>
        public ToggleButton PowerButton
        {
            get
            {
                return this.xPTZ_PowerButton;
            }
        }

        /// <summary>
        /// Gets PresetButton.
        /// </summary>
        public ToggleButton PresetButton
        {
            get
            {
                return this.xPTZ_PresetButton;
            }
        }

        /// <summary>
        /// Gets AudioButton.
        /// </summary>
        public ToggleButton AudioButton
        {
            get
            {
                return this.xPTZ_AudioButton;
            }
        }

        /// <summary>
        /// Gets or sets ParentCamreaID.
        /// </summary>
        public string ParentCamreaID
        {
            get
            {
                return this._parentCameraID;
            }

            set
            {
                this._parentCameraID = value;
            }
        }

        /// <summary>
        /// Gets or sets PTZState.
        /// </summary>
        public PTZStates PTZState
        {
            get
            {
                return this._ptzState;
            }

            set
            {
                if (value == PTZStates.None)
                {
                    this.xMainGrid.Opacity = 0;
                    foreach (ToggleButton buttton in this._toggleButtonGroupList)
                    {
                        buttton.IsChecked = false;
                    }

                    this.Cursor = Cursors.Arrow;
                }

                this._ptzState = value;
            }
        }

        public ObservableCollection<string> PresetSource
        {
            get
            {
                return this.presetSource;
            }
        }

        /// <summary>
        /// Gets or sets usePreset.
        /// </summary>
        public bool UsePreset
        {
            get
            {
                return this.usePreset;
            }
            set
            {
                this.usePreset = value;
                this.NotifyPropertyChanged("UsePreset");
            }
        }

        /// <summary>
        /// Gets or sets useIris.
        /// </summary>
        public bool UseIris
        {
            get
            {
                return this.useIris;
            }
            set
            {
                this.useIris = value;
                this.NotifyPropertyChanged("UseIris");
            }
        }

        /// <summary>
        /// Gets or sets useCameraPower.
        /// </summary>
        public bool UseCameraPower
        {
            get
            {
                return this.useCameraPower;
            }
            set
            {
                this.useCameraPower = value;
                this.NotifyPropertyChanged("UseCameraPower");
            }
        }

        /// <summary>
        /// Gets or sets useCameraLight.
        /// </summary>
        public bool UseCameraLight
        {
            get
            {
                return this.useCameraLight;
            }
            set
            {
                this.useCameraLight = value;
                this.NotifyPropertyChanged("UseCameraLight");
            }
        }

        /// <summary>
        /// Gets or sets useAux1.
        /// </summary>
        public bool UseAux1
        {
            get
            {
                return this.useAux1;
            }
            set
            {
                this.useAux1 = value;
                this.NotifyPropertyChanged("UseAux1");
            }
        }

        /// <summary>
        /// Gets or sets useAux2.
        /// </summary>
        public bool UseAux2
        {
            get
            {
                return this.useAux2;
            }
            set
            {
                this.useAux2 = value;
                this.NotifyPropertyChanged("UseAux2");
            }
        }

        /// <summary>
        /// Gets UsePowert.
        /// </summary>
        public bool UsePower
        {
            get
            {
                return this.usePower;
            }
            set
            {
                this.usePower = value;
                this.NotifyPropertyChanged("UsePower");
            }
        }

        /// <summary>
        /// Gets or sets useFocus.
        /// </summary>
        public bool UseFocus
        {
            get
            {
                return this.useFocus;
            }
            set
            {
                this.useFocus = value;
                this.NotifyPropertyChanged("UseFocus");
            }
        }

        /// <summary>
        /// Gets or sets usePanTilt.
        /// </summary>
        public bool UsePanTilt
        {
            get
            {
                return this.usePanTilt;
            }
            set
            {
                this.usePanTilt = value;
                this.NotifyPropertyChanged("UsePanTilt");
            }
        }

        /// <summary>
        /// Gets or sets useZoom.
        /// </summary>
        public bool UseZoom
        {
            get
            {
                return this.useZoom;
            }
            set
            {
                this.useZoom = value;
                this.NotifyPropertyChanged("UseZoom");
            }
        }

        /// <summary>
        /// Gets or sets usePanTiltSpeed.
        /// </summary>
        public bool UsePanTiltSpeed
        {
            get
            {
                return this.usePanTiltSpeed;
            }
            set
            {
                this.usePanTiltSpeed = value;
                this.NotifyPropertyChanged("UsePanTiltSpeed");
            }
        }

        /// <summary>
        /// Gets or sets useMic.
        /// </summary>
        public bool UseMic
        {
            get
            {
                return this.useMic;
            }
            set
            {
                this.useMic = value;
                this.NotifyPropertyChanged("UseMic");
            }
        }

        /// <summary>
        /// Gets or sets useSpeaker.
        /// </summary>
        public bool UseSpeaker
        {
            get
            {
                return this.useSpeaker;
            }
            set
            {
                this.useSpeaker = value;
                this.NotifyPropertyChanged("UseSpeaker");
            }
        }

        /// <summary>
        /// Gets or sets UseAudio.
        /// </summary>
        public bool UseAudio
        {
            get
            {
                return this.useAudio;
            }
            set
            {
                this.useAudio = value;
                this.NotifyPropertyChanged("UseAudio");
            }
        }

        /// <summary>
        /// Gets or sets ptzInformation.
        /// </summary>
        public PTZInformation PtzInformation
        {
            get
            {
                return this.ptzInformation;
            }
            set
            {
                this.ptzInformation = value;

                if (this.ptzInformation == null)
                {
                    //MessageBox.Show("from PTZControl PTZInformation이 null입니다.\r\nCameraController.Log에 존재하지 않는 Camera인지 확인해 주세요~");
                    return;
                }

                this.NotifyPropertyChanged("PtzInformation");
            }
        }

        /// <summary>
        /// Gets or sets selectedCameraId.
        /// </summary>
        public string SelectedCameraId
        {
            get
            {
                return this.selectedCameraId;
            }
            set
            {
                this.selectedCameraId = value;
                this.NotifyPropertyChanged("SelectedCameraId");
            }
        }

        #endregion

        #region Implemented Interfaces

        #region IDisposable

        /// <summary>
        /// dispose.
        /// </summary>
        public void Dispose()
        {
        }

        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// on mouse left button down.
        /// </summary>
        /// <param name="e">
        /// The e.
        /// </param>
        protected override void OnMouseLeftButtonDown(MouseButtonEventArgs e)
        {
            if (this._ptzState != PTZStates.None)
            {
                this.PTZControl_MousePressed(null, null);

                this.SetClickedMouseCursor();

                base.OnMouseLeftButtonDown(e);

                e.Handled = true;

                if (this._mousePressTimer == null)
                {
                    this._mousePressTimer = new DispatcherTimer(
                        new TimeSpan(0, 0, 0, 0, 300),
                        DispatcherPriority.Input,
                        this.RaiseMousePressedEvent,
                        this.Dispatcher);
                    this._mousePressTimer.Start();
                }
            }

            //Cell의 선택 해제가 되는걸 막음 !!
            e.Handled = true;
        }

        /// <summary>
        /// on mouse pressed.
        /// </summary>
        protected virtual void OnMousePressed()
        {
            this.RaiseMousePressedEvent();
        }

        private void RaiseMousePressedEvent()
        {
            var newEventArgs = new RoutedEventArgs(MousePressedEvent);
            this.RaiseEvent(newEventArgs);
        }

        private void SetClickedMouseCursor()
        {
            if (this._ptzState == PTZStates.PanTilt)
            {
                // region 을 체크 해서 마우스 커서를 바꾸어 주고 카메라가 저장하고 있는 위치 타입을 결정해준다. 
                if (this._panTiltState == PtzControlCommand.LeftUp)
                {
                    Mouse.OverrideCursor = _pantilt_leftUp_click;
                }
                else if (this._panTiltState == PtzControlCommand.LeftDown)
                {
                    Mouse.OverrideCursor = _pantilt_leftDown_click;
                }
                else if (this._panTiltState == PtzControlCommand.RightDown)
                {
                    Mouse.OverrideCursor = _pantilt_rightDown_click;
                }
                else if (this._panTiltState == PtzControlCommand.RightUp)
                {
                    Mouse.OverrideCursor = _pantilt_rightUp_click;
                }
                else if (this._panTiltState == PtzControlCommand.Left)
                {
                    Mouse.OverrideCursor = _pantilt_left_click;
                }
                else if (this._panTiltState == PtzControlCommand.Right)
                {
                    Mouse.OverrideCursor = _pantilt_right_click;
                }
                else if (this._panTiltState == PtzControlCommand.Up)
                {
                    Mouse.OverrideCursor = _pantilt_up_click;
                }
                else if (this._panTiltState == PtzControlCommand.Down)
                {
                    Mouse.OverrideCursor = _pantilt_down_click;
                }
            }
            else if (this._ptzState == PTZStates.Zoom)
            {
                if (this._zoomState == PtzControlCommand.ZoomIn)
                {
                    Mouse.OverrideCursor = _zoom_zoomIn_click;
                }
                else if (this._zoomState == PtzControlCommand.ZoomOut)
                {
                    Mouse.OverrideCursor = _zoom_zoomOut_click;
                }
            }
            else if (this._ptzState == PTZStates.Focus)
            {
                if (this._focusState == PtzControlCommand.FocusFar)
                {
                    Mouse.OverrideCursor = _focus_far_click;
                }
                else if (this._focusState == PtzControlCommand.FocusNear)
                {
                    Mouse.OverrideCursor = _focus_near_click;
                }
            }
            else if (this._ptzState == PTZStates.Iris)
            {
                if (this._irisState == PtzControlCommand.IrisOpen)
                {
                    Mouse.OverrideCursor = _iris_open_click;
                }
                else if (this._irisState == PtzControlCommand.IrisClose)
                {
                    Mouse.OverrideCursor = _iris_close_click;
                }
            }
        }

        private void SetCursors()
        {
            if (_pantilt_leftUp == null)
            {
                _pantilt_leftUp = CursorHelper.CreateCursorWithBitmap((Bitmap)Properties.Resources.leftUp, 5, 5);
            }

            if (_pantilt_leftDown == null)
            {
                _pantilt_leftDown = CursorHelper.CreateCursorWithBitmap((Bitmap)Properties.Resources.leftDown, 5, 5);
            }

            if (_pantilt_rightDown == null)
            {
                _pantilt_rightDown = CursorHelper.CreateCursorWithBitmap((Bitmap)Properties.Resources.rightDown, 5, 5);
            }

            if (_pantilt_rightUp == null)
            {
                _pantilt_rightUp = CursorHelper.CreateCursorWithBitmap((Bitmap)Properties.Resources.rightUp, 5, 5);
            }

            if (_pantilt_left == null)
            {
                _pantilt_left = CursorHelper.CreateCursorWithBitmap((Bitmap)Properties.Resources.left, 5, 5);
            }

            if (_pantilt_right == null)
            {
                _pantilt_right = CursorHelper.CreateCursorWithBitmap((Bitmap)Properties.Resources.right, 5, 5);
            }

            if (_pantilt_up == null)
            {
                _pantilt_up = CursorHelper.CreateCursorWithBitmap((Bitmap)Properties.Resources.up, 5, 5);
            }

            if (_pantilt_down == null)
            {
                _pantilt_down = CursorHelper.CreateCursorWithBitmap((Bitmap)Properties.Resources.down, 5, 5);
            }

            if (_focus_normal == null)
            {
                _focus_normal = CursorHelper.CreateCursorWithBitmap((Bitmap)Properties.Resources.focus_normal, 5, 5);
            }

            if (_zoom_normal == null)
            {
                _zoom_normal = CursorHelper.CreateCursorWithBitmap((Bitmap)Properties.Resources.zoom_normal, 5, 5);
            }

            if (_pantilt_leftUp_click == null)
            {
                _pantilt_leftUp_click = CursorHelper.CreateCursorWithBitmap((Bitmap)Properties.Resources.leftUp_click, 5, 5);
            }

            if (_pantilt_leftDown_click == null)
            {
                _pantilt_leftDown_click = CursorHelper.CreateCursorWithBitmap((Bitmap)Properties.Resources.leftDown_click, 5, 5);
            }

            if (_pantilt_rightDown_click == null)
            {
                _pantilt_rightDown_click = CursorHelper.CreateCursorWithBitmap((Bitmap)Properties.Resources.rightDown_click, 5, 5);
            }

            if (_pantilt_rightUp_click == null)
            {
                _pantilt_rightUp_click = CursorHelper.CreateCursorWithBitmap((Bitmap)Properties.Resources.rightUp_click, 5, 5);
            }

            if (_pantilt_left_click == null)
            {
                _pantilt_left_click = CursorHelper.CreateCursorWithBitmap((Bitmap)Properties.Resources.left_click, 5, 5);
            }

            if (_pantilt_right_click == null)
            {
                _pantilt_right_click = CursorHelper.CreateCursorWithBitmap((Bitmap)Properties.Resources.right_click, 5, 5);
            }

            if (_pantilt_up_click == null)
            {
                _pantilt_up_click = CursorHelper.CreateCursorWithBitmap((Bitmap)Properties.Resources.up_click, 5, 5);
            }

            if (_pantilt_down_click == null)
            {
                _pantilt_down_click = CursorHelper.CreateCursorWithBitmap((Bitmap)Properties.Resources.down_click, 5, 5);
            }

            if (_focus_near_click == null)
            {
                _focus_near_click = CursorHelper.CreateCursorWithBitmap((Bitmap)Properties.Resources.focus_near, 5, 5);
            }

            if (_focus_far_click == null)
            {
                _focus_far_click = CursorHelper.CreateCursorWithBitmap((Bitmap)Properties.Resources.focus_far, 5, 5);
            }

            if (_zoom_zoomIn_click == null)
            {
                _zoom_zoomIn_click = CursorHelper.CreateCursorWithBitmap((Bitmap)Properties.Resources.zoom_in, 5, 5);
            }

            if (_zoom_zoomOut_click == null)
            {
                _zoom_zoomOut_click = CursorHelper.CreateCursorWithBitmap((Bitmap)Properties.Resources.zoom_out, 5, 5);
            }

            if (_iris_open_normal == null)
            {
                _iris_open_normal = CursorHelper.CreateCursorWithBitmap((Bitmap)Properties.Resources.iris_open, 5, 5);
            }

            if (_iris_close_normal == null)
            {
                _iris_close_normal = CursorHelper.CreateCursorWithBitmap((Bitmap)Properties.Resources.iris_close, 5, 5);
            }

            if (_iris_open_click == null)
            {
                _iris_open_click = CursorHelper.CreateCursorWithBitmap((Bitmap)Properties.Resources.iris_open_click, 5, 5);
            }

            if (_iris_close_click == null)
            {
                _iris_close_click = CursorHelper.CreateCursorWithBitmap((Bitmap)Properties.Resources.iris_close_click, 5, 5);
            }
        }

        private void SetNormalMouseCursor()
        {
            if (this._ptzState == PTZStates.PanTilt)
            {
                // region 을 체크 해서 마우스 커서를 바꾸어 주고 카메라가 저장하고 있는 위치 타입을 결정해준다. 
                if (this._panTiltState == PtzControlCommand.LeftUp)
                {
                    this.Cursor = _pantilt_leftUp;
                }
                else if (this._panTiltState == PtzControlCommand.LeftDown)
                {
                    this.Cursor = _pantilt_leftDown;
                }
                else if (this._panTiltState == PtzControlCommand.RightDown)
                {
                    this.Cursor = _pantilt_rightDown;
                }
                else if (this._panTiltState == PtzControlCommand.RightUp)
                {
                    this.Cursor = _pantilt_rightUp;
                }
                else if (this._panTiltState == PtzControlCommand.Left)
                {
                    this.Cursor = _pantilt_left;
                }
                else if (this._panTiltState == PtzControlCommand.Right)
                {
                    this.Cursor = _pantilt_right;
                }
                else if (this._panTiltState == PtzControlCommand.Up)
                {
                    this.Cursor = _pantilt_up;
                }
                else if (this._panTiltState == PtzControlCommand.Down)
                {
                    this.Cursor = _pantilt_down;
                }
            }
            else if (this._ptzState == PTZStates.Focus)
            {
                this.Cursor = _focus_normal;
            }
            else if (this._ptzState == PTZStates.Zoom)
            {
                this.Cursor = _zoom_normal;
            }
            else if (this._ptzState == PTZStates.Iris)
            {
                if (this._irisState == PtzControlCommand.IrisOpen)
                {
                    this.Cursor = _iris_open_normal;
                }
                else if (this._irisState == PtzControlCommand.IrisClose)
                {
                    this.Cursor = _iris_close_normal;
                }
            }
        }

        private void ShowSubUserControl(PTZStates ptzState)
        {
            this.xPTZ_Speed.Visibility = System.Windows.Visibility.Collapsed;
            this.xZoom_Speed.Visibility = System.Windows.Visibility.Collapsed;
            this.xAuto_Iris.Visibility = System.Windows.Visibility.Collapsed;
            this.xAuto_Focus.Visibility = System.Windows.Visibility.Collapsed;
            this.xPreset_Sequence.Visibility = System.Windows.Visibility.Collapsed;
            this.xAudio_OnOff.Visibility = System.Windows.Visibility.Collapsed;
            this.xPower_Component.Visibility = System.Windows.Visibility.Collapsed;

            if (ptzState == PTZStates.PanTilt)
                this.xPTZ_Speed.Visibility = System.Windows.Visibility.Visible;
            else if (ptzState == PTZStates.Zoom)
                this.xZoom_Speed.Visibility = System.Windows.Visibility.Visible;
            else if (ptzState == PTZStates.Focus)
                this.xAuto_Focus.Visibility = System.Windows.Visibility.Visible;
            else if (ptzState == PTZStates.Iris)
                this.xAuto_Iris.Visibility = System.Windows.Visibility.Visible;
            else if (ptzState == PTZStates.Power)
                this.xPower_Component.Visibility = System.Windows.Visibility.Visible;
            else if (ptzState == PTZStates.Preset)
                this.xPreset_Sequence.Visibility = System.Windows.Visibility.Visible;
            else if (ptzState == PTZStates.Audio)
                this.xAudio_OnOff.Visibility = System.Windows.Visibility.Visible;
        }

        private void SetToggleButtonGroupState(ToggleButton selectedToggleButton, bool isChecked)
        {
            foreach (ToggleButton button in this._toggleButtonGroupList)
            {
                if (button == selectedToggleButton)
                {
                    if (isChecked)
                    {
                        if (selectedToggleButton == this.xPTZ_PanTiltButton)
                        {
                            this._ptzState = PTZStates.PanTilt;

                            Panel.SetZIndex(this.xPTZ_Region_Grid_8Direction, 999);
                            Panel.SetZIndex(this.xPTZ_Region_Grid_UpDown, 0);

                            ShowSubUserControl(this._ptzState);
                        }
                        else if (selectedToggleButton == this.xPTZ_ZoomButton)
                        {
                            this._ptzState = PTZStates.Zoom;

                            Panel.SetZIndex(this.xPTZ_Region_Grid_UpDown, 999);
                            Panel.SetZIndex(this.xPTZ_Region_Grid_8Direction, 0);

                            ShowSubUserControl(this._ptzState);
                        }
                        else if (selectedToggleButton == this.xPTZ_FocusButton)
                        {
                            this._ptzState = PTZStates.Focus;

                            Panel.SetZIndex(this.xPTZ_Region_Grid_UpDown, 999);
                            Panel.SetZIndex(this.xPTZ_Region_Grid_8Direction, 0);

                            ShowSubUserControl(this._ptzState);
                        }
                        else if (selectedToggleButton == this.xPTZ_IrisButton)
                        {
                            this._ptzState = PTZStates.Iris;

                            Panel.SetZIndex(this.xPTZ_Region_Grid_UpDown, 999);
                            Panel.SetZIndex(this.xPTZ_Region_Grid_8Direction, 0);

                            ShowSubUserControl(this._ptzState);
                        }
                        else if (selectedToggleButton == this.xPTZ_PowerButton)
                        {
                            this._ptzState = PTZStates.Power;
                            this.Cursor = Cursors.Arrow;

                            ShowSubUserControl(this._ptzState);
                        }
                        else if (selectedToggleButton == this.xPTZ_PresetButton)
                        {
                            this._ptzState = PTZStates.Preset;
                            this.Cursor = Cursors.Arrow;

                            ShowSubUserControl(this._ptzState);
                        }
                        else if (selectedToggleButton == this.xPTZ_AudioButton)
                        {
                            this._ptzState = PTZStates.Audio;
                            this.Cursor = Cursors.Arrow;

                            ShowSubUserControl(this._ptzState);
                        }
                    }
                    else
                    {
                        this._ptzState = PTZStates.None;
                        this.Cursor = Cursors.Arrow;

                        ShowSubUserControl(this._ptzState);
                    }
                }
                else
                {
                    button.IsChecked = false;
                }
            }
        }

        public void UpdatePtzControlSize()
        {
            var cameraRect = new Rect(this.PointToScreen(new Point(0, 0)), this.PointToScreen(new Point(this.ActualWidth, this.ActualHeight)));

            var xScale = cameraRect.Width / this.ActualWidth;
            var yScale = cameraRect.Height / this.ActualHeight;

            if (cameraRect.Width >= 960 && cameraRect.Height >= 970)
            {
                this.xPTZ_MainGrid.Width = 480;
                this.xPTZ_MainGrid.Height = 97;
            }
            else if (cameraRect.Width >= 960 && cameraRect.Height < 970)
            {
                this.xPTZ_MainGrid.Width = 480.0 / (97.0 * 5.0) * cameraRect.Height;
                this.xPTZ_MainGrid.Height = cameraRect.Height / 5.0;
            }
            else if (cameraRect.Width < 960 && cameraRect.Height >= 970)
            {
                this.xPTZ_MainGrid.Width = cameraRect.Width / 2.0;
                this.xPTZ_MainGrid.Height = 97.0 / 960.0 * cameraRect.Width;
            }
            else if (cameraRect.Width < 960 && cameraRect.Height < 970)
            {
                var h = cameraRect.Height / 5.0;
                var w = cameraRect.Width / 2.0;

                var h_ratio = h / 97;
                var w_ratio = w / 480;

                if (h_ratio <= w_ratio)
                {
                    this.xPTZ_MainGrid.Width = 480.0 / (97.0 * 5.0) * cameraRect.Height;
                    this.xPTZ_MainGrid.Height = cameraRect.Height / 5.0;
                }
                else
                {
                    this.xPTZ_MainGrid.Width = cameraRect.Width / 2.0;
                    this.xPTZ_MainGrid.Height = 97.0 / 960.0 * cameraRect.Width;
                }
            }

            this.xPTZ_MainGrid.Width /= xScale;
            this.xPTZ_MainGrid.Height /= yScale;
        }

        public void RefreshSequenceList(ObservableCollection<SequenceNameList> sequenceNameList)
        {
            if (this.xComboBoxSequence.ItemsSource == null)
                this.xComboBoxSequence.ItemsSource = sequenceNameList;
        }

        private void PTZControl_MouseEnter(object sender, MouseEventArgs e)
        {
            // CameraControl에서 직접 관리함 (2012.04.23 by jhlee)
            //InnotiveDebug.Trace(4, "[blackRoot17] PTZ Mouse Enter.. id = {0}", this.ParentCamreaID);
            //ChangePTZControlVisible(true);
        }

        private void PTZControl_MouseLeave(object sender, MouseEventArgs e)
        {
            // CameraControl에서 직접 관리함 (2012.04.23 by jhlee)
            //InnotiveDebug.Trace(4, "[blackRoot17] PTZ Mouse Leave.. id = {0}", this.ParentCamreaID);
            //if (e.LeftButton != MouseButtonState.Pressed && e.RightButton != MouseButtonState.Pressed)
            //{
            //    ChangePTZControlVisible(false);
            //}
        }

        public void ChangePTZControlVisible(bool isVisible)
        {
            if (isVisible)
            {
                this.xMainGrid.Opacity = 100;
            }
            else
            {
                this.xMainGrid.Opacity = 0;
            }
        }

        private void PTZControl_MousePressed(object sender, RoutedEventArgs e)
        {
            if (this._ptzState != PTZStates.None)
            {
                Point mouseClickPoint = Mouse.GetPosition(this);

                if (this._ptzState == PTZStates.PanTilt)
                {
                    this.operatingManager.RequestPTZCommand(this._panTiltState, ((int)this.xSliderPtzSpeed.Value).ToString());
                }
                else if (this._ptzState == PTZStates.Zoom)
                {
                    this.operatingManager.RequestPTZCommand(this._zoomState, ((int)this.xSliderZoomSpeed.Value).ToString());
                }
                else if (this._ptzState == PTZStates.Focus)
                {
                    this.operatingManager.RequestPTZCommand(this._focusState, null);
                }
                else if (this._ptzState == PTZStates.Iris)
                {
                    this.operatingManager.RequestPTZCommand(this._irisState, null);
                }
            }
        }

        private void PTZControl_StylusUp(object sender, StylusEventArgs e)
        {
            System.Diagnostics.Debug.WriteLine("Stylus Up");

            Mouse.OverrideCursor = null;
            this.SetNormalMouseCursor();

            if (this._ptzState != PTZStates.None)
            {
                Point mouseClickPoint = Mouse.GetPosition(this);

                if (this._ptzState == PTZStates.PanTilt)
                {
                    this.operatingManager.RequestPTZCommand(PtzControlCommand.PantiltStop, ((int)this.xSliderPtzSpeed.Value).ToString());
                }
                else if (this._ptzState == PTZStates.Zoom)
                {
                    this.operatingManager.RequestPTZCommand(PtzControlCommand.ZoomStop, ((int)this.xSliderPtzSpeed.Value).ToString());
                }
                else if (this._ptzState == PTZStates.Focus)
                {
                    this.operatingManager.RequestPTZCommand(PtzControlCommand.FocusStop, ((int)this.xSliderPtzSpeed.Value).ToString());
                }
                else if (this._ptzState == PTZStates.Iris)
                {
                    this.operatingManager.RequestPTZCommand(PtzControlCommand.IrisStop, ((int)this.xSliderPtzSpeed.Value).ToString());
                }
            }
        }

        private void PTZControl_StylusLeave(object sender, StylusEventArgs e)
        {
            System.Diagnostics.Debug.WriteLine("Stylus Leave");

            Mouse.OverrideCursor = null;
            this.SetNormalMouseCursor();

            if (this._ptzState != PTZStates.None)
            {
                Point mouseClickPoint = Mouse.GetPosition(this);

                if (this._ptzState == PTZStates.PanTilt)
                {
                    this.operatingManager.RequestPTZCommand(PtzControlCommand.PantiltStop, ((int)this.xSliderPtzSpeed.Value).ToString());
                }
                else if (this._ptzState == PTZStates.Zoom)
                {
                    this.operatingManager.RequestPTZCommand(PtzControlCommand.ZoomStop, ((int)this.xSliderPtzSpeed.Value).ToString());
                }
                else if (this._ptzState == PTZStates.Focus)
                {
                    this.operatingManager.RequestPTZCommand(PtzControlCommand.FocusStop, ((int)this.xSliderPtzSpeed.Value).ToString());
                }
                else if (this._ptzState == PTZStates.Iris)
                {
                    this.operatingManager.RequestPTZCommand(PtzControlCommand.IrisStop, ((int)this.xSliderPtzSpeed.Value).ToString());
                }
            }
        }

        private void RaiseMousePressedEvent(object sender, EventArgs e)
        {
            if (Mouse.LeftButton == MouseButtonState.Pressed)
            {
                //Debug.WriteLine("[jhlee] mouse pressed!");
                this.RaiseMousePressedEvent();
            }
            else
            {
                // ovveride OnMouseLeftButtonUp 이 동작하지 않아서 임시로 여기다가 놓음
                Mouse.OverrideCursor = null;
                this.SetNormalMouseCursor();

                if (this._mousePressTimer != null)
                {
                    this._mousePressTimer.Stop();
                    this._mousePressTimer.Tick -= this.RaiseMousePressedEvent;
                    this._mousePressTimer = null;
                }

                this.operatingManager.Stop(this._ptzState);

                //if (this._ptzState == PTZStates.Focus || this._ptzState == PTZStates.Iris)
                //    this.operatingManager.AutoStop();
                //else
                //    this.operatingManager.PtzStop();
            }
        }

        private void xPTZ_MainControlButton_Clicked(object sender, RoutedEventArgs e)
        {
            var selectedButton = sender as ToggleButton;
            this.SetToggleButtonGroupState(selectedButton, (bool)selectedButton.IsChecked);
            this.operatingManager.SelectCamera();
        }

        private void xPTZ_MainControlButton_StylusUp(object sender, RoutedEventArgs e)
        {
            var selectedButton = sender as ToggleButton;
            selectedButton.IsChecked = !selectedButton.IsChecked;
            this.SetToggleButtonGroupState(selectedButton, (bool)selectedButton.IsChecked);
            this.operatingManager.SelectCamera();
        }

        private void xPTZ_Region_MouseEnter(object sender, MouseEventArgs e)
        {
            // PTZStates가 PanTilt일 때만
            //System.Diagnostics.Debug.WriteLine("Mouse Enter : " + sender.ToString());

            if (this._ptzState == PTZStates.PanTilt)
            {
                // region 을 체크 해서 마우스 커서를 바꾸어 주고 카메라가 저장하고 있는 위치 타입을 결정해준다. 
                if (sender == this.xPTZ_Region_LeftUp)
                {
                    this._panTiltState = PtzControlCommand.LeftUp;
                }
                else if (sender == this.xPTZ_Region_LeftDown)
                {
                    this._panTiltState = PtzControlCommand.LeftDown;
                }
                else if (sender == this.xPTZ_Region_RightDown)
                {
                    this._panTiltState = PtzControlCommand.RightDown;
                }
                else if (sender == this.xPTZ_Region_RightUp)
                {
                    this._panTiltState = PtzControlCommand.RightUp;
                }
                else if (sender == this.xPTZ_Region_Left || sender == this.xPTZ_Region_Center_Left)
                {
                    this._panTiltState = PtzControlCommand.Left;
                }
                else if (sender == this.xPTZ_Region_Right || sender == this.xPTZ_Region_Center_Right)
                {
                    this._panTiltState = PtzControlCommand.Right;
                }
                else if (sender == this.xPTZ_Region_Up || sender == this.xPTZ_Region_Center_Up)
                {
                    this._panTiltState = PtzControlCommand.Up;
                }
                else if (sender == this.xPTZ_Region_Down || sender == this.xPTZ_Region_Center_Down)
                {
                    this._panTiltState = PtzControlCommand.Down;
                }

                if (Mouse.LeftButton == MouseButtonState.Pressed)
                {
                    this.SetClickedMouseCursor();
                }
                else
                {
                    this.SetNormalMouseCursor();
                }
            }
        }

        private void xPTZ_Region_StylusEnter(object sender, StylusEventArgs e)
        {
            // PTZStates가 PanTilt일 때만

            System.Diagnostics.Debug.WriteLine("Stylus Enter : " + sender.ToString() + " " + (sender as FrameworkElement).Name);

            if (this._ptzState == PTZStates.PanTilt)
            {
                // region 을 체크 해서 마우스 커서를 바꾸어 주고 카메라가 저장하고 있는 위치 타입을 결정해준다. 
                if (sender == this.xPTZ_Region_LeftUp)
                {
                    this._panTiltState = PtzControlCommand.LeftUp;
                }
                else if (sender == this.xPTZ_Region_LeftDown)
                {
                    this._panTiltState = PtzControlCommand.LeftDown;
                }
                else if (sender == this.xPTZ_Region_RightDown)
                {
                    this._panTiltState = PtzControlCommand.RightDown;
                }
                else if (sender == this.xPTZ_Region_RightUp)
                {
                    this._panTiltState = PtzControlCommand.RightUp;
                }
                else if (sender == this.xPTZ_Region_Left || sender == this.xPTZ_Region_Center_Left)
                {
                    this._panTiltState = PtzControlCommand.Left;
                }
                else if (sender == this.xPTZ_Region_Right || sender == this.xPTZ_Region_Center_Right)
                {
                    this._panTiltState = PtzControlCommand.Right;
                }
                else if (sender == this.xPTZ_Region_Up || sender == this.xPTZ_Region_Center_Up)
                {
                    this._panTiltState = PtzControlCommand.Up;
                }
                else if (sender == this.xPTZ_Region_Down || sender == this.xPTZ_Region_Center_Down)
                {
                    this._panTiltState = PtzControlCommand.Down;
                }

                this.SetClickedMouseCursor();

                // ControlPanel을 클릭 했을 경우 return.
                var hitControl = e.OriginalSource as FrameworkElement;
                if (hitControl == null)
                    return;

                var speed = ((int)this.xSliderPtzSpeed.Value).ToString();
                if (this._ptzState == PTZStates.PanTilt)
                {
                    if (!(string.CompareOrdinal(hitControl.Name, "xPTZ_Region_LeftUp") == 0 ||
                        string.CompareOrdinal(hitControl.Name, "xPTZ_Region_Up") == 0 ||
                        string.CompareOrdinal(hitControl.Name, "xPTZ_Region_RightUp") == 0 ||
                        string.CompareOrdinal(hitControl.Name, "xPTZ_Region_Left") == 0 ||
                        string.CompareOrdinal(hitControl.Name, "xPTZ_Region_Right") == 0 ||
                        string.CompareOrdinal(hitControl.Name, "xPTZ_Region_LeftDown") == 0 ||
                        string.CompareOrdinal(hitControl.Name, "xPTZ_Region_Down") == 0 ||
                        string.CompareOrdinal(hitControl.Name, "xPTZ_Region_RightDown") == 0 ||
                        string.CompareOrdinal(hitControl.Name, "xPTZ_Region_Center_Up") == 0 ||
                        string.CompareOrdinal(hitControl.Name, "xPTZ_Region_Center_Left") == 0 ||
                        string.CompareOrdinal(hitControl.Name, "xPTZ_Region_Center_Right") == 0 ||
                        string.CompareOrdinal(hitControl.Name, "xPTZ_Region_Center_Down") == 0))
                    {
                        InnotiveDebug.Trace(3, "[shwlee] PTZ Up Source : {0}", e.OriginalSource);
                        return;
                    }

                    InnotiveDebug.Trace(3, "[shwlee] PTZ Down Source : {0}", e.OriginalSource);
                    // region 을 체크 해서 마우스 커서를 바꾸어 주고 카메라가 저장하고 있는 위치 타입을 결정해준다. 
                    switch (this._panTiltState)
                    {
                        case PtzControlCommand.LeftUp:
                            this.operatingManager.RequestPTZCommand(PtzControlCommand.LeftUp_Start, speed);
                            break;
                        case PtzControlCommand.LeftDown:
                            this.operatingManager.RequestPTZCommand(PtzControlCommand.LeftDown_Start, speed);
                            break;
                        case PtzControlCommand.RightDown:
                            this.operatingManager.RequestPTZCommand(PtzControlCommand.RightDown_Start, speed);
                            break;
                        case PtzControlCommand.RightUp:
                            this.operatingManager.RequestPTZCommand(PtzControlCommand.RightUp_Start, speed);
                            break;
                        case PtzControlCommand.Left:
                            this.operatingManager.RequestPTZCommand(PtzControlCommand.Left_Start, speed);
                            break;
                        case PtzControlCommand.Right:
                            this.operatingManager.RequestPTZCommand(PtzControlCommand.Right_Start, speed);
                            break;
                        case PtzControlCommand.Up:
                            this.operatingManager.RequestPTZCommand(PtzControlCommand.Up_Start, speed);
                            break;
                        case PtzControlCommand.Down:
                            this.operatingManager.RequestPTZCommand(PtzControlCommand.Down_Start, speed);
                            break;
                    }

                    this.RaisePTZControlUIClickedEvent();
                }

                if (this._ptzState != PTZStates.None)
                {
                    if (this._ptzState == PTZStates.PanTilt)
                    {
                        this.operatingManager.RequestPTZCommand(this._panTiltState, ((int)this.xSliderPtzSpeed.Value).ToString());
                    }
                }
            }
        }

        private void xPTZ_Region_UpDown_MouseEnter(object sender, MouseEventArgs e)
        {
            // _ptzState가 Focus/Zoom/Iris 일 경우 설정.
            if (this._ptzState == PTZStates.Focus || this._ptzState == PTZStates.Zoom || this._ptzState == PTZStates.Iris)
            {
                if (sender == this.xPTZ_Region_UpDown_Up)
                {
                    this._zoomState = PtzControlCommand.ZoomIn;
                    this._focusState = PtzControlCommand.FocusFar;
                    this._irisState = PtzControlCommand.IrisOpen;
                }
                else if (sender == this.xPTZ_Region_UpDown_Down)
                {
                    this._zoomState = PtzControlCommand.ZoomOut;
                    this._focusState = PtzControlCommand.FocusNear;
                    this._irisState = PtzControlCommand.IrisClose;
                }

                if (Mouse.LeftButton == MouseButtonState.Pressed)
                {
                    this.SetClickedMouseCursor();
                }
                else
                {
                    this.SetNormalMouseCursor();
                }
            }
        }

        private void xPTZ_Region_UpDown_StylusEnter(object sender, StylusEventArgs e)
        {
            // _ptzState가 Focus/Zoom/Iris 일 경우 설정.
            if (this._ptzState == PTZStates.Focus || this._ptzState == PTZStates.Zoom || this._ptzState == PTZStates.Iris)
            {
                if (sender == this.xPTZ_Region_UpDown_Up)
                {
                    this._zoomState = PtzControlCommand.ZoomIn;
                    this._focusState = PtzControlCommand.FocusFar;
                    this._irisState = PtzControlCommand.IrisOpen;
                }
                else if (sender == this.xPTZ_Region_UpDown_Down)
                {
                    this._zoomState = PtzControlCommand.ZoomOut;
                    this._focusState = PtzControlCommand.FocusNear;
                    this._irisState = PtzControlCommand.IrisClose;
                }

                this.SetClickedMouseCursor();

                // ControlPanel을 클릭 했을 경우 return.
                var hitControl = e.OriginalSource as FrameworkElement;
                if (hitControl == null)
                    return;

                var speed = ((int)this.xSliderPtzSpeed.Value).ToString();
                if (this._ptzState == PTZStates.Zoom)
                {
                    if (!(string.CompareOrdinal(hitControl.Name, "xPTZ_Region_UpDown_Up") == 0 ||
                        string.CompareOrdinal(hitControl.Name, "xPTZ_Region_UpDown_Down") == 0))
                    {
                        return;
                    }

                    switch (this._zoomState)
                    {
                        case PtzControlCommand.ZoomIn:
                            this.operatingManager.RequestPTZCommand(PtzControlCommand.ZoomIn_Start, speed);
                            break;
                        case PtzControlCommand.ZoomOut:
                            this.operatingManager.RequestPTZCommand(PtzControlCommand.ZoomOut_Start, speed);
                            break;
                    }

                    this.RaisePTZControlUIClickedEvent();
                }
                else if (this._ptzState == PTZStates.Focus)
                {
                    if (!(string.CompareOrdinal(hitControl.Name, "xPTZ_Region_UpDown_Up") == 0 ||
                        string.CompareOrdinal(hitControl.Name, "xPTZ_Region_UpDown_Down") == 0))
                    {
                        return;
                    }

                    switch (this._focusState)
                    {
                        case PtzControlCommand.FocusFar:
                            this.operatingManager.RequestPTZCommand(PtzControlCommand.FocusFar_Start, speed);
                            break;
                        case PtzControlCommand.FocusNear:
                            this.operatingManager.RequestPTZCommand(PtzControlCommand.FocusNear_Start, speed);
                            break;
                    }

                    this.RaisePTZControlUIClickedEvent();
                }
                else if (this._ptzState == PTZStates.Iris)
                {
                    if (!(string.CompareOrdinal(hitControl.Name, "xPTZ_Region_UpDown_Up") == 0 ||
                        string.CompareOrdinal(hitControl.Name, "xPTZ_Region_UpDown_Down") == 0))
                    {
                        return;
                    }

                    switch (this._irisState)
                    {
                        case PtzControlCommand.IrisOpen:
                            this.operatingManager.RequestPTZCommand(PtzControlCommand.IrisOpen_Start, speed);
                            break;
                        case PtzControlCommand.IrisClose:
                            this.operatingManager.RequestPTZCommand(PtzControlCommand.IrisClose_Start, speed);
                            break;
                    }

                    this.RaisePTZControlUIClickedEvent();
                }

                if (this._ptzState != PTZStates.None)
                {
                    Point mouseClickPoint = Mouse.GetPosition(this);

                    if (this._ptzState == PTZStates.PanTilt)
                    {
                        this.operatingManager.RequestPTZCommand(this._panTiltState, ((int)this.xSliderPtzSpeed.Value).ToString());
                    }
                    else if (this._ptzState == PTZStates.Zoom)
                    {
                        this.operatingManager.RequestPTZCommand(this._zoomState, ((int)this.xSliderZoomSpeed.Value).ToString());
                    }
                    else if (this._ptzState == PTZStates.Focus)
                    {
                        this.operatingManager.RequestPTZCommand(this._focusState, null);
                    }
                    else if (this._ptzState == PTZStates.Iris)
                    {
                        this.operatingManager.RequestPTZCommand(this._irisState, null);
                    }
                }
            }
        }

        #endregion

        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// The notify property changed.
        /// </summary>
        /// <param name="info">
        /// The info.
        /// </param>
        protected void NotifyPropertyChanged(string info)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }

        #endregion // INotifyPropertyChanged
    }
}