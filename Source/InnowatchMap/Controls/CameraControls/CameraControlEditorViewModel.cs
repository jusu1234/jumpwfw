﻿
namespace Innotive.InnoWatch.DLLs.CameraControls
{
    using Innotive.InnoWatch.Commons.ViewModels;

    class CameraControlEditorViewModel : BaseViewModel
    {
        private bool isTemplateMode;

        public CameraControlEditorViewModel(bool isTemplateMode)
        {
            this.IsTemplateMode = isTemplateMode;
        }

        public bool IsTemplateMode
        {
            get
            {
                return this.isTemplateMode;
            }

            set
            {
                this.isTemplateMode = value;
                this.OnPropertyChanged("IsTemplateMode");
            }
        }
    }
}
