﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PTZEnableStateToVisibilityConverter.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Defines the PTZEnableStateToVisibilityConverter type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.DLLs.CameraControls.Converters
{
    using System;
    using System.Windows;
    using System.Windows.Data;

    /// <summary>
    /// PTZ 활성화.
    /// </summary>
    public class PTZEnableStateToVisibilityConverter : IValueConverter
    {
        /// <summary>
        /// 값이 Always인 경우는 Alwyas On을 의미한다.
        /// 값이 None인 경우는 Always Off를 의미한다.
        /// 값이 ZoomLevel인 경우는 Zoom Level에 따른 상황이 발생하므로
        /// UpdateLayoutSizeChange 이벤트가 발생될때마다 Visibility 를 변경하게 된다.
        /// </summary>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <param name="targetType">
        /// The target type.
        /// </param>
        /// <param name="parameter">
        /// The parameter.
        /// </param>
        /// <param name="culture">
        /// The culture.
        /// </param>
        /// <returns>
        /// visibility를 반환하게 된다.
        /// </returns>
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            switch ((PTZEnableState)value)
            {
                case PTZEnableState.Always:
                    return Visibility.Visible;
                case PTZEnableState.None:
                    return Visibility.Collapsed;
                case PTZEnableState.ZoomLevel:
                    return Visibility.Visible;
                default:
                    return Visibility.Collapsed;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return null;
        }
    }
}
