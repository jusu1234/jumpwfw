﻿
namespace Innotive.InnoWatch.DLLs.CameraControls.Converters
{
    using System;
    using System.Globalization;
    using System.Windows;
    using System.Windows.Data;

    public class MultiBindingForBooleanConverter :IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            var firstCondition = values[0].ToString();
            var secondCondition = (bool)values[1];

            if (string.Equals(firstCondition, "Visible") && secondCondition)
            {
                return Visibility.Visible;
            }

            return Visibility.Collapsed;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}
