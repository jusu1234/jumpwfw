﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CameraControlEditor.xaml.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Interaction logic for CameraControl.xaml
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.DLLs.CameraControls
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Controls.Primitives;
    using System.Windows.Data;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Shapes;
    using Innotive.InnoWatch.Commons.BaseControls;
    using Innotive.InnoWatch.Commons.Converters;
    using Innotive.InnoWatch.Commons.PropertyEditor;
    using Innotive.InnoWatch.Commons.Publics;
    using Innotive.InnoWatch.Commons.ResourceManagements;
    using Innotive.InnoWatch.DLLs.CameraControls.Converters;
    using Innotive.InnoWatch.DLLs.CameraControls.ValidationRule;
    
    /// <summary>
    /// Interaction logic for CameraControl.xaml
    /// </summary>
    public partial class CameraControlEditor : BaseEditor, IEditor
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CameraControlEditor"/> class.
        /// </summary>
        public CameraControlEditor()
        {
        }

        /// <summary>
        /// 클래스 생성자
        /// </summary>
        public CameraControlEditor(Window parent, ResourceManager resourceManager, bool isTemplateMode) :
            base(parent, resourceManager)
        {
            InitializeComponent();

            this.DataContext = new CameraControlEditorViewModel(isTemplateMode);

            this.xCamera_BorderImageSource.KeyUp += new KeyEventHandler(Camera_BorderImageSource_KeyUp);
            this.xButton_BorderImageSource.Click += new RoutedEventHandler(Button_BorderImageSource_Click);
            
            this.xCamera_BorderColorRectangle.MouseLeftButtonDown += new MouseButtonEventHandler(Camera_BorderColorRectangle_MouseLeftButtonDown);
            this.xCamera_TextColorRectangle.MouseLeftButtonDown += new MouseButtonEventHandler(Camera_TextColorRectangle_MouseLeftButtonDown);
			this.xCamera_OutlineColorRectangle.MouseLeftButtonDown += new MouseButtonEventHandler(Camera_OutlineColorRectangle_MouseLeftButtonDown);

            //string.Empty를 허용할 textbox들
            this._exceptTargetElementList.Add(xCamera_BorderImageSource);
            this._exceptTargetElementList.Add(xCamera_ID);
            this._exceptTargetElementList.Add(xCamera_GroupTag);
            this._exceptTargetElementList.Add(xCamera_NavigationName);

            this._exceptNullElementList.Add(xCamera_ID);
            this._exceptNullElementList.Add(xCamera_GroupTag);
            this._exceptNullElementList.Add(xCamera_NavigationName);

            this.xPTZ_RadioButton_AlwaysOff.Click += new RoutedEventHandler(xPTZ_RadioButton_AlwaysOff_Click);
            this.xPTZ_RadioButton_AlwaysOn.Click += new RoutedEventHandler(xPTZ_RadioButton_AlwaysOn_Click);
            this.xPTZ_RadioButton_ZoomLevel.Click += new RoutedEventHandler(xPTZ_RadioButton_ZoomLevel_Click);
        }

        void xPTZ_RadioButton_ZoomLevel_Click(object sender, RoutedEventArgs e)
        {
            foreach (FrameworkElement element in this._elementList)
            {
                if (element is CameraControl && (bool)xPTZ_RadioButton_ZoomLevel.IsChecked)
                {
                    (element as CameraControl).PTZEnabled = PTZEnableState.ZoomLevel;
                }
            }
        }

        void xPTZ_RadioButton_AlwaysOn_Click(object sender, RoutedEventArgs e)
        {
            foreach (FrameworkElement element in this._elementList)
            {
                if (element is CameraControl && (bool)xPTZ_RadioButton_AlwaysOn.IsChecked)
                {
                    (element as CameraControl).PTZEnabled = PTZEnableState.Always;
                }
            }
        }

        void xPTZ_RadioButton_AlwaysOff_Click(object sender, RoutedEventArgs e)
        {
            foreach (FrameworkElement element in this._elementList)
            {
                if (element is CameraControl && (bool)xPTZ_RadioButton_AlwaysOff.IsChecked)
                {
                    (element as CameraControl).PTZEnabled = PTZEnableState.None;
                }
            }
        }

        /// <summary>
        /// Finalizes an instance of the <see cref="CameraControlEditor"/> class. 
        /// 클래스 소멸자.
        /// </summary>
        ~CameraControlEditor()
        {
            this.Dispatcher.BeginInvoke(new Action(this.ClearBinding));

            if (this.xCamera_BorderImageSource == null)
            {
                return;
            }

            this.xCamera_BorderImageSource.KeyUp -= new KeyEventHandler(Camera_BorderImageSource_KeyUp);
            this.xCamera_BorderColorRectangle.MouseLeftButtonDown -= new MouseButtonEventHandler(Camera_BorderColorRectangle_MouseLeftButtonDown);
            this.xCamera_TextColorRectangle.MouseLeftButtonDown -= new MouseButtonEventHandler(Camera_TextColorRectangle_MouseLeftButtonDown);
            this.xButton_BorderImageSource.Click -= new RoutedEventHandler(Button_BorderImageSource_Click);
            this.xCamera_OutlineColorRectangle.MouseLeftButtonDown -= new MouseButtonEventHandler(Camera_OutlineColorRectangle_MouseLeftButtonDown);

            this.xPTZ_RadioButton_AlwaysOff.Click -= new RoutedEventHandler(xPTZ_RadioButton_AlwaysOff_Click);
            this.xPTZ_RadioButton_AlwaysOn.Click -= new RoutedEventHandler(xPTZ_RadioButton_AlwaysOn_Click);
            this.xPTZ_RadioButton_ZoomLevel.Click -= new RoutedEventHandler(xPTZ_RadioButton_ZoomLevel_Click);
        }

        public new void ClearBinding()
        {
            base.ClearBinding();

            BindingOperations.ClearAllBindings(this.xCamera_ID);
            BindingOperations.ClearAllBindings(this.xCamera_GroupTag);
            BindingOperations.ClearAllBindings(this.xCamera_NavigationName);
            BindingOperations.ClearAllBindings(this.xCamera_BorderLeft);
            BindingOperations.ClearAllBindings(this.xCamera_BorderTop);
            BindingOperations.ClearAllBindings(this.xCamera_BorderRight);
            BindingOperations.ClearAllBindings(this.xCamera_BorderBottom);
            BindingOperations.ClearAllBindings(this.xCamera_BorderImageSource);
            BindingOperations.ClearAllBindings(this.xCamera_BorderColorRectangle);
            BindingOperations.ClearAllBindings(this.xCamera_BorderColorTextBox);
            
            BindingOperations.ClearAllBindings(this.xCamera_TextColorRectangle);
            BindingOperations.ClearAllBindings(this.xCamera_TextColorTextBox);
            BindingOperations.ClearAllBindings(this.xCamera_TextLeft);
            BindingOperations.ClearAllBindings(this.xCamera_TextTop);
            BindingOperations.ClearAllBindings(this.xCamera_TextRight);
            BindingOperations.ClearAllBindings(this.xCamera_TextBottom);
            BindingOperations.ClearAllBindings(this.xCamera_OutlineColorRectangle);
            BindingOperations.ClearAllBindings(this.xCamera_OutlineColorTextBox);

            BindingOperations.ClearAllBindings(this.xPTZ_CheckBox_FocusControl);
            BindingOperations.ClearAllBindings(this.xPTZ_CheckBox_LightControl);
            BindingOperations.ClearAllBindings(this.xPTZ_CheckBox_PanTiltControl);
            BindingOperations.ClearAllBindings(this.xPTZ_CheckBox_PresetControl);
            BindingOperations.ClearAllBindings(this.xPTZ_CheckBox_WiperControl);
            BindingOperations.ClearAllBindings(this.xPTZ_CheckBox_ZoomControl);
            BindingOperations.ClearAllBindings(this.xPTZ_MinZoomLevel);
            BindingOperations.ClearAllBindings(this.xPTZ_RadioButton_AlwaysOff);
            BindingOperations.ClearAllBindings(this.xPTZ_RadioButton_AlwaysOn);
            BindingOperations.ClearAllBindings(this.xPTZ_RadioButton_ZoomLevel);
        }

        public new void Binding(List<BaseControl> elementList)
        {
            this.ClearBinding();

            this.xMainExpander.Header = "Camera";

            base.Binding(elementList);
            this.Binding(elementList[elementList.Count - 1]);

            Multi_SetNotifycationForAction(
                elementList, 
                xCamera_ID,
                CameraControl.IDProperty, 
                TextBox.TextProperty, 
                null, new CameraIDValidationRule());

            Multi_SetNotifycationForAction(
                elementList,
                xCamera_GroupTag,
                CameraControl.GroupTagProperty,
                TextBox.TextProperty,
                null, new ExceptionValidationRule());

            Multi_SetNotifycationForAction(
                elementList,
                xCamera_NavigationName,
                CameraControl.DescriptionProperty,
                TextBox.TextProperty,
                null, new ExceptionValidationRule());

            object[] marginConvertParameterList = new object[elementList.Count];

            for (int i = 0; i < elementList.Count; i++)
            {
                marginConvertParameterList[i] = elementList[i];
            }

            Multi_SetNotifycationForAction(
                elementList, 
                xCamera_BorderLeft,
                CameraControl.BorderMarginProperty, 
                TextBox.TextProperty,
                new MarginToTextConverter(
                    MarginDirect.Left, 
                    CameraControl.BorderMarginProperty), 
                    marginConvertParameterList);

            Multi_SetNotifycationForAction(
                elementList, 
                xCamera_BorderTop,
                CameraControl.BorderMarginProperty, 
                TextBox.TextProperty,
                new MarginToTextConverter(
                    MarginDirect.Top, CameraControl.BorderMarginProperty), 
                    marginConvertParameterList);

            Multi_SetNotifycationForAction(
                elementList, 
                xCamera_BorderRight,
                CameraControl.BorderMarginProperty, 
                TextBox.TextProperty,
                new MarginToTextConverter(
                    MarginDirect.Right, 
                    CameraControl.BorderMarginProperty), 
                    marginConvertParameterList);

            Multi_SetNotifycationForAction(
                elementList, 
                xCamera_BorderBottom,
                CameraControl.BorderMarginProperty, 
                TextBox.TextProperty,
                new MarginToTextConverter(
                    MarginDirect.Bottom, 
                    CameraControl.BorderMarginProperty), 
                    marginConvertParameterList);

            Multi_SetNotifycationForAction(
                elementList, 
                xCamera_BorderImageSource,
                CameraControl.BorderImageSourceProperty, 
                TextBox.TextProperty,
                null, new ExceptionValidationRule());

            Multi_SetNotifycationForAction(
                elementList, 
                xCamera_BorderColorRectangle,
                Control.BackgroundProperty, 
                Shape.FillProperty,
                null, new ExceptionValidationRule());

            Multi_SetNotifycationForAction(
                elementList, 
                xCamera_BorderColorTextBox,
                Control.BackgroundProperty, 
                TextBox.TextProperty,
                new BrushToStringCoverter(), new ExceptionValidationRule());

            Multi_SetNotifycationForAction(
                elementList, 
                xCamera_TextColorRectangle,
                CameraControl.TextBrushProperty, 
                Shape.FillProperty,
                null, new ExceptionValidationRule());

            Multi_SetNotifycationForAction(
                elementList, 
                xCamera_TextColorTextBox,
                CameraControl.TextBrushProperty, 
                TextBox.TextProperty,
                new BrushToStringCoverter(), new ExceptionValidationRule());

            Multi_SetNotifycationForAction(
                elementList, 
                xCamera_OutlineColorRectangle,
                CameraControl.OutlineBrushProperty, 
                Shape.FillProperty,
                null, new ExceptionValidationRule());
            
            Multi_SetNotifycationForAction(
                elementList, 
                xCamera_OutlineColorTextBox,
                CameraControl.OutlineBrushProperty, 
                TextBox.TextProperty,
                new BrushToStringCoverter(), new ExceptionValidationRule());
            
            Multi_SetNotifycationForAction(
                elementList, 
                xCamera_OutlineThickness,
                CameraControl.OutlineThicknessProperty, 
                TextBox.TextProperty,
                null, new ExceptionValidationRule());

            Multi_SetNotifycationForAction(
                elementList, 
                xCamera_TextLeft,
                CameraControl.TextMarginProperty, 
                TextBox.TextProperty,
                new MarginToTextConverter(
                    MarginDirect.Left, 
                    CameraControl.TextMarginProperty), 
                    marginConvertParameterList);

            Multi_SetNotifycationForAction(
                elementList, 
                xCamera_TextTop,
                CameraControl.TextMarginProperty, 
                TextBox.TextProperty,
                new MarginToTextConverter(
                    MarginDirect.Top, 
                    CameraControl.TextMarginProperty), 
                    marginConvertParameterList);

            Multi_SetNotifycationForAction(
                elementList, 
                xCamera_TextRight,
                CameraControl.TextMarginProperty, 
                TextBox.TextProperty,
                new MarginToTextConverter(
                    MarginDirect.Right, 
                    CameraControl.TextMarginProperty), 
                    marginConvertParameterList);

            Multi_SetNotifycationForAction(
                elementList, 
                xCamera_TextBottom,
                CameraControl.TextMarginProperty, 
                TextBox.TextProperty,
                new MarginToTextConverter(
                    MarginDirect.Bottom, 
                    CameraControl.TextMarginProperty), 
                    marginConvertParameterList);

            Multi_SetNotifycationForAction(
                elementList, 
                xPTZ_CheckBox_PanTiltControl,
                CameraControl.PanTiltControlableProperty,
                ToggleButton.IsCheckedProperty,
                null, new ExceptionValidationRule());

            Multi_SetNotifycationForAction(
                elementList, 
                xPTZ_CheckBox_ZoomControl,
                CameraControl.ZoomControlableProperty,
                ToggleButton.IsCheckedProperty,
                null, new ExceptionValidationRule());

            Multi_SetNotifycationForAction(
                elementList, 
                xPTZ_CheckBox_FocusControl,
                CameraControl.FocusControlableProperty,
                ToggleButton.IsCheckedProperty,
                null, new ExceptionValidationRule());

            Multi_SetNotifycationForAction(
                elementList, 
                xPTZ_CheckBox_WiperControl,
                CameraControl.WiperControlableProperty,
                ToggleButton.IsCheckedProperty,
                null, new ExceptionValidationRule());

            Multi_SetNotifycationForAction(
                elementList, 
                xPTZ_CheckBox_LightControl,
                CameraControl.LightControlableProperty,
                ToggleButton.IsCheckedProperty,
                null, new ExceptionValidationRule());

            Multi_SetNotifycationForAction(
                elementList, 
                xPTZ_CheckBox_PresetControl,
                CameraControl.PresetControlableProperty, 
                ToggleButton.IsCheckedProperty,
                null, new ExceptionValidationRule());
            
            // Radio Button Binding 이 제대로 동작하지 않는 버그가 있어서 일단 임시처리함. 
            CameraControl lastElement = elementList[elementList.Count - 1] as CameraControl;
            if (lastElement != null)
            {
                if (lastElement.PTZEnabled == PTZEnableState.Always)
                {
                    this.xPTZ_RadioButton_AlwaysOn.IsChecked = true;
                }
                else if (lastElement.PTZEnabled == PTZEnableState.None)
                {
                    this.xPTZ_RadioButton_AlwaysOff.IsChecked = true;
                }

                if (lastElement.PTZEnabled == PTZEnableState.ZoomLevel)
                {
                    this.xPTZ_RadioButton_ZoomLevel.IsChecked = true;
                }
            }

            /*
            string[] ptzEnabledStateList = new string[elementList.Count];
            
            for(int i=0;i<elementList.Count;i++)
            {
                if((elementList[i] as CameraControl).PTZEnabled == PTZEnableState.Always)
                    ptzEnabledStateList[i] = "Always";
                else if ((elementList[i] as CameraControl).PTZEnabled == PTZEnableState.ZoomLevel)
                    ptzEnabledStateList[i] = "ZoomLevel";
                else 
                    ptzEnabledStateList[i] = "Always";
                
            }
            
            Multi_SetNotifycationForAction(elementList, xPTZ_RadioButton_AlwaysOn,
                CameraControl.PTZEnabledProperty, RadioButton.IsCheckedProperty,
                new CameraPTZEnableStateConverter(), ptzEnabledStateList);


            Multi_SetNotifycationForAction(elementList, xPTZ_RadioButton_AlwaysOff,
                CameraControl.PTZEnabledProperty, RadioButton.IsCheckedProperty,
                new CameraPTZEnableStateConverter(), ptzEnabledStateList);


            Multi_SetNotifycationForAction(elementList, xPTZ_RadioButton_ZoomLevel,
                CameraControl.PTZEnabledProperty, RadioButton.IsCheckedProperty,
                new CameraPTZEnableStateConverter(), ptzEnabledStateList);
            
            */

            Multi_SetNotifycationForAction(
                elementList, 
                xPTZ_LockToParentMaxLevel,
                CameraControl.PTZLockToParentsMinLevelProperty, 
                ToggleButton.IsCheckedProperty,
                null, new ExceptionValidationRule());

            Multi_SetNotifycationForAction(
                elementList, 
                xPTZ_MinZoomLevel,
                CameraControl.PTZMinLevelProperty, 
                TextBox.TextProperty,
                null, new ExceptionValidationRule());

            this.SetMinLevelEnableBinding();
         }

        void SetMinLevelEnableBinding()
        {
            MultiBinding multiBinding = new MultiBinding();

            Binding radioBinding = new Binding();
            radioBinding.Source = this.xPTZ_RadioButton_ZoomLevel;
            radioBinding.Path = new PropertyPath(RadioButton.IsCheckedProperty);
            multiBinding.Bindings.Add(radioBinding);

            Binding checkBoxBinding = new Binding();
            checkBoxBinding.Source = this.xPTZ_LockToParentMaxLevel;
            checkBoxBinding.Path = new PropertyPath(CheckBox.IsCheckedProperty);
            multiBinding.Bindings.Add(checkBoxBinding);

            multiBinding.Converter = new CameraEditorPTZUIEnableStateConverter();
            multiBinding.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;
            multiBinding.Mode = BindingMode.TwoWay;
            this.xPTZ_MinZoomLevel.SetBinding(TextBox.IsEnabledProperty, multiBinding);
            
        }

        private void Camera_BorderImageSource_KeyUp(object sender, KeyEventArgs e)
        {
            foreach (FrameworkElement element in this._elementList)
            {
                if (element is CameraControl)
                {
                    (element as CameraControl).BorderImageSource = System.IO.Path.GetFileName(this.xCamera_BorderImageSource.Text);
                }
            }
        }

        private void Camera_BorderColorRectangle_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            Color oldColor = ((sender as Shape).Fill as SolidColorBrush).Color;
            Color newColor = ColorSelectDialog(oldColor);
            if (newColor.Equals(oldColor) == false)
            {
                (sender as Shape).Fill = new SolidColorBrush(newColor);

                //if (this._element is CameraControl)
                //    (this._element as CameraControl).BorderBrush = new SolidColorBrush(newColor);

                foreach (FrameworkElement camCtrl in this._elementList)
                {
                    if (camCtrl is CameraControl)
                        (camCtrl as CameraControl).BorderBrush = new SolidColorBrush(newColor);    
                }
                UpdateBindingSource(xCamera_BorderColorRectangle, Shape.FillProperty);
            }

        }

        void Camera_TextColorRectangle_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            Color oldColor = ((sender as Shape).Fill as SolidColorBrush).Color;
            Color newColor = ColorSelectDialog(oldColor);
            if (newColor.Equals(oldColor) == false)
            {
                (sender as Shape).Fill = new SolidColorBrush(newColor);
                UpdateBindingSource(xCamera_TextColorRectangle, Shape.FillProperty);
            }

            
        }

        void Camera_OutlineColorRectangle_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            Color oldColor = ((sender as Shape).Fill as SolidColorBrush).Color;
            Color newColor = ColorSelectDialog(oldColor);
            if (newColor.Equals(oldColor) == false)
            {
                (sender as Shape).Fill = new SolidColorBrush(newColor);
                UpdateBindingSource(sender as FrameworkElement, Shape.FillProperty);
            }

            
        }

        private Color ColorSelectDialog(Color defaultColor)
        {
            //ColorPickerDialog cPicker = new ColorPickerDialog();
            //cPicker.Owner = this._parent;
            //cPicker.StartingColor = defaultColor;
            //if (cPicker.ShowDialog().Value == true)
            //    return cPicker.SelectedColor;
            //else
            //    return defaultColor;

            return Colors.Black;
        }

        private void Button_BorderImageSource_Click(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
            dlg.Multiselect = false;

            dlg.Filter = Public.ResourceExt(Public.ResourceType.Image);

            if (dlg.ShowDialog().Value == false)
                return;

            // 파일 확장자 유효성 검사 - mhkim
            string fileExt = System.IO.Path.GetExtension(dlg.FileName);
            if (string.IsNullOrEmpty(fileExt) || string.Compare(fileExt, "lnk") == 0)
            {
                MessageBox.Show("File extension is invalid.");
                return;
            }

            if (!File.Exists(dlg.FileName))
            {
                MessageBox.Show("File does not exist");
                return;
            }

            string fileName = this._resourceManager.AddResource(Public.ResourceType.Image, dlg.FileName, true);
            //xCamera_BorderImageSource.Text = System.IO.Path.GetFileName(fileName);

            foreach (FrameworkElement element in this._elementList)
            {
                if (element is CameraControl)
                    (element as CameraControl).BorderImageSource = System.IO.Path.GetFileName(fileName);            
            }
        }
    }
}
