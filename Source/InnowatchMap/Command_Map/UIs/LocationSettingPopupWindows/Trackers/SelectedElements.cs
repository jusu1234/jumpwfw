﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SelectedElements.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Defines the SelectedElements type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.Console.UIs.LocationSettingPopupWindows.Trackers
{
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Media;

    using GMap.NET;

    using Innotive.InnoWatch.Commons.BaseControls;
    using Innotive.InnoWatch.Commons.EventArguments;
    using Innotive.InnoWatch.Commons.Publics;
    using Innotive.InnoWatch.Commons.Utils;
    using Innotive.InnoWatch.Console.Models;
    using Innotive.InnoWatch.Console.UIs;
    using Innotive.InnoWatch.DLLs.BookmarkControls;    
    using Innotive.InnoWatch.DLLs.LayoutControls;
    using Innotive.InnoWatch.DLLs.MapControls;

    /// <summary>
    /// 에디터에서 선택된 엘리먼트에 대한 클래스.
    /// List(BaseControl) 을 사용하기 위해 구현 상속함.
    /// </summary>
    public class SelectedElements : List<BaseControl>, ISelectedElements
    {
        public const string NORMALFILL = "NormalFill";
        public const string MOUSEOVERFILL = "MouseOverFill";
        public const string MOUSEDOWNFILL = "MouseDownFill";
        public const string NORMALSTROKE = "NormalStroke";
        public const string MOUSEOVERSTROKE = "MouseOverStroke";
        public const string MOUSEDOWNSTROKE = "MouseDownStroke";
        public const string ISFILL = "IsFill";
        public const string ISSTROKE = "IsStroke";

        public const string ISLOCKEDIT = "IsLockEdit";
        public const string STROKETHICKNESS = "StrokeThickness";

        public const string FONTSIZE = "FontSize";
        public const string FOREGROUND = "Foreground";
        public const string BACKGROUND = "Background";
        public const string TEXT = "Text";
        public const string FONTMAMILY = "FontFamily";
        public const string ALIGNMENT = "Alignment";
        public const string PADDING = "Padding";
        
        public const string INSIDEBACKGROUND = "LayoutInsideBackground";
        public const string OUTSIDEBACKGROUND = "LayoutOutsideBackground";

        private readonly LocationSettingPopupWindow owner;

        /// <summary>
        /// Initializes a new instance of the <see cref="SelectedElements"/> class.
        /// </summary>
        public SelectedElements(LocationSettingPopupWindow locationSettingPopupWindow)
        {
            this.owner = locationSettingPopupWindow;
        }

        /// <summary>
        /// INotifyPropertyChanged 인터페이스에 따른 기본 구현.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Gets EditableElements : 선택된 엘리먼트 중 에디팅할수 있는 엘리먼트.
        /// </summary>
        public List<BaseControl> EditableElements
        {
            get { return GetEditableElements(); }
        }

        /// <summary>
        /// Gets EditableElement : 선택된 엘리먼트 중 IsLockEdit가 False인 것들중 마지막으로 엘리먼트.
        /// </summary>
        public BaseControl EditableElement
        {
            get { return GetEditingElement(); }
        }

        public BaseControl Parent
        {
            get
            {
                return this.GetParent();
            }
        }

        /// <summary>
        /// Gets or sets Name.
        /// </summary>
        public NameGuid Name
        {
            get
            {
                return this.GetName();
            }

            set
            {
                this.SetName(value);
                this.OnPropertyChanged("Name");
            }
        }

        /// <summary>
        /// Gets or sets NormalFill.
        /// </summary>
        public Brush NormalFill
        {
            get
            {
                return this.GetNormalFill();
            }

            set
            {
                this.SetNormalFill(value);
                this.OnPropertyChanged(NORMALFILL);
            }
        }

        /// <summary>
        /// Gets or sets MouseOverFill.
        /// </summary>
        public Brush MouseOverFill
        {
            get
            {
                return this.GetMouseOverFill();
            }

            set
            {
                this.SetMouseOverFill(value);
                this.OnPropertyChanged(MOUSEOVERFILL);
            }
        }

        /// <summary>
        /// Gets or sets MouseOverFill.
        /// </summary>
        public Brush MouseDownFill
        {
            get
            {
                return this.GetMouseDownFill();
            }

            set
            {
                this.SetMouseDownFill(value);
                this.OnPropertyChanged(MOUSEDOWNFILL);
            }
        }

        /// <summary>
        /// Gets or sets NormalFill.
        /// </summary>
        public Brush NormalStroke
        {
            get
            {
                return this.GetNormalStroke();
            }

            set
            {
                this.SetNormalStroke(value);
                this.OnPropertyChanged(NORMALSTROKE);
            }
        }

        /// <summary>
        /// Gets or sets MouseOverFill.
        /// </summary>
        public Brush MouseOverStroke
        {
            get
            {
                return this.GetMouseOverStroke();
            }

            set
            {
                this.SetMouseOverStroke(value);
                this.OnPropertyChanged(MOUSEOVERSTROKE);
            }
        }

        /// <summary>
        /// Gets or sets MouseOverFill.
        /// </summary>
        public Brush MouseDownStroke
        {
            get
            {
                return this.GetMouseDownStroke();
            }

            set
            {
                this.SetMouseDownStroke(value);
                this.OnPropertyChanged(MOUSEDOWNSTROKE);
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether IsFill.
        /// </summary>
        public bool IsFill
        {
            get
            {
                return this.GetIsFill();
            }

            set
            {
                this.SetIsFill(value);
                this.OnPropertyChanged(ISFILL);
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether IsStroke.
        /// </summary>
        public bool IsStroke
        {
            get
            {
                return this.GetIsStroke();
            }

            set
            {
                this.SetIsStroke(value);
                this.OnPropertyChanged(ISSTROKE);
            }
        }

        /// <summary>
        /// Gets or sets FontSize.
        /// </summary>
        public double? FontSize
        {
            get
            {
                return this.GetFontSize();
            }

            set
            {
                this.SetFontSize(value);
                this.OnPropertyChanged("FontSize");
            }
        }

        /// <summary>
        /// Gets or sets Foreground.
        /// </summary>
        public Brush Foreground
        {
            get
            {
                return this.GetForeground();
            }

            set
            {
                this.SetForeground(value);
                this.OnPropertyChanged("Foreground");
            }
        }

        /// <summary>
        /// Gets or sets Background.
        /// </summary>
        public Brush Background
        {
            get
            {
                return this.GetBackground();
            }

            set
            {
                this.SetBackground(value);
                this.OnPropertyChanged("Background");
            }
        }

        /// <summary>
        /// Gets or sets Text.
        /// </summary>
        public string Text
        {
            get
            {
                return this.GetText();
            }

            set
            {
                this.SetText(value);
                this.OnPropertyChanged("Text");
            }
        }

        /// <summary>
        /// Gets or sets FontFamily.
        /// </summary>
        public FontFamily FontFamily
        {
            get
            {
                return this.GetFontFamily();
            }

            set
            {
                this.SetFontFamily(value);
                this.OnPropertyChanged("FontFamily");
            }
        }

        /// <summary>
        /// Gets or sets Alignment.
        /// </summary>
        public TextAlignment? Alignment
        {
            get
            {
                return this.GetAlignment();
            }

            set
            {
                this.SetAlignment(value);
                this.OnPropertyChanged("Alignment");
            }
        }

        public Thickness Padding
        {
            get
            {
                return this.GetPadding();
            }

            set
            {
                this.SetPadding(value);
                this.OnPropertyChanged("Padding");
            }
        }

        public double? PaddingLeft
        {
            get
            {
                return this.GetPaddingLeft();
            }

            set
            {
                this.SetPaddingLeft(value);
                this.OnPropertyChanged("PaddingLeft");
            }
        }

        public double? PaddingTop
        {
            get
            {
                return this.GetPaddingTop();
            }

            set
            {
                this.SetPaddingTop(value);
                this.OnPropertyChanged("PaddingTop");
            }
        }

        public double? PaddingRight
        {
            get
            {
                return this.GetPaddingRight();
            }

            set
            {
                this.SetPaddingRight(value);
                this.OnPropertyChanged("PaddingLeft");
            }
        }

        public double? PaddingBottom
        {
            get
            {
                return this.GetPaddingBottom();
            }

            set
            {
                this.SetPaddingBottom(value);
                this.OnPropertyChanged("PaddingLeft");
            }
        }

        /// <summary>
        /// Gets or sets MouseOverFill.
        /// </summary>
        public double StrokeThickness
        {
            get
            {
                return this.GetStrokeThickness();
            }

            set
            {
                this.SetStrokeThickness(value);
                this.OnPropertyChanged(STROKETHICKNESS);
            }
        }

        /// <summary>
        /// Gets or sets Left.
        /// </summary>
        public double? Left
        {
            get
            {
                return this.GetLeft();
            }

            set
            {
                this.SetLeft(value);
                this.OnPropertyChanged("Left");
            }
        }

        /// <summary>
        /// Gets or sets Left.
        /// </summary>
        public double? Top
        {
            get
            {
                return this.GetTop();
            }

            set
            {
                this.SetTop(value);
                this.OnPropertyChanged("Top");
            }
        }

        /// <summary>
        /// Gets or sets Left.
        /// </summary>
        public double? Width
        {
            get
            {
                return this.GetWidth();
            }

            set
            {
                if (value < 0)
                {
                    return;
                }

                this.SetWidth(value);
                this.OnPropertyChanged("Width");
            }
        }

        /// <summary>
        /// Gets or sets Left.
        /// </summary>
        public double? Height
        {
            get
            {
                return this.GetHeight();
            }

            set
            {
                if (value < 0)
                {
                    return;
                }

                this.SetHeight(value);
                this.OnPropertyChanged("Height");
            }
        }

        /// <summary>
        /// Gets or sets Opacity.
        /// </summary>
        public double? Opacity
        {
            get
            {
                return this.GetOpacity();
            }

            set
            {
                this.SetOpacity(value);
                this.OnPropertyChanged("Opacity");
            }
        }

        public bool? PlayVisible
        {
            get
            {
                return this.GetPlayVisible();
            }
            set
            {
                this.SetPlayVisible(value);
                this.OnPropertyChanged("PlayVisible");
            }
        }

        /// <summary>
        /// Gets or sets a value indicating LockType.
        /// </summary>
        public LockTypes? LockType
        {
            get
            {
                return this.GetLockType();
            }

            set
            {
                if (value == null) return;
                this.SetLockType((LockTypes)value);
                this.OnPropertyChanged("LockType");
            }
        }

        public bool? IsAppearanceInAllLevel
        {
            get
            {
                return this.GetIsAppearanceInAllLevel();
            }
            set
            {
                this.SetIsAppearanceInAllLevel(value);
                this.OnPropertyChanged("IsAppearanceInAllLevel");
            }
        }

        public double? AppearanceMinLevel
        {
            get
            {
                return this.GetAppearanceMinLevel();
            }
            set
            {
                this.SetAppearanceMinLevel(value);
                this.OnPropertyChanged("AppearanceMinLevel");
            }
        }

        /// <summary>
        /// Layout 관련 속성.
        /// </summary>
        public bool? IsZoomEnabled
        {
            get
            {
                return this.GetIsZoomEnabled();
            }
            set
            {
                this.SetIsIsZoomEnabled(value);
                this.OnPropertyChanged("IsZoomEnable");
            }
        }

        public double? CenterLatitude
        {
            get
            {
                return this.GetCenterLatitude();
            }
            set
            {
                this.SetCenterLatitude(value);
            }
        }

        public double? CenterLongitude
        {
            get
            {
                return this.GetCenterLongitude();
            }
            set
            {
                this.SetCenterLongitude(value);
            }
        }

        public double? MapLatitude
        {
            get
            {
                return this.GetMapLatitude();
            }
            set
            {
                this.SetMapLatitude(value);
            }
        }

        public double? MapLongitude
        {
            get
            {
                return this.GetMapLongitude();
            }
            set
            {
                this.SetMapLongitude(value);
            }
        }

        public double? MapZoom
        {
            get
            {
                return this.GetMapZoom();
            }
            set
            {
                this.SetMapZoom(value);
            }
        }

        public double? MinZoomPercentage
        {
            get
            {
                return this.GetMinZoomPercentage();
            }
            set
            {
                this.SetMinZoomPercentage(value);
            }
        }

        public double? MaxZoomPercentage
        {
            get
            {
                return this.GetMaxZoomPercentage();
            }
            set
            {
                this.SetMaxZoomPercentage(value);
            }
        }

        public double? ZoomInRatio
        {
            get
            {
                return this.GetZoomInRatio();
            }
            set
            {
                this.SetZoomInRatio(value);
            }
        }

        public double? ZoomOutRatio
        {
            get
            {
                return this.GetZoomOutRatio();
            }
            set
            {
                this.SetZoomOutRatio(value);
            }
        }

        public double? AppearanceMaxLevel
        {
            get
            {
                return this.GetAppearanceMaxLevel();
            }
            set
            {
                this.SetAppearanceMaxLevel(value);
                this.OnPropertyChanged("AppearanceMaxLevel");
            }
        }

        /// <summary>
        /// 엘리먼트들의 부모의 현재 ZoomRatio
        /// </summary>
        public double? CurrentZoomPercentage
        {
            get
            {
                return this.GetCurrentZoomPercentage();
            }
            set
            {
                this.SetCurrentZoomPercentage(value);
                this.OnPropertyChanged("CurrentZoomPercentage");
            }
        }

        /// <summary>
        /// Gets or sets InsideBackground.
        /// </summary>
        public Brush InsideBackground
        {
            get
            {
                return this.GetInsideBackground();
            }

            set
            {
                this.SetInsideBackground(value);
                this.OnPropertyChanged(INSIDEBACKGROUND);
            }
        }

        public Brush OutsideBackground
        {
            get
            {
                return this.GetOutsideBackground();
            }

            set
            {
                this.SetOutsideBackground(value);
                this.OnPropertyChanged(OUTSIDEBACKGROUND);
            }
        }

        public double? OriginWidth
        {
            get
            {
                return this.GetOriginWidth();
            }
            set
            {
                this.SetOriginWidth(value);
            }
        }

        public double? OriginHeight
        {
            get
            {
                return this.GetOriginHeight();
            }
            set
            {
                this.SetOriginHeight(value);
            }
        }

        public string MovieSourcePath
        {
            get
            {
                return this.GetMovieSourcePath();
            }
            set
            {
                this.SetMovieSourcePath(value);
            }
        }

        public bool? IsMovieMute
        {
            get
            {
                return GetIsMovieMute();
            }
            set
            {
                this.SetIsMovieMute(value);
            }
        }

        public Visibility? MovieControlPanelVisibility
        {
            get
            {
                return this.GetMovieControlPanelVisibility();
            }
            set
            {
                this.SetMovieControlPanelVisibility(value);
            }
        }

        public bool? MovieControlPanelAutoHide
        {
            get
            {
                return this.GetMovieControlPanelAutoHide();
            }
            set
            {
                this.SetMovieControlPanelAutoHide(value);
            }
        }

        public void MoviePlay()
        {
            
        }

        public void MovieStop()
        {
            
        }



        public void SetLTWH()
        {
            this.OnPropertyChanged("Left");
            this.OnPropertyChanged("Top");
            this.OnPropertyChanged("Width");
            this.OnPropertyChanged("Height");
        }

        public void SetGisPosition()
        {
            this.OnPropertyChanged("CenterLatitude");
            this.OnPropertyChanged("CenterLongitude");

            this.OnPropertyChanged("MapLatitude");
            this.OnPropertyChanged("MapLongitude");

            this.OnPropertyChanged("LeftGis");
            this.OnPropertyChanged("TopGis");
            this.OnPropertyChanged("RightGis");
            this.OnPropertyChanged("BottomGis");
        }

        /// <summary>
        /// 편집가능한 선택된 엘리먼트들을 Point 만큼 이동시킨다.
        /// </summary>
        /// <param name="point">
        /// The point.
        /// </param>
        public void Move(Point point)
        {

            foreach (BaseControl control in this)
            {
                if (control.IsLockEdit || !control.EditVisible)
                {
                    continue;
                }
            
                if (point.X.Equals(0) && point.Y.Equals(0)) continue;

                control.Left = Canvas.GetLeft(control) + point.X;
                control.Top = Canvas.GetTop(control) + point.Y;
            }

            this.OnPropertyChanged("Left");
            this.OnPropertyChanged("Top");


        }

        /// <summary>
        /// 선택된 엘리먼트 리스트를 초기화한다.
        /// </summary>
        public new void Clear()
        {
            base.Clear();
        }

        /// <summary>
        /// 목록을 초기화 하고 이벤트를 발생시킨다.
        /// </summary>
        public void ClearWithEvent()
        {
            if (Count <= 0)
            {
                return;
            }

            Clear();
            if (this.owner.MapLayoutControl != null)
            {
                this.owner.MapLayoutControl.RaiseEvent(
                    new SelectedElementsChangedRoutedEventArgs(
                        LayoutControl.eSelectedElementsChangedEvent, this.owner.MapLayoutControl));
            }
        }

        /// <summary>
        /// 엘리먼트를 부모로부터 제거한다.
        /// </summary>
        public void DeleteAll()
        {
            // 선택된 것들을 돌면서 삭제
            foreach (var element in this)
            {
                Debug.Assert(this.owner != null, "LocationSettingPopupWindow가 존재하지 않습니다.");
                this.owner.MapControl.RemoveElement(element);
            }
        }

        /// <summary>
        /// FocusedLayout의 모든 자식들을 선택하고 이벤트를 발생시킨다.
        /// </summary>
        public void AddAllInFocusedLayout()
        {
            var ownerFocusedBaseLayoutControl = this.owner.MapLayoutControl as IBaseLayoutControl;

            if (ownerFocusedBaseLayoutControl == null)
            {
                return;
            }

            Clear();

            foreach (var element in ownerFocusedBaseLayoutControl.GetElements())
            {
                if (element is IFoundationElement)
                {
                    continue;
                }

                if ((element as BookmarkControl) != null)
                {
                    continue;
                }

                var baseControl = element as BaseControl;
                if (baseControl == null)
                {
                    continue;
                }

                if (baseControl.IsLockEdit || !baseControl.EditVisible)
                {
                    continue;
                }

                if (!Contains(baseControl))
                {
                    base.Add(element as BaseControl);
                }
            }

            if (Count == 0)
            {
                Add(this.owner.MapLayoutControl);
            }

            this.owner.MapLayoutControl.RaiseEvent(
                new SelectedElementsChangedRoutedEventArgs(
                    LayoutControl.eSelectedElementsChangedEvent, this.owner.MapLayoutControl));
        }

        /// <summary>
        /// 엘리먼트를 추가하고 이벤트를 발생시킨다.
        /// </summary>
        /// <param name="list">
        /// The list.
        /// </param>
        public void AddElements(List<BaseControl> list, BaseLayoutControl baseLayoutControl)
        {
            if (list.Count == 0)
            {
                return;
            }

            if (baseLayoutControl == null)
            {
                return;
            }

            foreach (var element in list)
            {
                if (!Contains(element))
                {
                    base.Add(element);
                }
            }

            baseLayoutControl.RaiseEvent(new SelectedElementsChangedRoutedEventArgs(LayoutControl.eSelectedElementsChangedEvent, baseLayoutControl));
        }

        /// <summary>
        /// 엘리먼트를 추가하고 이벤트를 발생시킨다.
        /// </summary>
        /// <param name="element">
        /// The element.
        /// </param>
        public new void Add(BaseControl element)
        {
            if (Contains(element))
            {
                return;
            }

            if(element is MapControl)
            {
                var mapControl = element as MapControl;
                mapControl.Map.OnPositionChanged += this.Map_OnPositionChanged;
                mapControl.Map.OnMapZoomChanged += this.Map_OnMapZoomChanged;
            }

            base.Add(element);

            var layoutControl = element as LayoutControl;
            if (layoutControl != null) layoutControl.ChangedCurrentZoomPercentage += this.LayoutControlChangedCurrentZoomPercentage;

            this.owner.MapLayoutControl.RaiseEvent(
                new SelectedElementsChangedRoutedEventArgs(LayoutControl.eSelectedElementsChangedEvent, this.owner.MapLayoutControl));
        }

        /// <summary>
        /// 엘리먼트를 제거하고 이벤트를 발생시킨다.
        /// </summary>
        /// <param name="element">
        /// The element.
        /// </param>
        public new void Remove(BaseControl element)
        {
            if (!Contains(element))
            {
                return;
            }


            if (element is MapControl)
            {
                var mapControl = element as MapControl;
                mapControl.Map.OnPositionChanged -= this.Map_OnPositionChanged;
                mapControl.Map.OnMapZoomChanged -= this.Map_OnMapZoomChanged;
            }

            base.Remove(element);

            this.owner.MapLayoutControl.RaiseEvent(
                new SelectedElementsChangedRoutedEventArgs(
                    LayoutControl.eSelectedElementsChangedEvent, this.owner.MapLayoutControl));
        }

        /// <summary>
        /// The clone.
        /// </summary>
        /// <param name="list">
        /// The list.
        /// </param>
        public void Clone(ref List<BaseControl> list)
        {
            list.Clear();

            list.AddRange(this);
        }

        /// <summary>
        /// 해당 엘리먼트를 레이아웃의 포커스레이아웃으로 설정한다.
        /// </summary>
        /// <param name="selectedElement">
        /// The selected element.
        /// </param>
        /// <param name="layoutControl">
        /// The layout control.
        /// </param>
        public void SetFocusedLayout(BaseControl selectedElement, BaseLayoutControl layoutControl)
        {
            BaseLayoutControl target;

            // 선택된 엘리먼트가 레이아웃 콘트롤이면
            if (selectedElement is BaseLayoutControl)
            {
                target = selectedElement as BaseLayoutControl;
            }
            else
            {
                target = selectedElement.ParentBaseLayoutControl;
            }

            if (target == null)
            {
                return;
            }

            var rootLayoutControl = LayoutUtils.GetRootLayoutControl(selectedElement);

            if (rootLayoutControl != null)
            {
                rootLayoutControl.ChildrenSetFocused(false);
                this.Clear();
            }

            var baseLayoutControl = target as IBaseLayoutControl;
            if (baseLayoutControl != null)
            {
                baseLayoutControl.IsFocusedLayout = true;
            }

            layoutControl.RaiseEvent(
                new FocusedLayoutChangedRoutedEventArgs(LayoutControl.eFocusedLayoutChangedEvent, target));
        }

        /// <summary>
        /// 해당 프로퍼티문자열에 해당하는 DP를 반환한다.
        /// </summary>
        /// <param name="propertyName">
        /// The property name.
        /// </param>
        /// <returns>
        /// The DP. 
        /// </returns>
        public DependencyProperty GetDependencyProperty(string propertyName)
        {
            System.Diagnostics.Debug.Assert(!string.IsNullOrEmpty(propertyName), "string.IsNullOrEmpty(propertyName)");

            if (string.IsNullOrEmpty(propertyName))
            {
                return null;
            }

            DependencyProperty dp;

            switch (propertyName)
            {
                case NORMALFILL:
                    dp = BaseShapeControl.NormalFillProperty;
                    break;
                case MOUSEOVERFILL:
                    dp = BaseShapeControl.MouseOverFillProperty;
                    break;
                case MOUSEDOWNFILL:
                    dp = BaseShapeControl.MouseDownFillProperty;
                    break;
                case NORMALSTROKE:
                    dp = BaseShapeControl.NormalStrokeProperty;
                    break;
                case MOUSEOVERSTROKE:
                    dp = BaseShapeControl.MouseOverStrokeProperty;
                    break;
                case MOUSEDOWNSTROKE:
                    dp = BaseShapeControl.MouseDownStrokeProperty;
                    break;
                case ISFILL:
                    dp = BaseShapeControl.IsFillProperty;
                    break;
                case ISSTROKE:
                    dp = BaseShapeControl.IsStrokeProperty;
                    break;
                case STROKETHICKNESS:
                    dp = BaseShapeControl.StrokeThicknessProperty;
                    break;               
                default:
                    dp = null;
                    break;
            }

            return dp;
        }

        public void SetOpacity(double value)
        {
            foreach (var element in this)
            {
                element.Opacity = value;
            }
        }

        /// <summary>
        /// INotifyPropertyChanged 인터페이스에 따른 기본 구현.
        /// </summary>
        /// <param name="info">
        /// The info.
        /// </param>
        protected void OnPropertyChanged(string info)
        {
            var handler = this.PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(info));
            }
        }

        private BaseLayoutControl GetParent()
        {
            BaseLayoutControl result = null;
            foreach (var control in EditableElements)
            {
                var parentControl = control.ParentBaseLayoutControl;
                if (parentControl == null) return null;

                if (result == null)
                {
                    result = control.ParentBaseLayoutControl;
                    continue;
                }

                if (result != control.ParentBaseLayoutControl)
                {
                    return null;
                }
            }
            return result;
        }

        private void SetPlayVisible(bool? value)
        {
            if (value == null) return;

            foreach (var control in EditableElements)
            {
                control.PlayVisible = (bool)value;
            }
        }

        private bool? GetPlayVisible()
        {
            bool? result = null;

            foreach (var control in EditableElements)
            {
                if (result == null)
                {   
                    result = control.PlayVisible;
                    continue;
                }

                if (result != control.PlayVisible) return null;
                
            }

            return result;
        }

        private double? GetCenterLatitude()
        {
            double? result = null;
            foreach (var control in EditableElements)
            {
                if (result == null)
                {
                    result = control.CenterLatitude;
                    continue;
                }

                if (result != control.CenterLatitude) return null;
            }
            return result;
        }

        private void SetCenterLatitude(double? value)
        {
            if (value == null) return;

            foreach (var control in this.EditableElements)
            {
                var mapControl = control.ParentBaseLayoutControl as MapControl;
                if (mapControl == null) continue;

                var center = mapControl.Map.FromLatLngToLocal(new PointLatLng((double)value, control.CenterLongitude));
                control.Top = center.Y - control.Height / 2;
            }
        }

        private double? GetCenterLongitude()
        {
            double? result = null;
            foreach (var control in this.EditableElements)
            {
                if (result == null)
                {
                    result = control.CenterLongitude;
                    continue;
                }

                if (result != control.CenterLongitude) return null;
            }
            return result;
        }

        private void SetCenterLongitude(double? value)
        {
            if (value == null) return;

            foreach (var control in this.EditableElements)
            {
                var mapControl = control.ParentBaseLayoutControl as MapControl;
                if(mapControl == null) continue;

                var center = mapControl.Map.FromLatLngToLocal(new PointLatLng(control.CenterLatitude, (double)value));
                control.Left = center.X - control.Width /2;
            }
        }

        private double? GetMapLatitude()
        {
            double? result = null;
            foreach (var control in EditableElements)
            {
                var mapControl = control as MapControl;
                if (mapControl == null)
                {
                    continue;
                }

                if (result == null)
                {
                    result = mapControl.MapLatitude;
                    continue;
                }

                if (result != mapControl.MapLatitude) return null;
            }
            return result;
        }

        private void SetMapLatitude(double? value)
        {
            if (value == null) return;

            foreach (var control in EditableElements)
            {
                var mapControl = control as MapControl;
                if (mapControl != null)
                {
                    mapControl.MapLatitude = (double)value;
                }
            }
        }

        private double? GetMapLongitude()
        {
            double? result = null;
            foreach (var control in this.EditableElements)
            {
                var mapControl = control as MapControl;
                if (mapControl == null)
                {
                    continue;
                }

                if (result == null)
                {
                    result = mapControl.MapLongitude;
                    continue;
                }

                if (result != mapControl.MapLongitude) return null;
            }
            return result;
        }

        private void SetMapLongitude(double? value)
        {
            if (value == null) return;

            foreach (var control in this.EditableElements)
            {
                var mapControl = control as MapControl;
                if (mapControl != null)
                {
                    mapControl.MapLongitude = (double)value;
                }
            }
        }

        private double? GetMinZoomPercentage()
        {
            double? result = null;

            foreach (var control in EditableElements)
            {
                var layaoutControl = control as LayoutControl;
                if (layaoutControl == null) continue;

                if (result == null)
                {
                    result = layaoutControl.MinRatio;
                    continue;
                }

                if (result != layaoutControl.MinRatio) return null;
            }

            return result * 100;
        }

        private void SetMinZoomPercentage(double? value)
        {
            if (value == null) return;

            foreach (var control in EditableElements)
            {
                var layoutControl = control as LayoutControl;
                if (layoutControl == null) continue;

                layoutControl.MinRatio = (double)value / 100;
            }
        }

        private double? GetZoomInRatio()
        {
            double? result = null;

            foreach (var control in EditableElements)
            {
                var layoutControl = control as LayoutControl;
                if (layoutControl == null) continue;
                if (result == null)
                {
                    result = layoutControl.ZoomInRatio;
                    continue;
                }
                if (result != layoutControl.ZoomInRatio) return null;
            }
            return result;
        }

        private void SetZoomInRatio(double? value)
        {
            if (value == null) return;

            foreach (var control in EditableElements)
            {
                var layoutControl = control as LayoutControl;
                if (layoutControl == null) continue;

                layoutControl.ZoomInRatio = (double)value;
            }
        }

        private double? GetZoomOutRatio()
        {
            double? result = null;

            foreach (var control in EditableElements)
            {
                var layoutControl = control as LayoutControl;
                if (layoutControl == null) continue;
                if (result == null)
                {
                    result = layoutControl.ZoomOutRatio;
                    continue;
                }
                if (result != layoutControl.ZoomOutRatio) return null;
            }
            return result;
        }

        private void SetZoomOutRatio(double? value)
        {
            if (value == null) return;

            foreach (var control in EditableElements)
            {
                var layoutControl = control as LayoutControl;
                if (layoutControl == null) continue;

                layoutControl.ZoomOutRatio = (double)value;
            }
        } 

        private double? GetMaxZoomPercentage()
        {
            double? result = null;
            foreach (var control in EditableElements)
            {
                var layaoutControl = control as LayoutControl;
                if (layaoutControl == null) continue;

                if (result == null)
                {
                    result = layaoutControl.MaxRatio;
                    continue;
                }

                if (result != layaoutControl.MaxRatio) return null;
            }

            return result * 100;
        }

        private void SetMaxZoomPercentage(double? value)
        {
            if (value == null) return;

            foreach (var control in EditableElements)
            {
                var layoutControl = control as LayoutControl;
                if (layoutControl == null) continue;

                layoutControl.MaxRatio = (double)value / 100;
            }
        }

        private void SetMapZoom(double? value)
        {
            if (value == null) return;

            foreach (var control in EditableElements)
            {
                if (!(control is MapControl)) continue;
                (control as MapControl).MapZoom = (double)value;
            }
        }

        private double? GetMapZoom()
        {
            double? result = null;
            foreach (var control in EditableElements)
            {
                var mapControl = control as MapControl;
                if (mapControl == null)
                {
                    continue;
                }

                if (result == null)
                {
                    result = mapControl.MapZoom;
                    continue;
                }

                if (result != mapControl.MapZoom) return null;
            }
            return result;
        }

        private void SetIsIsZoomEnabled(bool? value)
        {
            if (value == null) return;

            foreach (var control in EditableElements)
            {
                if (!(control is LayoutControl)) continue;
                (control as LayoutControl).IsZoomEnabled = (bool)value;
            }
        }

        private bool? GetIsZoomEnabled()
        {
            bool? result = null;

            foreach (var control in EditableElements)
            {
                if (!(control is LayoutControl)) return null;

                if (result == null)
                {
                    result = (control as LayoutControl).IsZoomEnabled;
                    continue;
                }

                if (result != (control as LayoutControl).IsZoomEnabled) return null;
            }

            return result;
        }

        private void SetAppearanceMinLevel(double? value)
        {
            if (value == null) return;

            foreach (var control in EditableElements)
            {
                control.AppearanceMinLevel = (double)value;
            }
        }

        private double? GetAppearanceMinLevel()
        {
            double? result = null;

            foreach (var control in EditableElements)
            {
                if (result == null)
                {
                    result = control.AppearanceMinLevel;
                    continue;
                }

                if (result != control.AppearanceMinLevel) return null;
            }

            return result;
        }

        private void SetAppearanceMaxLevel(double? value)
        {
            if (value == null) return;

            foreach (var control in EditableElements)
            {
                control.AppearanceMaxLevel = (double)value;
            }
        }

        private double? GetAppearanceMaxLevel()
        {
            double? result = null;

            foreach (var control in EditableElements)
            {
                if (result == null)
                {
                    result = control.AppearanceMaxLevel;
                    continue;
                }

                if (result != control.AppearanceMaxLevel) return null;
            }

            return result;
        }

        private void SetCurrentZoomPercentage(double? value)
        {
            if (value == null) return;
            if (value == double.NaN) return;

            foreach (var control in this.EditableElements)
            {
                var layoutControl = control as LayoutControl;
                if (layoutControl == null) continue;
                layoutControl.CurrentZoomPercentage = (double)value;
            }
        }

        private double? GetCurrentZoomPercentage()
        {
            double? result = null;

            foreach (var control in EditableElements)
            {
                var layoutControl = control as LayoutControl;

                if (layoutControl == null) continue;
                if (result == null)
                {
                    result = layoutControl.CurrentZoomPercentage;
                    continue;
                }

                if (result != layoutControl.CurrentZoomPercentage) return null;
            }

            return result;
        }

        private void SetIsAppearanceInAllLevel(bool? value)
        {
            if (value == null) return;

            foreach (var control in this.EditableElements)
            {
                control.IsAppearInAllLevel = (bool)value;
            }
        }

        private bool? GetIsAppearanceInAllLevel()
        {
            bool? result = null;

            foreach (var control in EditableElements)
            {
                if (result == null)
                {
                    result = control.IsAppearInAllLevel;
                    continue;
                }

                if (result != control.IsAppearInAllLevel) return null;
            }

            return result;
        }

        private void SetOriginWidth(double? value)
        {
            if (value == null) return;

            foreach (var control in EditableElements)
            {
                var layoutControl = control as LayoutControl;
                if (layoutControl == null) continue;

                layoutControl.OriginWidth = (double)value;
            }
        }

        private double? GetOriginWidth()
        {
            double? result = null;

            foreach (var control in EditableElements)
            {
                var layoutControl = control as LayoutControl;
                if (layoutControl == null) continue;

                if (result == null)
                {
                    result = layoutControl.OriginWidth;
                    continue;
                }

                if (result != layoutControl.OriginWidth) return null;
            }

            return result;
        }

        private void SetOriginHeight(double? value)
        {
            if (value == null) return;

            foreach (var control in EditableElements)
            {
                var layoutControl = control as LayoutControl;
                if (layoutControl == null) continue;

                layoutControl.OriginHeight = (double)value;
            }
        }

        private double? GetOriginHeight()
        {
            double? result = null;

            foreach (var control in EditableElements)
            {
                var layoutControl = control as LayoutControl;
                if (layoutControl == null) continue;

                if (result == null)
                {
                    result = layoutControl.OriginHeight;
                    continue;
                }

                if (result != layoutControl.OriginHeight) return null;
            }

            return result;
        }

        /// <summary>
        /// 레이아웃 콘트롤의 CurrentZoomPercentage가 변경되면 실행되는 메서드.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LayoutControlChangedCurrentZoomPercentage(object sender, System.EventArgs e)
        {
            this.OnPropertyChanged("CurrentZoomPercentage");
        }

        void Map_OnPositionChanged(GMap.NET.PointLatLng point)
        {
            this.OnPropertyChanged("CenterLatitude");
            this.OnPropertyChanged("CenterLongitude");
        }

        void Map_OnMapZoomChanged()
        {
            this.OnPropertyChanged("MapZoom");
        }

        private void SetName(NameGuid nameGUID)
        {
            var value = nameGUID.Name;

            if (Name.Name == value)
            {
                return;
            }

            // 이름에 사용할 수 없는 Character 체크
            var tmpName = InnoElementNameUtil.MakeCorrectElementName(value);
            if (tmpName != null && value != null)
            {
                if (!Equals(tmpName, value))
                {
                    value = tmpName;
                }
            }

            var typeBaseName = InnoElementNameUtil.GetControlTypeName(EditableElement);
            var uniqueName = LayoutUtils.GetUniqueName(
                EditableElement,
                value,
                this.owner.MapLayoutControl,
                typeBaseName);

            EditableElement.Name = uniqueName;
        }

        private NameGuid GetName()
        {
            
            var selectedElementCount = Count;

            if (selectedElementCount != 0)
            {
                var nameGuid = new NameGuid(this[selectedElementCount - 1].Name, this[selectedElementCount - 1].SyncGUID);
                return nameGuid;
            }
            else
            {
                var nameGuid = new NameGuid(EditableElement.Name, EditableElement.SyncGUID);
                return nameGuid;
            } 
        }

        private void SetLeft(double? value)
        {
            if (value == null) return;

            foreach (var element in EditableElements)
            {
                Canvas.SetLeft(element, (double)value);
            }
        }

        private double? GetLeft()
        {
            double? result = null;

            foreach (var control in this)
            {
                if (result == null)
                {
                    result = Canvas.GetLeft(control);
                    continue;
                }

                if (!(result.Equals(Canvas.GetLeft(control))))
                {
                    return null;
                }
            }
            return result;
        }

        private void SetTop(double? value)
        {
            if (value == null) return;

            foreach (var element in EditableElements)
            {
                Canvas.SetTop(element, (double)value);
            }
        }

        private double? GetTop()
        {
            double? result = null;

            foreach (var control in this)
            {
                if (result == null)
                {
                    result = Canvas.GetTop(control);
                    continue;
                }

                if (result != Canvas.GetTop(control))
                {
                    return null;
                }
            }
            return result;
        }

        private void SetWidth(double? value)
        {
            if (value == null) return;

            foreach (var element in EditableElements)
            {
                element.Width = (double)value;
            }
        }

        private double? GetWidth()
        {
            double? result = null;

            foreach (var control in this)
            {
                if (result == null)
                {
                    result = control.Width;
                    continue;
                }

                if (result != control.Width)
                {
                    return null;
                }
            }
            return result;
        }

        private void SetMovieSourcePath(string value)
        {
            if (value == null) return;

            
        }

        private string GetMovieSourcePath()
        {
            string result = null;

            
            return result;
        }

        private void SetIsMovieMute(bool? value)
        {
            if (value == null) return;

            foreach (var element in EditableElements)
            {
                
            }
        }

        private bool? GetIsMovieMute()
        {
            bool? result = null;

            foreach (var control in EditableElements)
            {
                
            }
            return result;
        }

        private void SetMovieControlPanelVisibility(Visibility? value)
        {
            if (value == null) return;

            foreach (var element in EditableElements)
            {
               
            }
        }

        private Visibility? GetMovieControlPanelVisibility()
        {
            Visibility? result = null;

            foreach (var control in EditableElements)
            {
                
            }
            return result;
        }

        private void SetMovieControlPanelAutoHide(bool? value)
        {
            if (value == null) return;

            foreach (var element in EditableElements)
            {
               
            }
        }

        private bool? GetMovieControlPanelAutoHide()
        {
            bool? result = null;

            foreach (var control in EditableElements)
            {
                
            }
            return result;
        }

        private void SetHeight(double? value)
        {
            if (value == null) return;

            foreach (var element in EditableElements)
            {
                element.Height = (double)value;
            }
        }

        private double? GetHeight()
        {
            double? result = null;

            foreach (var control in this)
            {
                if (result == null)
                {
                    result = control.Height;
                    continue;
                }

                if (!(result.Equals(control.Height)))
                {
                    return null;
                }
            }
            return result;
        }

        /// <summary>
        /// 선택된 엘리먼트의 Opacity가 모두 동일하면 속성값을 반환합니다.
        /// </summary>
        /// <returns>
        /// 다른 값이 존재하면 null을 반환합니다.
        /// </returns>
        private double? GetOpacity()
        {
            double? result = null;
            foreach (var control in this)
            {
                if (result == null)
                {
                    result = control.Opacity;
                    continue;
                }

                if (result != control.Opacity)
                {
                    return null;
                }

                return result;
            }
            return EditableElement.Opacity;
        }

        private void SetOpacity(double? value)
        {
            if (value == null) return;

            foreach (var element in EditableElements)
            {
                element.Opacity = (double)value;
            }
        }

        private Brush GetNormalFill()
        {
           

            return null;
        }

        private void SetNormalFill(Brush value)
        {
            
        }

        private Brush GetMouseOverFill()
        {
           

            return null;
        }

        private void SetMouseOverFill(Brush value)
        {
           
        }

        private Brush GetMouseDownFill()
        {
            
            return null;
        }

        private void SetMouseDownFill(Brush value)
        {
            foreach (var element in EditableElements)
            {
                
            }
        }

        private Brush GetNormalStroke()
        {
           
            return null;
        }

        private void SetNormalStroke(Brush value)
        {
            foreach (var element in EditableElements)
            {
               
            }
        }

        private Brush GetMouseOverStroke()
        {
           
            return null;
        }

        private void SetMouseOverStroke(Brush value)
        {
            foreach (var element in EditableElements)
            {
                
            }
        }

        private Brush GetMouseDownStroke()
        {
           

            return null;
        }

        private void SetMouseDownStroke(Brush value)
        {
            foreach (var element in EditableElements)
            {
               
            }
        }

        private bool GetIsFill()
        {
            
            return true;
        }

        private void SetIsFill(bool value)
        {
            foreach (var element in EditableElements)
            {
                
            }
        }

        private bool GetIsStroke()
        {
            

            return true;
        }

        private void SetIsStroke(bool value)
        {
            foreach (var element in EditableElements)
            {
                
            }
        }

        private double GetStrokeThickness()
        {
            
            return double.NaN;
        }

        private void SetStrokeThickness(double value)
        {
            foreach (var element in EditableElements)
            {
               
            }
        }

        private Brush GetInsideBackground()
        {
            if (!(EditableElement is BaseLayoutControl))
            {
                return null;
            }

            return ((LayoutControl)EditableElement).InsideBackground;
        }

        private void SetInsideBackground(Brush value)
        {
            foreach (var element in EditableElements)
            {
                if (!(EditableElement is LayoutControl))
                {
                    continue;
                }

                ((LayoutControl)element).InsideBackground = value;
            }
        }


        private Brush GetOutsideBackground()
        {
            if (!(EditableElement is BaseLayoutControl))
            {
                return null;
            }

            return ((LayoutControl)EditableElement).OutsideBackground;
        }

        private void SetOutsideBackground(Brush value)
        {
            foreach (var element in EditableElements)
            {
                if (!(EditableElement is LayoutControl))
                {
                    continue;
                }

                ((LayoutControl)element).OutsideBackground = value;
            }
        }

        private double? GetFontSize()
        {
            double? result = null;

            foreach (var control in EditableElements)
            {
                
            }

            return result;
        }

        private void SetFontSize(double? value)
        {
            if (value == null) return;
            foreach (var control in EditableElements)
            {
                
            }
        }

        private Brush GetForeground()
        {
            Brush result = null;

            foreach (var control in EditableElements)
            {
                
            }

            return result;
        }

        private void SetForeground(Brush value)
        {
            if (value == null) return;

            foreach (var control in EditableElements)
            {
                
            }
        }

        private Brush GetBackground()
        {
            Brush result = null;

            foreach (var control in EditableElements)
            {
                
            }

            return result;
        }

        private void SetBackground(Brush value)
        {
            if (value == null) return;

            foreach (var control in EditableElements)
            {
                
            }
        }

        private string GetText()
        {
            string result = null;

            foreach(var control in EditableElements)
            {
                
            }

            return result;
        }

        private void SetText(string value)
        {
            if (value == null) return;

            foreach (var control in EditableElements)
            {
                
            }
        }

        private FontFamily GetFontFamily()
        {
            FontFamily result = null;

            foreach (var control in EditableElements)
            {
                
            }

            return result;
        }

        private void SetFontFamily(FontFamily value)
        {
            if (value == null) return;

            foreach (var element in EditableElements)
            {
               
            }
        }

        private TextAlignment? GetAlignment()
        {
            TextAlignment? result = null;

            foreach (var control in EditableElements)
            {
                

            }

            return result;
        }

        private void SetAlignment(TextAlignment? value)
        {
            if (value == null) return;

            foreach (var control in EditableElements)
            {
                
            }
        }

        private Thickness GetPadding()
        {
            
            var th = new Thickness(0);
            return th;
        }

        private void SetPadding(Thickness value)
        {
            foreach (var element in EditableElements)
            {
                
            }
        }

        private double? GetPaddingLeft()
        {
            double? result = null;

            foreach (var control in EditableElements)
            {
               
            }

            return result;
        }

        private void SetPaddingLeft(double? value)
        {
            if (value == null) return;

            foreach (var control in EditableElements)
            {
                
            }
        }

        private double? GetPaddingTop()
        {
            double? result = null;

            foreach (var control in EditableElements)
            {
                
            }

            return result;
        }

        private void SetPaddingTop(double? value)
        {
            if (value == null) return;

            foreach (var control in EditableElements)
            {
                
            }
        }

        private double? GetPaddingRight()
        {
            double? result = null;

            foreach (var control in EditableElements)
            {
                
            }

            return result;
        }

        private void SetPaddingRight(double? value)
        {
            if (value == null) return;

            foreach (var control in EditableElements)
            {
                
            }
        }

        private double? GetPaddingBottom()
        {
            double? result = null;

            foreach (var control in EditableElements)
            {
                
            }

            return result;
        }

        private void SetPaddingBottom(double? value)
        {
            if (value == null) return;

            foreach (var control in EditableElements)
            {
               
            }
        }

        private List<BaseControl> GetEditableElements()
        {
            var list = new List<BaseControl>();

            foreach (var control in this)
            {
                list.Add(control);
            }

            if (list.Count == 0)
            {
                list.Add(this.owner.MapLayoutControl);
            }

            return list;
        }

        private BaseControl GetEditingElement()
        {
            return EditableElements.Count == 0 ? null : EditableElements[EditableElements.Count - 1];
        }

        private void SetLockType(LockTypes value)
        {
            foreach (var element in EditableElements)
            {
                BaseLayoutControl.SetLockType(element, value);
            }
        }

        private LockTypes? GetLockType()
        {
            LockTypes? result = null;
            foreach (var control in this)
            {
                if (result == null)
                {
                    result = BaseLayoutControl.GetLockType(control);
                }

                if (result != BaseLayoutControl.GetLockType(control)) return null;
            }

            return result;
        }


        public double? LeftGis
        {
            get
            {
                return GetLeftGis();
            }

            set
            {
                if (value < 0)
                {
                    return;
                }

                SetLeftGis(value);
                OnPropertyChanged("LeftGis");
            }
        }

        public double? TopGis
        {
            get
            {
                return GetTopGis();
            }

            set
            {
                if (value < 0)
                {
                    return;
                }

                SetTopGis(value);
                OnPropertyChanged("TopGis");
            }
        }

        public double? RightGis
        {
            get
            {
                return GetRightGis();
            }
            set
            {
                if (value < 0)
                {
                    return;
                }

                SetRightGis(value);
                OnPropertyChanged("RightGis");
            }
        }

        public double? BottomGis
        {
            get
            {
                return GetBottomGis();
            }
            set
            {
                if (value < 0)
                {
                    return;
                }

                SetBottomGis(value);
                OnPropertyChanged("BottomGis");
            }
        }

        private void SetLeftGis(double? value)
        {
            if (value == null) return;

            foreach (var element in EditableElements)
            {
                element.LeftGis = (double)value;
            }
        }

        private double? GetLeftGis()
        {
            double? result = null;

            foreach (var control in this)
            {
                if (result == null)
                {
                    result = control.LeftGis;
                    continue;
                }

                if (result != control.LeftGis)
                {
                    return null;
                }
            }
            return result;
        }

        private void SetTopGis(double? value)
        {
            if (value == null) return;

            foreach (var element in EditableElements)
            {
                element.TopGis = (double)value;
            }
        }

        private double? GetTopGis()
        {
            double? result = null;

            foreach (var control in this)
            {
                if (result == null)
                {
                    result = control.TopGis;
                    continue;
                }

                if (result != control.TopGis)
                {
                    return null;
                }
            }
            return result;
        }

        private void SetRightGis(double? value)
        {
            if (value == null) return;

            foreach (var element in EditableElements)
            {
                element.RightGis = (double)value;
            }
        }

        private double? GetRightGis()
        {
            double? result = null;

            foreach (var control in this)
            {
                if (result == null)
                {
                    result = control.RightGis;
                    continue;
                }

                if (result != control.RightGis)
                {
                    return null;
                }
            }
            return result;
        }

        private void SetBottomGis(double? value)
        {
            if (value == null) return;

            foreach (var element in EditableElements)
            {
                element.BottomGis = (double)value;
            }
        }

        private double? GetBottomGis()
        {
            double? result = null;

            foreach (var control in this)
            {
                if (result == null)
                {
                    result = control.BottomGis;
                    continue;
                }

                if (result != control.BottomGis)
                {
                    return null;
                }
            }
            return result;
        }
    }
}