﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TrackerSizeChangedEventArgs.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   트래커의 사이즈 변경시 발생하는 이벤트 아규먼트.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.Console.UIs.LocationSettingPopupWindows.Trackers
{
    using System;

    /// <summary>
    /// A.트래커의 사이즈 변경시 발생하는 이벤트 아규먼트.
    /// </summary>
    public class TrackerSizeChangedEventArgs : EventArgs
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TrackerSizeChangedEventArgs"/> class.
        /// </summary>
        /// <param name="gapX">
        /// The gap x.
        /// </param>
        /// <param name="gapY">
        /// The gap y.
        /// </param>
        /// <param name="gapWidth">
        /// The gap width.
        /// </param>
        /// <param name="gapHeight">
        /// The gap height.
        /// </param>
        public TrackerSizeChangedEventArgs(double gapX, double gapY, double gapWidth, double gapHeight)
        {
            this.GapX = gapX;
            this.GapY = gapY;
            this.GapWidth = gapWidth;
            this.GapHeight = gapHeight;
        }

        /// <summary>
        /// Gets or sets GapX.
        /// 좌단 변화량.
        /// </summary>
        public double GapX { get; set; }

        /// <summary>
        /// Gets or sets GapY.
        /// 상단 변화량.
        /// </summary>
        public double GapY { get; set; }

        /// <summary>
        /// Gets or sets GapWidth.
        /// 넓이 변화량.
        /// </summary>
        public double GapWidth { get; set; }

        /// <summary>
        /// Gets or sets GapHeight.
        /// 높이 변화량.
        /// </summary>
        public double GapHeight { get; set; }
    }
}
