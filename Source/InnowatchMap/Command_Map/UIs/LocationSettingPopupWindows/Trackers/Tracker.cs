﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Tracker.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Defines the ChangedEventHandler type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.Console.UIs.LocationSettingPopupWindows.Trackers
{
    using System;
    using System.Collections.Generic;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Shapes;

    using Innotive.InnoWatch.Commons.BaseControls;
    using Innotive.InnoWatch.Commons.Utils;
    using Innotive.InnoWatch.DLLs.BookmarkControls;
    using Innotive.InnoWatch.DLLs.LayoutControls;
    using Innotive.InnoWatch.DLLs.MapControls;

    /// <summary>
    /// A. 이벤트 델리게이트.
    /// </summary>
    /// <param name="sender">
    /// The sender.
    /// </param>
    /// <param name="e">
    /// The e.
    /// </param>
    public delegate void ChangedEventHandler(object sender, TrackerSizeChangedEventArgs e);

    /// <summary>
    /// 엘리먼트를 조작하는 트래커 클래스.
    /// </summary>
    public class Tracker : FrameworkElement
    {
        private readonly MapControl mapControl;

        private const double StrokeThickness = 1;

        private const double TrackerPointLength = 6;
        
        private readonly Canvas groundCanvas;

        private readonly BaseControl element;

        private readonly Rectangle oldElementRect = new Rectangle();

        private readonly SolidColorBrush rectColor = new SolidColorBrush(Color.FromRgb(80, 128, 255));

        private readonly SolidColorBrush bookmarkRectColor = new SolidColorBrush(Color.FromRgb(255, 0, 0));

        private readonly LocationSettingPopupWindow locationSettingPopupWindow;

        // Main Rectangle
        private readonly Rectangle tracker;

        // 8 spot for resizing
        private readonly Rectangle leftTop;

        private readonly Rectangle top;

        private readonly Rectangle rightTop;

        private readonly Rectangle left;

        private readonly Rectangle right;

        private readonly Rectangle leftBottom;

        private readonly Rectangle bottom;

        private readonly Rectangle rightBottom;

        private readonly Ellipse center;

        // 객체 Resize 관련
        private bool isMouseDown;

        private Point mouseDownPoint = new Point(0, 0);

        private bool editEnable = true;

        private Point startPoint;

        // bookmark용
        private TrackerResizeType mouseDownTrackerResizeType = TrackerResizeType.None;

        private Point bookmarkLockedPoint;

        private ContextMenu contextMenu;

        /// <summary>
        /// Initializes a new instance of the <see cref="Tracker"/> class.
        /// </summary>
        /// <param name="locationSettingPopupWindow">
        /// The location setting popup window.
        /// </param>
        /// <param name="groundCanvas">
        /// The ground canvas.
        /// </param>
        /// <param name="element">
        /// The element.
        /// </param>
        public Tracker(LocationSettingPopupWindow locationSettingPopupWindow, Canvas groundCanvas, BaseControl element)
        {
            this.element = element;

            this.mapControl = groundCanvas.Children[0] as MapControl;

            var list = GetAllVisualChildren(groundCanvas);
            foreach (var uiElement in list)
            {
                if (uiElement is MapControl)
                {
                    this.mapControl = uiElement as MapControl;
                    break;
                }
            }

            // Bookmark Tracker인 경우 색깔을 다르게 설정함 !!
            if (this.element is BookmarkControl)
            {
                this.rectColor = this.bookmarkRectColor;
            }

            this.locationSettingPopupWindow = locationSettingPopupWindow;
            this.groundCanvas = groundCanvas;

            this.TrackerCanvas = new Canvas();
            this.groundCanvas.Children.Add(this.TrackerCanvas);

            this.tracker = new Rectangle
                {
                    SnapsToDevicePixels = true,
                    StrokeThickness = StrokeThickness,
                    Stroke = this.rectColor,
                    Width = 0,
                    Height = 0
                };

            this.TrackerCanvas.Children.Add(this.tracker);

            this.leftTop = this.CreateTrackerPoint(this.TrackerCanvas);
            this.leftTop.SnapsToDevicePixels = true;
            this.top = this.CreateTrackerPoint(this.TrackerCanvas);
            this.top.SnapsToDevicePixels = true;
            this.rightTop = this.CreateTrackerPoint(this.TrackerCanvas);
            this.rightTop.SnapsToDevicePixels = true;
            this.left = this.CreateTrackerPoint(this.TrackerCanvas);
            this.left.SnapsToDevicePixels = true;
            this.right = this.CreateTrackerPoint(this.TrackerCanvas);
            this.right.SnapsToDevicePixels = true;
            this.leftBottom = this.CreateTrackerPoint(this.TrackerCanvas);
            this.leftBottom.SnapsToDevicePixels = true;
            this.bottom = this.CreateTrackerPoint(this.TrackerCanvas);
            this.bottom.SnapsToDevicePixels = true;
            this.rightBottom = this.CreateTrackerPoint(this.TrackerCanvas);
            this.rightBottom.SnapsToDevicePixels = true;

            // Center point is Ellipse
            this.center = new Ellipse { StrokeThickness = StrokeThickness, Stroke = this.rectColor, Width = TrackerPointLength, Height = TrackerPointLength };
            this.TrackerCanvas.Children.Add(this.center);
            this.center.SnapsToDevicePixels = true;

            this.center.MouseLeftButtonDown += this.CenterMouseLeftButtonDown;
            this.center.MouseMove += this.CenterMouseMove;
            this.center.MouseLeftButtonUp += this.Center_MouseLeftButtonUp;

            this.RedrawTracker();

            this.AssignContextMenu(element);
        }

        private enum TrackerResizeType
        {
            None,

            LeftTop,

            Left,

            LeftBottom,

            Top,

            Bottom,

            RightTop,

            Right,

            RightBottom
        }

        /// <summary>
        /// Gets TrackerCanvas.
        /// </summary>
        public Canvas TrackerCanvas { get; private set; }

        /// <summary>
        /// Gets or sets ChangeEvent.
        /// </summary>
        public ChangedEventHandler ChangeEvent { get; set; }

        /// <summary>
        /// 트래커를 제거한다.
        /// </summary>
        public void RemoveTracker()
        {
            this.TrackerCanvas.Children.Remove(this.tracker);
            this.TrackerCanvas.Children.Remove(this.leftTop);
            this.TrackerCanvas.Children.Remove(this.top);
            this.TrackerCanvas.Children.Remove(this.rightTop);
            this.TrackerCanvas.Children.Remove(this.left);
            this.TrackerCanvas.Children.Remove(this.right);
            this.TrackerCanvas.Children.Remove(this.leftBottom);
            this.TrackerCanvas.Children.Remove(this.bottom);
            this.TrackerCanvas.Children.Remove(this.rightBottom);
            this.TrackerCanvas.Children.Remove(this.center);
        }

        /// <summary>
        /// Tracker를 움직이는 경우에 Element를 직접 수정한다.
        /// </summary>
        /// <param name="gapLeft">
        /// The gap left.
        /// </param>
        /// <param name="gapTop">
        /// The gap top.
        /// </param>
        /// <param name="gapRight">
        /// The gap right.
        /// </param>
        /// <param name="gapBottom">
        /// The gap bottom.
        /// </param>
        public void ResizeSelectedElement(double gapLeft, double gapTop, double gapRight, double gapBottom)
        {
            if (!this.editEnable)
            {
                return;
            }

            if (double.IsInfinity(gapLeft) || double.IsInfinity(gapTop) || double.IsInfinity(gapRight)
                || double.IsInfinity(gapBottom))
            {
                return;
            }

            if (double.IsNaN(gapLeft) || double.IsNaN(gapTop) || double.IsNaN(gapRight) || double.IsNaN(gapBottom))
            {
                return;
            }

            var newLeft = Canvas.GetLeft(this.oldElementRect) + gapLeft;
            var newTop = Canvas.GetTop(this.oldElementRect) + gapTop;
            var newRight = Canvas.GetLeft(this.oldElementRect) + this.oldElementRect.Width + gapRight;
            var newBotton = Canvas.GetTop(this.oldElementRect) + this.oldElementRect.Height + gapBottom;

            double resultX1, resultY1, resultX2, resultY2;

            if (newLeft < newRight)
            {
                resultX1 = newLeft;
                resultX2 = newRight;

                
            }
            else
            {
                resultX1 = newRight;
                resultX2 = newLeft;

            }

            if (newTop < newBotton)
            {
                resultY1 = newTop;
                resultY2 = newBotton;

            }
            else
            {
                resultY1 = newBotton;
                resultY2 = newTop;

            }

            var width = Math.Abs(resultX2 - resultX1);
            var height = Math.Abs(resultY2 - resultY1);

            this.element.Left = resultX1;
            this.element.Top = resultY1;
            this.element.Width = width;
            this.element.Height = height;

            if (this.element.ParentBaseLayoutControl is MapControl)
            {
                var control = this.element.ParentBaseLayoutControl as MapControl;
                var elementRight = resultX1 + width;
                var elementBottom = resultY1 + height;

                var gisLeftTop = control.Map.FromLocalToLatLng(Convert.ToInt32(resultX1), Convert.ToInt32(resultY1));
                var gisRightBottom = control.Map.FromLocalToLatLng(Convert.ToInt32(elementRight), Convert.ToInt32(elementBottom));

                this.element.LeftGis = gisLeftTop.Lng;
                this.element.TopGis = gisLeftTop.Lat;
                this.element.RightGis = gisRightBottom.Lng;
                this.element.BottomGis = gisRightBottom.Lat;
            }

            // Bookmark의 Tracker는 비율을 적용해야 하기 때문에 x,y,width,height를 다시 계산해줌 !!
            this.RecalcBookmarkControlRect();

            this.RedrawTracker();
        }

        /// <summary>
        /// 트래커를 다시 그린다.
        /// </summary>
        public void RedrawTracker()
        {
            this.SetEditEnable(!this.element.IsLockEdit);

            this.RedrawTrackerPointColor(this.element);

            var layout = this.element.ParentBaseLayoutControl;

            if (layout == null)
            {
                return;
            }

            var drawRect = InnoConvertUtil.GetBoundingBox(this.element, this.groundCanvas);

            if (drawRect == Rect.Empty)
            {
                return;
            }

            // 0.5의 마법이라고나 할까? 남대리 & 박대리 추천 코드 0.5
            Canvas.SetLeft(this.tracker, drawRect.Left - 0.5);
            Canvas.SetTop(this.tracker, drawRect.Top - 0.5);

            this.tracker.Width = Math.Abs(drawRect.Right - drawRect.Left) + 0.5;
            this.tracker.Height = Math.Abs(drawRect.Bottom - drawRect.Top) + 0.5;

            var newCenterPoint = new Point(drawRect.Left + (this.tracker.Width / 2), drawRect.Top + (this.tracker.Height / 2));

            this.SetTrackerPointPosition(TrackerPointLength / 2, drawRect.TopLeft, drawRect.BottomRight, newCenterPoint);

            if (this.tracker.Width >= TrackerPointLength * 3)
            {
                this.center.Visibility = Visibility.Visible;
                this.top.Visibility = Visibility.Visible;
                this.bottom.Visibility = Visibility.Visible;
            }
            else
            {
                this.center.Visibility = Visibility.Hidden;
                this.top.Visibility = Visibility.Hidden;
                this.bottom.Visibility = Visibility.Hidden;
            }

            if (this.tracker.Height > TrackerPointLength * 3)
            {
                this.center.Visibility = Visibility.Visible;
                this.left.Visibility = Visibility.Visible;
                this.right.Visibility = Visibility.Visible;
            }
            else
            {
                this.center.Visibility = Visibility.Hidden;
                this.left.Visibility = Visibility.Hidden;
                this.right.Visibility = Visibility.Hidden;
            }

            if (this.element is BookmarkControl)
            {
                this.top.Visibility = Visibility.Hidden;
                this.bottom.Visibility = Visibility.Hidden;
                this.left.Visibility = Visibility.Hidden;
                this.right.Visibility = Visibility.Hidden;
            }
        }

        private static IEnumerable<UIElement> GetAllVisualChildren(DependencyObject parentObj)
        {
            var result = new List<UIElement>();

            if (parentObj is UIElement)
            {
                result.Add(parentObj as UIElement);
            }

            var count = VisualTreeHelper.GetChildrenCount(parentObj);
            for (var i = 0; i < count; ++i)
            {
                var childObj = VisualTreeHelper.GetChild(parentObj, i);
                result.AddRange(GetAllVisualChildren(childObj));
            }

            return result;
        }

        /// <summary>
        /// 트래커 포인터들을 재배치한다.
        /// </summary>
        /// <param name="gap">
        /// 트래커 포인터는 자기 크기의 절반만큼 포인트를 트래커에서 이동시킨다.
        /// </param>
        /// <param name="newStartPoint">
        /// 트래커의 좌상단.
        /// </param>
        /// <param name="newEndPoint">
        /// 트래커의 우하단.
        /// </param>
        /// <param name="newCenterPoint">
        /// 트래커의 중점 좌표.
        /// </param>
        private void SetTrackerPointPosition(double gap, Point newStartPoint, Point newEndPoint, Point newCenterPoint)
        {
            Canvas.SetLeft(this.leftTop, newStartPoint.X - gap);
            Canvas.SetTop(this.leftTop, newStartPoint.Y - gap);

            Canvas.SetLeft(this.rightTop, newEndPoint.X - gap);
            Canvas.SetTop(this.rightTop, newStartPoint.Y - gap);

            Canvas.SetLeft(this.leftBottom, newStartPoint.X - gap);
            Canvas.SetTop(this.leftBottom, newEndPoint.Y - gap);

            Canvas.SetLeft(this.rightBottom, newEndPoint.X - gap);
            Canvas.SetTop(this.rightBottom, newEndPoint.Y - gap);

            Canvas.SetLeft(this.top, newCenterPoint.X - gap);
            Canvas.SetTop(this.top, newStartPoint.Y - gap);

            Canvas.SetLeft(this.bottom, newCenterPoint.X - gap);
            Canvas.SetTop(this.bottom, newEndPoint.Y  - gap);

            Canvas.SetLeft(this.left, newStartPoint.X - gap);
            Canvas.SetTop(this.left, newCenterPoint.Y - gap);

            Canvas.SetLeft(this.right, newEndPoint.X - gap);
            Canvas.SetTop(this.right, newCenterPoint.Y - gap);

            Canvas.SetLeft(this.center, newCenterPoint.X - gap);
            Canvas.SetTop(this.center, newCenterPoint.Y - gap);
        }

        private void SetOldElementRect()
        {
            Canvas.SetLeft(this.oldElementRect, Canvas.GetLeft(this.element));
            Canvas.SetTop(this.oldElementRect, Canvas.GetTop(this.element));
            this.oldElementRect.Width = this.element.Width;
            this.oldElementRect.Height = this.element.Height;
        }

        private void AssignContextMenu(BaseControl control)
        {
            this.contextMenu = null;
            this.contextMenu = new ContextMenu();
            var menuItem1 = new MenuItem { Header = "Bring to Front" };
            var menuItem2 = new MenuItem { Header = "Bring Forward" };
            var menuItem3 = new MenuItem { Header = "Send Backward" };
            var menuItem4 = new MenuItem { Header = "Send to Back" };
            menuItem1.Click += this.BringToFrontClick;
            menuItem2.Click += this.BringForwardClick;
            menuItem3.Click += this.SendToBackward;
            menuItem4.Click += this.SendToBackClick;
            this.contextMenu.Items.Add(menuItem1);
            this.contextMenu.Items.Add(menuItem2);
            this.contextMenu.Items.Add(menuItem3);
            this.contextMenu.Items.Add(menuItem4);

            control.ContextMenu = this.contextMenu;
            this.element.PreviewMouseRightButtonDown += this.GroundCanvasPreviewMouseRightButtonDown;
        }

        private void SendToBackward(object sender, RoutedEventArgs e)
        {
            this.mapControl.SendBackward(this.element);
        }

        private void BringForwardClick(object sender, RoutedEventArgs e)
        {
            this.mapControl.BringForward(this.element);
        }

        private void GroundCanvasPreviewMouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            this.contextMenu.IsOpen = true;
        }

        private void SendToBackClick(object sender, RoutedEventArgs e)
        {
            this.mapControl.SendToBack(this.element);
        }

        private void BringToFrontClick(object sender, RoutedEventArgs e)
        {
            this.mapControl.BringToFront(this.element);
        }

        private void CenterMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;

            if (!((IInputElement)sender).IsMouseCaptured)
            {
                ((IInputElement)sender).CaptureMouse();
            }

            this.startPoint = e.GetPosition(this.groundCanvas);
        }

        private void CenterMouseMove(object sender, MouseEventArgs e)
        {
            if (e.MouseDevice.LeftButton != MouseButtonState.Pressed)
            {
                return;
            }

            e.Handled = true;
            this.locationSettingPopupWindow.DirectMove(this.element, this.startPoint, e);
            this.startPoint = e.GetPosition(this.groundCanvas);
        }

        private void Center_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;
            if (((IInputElement)sender).IsMouseCaptured)
            {
                ((IInputElement)sender).ReleaseMouseCapture();
            }
        }

        private void SetEditEnable(bool value)
        {
            this.editEnable = value;

            this.tracker.IsHitTestVisible = value;

            this.leftTop.IsHitTestVisible = value;
            this.top.IsHitTestVisible = value;
            this.rightTop.IsHitTestVisible = value;
            this.left.IsHitTestVisible = value;
            this.right.IsHitTestVisible = value;
            this.leftBottom.IsHitTestVisible = value;
            this.bottom.IsHitTestVisible = value;
            this.rightBottom.IsHitTestVisible = value;
        }

        private Rectangle CreateTrackerPoint(Canvas canvas)
        {
            var rectangle = new Rectangle
                {
                    StrokeThickness = StrokeThickness,
                    Stroke = this.rectColor,
                    Width = TrackerPointLength,
                    Height = TrackerPointLength,
                    Fill = Brushes.White
                };

            canvas.Children.Add(rectangle);

            rectangle.MouseLeftButtonDown += this.TrackerPointMouseLeftButtonDown;
            rectangle.MouseMove += this.TrackerPointMouseMove;
            rectangle.MouseLeftButtonUp += this.TrackerPointMouseLeftButtonUp;
            rectangle.MouseEnter += this.TrackerPointMouseEnter;
            rectangle.MouseLeave += this.TrackerPointMouseLeave;

            return rectangle;
        }

        private LockTypes GetLockType(FrameworkElement control)
        {
            // by jhlee (BaseLayoutControl.LockType)
            if (control is BaseControl)
            {
                return BaseLayoutControl.GetLockType(control as BaseControl);
            }

            throw new Exception(
                "Class Name: " + this.GetType().FullName + " & Function Name: "
                + System.Reflection.MethodBase.GetCurrentMethod().Name);
        }

        private void RedrawTrackerPointColor(FrameworkElement control)
        {
            // by jhlee (BaseLayoutControl.LockType)
            Brush drawBrush = this.GetLockType(control) != LockTypes.None ? Brushes.Black : Brushes.White;

            this.leftTop.Fill = drawBrush;
            this.top.Fill = drawBrush;
            this.rightTop.Fill = drawBrush;
            this.left.Fill = drawBrush;
            this.right.Fill = drawBrush;
            this.leftBottom.Fill = drawBrush;
            this.bottom.Fill = drawBrush;
            this.rightBottom.Fill = drawBrush;
            this.center.Fill = Brushes.Olive;
        }

        private Point GetMovedGapPoint(Point point)
        {
            var gapX = point.X - this.mouseDownPoint.X;
            var gapY = point.Y - this.mouseDownPoint.Y;

            // mouse이동량을 overlay canvas의 좌표계에서 viewbox좌표계로 변경함 !!
            double ratioX = InnoConvertUtil.GetRatioElementX(this.element, this.groundCanvas);
            double ratioY = InnoConvertUtil.GetRatioElementY(this.element, this.groundCanvas);

            return new Point(gapX * ratioX, gapY * ratioY);
        }

        private void ResizeSelectedElement(object sender, double trackerPointGapX, double trackerPointGapY)
        {
            if (!this.editEnable)
            {
                return;
            }

            var curRect = sender as Rectangle;
            if (curRect == null)
            {
                return;
            }

            double gapLeft = 0;
            double gapTop = 0;
            double gapRight = 0;
            double gapBottom = 0;

            if (curRect == this.leftTop)
            {
                gapLeft = trackerPointGapX;
                gapTop = trackerPointGapY;
            }
            else if (curRect == this.top)
            {
                gapTop = trackerPointGapY;
            }
            else if (curRect == this.rightTop)
            {
                gapTop = trackerPointGapY;
                gapRight = trackerPointGapX;
            }
            else if (curRect == this.right)
            {
                gapRight = trackerPointGapX;
            }
            else if (curRect == this.rightBottom)
            {
                gapRight = trackerPointGapX;
                gapBottom = trackerPointGapY;
            }
            else if (curRect == this.bottom)
            {
                gapBottom = trackerPointGapY;
            }
            else if (curRect == this.leftBottom)
            {
                gapLeft = trackerPointGapX;
                gapBottom = trackerPointGapY;
            }
            else if (curRect == this.left)
            {
                gapLeft = trackerPointGapX;
            }
            else
            {
                return;
            }

            this.RaiseChangedEvent(gapLeft, gapTop, gapRight, gapBottom);
        }

        private void RaiseChangedEvent(double gapX, double gapY, double gapWidth, double gapHeight)
        {
            if (this.ChangeEvent != null)
            {
                this.ChangeEvent(this, new TrackerSizeChangedEventArgs(gapX, gapY, gapWidth, gapHeight));
            }
        }

        private void TrackerPointMouseEnter(object sender, MouseEventArgs e)
        {
            if ((sender as Rectangle) == this.leftTop)
            {
                this.groundCanvas.Cursor = Cursors.SizeNWSE;
            }
            else if ((sender as Rectangle) == this.top)
            {
                this.groundCanvas.Cursor = Cursors.SizeNS;
            }
            else if ((sender as Rectangle) == this.rightTop)
            {
                this.groundCanvas.Cursor = Cursors.SizeNESW;
            }
            else if ((sender as Rectangle) == this.right)
            {
                this.groundCanvas.Cursor = Cursors.SizeWE;
            }
            else if ((sender as Rectangle) == this.rightBottom)
            {
                this.groundCanvas.Cursor = Cursors.SizeNWSE;
            }
            else if ((sender as Rectangle) == this.bottom)
            {
                this.groundCanvas.Cursor = Cursors.SizeNS;
            }
            else if ((sender as Rectangle) == this.leftBottom)
            {
                this.groundCanvas.Cursor = Cursors.SizeNESW;
            }
            else if ((sender as Rectangle) == this.left)
            {
                this.groundCanvas.Cursor = Cursors.SizeWE;
            }
        }

        // Common MouseEvent for MouseLeftButtondown, MouseLeftButtonup, MouseLeave
        private void TrackerPointMouseLeave(object sender, MouseEventArgs e)
        {
            this.groundCanvas.Cursor = Cursors.Arrow;
        }

        private void TrackerPointMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            // Space가 눌린경우면, Zoom/Pan을 처리해야 함 !!
            if (Keyboard.IsKeyDown(Key.Space))
            {
                return;
            }

            var rect = sender as Rectangle;

            if (rect == null)
            {
                return;
            }

            if (this.element is BookmarkControl)
            {
                this.SetBookmarkLockedPoint(rect);
            }

            if (rect.IsMouseCaptured)
            {
                rect.ReleaseMouseCapture();
            }

            rect.CaptureMouse();

            this.SetOldElementRect();

            foreach (var item in this.locationSettingPopupWindow.TrackerManager)
            {
                if (item == this)
                {
                    continue;
                }

                item.SetOldElementRect();
            }

            // 상태 저장 !!
            this.isMouseDown = true;
            this.mouseDownPoint = e.GetPosition(this.TrackerCanvas);

            if (this.element.ParentBaseLayoutControl is MapControl)
            {
                this.locationSettingPopupWindow.SelectedElements.SetOpacity(0.3);
            }

            // Zoom/Pan 처리하지 않음 !!
            e.Handled = true;
        }

        private void TrackerPointMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            this.isMouseDown = false;
            this.mouseDownPoint.X = 0;
            this.mouseDownPoint.Y = 0;

            // bookmark 관련 정보 초기화
            this.bookmarkLockedPoint = new Point(0, 0);
            this.mouseDownTrackerResizeType = TrackerResizeType.None;

            var senderElement = sender as FrameworkElement;
            if (senderElement != null && senderElement.IsMouseCaptured)
            {
                senderElement.ReleaseMouseCapture();
            }

            // Space가 눌린경우면, Zoom/Pan을 처리해야 함 !!
            if (Keyboard.IsKeyDown(Key.Space))
            {
                return;
            }

            // Zoom/Pan 처리하지 않음 !!
            e.Handled = true;

            if (this.element.ParentBaseLayoutControl is MapControl)
            {
                this.locationSettingPopupWindow.SelectedElements.SetOpacity(1.0);
            }
        }

        private void TrackerPointMouseMove(object sender, MouseEventArgs e)
        {
            // Space가 눌린경우면, Zoom/Pan을 처리해야 함 !!
            if (Keyboard.IsKeyDown(Key.Space))
            {
                return;
            }

            if (!this.isMouseDown)
            {
                return;
            }

            var layout = this.element.ParentBaseLayoutControl;
            if (layout == null)
            {
                return;
            }

            var point = e.GetPosition(this.TrackerCanvas);
            var gapPoint = this.GetMovedGapPoint(point);

            ResizeSelectedElement(sender, gapPoint.X, gapPoint.Y);

            // Zoom/Pan 처리하지 않음 !!
            e.Handled = true;
        }

        // BookmarkControl은 Layout의 비율을 따라가야 하기 때문에 Tracker가 눌린 위치에 따라서 변하지 않는 기준점을 하나 설정함 !!
        // 나중에 Element의 Rect값을 결정할때 여기서 설정한 기준점을 기준으로 Rect값을 다시 계산함 !!
        private void SetBookmarkLockedPoint(Rectangle selectedTrackerRectangle)
        {
            if (!(this.element is BookmarkControl))
            {
                return;
            }

            var elementLeft = Canvas.GetLeft(this.element);
            var elementTop = Canvas.GetTop(this.element);
            var elementRight = elementLeft + this.element.Width;
            var elementBottom = elementTop + this.element.Height;

            if (selectedTrackerRectangle == this.leftTop)
            {
                this.mouseDownTrackerResizeType = TrackerResizeType.LeftTop;
                this.bookmarkLockedPoint = new Point(elementRight, elementBottom);
            }
            else if (selectedTrackerRectangle == this.left)
            {
                this.mouseDownTrackerResizeType = TrackerResizeType.Left;
                this.bookmarkLockedPoint = new Point(elementRight, elementTop);
            }
            else if (selectedTrackerRectangle == this.leftBottom)
            {
                this.mouseDownTrackerResizeType = TrackerResizeType.LeftBottom;
                this.bookmarkLockedPoint = new Point(elementRight, elementTop);
            }
            else if (selectedTrackerRectangle == this.top)
            {
                this.mouseDownTrackerResizeType = TrackerResizeType.Top;
                this.bookmarkLockedPoint = new Point(elementLeft, elementBottom);
            }
            else if (selectedTrackerRectangle == this.bottom)
            {
                this.mouseDownTrackerResizeType = TrackerResizeType.Bottom;
                this.bookmarkLockedPoint = new Point(elementLeft, elementTop);
            }
            else if (selectedTrackerRectangle == this.rightTop)
            {
                this.mouseDownTrackerResizeType = TrackerResizeType.RightTop;
                this.bookmarkLockedPoint = new Point(elementLeft, elementBottom);
            }
            else if (selectedTrackerRectangle == this.right)
            {
                this.mouseDownTrackerResizeType = TrackerResizeType.Right;
                this.bookmarkLockedPoint = new Point(elementLeft, elementTop);
            }
            else if (selectedTrackerRectangle == this.rightBottom)
            {
                this.mouseDownTrackerResizeType = TrackerResizeType.RightBottom;
                this.bookmarkLockedPoint = new Point(elementLeft, elementTop);
            }
            else
            {
                this.mouseDownTrackerResizeType = TrackerResizeType.None;
                this.bookmarkLockedPoint = new Point(0, 0);
            }
        }

        // BookmarkControl은 Layout의 비율을 따라가기 때문에 Rect값을 다시 계산해줌 !!
        private void RecalcBookmarkControlRect()
        {
            if (!(this.element is BookmarkControl))
            {
                return;
            }

            // Tracker를 선택해서 Resize중이 아니면 그냥 return
            if (this.mouseDownTrackerResizeType == TrackerResizeType.None)
            {
                return;
            }

            var layoutControl = this.element.ParentBaseLayoutControl as LayoutControl;
            if (layoutControl == null)
            {
                return;
            }

            // Object가 화면을 벗어나지 않는 기준을 정함 (넓이를 기준으로 할지, 높이를 기준으로 할지 결정)
            var heightZoomRatio = this.element.Height / layoutControl.OriginHeight;
            var heightRatioWidth = layoutControl.OriginWidth * heightZoomRatio;

            var isApplyWidth = heightRatioWidth > this.element.Width;

            // 넓이를 기준으로 bookmark의 높이를 다시 계산함 !!
            if (isApplyWidth)
            {
                var newHeight = layoutControl.OriginHeight * this.element.Width / layoutControl.OriginWidth;
                this.element.Height = newHeight;
            }
            else 
            {
                // 높이를 기준으로 bookmark의 넓이를 다시 계산함 !!
                double newWidth = layoutControl.OriginWidth * this.element.Height / layoutControl.OriginHeight;
                this.element.Width = newWidth;
            }

            // BookmarkControl의 x,y 좌표를 기준점을 중심으로 다시 설정함 !!
            var elementLeft = Canvas.GetLeft(this.element);
            var elementTop = Canvas.GetTop(this.element);

            if (elementLeft < this.bookmarkLockedPoint.X)
            {
                Canvas.SetLeft(this.element, this.bookmarkLockedPoint.X - this.element.Width);
            }
            else
            {
                Canvas.SetLeft(this.element, this.bookmarkLockedPoint.X);
            }

            if (elementTop < this.bookmarkLockedPoint.Y)
            {
                Canvas.SetTop(this.element, this.bookmarkLockedPoint.Y - this.element.Height);
            }
            else
            {
                Canvas.SetTop(this.element, this.bookmarkLockedPoint.Y);
            }
        }
    }
}