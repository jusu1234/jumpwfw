﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TrackerManager.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Defines the TrackerManager type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.Console.UIs.LocationSettingPopupWindows.Trackers
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Windows;
    using System.Windows.Controls;

    using Innotive.InnoWatch.Commons.BaseControls;
    using Innotive.InnoWatch.Commons.Utils;
    using Innotive.InnoWatch.DLLs.BookmarkControls;

    /// <summary>
    /// 트래커 목록을 관리한다.
    /// </summary>
    public class TrackerManager : List<Tracker>
    {
        private readonly Canvas ownerCanvas;

        /// <summary>
        /// 현재 트래커의 상태를 보여주는 플레그.
        /// </summary>
        private bool isHiding;

        /// <summary>
        /// Initializes a new instance of the <see cref="TrackerManager"/> class.
        /// </summary>
        /// <param name="win">
        /// The win.
        /// </param>
        /// <param name="container">
        /// The container.
        /// </param>
        public TrackerManager(LocationSettingPopupWindow win, Canvas container)
        {
            this.Win = win;
            this.ownerCanvas = container;
        }

        private LocationSettingPopupWindow Win { get; set; }

        /// <summary>
        /// 트래크를 숨긴다.
        /// </summary>
        public void HideTracker()
        {
            if (this.isHiding)
            {
                return;
            }

            foreach (var tracker in this)
            {
                if (tracker.TrackerCanvas.Visibility == Visibility.Visible)
                {
                    tracker.TrackerCanvas.Visibility = Visibility.Hidden;
                }
            }

            this.isHiding = true;
        }

        /// <summary>
        /// 트래커를 나타나게 한다.
        /// </summary>
        public void ShowTracker()
        {
            foreach (Tracker tracker in this)
            {
                if (tracker.TrackerCanvas.Visibility == Visibility.Visible)
                {
                    continue;
                }

                tracker.TrackerCanvas.Visibility = Visibility.Visible;
            }

            this.isHiding = false;
        }

        /// <summary>
        /// 트래커를 모두 삭제한다.
        /// </summary>
        public new void Clear()
        {
            foreach (var tracker in this.Where(tracker => tracker != null))
            {
                tracker.RemoveTracker();
            }

            base.Clear();
        }

        /// <summary>
        /// 바쁘지 않을때 트랙커를 다시 그린다.
        /// </summary>
        public void RedrawAsync()
        {
            AsyncWorker.Instance.UpdateNotify();
            this.Redraw();
        }

        /// <summary>
        /// 트래커를 추가한다.
        /// </summary>
        /// <param name="element">
        /// The element.
        /// </param>
        public void AddFromElement(BaseControl element)
        {
            if (element == null)
            {
                return;
            }

            if (this.Win.MapLayoutControl == null)
            {
                return;
            }
            
            var tracker = new Tracker(this.Win, this.ownerCanvas, element);
            tracker.ChangeEvent += this.OnChangedEvent;
            Add(tracker);
        }

        /// <summary>
        /// 엘리먼트 목록 만큼 Tracker를 추가한다.
        /// </summary>
        /// <param name="elementList">
        /// The element list.
        /// </param>
        public void AddFromElements(List<BaseControl> elementList)
        {
            this.Clear();
            if (elementList == null || elementList.Count < 1)
            {
                return;
            }

            if (this.Win.MapLayoutControl == null)
            {
                return;
            }

            foreach (var element in elementList)
            {
                this.AddFromElement(element);
            }
        }

        /// <summary>
        /// 엘리멑느를 추가한다.
        /// </summary>
        /// <param name="selectecElements">
        /// The selectec elements.
        /// </param>
        public void AddElements(SelectedElements selectecElements)
        {
            this.Clear();
            foreach (var element in selectecElements)
            {
                if (element as BookmarkControl != null)
                {
                    if (element.Visibility != Visibility.Visible)
                    {
                        continue;
                    }
                }
                else if (element.IsLockEdit || !element.EditVisible)
                {
                    continue;
                }

                this.AddFromElement(element);
            }
        }

        private void Redraw()
        {
            foreach (Tracker tracker in this)
            {
                if (tracker == null)
                {
                    continue;
                }

                tracker.RedrawTracker();
            }
        }

        private void OnChangedEvent(object sender, TrackerSizeChangedEventArgs e)
        {
            foreach (Tracker tracker in this)
            {
                tracker.ResizeSelectedElement(e.GapX, e.GapY, e.GapWidth, e.GapHeight);
            }

            this.Win.SelectedElements.SetLTWH();
            this.Win.SelectedElements.SetGisPosition();
        }
    }
}
