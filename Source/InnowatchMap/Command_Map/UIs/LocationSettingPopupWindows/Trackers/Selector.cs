﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Selector.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   검은색과 하얀색이 교차하는 사각형을 그린다.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.Console.UIs.LocationSettingPopupWindows.Trackers
{
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Media;
    using System.Windows.Shapes;

    /// <summary>
    /// 검은색과 하얀색이 교차하는 사각형을 그린다.
    /// </summary>
    class DashRect
    {
        private readonly Rectangle black = new Rectangle();
        private readonly Rectangle white = new Rectangle();

        public DashRect(Canvas ownerCanvas)
        {
            ownerCanvas.Children.Add(white);
            ownerCanvas.Children.Add(black);

            //흰색 선위에 검은색 Dash선을 그려줌 !!
            white.SnapsToDevicePixels = true;
            white.Width = 0;
            white.Height = 0;
            white.Stroke = Brushes.White;
            white.StrokeThickness = 1;

            black.SnapsToDevicePixels = true;
            black.Width = 0;
            black.Height = 0;
            black.Stroke = Brushes.Black;
            black.StrokeThickness = 1;

            black.StrokeDashArray.Add(1);
            black.StrokeDashArray.Add(2);
            black.StrokeDashArray.Add(2);
            black.StrokeDashArray.Add(1);

            Panel.SetZIndex(white, 0);
            Panel.SetZIndex(black, 1);
        }

        public void SetVisiblity(Visibility visibility)
        {
            this.black.Visibility = visibility;
            this.white.Visibility = visibility;
        }

        public void SetArea(Rect rect)
        {
            Canvas.SetLeft(this.white, rect.X);
            Canvas.SetTop(this.white, rect.Y);
            this.white.Width = rect.Width;
            this.white.Height = rect.Height;

            Canvas.SetLeft(this.black, rect.X);
            Canvas.SetTop(this.black, rect.Y);
            this.black.Width = rect.Width;
            this.black.Height = rect.Height;
        }

    }


    public class Selector : FrameworkElement
    {
        private readonly Canvas ownerCanvas;
        private readonly DashRect dashRect;

        public Selector(Canvas canvas)
        {
            ownerCanvas = canvas;
            dashRect = new DashRect(ownerCanvas);
        }

        public void ShowTracker(bool isShow)
        {
            this.dashRect.SetVisiblity(isShow ? Visibility.Visible : Visibility.Hidden);
        }

        public void DrawTracker(Rect rect)
        {
            ShowTracker(true);

            this.dashRect.SetArea(rect);
        }
    }
}
