﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CameraGroupList.xaml.cs" company="Innotive Inc. Korea">
//   Copyright ⓒ Innotive inc. Korea 2011
// </copyright>
// <summary>
//   Interaction logic for CameraGroupListUserControl.xaml.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.Console.UIs.LocationSettingPopupWindows
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Collections.Specialized;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;

    using Innotive.InnoWatch.Console.Configs;
    using Innotive.InnoWatch.Console.Controls;
    using Innotive.InnoWatch.Console.Controls.DragDrop;
    using Innotive.InnoWatch.Console.Models;
    using Innotive.InnoWatch.DLLs.MapControls;
    using Innotive.InnoWatch.DLLs.TreeListBoxControls;

    using TreeListBoxControls;

    /// <summary>
    /// Interaction logic for CameraGroupListUserControl.xaml.
    /// </summary>
    public partial class CameraGroupList
    {
        /// <summary>
        ///   Selection Mode 를 지정한다. ( 개별 / 다중 선택 기능 )
        /// </summary>
        public static readonly DependencyProperty SelectionModeProperty = DependencyProperty.Register(
            "SelectionMode", 
            typeof(SelectionMode), 
            typeof(CameraGroupList), 
            new PropertyMetadata(OnSelectionModePropertyChanged));

        /// <summary>
        ///   Initializes a new instance of the <see cref = "CameraGroupList" /> class.
        /// </summary>
        public CameraGroupList()
        {
            this.InitializeComponent();
            
            // SelectionMode 는 변경이 일어날 때만 SET 되므로 초기값 필요함. 
            // xTreeView_CameraList.SelectionMode = SelectionMode.Single;

            // MultiGrid 용 Drag
            var sourceCameraDragAdvirsor = new CameraListDragSourceAdvisor(CameraListDragSourceAdvisor.CameraListDragSourceAdvisorSourceType.Camera);
            Commons.Utils.DragDropManager.SetDragSourceAdvisor(this.xTreeView_CameraList, sourceCameraDragAdvirsor);

            this.HighlightItemIdList = new ObservableCollection<string>();
            this.HiddenItemIdList = new ObservableCollection<string>();

            this.xTreeView_CameraList.NewItemCreated += this.xTreeView_CameraList_NewItemCreated;
            this.HighlightItemIdList.CollectionChanged += this.HighlightItemIdList_CollectionChanged;
            this.HiddenItemIdList.CollectionChanged += this.HiddenItemIdList_CollectionChanged;

            this.xTreeView_CameraList.SelectionChanged += this.TreeView_CameraList_SelectionChanged;
            this.xTreeView_CameraList.MouseDoubleClick += this.xTreeView_CameraList_MouseDoubleClick;
        }

        /// <summary>
        ///   카메라 클릭 이벤트.
        /// </summary>
        public event EventHandler<MouseButtonEventArgs> MouseDoubleClicked;

        /// <summary>
        ///   카메라 선택 변경 이벤트
        /// </summary>
        public event EventHandler<SelectionChangedEventArgs> SelectionChanged;

        /// <summary>
        ///   Gets HighlightItemIdList.
        /// </summary>
        public ObservableCollection<string> HighlightItemIdList { get; private set; }

        /// <summary>
        ///   Gets or sets SelectionMode.
        /// </summary>
        public SelectionMode SelectionMode
        {
            get
            {
                return (SelectionMode)this.GetValue(SelectionModeProperty);
            }

            set
            {
                this.SetValue(SelectionModeProperty, value);
            }
        }

        /// <summary>
        ///   Gets or sets HiddenItemIdList.
        /// </summary>
        private ObservableCollection<string> HiddenItemIdList { get; set; }

        /// <summary>
        /// 카메라 리스트 선택 초기화.
        /// </summary>
        public void ClearCameraSelection()
        {
            this.xTreeView_CameraList.UnselectAll();
        }

        /// <summary>
        /// 폴더를 제외한 모든 Camera의 List를 구함.
        /// </summary>
        /// <returns>
        /// Camera List.
        /// </returns>
        public List<Camera> GetAllCameraListExceptFolder()
        {
            var cameras = this.xTreeView_CameraList.HierarchalItemsSource as Cameras;

            return cameras == null ? null : this.GetAllCameraList(cameras);
        }

        /// <summary>
        /// 폴더 제거된 선택된 카메라 가져오기.
        /// </summary>
        /// <returns>
        /// 폴더 제거된 순수 카메라 목록.
        /// </returns>
        public Cameras GetSelectedCameras()
        {
            var selectedItems = this.xTreeView_CameraList.SelectedItems;

            var cameras = new Cameras();
            foreach (var t in selectedItems)
            {
                var info = t as TreeListBoxInfo;
                if (info != null)
                {
                    var camera = info.DataItem as Camera;
                    if (camera != null)
                    {
                        if (!camera.IsFolder)
                        {
                            cameras.Add(camera);
                        }
                    }
                }
            }

            return cameras;
        }

        /// <summary>
        /// 카메라 데이터 열기.
        /// </summary>
        /// <param name="cameras">
        /// The cameras.
        /// </param>
        public void OpenCameras(Cameras cameras)
        {
            // 트리뷰 아이템 소스 초기화
            this.xTreeView_CameraList.ChildItemSourcePath = "Items";
            this.xTreeView_CameraList.UnselectAll();

            this.xTreeView_CameraList.HierarchalItemsSource = cameras;

            this.HighlightItemIdList.Clear();
            this.HiddenItemIdList.Clear();
        }

        /// <summary>
        /// 맵 카메라만 표시함.
        /// </summary>
        /// <param name="cameras">
        /// The cameras.
        /// </param>
        public void OpenCamerasForLocation(Cameras cameras)
        {
            // 트리뷰 아이템 소스 초기화
            this.xTreeView_CameraList.ChildItemSourcePath = "Items";
            this.xTreeView_CameraList.UnselectAll();

            this.xTreeView_CameraList.HierarchalItemsSource = cameras;

            this.HighlightItemIdList.Clear();
            this.HiddenItemIdList.Clear();

            var locationCameraIdList = MapControlRepository.GetCameraElementIdList();
            var currentCameraList = this.GetAllCameraListExceptFolder();
            foreach (var item in currentCameraList)
            {
                if (item.IsFolder)
                {
                    continue;
                }

                if (locationCameraIdList.Contains(item.Id))
                {
                    continue;
                }

                this.HiddenItemIdList.Add(item.Id);
            }
        }

        /// <summary>
        /// Mouse Enter
        /// </summary>
        /// <param name="e">
        /// The e.
        /// </param>
        protected override void OnMouseEnter(MouseEventArgs e)
        {
            base.OnMouseEnter(e);
            this.xTreeView_CameraList.IsEnabled = true;
        }

        /// <summary>
        /// Mouse Leave
        /// </summary>
        /// <param name="e">
        /// The e.
        /// </param>
        protected override void OnMouseLeave(MouseEventArgs e)
        {
            base.OnMouseLeave(e);
            this.xTreeView_CameraList.IsEnabled = false;
        }

        /// <summary>
        /// Selection Mode 값이 변경되었을때
        /// </summary>
        /// <param name="d">
        /// The d.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private static void OnSelectionModePropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var cameraGroupListUserControl = d as CameraGroupListUserControl;
            if (cameraGroupListUserControl != null)
            {
                cameraGroupListUserControl.xTreeView_CameraList.SelectionMode = (SelectionMode)e.NewValue;
            }
        }

        /// <summary>
        /// HiddenStyle 적용
        /// </summary>
        /// <param name="item">
        /// The item.
        /// </param>
        private void ApplyHiddenStyle(object item)
        {
            var control = item as TreeListBoxItem;
            if (control == null)
            {
                return;
            }

            control.Visibility = Visibility.Collapsed;
        }

        /// <summary>
        /// NormalStyle 적용
        /// </summary>
        /// <param name="item">
        /// The item.
        /// </param>
        private void ApplyNormalStyle(object item)
        {
            var control = item as ItemsControl;
            if (control == null)
            {
                return;
            }

            control.Opacity = 1;
            control.Visibility = Visibility.Visible;
        }

        /// <summary>
        /// Camera List 구함
        /// </summary>
        /// <param name="cameras">
        /// The cameras.
        /// </param>
        /// <returns>
        /// 카메라 LIST
        /// </returns>
        private List<Camera> GetAllCameraList(IEnumerable<Camera> cameras)
        {
            var cameraList = new List<Camera>();
            foreach (var camera in cameras)
            {
                if (camera == null)
                {
                    continue;
                }

                this.GetAllCameraList_Internal(camera, cameraList);
            }

            return cameraList;
        }

        /// <summary>
        /// Camera List 구함 ( 재귀용 )
        /// </summary>
        /// <param name="camera">
        /// The camera.
        /// </param>
        /// <param name="cameraList">
        /// The camera list.
        /// </param>
        private void GetAllCameraList_Internal(Camera camera, List<Camera> cameraList)
        {
            if (!camera.IsFolder)
            {
                cameraList.Add(camera);
            }
            else
            {
                // InnoTrace.Trace(3, "[blackRoot02] Group id = {0}, no = {1}, name = {2}", camera.Id, camera.No, camera.Name);
                foreach (Camera childCamera in camera.Items)
                {
                    if (childCamera == null)
                    {
                        continue;
                    }

                    this.GetAllCameraList_Internal(childCamera, cameraList);
                }
            }
        }

        /// <summary>
        /// HiddenItemIdList 아이템 컬렉션에 변경이 발생시
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void HiddenItemIdList_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems != null)
            {
                foreach (var item in e.NewItems)
                {
                    foreach (TreeListBoxInfo listItem in this.xTreeView_CameraList.Items)
                    {
                        var camera = listItem.DataItem as Camera;
                        if (camera == null)
                        {
                            continue;
                        }

                        if (string.Compare(camera.Id, item.ToString()) == 0)
                        {
                            var treeListBoxItem =
                                this.xTreeView_CameraList.ItemContainerGenerator.ContainerFromItem(listItem) as
                                TreeListBoxItem;
                            this.ApplyHiddenStyle(treeListBoxItem);
                        }
                    }
                }
            }

            if (e.OldItems != null)
            {
                foreach (var item in e.OldItems)
                {
                    foreach (TreeListBoxInfo listItem in this.xTreeView_CameraList.Items)
                    {
                        var camera = listItem.DataItem as Camera;
                        if (camera == null)
                        {
                            continue;
                        }

                        if (string.Compare(camera.Id, item.ToString()) == 0)
                        {
                            var treeListBoxItem =
                                this.xTreeView_CameraList.ItemContainerGenerator.ContainerFromItem(listItem) as
                                TreeListBoxItem;
                            this.ApplyNormalStyle(treeListBoxItem);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// HighlightItemIdList 아이템 컬렉션에 변경이 발생시
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void HighlightItemIdList_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems != null)
            {
                foreach (var item in e.NewItems)
                {
                    foreach (TreeListBoxInfo listItem in this.xTreeView_CameraList.Items)
                    {
                        var camera = listItem.DataItem as Camera;
                        if (camera == null)
                        {
                            continue;
                        }

                        if (string.Compare(camera.Id, item.ToString()) == 0)
                        {
                            camera.IsLoadedMap = true;
                        }
                    }
                }
            }

            if (e.OldItems != null)
            {
                foreach (var item in e.OldItems)
                {
                    foreach (TreeListBoxInfo listItem in this.xTreeView_CameraList.Items)
                    {
                        var camera = listItem.DataItem as Camera;
                        if (camera == null)
                        {
                            continue;
                        }

                        if (string.Compare(camera.Id, item.ToString()) == 0)
                        {
                            camera.IsLoadedMap = false;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// 선택 목록 항목 중에 폴더 제거.
        /// </summary>
        private void RemoveFolderSelection_CameraList()
        {
            var selectedItems = this.xTreeView_CameraList.SelectedItems;

            var infos = new List<TreeListBoxInfo>();
            for (int i = 0; i < selectedItems.Count; i++)
            {
                var info = selectedItems[i] as TreeListBoxInfo;
                if (info != null)
                {
                    var camera = info.DataItem as Camera;
                    if (camera != null)
                    {
                        if (camera.IsFolder)
                        {
                            infos.Add(info);
                        }
                    }
                }
            }

            for (int i = 0; i < infos.Count; i++)
            {
                selectedItems.Remove(infos[i]);
            }

            infos.Clear();
        }

        /// <summary>
        /// 카메라 리스트 선택 변경.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void TreeView_CameraList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            // by jhlee (2013.05.15 Map 외주)
            //var consoleWindowViewModel = App.ConsoleWindow.DataContext as ConsoleWindowViewModel;
            //if (consoleWindowViewModel != null)
            //{
            //    var gridCellSelected =
            //        consoleWindowViewModel.StageControlManager.GridControlAtSelectedStage.IsCellSelected;

            //    if (gridCellSelected)
            //    {
            //        // Reload
            //        if (this.xTreeView_CameraList.SelectionMode == SelectionMode.Multiple)
            //        {
            //            this.RemoveFolderSelection_CameraList();
            //        }

            //        var cameras = this.GetSelectedCameras();

            //        if (cameras.Count > 0)
            //        {
            //            while (this.xTreeView_CameraList.SelectedItems.Count >
            //                   CommandConfigExecution.GetInstance().Config.Command.StageMode.CameraList.SelectingLimit.Value)
            //            {
            //                this.xTreeView_CameraList.SelectedItems.RemoveAt(0);
            //            }
            //        }
            //    }
            //}

            if (this.SelectionChanged != null)
            {
                this.SelectionChanged(sender, e);
            }
        }

        /// <summary>
        /// 카메라 리스트에서 더블클릭 시
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void xTreeView_CameraList_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (this.MouseDoubleClicked != null)
            {
                this.MouseDoubleClicked(sender, e);
            }
        }

        /// <summary>
        /// 새로운 아이템 생성시
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void xTreeView_CameraList_NewItemCreated(object sender, TreeListBoxItemCreatedEventArgs e)
        {
            var camera = e.NewItemInfo.DataItem as Camera;
            if (camera == null)
            {
                return;
            }

            if (this.HighlightItemIdList.Contains(camera.Id))
            {
                camera.IsLoadedMap = true;
            }

            if (this.HiddenItemIdList.Contains(camera.Id))
            {
                this.ApplyHiddenStyle(e.NewItem);
            }
        }
    }
}