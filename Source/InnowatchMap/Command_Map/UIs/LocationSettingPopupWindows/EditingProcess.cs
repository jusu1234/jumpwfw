﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="EditingProcess.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Defines the EditingProcess type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.Console.UIs.LocationSettingPopupWindows
{
    using System;
    using System.Diagnostics;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;

    using Innotive.InnoWatch.Commons.BaseControls;
    using Innotive.InnoWatch.Commons.EventArguments;
    using Innotive.InnoWatch.Commons.GuidManagers;
    using Innotive.InnoWatch.Commons.Publics;
    using Innotive.InnoWatch.DLLs.LayoutControls;
    using Innotive.InnoWatch.DLLs.MapControls;

    /// <summary>
    /// 에디팅 프로세스.
    /// </summary>
    public class EditingProcess : BaseProcess
    {
        /// <summary>
        /// 마우스다운 위치.
        /// </summary>
        private Point startPoint;

        /// <summary>
        /// 지금 마우스가 눌린 상태인지 체크.
        /// </summary>
        private bool isMouseDown;

        private LocationSettingPopupWindow owner;

        /// <summary>
        /// Initializes a new instance of the <see cref="EditingProcess"/> class.
        /// </summary>
        /// <param name="owner">
        /// The owner.
        /// </param>
        public EditingProcess(LocationSettingPopupWindow locationSettingPopupWindow)
            : base()
        {
            Debug.Assert(locationSettingPopupWindow != null, "LocationSettingPopupWindow 가 존재하지 않습니다.");

            this.owner = locationSettingPopupWindow;
        }

        /// <summary>
        /// 오브젝트리스트를 갱신합니다.
        /// </summary>
        /// <param name="myObject">
        /// The my object.
        /// </param>
        /// <param name="myArgs">
        /// The my args.
        /// </param>
        public delegate void RefreshObjectList(object myObject, EventArgs myArgs);

        /// <summary>
        /// 마우스 오른쪽 클릭에 대한 정의.
        /// 플레이어에서만 사용함.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        public override void MouseRightButtonDownProcess(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;
        }

        /// <summary>
        /// 마우스 오른쪽 클릭에 대한 정의.
        /// 플레이어에서만 사용함.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        public override void MouseRightButtonUpProcess(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;
        }

        /// <summary>
        /// 왼쪽 마우스 버튼 다운 구현.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        public override void MouseLeftButtonDownProcess(object sender, MouseButtonEventArgs e)
        {
            if (e.ClickCount == 2)
            {
                DoFocuseLayout(e);
                return;
            }

            if (sender is MapControl)
            {
                e.Handled = true;
                return;
            }

            e.Handled = true;
            this.DoDirectMouseLeftButtonDown(sender, e);
        }

        public override void StylusDownProcess(object sender, StylusDownEventArgs e)
        {
            if (e.InAir == false)
            {

            }
        }

        public override void TouchDownProcess(object sender, TouchEventArgs e)
        {

        }

        /// <summary>
        /// 마우스 움직일 때 구현.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        public override void MouseMoveProcess(object sender, MouseEventArgs e)
        {
            e.Handled = true;
            this.DoDirectMouseMove(e);
        }

        public override void StylusMoveProcess(object sender, StylusEventArgs e)
        {
            if (e.InAir == false)
            {

            }
        }

        public override void TouchMoveProcess(object sender, TouchEventArgs e)
        {

        }

        /// <summary>
        /// 왼쪽 마우스 버튼 업 구현.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        public override void MouseLeftButtonUpProcess(object sender, MouseButtonEventArgs e)
        {
            if (!this.isMouseDown)
            {
                return;
            }

            var element = GetBaseControl(e);

            // 선택한 엘리먼트가 베이스콘트롤이 아니면 빠져나간다.
            if (element == null)
            {
                return;
            }

            if (sender is MapControl)
            {
                e.Handled = true;
                return;
            }

            // 마우스를 해제한다.
            if (element.IsMouseCaptured)
            {
                element.ReleaseMouseCapture();
            }

            e.Handled = true;
            this.DoDirectMouseLeftButtonUp();
        }

        public override void StylusUpProcess(object sender, StylusEventArgs e)
        {
            if (e.InAir == false)
            {

            }
        }

        public override void TouchUpProcess(object sender, TouchEventArgs e)
        {

        }

        /// <summary>
        /// 마우스 휠 처리.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        public override void MouseWheelProcess(object sender, MouseWheelEventArgs e)
        {
            var element = GetBaseControl(e);
            if (element == null)
            {
                return;
            }

            if (element is MapControl)
            {
                this.owner.SelectedElements.SetLTWH();
                this.owner.SelectedElements.SetGisPosition();
            }
        }

        #region Touch

        public override void ManipulationStarting(object sender, ManipulationStartingEventArgs e)
        {

        }

        public override void ManipulationDelta(object sender, ManipulationDeltaEventArgs e)
        {

        }

        public override void ManipulationInertiaStarting(object sender, ManipulationInertiaStartingEventArgs e)
        {

        }

        #endregion // Touch

        /// <summary>
        /// The ActionRaise.
        /// </summary>
        /// <param name="lc">
        /// The lc.
        /// </param>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="triggerEventType">
        /// The trigger Event Type.
        /// </param>
        /// <returns>
        /// The action raise.
        /// </returns>
        protected bool ActionRaise(LayoutControl lc, object sender, TriggerEventType triggerEventType)
        {
            var element = sender as BaseControl;

            if (element == null)
            {
                return false;
            }

            var trigerElementGuid = GuidManager.GetSyncGuid(element);

            if (trigerElementGuid == Guid.Empty)
            {
                return false;
            }

            var args = new ActionEventArgs(triggerEventType);
            lc.ActionRaise(sender, args);

            return args.IsExcuted;
        }

        /// <summary>
        /// Layout을 선택하면 e.Source가 Layoutout이 아닌 insideCanvas나 outSideCanvas이기 때문에 사용한다.
        /// </summary>
        /// <param name="e">
        /// The e.
        /// </param>
        /// <returns>
        /// BaseControl을 반환한다.
        /// </returns>
        private static BaseControl GetBaseControl(MouseEventArgs e)
        {
            if (e.Source is BaseControl)
            {
                return (BaseControl)e.Source;
            }

            return LayoutUtils.GetParentBaseLayoutControl(e.Source as FrameworkElement);
        }

        /// <summary>
        /// 해당 엘리먼트에 포커스를 준다.
        /// </summary>
        /// <param name="e">
        /// The e.
        /// </param>
        private void DoFocuseLayout(MouseButtonEventArgs e)
        {
            var element = GetBaseControl(e);

            if (element == null)
            {
                return;
            }

            DoSelecteElement(element);
        }

        /// <summary>
        /// 엘리먼트를 선택한다.
        /// </summary>
        /// <param name="element">
        /// The element.
        /// </param>
        private void DoSelecteElement(BaseControl element)
        {
            if (element == null)
            {
                return;
            }

            if (!this.owner.SelectedElements.Contains(element))
            {
                this.owner.ElementSelect(element);
            }
        }

        /// <summary>
        /// 마우스를 가지고 있지 않다면 마우스를 일단 획득한다.
        /// CaptureMouse 후에는 MouseMove 이벤트가 자동 발생되므로 순서에 주의한다.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        private static void CaputureMouse(object sender)
        {
            if (!((BaseControl)sender).IsMouseCaptured)
            {
                ((BaseControl)sender).CaptureMouse();
            }
        }

        /// <summary>
        /// 엘리먼트를 선택한다.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void DoDirectMouseLeftButtonDown(object sender, MouseEventArgs e)
        {
            var element = sender as BaseControl;
            if (element == null)
            {
                return;
            }

            var parentLayout = element.ParentBaseLayoutControl;
            if (parentLayout == null)
            {
                return;
            }

            CaputureMouse(element);

            this.isMouseDown = true;

            this.startPoint = e.GetPosition(this.owner.xMapCanvas);
            this.owner.ElementSelect(element);
        }



        /// <summary>
        /// 엘리먼트를 움직인다.
        /// </summary>
        /// <param name="e">
        /// The e.
        /// </param>
        private void DoDirectMouseMove(MouseEventArgs e)
        {
            if (!this.isMouseDown)
            {
                return;
            }

            if (e.MouseDevice.LeftButton != MouseButtonState.Pressed)
            {
                return;
            }

            var element = GetBaseControl(e);

            if (element == null)
            {
                return;
            }

            var parentLayout = element.ParentBaseLayoutControl;

            if (parentLayout == null)
            {
                return;
            }

            if (!this.owner.SelectedElements.Contains(element))
            {
                return;
            }

            this.owner.DirectMove(element, this.startPoint, e);
            this.startPoint = e.GetPosition(this.owner.xMapCanvas);
        }

        /// <summary>
        /// 마우스다운을 종료한다.
        /// </summary>
        private void DoDirectMouseLeftButtonUp()
        {
            this.isMouseDown = false;
        }
    }
}