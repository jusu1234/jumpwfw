﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LocationUserControl.xaml.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Interaction logic for LocationUserControl.xaml
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.Console.Controls.Locations
{
    using System;
    using System.Windows;
    using System.Windows.Controls;
    
    using Innotive.InnoWatch.Commons.Utils;
    using Innotive.InnoWatch.Console.Models;
    using Innotive.InnoWatch.DLLs.TreeListBoxControls;

    using TreeListBoxControls;

    /// <summary>
    /// Interaction logic for LocationUserControl.xaml.
    /// </summary>
    public partial class LocationUserControl : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LocationUserControl"/> class.
        /// </summary>
        public LocationUserControl()
        {
            InitializeComponent();

            this.xLoadLocationButton.IsEnabled = false;

            this.xLocationSearchWithBookmarkUserControl.eLocationSearched += this.xLocationSearchWithBookmarkUserControl_eLocationSearched;
            this.xLocationSearchWithBookmarkUserControl.eLocationSearchItemDoubleClick += this.xLocationSearchWithBookmarkUserControl_eLocationSearchItemDoubleClick;
            this.xLocationSearchWithBookmarkUserControl.ShowPinObjectEventHandler += XLocationSearchWithBookmarkUserControlOnShowPinObjectEventHandler;

            this.xCameraGroupListUserControl.MouseDoubleClicked += this.XCameraGroupListUserControl_MouseDoubleClicked;
            this.xCameraGroupListUserControl.xTreeView_CameraList.SelectionChanged += this.xTreeView_CameraList_SelectionChanged;

            this.xLoadLocationButton.Click += this.xLoadLocationButton_Click;

            this.xExpander.Expanded += this.xExpander_Expanded;
            this.xExpander.Collapsed += this.xExpander_Collapsed;
        }

        private void xExpander_Collapsed(object sender, RoutedEventArgs e)
        {
            InnoRegistryUtil.Write("LocationUserControl_Expander", "Collapsed", InnoRegistryUtil.KeyType.Command);
        }

        private void xExpander_Expanded(object sender, RoutedEventArgs e)
        {
            InnoRegistryUtil.Write("LocationUserControl_Expander", "Expanded", InnoRegistryUtil.KeyType.Command);
        }

        private void XLocationSearchWithBookmarkUserControlOnShowPinObjectEventHandler(object sender, ShowPinObjectEventArgs showPinObjectEventArgs)
        {
            this.OnShwoPinObjectEventHandler(showPinObjectEventArgs.ShowPinObject);
        }

        /// <summary>
        /// Location Search.
        /// </summary>
        public event EventHandler<LocationListEventArgs> eLocationSearched;

        /// <summary>
        /// Location Search.
        /// </summary>
        public event EventHandler<ShowPinObjectEventArgs> ShwoPinObjectEventHandler;

        /// <summary>
        /// LocationSearchItemDoubleClick event handler.
        /// </summary>
        public event EventHandler<LocationEventArgs> eLocationSearchItemDoubleClick;

        /// <summary>
        /// LocationCameraItemDoubleClicked event handler.
        /// </summary>
        public event EventHandler<LocationCameraEventArgs> eLocationCameraItemDoubleClicked;

        /// <summary>
        /// LocationLoadButtonClicked event handler.
        /// </summary>
        public event EventHandler<LocationCameraEventArgs> eLocationLoadButtonClicked;

        /// <summary>
        /// 즐겨찾기 선택 초기화.
        /// </summary>
        public void ClearSelection()
        {
            this.xCameraGroupListUserControl.xTreeView_CameraList.SelectedItem = null;
        }

        /// <summary>
        /// 컨트롤 활성화 및 비활성화
        /// </summary>
        /// <param name="flag"></param>
        public void ToggleContentEnable(bool flag)
        {
            if (this.Dispatcher.CheckAccess())
            {
                this.xRootGrid.IsEnabled = flag;
            }
            else
            {
                this.Dispatcher.BeginInvoke(new Action(() =>
                {
                    this.xRootGrid.IsEnabled = flag;
                }));
            }
        }

        private void InvokeELocationSearched(object sender, LocationListEventArgs e)
        {
            var handler = this.eLocationSearched;

            if (handler != null)
            {
                handler(sender ?? this, e);
            }
        }

        public void OnShwoPinObjectEventHandler( bool showPinObject)
        {
            var handler = this.ShwoPinObjectEventHandler;
            if (handler != null)
            {
                handler(this, new ShowPinObjectEventArgs(showPinObject));
            }
        }

        private void InvokeELocationLoadButtonClicked(object sender, LocationCameraEventArgs e)
        {
            var handler = this.eLocationLoadButtonClicked;
            if (handler != null)
            {
                handler(sender ?? this, e);
            }
        }

        private void InvokeESearchItemDoubleClick(object sender, LocationEventArgs e)
        {
            var handler = this.eLocationSearchItemDoubleClick;
            if (handler != null)
            {
                handler(sender ?? this, e);
            }
        }

        private void InvokeELocationCameraItemDoubleClicked(object sender, LocationCameraEventArgs e)
        {
            var handler = this.eLocationCameraItemDoubleClicked;
            if (handler != null)
            {
                handler(sender ?? this, e);
            }
        }

        private void xLocationSearchWithBookmarkUserControl_eLocationSearchItemDoubleClick(object sender, LocationEventArgs e)
        {
            this.InvokeESearchItemDoubleClick(sender, e);
        }

        private void xLocationSearchWithBookmarkUserControl_eLocationSearched(object sender, LocationListEventArgs e)
        {

            this.InvokeELocationSearched(sender, e);
        }

        private void XCameraGroupListUserControl_MouseDoubleClicked(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            var listBox = sender as TreeListBox;
            if (listBox == null)
            {
                return;
            }

            var info = listBox.SelectedItem as TreeListBoxInfo;
            if (info == null)
            {
                return;
            }

            var camera = info.DataItem as Camera;
            if (camera == null)
            {
                return;
            }

            this.InvokeELocationCameraItemDoubleClicked(sender, new LocationCameraEventArgs(camera.Id));
        }

        private void xLoadLocationButton_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            var info = xCameraGroupListUserControl.xTreeView_CameraList.SelectedItem as TreeListBoxInfo;
            if (info == null)
            {
                return;
            }

            var camera = info.DataItem as Camera;
            if (camera == null)
            {
                return;
            }

            // 더블클릭 이벤트 사용.
            this.InvokeELocationLoadButtonClicked(sender, new LocationCameraEventArgs(camera.Id));
        }

        void xTreeView_CameraList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            this.xLoadLocationButton.IsEnabled = false;

            var info = xCameraGroupListUserControl.xTreeView_CameraList.SelectedItem as TreeListBoxInfo;
            if (info == null)
            {
                return;
            }

            var camera = info.DataItem as Camera;
            if (camera == null)
            {
                return;
            }

            if (!camera.IsFolder)
            {
                this.xLoadLocationButton.IsEnabled = true;
            }
        }
    }
}
