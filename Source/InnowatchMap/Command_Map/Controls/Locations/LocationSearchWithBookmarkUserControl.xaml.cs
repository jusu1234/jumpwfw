﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LocationSearchWithBookmarkUserControl.xaml.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Interaction logic for LocationSearchUserControl.xaml
// </summary>
// --------------------------------------------------------------------------------------------------------------------


namespace Innotive.InnoWatch.Console.Controls.Locations
{
    using System;
    using System.Windows;
    using System.Windows.Input;

    /// <summary>
    /// Interaction logic for LocationSearchUserControl.xaml.
    /// </summary>
    public partial class LocationSearchWithBookmarkUserControl
    {
        private readonly LocationSearchWithBookmarkUserControlViewModel viewModel = new LocationSearchWithBookmarkUserControlViewModel();
        private readonly LocationSearchUserControl locationSearchUserControl;
        private LocationBookmarkUserControl locationBookmarkUserControl;

        /// <summary>
        /// Initializes a new instance of the <see cref="LocationSearchWithBookmarkUserControl"/> class.
        /// </summary>
        public LocationSearchWithBookmarkUserControl()
        {
            InitializeComponent();
            this.DataContext = this.viewModel;

            this.locationSearchUserControl = new LocationSearchUserControl();
            this.xSearchNBookmarkPanel.Children.Add(this.locationSearchUserControl);
            this.locationSearchUserControl.LocationItemDoubleClick += this.locationSearchUserControl_ItemDoubleClick;

            this.locationBookmarkUserControl = null;

            this.xKeywordTextBox.KeyDown += this.xKeywordTextBox_KeyDown;
            this.xIsShowIcon.IsChecked = true;
        }

        /// <summary>
        /// Location Search.
        /// </summary>
        public event EventHandler<LocationListEventArgs> eLocationSearched;

        /// <summary>
        /// LocationSearchItemDoubleClick event handler.
        /// </summary>
        public event EventHandler<LocationEventArgs> eLocationSearchItemDoubleClick;

        public event EventHandler<ShowPinObjectEventArgs> ShowPinObjectEventHandler;

        public void OnShowPinObjectEventHandler(bool showPinObject)
        {
            var handler = this.ShowPinObjectEventHandler;
            if (handler != null)
            {
                handler(this, new ShowPinObjectEventArgs(showPinObject));
            }
        }

        private void InvokeELocationSearched(object sender, LocationListEventArgs e)
        {
            var handler = this.eLocationSearched;

            if (handler != null)
            {
                handler(sender ?? this, e);
            }
        }

        private void InvokeESearchItemDoubleClick(object sender, LocationEventArgs e)
        {
            var handler = this.eLocationSearchItemDoubleClick;
            if (handler != null)
            {
                handler(sender ?? this, e);
            }
        }

        private void locationSearchUserControl_ItemDoubleClick(object sender, LocationEventArgs e)
        {
            this.InvokeESearchItemDoubleClick(sender, e);
        }

        private void BookmarkButtonClick(object sender, RoutedEventArgs e)
        {
            if (this.xBookmarkListButton.IsChecked == true)
            {
                this.xIsShowIcon.Visibility = Visibility.Hidden;
                this.OpenLocationBookmarkUserControl();
                
            }
            else
            {
                this.xIsShowIcon.Visibility = Visibility.Visible;
                this.CloseLocationBookmarkUserControl();
            }
        }

        private void xLocationSearchButton_Click(object sender, RoutedEventArgs e)
        {
            this.CloseLocationBookmarkUserControl();
            this.Search();
        }

        private void xKeywordTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                this.xBookmarkListButton.IsChecked = false;
                this.CloseLocationBookmarkUserControl();
                this.Search();
            }
        }

        private void LocationBookmarkUserControlCloseButton_Click(object sender, RoutedEventArgs e)
        {
            this.xBookmarkListButton.IsChecked = false;
            this.CloseLocationBookmarkUserControl();
        }

        private void locationBookmarkUserControl_ItemDoubleClick(object sender, LocationEventArgs e)
        {
            this.InvokeESearchItemDoubleClick(sender, e);   
        }

        private void Search()
        {
            this.locationSearchUserControl.Search(this.viewModel.GoogleSearch);

            var locationList = new LocationListEventArgs((bool)this.xIsShowIcon.IsChecked);
            var count = 0;

            foreach (var location in this.locationSearchUserControl.Locations)
            {
                var info = new LocationListEventArgs.SearchedLocationInfo
                {
                    Content = location.Content,
                    Index = count,
                    Latitude = location.Lat,
                    Longitude = location.Lng
                };

                locationList.LocationInfoList.Add(info);

                count++;
            }

            this.InvokeELocationSearched(this, locationList);
        }

        private void OpenLocationBookmarkUserControl()
        {
            if (this.locationBookmarkUserControl != null)
            {
                return;
            }

            this.locationBookmarkUserControl = new LocationBookmarkUserControl();
            this.xSearchNBookmarkPanel.Children.Add(this.locationBookmarkUserControl);
            this.locationBookmarkUserControl.xCloseButton.Click += this.LocationBookmarkUserControlCloseButton_Click;
            this.locationBookmarkUserControl.LocationItemDoubleClick += this.locationBookmarkUserControl_ItemDoubleClick;
        }

        private void CloseLocationBookmarkUserControl()
        {
            if (this.locationBookmarkUserControl == null)
            {
                return;
            }

            this.locationBookmarkUserControl.xCloseButton.Click -= this.LocationBookmarkUserControlCloseButton_Click;
            this.locationBookmarkUserControl.LocationItemDoubleClick -= this.locationBookmarkUserControl_ItemDoubleClick;
            this.xSearchNBookmarkPanel.Children.Remove(this.locationBookmarkUserControl);
            this.locationBookmarkUserControl = null;

            this.locationSearchUserControl.RefreshLocationBookmark();
            this.Search();
        }

        private void xIsShowIcon_Checked(object sender, RoutedEventArgs e)
        {
            var isShowChecked = false;
            if (this.xIsShowIcon.IsChecked != null)
            {
                isShowChecked = (bool)this.xIsShowIcon.IsChecked;
            }

            this.OnShowPinObjectEventHandler(isShowChecked);    
        }

        private void xIsShowIcon_Unchecked(object sender, RoutedEventArgs e)
        {
            var isShowChecked = false;
            if (this.xIsShowIcon.IsChecked != null)
            {
                isShowChecked = (bool)this.xIsShowIcon.IsChecked;
            }

            this.OnShowPinObjectEventHandler(isShowChecked);    

        }

        private void XKeywordTextBoxKeyUp(object sender, KeyEventArgs e)
        {
            // 텍스트 박스에서 생긴 키 이벤트는 텍스트박스에서만 동작해야 하므로 이벤트 연결이 더이상 내려가지 않도록 한다.
            switch (e.Key)
            {
                case Key.F12:
                case Key.F11:
                case Key.F10:
                case Key.F9:
                case Key.F8:
                case Key.F7:
                case Key.F6:
                case Key.F5:
                case Key.F4:
                case Key.F3:
                case Key.F2:
                case Key.F1:
                    break;
                default:
                    e.Handled = true;
                    break;
            }
        }
    }
}
