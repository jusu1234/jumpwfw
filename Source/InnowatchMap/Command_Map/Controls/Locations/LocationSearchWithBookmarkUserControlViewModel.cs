﻿// -----------------------------------------------------------------------
// <copyright file="LacationSearchWithBookmarkUserControlViewModel.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace Innotive.InnoWatch.Console.Controls.Locations
{
    using System.ComponentModel;
    using System.Windows;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class LocationSearchWithBookmarkUserControlViewModel: INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private bool isSearchView;
        private string googleSearch;

        public string GoogleSearch
        {
            get
            {
                return this.googleSearch;
            }
            set
            {
                this.googleSearch = value;
                this.OnPropertyChanged("GoogleSearch");
            }
        }

        public bool IsSerchView
        {
            get
            {
                return this.isSearchView;
            }
            set
            {
                this.isSearchView = value;
                if (value)
                {
                    this.SearchVisibility = Visibility.Visible;
                    this.BookmarkVisibility = Visibility.Collapsed;
                }
                else
                {
                    this.SearchVisibility = Visibility.Collapsed;
                    this.BookmarkVisibility = Visibility.Visible;
                }
                this.OnPropertyChanged("IsSearchView");
            }
        }

        private Visibility searchVisibility;

        public Visibility SearchVisibility
        {
            get
            {
                return this.searchVisibility;
            }
            set
            {
                this.searchVisibility = value;
                this.OnPropertyChanged("SearchVisibility");
            }
        }

        private Visibility bookmarkVisibility;

        public Visibility BookmarkVisibility
        {
            get
            {
                return this.bookmarkVisibility;
            }
            set
            {
                this.bookmarkVisibility = value;
                this.OnPropertyChanged("BookmarkVisibility");
            }
        }

        public void OnPropertyChanged(string propertyName)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
