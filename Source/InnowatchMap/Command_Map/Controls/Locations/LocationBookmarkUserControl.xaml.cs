﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LocationBookmarkUserControl.xaml.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Interaction logic for LocationBookmarkUserControl.xaml
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.Console.Controls.Locations
{
    using System;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;
    using System.Windows.Media;

    using Innotive.InnoWatch.Console.LocationBookmarkXmls;

    /// <summary>
    /// Interaction logic for LocationBookmarkUserControl.xaml.
    /// </summary>
    public partial class LocationBookmarkUserControl
    {
        private readonly LocationBookmarkUserControlViewModel viewModel = new LocationBookmarkUserControlViewModel();

        private Point startPoint;

        private ListBoxItem selectedItem;

        private int oldIndex;

        /// <summary>
        /// Initializes a new instance of the <see cref="LocationBookmarkUserControl"/> class.
        /// </summary>
        public LocationBookmarkUserControl()
        {
            InitializeComponent();
            this.DataContext = this.viewModel;
        }

        /// <summary>
        /// 아이템 더블클릭 이벤트.
        /// </summary>
        public event EventHandler<LocationEventArgs> LocationItemDoubleClick;

        /// <summary>
        /// Reload Bookmark.
        /// </summary>
        public void ReloadBookmark()
        {
            this.viewModel.ReloadBookmark();
        }

        private void InvokeLocationItemDoubleClick(object sender, LocationEventArgs e)
        {
            var handler = this.LocationItemDoubleClick;
            if (handler != null)
            {
                handler(sender ?? this, e);
            }
        }

        private void xCloseMark_Click(object sender, RoutedEventArgs e)
        {
            var button = sender as Button; 
            if (button != null)
            {
                var item = button.DataContext as Item;
                if (item != null)
                {
                    this.viewModel.DeleteItem(item); 
                }
            } 
            else 
            { 
                return; 
            } 
        }

        // 1. 마우스 왼쪽 버튼을 클릭할 때
        private void ListBox_OnPreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            var listBox = sender as ListBox;
            if (listBox == null)
            {
                return;
            }

            // 시작점을 저장한다.
            this.startPoint = e.GetPosition(null);

            // 현재 마우스가 있는 리스트 박스 아이템을 찾는다.
            this.selectedItem = FindAnchestor<ListBoxItem>((DependencyObject)e.OriginalSource);
            
            if (this.selectedItem != null)
            {
                this.oldIndex = listBox.ItemContainerGenerator.IndexFromContainer(this.selectedItem);
            }
        }

        // 2. 마우스를 드래그할 때
        private void ListBox_OnPreviewMouseMove(object sender, MouseEventArgs e)
        {
            if (this.selectedItem == null)
            {
                return;
            }

            var mousePos = e.GetPosition(null);
            var diff = this.startPoint - mousePos;

            // 눌려져 있지 않으면 빠져나간다.
            if (e.LeftButton != MouseButtonState.Pressed)
            {
                return;
            }

            // 시스템에서 정의한 드래그 거리에 미치지 못하면 빠져나간다.
            if ((Math.Abs(diff.X) <= SystemParameters.MinimumHorizontalDragDistance) &&
            (Math.Abs(diff.Y) <= SystemParameters.MinimumVerticalDragDistance))
            {
                return;
            }

            // 현재 리스트 박스를 찾는다.
            var listBox = sender as ListBox;
            if (listBox == null)
            {
                return;
            }
            
            // 드래그에 사용할 데이타 서식을 "StringItem"으로 지정합니다.
            var data = (Item)listBox.ItemContainerGenerator.ItemFromContainer(this.selectedItem);
            var dragData = new DataObject("StringItem", data);

            System.Windows.DragDrop.DoDragDrop(this.selectedItem, dragData, DragDropEffects.Move);
        }

        // 3. 드랍 가능한 위치에 들어 왔을 때
        private void ListBox_OnDragEnter(object sender, DragEventArgs e)
        {
            if (!e.Data.GetDataPresent("StringItem") || sender == e.Source)
            {
                e.Effects = DragDropEffects.None;
            }
        }

        // 4. 아이템을 드랍할 때
        private void ListBox_OnDrop(object sender, DragEventArgs e)
        {
            if (!e.Data.GetDataPresent("StringItem"))
            {
                return;
            }

            var listBox = sender as ListBox;
            if (listBox == null)
            {
                return;
            }

            var listBoxItem = FindAnchestor<ListBoxItem>((DependencyObject)e.OriginalSource);

            // 아이템이 아닌곳에 드랍하면 맨 밑으로 이동시킨다.(리스트박스에 빈 화면이 있는 경우)
            if (listBoxItem == null)
            {
                this.viewModel.Items.Move(this.oldIndex, this.viewModel.Items.Count - 1);
                return;
            }

            var index = listBox.ItemContainerGenerator.IndexFromContainer(listBoxItem);
            this.viewModel.Items.Move(this.oldIndex, index);
        }

        //  해당 오브젝트가 나올때까지 부모를 찾아간다.
        private static T FindAnchestor<T>(DependencyObject current) where T : DependencyObject
        {
            do
            {
                if (current is T)
                {
                    return (T)current;
                }
                current = VisualTreeHelper.GetParent(current);
            }
            while (current != null);
            return null;
        }

        private void xLoadButton_Click(object sender, RoutedEventArgs e)
        {
            var location = this.xBookmarkListBox.SelectedItem as Item;
            if (location == null)
            {
                return;
            }

            this.InvokeLocationItemDoubleClick(sender, new LocationEventArgs(location.Lat, location.Lng));
        }

        private void xBookmarkListBox_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var location = this.xBookmarkListBox.SelectedItem as Item;
            if (location == null)
            {
                return;
            }

            this.InvokeLocationItemDoubleClick(sender, new LocationEventArgs(location.Lat, location.Lng));
        }
    }
}
