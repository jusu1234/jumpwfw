// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LocationSearchDragSourceAdvisor.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   LocationSearchDragSourceAdvisor Class.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.Console.Controls.DragDrop
{
    using System.Linq;
    using System.Windows;
    using System.Windows.Controls;

    using Innotive.InnoWatch.Commons.Map;
    using Innotive.InnoWatch.Commons.Utils;

    /// <summary>
    /// LocationSearchDragSourceAdvisor Class.
    /// </summary>
    public class LocationSearchDragSourceAdvisor : IDragSourceAdvisor
    {
        /// <summary>
        /// 데이터 형식.
        /// </summary>
        private static readonly DataFormat SupportedFormat = DataFormats.GetDataFormat("Innotive.Location");

        /// <summary>
        /// Gets SupportedEffects.
        /// </summary>
        public DragDropEffects SupportedEffects
        {
            get
            {
                return DragDropEffects.Copy;
            }
        }

        /// <summary>
        /// Gets or sets SourceUI.
        /// </summary>
        public UIElement SourceUI { get; set; }

        /// <summary>
        /// 드래그 된 데이타 객체를 반환한다.
        /// </summary>
        /// <param name="draggedElt">
        /// The dragged elt.
        /// </param>
        /// <returns>
        /// 데이터 객체.
        /// </returns>
        public DataObject GetDataObject(UIElement draggedElt)
        {
            var source = this.SourceUI as ListBox;

            if (source == null)
            {
                return null;
            }

            var datas = source.SelectedItems.Cast<GiocodeData>().ToList();
            return new DataObject(SupportedFormat.Name, datas);
        }

        /// <summary>
        /// </summary>
        /// <param name="dragElt">
        /// The drag elt.
        /// </param>
        /// <returns>
        /// </returns>
        public bool IsDraggable(UIElement dragElt)
        {
            var source = this.SourceUI as ListBox;
            if (source == null)
            {
                return false;
            }

            foreach (var item in source.Items)
            {
                var container = source.ItemContainerGenerator.ContainerFromItem(item) as ListBoxItem;
                if (container != null)
                {
                    if (container.IsMouseOver)
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        /// <summary>
        /// </summary>
        /// <returns>
        /// </returns>
        public UIElement GetTopContainer()
        {
            return Application.Current.MainWindow.Content as UIElement;
        }

        /// <summary>
        /// </summary>
        /// <param name="draggedElt">
        /// The dragged elt.
        /// </param>
        /// <param name="finalEffects">
        /// The final effects.
        /// </param>
        public void FinishDrag(UIElement draggedElt, DragDropEffects finalEffects)
        {

        }
    }
}