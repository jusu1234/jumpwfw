// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CameraListDragSourceAdvisor.cs" company="Innotive Inc. Korea">
//   Copyright ⓒ Innotive inc. Korea 2011
// </copyright>
// <summary>
//   CameraListDragSourceAdvisor class.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.Console.Controls.DragDrop
{
    using System.Collections.Generic;
    using System.Windows;
    using Innotive.InnoWatch.Commons.Utils;
    using Innotive.InnoWatch.Console.Models;
    using TreeListBoxControls;

    /// <summary>
    /// CameraListDragSourceAdvisor class.
    /// </summary>
    public class CameraListDragSourceAdvisor : IDragSourceAdvisor
    {
        /// <summary>
        /// 데이터 형식.
        /// </summary>
        private static readonly DataFormat SupportedFormatForCamera = DataFormats.GetDataFormat("Innotive.Camera");
        private static readonly DataFormat SupportedFormatForLocation = DataFormats.GetDataFormat("Innotive.Location");

        /// <summary>
        /// Initializes a new instance of the <see cref="CameraListDragSourceAdvisor"/> class.
        /// </summary>
        /// <param name="sourceType">
        /// The source Type.
        /// </param>
        public CameraListDragSourceAdvisor(CameraListDragSourceAdvisorSourceType sourceType)
        {
            this.SourceType = sourceType;
        }

        #region IDragSourceAdvisor Members

        /// <summary>
        /// 소스 타입.
        /// </summary>
        public enum CameraListDragSourceAdvisorSourceType
        {
            /// <summary>
            /// 카메라 리스트 아이템.
            /// </summary>
            Camera,

            /// <summary>
            /// 로케이션 아이템.
            /// </summary>
            Location
        }

        /// <summary>
        /// Gets SupportedEffects.
        /// </summary>
        public DragDropEffects SupportedEffects
        {
            get
            {
                return DragDropEffects.Copy;
            }
        }

        /// <summary>
        /// Gets or sets SourceUI.
        /// </summary>
        public UIElement SourceUI { get; set; }

        /// <summary>
        /// Gets or sets SourceType.
        /// </summary>
        public CameraListDragSourceAdvisorSourceType SourceType { get; set; }

        /// <summary>
        /// Return data object.
        /// </summary>
        /// <param name="draggedElt">
        /// The dragged elt.
        /// </param>
        /// <returns>
        /// DataObject instance.
        /// </returns>
        public DataObject GetDataObject(UIElement draggedElt)
        {
            var control = this.SourceUI as TreeListBox;
            if (control != null)
            {
                if (control.SelectedItems.Count > 0)
                {
                    var datas = new List<Camera>();
                    foreach (var item in control.SelectedItems)
                    {
                        if (item is TreeListBoxInfo)
                        {
                            var camera = (item as TreeListBoxInfo).DataItem as Camera;
                            if (camera != null)
                            {
                                if (!camera.IsFolder)
                                {
                                    datas.Add(camera);
                                }
                            }
                        }
                    }

                    if (this.SourceType == CameraListDragSourceAdvisorSourceType.Camera)
                    {
                        return new DataObject(SupportedFormatForCamera.Name, datas);
                    }

                    if (this.SourceType == CameraListDragSourceAdvisorSourceType.Location)
                    {
                        var dataList = new List<Map>();
                        foreach (var mapData in datas)
                        {
                            var map = new Map { FocusedCameraId = mapData.Id };
                            dataList.Add(map);
                        }

                        datas.Clear();

                        return new DataObject(SupportedFormatForLocation.Name, dataList);
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// 드레그 완료 후 작업.
        /// </summary>
        /// <param name="draggedElt">
        /// The dragged elt.
        /// </param>
        /// <param name="finalEffects">
        /// The final effects.
        /// </param>
        public void FinishDrag(UIElement draggedElt, DragDropEffects finalEffects)
        {
        }

        /// <summary>
        /// 드래그 가능 여부 검사.
        /// </summary>
        /// <param name="dragElt">
        /// The drag elt.
        /// </param>
        /// <returns>
        /// 가능 여부.
        /// </returns>
        public bool IsDraggable(UIElement dragElt)
        {
            var control = this.SourceUI as TreeListBox;
            if (control != null)
            {
                if (control.MouseDownOnScrollBar)
                {
                    return false;
                }

                foreach (var item in control.SelectedItems)
                {
                    if (item is TreeListBoxInfo)
                    {
                        var camera = (item as TreeListBoxInfo).DataItem as Innotive.InnoWatch.Console.Models.Camera;
                        if (camera != null)
                        {
                            if (camera.IsFolder)
                            {
                                return false;
                            }
                        }
                    }
                    else
                    {
                        return false;
                    }
                }

                return true;
            }

            return false;
        }

        /// <summary>
        /// GetTopContainer return.
        /// </summary>
        /// <returns>
        /// UIElement instance.
        /// </returns>
        public UIElement GetTopContainer()
        {
            return Application.Current.MainWindow.Content as UIElement;
        }

        #endregion
    }
}