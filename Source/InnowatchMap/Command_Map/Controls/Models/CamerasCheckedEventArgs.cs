﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CamerasCheckedEventArgs.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   카메라 체크 이벤트 속성.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.Console.Models
{
    using System;

    /// <summary>
    /// 카메라 체크 이벤트 속성.
    /// </summary>
    public class CamerasCheckedEventArgs : EventArgs
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="CamerasCheckedEventArgs"/> class. 
        /// 클래스 생성자.
        /// </summary>
        /// <param name="current">
        /// 카메라.
        /// </param>
        /// <param name="isChecked">
        /// 체크 여부.
        /// </param>
        public CamerasCheckedEventArgs(Camera current, bool isChecked)
        {
            this.Current = current;
            this.IsChecked = isChecked;
        }

        #endregion

        #region Properties

        /// <summary>
        /// 카메라.
        /// </summary>
        public Camera Current { get; protected set; }

        /// <summary>
        /// 체크 여부.
        /// </summary>
        public bool IsChecked { get; protected set; }

        #endregion
    }
}