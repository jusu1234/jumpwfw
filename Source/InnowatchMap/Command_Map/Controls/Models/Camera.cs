// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Camera.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   카메라 클래스.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.Console.Models
{
    using System;
    using System.Windows.Media;
    using Innotive.InnoWatch.Commons.Models;

    /// <summary>
    /// 카메라 클래스.
    /// </summary>
    public class Camera : BaseModel, IEquatable<Camera>, IHasDepth
    {
        /// <summary>
        /// 카메라 CameraHeight.
        /// </summary>
        private int cameraHeight = 0;

        /// <summary>
        /// 카메라 CameraWidth.
        /// </summary>
        private int cameraWidth = 0;

        /// <summary>
        /// 캐스트 서버 ID.
        /// </summary>
        private string castServerId = string.Empty;

        /// <summary>
        /// GridControl에 선택 되었을 때 색상.
        /// </summary>
        private Brush colorOfSelectedByGridControl = new SolidColorBrush(Colors.Blue);

        /// <summary>
        /// 카메라 InternalHost.
        /// </summary>
        private string internalHost = string.Empty;

        /// <summary>
        /// 카메라 선택 여부.
        /// </summary>
        private bool isChecked = false;

        /// <summary>
        /// 체크 가능 여부.
        /// </summary>
        private bool isCheckingEnabled = false;

        /// <summary>
        /// 폴더 인지 여부.
        /// </summary>
        private bool isFolder = false;

        /// <summary>
        /// 맵에 올라간 카메라인지 여부.
        /// </summary>
        private bool isLoadedMap = false;

        /// <summary>
        /// 재생 여부.
        /// </summary>
        private bool isPlaying = false;

        /// <summary>
        /// GridControl에 선택 여부.
        /// </summary>
        private bool isSelectedByGridControl = false;

        /// <summary>
        /// 포함된 카메라 컬렉션.
        /// </summary>
        private readonly Cameras items = new Cameras();

        /// <summary>
        /// 카메라 Mac Address.
        /// </summary>
        private string macAddress = string.Empty;

        /// <summary>
        /// 카메라 Manufacturer.
        /// </summary>
        private string manufacturer = string.Empty;

        /// <summary>
        /// 카메라 ModelNo.
        /// </summary>
        private string modelNo = string.Empty;
        
        /// <summary>
        /// 카메라 Port.
        /// </summary>
        private string port = string.Empty;

        /// <summary>
        /// 제어 타입
        /// </summary>
        private string controlType = string.Empty;

        /// <summary>
        /// 내부에 사용되는 카메라 체크 이벤트 사용 여부.
        /// </summary>
        private bool raiseCamerasChecked = true;

        /// <summary>
        /// 카메라 RdsHost.
        /// </summary>
        private string rdsIp = string.Empty;

        /// <summary>
        /// 카메라 RdsPort.
        /// </summary>
        private int rdsPort = 5000;

        private bool isNotFolder;

        /// <summary>
        /// Initializes a new instance of the <see cref="Camera"/> class. 
        /// 클래스 생성자.
        /// </summary>
        public Camera()
        {
            this.Parent = null;
            this.Items.Parent = this;
            this.IsProcessingParentMustChangeIsCheckedStatus = false;
        }

        /// <summary>
        /// 카메라 체크 이벤트 (한번에 여러개의 목록을 가져오기 위한 이벤트).
        /// </summary>
        public static event EventHandler<CamerasCheckedEventArgs> eCamerasChecked = null;

        /// <summary>
        /// 카메라 CameraHeight.
        /// </summary>
        public int CameraHeight
        {
            get
            {
                return this.cameraHeight;
            }

            set
            {
                this.cameraHeight = value;
                this.NotifyPropertyChanged("CameraHeight");
            }
        }

        /// <summary>
        /// 카메라 CameraWidth.
        /// </summary>
        public int CameraWidth
        {
            get
            {
                return this.cameraWidth;
            }

            set
            {
                this.cameraWidth = value;
                this.NotifyPropertyChanged("CameraWidth");
            }
        }

        /// <summary>
        /// 캐스트 서버 ID.
        /// </summary>
        public string CastServerId
        {
            get
            {
                return this.castServerId;
            }

            set
            {
                this.castServerId = value;
                this.NotifyPropertyChanged("CastServerId");
            }
        }

        /// <summary>
        /// Gets ChildCount.
        /// </summary>
        public int ChildCount
        {
            get
            {
                return this.Items.Count;
            }
        }

        /// <summary>
        /// GridControl에 선택 되었을 때 색상.
        /// </summary>
        public Brush ColorSelectedByGridControl
        {
            get
            {
                return this.colorOfSelectedByGridControl;
            }

            set
            {
                this.colorOfSelectedByGridControl = value;
                this.NotifyPropertyChanged("ColorSelectedByGridControl");
            }
        }

        /// <summary>
        /// Gets Depth.
        /// </summary>
        public int Depth
        {
            get
            {
                return this.CountParent(this, 0);
            }
        }

        /// <summary>
        /// 카메라 InternalHost.
        /// </summary>
        public string InternalHost
        {
            get
            {
                return this.internalHost;
            }

            set
            {
                this.internalHost = value;
                this.NotifyPropertyChanged("InternalHost");
            }
        }

        /// <summary>
        /// 카메라 선택 여부.
        /// </summary>
        public bool IsChecked
        {
            get
            {
                return this.isChecked;
            }

            set
            {
                if (this.isChecked != value)
                {
                    this.isChecked = value;
                    this.NotifyPropertyChanged("IsChecked");

                    if (this.raiseCamerasChecked)
                    {
                        if (eCamerasChecked != null)
                        {
                            eCamerasChecked(this, new CamerasCheckedEventArgs(this, value));
                        }
                    }

                    // 부모 체크 상태 변경 여부 검사 중이 아니면
                    if (!this.IsProcessingParentMustChangeIsCheckedStatus)
                    {
                        // 하위 아이템들 검사                        
                        this.CheckDown(this.Items, value);

                        // 상위 아이템이 있으면 검사 (같은 레벨에 있는 체크가 모두 같아지면)
                        this.CheckUp(this.Parent, value);
                    }

                    // 체크 여부 이벤트 버블링
                    this.CheckingEventBubbling(this, value);
                }
            }
        }

        /// <summary>
        /// 체크 가능 여부.
        /// </summary>
        public bool IsCheckingEnabled
        {
            get
            {
                return this.isCheckingEnabled;
            }

            set
            {
                this.isCheckingEnabled = value;
                this.NotifyPropertyChanged("IsCheckingEnabled");
            }
        }

        /// <summary>
        /// 폴더 인지 여부.
        /// </summary>
        public bool IsFolder
        {
            get
            {
                return this.isFolder;
            }

            set
            {
                this.isFolder = value;
                this.IsNotFolder = !value;
                this.NotifyPropertyChanged("IsFolder");
            }
        }

        /// <summary>
        /// 폴더 인지 여부.
        /// </summary>
        public bool IsNotFolder
        {
            get
            {
                return this.isNotFolder;
            }

            set
            {
                this.isNotFolder = value;
                this.NotifyPropertyChanged("IsNotFolder");
              
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether IsLoadedMap.
        /// 폴더 인지 여부.
        /// </summary>
        public bool IsLoadedMap
        {
            get
            {
                return this.isLoadedMap;
            }

            set
            {
                this.isLoadedMap = value;
                this.NotifyPropertyChanged("IsLoadedMap");
            }
        }

        /// <summary>
        /// 재생 여부.
        /// </summary>
        public bool IsPlaying
        {
            get
            {
                return this.isPlaying;
            }

            set
            {
                this.isPlaying = value;
                this.NotifyPropertyChanged("IsPlaying");
            }
        }

        /// <summary>
        /// 카메라가 RDS 인지 여부 반환.
        /// </summary>
        public bool IsRDS
        {
            get
            {
                if (string.Compare(this.controlType, "RDS", true) == 0)
                {
                    return true;
                }

                return false;
            }
        }

        /// <summary>
        /// GridControl에 선택 여부.
        /// </summary>
        public bool IsSelectedByGridControl
        {
            get
            {
                return this.isSelectedByGridControl;
            }

            set
            {
                this.isSelectedByGridControl = value;
                this.NotifyPropertyChanged("IsSelectedByGridControl");

                if (this.Parent != null)
                {
                    if (this.Parent.Parent != null)
                    {
                        this.Parent.Parent.ColorSelectedByGridControl = this.ColorSelectedByGridControl;
                        this.Parent.Parent.IsSelectedByGridControl = value;
                    }
                }
            }
        }

        /// <summary>
        /// 포함된 카메라 컬렉션.
        /// </summary>
        public Cameras Items
        {
            get
            {
                return this.items;
            }
        }

        /// <summary>
        /// 카메라 Mac Address.
        /// </summary>
        public string MacAddress
        {
            get
            {
                return this.macAddress;
            }

            set
            {
                this.macAddress = value;
                this.NotifyPropertyChanged("MacAddress");
            }
        }

        /// <summary>
        /// 카메라 Manufacturer.
        /// </summary>
        public string Manufacturer
        {
            get
            {
                return this.manufacturer;
            }

            set
            {
                this.manufacturer = value;
                this.NotifyPropertyChanged("Manufacturer");
            }
        }

        /// <summary>
        /// 카메라 ModelNo.
        /// </summary>
        public string ModelNo
        {
            get
            {
                return this.modelNo;
            }

            set
            {
                this.modelNo = value;
                this.NotifyPropertyChanged("ModelNo");
            }
        }
        
        /// <summary>
        /// 부모 참조.
        /// </summary>
        public Cameras Parent { get; set; }

        /// <summary>
        /// 카메라 Port.
        /// </summary>
        public string Port
        {
            get
            {
                return this.port;
            }

            set
            {
                this.port = value;
                this.NotifyPropertyChanged("Port");
            }
        }

        /// <summary>
        /// 제어 Type.
        /// </summary>
        public string ControlType
        {
            get
            {
                return this.controlType;
            }

            set
            {
                this.controlType = value;
                this.NotifyPropertyChanged("ControlType");
            }
        }

        /// <summary>
        /// PTZ 가능 여부.
        /// </summary>
        public bool PTZEnabled
        {
            get
            {
                if (string.Compare(this.ControlType, "PTZ", true) == 0)
                {
                    return true;
                }

                return false;
            }
        }

        /// <summary>
        /// 카메라 RdsHost.
        /// </summary>
        public string RdsHost
        {
            get
            {
                return this.rdsIp;
            }

            set
            {
                this.rdsIp = value;
                this.NotifyPropertyChanged("RdsHost");
            }
        }

        /// <summary>
        /// 카메라 RdsPort.
        /// </summary>
        public int RdsPort
        {
            get
            {
                return this.rdsPort;
            }

            set
            {
                this.rdsPort = value;
                this.NotifyPropertyChanged("RdsPort");
            }
        }

        /// <summary>
        /// 객체 ID.
        /// </summary>
        public int ItemOrder
        {
            get
            {
                if (this.Parent == null)
                {
                    return 0;
                }
                else
                {
                    return this.Parent.IndexOf(this);
                }
            }

            //set
            //{
            //    if (value != this._itemOrder)
            //    {
            //        this._itemOrder = value;
            //        this.NotifyPropertyChanged("ItemOrder");
            //    }
            //}
        }

        /// <summary>
        /// 부모 체크를 변경해야 하는지 여부 검사 중 여부.
        /// </summary>
        private bool IsProcessingParentMustChangeIsCheckedStatus { get; set; }

        /// <summary>
        /// 체크 여부 이벤트 버블링.
        /// </summary>
        /// <param name="current">
        /// 대상 카메라.
        /// </param>
        /// <param name="isChecked">
        /// 체크 여부.
        /// </param>
        public void CheckingEventBubbling(Camera current, bool isChecked)
        {
            if (this.Parent != null)
            {
                this.Parent.CheckingEventBubbling(current, isChecked);
            }
        }

        /// <summary>
        /// 복사본.
        /// </summary>
        public Camera Clone()
        {
            var camera = new Camera
                {
                    Id = this.Id,
                    Name = this.Name,
                    CastServerId = this.CastServerId,
                    Port = this.Port,
                    MacAddress = this.MacAddress,
                    ModelNo = this.ModelNo,
                    Manufacturer = this.Manufacturer,
                    ControlType =  this.ControlType,
                };

            return camera;
        }

        /// <summary>
        /// The count parent.
        /// </summary>
        /// <param name="camera">
        /// The camera.
        /// </param>
        /// <param name="count">
        /// The count.
        /// </param>
        /// <returns>
        /// The count parent.
        /// </returns>
        public int CountParent(Camera camera, int count)
        {
            if (camera.Parent != null)
            {
                if (camera.Parent.Parent != null)
                {
                    count++;

                    return this.CountParent(camera.Parent.Parent, count);
                }
            }

            return count;
        }

        /// <summary>
        /// 내부에 사용되는 카메라 체크 이벤트 사용 미사용.
        /// </summary>
        public void DisableCamerasCheckedEvent()
        {
            this.raiseCamerasChecked = false;
        }

        /// <summary>
        /// 내부에 사용되는 카메라 체크 이벤트 사용 설정.
        /// </summary>
        public void EnableCamerasCheckedEvent()
        {
            this.raiseCamerasChecked = true;
        }

        /// <summary>
        /// 내부에 사용되는 카메라 체크 이벤트 사용 여부.
        /// </summary>
        /// <returns>
        /// The get cameras checked event.
        /// </returns>
        public bool GetCamerasCheckedEvent()
        {
            return this.raiseCamerasChecked;
        }

        /// <summary>
        /// 내부에 사용되는 카메라 체크 이벤트 사용 여부 설정.
        /// </summary>
        /// <param name="flag">
        /// The flag.
        /// </param>
        public void SetCamerasCheckedEvent(bool flag)
        {
            this.raiseCamerasChecked = flag;
        }

        /// <summary>
        /// The to string.
        /// </summary>
        /// <returns>
        /// The to string.
        /// </returns>
        public override string ToString()
        {
            return "Camera(Id: " + this.Id + ", Name: " + this.Name + ", IsChecked: " + this.IsChecked + ", IsFolder: " +
                   this.IsFolder + ") ";
        }

        /// <summary>
        /// The equals.
        /// </summary>
        /// <param name="other">
        /// The other.
        /// </param>
        /// <returns>
        /// The equals.
        /// </returns>
        public bool Equals(Camera other)
        {
            if (!(this as BaseModel).Equals(other as BaseModel))
            {
                return false;
            }

            if (!this.IsFolder.Equals(other.IsFolder))
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// 하위 아이템 체크 검사.
        /// </summary>
        /// <param name="currents">
        /// The currents.
        /// </param>
        /// <param name="flag">
        /// The flag.
        /// </param>
        protected void CheckDown(Cameras currents, bool flag)
        {
            foreach (Camera Current in currents)
            {
                Current.raiseCamerasChecked = false;
                Current.IsChecked = flag;
                Current.raiseCamerasChecked = true;

                // 재귀적으로 하지 않아도 됨 (INotifyPropertyChanged가 알아서 함)
                // Current.CheckDown(Current.Items, flag);
            }
        }

        /// <summary>
        /// 상위 아이템 체크 검사.
        /// </summary>
        /// <param name="currents">
        /// The currents.
        /// </param>
        /// <param name="flag">
        /// The flag.
        /// </param>
        protected void CheckUp(Cameras currents, bool flag)
        {
            if (currents != null)
            {
                this.IsProcessingParentMustChangeIsCheckedStatus = true;
                bool isAllSame = true;

                foreach (Camera Current in currents)
                {
                    if (!Current.Equals(this))
                    {
                        if (Current.IsChecked != this.IsChecked)
                        {
                            isAllSame = false;
                            break;
                        }
                    }
                }

                // 모두 같으면 부모 체크 상태 변경
                if (isAllSame)
                {
                    if (currents.Parent != null)
                    {
                        currents.Parent.raiseCamerasChecked = false;
                        currents.Parent.IsChecked = flag;
                        currents.Parent.raiseCamerasChecked = true;

                        // 재귀적으로 하지 않아도 됨 (INotifyPropertyChanged가 알아서 함)
                        // currents.Parent.CheckUp(currents.Parent.Parent, flag);
                    }
                }

                this.IsProcessingParentMustChangeIsCheckedStatus = false;
            }
        }
    }
}