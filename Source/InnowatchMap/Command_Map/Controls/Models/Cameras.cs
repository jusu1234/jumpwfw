// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Cameras.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   카메라 컬렉션.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.Console.Models
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.IO;
    using System.Linq;
    using System.Windows.Media;
    using System.Xml;

    using Innotive.InnoWatch.Commons.Collections.ObjectModel;
    using Innotive.InnoWatch.Commons.Utils;

    /// <summary>
    /// 카메라 컬렉션.
    /// </summary>
    public class Cameras : DispatchedObservableCollection<Camera>
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="Cameras"/> class. 
        /// 클래스 생성자.
        /// </summary>
        public Cameras()
        {
            // 속성 초기화
            this.Parent = null;
        }

        #endregion

        #region Events

        /// <summary>
        /// 카메라 체크 설정 이벤트.
        /// </summary>
        public event EventHandler<CameraCheckingEventArgs> eCameraChecked = null;

        /// <summary>
        /// 데이터 읽기 완료 이벤트.
        /// </summary>
        public event EventHandler eLoadingComplete = null;

        /// <summary>
        /// 데이터 저장 완료 이벤트.
        /// </summary>
        public event EventHandler eSavingComplete = null;

        #endregion

        #region Properties

        /// <summary>
        /// 데이터 로드 완료 여부.
        /// </summary>
        public bool DataLoadComplete { get; set; }

        /// <summary>
        /// 부모 참조.
        /// </summary>
        public Camera Parent { get; set; }

        #endregion

        #region Public Methods

        /// <summary>
        /// 카메라 선택 초기화.
        /// </summary>
        /// <param name="cameras">
        /// 기준이 되는 카메라 컬렉션.
        /// </param>
        public static void ClearSelectForAll(Cameras cameras)
        {
            if (cameras != null)
            {
                foreach (var camera in cameras)
                {
                    camera.IsSelectedByGridControl = false;
                    ClearSelectForAll(camera.Items);
                }
            }
        }

        /// <summary>
        /// 카메라 가져오기.
        /// </summary>
        /// <param name="cameras">
        /// 기준 카메라 컬렉션.
        /// </param>
        /// <param name="id">
        /// 카메라 Id.
        /// </param>
        /// <returns>
        /// 해당 카메라, 못찾으면 null 반환.
        /// </returns>
        public static Camera GetByCameraId(Cameras cameras, string id)
        {
            Camera foundCamera = null;

            if (cameras != null)
            {
                foreach (var camera in cameras)
                {
                    if (camera.IsFolder)
                    {
                        foundCamera = GetByCameraId(camera.Items, id);

                        if (foundCamera != null)
                        {
                            return foundCamera;
                        }
                    }
                    else
                    {
                        if (string.Compare(camera.Id, id, true) == 0)
                        {
                            return camera;
                        }
                    }
                }
            }

            return foundCamera;
        }

        /// <summary>
        /// 카메라 갯수 가져오기.
        /// </summary>
        /// <param name="cameras">
        /// 기준 컬렉션.
        /// </param>
        /// <param name="startCount">
        /// 카운트 시작.
        /// </param>
        /// <returns>
        /// The get camera count.
        /// </returns>
        public static int GetCameraCount(Cameras cameras, int startCount)
        {
            var count = startCount;

            if (cameras != null)
            {
                count += cameras.Traverse(c => c.Items).Count();
            }

            return count;
        }

        /// <summary>
        /// 카메라 ID로 카메라 이름 가져오기.
        /// </summary>
        /// <param name="cameras">
        /// 기준이 되는 카메라 컬렉션.
        /// </param>
        /// <param name="id">
        /// 카메라 ID.
        /// </param>
        /// <returns>
        /// 카메라 이름, 찾기 실패하면 ID 반환.
        /// </returns>
        public static string GetCameraNameById(Cameras cameras, string id)
        {
            if (cameras != null)
            {
                foreach (var camera in cameras)
                {
                    if (camera.Id == id && !camera.IsFolder)
                    {
                        return camera.Name;
                    }

                    var temp = GetCameraNameById(camera.Items, id);

                    if (temp != id)
                    {
                        return temp;
                    }
                }
            }

            return id;
        }

        /// <summary>
        /// 카메라 No로 카메라 PTZ 여부 가져오기.
        /// </summary>
        /// <param name="cameras">
        /// 기준이 되는 카메라 컬렉션.
        /// </param>
        /// <param name="id">
        /// 카메라 Id.
        /// </param>
        /// <returns>
        /// PTZ 여부 / 찾지 못하면 False  반환.
        /// </returns>
        public static bool GetCameraPTZById(Cameras cameras, string id)
        {
            if (cameras != null)
            {
                foreach (var camera in cameras)
                {
                    if (camera.Id == id && !camera.IsFolder)
                    {
                        return camera.PTZEnabled;
                    }

                    var temp = GetCameraPTZById(camera.Items, id);

                    if (temp)
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        /// <summary>
        /// 하위의 모든 카메라 목록 가져오기.
        /// </summary>
        /// <param name="camera">
        /// 기준 항목.
        /// </param>
        /// <param name="listCameras">
        /// 기존 카메라 목록.
        /// </param>
        /// <returns>
        /// 카메라 목록.
        /// </returns>
        public static List<Camera> GetCameras(Camera camera, List<Camera> listCameras)
        {
            if (listCameras == null)
            {
                listCameras = new List<Camera>();
            }

            if (camera.IsFolder)
            {
                foreach (var c1 in camera.Items)
                {
                    var temp = GetCameras(c1, null);

                    foreach (var c2 in temp)
                    {
                        listCameras.Add(c2);
                    }
                }
            }
            else
            {
                listCameras.Add(camera);
            }

            return listCameras;
        }

        /// <summary>
        /// 하위의 모든 카메라 목록 가져오기.
        /// </summary>
        /// <param name="cameras">
        /// 기준 항목.
        /// </param>
        /// <returns>
        /// 카메라 목록.
        /// </returns>
        public static List<Camera> GetCameras(Cameras cameras)
        {
            var resultCameraList = new List<Camera>();
            foreach (var camera in cameras)
            {
                Cameras.GetCameras(camera, resultCameraList);
            }

            return resultCameraList;
        }

        /// <summary>
        /// 카메라가 체크되어 있는지 여부 확인.
        /// </summary>
        /// <param name="cameras">
        /// 기준 컬렉션.
        /// </param>
        /// <param name="id">
        /// 확인 카메라 ID.
        /// </param>
        /// <returns>
        /// 체크 여부.
        /// </returns>
        public static bool IsCameraChecked(Cameras cameras, string id)
        {
            var flag = false;

            if (cameras != null)
            {
                var queryresult =
                    cameras.Traverse(c => c.Items).Where(c => string.Compare(c.Id, id, true) == 0);

                if (queryresult.Count() > 0)
                {
                    flag = queryresult.First().IsChecked;
                }
            }

            return flag;
        }

        /// <summary>
        /// Contained check.
        /// </summary>
        /// <param name="camera">
        /// The camera.
        /// </param>
        /// <returns>
        /// check result.
        /// </returns>
        public bool CheckContainedItem(Camera camera)
        {
            var queryresult = this.Traverse(c => c.Items).Where(c => c.Equals(camera));
            return queryresult.Count() > 0;
        }

        /// <summary>
        /// 카메라 이름 파싱.
        /// </summary>
        /// <param name="cameras">
        /// 기준이되는 카메라 켈렉션.
        /// </param>
        /// <param name="xml">
        /// XML.
        /// </param>
        /// <returns>
        /// 카메라 이름 목록.
        /// </returns>
        public static List<string> ParseCameraName(Cameras cameras, string xml)
        {
            // 체크 되어야 할 카메라들
            var cameraNames = new List<string>();

            if (xml != string.Empty)
            {
                var stringReader = new StringReader(xml);
                var xmlTextReader = new XmlTextReader(stringReader);

                while (xmlTextReader.Read())
                {
                    if (xmlTextReader.NodeType == XmlNodeType.Element)
                    {
                        if (string.Compare(xmlTextReader.Name, "camera", true) == 0)
                        {
                            if (xmlTextReader.AttributeCount > 0)
                            {
                                var id = string.Empty;

                                for (var i = 0; i < xmlTextReader.AttributeCount; i++)
                                {
                                    xmlTextReader.MoveToAttribute(i);
                                    if (string.Compare(xmlTextReader.Name, "id", true) == 0)
                                    {
                                        id = xmlTextReader.Value;
                                    }
                                }

                                var name = GetCameraNameById(cameras, id);
                                if (!cameraNames.Contains(name))
                                {
                                    cameraNames.Add(name);
                                }
                            }
                        }
                    }
                }
            }

            return cameraNames;
        }

        /// <summary>
        /// 카메라 ID로 선택 하기.
        /// </summary>
        /// <param name="cameras">
        /// 기준이 되는 카메라 컬렉션.
        /// </param>
        /// <param name="id">
        /// 카메라 ID.
        /// </param>
        /// <param name="color">
        /// 카메라 선택 색상.
        /// </param>
        public static void SelectByCameraId(Cameras cameras, string id, Brush color)
        {
            if (cameras != null)
            {
                foreach (var camera in cameras)
                {
                    if (camera.Id == id && !camera.IsFolder)
                    {
                        // 색상 선택을 꼭 먼저해야함.
                        camera.ColorSelectedByGridControl = color;
                        camera.IsSelectedByGridControl = true;
                    }

                    SelectByCameraId(camera.Items, id, color);
                }
            }
        }

        /// <summary>
        /// 카메라 ID로 체크 하기 (이벤트 사용 여부).
        /// </summary>
        /// <param name="cameras">
        /// 기준이 되는 카메라 컬렉션.
        /// </param>
        /// <param name="id">
        /// 카메라 Id.
        /// </param>
        /// <param name="suppressEvent">
        /// 이벤트 사용 여부.
        /// </param>
        public static void SetCheckByCameraId(Cameras cameras, string id, bool suppressEvent)
        {
            if (cameras != null)
            {
                foreach (var camera in cameras)
                {
                    if (!camera.IsFolder)
                    {
                        if (camera.Id == id)
                        {
                            if (suppressEvent)
                            {
                                camera.DisableCamerasCheckedEvent();
                            }

                            camera.IsChecked = true;
                            if (suppressEvent)
                            {
                                camera.EnableCamerasCheckedEvent();
                            }

                            break;
                        }
                    }
                    else
                    {
                        SetCheckByCameraId(camera.Items, id, suppressEvent);
                    }
                }
            }
        }

        /// <summary>
        /// 카메라 ID로 체크 하기.
        /// </summary>
        /// <param name="cameras">
        /// 기준이 되는 카메라 컬렉션.
        /// </param>
        /// <param name="Id">
        /// 카메라 Id.
        /// </param>
        public static void SetCheckByCameraId(Cameras cameras, string id)
        {
            if (cameras != null)
            {
                foreach (var camera in cameras)
                {
                    if (!camera.IsFolder)
                    {
                        if (camera.Id == id)
                        {
                            camera.IsChecked = true;
                            break;
                        }
                    }
                    else
                    {
                        SetCheckByCameraId(camera.Items, id);
                    }
                }
            }
        }

        /// <summary>
        /// 체크 가능 여부 변경하기.
        /// </summary>
        /// <param name="cameras">
        /// The cameras.
        /// </param>
        /// <param name="flag">
        /// The flag.
        /// </param>
        public static void SetCheckingEnabledForAll(Cameras cameras, bool flag)
        {
            if (cameras != null)
            {
                foreach (var camera in cameras)
                {
                    camera.IsCheckingEnabled = flag;

                    SetCheckingEnabledForAll(camera.Items, flag);
                }
            }
        }

        /// <summary>
        /// 모든 카메라 선택 해제.
        /// </summary>
        /// <param name="cameras">
        /// The cameras.
        /// </param>
        /// <param name="useEvent">
        /// The use Event.
        /// </param>
        public static void UncheckAll(Cameras cameras, bool useEvent)
        {
            if (cameras != null)
            {
                foreach (var camera in cameras)
                {
                    if (camera.IsChecked)
                    {
                        var old = camera.GetCamerasCheckedEvent();
                        camera.SetCamerasCheckedEvent(useEvent);
                        camera.IsChecked = false;
                        camera.SetCamerasCheckedEvent(old);
                    }
                    else
                    {
                        // 직접 하위 아이템을 삭제하는 것 보다
                        // 하나의 상위 아이템을 토글하는것이 빠르다.
                        var old = camera.GetCamerasCheckedEvent();
                        camera.SetCamerasCheckedEvent(useEvent);
                        camera.IsChecked = true;
                        camera.IsChecked = false;
                        camera.SetCamerasCheckedEvent(old);

                        // 상위 아이템을 삭제했기 때문에 하위 아이템은 할 팔요 없음.
                        // Cameras.UncheckAll(camera.Items, useEvent);
                    }
                }
            }
        }

        /// <summary>
        /// 체크 여부 이벤트 버블링.
        /// </summary>
        /// <param name="current">
        /// 대상 카메라.
        /// </param>
        /// <param name="isChecked">
        /// 체크 여부.
        /// </param>
        public void CheckingEventBubbling(Camera current, bool isChecked)
        {
            // 현재 컬렉션에 이벤트 등록이 되어 있으면 호출
            if (this.eCameraChecked != null)
            {
                this.eCameraChecked(current, new CameraCheckingEventArgs(current, isChecked));
            }

            // 부모에 이벤트 호출
            if (this.Parent != null)
            {
                this.Parent.CheckingEventBubbling(current, isChecked);
            }
        }

        /// <summary>
        /// 로드 완료 이벤트 호출.
        /// </summary>
        public void RaiseLoadCompletedEvent()
        {
            // 읽기 완료 이벤트 호출
            if (this.eLoadingComplete != null)
            {
                this.eLoadingComplete(this, new EventArgs());
            }
        }

        /// <summary>
        /// 저장 완료 이벤트 호출.
        /// </summary>
        public void RaiseSaveCompletedEvent()
        {
            // 저장 완료 이벤트 호출
            if (this.eSavingComplete != null)
            {
                this.eSavingComplete(this, new EventArgs());
            }
        }

        /// <summary>
        /// 상위 폴더 찾기.
        /// </summary>
        /// <param name="camera">
        /// The camera.
        /// </param>
        /// <returns>
        /// The find upper folder.
        /// </returns>
        public static Camera FindUpperFolder(Camera camera)
        {
            if (camera != null)
            {
                if (camera.IsFolder)
                {
                    return camera;
                }

                if (camera.Parent != null)
                {
                    if (camera.Parent.Parent != null)
                    {
                        return FindUpperFolder(camera.Parent.Parent);
                    }
                }
            }

            return null;
        }

        #endregion

        #region Methods

        /// <summary>
        /// The on collection changed.
        /// </summary>
        /// <param name="e">
        /// The e.
        /// </param>
        protected override void OnCollectionChanged(NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems != null)
            {
                foreach (Camera current in e.NewItems)
                {
                    if (current != null)
                    {
                        current.Parent = this;
                    }
                    
                }
            }

            //var orderCounter = 0;
            //foreach (var item in this.Items)
            //{
            //    item.ItemOrder = orderCounter++;
            //}

            // 부모를 폴더로 설정 여부 계산 및 할당
            //if (this.Parent != null)
            //{
            //    this.Parent.IsFolder = this.Items.Count > 0;
            //}

            base.OnCollectionChanged(e);
        }

        #endregion
    }
}