﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ControlNode.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Defines the ControlNode type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using Innotive.InnoWatch.Console.Configs.Control.WindowState;

namespace Innotive.InnoWatch.Console.Configs.Control
{
    using Innotive.InnoWatch.Console.Configs.Control.RDS;
    using Innotive.InnoWatch.Console.Configs.Control.PTZ;

    /// <summary>
    /// Command > RDS >> Operating.
    /// </summary>
    public class CommandControlNode
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CommandControlNode"/> class.
        /// </summary>
        public CommandControlNode()
        {
            this.RDS = new CommandRDSNode();
            this.WindowState = new CommandWindowStateNode();
            this.PTZ = new CommandPTZNode();
        }

        /// <summary>
        /// Gets or sets RDS.
        /// </summary>
        public CommandRDSNode RDS { get; set; }

        /// <summary>
        /// Gets or sets WindowState.
        /// </summary>
        public CommandWindowStateNode WindowState { get; set; }

        public CommandPTZNode PTZ { get; set; }
    }
}
