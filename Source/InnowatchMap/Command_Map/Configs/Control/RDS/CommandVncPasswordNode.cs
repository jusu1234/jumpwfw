﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CommandVncPasswordNode.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   VNC의 Password를 설정한다.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.Console.Configs.Control.RDS
{
    using System.Xml.Serialization;

    /// <summary>
    /// VNC의 Password를 설정한다.
    /// 기본값: 1111.
    /// </summary>
    public class CommandVncPasswordNode : Innotive.InnoWatch.Commons.Configs.AttributeTypes.BaseNode
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CommandVncPasswordNode"/> class.
        /// </summary>
        public CommandVncPasswordNode()
        {
            this.Value = "1111";
            this.Comment = "VNC 접속에 사용할 암호를 설정합니다.";
        }

        /// <summary>
        /// Gets or sets Value.
        /// </summary>
        [XmlAttribute]
        public string Value { get; set; }
    }
}
