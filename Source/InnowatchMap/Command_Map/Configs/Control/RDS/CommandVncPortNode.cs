﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CommandVncPortNode.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Defines the CommandVncPortNode type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.Console.Configs.Control.RDS
{
    using System.Xml.Serialization;

    /// <summary>
    /// VncPort 기본값: 5100.
    /// </summary>
    public class CommandVncPortNode : Innotive.InnoWatch.Commons.Configs.AttributeTypes.BaseNode
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CommandVncPortNode"/> class.
        /// </summary>
        public CommandVncPortNode()
        {
            this.Value = 5100;
            this.Comment = "VNC를 사용할 포트를 설정합니다. (기본값: 5100)";
            this.Type = "port";
        }

        /// <summary>
        /// Gets or sets Value.
        /// </summary>
        [XmlAttribute]
        public int Value { get; set; }
    }
}
