﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="RdsNode.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Defines the RdsNode type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.Console.Configs.Control.RDS
{
    /// <summary>
    /// RDS 제어 사용 유무를 결정한다.
    /// </summary>
    public class CommandRDSNode
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CommandRDSNode"/> class.
        /// </summary>
        public CommandRDSNode()
        {
            this.Operating = new CommandOperatingNode(false);
            this.InputProtocolType = new CommandInputProtocolTypeNode();
            this.VncPassword = new CommandVncPasswordNode();
            this.VncPort = new CommandVncPortNode();
        }

        /// <summary>
        /// Gets or sets Operating.
        /// </summary>
        public CommandOperatingNode Operating { get; set; }

        /// <summary>
        /// Gets or sets InputProtocolType.
        /// </summary>
        public CommandInputProtocolTypeNode InputProtocolType { get; set; }

        /// <summary>
        /// Gets or sets VncPassword.
        /// </summary>
        public CommandVncPasswordNode VncPassword { get; set; }

        /// <summary>
        /// Gets or sets VncPort.
        /// </summary>
        public CommandVncPortNode VncPort { get; set; }
    }
}
