﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="OperateNode.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Defines the OperateNode type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.Console.Configs.Control.RDS
{
    using Commons.Configs;

    /// <summary>
    /// RDS 제어기능을 사용하는지 설정한다.
    /// </summary>
    public class CommandOperatingNode : ConvertBoolean
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CommandOperatingNode"/> class.
        /// </summary>
        public CommandOperatingNode()
        { 
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CommandOperatingNode"/> class.
        /// </summary>
        /// <param name="defaultValue">
        /// The default Value.
        /// </param>
        public CommandOperatingNode(bool defaultValue) : base(defaultValue)
        {
            this.Comment = "RDS 제어 사용 여부를 설정합니다. (true/false)";
        }
    }
}
