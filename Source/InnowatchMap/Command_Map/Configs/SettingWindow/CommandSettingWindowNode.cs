﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SettingWindowNode.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Defines the SettingWindowNode type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.Console.Configs.SettingWindow
{
    using Innotive.InnoWatch.Console.Configs.SettingWindow.StagePosition;

    /// <summary>
    /// iCommand에서 사용되는 설정윈도우 속성을 설정한다.
    /// </summary>
    public class CommandSettingWindowNode
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CommandSettingWindowNode"/> class.
        /// </summary>
        public CommandSettingWindowNode()
        {
            this.StagePosition = new CommandStagePositionNode();
        }

        /// <summary>
        /// Gets or sets StagePosition.
        /// 스테이지의 크기와 좌표를 자동으로 설정할 것인지 정한다.
        /// </summary>
        public CommandStagePositionNode StagePosition { get; set; }
    }
}
