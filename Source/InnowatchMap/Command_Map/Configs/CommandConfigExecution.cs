﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CommandConfigExecution.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Config 파일을 생성하고 갱신하고 읽어들이는 객체
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using Innotive.InnoWatch.Commons.BaseControls;

namespace Innotive.InnoWatch.Console.Configs
{
    using System;
    using System.Diagnostics;
    using System.IO;
    using System.Xml.Serialization;
    using Commons.Configs;
    using Commons.Utils;

    using Innotive.InnoWatch.CommonUtils.Log;

    /// <summary>
    /// Config 파일을 생성하고 갱신하고 읽어들이는 객체.
    /// </summary>
    public class CommandConfigExecution : ConfigExecution
    {
        private CommandConfigExecution()
        {
            this.DefaultConfig = new CommandConfig();
            this.Config = new CommandConfig();
        }

        /// <summary>
        /// Gets or sets CommandConfig.
        /// </summary>
        private CommandConfig DefaultConfig { get; set; }

        /// <summary>
        /// Gets or sets CommandConfig.
        /// </summary>
        public CommandConfig Config { get; set; }

        /// <summary>
        /// For Singleton.
        /// </summary>
        /// <returns>
        /// Return Nested.Instance.
        /// </returns>
        public static CommandConfigExecution GetInstance()
        {
            return Nested<CommandConfigExecution>.Instance;
        }

        /// <summary>
        /// For Singleton.
        /// </summary>
        public static void Initialize()
        {
            //Nested.Initialize();
        }

        /// <summary>
        /// Merge Default.
        /// </summary>
        public void MergeDefault()
        {
            this.Config.Merge(this.DefaultConfig);
        }

        /// <summary>
        /// 기본 속성 파일을 읽어 들이고 갱신한다.
        /// </summary>
        /// <returns></returns>
        public bool DoDefault()
        {
            // 파일이 존재하고
            if (File.Exists(DefaultConfigFileName))
            {
                // 잘 Decrypt 하면
                var tempConfig = ReadWithDecrypt(DefaultConfigFileName);
                if (tempConfig != null)
                {
                    this.DefaultConfig = tempConfig;

                    return true;
                }
            }

            return false;
        }
        
        /// <summary>
        /// 속성 파일을 읽어 들이고 갱신한다.
        /// </summary>
        /// <returns>
        /// Return bool.
        /// </returns>
        public bool Do()
        {
            return this.Do(ConfigFileName);
        }

        /// <summary>
        /// Do()를 위한 테스트 함수라고 생각해도 된다. 일반적으로 이 함수는 사용하지 않는다.
        /// </summary>
        /// <param name="fileName">
        /// The file name.
        /// </param>
        /// <returns>
        /// Return bool.
        /// </returns>
        public bool Do(string fileName)
        {
            App.Log.Info(string.Format("[Initialize Config] Try to load config file : {0}", fileName));

            // 파일이 존재하지 않으면 기본값으로 파일을 생성한다.
            if (!File.Exists(fileName))
            {
                App.Log.Info(string.Format("[Initialize Config] Not exist config file : {0}. Write to empty config file.", fileName));

                if (!Write(fileName, this.Config))
                {
                    App.Log.Info(string.Format("[Initialize Config] Config file write failed. : {0}", fileName));
                    return false;
                }

                return true;
            }

            // 파일이 존재하는 경우
            var result = false;
            var tempConfig = Read(fileName);
            if (tempConfig != null)
            {
                this.Config = tempConfig;
                result = true;
            }
            else
            {
                App.Log.Info(string.Format("[Initialize Config] Config file read failed : {0}", fileName));
            }

            App.Log.Info(string.Format("[Initialize Config] Rewrite read config file : {0}", fileName));
            if (!Write(fileName, this.Config))
            {
                App.Log.Info(string.Format("[Initialize Config] Config file write failed after file read : {0}", fileName));
                result = false;
            }

            return result;
        }

        /// <summary>
        /// 해당 파일을 읽어 들여서 CommandConfig 객체를 반환한다.
        /// </summary>
        /// <param name="filename">
        /// The filename.
        /// </param>
        /// <returns>
        /// CommandConfig를 반환한다.
        /// </returns>
        private static CommandConfig Read(string filename)
        {
            if (string.IsNullOrEmpty(filename))
            {
                Debug.Assert(!string.IsNullOrEmpty(filename), "CommandConfig의 파일명에 빈 문자열이 입력되었습니다.");
                return null;
            }

            CommandConfig commandConfig;

            var serializer = new XmlSerializer(typeof(CommandConfig));

            serializer.UnknownNode += SerializerUnknownNode;
            serializer.UnknownAttribute += SerializerUnknownAttribute;

            try
            {
                using (var fs = new FileStream(filename, FileMode.Open))
                {
                    commandConfig = (CommandConfig)serializer.Deserialize(fs);
                    fs.Close();
                }
            }
            catch (Exception ex)
            {
                InnotiveDebug.Log.Error("[Initialize Config] CommandConfig Read Error : " + ex.Message);
                return null;
            }

            return commandConfig;
        }

        /// <summary>
        /// 해당 파일을 읽어 들여서 CommandConfig 객체를 반환한다.
        /// </summary>
        /// <param name="filename">
        /// The filename.
        /// </param>
        /// <returns>
        /// CommandConfig를 반환한다.
        /// </returns>
        public static CommandConfig ReadWithDecrypt(string filename)
        {
            if (string.IsNullOrEmpty(filename))
            {
                Debug.Assert(!string.IsNullOrEmpty(filename), "CommandConfig의 파일명에 빈 문자열이 입력되었습니다.");
                return null;
            }
            
            CommandConfig commandConfig;

            var serializer = new XmlSerializer(typeof(CommandConfig));

            serializer.UnknownNode += SerializerUnknownNode;
            serializer.UnknownAttribute += SerializerUnknownAttribute;

            try
            {
                string cipher = File.ReadAllText(filename, System.Text.Encoding.UTF8);
                string plain = InnoStringUtil.DecryptString(cipher, DefaultConfigCipherPhrase);

                var stringReader = new StringReader(plain);
                commandConfig = (CommandConfig)serializer.Deserialize(stringReader);
                stringReader.Close();
            }
            catch (Exception ex)
            {
                InnotiveDebug.Log.Error("[Initialize Config] CommandConfig ReadWithDecrypt Error : " + ex.Message);
                return null;
            }

            return commandConfig;
        }

        /// <summary>
        /// 문자열을 CommandConfig으로 변환한다.
        /// </summary>
        /// <param name="config"></param>
        /// <returns></returns>
        public CommandConfig ReadFromString(string config)
        {
            CommandConfig displayConfig;

            var serializer = new XmlSerializer(typeof(CommandConfig));

            serializer.UnknownNode += SerializerUnknownNode;
            serializer.UnknownAttribute += SerializerUnknownAttribute;

            try
            {
                var stringReader = new StringReader(config);
                displayConfig = (CommandConfig)serializer.Deserialize(stringReader);
                stringReader.Close();
            }
            catch (Exception ex)
            {
                InnotiveDebug.Log.Error("[Initialize Config] CommandConfig ReadFromString Error : " + ex.Message);
                return null;
            }

            return displayConfig;
        }

        /// <summary>
        /// CommandConfig 객체를 XML 형식의 파일로 변환한다.
        /// </summary>
        /// <param name="filename">
        /// The filename.
        /// </param>
        /// <param name="config">
        /// The config.
        /// </param>
        /// <returns>
        /// 성공하면 True를 반환한다.
        /// </returns>
        private static bool Write(string filename, CommandConfig config)
        {
            if (string.IsNullOrEmpty(filename))
            {
                Debug.Assert(!string.IsNullOrEmpty(filename), "CommandConfig의 파일명에 빈 문자열이 입력되었습니다.");
                return false;
            }

            try
            {
                //var serializer = new XmlSerializer(typeof(CommandConfig));
                //TextWriter writer = new StreamWriter(filename, false, System.Text.Encoding.UTF8);
                //serializer.Serialize(writer, config);
                //writer.Close();

                var serializer = new XmlSerializer(typeof(CommandConfig));
                var stringWriter = new StringWriter();
                serializer.Serialize(stringWriter, config);
                var result = stringWriter.ToString();
                result = result.Replace("utf-16", "utf-8");
                stringWriter.Close();

                string oldConfig = string.Empty;
                if (File.Exists(filename))
                {
                    oldConfig = File.ReadAllText(filename);
                }

                if (string.Compare(oldConfig, result, false) != 0)
                {
                    File.WriteAllText(filename, result, System.Text.Encoding.UTF8);
                }
            }
            catch (Exception ex)
            {
                InnotiveDebug.Log.Error("[Initialize Config] CommandConfig Write Error : " + ex.Message);
                return false;
            }

            return true;
        }

        /// <summary>
        /// CommandConfig 객체를 XML 형식의 파일로 변환한다.
        /// </summary>
        /// <param name="filename">
        /// The filename.
        /// </param>
        /// <param name="config">
        /// The config.
        /// </param>
        /// <returns>
        /// 성공하면 True를 반환한다.
        /// </returns>
        public static bool WriteWithEncrypt(string filename, CommandConfig config)
        {
            if (string.IsNullOrEmpty(filename))
            {
                Debug.Assert(!string.IsNullOrEmpty(filename), "CommandConfig의 파일명에 빈 문자열이 입력되었습니다.");
                return false;
            }

            try
            {
                var serializer = new XmlSerializer(typeof(CommandConfig));
                var stringWriter = new StringWriter();
                serializer.Serialize(stringWriter, config);
                var result = stringWriter.ToString();
                stringWriter.Close();
                
                result = InnoStringUtil.EncryptString(result, DefaultConfigCipherPhrase);

                File.WriteAllText(filename, result, System.Text.Encoding.UTF8);
            }
            catch (Exception ex)
            {
                InnotiveDebug.Log.Error("[Initialize Config] CommandConfig WriteWithEncrypt Error : " + ex.Message);
                return false;
            }

            return true;
        }

        /// <summary>
        /// 현재 Config을 String 문자열로 반환.
        /// </summary>
        /// <returns></returns>
        public string WriteToString()
        {
            try
            {
                var serializer = new XmlSerializer(typeof(CommandConfig));
                var stringWriter = new StringWriter();
                serializer.Serialize(stringWriter, this.Config);
                var result = stringWriter.ToString();
                stringWriter.Close();

                return result;
            }
            catch (Exception ex)
            {
                InnotiveDebug.Log.Error("[Initialize Config] CommandConfig WriteToString Error : " + ex.Message);                
            }

            return string.Empty;
        }
        
        public bool Write()
        {
            if (this.Config == null)
            {
                InnotiveDebug.Log.Info("[Initialize Config] Read config is empty.");
                return false;
            }

            return Write(ConfigFileName, this.Config);
        }
    }
}
