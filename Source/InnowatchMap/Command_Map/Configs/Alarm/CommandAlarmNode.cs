﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AlarmNode.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Defines the AlarmNode type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.Console.Configs.Alarm
{
    using Innotive.InnoWatch.Console.Configs.Alarm.ActivatedList;
    using Innotive.InnoWatch.Console.Configs.Alarm.Connection;
    using Innotive.InnoWatch.Console.Configs.Alarm.Message;
    using Innotive.InnoWatch.Console.Configs.Alarm.PopupWindow;
    using Innotive.InnoWatch.Console.Configs.Alarm.StatusViewer;

    /// <summary>
    /// Command > Message >> SendConfirmMessageToServer.
    /// </summary>
    public class CommandAlarmNode
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CommandAlarmNode"/> class.
        /// </summary>
        public CommandAlarmNode()
        {
            this.Connection = new CommandConnectionNode();
            this.Message = new CommandMessageNode();
            this.ActivatedList = new CommandActivatedListNode();
            this.StatusViewer = new CommandStatusViewerNode();
            this.AlarmPopupWindow = new PopupWindowNode();
        }

        /// <summary>
        /// Gets or sets Connection.
        /// </summary>
        public CommandConnectionNode Connection { get; set; }

        /// <summary>
        /// Gets or sets Message.
        /// </summary>
        public CommandMessageNode Message { get; set; }

        /// <summary>
        /// Gets or sets ActivatedList.
        /// </summary>
        public CommandActivatedListNode ActivatedList { get; set; }

        /// <summary>
        /// Gets or sets StatusViewer.
        /// </summary>
        public CommandStatusViewerNode StatusViewer { get; set; }

        /// <summary>
        /// Gets or sets AlarmPopupWindow.
        /// </summary>
        public PopupWindowNode AlarmPopupWindow { get; set; }
    }
}