﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CommandStatusViewerNode.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Defines the CommandStatusViewerNode type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.Console.Configs.Alarm.StatusViewer
{
    using Innotive.InnoWatch.Commons.Configs.AttributeTypes;

    /// <summary>
    /// Status Viewer에 대한 속성들을 설정한다.
    /// </summary>
    public class CommandStatusViewerNode
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CommandStatusViewerNode"/> class.
        /// </summary>
        public CommandStatusViewerNode()
        {
            this.Use = new UseNode(false, "우측 상단에 현재 알람 상태를 표시할 지 설정합니다. (true/false)");
        }

        /// <summary>
        /// Gets or sets Use.
        /// Status Viewer를 사용할 것인지를 결정한다.
        /// 기본값: true.
        /// </summary>
        public UseNode Use { get; set; }
    }
}
