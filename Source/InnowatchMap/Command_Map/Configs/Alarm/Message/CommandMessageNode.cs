﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MessageNode.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Defines the MessageNode type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.Console.Configs.Alarm.Message
{
    /// <summary>
    /// Alarm Message에 대한 처리를 결정한다.
    /// </summary>
    public class CommandMessageNode
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CommandMessageNode"/> class.
        /// </summary>
        public CommandMessageNode()
        {
            this.SendConfirmMessageToServer = new CommandSendConfirmMessageToServerNode(true);
        }

        /// <summary>
        /// Gets or sets SendConfirmMessageToServer.
        /// </summary>
        public CommandSendConfirmMessageToServerNode SendConfirmMessageToServer { get; set; }
    }
}