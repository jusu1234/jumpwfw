﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CommandConnectionNode.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Defines the CommandConnectionNode type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.Console.Configs.Alarm.Connection
{
    using Innotive.InnoWatch.Commons.Configs.AttributeTypes;

    /// <summary>
    /// 알람에 대한 속성을 설정한다.
    /// </summary>
    public class CommandConnectionNode
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CommandConnectionNode"/> class.
        /// </summary>
        public CommandConnectionNode()
        {
            this.Use = new UseNode(false, "알람 사용 여부를 설정합니다. (true/false)");
        }
        
        /// <summary>
        /// Gets or sets UseNode.
        /// 알람 기능을 사용할 것인지 설정한다.
        /// 기본값: true.
        /// </summary>
        public UseNode Use { get; set; }
    }
}

