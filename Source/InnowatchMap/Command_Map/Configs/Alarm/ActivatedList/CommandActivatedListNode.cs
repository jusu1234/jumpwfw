﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CommandActivatedListNode.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Defines the CommandActivatedList type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.Console.Configs.Alarm.ActivatedList
{
    /// <summary>
    /// Activated Alarm List에 대한 속성들을 설정한다.
    /// </summary>
    public class CommandActivatedListNode
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CommandActivatedListNode"/> class.
        /// </summary>
        public CommandActivatedListNode()
        {
            this.RowCountNode = new CommandRowCountNode();
        }

        /// <summary>
        /// Gets or sets RowCountNode.
        /// </summary>
        public CommandRowCountNode RowCountNode { get; set; }
    }
}
