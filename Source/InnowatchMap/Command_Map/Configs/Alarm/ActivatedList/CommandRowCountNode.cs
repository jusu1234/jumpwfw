﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CommandRowCountNode.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Activated Alarm List에 표시될  행의 개수를 설정한다.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.Console.Configs.Alarm.ActivatedList
{
    using System.Xml.Serialization;

    /// <summary>
    /// Activated Alarm List에 표시될  행의 개수를 설정한다.
    /// </summary>
    public class CommandRowCountNode : Innotive.InnoWatch.Commons.Configs.AttributeTypes.BaseNode
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CommandRowCountNode"/> class.
        /// </summary>
        public CommandRowCountNode()
        {
            this.Value = 20;
            this.Comment = "알람 목록의 행 개수를 설정합니다.";
        }

        /// <summary>
        /// Gets or sets a value indicating whether Value.
        /// </summary>
        [XmlAttribute]
        public int Value { get; set; }
    }
}
