﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CommandStageModeNode.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Defines the CommandStageNode type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.Console.Configs.StageMode
{
    using CameraList;
    using Layout;
    using LayoutList;
    using Stage;
    using StageCell;
    using Innotive.InnoWatch.Commons.Publics;
    using Innotive.InnoWatch.Console.Configs.StageMode.Map;
    using Innotive.InnoWatch.Console.Configs.StageMode.Image;
    using Innotive.InnoWatch.Console.Configs.StageMode.Keyboard;


    /// <summary>
    /// </summary>
    public class CommandStageModeNode
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CommandStageModeNode"/> class.
        /// </summary>
        public CommandStageModeNode()
        {
            this.CameraList = new CommandCameraListNode();
            this.Layout = new CommandLayoutNode();
            this.LayoutList = new CommandLayoutListNode();
            this.Map = new CommandMapNode();
            this.Image = new CommandImageNode();
            this.Stage = new CommandStageNode();
            this.StageCell = new CommandStageCellNode();
            this.Keyboard = new CommandKeyboardNode();
        }

        /// <summary>
        /// Gets or sets CameraList.
        /// </summary>
        public CommandCameraListNode CameraList { get; set; }

        /// <summary>
        /// Gets or sets Layout.
        /// </summary>
        public CommandLayoutNode Layout { get; set; }

        /// <summary>
        /// Gets or sets LayoutList.
        /// </summary>
        public CommandLayoutListNode LayoutList { get; set; }

        /// <summary>
        /// Gets or sets Map.
        /// </summary>
        public CommandMapNode Map { get; set; }

        /// <summary>
        /// Gets or sets Image.
        /// </summary>
        public CommandImageNode Image { get; set; }

        /// <summary>
        /// Gets or sets Stage.
        /// </summary>
        public CommandStageNode Stage { get; set; }

        /// <summary>
        /// Gets or sets StageCell.
        /// </summary>
        public CommandStageCellNode StageCell { get; set; }

        public CommandKeyboardNode Keyboard { get; set; }
    }
}
