﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CommandLayoutListNode.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Defines the CommandLayoutListNode type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.Console.Configs.StageMode.LayoutList
{
    using Commons.Configs.AttributeTypes;

    /// <summary>
    /// 스테이지 모드에서 레이아웃 목록에 대한 설정.
    /// </summary>
    public class CommandLayoutListNode
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CommandLayoutListNode"/> class.
        /// </summary>
        public CommandLayoutListNode()
        {
            this.Use = new UseNode(true, "레이아웃 목록 탭을 보일 지 여부를 설정합니다. (true/false)");
        }

        /// <summary>
        /// Gets or sets Value.
        /// 스테이지 모드에서 레이아웃 목록을 사용할지 설정한다.
        /// 기본값: true.
        /// </summary>
        public UseNode Use { get; set; }
    }
}
