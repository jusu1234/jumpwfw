﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CommandLoadSameLayoutNode.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Defines the LoadSameLayout type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.Console.Configs.StageMode.Layout
{
    using System.Xml.Serialization;
    using Innotive.InnoWatch.Commons.Configs;

    /// <summary>
    /// 스테이지 모드에 같은 레이아웃을 올릴 수 있도록 설정한다.
    /// </summary>
    public class CommandLoadSameLayoutNode : ConvertBoolean
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CommandLoadSameLayoutNode"/> class.
        /// </summary>
        public CommandLoadSameLayoutNode()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CommandLoadSameLayoutNode"/> class.
        /// </summary>
        /// <param name="defaultValue">
        /// The default Value.
        /// </param>
        public CommandLoadSameLayoutNode(bool defaultValue) : base(defaultValue)
        {
            this.Comment = "모든 Stage를 통틀어 같은 레이아웃을 추가할 수 있을 지 여부를 설정합니다. (true/false)";
        }
    }
}
