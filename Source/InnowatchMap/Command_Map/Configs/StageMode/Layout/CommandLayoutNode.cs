﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LayoutNode.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Defines the LayoutNode type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.Console.Configs.StageMode.Layout
{
    /// <summary>
    /// 스테이지 모드에서 Layout에 관련된 속성들을 설정한다.
    /// </summary>
    public class CommandLayoutNode
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CommandLayoutNode"/> class.
        /// </summary>
        public CommandLayoutNode()
        {
            this.LoadSameLayout = new CommandLoadSameLayoutNode(false);
            this.Limit = new CommandLimitNode();
        }

        /// <summary>
        /// Gets or sets LoadSameLayout.
        /// </summary>
        public CommandLoadSameLayoutNode LoadSameLayout { get; set; }

        /// <summary>
        /// Gets or sets Limit. 
        /// 스테이지 모드에서 올라갈 수 있는 레이아웃의 총 수를 설정한다.
        /// 기본값: 5.
        /// </summary>
        public CommandLimitNode Limit { get; set; }
    }
}
