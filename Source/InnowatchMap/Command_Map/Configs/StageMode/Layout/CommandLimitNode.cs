﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LimitNode.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Defines the LimitNode type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.Console.Configs.StageMode.Layout
{
    using System.Xml.Serialization;

    /// <summary>
    /// 스테이지 모드에서 올라갈 수 있는 레이아웃의 총 수를 설정한다.
    /// </summary>
    public class CommandLimitNode : Innotive.InnoWatch.Commons.Configs.AttributeTypes.BaseNode
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CommandLimitNode"/> class.
        /// </summary>
        public CommandLimitNode()
        {
            this.Value = 5;
            this.Comment = "모든 Stage를 통틀어 레이아웃을 추가할 수 있는 최대 개수를 설정합니다.";
        }

        /// <summary>
        /// Gets or sets Value.
        /// 기본값: 5.
        /// </summary>
        [XmlAttribute]
        public int Value { get; set; }
    }
}
