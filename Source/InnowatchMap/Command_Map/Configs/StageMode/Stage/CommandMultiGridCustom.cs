﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CommandMultiGridCustom.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Defines the CommandMultiGridCustom type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.Console.Configs.StageMode.Stage
{
    using Commons.Configs;

    /// <summary>
    /// MultiGrid Custom 사용 여부를 설정합니다.
    /// </summary>
    public class CommandMultiGridCustom : ConvertBoolean
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CommandMultiGridCustom"/> class.
        /// </summary>
        public CommandMultiGridCustom()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CommandMultiGridCustom"/> class.
        /// </summary>
        /// <param name="defaultValue">
        /// The default value.
        /// </param>
        public CommandMultiGridCustom(bool defaultValue) : base(defaultValue)
        {
            this.Comment = "Multigrid Custome 사용 여부를 설정합니다.(true/false)";
        }
    }
}
