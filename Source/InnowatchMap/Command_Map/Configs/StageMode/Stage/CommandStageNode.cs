﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CommandStageNode.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Defines the CommandStageNode type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.Console.Configs.StageMode.Stage
{
    /// <summary>
    /// 스테이지별 관련 속성들을 설정한다.
    /// </summary>
    public class CommandStageNode
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CommandStageNode"/> class.
        /// </summary>
        public CommandStageNode()
        {
            this.LayoutLimit = new CommandLayoutLimitNode();
            this.PlayOnControlMode = new CommandPlayOnControlMode(false);
            this.MultiGridTemplate = new CommandMultiGridTemplate(true);
            this.MultiGridCustom = new CommandMultiGridCustom(true);
            this.UseSyncToggleButton = new CommandUseSyncToggleButton(false);
        }

        /// <summary>
        /// Gets or sets LayoutLimit.
        /// 스테이지당 레이아웃이 들어갈 수 있는 개수를 설정한다.
        /// 기본값: 3.
        /// </summary>
        public CommandLayoutLimitNode LayoutLimit { get; set; }

        /// <summary>
        /// Gets or sets PlayOnControlMode.
        /// 스테이지 모드에서 제어 상태인 컨트롤만 영상을 재생시킨다.
        /// 기본값은 false.
        /// </summary>
        public CommandPlayOnControlMode PlayOnControlMode { get; set; }

        /// <summary>
        /// Gets or sets MultiGridTemplate.
        /// MultiGrid Template 사용 여부를 설정합니다.
        /// 기본값은 false.
        /// </summary>
        public CommandMultiGridTemplate MultiGridTemplate { get; set; }

        /// <summary>
        /// Gets or sets MultiGRidCustom.
        /// MultiGrid Custom 사용 여부를 설정합니다.
        /// 기본값은 true.
        /// </summary>
        public CommandMultiGridCustom MultiGridCustom { get; set; }

        /// <summary>
        /// Gets or sets UseSyncToggleButton.
        /// Sync Toggle Button 사용 여부를 설정합니다.
        /// 기본값은 false.
        /// </summary>
        public CommandUseSyncToggleButton UseSyncToggleButton { get; set; }
    }
}
