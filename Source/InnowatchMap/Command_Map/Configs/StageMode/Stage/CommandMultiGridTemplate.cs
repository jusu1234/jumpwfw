﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MultiGridTemplate.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Defines the MultiGridTemplate type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.Console.Configs.StageMode.Stage
{
    using Commons.Configs;

    /// <summary>
    /// Multigrid Template 사용 여부를 설정합니다.
    /// </summary>
    public class CommandMultiGridTemplate : ConvertBoolean
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CommandMultiGridTemplate"/> class.
        /// </summary>
        public CommandMultiGridTemplate()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CommandMultiGridTemplate"/> class.
        /// </summary>
        /// <param name="defaultValue">
        /// The default Value.
        /// </param>
        public CommandMultiGridTemplate(bool defaultValue) : base(defaultValue)
        {
            this.Comment = "Multigrid Template 사용 여부를 설정합니다.(true/false)";
        }
    }
}
