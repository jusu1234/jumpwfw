﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CommandAutoClearSelection.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Defines the CommandAutoClearSelection type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.Console.Configs.StageMode.StageCell
{
    using Commons.Configs;
    using System.Xml.Serialization;

    /// <summary>
    /// 셀 분할/병합 작업 후 Section 표시를 해제할지 설정한다.
    /// </summary>
    public class CommandAutoClearSelectionNode : ConvertBoolean
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CommandAutoClearSelectionNode"/> class.
        /// </summary>
        public CommandAutoClearSelectionNode()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CommandAutoClearSelectionNode"/> class.
        /// </summary>
        /// <param name="defaultValue">
        /// The default Value.
        /// </param>
        public CommandAutoClearSelectionNode(bool defaultValue) : base(defaultValue)
        {
            this.Comment = "Stage의 Cell들을 선택하여 병합시 자동으로 선택이 해제할지 여부를 설정합니다. (true/false)";
        }
    }
}