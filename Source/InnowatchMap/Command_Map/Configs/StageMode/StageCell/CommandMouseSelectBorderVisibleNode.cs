﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CommandMouseSelectBorderVisibleNode.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Defines the CommandMouseSelectBorderVisible type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.Console.Configs.StageMode.StageCell
{
    using Commons.Configs;
    using System.Xml.Serialization;

    /// <summary>
    /// </summary>
    public class CommandMouseSelectBorderVisibleNode : ConvertBoolean
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CommandMouseSelectBorderVisibleNode"/> class.
        /// </summary>
        public CommandMouseSelectBorderVisibleNode()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CommandMouseSelectBorderVisibleNode"/> class. 
        /// </summary>
        /// <param name="defaultValue">
        /// The default Value.
        /// </param>
        public CommandMouseSelectBorderVisibleNode(bool defaultValue) : base(defaultValue)
        {
            this.Comment = "Stage의 Cell을 선택했을 때 테두리를 보여줄지 여부를 설정합니다. (true/false)";
        }
    }
}
