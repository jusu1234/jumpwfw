﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CommandMapNode.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Defines the CommandMapNode type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.Console.Configs.StageMode.Map
{
    using Commons.Configs.AttributeTypes;

    /// <summary>
    /// 스테이지 모드에서 맵 사용 설정.
    /// </summary>
    public class CommandMapNode
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CommandMapNode"/> class.
        /// </summary>
        public CommandMapNode()
        {
            this.Use = new UseNode(true, "맵 사용 유무를 설정합니다. (true/false)");
        }

        /// <summary>
        /// Gets or sets Value.
        /// 스테이지 모드에서 레이아웃 목록을 사용할지 설정한다.
        /// 기본값: true.
        /// </summary>
        public UseNode Use { get; set; }
    }
}
