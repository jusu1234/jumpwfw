﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SelectingLimit.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Defines the SelectingLimit type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.Console.Configs.StageMode.CameraList
{
    using System.Xml.Serialization;

    /// <summary>
    /// 리스트 UI에 카메라 리스트에 있는 카메라를 선택할 수 있는 개수를 제한한다.
    /// 설정된 값을 초과하면 선택이 초기화 된다.
    /// </summary>
    public class CommandSelectingLimitNode : Innotive.InnoWatch.Commons.Configs.AttributeTypes.BaseNode
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CommandSelectingLimitNode"/> class.
        /// </summary>
        public CommandSelectingLimitNode()
        {
            this.Value = 99;
            this.Comment = "카메라 목록 탭에서 선택할 수 있는 최대 카메라 개수를 설정합니다.";
        }

        /// <summary>
        /// Gets or sets a value indicating whether Value.
        /// </summary>
        [XmlAttribute]
        public int Value { get; set; }
    }
}
