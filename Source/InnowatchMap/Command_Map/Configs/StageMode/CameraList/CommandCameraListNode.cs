﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CommandCameraListNode.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Defines the CameraListNode type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.Console.Configs.StageMode.CameraList
{
    using Commons.Configs.AttributeTypes;

    /// <summary>
    /// 카메라 목록에 관련된 속성들을 설정한다.
    /// </summary>
    public class CommandCameraListNode
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CommandCameraListNode"/> class.
        /// </summary>
        public CommandCameraListNode()
        {
            this.NameFormat = new CommandNameFormatNode();
            this.SelectingLimit = new CommandSelectingLimitNode();
            this.Use = new UseNode(true, "카메라 목록 탭을 보일 지 여부를 설정합니다. (true/false)");
        }

        /// <summary>
        /// Gets or sets NameFormat.
        /// 카메라 리스트에 표시되는 문자열 포맷을 설정한다.
        /// 기본값: "{NAME} ({ID})".
        /// </summary>
        public CommandNameFormatNode NameFormat { get; set; }

        /// <summary>
        /// Gets or sets SelectingLimit.
        /// 리스트 UI에 카메라 리스트에 있는 카메라를 선택할 수 있는 개수를 제한한다.
        /// 설정된 값을 초과하면 카메라에 대한 선택이 초기화 된다.
        /// 기본값: 99.
        /// </summary>
        public CommandSelectingLimitNode SelectingLimit { get; set; }

        /// <summary>
        /// Gets or sets Visible.
        /// 스테이지 모드에서 카메라 목록을 사용할 것인지 설정한다.
        /// 기본값: true.
        /// </summary>
        public UseNode Use { get; set; }
    }
}
