﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CommandResourcesNode.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Defines the ControlNode type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using Innotive.InnoWatch.Console.Configs.Resources.Login;
using Innotive.InnoWatch.Console.Configs.Resources.Product;

namespace Innotive.InnoWatch.Console.Configs.Resources
{
    public class CommandResourcesNode
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CommandResourcesNode"/> class.
        /// </summary>
        public CommandResourcesNode()
        {
            this.LoginLogo = new CommandLoginLogoNode();
            this.ProductName = new CommandProductNameNode();
        }

        public CommandLoginLogoNode LoginLogo { get; set; }

        public CommandProductNameNode ProductName { get; set; }
    }
}
