﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CommandLoginLogoNode.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.Console.Configs.Resources.Login
{
    using System.Xml.Serialization;

    /// <summary>
    /// 로그인창 로고 이미지 설정.
    /// </summary>
    public class CommandLoginLogoNode : Innotive.InnoWatch.Commons.Configs.AttributeTypes.BaseNode
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CommandLoginLogoNode"/> class.
        /// </summary>
        public CommandLoginLogoNode()
        {
            this.Value = string.Empty;
            this.Comment = "로그인 화면에 표시할 이미지 로고 경로를 설정합니다. (없으면 기본값 사용)";
            this.Type = "string";
        }

        /// <summary>
        /// Gets or sets Value.
        /// </summary>
        [XmlAttribute]
        public string Value { get; set; }
    }
}
