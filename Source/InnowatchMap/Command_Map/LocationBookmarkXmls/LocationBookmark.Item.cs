﻿// -----------------------------------------------------------------------
// <copyright file="LocationBookmarkItem.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace Innotive.InnoWatch.Console.LocationBookmarkXmls
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Xml.Serialization;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    [XmlRoot("Item")]
    public class Item
    {
        [XmlAttribute("Title")]
        public string Title { get; set; }

        [XmlAttribute("Content")]
        public string Content { get; set; }

        [XmlAttribute("Lat")]
        public double Lat { get; set; }

        [XmlAttribute("Lng")]
        public double Lng { get; set; }

        [XmlAttribute("Index")]
        public int Index { get; set; }
    }
}
