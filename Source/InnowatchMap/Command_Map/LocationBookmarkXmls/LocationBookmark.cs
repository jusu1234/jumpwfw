﻿// -----------------------------------------------------------------------
// <copyright file="LocationBookmark.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace Innotive.InnoWatch.Console.LocationBookmarkXmls
{
    using System.Collections.ObjectModel;
    using System.Xml.Serialization;

    /// <summary>
    /// LocationBookmark XML 클래스.
    /// </summary>
    [XmlRoot("LocationBookmark")]
    public class LocationBookmark
    {
        private ObservableCollection<Item> items = new ObservableCollection<Item>(); 

        [XmlElement("Item")]
        public ObservableCollection<Item> Items
        {
            get
            {
                return this.items;
            }
            set
            {
                this.items = value;
            }
        }
    }
}
