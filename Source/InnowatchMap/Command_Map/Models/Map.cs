﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Map.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   맵 클래스.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.Console.Models
{
    using Innotive.InnoWatch.Commons.Models;

    /// <summary>
    /// 뷰 클래스.
    /// </summary>
    public class Map : BaseModel
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="Map"/> class. 
        /// </summary>
        public Map()
        {
        }

        #endregion

        /// <summary>
        /// Gets or sets FocusedCameraId.
        /// </summary>
        public string FocusedCameraId { get; set; }
    }
}