﻿
namespace GMap.NET.MapProviders
{
   using System;
   using System.Collections.Generic;
   using System.Diagnostics;
   using System.Globalization;
   using System.Xml;
   using GMap.NET.Internals;
   using GMap.NET.Projections;

   public abstract class VWorldMapProviderBase : GMapProvider
   {
      public VWorldMapProviderBase()
      {
         MaxZoom = null;
         RefererUrl = "http://map.vworld.kr/";
         Copyright = string.Empty;
      }

      #region GMapProvider Members

      public override Guid Id
      {
         get
         {
            throw new NotImplementedException();
         }
      }

      public override string Name
      {
         get
         {
            throw new NotImplementedException();
         }
      }

      public override PureProjection Projection
      {
         get
         {
            return MercatorProjection.Instance;
         }
      }

      public override GMapProvider[] Overlays
      {
         get
         {
            throw new NotImplementedException();
         }
      }

      public override PureImage GetTileImage(GPoint pos, int zoom)
      {
         throw new NotImplementedException();
      }

      #endregion
   }

   /// <summary>
   /// OpenStreetMap provider
   /// </summary>
   public class VWorldMapProvider : VWorldMapProviderBase
   {
      public static readonly VWorldMapProvider Instance;

      VWorldMapProvider()
      {
      }

      static VWorldMapProvider()
      {
          Instance = new VWorldMapProvider();
      }

      #region GMapProvider Members

      readonly Guid id = new Guid("E716E557-5772-45CD-A18E-1D2E2C3F7E9F");
      public override Guid Id
      {
         get
         {
            return id;
         }
      }

      readonly string name = "VWorld";
      public override string Name
      {
         get
         {
            return name;
         }
      }

      GMapProvider[] overlays;
      public override GMapProvider[] Overlays
      {
         get
         {
            if(overlays == null)
            {
               overlays = new GMapProvider[] { this };
            }
            return overlays;
         }
      }

      public override PureImage GetTileImage(GPoint pos, int zoom)
      {
         string url = MakeTileImageUrl(pos, zoom, string.Empty);
         System.Diagnostics.Debug.WriteLine("VWorld Url : " + url);

         return GetTileImageUsingHttp(url);
      }

      #endregion

      string MakeTileImageUrl(GPoint pos, int zoom, string language)
      {
         return string.Format(UrlFormat, zoom, pos.X, pos.Y);
      }

      static readonly string UrlFormat = "http://xdworld.vworld.kr:8080/base/{0}/{1}/{2}.png";
   }
}
