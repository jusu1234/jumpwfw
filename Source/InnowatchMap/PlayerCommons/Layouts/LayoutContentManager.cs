﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LayoutContentManager.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Defines the LayoutContentManager type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using Innotive.InnoWatch.Commons.BaseControls;

namespace Innotive.InnoWatch.PlayerCommons.Layouts
{
    using System;
    using System.Collections.Generic;
    using System.Windows;
    using System.Windows.Media;
    using System.Windows.Media.Animation;

    using Innotive.InnoWatch.Commons.Bookmarks;
    using Innotive.InnoWatch.Commons.EventArguments;
    using Innotive.InnoWatch.Commons.GuidManagers;
    using Innotive.InnoWatch.CommonUtils.Log;
    using Innotive.InnoWatch.DLLs.LayoutControls;
    using Innotive.InnoWatch.DLLs.MapControls;
    using Innotive.InnoWatch.PlayerCommons.Actions;
    using Innotive.InnoWatch.PlayerCommons.DisplayAreas;

    /// <summary>
    /// Layout Content Manager Class.
    /// </summary>
    /// <remarks>
    /// 내부 컨텐츠 싱크 전용 클래스.
    /// </remarks>
    public class LayoutContentManager
    {
        #region Constructors and Destructors

        public LayoutContentManager(PlayerDisplayAreaManager displayAreaManager)
        {
        }

        #endregion

        #region Private Helper Classes

        private void ActionEffectDoubleEffectCompleted(object sender, EffectCompletedRoutedEventArgs e)
        {
            var actionEffect = sender as PlayerActionEffect;
            if (actionEffect != null)
            {
                actionEffect.eDoubleEffectCompleted -= ActionEffectDoubleEffectCompleted;
            }
        }

        private void ActionEffectSolidColorBrushEffectCompleted(object sender, EffectCompletedRoutedEventArgs e)
        {
            var actionEffect = sender as PlayerActionEffect;
            if (actionEffect != null)
            {
                actionEffect.eSolidColorBrushEffectCompleted -= ActionEffectSolidColorBrushEffectCompleted;
            }
        }

        #endregion // Private Helper Classes

        #region Protected Helper Classes

        /// <summary>
        /// The get owner layout control.
        /// </summary>
        /// <param name="targetElementGuid">
        /// The target element guid.
        /// </param>
        /// <returns>
        /// </returns>
        protected LayoutControl GetOwnerLayoutControl(Guid targetElementGuid)
        {
            var findElement = GuidManager.GetSyncObject(targetElementGuid) as FrameworkElement;
            if (findElement == null)
            {
                InnotiveDebug.Trace(4, "<PlayerCommons> GuidManager에서 element를 찾지 못했습니다");

                return null;
            }

            var ownerLayoutControl = GetOwnerLayoutControl(findElement);
            if (ownerLayoutControl == null)
            {
                InnotiveDebug.Trace(4, "<PlayerCommons> Owner layout control이 존재하지 않습니다");

                return null;
            }

            return ownerLayoutControl;
        }

        // 바로위에 존재하는 Layout을 구함 (iCanvas는 제외)
        // 자신이 LayoutControl인 경우 자신을 return함 !!
        /// <summary>
        /// The get owner layout control.
        /// </summary>
        /// <param name="element">
        /// The element.
        /// </param>
        /// <returns>
        /// </returns>
        protected LayoutControl GetOwnerLayoutControl(FrameworkElement element)
        {
            var paramLayoutControl = element as LayoutControl;
            if (paramLayoutControl != null && paramLayoutControl.LayoutType == LayoutType.LayoutControl)
            {
                return paramLayoutControl;
            }

            if (element.Parent == null)
            {
                return null;
            }

            if (element.Parent is LayoutControl)
            {
                var findLayoutControl = element.Parent as LayoutControl;
                if (findLayoutControl.LayoutType == LayoutType.LayoutControl)
                {
                    return element.Parent as LayoutControl;
                }
            }

            return GetOwnerLayoutControl(element.Parent as FrameworkElement);
        }

        #endregion // Protected Helper Classes

        #region Methods

        /// <summary>
        /// The bring to front.
        /// </summary>
        /// <param name="targetElementGuid">
        /// The target element guid.
        /// </param>
        /// <param name="effect">
        /// The effect.
        /// </param>
        /// <param name="startTime">
        /// The start time.
        /// </param>
        /// <param name="duration">
        /// The duration.
        /// </param>
        public void BringToFront(Guid targetElementGuid, string effect, double startTime, double duration)
        {
            var list = GuidManager.GetSyncObjectList(targetElementGuid);
            if (list == null)
            {
                return;
            }

            foreach (UIElement element in list)
            {
                var layoutControl = ((BaseControl) element).ParentBaseLayoutControl as LayoutControl;
                if (layoutControl == null)
                {
                    continue;
                }

                layoutControl.BringToFront(element as FrameworkElement);
            }
        }

        /// <summary>
        /// The change element property.
        /// </summary>
        /// <param name="elementGuid">
        /// The element guid.
        /// </param>
        /// <param name="property">
        /// The property.
        /// </param>
        /// <param name="propertyValue">
        /// The property value.
        /// </param>
        /// <param name="effect">
        /// The effect.
        /// </param>
        /// <param name="startTime">
        /// The start time.
        /// </param>
        /// <param name="duration">
        /// The duration.
        /// </param>
        public void ChangeElementProperty(
            Guid elementGuid, 
            DependencyProperty property, 
            object propertyValue, 
            string effect, 
            double startTime, 
            double duration)
        {
            var elementList = GuidManager.GetSyncObjectList(elementGuid);

            // Hwlee. 5.25 PlayerLayoutFileData의 HandledLayoutControlLoaded에서 자식 Canvas의 Loaded까지 보장해주지 못해서 밑의 방법으로는 Element를 찾지 못하는 경우가 발생함. 
            // List<FrameworkElement> elementList = MainProcess.Instance.ProjectManager.PlayerLayoutManager.GetElementList(Guid.Empty, elementGuid);
            if (elementList == null || elementList.Count < 1)
            {
                return;
            }

            foreach (UIElement uiElement in elementList)
            {
                var element = uiElement as FrameworkElement;

                // shwlee - duration이 0 일 때 StoryBoard에 넣지 않으면(직접 값을 할당하면) 
                // 화면에 보이지 않는 영역의 Layout이 보이는 영역으로 들어왔을 때 UpdateViewArea 실행 안됨
                // duration 이 0이라도  StoryBoard에 넣어줘야함.
                var actionEffect = new PlayerActionEffect();

                // double animation
                if (property.PropertyType.IsAssignableFrom(typeof(double)))
                {
                    string strValue = propertyValue.ToString();
                    double doubleValue;
                    if (double.TryParse(strValue, out doubleValue))
                    {
                        actionEffect.eDoubleEffectCompleted += ActionEffectDoubleEffectCompleted;
                        actionEffect.BeginDoubleEffect(element, property, doubleValue, 0, duration);
                        continue;
                    }
                }
                    
                    // color animation
                else if (property.PropertyType.IsAssignableFrom(typeof(SolidColorBrush)))
                {
                    var brush = propertyValue as SolidColorBrush;
                    actionEffect.eSolidColorBrushEffectCompleted += ActionEffectSolidColorBrushEffectCompleted;
                    actionEffect.BeginSolidColorBrushEffect(element, property, brush, 0, duration);
                    continue;
                }
                else if (property.PropertyType.IsAssignableFrom(typeof(Color)))
                {
                    continue;
                }
                else
                {
                    // no animation
                    if (element != null)
                    {
                        element.BeginAnimation(property, null);
                        element.SetValue(property, propertyValue);
                    }
                }
            }
        }

        /// <summary>
        /// The go to location.
        /// </summary>
        /// <param name="bookmark">
        /// The bookmark.
        /// </param>
        /// <param name="effect">
        /// The effect.
        /// </param>
        /// <param name="startTime">
        /// The start time.
        /// </param>
        /// <param name="duration">
        /// The duration.
        /// </param>
        /// <param name="keySpline">
        /// The key Spline.
        /// </param>
        public void GoToLocation(
            DataBookmark bookmark, 
            string effect, 
            double startTime, 
            double duration, 
            KeySpline keySpline)
        {
            // Bookmark의 item 각각 (Layout + iCanvas)을 처리함 !!
            foreach (var item in bookmark.BookmarkItems)
            {
                List<LayoutControl> destLayoutControlList =
                    MainProcess.Instance.ProjectManager.PlayerLayoutManager.GetLayoutControlList(item.DestLayoutGUID);
                if (destLayoutControlList == null || destLayoutControlList.Count < 1)
                {
                    continue;
                }

                foreach (LayoutControl destLayoutControl in destLayoutControlList)
                {
                    destLayoutControl.GoToLocationAnimation(item.ZoomRect, startTime, duration, keySpline);
                }
            }
        }

        /// <summary>
        /// The panning start. 자체 Sync를 위한 함수임 !!.
        /// </summary>
        /// <param name="layoutGuid">
        /// The layout guid.
        /// </param>
        public void PanningStart(Guid layoutGuid)
        {
            List<UIElement> list = GuidManager.GetSyncObjectList(layoutGuid);
            if (list == null)
            {
                return;
            }
        }

        /// <summary>
        /// The panning end. 자체 Sync를 위한 함수임 !!.
        /// </summary>
        /// <param name="layoutGuid">
        /// The layout guid.
        /// </param>
        public void PanningEnd(Guid layoutGuid)
        {
            var list = GuidManager.GetSyncObjectList(layoutGuid);
            if (list == null)
            {
                return;
            }
        }

        /// <summary>
        /// The panning end. iCommand에서 Panning이 종료됐음을 알림 !! Sync를 맞춤 !!.
        /// </summary>
        /// <param name="layoutGuid">
        /// The layout guid.
        /// </param>
        /// <param name="rect">
        /// The rect.
        /// </param>
        public void RecvPanningEnd(Guid layoutGuid, Rect rect)
        {
            var list = GuidManager.GetSyncObjectList(layoutGuid);
            if (list == null)
            {
                return;
            }

            foreach (var element in list)
            {
                var layoutControl = element as LayoutControl;
                if (layoutControl == null)
                {
                    continue;
                }

                layoutControl.LayoutActualRect = rect;
            }

            Commons.Utils.ZoomPanSmoothManager.End(this);
        }

        /// <summary>
        /// The setting rect.
        /// </summary>
        /// <param name="layoutSyncGuid">
        /// The layout sync guid.
        /// </param>
        /// <param name="layoutOriginGuid">
        /// The layout origin guid.
        /// </param>
        /// <param name="rect">
        /// The rect.
        /// </param>
        public void SettingRect(Guid layoutSyncGuid, Guid layoutOriginGuid, Rect rect)
        {
            var list = GuidManager.GetSyncObjectList(layoutSyncGuid);
            if (list == null)
            {
                return;
            }

            foreach (UIElement element in list)
            {
                var layoutControl = element as LayoutControl;
                if (layoutControl == null)
                {
                    continue;
                }

                if (layoutControl.OriginGUID != Guid.Empty && layoutOriginGuid != Guid.Empty)
                {
                    if (layoutControl.OriginGUID == layoutOriginGuid)
                    {
                        continue;
                    }
                }

                layoutControl.LayoutActualRect = rect;
            }
        }

        /// <summary>
        /// The zoom end.
        /// </summary>
        /// <param name="layoutGuid">
        /// The layout guid.
        /// </param>
        public void ZoomEnd(Guid layoutGuid)
        {
            var list = GuidManager.GetSyncObjectList(layoutGuid);
            if (list == null)
            {
                return;
            }
        }

        /// <summary>
        /// The zoom in.
        /// </summary>
        /// <param name="layoutSyncGuid">
        /// The layout sync guid.
        /// </param>
        /// <param name="layoutOriginGuid">
        /// The layout origin guid.
        /// </param>
        /// <param name="desireRect">
        /// The desire rect.
        /// </param>
        public void ZoomIn(Guid layoutSyncGuid, Guid layoutOriginGuid, Rect desireRect)
        {
            var list = GuidManager.GetSyncObjectList(layoutSyncGuid);
            if (list == null)
            {
                return;
            }

            foreach (LayoutControl element in list)
            {
                element.ZoomAnimationSync(layoutOriginGuid, desireRect);
            }
        }

        /// <summary>
        /// The zoom in.
        /// </summary>
        /// <param name="layoutSyncGuid">
        /// The layout sync guid.
        /// </param>
        /// <param name="percentage">
        /// The percentage.
        /// </param>
        /// <param name="duration">
        /// The duration.
        /// </param>
        public Rect ZoomIn(Guid layoutSyncGuid, double percentage, double duration)
        {
            var list = GuidManager.GetSyncObjectList(layoutSyncGuid);
            if (list == null)
            {
                return Rect.Empty;
            }

            var desireRect = Rect.Empty;
            foreach (var element in list)
            {
                var layoutControl = element as LayoutControl;
                if (layoutControl == null)
                {
                    continue;
                }

                desireRect = layoutControl.ZoomIn(percentage / 100, duration);
            }

            return desireRect;
        }

        /// <summary>
        /// The zoom out.
        /// </summary>
        /// <param name="layoutSyncGuid">
        /// The layout sync guid.
        /// </param>
        /// <param name="layoutOriginGuid">
        /// The layout origin guid.
        /// </param>
        /// <param name="desireRect">
        /// The desire rect.
        /// </param>
        public void ZoomOut(Guid layoutSyncGuid, Guid layoutOriginGuid, Rect desireRect)
        {
            var list = GuidManager.GetSyncObjectList(layoutSyncGuid);
            if (list == null)
            {
                return;
            }

            foreach (LayoutControl element in list)
            {
                element.ZoomAnimationSync(layoutOriginGuid, desireRect);
            }
        }

        /// <summary>
        /// The zoom out.
        /// </summary>
        /// <param name="layoutSyncGuid">
        /// The layout sync guid.
        /// </param>
        /// <param name="percentage">
        /// The percentage.
        /// </param>
        /// <param name="duration">
        /// The duration.
        /// </param>
        public Rect ZoomOut(Guid layoutSyncGuid, double percentage, double duration)
        {
            var list = GuidManager.GetSyncObjectList(layoutSyncGuid);
            if (list == null)
            {
                return Rect.Empty;
            }

            var desireRect = Rect.Empty;
            foreach (var element in list)
            {
                var layoutControl = element as LayoutControl;
                if (layoutControl == null)
                {
                    continue;
                }

                desireRect = layoutControl.ZoomOut(percentage / 100, duration);
            }

            return desireRect;
        }

        /// <summary>
        /// 해당 Map control 의 줌 레벨을 변경한다.
        /// </summary>
        /// <param name="mapControlSyncGuid">MapControl GUID</param>
        /// <param name="zoomLevel">Zoom Level</param>
        public void MapControlZoomChanged(Guid mapControlSyncGuid, double zoomLevel)
        {
            var list = GuidManager.GetSyncObjectList(mapControlSyncGuid);
            if (list == null)
            {
                return;
            }

            foreach (var element in list)
            {
                var mapControl = element as MapControl;

                if(mapControl != null)
                {
                    mapControl.MapControlZoomChangedSync(zoomLevel);
                }
            }
        }

        /// <summary>
        /// 해당 Map control 의 변경 사항을 적용한다.
        /// </summary>
        /// <param name="mapControlSyncGuid">MapControl GUID</param>
        /// <param name="latitude">위도</param>
        /// <param name="longtitude">경도</param>
        public void MapControlPositionChanged(Guid mapControlSyncGuid, double latitude, double longtitude)
        {
            var list = GuidManager.GetSyncObjectList(mapControlSyncGuid);
            if (list == null)
            {
                return;
            }

            foreach (var element in list)
            {
                var mapControl = element as MapControl;

                if (mapControl != null)
                {
                    mapControl.MapControlPositionChangedSync(latitude, longtitude);
                }
            }
        }

        /// <summary>
        /// Map control 의 해당 카메라를 전체 화면으로 만든다.
        /// </summary>
        /// <param name="mapControlSyncGuid">
        /// MapControl GUID
        /// </param>
        /// <param name="cameraId">
        /// The camera Id.
        /// </param>
        /// <param name="isFull">
        /// The is Full.
        /// </param>
        public void MapControlFullViewCamera(Guid mapControlSyncGuid, string cameraId, bool isFull)
        {
            var list = GuidManager.GetSyncObjectList(mapControlSyncGuid);
            if (list == null)
            {
                return;
            }

            foreach (var element in list)
            {
                var mapControl = element as MapControl;

                if (mapControl != null)
                {
                    mapControl.MapControlFullViewCameraSync(cameraId, isFull);
                }
            }
        }

        /// <summary>
        /// The xaml control storyboard.
        /// </summary>
        /// <param name="targetLayoutGuid">
        /// The target layout guid.
        /// </param>
        /// <param name="targetElementGuid">
        /// The target element guid.
        /// </param>
        /// <param name="targetElementStoryboardName">
        /// The target element storyboard name.
        /// </param>
        public void XamlControlStoryboard(
            Guid targetLayoutGuid, Guid targetElementGuid, string targetElementStoryboardName)
        {
            var list = GuidManager.GetSyncObjectList(targetElementGuid);
            if (list == null)
            {
                return;
            }
        }

        #endregion
    }
}