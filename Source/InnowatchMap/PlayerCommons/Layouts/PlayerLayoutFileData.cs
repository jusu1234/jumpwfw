﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PlayerLayoutFileData.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   player layout file data.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.PlayerCommons.Layouts
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Media;
    using System.Xml;
    using Innotive.InnoWatch.Commons.BaseControls;
    using Innotive.InnoWatch.Commons.Bookmarks;
    using Innotive.InnoWatch.Commons.Configs;
    using Innotive.InnoWatch.Commons.EventArguments;
    using Innotive.InnoWatch.Commons.Files;
    using Innotive.InnoWatch.Commons.Layouts;
    using Innotive.InnoWatch.Commons.Publics;
    using Innotive.InnoWatch.Commons.Utils;
    using Innotive.InnoWatch.Commons.Xamls;
    using Innotive.InnoWatch.CommonUtils.Log;
    using Innotive.InnoWatch.CommonUtils.Models;
    using Innotive.InnoWatch.DLLs.CameraControls;
    using Innotive.InnoWatch.DLLs.LayoutControls;
    using Innotive.InnoWatch.DLLs.MapControls;
    using Innotive.InnoWatch.PlayerCommons.Managers;

    /// <summary>
    /// player layout file data.
    /// </summary>
    public class PlayerLayoutFileData : ILayoutFileData
    {
        /// <summary>
        /// Object에 대한 실시간 Xaml 데이타.
        /// </summary>
        private string _currentXamlString = string.Empty;

        private InnoFilePath fileName;

        private List<BaseLayoutControl> layoutControlList = new List<BaseLayoutControl>();

        /// <summary>
        /// Initializes a new instance of the <see cref="PlayerLayoutFileData"/> class.
        /// </summary>
        /// <param name="layoutFileName">
        /// The layout file name.
        /// </param>
        public PlayerLayoutFileData(InnoFilePath layoutFileName)
        {
            this.Read(layoutFileName);
        }

        /// <summary>
        /// Gets or sets InsideBackground.
        /// </summary>
        public Brush InsideBackground { get; set; }

        /// <summary>
        /// Gets or sets OutsideBackground.
        /// </summary>
        public Brush OutsideBackground { get; set; }

        /// <summary>
        /// Gets or sets layoutControlList.
        /// </summary>
        public List<BaseLayoutControl> LayoutControlList
        {
            get
            {
                return this.layoutControlList;
            }

            set
            {
                this.layoutControlList = value;
            }
        }

        /// <summary>
        /// Gets or sets Name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets LayoutDescription.
        /// </summary>
        public string LayoutDescription { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether LayoutShowToList.
        /// </summary>
        public bool LayoutShowToList { get; set; }

        /// <summary>
        /// Gets or sets FileName.
        /// </summary>
        public InnoFilePath FileName
        {
            get
            {
                return this.fileName;
            }

            set
            {
                this.SetAbsoluteFileName(value);
            }
        }

        private void SetAbsoluteFileName(InnoFilePath fileName)
        {
            if (fileName.IsAbsolutePath)
            {
                this.fileName = fileName;
            }
            else
            {
                fileName.MakeAbsolutePath(fileName);
                this.fileName = fileName;
            }
        }

        /// <summary>
        /// Gets or sets OriginGUID.
        /// </summary>
        public Guid OriginGUID { get; set; }

        /// <summary>
        /// Gets or sets SyncGUID.
        /// </summary>
        public Guid SyncGUID { get; set; }

        /// <summary>
        /// Gets or sets Size.
        /// </summary>
        public Size Size { get; set; }

        /// <summary>
        /// SyncGuid가 동일한 Layout List의 최상위에 있는 레이아웃을 반환합니다.
        /// GetPrimaryLayout: 
        /// </summary>
        /// <returns></returns>
        public BaseLayoutControl GetPrimaryLayout()
        {
            if (this.LayoutControlList.Count > 0)
            {
                return this.LayoutControlList[0];
            }

            return null;
        }

        /// <summary>
        /// LayoutControl에 대한 리스트를 클리어 한다.
        /// </summary>
        public void Clear()
        {
            var tempList = new List<BaseLayoutControl>();
            tempList.AddRange(this.LayoutControlList);
            this.LayoutControlList.Clear();

            foreach (var layoutControl in tempList)
            {
                layoutControl.Dispose();
            }

            tempList.Clear();
        }

        public void GoHome(BaseLayoutControl layoutControl)
        {
            if (layoutControl == null)
            {
                return;
            }

            if ((layoutControl as LayoutControl) == null)
            {
                return;
            }

            // bookmark중 home이 있으면 이동함 !!
            var bookmark = MainProcess.Instance.ProjectManager.BookmarkManager.GetBookmarkHome(layoutControl.SyncGUID);

            if (bookmark != null)
            {
                this.GoBookmark(layoutControl, bookmark);
            }
        }

        /// <summary>
        /// 레이아웃 크기 변경시 요소 업데이트.
        /// </summary>
        /// <param name="layout">
        /// 업데이트할 레이아웃.
        /// </param>
        public static void UpdateLayoutSizeChange(LayoutControl layout)
        {
            if (layout == null)
            {
                return;
            }

            layout.UpdateLayoutSizeChange(layout);
        }

        /// <summary>
        /// 레이아웃 크기 변경시 요소 업데이트.
        /// </summary>
        /// <param name="layout">
        /// 업데이트할 레이아웃.
        /// </param>
        //public static void UpdateLayoutSizeChangeAll(LayoutControl layout)
        //{
        //    if (layout == null)
        //    {
        //        return;
        //    }

        //    foreach (var childElement in layout.GetElements())
        //    {
        //        // 레이아웃 중첩구 처리
        //        if (childElement is LayoutControl)
        //        {
        //            UpdateLayoutSizeChangeAll(childElement as LayoutControl);
        //        }

        //        UpdateCameraPtzVisibility(layout, childElement);

        //        if (!(childElement is BaseControl))
        //        {
        //            continue;
        //        }

        //        var baseControl = childElement as BaseControl;

        //        if (baseControl.IsAppearInAllLevel || !baseControl.PlayVisible)
        //        {
        //            continue;
        //        }

        //        var zoomRatioPercent = layout.ZoomRatio * 100;

        //        if (zoomRatioPercent >= baseControl.AppearanceMinLevel &&
        //            zoomRatioPercent <= baseControl.AppearanceMaxLevel)
        //        {
        //            baseControl.Visibility = Visibility.Visible;
        //        }
        //        else
        //        {
        //            baseControl.Visibility = Visibility.Hidden;
        //        }
        //    }
        //}

        public DataBookmark MakeNewBookmark()
        {
            var bookmark = new DataBookmark();

            foreach (var childObject in LayoutUtils.GetAllChildObjectList(this.GetPrimaryLayout()))
            {
                if (childObject is LayoutControl)
                {
                    var layoutControl = (childObject as LayoutControl);

                    var bookmarkItem = new DataBookmarkItem
                        {
                            DestLayoutGUID = layoutControl.SyncGUID,
                            ZoomRect = layoutControl.LayoutActualRect
                        };

                    bookmark.BookmarkItems.Add(bookmarkItem);
                }
            }

            return bookmark;
        }

        /// <summary>
        /// trace list.
        /// </summary>
        public void TraceList()
        {
            InnotiveDebug.Trace(1, "-----------------------------------------------------------");
            InnotiveDebug.Trace(1, "list에 추가된 layout count = {0}", this.LayoutControlList.Count);

            foreach (BaseLayoutControl baseLayoutControl in this.LayoutControlList)
            {
                if (baseLayoutControl == null)
                {
                    InnotiveDebug.Trace(4, "LayoutControl이 null임 !!");
                    continue;
                }

                InnotiveDebug.Trace(1, "layout name = {0}", baseLayoutControl.Name);
            }

            InnotiveDebug.Trace(1, "-----------------------------------------------------------");
        }

        /// <summary>
        /// trace camera brush status.
        /// </summary>
        public void TraceAllCameraBrushStatus()
        {
            foreach (LayoutControl layoutControl in this.LayoutControlList)
            {
                foreach (var element in LayoutUtils.GetAllChildObjectList(layoutControl))
                {
                    var camera = element as CameraControl;
                    if (camera == null)
                    {
                        continue;
                    }

                    if (camera.GetCameraBrush() == null)
                        InnotiveDebug.Trace(4, "camera ID = {0}, Name = {1}, brush = null !!", camera.ID, camera.Name);
                    else
                        InnotiveDebug.Trace(1, "camera ID = {0}, Name = {1}, brush = null이 아님 !!", camera.ID, camera.Name);
                }
            }
        }

        /// <summary>
        /// 내부 목록에 생성한 LayoutControl을 목록에 추가하고 해당Layout을 반환한다.
        /// </summary>
        /// <param name="loadedActionGuid">
        /// The loaded Action Guid.
        /// </param>
        /// <returns>
        /// The LayoutControl.
        /// </returns>
        internal LayoutControl LoadLayoutControl(Guid loadedActionGuid, bool createVideoSurface)
        {
            LayoutControl groundLayoutControl;

            // load된 layout이 존재하지 않으면 file에서 layout을 load함.
            if (string.IsNullOrEmpty(this._currentXamlString))
            {
                try
                {
                    string xamlString = File.ReadAllText(this.FileName.Path);
                    groundLayoutControl = (LayoutControl)InnoXamlReader.Load(xamlString);
                }
                catch
                {
                    return null;
                }
            }
            else
            {
                // load된 layout이 존재하는 경우 xaml data에서 layout을 load함 !! (Sync를 맞추기 위해)
                try
                {
                    groundLayoutControl = (LayoutControl)InnoXamlReader.Load(this._currentXamlString);
                }
                catch
                {
                    return null;
                }
            }

            if (groundLayoutControl == null)
            {
                return null;
            }

            groundLayoutControl.LoadedActionGuid = loadedActionGuid;

            this.LayoutControlList.Add(groundLayoutControl);

            SetPlayerProcess(groundLayoutControl);

            SetMapEventHandler(groundLayoutControl);

            this.AttachEventHandler(groundLayoutControl);

            // 영상 표출 수가 0이면 Surface를 생성하지 않는다.
            if(CommonsConfig.Instance.MaxHighResSourceCount != 0)
            {
                if (Public.GetProgramType() != ProgramType.iEditor && createVideoSurface)
                {
                    // Layout 이 생성되면 전용 VideoSurface를 하나 만들어 가진다.
                    LayoutControl.AddVideoSurface(groundLayoutControl);
                }    
            }

            return groundLayoutControl;
        }



        internal List<LayoutControl> GetAllLayoutControlList()
        {
            var result = new List<LayoutControl>();
            foreach (var layoutControl in this.LayoutControlList)
            {
                result.Add((LayoutControl)layoutControl);
            }
            return result;
        }

        internal List<FrameworkElement> GetElementList(Guid layoutGUID, Guid elementGUID)
        {
            if (layoutGUID == Guid.Empty)
            {
                return this.GetElementList(elementGUID);
            }

            var result = new List<FrameworkElement>();

            foreach (var layoutControl in this.LayoutControlList)
            {
                if (layoutControl.SyncGUID == layoutGUID)
                {
                    result.AddRange(GetElementListFromLayout((LayoutControl)layoutControl, elementGUID));
                }

                foreach (var element in LayoutUtils.GetAllChildObjectList(layoutControl))
                {
                    var canvas = element as LayoutControl;
                    if (canvas == null)
                    {
                        continue;
                    }

                    if (canvas.SyncGUID == layoutGUID)
                    {
                        result.AddRange(GetElementListFromLayout(canvas, elementGUID));
                    }
                }
            }

            return result;
        }

        internal List<FrameworkElement> GetElementList(Guid elementGUID)
        {
            var result = new List<FrameworkElement>();
            if (elementGUID == Guid.Empty)
            {
                return result;
            }

            foreach (LayoutControl layoutControl in this.LayoutControlList)
            {
                result.AddRange(GetElementListFromLayout(layoutControl, elementGUID));

                foreach (var element in LayoutUtils.GetAllChildObjectList(layoutControl))
                {
                    var canvas = element as LayoutControl;
                    if (canvas == null)
                    {
                        continue;
                    }

                    result.AddRange(GetElementListFromLayout(canvas, elementGUID));
                }
            }

            return result;
        }

        internal void DoMouseOverByElementGuid(Guid elementGuid, bool isMouseOver)
        {

        }

        internal List<LayoutControl> GetLayoutControlList(Guid layoutGUID)
        {
            var result = new List<LayoutControl>();

            foreach (LayoutControl layoutControl in this.LayoutControlList)
            {
                if (layoutControl.SyncGUID == layoutGUID)
                {
                    result.Add(layoutControl);
                }

                foreach (var element in LayoutUtils.GetAllChildObjectList(layoutControl))
                {
                    var canvas = element as LayoutControl;
                    if (canvas == null)
                    {
                        continue;
                    }

                    if (canvas.SyncGUID == layoutGUID)
                    {
                        result.Add(canvas);
                    }
                }
            }

            return result;
        }

        internal void DoGoHomeActionByTargetLayoutGuid(Guid layoutGuid, string effect, double start, double duration)
        {
            foreach (LayoutControl layoutControl in this.LayoutControlList)
            {
                if (layoutControl.SyncGUID == layoutGuid)
                {
                    var bookmarkHome = MainProcess.Instance.ProjectManager.BookmarkManager.GetBookmarkHome(layoutControl.SyncGUID);
                    if (bookmarkHome != null)
                    {
                        MainProcess.Instance.ProjectManager.LayoutContentsSyncManager.GoToLocation(
                            layoutGuid,
                            bookmarkHome.SyncGUID,
                            effect,
                            start,
                            duration,
                            null);
                    }
                }
            }
        }

        internal void GoToLocationByTargetLayoutGuid(Guid layoutGuid, Rect zoomRect, string effect, double startTime, double duration)
        {
            foreach (LayoutControl layoutControl in this.LayoutControlList)
            {
                if (layoutControl.SyncGUID == layoutGuid)
                {
                    layoutControl.GoToLocationAnimation(zoomRect, startTime, duration, null);
                }

                foreach (var element in LayoutUtils.GetAllChildObjectList(layoutControl))
                {
                    var canvas = element as LayoutControl;
                    if (canvas == null)
                    {
                        continue;
                    }

                    if (canvas.SyncGUID == layoutGuid)
                    {
                        canvas.GoToLocationAnimation(zoomRect, startTime, duration, null);
                    }
                }
            }
        }

        /// <summary>
        /// RemoveLayoutControl: 20081128.
        /// </summary>
        /// <param name="layoutControl">
        /// The LayoutCotnrol.
        /// </param>
        internal void RemoveLayoutControl(LayoutControl layoutControl)
        {
            if (layoutControl == null)
            {
                return;
            }

            this.DetachEventHandler(layoutControl);

            DisposeAllChildMirrors(layoutControl);

            DetachPlayVisible(layoutControl);

            UnregisterXamlviewerToManager(layoutControl);

            this.LayoutControlList.Remove(layoutControl);

            layoutControl.Dispose();

            ILayoutFileData layoutFileData =
                MainProcess.Instance.ProjectManager.PlayerLayoutManager.GetLayoutFileData(layoutControl.SyncGUID);
            InnotiveDebug.Trace(
                1,
                "PlayerLayoutFileData.RemoveLayoutControl() layout control 삭제함 guid = {0}, name = {1} ===> count = {2}",
                layoutControl.SyncGUID,
                layoutFileData.Name,
                layoutFileData.LayoutControlList.Count);
        }

        /// <summary>
        /// _xamlString에 현재 PrimaryLayout의 데이타를 설정한다.
        /// </summary>
        internal void SetXamlString()
        {
            this._currentXamlString = this.GetPrimaryLayout() == null ? string.Empty : ((LayoutControl)this.GetPrimaryLayout()).GetXamlString();
        }

        private static void SetPlayerProcess(BaseControl layoutControl)
        {
            if (layoutControl == null)
            {
                return;
            }

            layoutControl.Process = MainProcess.Instance.PlayerProcess;

            foreach (var element in LayoutUtils.GetAllChildObjectList(layoutControl))
            {
                element.Process = MainProcess.Instance.PlayerProcess;
            }
        }

        private static void SetMapEventHandler(BaseControl layoutControl)
        {
            if (layoutControl == null)
            {
                return;
            }

            foreach (var element in LayoutUtils.GetAllChildObjectList(layoutControl))
            {
                if (element is MapControl)
                {
                    var mapControl = element as MapControl;
                    mapControl.MapZoomChangedEventHandler += mapControl_MapZoomChangedEventHandler;
                    mapControl.MapPositionChangedEventHandler += mapControl_MapPositionChangedEventHandler;
                    mapControl.MapPositionChangeEndEventHandler += mapControl_MapPositionChangeEndEventHandler;
                    mapControl.MapPositionFitCameraHandler += mapControl_MapPositionFitCameraHandler;
                    mapControl.MapFullViewCameraHandler += mapControl_MapFullViewCameraHandler;
                }
            }
        }

        static void mapControl_MapFullViewCameraHandler(object sender, DLLs.MapControls.Event.FullViewCameraEventArgs e)
        {
            var mapControl = sender as MapControl;
            if (mapControl == null)
            {
                return;
            }

            MainProcess.Instance.ProjectManager.LayoutContentsSyncManager.MapControlFullViewCamera(mapControl.SyncGUID, e.CameraId, e.IsFull);
        }

        static void mapControl_MapPositionFitCameraHandler(object sender, DLLs.MapControls.Event.FitCameraEventArgs e)
        {
            var mapControl = sender as MapControl;
            if (mapControl == null)
            {
                return;
            }

            // 노노 이게 아님.
            //MainProcess.Instance.ProjectManager.LayoutContentsSyncManager.MapControlFullViewCamera(mapControl.SyncGUID, e.CameraId);
        }

        static void mapControl_MapZoomChangedEventHandler(object sender, EventArgs e)
        {
            var mapControl = sender as MapControl;
            if (mapControl == null)
            {
                return;
            }

            MainProcess.Instance.ProjectManager.LayoutContentsSyncManager.MapControlZoomChanged(mapControl.SyncGUID, mapControl.MapZoom);
        }

        static void mapControl_MapPositionChangedEventHandler(object sender, EventArgs e)
        {
            var mapControl = sender as MapControl;
            if (mapControl == null)
            {
                return;
            }

            MainProcess.Instance.ProjectManager.LayoutContentsSyncManager.MapControlPositionChanged(mapControl.SyncGUID, mapControl.MapLatitude, mapControl.MapLongitude);
        }

        static void mapControl_MapPositionChangeEndEventHandler(object sender, EventArgs e)
        {
            var mapControl = sender as MapControl;
            if (mapControl == null)
            {
                return;
            }

            MainProcess.Instance.ProjectManager.LayoutContentsSyncManager.MapControlPositionChangeEnd(mapControl.SyncGUID, mapControl.MapLatitude, mapControl.MapLongitude);
        }

        /// <summary>
        /// ePlayVisibleChanged 이벤트를 레이아웃에서 제거한다.
        /// </summary>
        /// <param name="layoutControl">
        /// The LayoutControl.
        /// </param>
        private static void DetachPlayVisible(LayoutControl layoutControl)
        {
            if (layoutControl == null)
            {
                return;
            }

            foreach (var element in LayoutUtils.GetAllChildObjectList(layoutControl))
            {
                if (element is BaseLayoutControl)
                {
                    var control = element as BaseLayoutControl;
                    control.PlayVisibleChanged -= PlayerLayoutFileDataEPlayVisibleChanged;

                    DetachPlayVisible(control as LayoutControl);
                }
                else
                {
                    var control = element;
                    control.PlayVisibleChanged -= PlayerLayoutFileDataEPlayVisibleChanged;
                }
            }
        }

        private static List<FrameworkElement> GetElementListFromElementList(UIElementCollection elementList, Guid elementGUID)
        {
            var result = new List<FrameworkElement>();

            foreach (var element in elementList)
            {
                if (element is BaseLayoutControl)
                {
                    var layoutControl = element as BaseLayoutControl;
                    if (layoutControl.SyncGUID == elementGUID)
                    {
                        result.Add((FrameworkElement)element);
                    }
                }
                else if (element is BaseControl)
                {
                    var baseControl = element as BaseControl;
                    if (baseControl.SyncGUID == elementGUID)
                    {
                        result.Add((FrameworkElement)element);
                    }
                }
            }

            return result;
        }

        private static void layoutControl_eChanged(object sender, EventArgs e)
        {
            var layout = sender as LayoutControl;
            if (layout != null)
            {
                UpdateLayoutSizeChange(layout);
            }
        }

        private static void LayoutControlETriggerActionEvent(object sender, ActionEventArgs actionEventArgs)
        {
            actionEventArgs.IsExcuted =
                MainProcess.Instance.ProjectManager.PlayerActionManager.ExcuteActionProcess(
                    sender as FrameworkElement, actionEventArgs.TriggerEventType, actionEventArgs.IsSendToSyncServer);
        }

        private static void PlayerLayoutFileDataEPlayVisibleChanged(object sender, RoutedEventArgs e)
        {
            var element = e.Source as BaseControl;

            Debug.Assert(element != null, "element != null");

            var layout = element.ParentBaseLayoutControl as LayoutControl;

            if (element is LayoutControl)
            {
                var control = element as BaseLayoutControl;

                // element 가 RootLayoutControl 인 경우 
                if (layout == null)
                {
                    layout = control as LayoutControl;
                }

                if (control.PlayVisible)
                {
                    // Appear 레벨이 설정되있는 경우 
                    if (!control.IsAppearInAllLevel)
                    {
                        // 부모의 레벨이 설정된 레벨 안에 존재하는 경우에만 보여주도록 한다.
                        if (layout != null)
                        {
                            var zoomRatioPercent = layout.ZoomRatio * 100;

                            if (zoomRatioPercent >= control.AppearanceMinLevel &&
                                zoomRatioPercent <= control.AppearanceMaxLevel)
                            {
                                control.Visibility = Visibility.Visible;
                            }
                        }
                    }
                    else
                    {
                        // 모든 레벨에서 나타나는 경우 
                        control.Visibility = Visibility.Visible;
                    }
                }
                else
                {
                    control.Visibility = Visibility.Hidden;
                }
            }
            else if (element is BaseControl)
            {
                if (layout == null)
                {
                    return;
                }

                if (element.PlayVisible)
                {
                    // Appear 레벨이 설정되있는 경우 
                    if (!element.IsAppearInAllLevel)
                    {
                        // 부모의 레벨이 설정된 레벨 안에 존재하는 경우에만 보여주도록 한다.
                        var zoomRatioPercent = layout.ZoomRatio * 100;

                        if (zoomRatioPercent >= element.AppearanceMinLevel &&
                            zoomRatioPercent <= element.AppearanceMaxLevel)
                        {
                            element.Visibility = Visibility.Visible;
                        }
                    }
                    else
                    {
                        // 모든 레벨에서 나타나는 경우 
                        element.Visibility = Visibility.Visible;
                    }
                }
                else
                {
                    element.Visibility = Visibility.Hidden;
                }
            }
        }

        private static void RegisterXamlViewerToManager(LayoutControl layoutControl)
        {
            if (layoutControl == null)
            {
                return;
            }

            foreach (var element in LayoutUtils.GetAllChildObjectList(layoutControl))
            {
                
            }
        }

        private static void SetPlayVisible(LayoutControl layoutControl)
        {
            if (layoutControl == null)
            {
                return;
            }

            foreach (var element in LayoutUtils.GetAllChildObjectList(layoutControl))
            {
                if (element is BaseLayoutControl)
                {
                    var control = element as BaseLayoutControl;
                    if (control.IsAppearInAllLevel)
                    {
                        control.Visibility = control.PlayVisible ? Visibility.Visible : Visibility.Hidden;
                    }
                    else
                    {
                        control.Visibility = Visibility.Hidden;
                    }

                    control.PlayVisibleChanged += PlayerLayoutFileDataEPlayVisibleChanged;
                }
                else
                {
                    var control = element;
                    if (control.IsAppearInAllLevel)
                    {
                        control.Visibility = control.PlayVisible ? Visibility.Visible : Visibility.Hidden;
                    }
                    else
                    {
                        control.Visibility = Visibility.Hidden;
                    }

                    control.PlayVisibleChanged += PlayerLayoutFileDataEPlayVisibleChanged;
                }
            }
        }

        private static void UnregisterXamlviewerToManager(LayoutControl layoutControl)
        {
            if (layoutControl == null)
            {
                return;
            }

            foreach (var element in LayoutUtils.GetAllChildObjectList(layoutControl))
            {
                
            }
        }

        private static void layoutControl_ePanningEnded(object sender, PanningEndedRoutedEventArgs e)
        {
            e.Handled = true;

            var layoutControl = e.OriginalSource as LayoutControl;
            if (layoutControl == null)
            {
                return;
            }

            MainProcess.Instance.ProjectManager.LayoutContentsSyncManager.PanningEnd(layoutControl);
        }

        private static void layoutControl_ePanningStarted(object sender, PanningStartedRoutedEventArgs e)
        {
            e.Handled = true;

            var layoutControl = e.OriginalSource as LayoutControl;
            if (layoutControl == null)
            {
                return;
            }

            MainProcess.Instance.ProjectManager.LayoutContentsSyncManager.PanningStart(layoutControl);
        }

        private static void layoutControl_eSettingRect(object sender, RectRoutedEventArgs e)
        {
            e.Handled = true;

            var layoutControl = e.OriginalSource as LayoutControl;
            Rect settingRect = e.Rect;
            if (MainProcess.Instance.ProjectManager.LayoutContentsSyncManager == null)
            {
                return;
            }
            MainProcess.Instance.ProjectManager.LayoutContentsSyncManager.SettingRect(layoutControl, settingRect);
        }

        private static void layoutControl_eZoomIn(object sender, RectRoutedEventArgs e)
        {
            e.Handled = true;

            var layoutControl = e.OriginalSource as LayoutControl;
            Rect desireRect = e.Rect;
            MainProcess.Instance.ProjectManager.LayoutContentsSyncManager.ZoomIn(layoutControl, desireRect);
        }

        private static void layoutControl_eZoomOut(object sender, RectRoutedEventArgs e)
        {
            e.Handled = true;

            var layoutControl = e.OriginalSource as LayoutControl;
            Rect desireRect = e.Rect;
            MainProcess.Instance.ProjectManager.LayoutContentsSyncManager.ZoomOut(layoutControl, desireRect);
        }

        /// <summary>
        /// 레이아웃에 있는 해당 GUID를 가진 모든 요소 찾아 반환함.
        /// </summary>
        /// <param name="layoutControl"></param>
        /// <param name="elementGUID"></param>
        /// <returns></returns>
        private static List<FrameworkElement> GetElementListFromLayout(LayoutControl layoutControl, Guid elementGUID)
        {
            var result = new List<FrameworkElement>();

            if (layoutControl != null)
            {
                result.AddRange(GetElementListFromElementList(layoutControl.GetUnlockedElements(), elementGUID));
                result.AddRange(GetElementListFromElementList(layoutControl.GetLockedElements(), elementGUID));
            }

            return result;
        }

        private static void DisposeAllChildMirrors(LayoutControl layoutControl)
        {
            if (layoutControl == null)
            {
                return;
            }

            var mirrorlist = LayoutUtils.GetAllChildMirrorObjectList(layoutControl);
            foreach (var element in mirrorlist)
            {
                
                element.Dispose();
            }
        }

        private void DetachEventHandler(LayoutControl layoutControl)
        {
            // 기존 존재하던 소스
            var layoutList = LayoutUtils.GetProgenyLayoutControls(layoutControl);
            foreach (var layout in layoutList)
            {
                layout.eTriggerActionEvent -= LayoutControlETriggerActionEvent;
                layout.eChanged -= layoutControl_eChanged;
                layoutControl.eVisibility -= layoutControl_eVisibility;
            }

            layoutControl.eTriggerActionEvent -= LayoutControlETriggerActionEvent;
            layoutControl.eChanged -= layoutControl_eChanged;
            layoutControl.eVisibility -= layoutControl_eVisibility;

            // 새로 추가된 소스
            layoutControl.Loaded -= this.layoutControl_Loaded;

            layoutControl.eZoomIn -= layoutControl_eZoomIn;
            layoutControl.eZoomOut -= layoutControl_eZoomOut;
            layoutControl.eSettingRect -= layoutControl_eSettingRect;
            layoutControl.ePanningStarted -= layoutControl_ePanningStarted;
            layoutControl.ePanningEnded -= layoutControl_ePanningEnded;
        }

        private void AttachEventHandler(LayoutControl layoutControl)
        {
            layoutControl.Loaded += this.layoutControl_Loaded;

            layoutControl.eZoomIn += layoutControl_eZoomIn;
            layoutControl.eZoomOut += layoutControl_eZoomOut;
            layoutControl.eSettingRect += layoutControl_eSettingRect;
            layoutControl.ePanningStarted += layoutControl_ePanningStarted;
            layoutControl.ePanningEnded += layoutControl_ePanningEnded;
        }

        private void GoBookmark(BaseLayoutControl baseLayoutControl, DataBookmark bookmark)
        {
            if (bookmark == null)
            {
                return;
            }

            var layoutList = LayoutUtils.GetProgenyLayoutControls(baseLayoutControl);

            layoutList.Add(baseLayoutControl as LayoutControl);

            // 선택된 Layout과 iCanvas들을 Bookmark의 위치로 이동함 !!));)
            foreach (var bookmarkItem in bookmark.BookmarkItems)
            {
                if (bookmarkItem == null)
                {
                    continue;
                }

                foreach (LayoutControl layoutControl in layoutList)
                {
                    if (layoutControl == null)
                    {
                        continue;
                    }

                    if (layoutControl.SyncGUID == bookmarkItem.DestLayoutGUID)
                    {
                        layoutControl.GoToLocationAnimation(bookmarkItem.ZoomRect, 0, 0, null);
                    }
                }
            }
        }

        public void UpdateXamlViewControlsFlag(LayoutControl layoutControl, bool flag)
        {
            if (layoutControl == null)
            {
                return;
            }

            
        }

        private void HandleLayoutControlLoaded(LayoutControl layoutControl)
        {
            if (layoutControl == null)
            {
                return;
            }

            this.UpdateXamlViewControlsFlag(layoutControl, true);

            //by blackRoot : bookmark home 동작이 됐다 안됐다 하는 문제를 해결하기 위함 !!
            //실제 Layout 내부 LayoutControl_Loaded()와 여기 함수의 호출순서가 일정하지 않음 !!
            //양쪽 다 LayoutActualRect를 수정하는 코드를 가지고 있음 !! 순서를 일정하게 하기 위해서 Layout내부 소스를 먼저 실행해주는게 좋을듯.. !!
            //GoHome이 완료되는 시점에 LayoutActualRect값이 설정이 되기 때문에 궂이 안해줘도 될듯 !!
            //layoutControl.LayoutActualRect = new Rect(0, 0, layoutControl.OriginWidth, layoutControl.OriginHeight);

            // go bookmark home
            this.GoHome(layoutControl);

            SetPlayVisible(layoutControl);
            UpdateLayoutSizeChangeForVisibility(layoutControl);

            //Layout 생성시 Camera의 Min/Max Level 적용
            //UpdateLayoutSizeChange(layoutControl);

            //RegisterCameraToPTZManager(layoutControl);

            ZoomPanSmoothManager.eEnded += layoutControl_eChanged;
            layoutControl.eTriggerActionEvent += LayoutControlETriggerActionEvent;
            layoutControl.eChanged += layoutControl_eChanged;
            layoutControl.eVisibility += layoutControl_eVisibility;

            var allchildobjectlist = LayoutUtils.GetAllChildObjectList(layoutControl);
            foreach (var element in allchildobjectlist)
            {
                var canvas = element as LayoutControl;
                if (canvas == null)
                {
                    continue;
                }

                canvas.eTriggerActionEvent += LayoutControlETriggerActionEvent;
                canvas.eChanged += layoutControl_eChanged;
                canvas.eVisibility += layoutControl_eVisibility;
            }

            if (!layoutControl.LoadedActionGuid.Equals(Guid.Empty))
            {
                MainProcess.Instance.ProjectManager.PlayerActionManager.Excute(layoutControl.LoadedActionGuid);
            }

            RegisterXamlViewerToManager(layoutControl);
            this.UpdateXamlViewControlsFlag(layoutControl, false);
        }

        private void layoutControl_Loaded(object sender, RoutedEventArgs e)
        {
            var layoutControl = sender as LayoutControl;

            if (layoutControl != null)
            {
                InnotiveDebug.Trace("[Action] PlayerLayoutFileData layoutControl_Loaded() LayoutName : {0}",
                                layoutControl.Name);
                this.HandleLayoutControlLoaded(layoutControl);
            }
        }

        private void layoutControl_eVisibility(object sender, RoutedEventArgs e)
        {
            var layout = sender as LayoutControl;
            if (layout != null)
            {
                UpdateLayoutSizeChangeForVisibility(layout);
            }
        }

        public static void UpdateLayoutSizeChangeForVisibility(LayoutControl layout)
        {
            if (layout == null)
            {
                return;
            }

            layout.UpdateLayoutSizeChangeForVisibility(layout);
        }

        private void Read(InnoFilePath layoutFile)
        {
            this.Init();
            this.FileName = layoutFile;

            if (this.FileName.IsAbsolutePath && !File.Exists(this.FileName.Path))
            {
                return;
            }

            var xmlDoc = new XmlDocument();
            xmlDoc.Load(this.FileName.Path);
            var node = InnoXMLUtil.GetBaseNode(xmlDoc);

            if (node != null)
            {
                double width;
                double height;

                this.Name = InnoXMLUtil.GetNodeAttribute(node, "Name");
                this.LayoutDescription = InnoXMLUtil.GetNodeAttribute(node, "LayoutDescription");

                this.LayoutShowToList =
                    !InnoXMLUtil.GetNodeAttribute(node, "LayoutShowToList").Equals(
                        "False", StringComparison.OrdinalIgnoreCase);

                this.SyncGUID = new Guid(InnoXMLUtil.GetNodeAttribute(node, "SyncGUID"));
                double.TryParse(InnoXMLUtil.GetNodeAttribute(node, "Width"), out width);
                double.TryParse(InnoXMLUtil.GetNodeAttribute(node, "Height"), out height);
                this.Size = new Size(width, height);
            }
        }

        private void Init()
        {
            this.Name = string.Empty;
            this.LayoutDescription = string.Empty;
            this.LayoutShowToList = true;
            this.OriginGUID = Guid.Empty;
            this.SyncGUID = Guid.Empty;
            this.Size = Size.Empty;
        }
    }
}