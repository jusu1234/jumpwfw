﻿
using Innotive.InnoWatch.Commons.BaseControls;

namespace Innotive.InnoWatch.PlayerCommons.Managers
{
    using System;
    using System.Windows.Controls;
    using System.Windows.Forms;
    using Innotive.InnoWatch.PlayerCommons.Configs;

    public class ServerStatusShowManager
    {
        /// <summary>
        /// 통신 상태 컨트롤.
        /// </summary>
        private readonly UIs.Controls.DisconnectIconsControl disconnectionControl = new UIs.Controls.DisconnectIconsControl();

        private Canvas parent;

        private ServerStatusShowManager()
        {
            // Singleton 생성자.
        }

        /// <summary>
        /// Gets Instance.
        /// </summary>
        public static ServerStatusShowManager Instance
        {
            get
            {
                return Nested<ServerStatusShowManager>.Instance;
            }
        }

        public void Initialize(bool use, Canvas parent, double left, double top)
        {
            if (parent == null) return;

            this.parent = parent;
            this.disconnectionControl.Use = use;
            if (use)
            {
                // 연결 상태 컨트롤 초기화.
                this.parent.Dispatcher.BeginInvoke(
                    new Action(() =>
                    {
                        this.disconnectionControl.ControlDataConnected = true;
                        this.disconnectionControl.ContentSyncConnected = true;
                        this.disconnectionControl.CameraControllerConnected = true;
                    }));

                Screen[] monitorDeviceList = Screen.AllScreens;
                if (monitorDeviceList.Length > 0)
                {
                    Canvas.SetLeft(this.disconnectionControl, left);
                    Canvas.SetTop(this.disconnectionControl, top);
                }

                // 연결 상태 컨트롤을 추가.
                this.parent.Dispatcher.BeginInvoke(
                    new Action(() =>
                        {
                            parent.Children.Add(this.disconnectionControl);

                            // 1.19 이형우 AlarmBorder는 알람이 있을 경우 항상 보이게 되므로 가장 높은 Zindex로 세팅해 놓아야 한다.
                            System.Windows.Controls.Panel.SetZIndex(this.disconnectionControl, 100000);
                        }));

                // 02. 싱크 서버 사용 여부에 따라 싱크 서버 연결 상태 표시 설정.
                if (PlayerCommonsConfig.Instance.UseAppSyncServer)
                {
                    this.parent.Dispatcher.BeginInvoke(
                    new Action(() =>
                    {
                        this.disconnectionControl.ContentSyncConnected = true;
                    }));

                    MainProcess.Instance.CommManager.AppSyncClient.eSyncConnected += this.AppSyncClient_eSyncConnected;
                    MainProcess.Instance.CommManager.AppSyncClient.eSyncDisconnected += this.AppSyncClient_eSyncDisconnected;
                }
                else
                {
                    this.parent.Dispatcher.BeginInvoke(
                    new Action(() =>
                    {
                        this.disconnectionControl.ContentSyncConnected = true;
                    }));
                }
            }
        }

        private void AppSyncClient_eSyncDisconnected(object sender, EventArgs e)
        {
            if (this.parent != null)
            {
                this.parent.Dispatcher.BeginInvoke(
                    new Action(() =>
                    {
                        this.disconnectionControl.ContentSyncConnected = false;
                    }));
            }
        }

        private void AppSyncClient_eSyncConnected(object sender, EventArgs e)
        {
            if (this.parent != null)
            {
                this.parent.Dispatcher.BeginInvoke(
                    new Action(() =>
                    {
                        this.disconnectionControl.ContentSyncConnected = true;
                    }));
            }
        }
    }
}
