// --------------------------------------------------------------------------------------------------------------------
// <copyright company="Innotive Inc. Korea" file="ProjectManager.cs">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   project manager.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.PlayerCommons.Projects
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Windows;
    using System.Windows.Media;
    using Innotive.InnoWatch.Commons.CameraManagers;
    using Innotive.InnoWatch.Commons.Configs;
    using Innotive.InnoWatch.Commons.Files;
    using Innotive.InnoWatch.Commons.GuidManagers;
    using Innotive.InnoWatch.Commons.Monitors;
    using Innotive.InnoWatch.Commons.Projects;
    using Innotive.InnoWatch.Commons.Publics;
    using Innotive.InnoWatch.Commons.ResourceManagements;
    using Innotive.InnoWatch.Commons.Utils;
    using Innotive.InnoWatch.CommonUtils.Log;
    using Innotive.InnoWatch.CommonUtils.Models.Sync.Control;    
    using Innotive.InnoWatch.PlayerCommons.Actions;
    using Innotive.InnoWatch.PlayerCommons.Communications.Sync;
    using Innotive.InnoWatch.PlayerCommons.DisplayAreas;
    using Innotive.InnoWatch.PlayerCommons.Layouts;
    using Innotive.InnoWatch.PlayerCommons.Managers;

    /// <summary>
    /// project manager.
    /// </summary>
    public class ProjectManager : IProjectManager
    {
        #region Constants and Fields

        private PlayerDisplayAreaManager _displayAreaManager;

        private DisplayAreaOpenChangeLayoutSyncManager _displayAreaOpenChangeLayoutSyncManager;

        private LayoutContentsSyncManager _layoutContentsSyncManager;

        private LocationMapSyncManager _locationMapSyncManager;

        private List<Window> _loadingImageWindowList = new List<Window>();

        private OverlayDisplayAreaManager _overlayDisplayAreaManager;

        private PlayerActionManager _playerActionManager;

        private PlayerLayoutFileManager _playerLayoutManager;

        private ResourceManager _playerResourceManager;

        private CameraControlPtzUIManager _cameraControllerManager;

        private readonly Innotive.InnoWatch.Commons.Bookmarks.BookmarkManager bookmarkManager;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ProjectManager"/> class.
        /// </summary>
        public ProjectManager()
        {
            this.MonitorManager = new MonitorManager();
            this.IsOpened = false;
            this.ProjectConfiguration = new BaseProjectConfiguration();

            this._playerLayoutManager = new PlayerLayoutFileManager();
            this._displayAreaManager = new PlayerDisplayAreaManager();
            this._overlayDisplayAreaManager = new OverlayDisplayAreaManager();

            this._displayAreaOpenChangeLayoutSyncManager = new DisplayAreaOpenChangeLayoutSyncManager(
                this._displayAreaManager, this._playerLayoutManager);

            this._layoutContentsSyncManager = new LayoutContentsSyncManager(this._displayAreaManager);

            this._locationMapSyncManager = new LocationMapSyncManager();

            this._playerResourceManager = ResourceManager.GetInstance(); 

            this._playerActionManager = new PlayerActionManager();

            this.ProjectConfiguration = new BaseProjectConfiguration();

            this.bookmarkManager = new InnoWatch.Commons.Bookmarks.BookmarkManager();

            this._cameraControllerManager = new CameraControlPtzUIManager();
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets DisplayAreaFileManager.
        /// </summary>
        public DisplayAreaFile DisplayAreaFileManager { get; set; }

        /// <summary>
        /// Gets DisplayAreaManager.
        /// </summary>
        public PlayerDisplayAreaManager DisplayAreaManager
        {
            get
            {
                return this._displayAreaManager;
            }
        }

        /// <summary>
        /// Gets DisplayAreaOpenChangeLayoutSyncManager.
        /// </summary>
        public DisplayAreaOpenChangeLayoutSyncManager DisplayAreaOpenChangeLayoutSyncManager
        {
            get
            {
                return this._displayAreaOpenChangeLayoutSyncManager;
            }
        }

        /// <summary>
        /// Gets LayoutContentsSyncManager.
        /// </summary>
        public LayoutContentsSyncManager LayoutContentsSyncManager
        {
            get
            {
                return this._layoutContentsSyncManager;
            }
        }

        public LocationMapSyncManager LocationMapSyncManager
        {
            get
            {
                return this._locationMapSyncManager;
            }
        }

        /// <summary>
        /// Gets LoadingImageWindowList.
        /// </summary>
        public List<Window> LoadingImageWindowList
        {
            get
            {
                return this._loadingImageWindowList;
            }
        }
        
        /// <summary>
        /// Gets OverlayDisplayAreaManager.
        /// </summary>
        public OverlayDisplayAreaManager OverlayDisplayAreaManager
        {
            get
            {
                return this._overlayDisplayAreaManager;
            }
        }

        /// <summary>
        /// Gets PlayerActionManager.
        /// </summary>
        public PlayerActionManager PlayerActionManager
        {
            get
            {
                return this._playerActionManager;
            }
        }

        /// <summary>
        /// Gets PlayerLayoutManager.
        /// </summary>
        public PlayerLayoutFileManager PlayerLayoutManager
        {
            get
            {
                return this._playerLayoutManager;
            }
        }

        /// <summary>
        /// Gets PlayerResourceManager.
        /// </summary>
        public ResourceManager PlayerResourceManager
        {
            get
            {
                return this._playerResourceManager;
            }
        }

        public Innotive.InnoWatch.Commons.Bookmarks.BookmarkManager BookmarkManager
        {
            get
            {
                return this.bookmarkManager;
            }
        }

        /// <summary>
        /// Gets CameraControllerManager.
        /// </summary>
        public CameraControlPtzUIManager CameraControllerManager
        {
            get
            {
                return this._cameraControllerManager;
            }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// change display mode.
        /// </summary>
        /// <param name="displayMode">
        /// The display mode.
        /// </param>
        public void ChangeDisplayMode(ConsoleDisplayMode displayMode)
        {
            Debug.WriteLine("<Player> ProjectManager.ChangeDisplayeMode : " + displayMode);

            this._overlayDisplayAreaManager.ChangeDisplayMode(displayMode);

            if (displayMode == ConsoleDisplayMode.ContentsMode)
            {
                if (CommonsConfig.Instance.EnableContentsMode)
                {
                    Debug.WriteLine("<Player> Contents Mode로 변경 !!");
                    InnotiveDebug.Trace(6, "================ <Player> Contents Mode로 변경 !!");
                    this.ShowGridLayout(false);
                    this.ShowContentsLayout(true);
                }
                else
                {
                    Debug.WriteLine("<Player> EnableContentsMode가 false임 !! Contents Mode로 변경하지 않음 !!");
                    InnotiveDebug.Trace(4, "================ <Player> EnableContentsMode가 false임 !! Contents Mode로 변경하지 않음 !!");
                }
            }
            else if (displayMode == ConsoleDisplayMode.ConsoleMode)
            {
                if (CommonsConfig.Instance.EnableCameraAssignMode)
                {
                    Debug.WriteLine("<Player> Camera Assign Mode로 변경 !!");
                    InnotiveDebug.Trace(6, "================ <Player> Camera Assign Mode로 변경 !!");
                    
                    this.ShowGridLayout(true);
                    this.ShowContentsLayout(false);
                    CameraManager.Instance.CurrentViewMode = ViewMode.Camera;
                }
                else
                {
                    Debug.WriteLine("<Player> EnableCameraAssignMode가 false임 !! Camera Assign Mode로 변경하지 않음 !!");
                    InnotiveDebug.Trace(4, "================ <Player> EnableCameraAssignMode가 false임 !! Camera Assign Mode로 변경하지 않음 !!");
                }
            }
        }

        public void CloseProject()
        {
            this.ProjectName = string.Empty;

            if (this.MonitorManager != null)
            {
                this.MonitorManager.Clear();
            }

            GuidManager.Clear();
        }

        /// <summary>
        /// open project.
        /// </summary>
        /// <param name="fileName">
        /// The file name.
        /// </param>
        /// <returns>
        /// The open project.
        /// </returns>
        /// <exception cref="Exception">
        /// </exception>
        public bool OpenProject(InnoFilePath fileName)
        {
            if (false == File.Exists(fileName.Path))
            {
                return false;
            }

            var projectFile = BaseFile.LoadBaseFile<ProjectFile>(
                fileName, projFile => projFile.ProjectFilePath = fileName);

            if (projectFile == null)
            {
                MessageBox.Show("존재하지 않는 프로젝트 파일입니다.");
                return false;
            }

            this.ProjectName = projectFile.ProjectName;

            this.ProjectDirectory = new InnoFilePath(Path.GetDirectoryName(fileName.Path));

            if (string.IsNullOrEmpty(this.ProjectName))
            {
                throw new Exception("Invalid ProjectFile Data");
            }

            Public.ProjectPath = this.ProjectDirectory;
            this.DisplayAreaFileManager = LoadFromProjectFile(fileName);
            this.MonitorManager.Load(projectFile.MonitorFileName);
            this._displayAreaManager.Load(projectFile.DisplayAreaFileName);
            this._playerActionManager.Load(projectFile.ActionsFileName.Path);
            this._playerLayoutManager.LoadAllFileData(projectFile.LayoutFileNameList);

            this.bookmarkManager.Load(projectFile.BookmarksFileName.Path);

            // file 정보를 저장함. 실제 Layout을 생성하지는 않음 !!
            this.ProjectConfiguration.Load(fileName);

            this.SetLoadingImageWindow();

            this._playerResourceManager.SetBaseDirectory(Directory.GetParent(fileName.Path).FullName);

            this.IsOpened = true;

            return true;
        }

        /// <summary>
        /// show loaded project.
        /// </summary>
        /// <returns>
        /// The show loaded project.
        /// </returns>
        public bool ShowLoadedProject()
        {
            if (!this.IsOpened)
            {
                return false;
            }

            this._displayAreaOpenChangeLayoutSyncManager.ShowStartLayouts();

            return true;                                    
        }

        #endregion

        #region Methods

        private static string GetDisPlayFilePath(InnoFilePath projectFilePath)
        {
            string fileName = Path.GetFileNameWithoutExtension(projectFilePath.Path);
            string dic = Path.GetDirectoryName(projectFilePath.Path);
            return string.Format("{0}\\{1}{2}", dic, fileName, new DisplayAreaFile().GetExtName());
        }

        /// <summary>
        /// load from project file.
        /// </summary>
        /// <param name="projectFile">
        /// The project file.
        /// </param>
        /// <returns>
        /// </returns>
        /// <exception cref="ArgumentException">
        /// </exception>
        /// <exception cref="InvalidOperationException">
        /// </exception>
        /// <exception cref="ApplicationException">
        /// </exception>
        private static DisplayAreaFile LoadFromProjectFile(InnoFilePath projectFile)
        {
            if (false == File.Exists(projectFile.Path))
            {
                throw new ArgumentException(string.Format("{0}", projectFile.Path));
            }

            // DisplayAreaFile Path 설정
            string disPlayFilePath = GetDisPlayFilePath(projectFile);

            if (false == File.Exists(disPlayFilePath))
            {
                throw new InvalidOperationException(string.Format("{0}", disPlayFilePath));
            }

            // DisplayAreaFile 로드
            var disPlay = BaseFile.LoadBaseFile<DisplayAreaFile>(new InnoFilePath(disPlayFilePath));

            if (null == disPlay)
            {
                throw new ApplicationException("DisplayAreaFile을 읽지 못했습니다.");
            }

            return disPlay;
        }

        private void SetLoadingImageWindow()
        {
            this._loadingImageWindowList.Clear();

            if (this.ProjectConfiguration.IsLoadingImageEnabled)
            {
                string imagePath = this.ProjectConfiguration.LoadingImagePath;

                if (string.IsNullOrEmpty(imagePath))
                {
                    return;
                }

                int displayAreaCount = this._displayAreaManager.Count;

                for (int i = 0; i < displayAreaCount; i++)
                {
                    
                }
            }
        }
        
        /// <summary>
        /// 컨텐츠 모드 여부에 따른 보이기/숨기기.
        /// </summary>
        /// <param name="isShow">
        /// The is Show.
        /// </param>
        private void ShowContentsLayout(bool isShow)
        {
            for (int i = 0; i < MainProcess.Instance.ProjectManager.DisplayAreaManager.Count; ++i)
            {
                PlayerDisplayArea displayArea = MainProcess.Instance.ProjectManager.DisplayAreaManager.GetDisplayArea(i) as PlayerDisplayArea;
                if (displayArea == null)
                {
                    continue;
                }

                if (isShow)
                {
                    displayArea.Show(true);
                }
                else
                {
                    displayArea.Show(false);
                }
            }
        }

        /// <summary>
        /// 스테이지 Grid 레이아웃 보이기/숨기기 (Grid Layout 숨기기/보이기).
        /// </summary>
        /// <param name="isShow">
        /// The is Show.
        /// </param>
        private void ShowGridLayout(bool isShow)
        {
            this._overlayDisplayAreaManager.ShowGridLayout(isShow);
        }

        #endregion

        public string ProjectName { get; set; }
        
        public InnoFilePath ProjectDirectory { get; set; }

        public bool IsOpened { get; set; }

        public MonitorManager MonitorManager { get; set; }

        public BaseProjectConfiguration ProjectConfiguration { get; set; }
    }
}