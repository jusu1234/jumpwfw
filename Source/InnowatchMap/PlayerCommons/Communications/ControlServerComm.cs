// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ControlServerComm.cs" company="Innotive Inc. Korea">
//   Copyright (c) 2012 Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   컨트롤 서버 통신 인터페이스.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.PlayerCommons.Communications
{
    using System;
    using System.ServiceModel;

    using Innotive.InnoWatch.Commons.Communications;
    using Innotive.InnoWatch.Commons.EventArguments;
    using Innotive.InnoWatch.Commons.Models;
    using Innotive.InnoWatch.CommonUtils.Log;
    using Innotive.InnoWatch.CommonUtils.Models;
    using Innotive.InnoWatch.PlayerCommons.Configs;

    /// <summary>/// 컨트롤 서버 통신 인터페이스.
    /// </summary>
    [CallbackBehavior(AutomaticSessionShutdown = false)]
    public class ControlServerComm : CommonUtils.Network.NetworkCore.IRecvContentSyncByTcp, IDisposable
    {
        /// <summary>
        /// 받기 액션을 위한 통신 관리자.
        /// </summary>
        private readonly CommManagerForActions _commManagerForReceiveActions; 

        /// <summary>
        /// 보내기 액션을 위한 통신 관리자.
        /// </summary>
        private readonly CommManagerForActions _commManagerForSendActions;

        /// <summary>
        /// Sync 도우미 속성.
        /// </summary>
        private readonly ControlServerCommForSync _syncHelper = new ControlServerCommForSync();

        private bool isDisposed = false;

        /// <summary>
        /// Initializes a new instance of the <see cref="ControlServerComm"/> class.
        /// </summary>
        public ControlServerComm()
        {
            this._commManagerForReceiveActions = new CommManagerForActions();
            this._commManagerForSendActions = new CommManagerForActions();
            this.StartCommManagerForAction();
            this.SendEnabled = true;
        }

        // Dispose 패턴 사용
        /// <summary>
        /// Finalizes an instance of the <see cref="ControlServerComm"/> class. 
        /// </summary>
        ~ControlServerComm()
        {
            this.Dispose();
        }
        
        /// <summary>
        /// 받기 액션을 위한 통신 관리자.
        /// </summary>
        public CommManagerForActions ReceiveManager
        {
            get
            {
                return this._commManagerForReceiveActions;
            }
        }

        /// <summary>
        /// 보내기 액션을 위한 통신 관리자.
        /// </summary>
        public CommManagerForActions SendManager
        {
            get
            {
                return this._commManagerForSendActions;
            }
        }

        /// <summary>
        /// Sync 도우미 속성.
        /// </summary>
        public ControlServerCommForSync SyncHelper
        {
            get
            {
                return this._syncHelper;
            }
        }

        /// <summary>
        /// 통신에서 보내기 기능의 활성화 유무 ( 과도한  Sync 를 제한하기 위해서 임시 구현 ). 
        /// </summary>
        public bool SendEnabled { get; set; }

        /// <summary>
        /// Release method.
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool isManage)
        {
            if (this.isDisposed)
            {
                return;
            }

            this.isDisposed = true;

            this.DoDispose(isManage);
        }

        /// <summary>
        /// do dispose.
        /// </summary>
        /// <param name="isManage">
        /// The is manage.
        /// </param>
        protected virtual void DoDispose(bool isManage)
        {
            if (isManage)
            {
                this._commManagerForSendActions.Stop();
                this._commManagerForReceiveActions.Stop();
            }
        }
        
        public void StartCommManagerForAction()
        {
            this._commManagerForSendActions.Start();
            this._commManagerForReceiveActions.Start();
        }

        /// <summary>
        /// Raise Alarm.
        /// </summary>
        /// <param name="alarmName">
        /// Alarm Name.
        /// </param>
        public void RaiseAlarm(string alarmName)
        {
            // Alarm은 iCommand, iViewer에서 받을 수 있음.
            if (MainProcess.Instance.ProgramType == ProgramType.iDisplay)
            {
                return;
            }

            this._commManagerForReceiveActions.Add(
               () => MainProcess.Instance.RaiseAlarmEvent(alarmName));
        }

        /// <summary>
        /// 카메라 정보 업데이트.
        /// </summary>
        public void UpdateCameraInformation()
        {
            if (MainProcess.Instance.ProgramType == ProgramType.iDisplay)
            {
                return;
            }

            this.SyncHelper.SendCameraInformationRefresh();
        }

        /// <summary>
        /// recv request new alarm.
        /// </summary>
        /// <param name="alaramParam">
        /// The AlarmRaisedEventParameter.
        /// </param>
        public void RecvRequestNewAlarm(AlarmRaisedEventParameter alaramParam)
        {
            // Alarm은 iCommand, iViewer에서 받을 수 있음.
            if (MainProcess.Instance.ProgramType == ProgramType.iDisplay)
            {
                return;
            }

            this._commManagerForReceiveActions.Add(
               () => MainProcess.Instance.RaiseExternalEvent(new ExternalAlarmRaisedEventArgs(alaramParam)));
        }

        void CommonUtils.Network.NetworkCore.IRecvContentSyncByTcp.RecvContentSync(CommonUtils.Models.Sync.BaseSyncList baseSyncList)
        {
            if (!PlayerCommonsConfig.Instance.UseAppSyncServer)
            {
                InnotiveDebug.Trace(this, 6, "[TcpSync] UseSyncServer == False라서 Sync를 수행하지 않음.");
                return;
            }

            // console일 때, Action을 수행한 console은 명령을 받지 않는다.
            if (MainProcess.Instance.ProgramType != ProgramType.iDisplay && MainProcess.Instance.ProgramType != ProgramType.iCommand)
            {
                InnotiveDebug.Trace(this, 6, "[TcpSync] Player가 아니라서 Sync를 수행하지 않음.");
                return;
            }

            InnotiveDebug.Trace(string.Format("[TcpSync] Sync Receive Start : List Size = {0}", baseSyncList.Count));

            if (baseSyncList != null)
            {
                for (var i = 0; i < baseSyncList.Count; i++)
                {
                    InnotiveDebug.Trace(this, 6, "[TcpSync] Sync Receive Start");

                    if (baseSyncList[i] == null)
                        continue;

                    // 중복된 싱크 메시지와 눈에 거의 띄지 않은 싱크들은 무시.
                    if (i < baseSyncList.Count - 1)
                    {
                        if (baseSyncList[i].SyncCommandType == CommonUtils.Models.Sync.SyncCommandType.SettingRect ||
                            baseSyncList[i].SyncCommandType == CommonUtils.Models.Sync.SyncCommandType.PanningEnd )
                        {
                            if (baseSyncList[i].SyncCommandType == baseSyncList[i + 1].SyncCommandType)
                            {
                                System.Diagnostics.Debug.WriteLine("[jhlee] Ignored a Sync Message!");
                                continue;
                            }
                        }
                    }

                    if (MainProcess.Instance.ProgramType == ProgramType.iCommand)
                    {
                        if (baseSyncList[i].SyncCommandType != CommonUtils.Models.Sync.SyncCommandType.MultiGrid &&
                            baseSyncList[i].SyncCommandType != CommonUtils.Models.Sync.SyncCommandType.MultiGridFullScreen &&
                            baseSyncList[i].SyncCommandType != CommonUtils.Models.Sync.SyncCommandType.MultiGridCell)
                        {
                            continue;
                        }
                    }

                    this.SyncHelper.RecvSyncDataFromServer(baseSyncList[i]);
                }
            }

            InnotiveDebug.Trace(string.Format("[TcpSync] Sync Receive End : List Size = {0}", baseSyncList.Count));
        }
    }
}