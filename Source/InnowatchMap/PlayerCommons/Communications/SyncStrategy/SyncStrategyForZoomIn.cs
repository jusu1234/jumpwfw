﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SyncStrategyForZoomIn.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Zoom In 싱크 클래스.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.PlayerCommons.Communications.SyncStrategy
{
    using System;
    using Innotive.InnoWatch.Commons.Utils;
    using Innotive.InnoWatch.CommonUtils.Log;
    using Innotive.InnoWatch.CommonUtils.Models.Sync.Control;

    /// <summary>
    /// Zoom In 싱크 클래스.
    /// </summary>
    public class SyncStrategyForZoomIn : ISyncStrategy
    {
        #region Implemented Interfaces

        #region ISyncStrategy

        /// <summary>
        /// do sync.
        /// </summary>
        /// <param name="item">
        /// The item.
        /// </param>
        public void DoSync(object item)
        {
            var data = item as ZoomInSync;
            
            if(data != null)
            {
                DoSync_Core(data);
            }
        }

        #endregion

        #region ZoomInData

        static private void DoSync_Core(ZoomInSync data)
        {
            InnotiveDebug.Trace(7, "===== DoSync() ZoomIn Rect = {0}", data.DesireRect);
            MainProcess.Instance.ProjectManager.LayoutContentsSyncManager.ZoomIn(data.LayoutGuid, Guid.Empty, InnoConvertUtil.ToRect(data.DesireRect));         
        }

        #endregion // ZoomInData

        #endregion
    }
}