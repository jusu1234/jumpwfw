﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SyncStrategyForPanningEnd.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Panning End 싱크 클래스.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.PlayerCommons.Communications.SyncStrategy
{
    using System;
    using Innotive.InnoWatch.Commons.Utils;
    using Innotive.InnoWatch.CommonUtils.Log;
    using Innotive.InnoWatch.CommonUtils.Models.Sync.Control;

    /// <summary>
    /// Panning End 싱크 클래스.
    /// </summary>
    public class SyncStrategyForPanningEnd : ISyncStrategy
    {
        #region Implemented Interfaces

        #region ISyncStrategy

        /// <summary>
        /// do sync.
        /// </summary>
        /// <param name="item">
        /// The item.
        /// </param>
        public void DoSync(object item)
        {
            var data = item as PanningEndSync;

            MainProcess.Instance.MainWindow.Dispatcher.BeginInvoke(
                new Action(() => DoSync_Core(data)));
        }        

        #region PanningEndData

        static private void DoSync_Core(PanningEndSync data)
        {
            InnotiveDebug.Trace(5, "===== DoSync() PanningEnd 호출함 !!");

            if (data.Rect != null)
            {
                MainProcess.Instance.ProjectManager.LayoutContentsSyncManager.RecvPanningEnd(data.LayoutGuid, InnoConvertUtil.ToRect(data.Rect));
            }
        }
        
        #endregion // PanningEndData

        #endregion

        #endregion
    }
}