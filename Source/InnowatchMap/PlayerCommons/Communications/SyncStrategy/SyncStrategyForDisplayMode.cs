﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SyncStrategyForDisplayMode.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Display Mode 싱크 클래스.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.PlayerCommons.Communications.SyncStrategy
{
    using System;
    using Innotive.InnoWatch.CommonUtils.Log;
    using Innotive.InnoWatch.CommonUtils.Models.Sync.Control;

    /// <summary>
    /// Display Mode 싱크 클래스.
    /// </summary>
    public class SyncStrategyForDisplayMode : ISyncStrategy
    {
        #region Implemented Interfaces

        #region ISyncStrategy

        /// <summary>
        /// do sync.
        /// </summary>
        /// <param name="item">
        /// The item.
        /// </param>
        public void DoSync(object item)
        {
            var data = item as DisplayModeSync;
            if (data != null)
            {
                MainProcess.Instance.MainWindow.Dispatcher.BeginInvoke(
                    new Action(() => DoSync_Core(data)));
            }
        }

        #endregion

        #region DisplayModeData

        static private void DoSync_Core(DisplayModeSync data)
        {
            InnotiveDebug.Trace(5, "===== DoSync() DisplayMode 호출함 !!");
            MainProcess.Instance.ProjectManager.ChangeDisplayMode(data.DisplayMode);
        }

        #endregion // DisplayModeData

        #endregion
    }
}