﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SyncStrategyForDisplayAreaInit.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   DisplayAreaInit 싱크 클래스.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.PlayerCommons.Communications.SyncStrategy
{
    using System;
    using System.Collections.Generic;
    using Innotive.InnoWatch.CommonUtils.Log;
    using Innotive.InnoWatch.CommonUtils.Models.Sync.Control;
    using Innotive.InnoWatch.PlayerCommons.Communications.Sync;

    /// <summary>
    /// DisplayAreaInit 싱크 클래스.
    /// </summary>
    public class SyncStrategyForDisplayAreaInit : ISyncStrategy
    {
        #region Implemented Interfaces

        #region ISyncStrategy

        /// <summary>
        /// do sync.
        /// </summary>
        /// <param name="item">
        /// The item.
        /// </param>
        public void DoSync(object item)
        {
            var data = item as DisplayAreaInitSync;
            MainProcess.Instance.MainWindow.Dispatcher.Invoke(
                          new Action(() => DoSync_Core(data)));
        }

        #endregion

        #endregion

        #region Private Function

        static private void DoSync_Core(DisplayAreaInitSync data)
        {
            InnotiveDebug.Trace(5, "===== DoSync() DisplayAreaInit 호출함 !!");

            // 해당 displayArea의 layout을 reload함 !!
            var openLayoutInfoList = new List<OpenLayoutInfo>();
            foreach (DisplayAreaInfo displayAreaInfo in data.DisplayAreaList)
            {
                var openLayoutInfo = new OpenLayoutInfo(displayAreaInfo.DisplayName, displayAreaInfo.LayoutGuid);
                openLayoutInfoList.Add(openLayoutInfo);
            }

            InnotiveDebug.Trace(6, "===================== DoSync() ==> Group Open Layout을 시작함 !!!");
            MainProcess.Instance.ProjectManager.DisplayAreaOpenChangeLayoutSyncManager.GroupOpenLayout(openLayoutInfoList);
            InnotiveDebug.Trace(6, "===================== DoSync() ==> Group Open Layout을 종료함 !!!");
        }

        #endregion // Private Function
    }
}