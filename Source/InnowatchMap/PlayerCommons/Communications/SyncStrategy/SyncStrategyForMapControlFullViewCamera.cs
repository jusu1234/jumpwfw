﻿
namespace Innotive.InnoWatch.PlayerCommons.Communications.SyncStrategy
{
    using Innotive.InnoWatch.CommonUtils.Log;
    using Innotive.InnoWatch.CommonUtils.Models.Sync.Control;

    /// <summary>
    /// MapControlChanged 싱크 클래스.
    /// </summary>
    public class SyncStrategyForMapControlFullViewCamera : ISyncStrategy
    {
        /// <summary>
        /// do sync.
        /// </summary>
        /// <param name="item">
        /// The item.
        /// </param>
        public void DoSync(object item)
        {
            var data = item as MapControlFullViewCameraSync;

            if (data != null)
            {
                DoSync_Core(data);
            }
        }

        static private void DoSync_Core(MapControlFullViewCameraSync data)
        {
            InnotiveDebug.Trace(7, "===== DoSync() MapControlFullViewCameraSync CameraId = {0} IsFull = {1}", data.CameraId, data.IsFull);
            MainProcess.Instance.ProjectManager.LayoutContentsSyncManager.MapControlFullViewCamera(data.MapControlGuid, data.CameraId, data.IsFull);
        }
    }
}