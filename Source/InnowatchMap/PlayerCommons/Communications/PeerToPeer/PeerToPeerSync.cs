﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PeerToPeerSync.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Defines Peer To Peer Sync Interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.PlayerCommons.Communications.PeerToPeer
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.ServiceModel;
    using System.Text;

    using Innotive.Communication.ControlServerMsg;

    [ServiceContract(Namespace = "http://schemas.innotive.com/innowatch/sync", CallbackContract = typeof(IPeerToPeerSync))]
    public interface IPeerToPeerSync
    {
        [OperationContract(IsOneWay = true)]
        void ReceivePeerToPeerSync(BaseSyncList synclist);
    }

    public interface IPeerToPeerSyncChannel : IPeerToPeerSync, IClientChannel
    {
        
    }
}
