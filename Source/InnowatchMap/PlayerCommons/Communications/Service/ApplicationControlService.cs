// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ApplicationControlService.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Defines the ApplicationControlService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.PlayerCommons.Communications.Service
{
    using System;
    using Innotive.InnoWatch.CommonUtils.Log;
    using Innotive.InnoWatch.Commons.Models;

    /// <summary>
    /// The application control service.
    /// </summary>
    public class ApplicationEventService : IApplicationEventService
    {
        /// <summary>
        /// The set grid by favorite.
        /// </summary>
        /// <param name="stageName">
        /// The stage name.
        /// </param>
        /// <param name="favoritePath">
        /// The favorite path.
        /// </param>
        public void SetGridByFavorite(string stageName, string favoritePath)
        {
            InnotiveDebug.Log.Info(string.Format("[Set Favorite] StageName : {0}, Favorite : {1}", stageName, favoritePath));
            ApplicationControlServiceEvent.InvokeEApplicationControl(new ApplicationControlEventArgs(stageName, favoritePath, "favorite"));
        }

        /// <summary>
        /// The set grid by cameras.
        /// </summary>
        /// <param name="stageName">
        /// The stage name.
        /// </param>
        /// <param name="cameraList">
        /// The cameras list.
        /// </param>
        public void SetGridByCameras(string stageName, string cameraList)
        {
            ApplicationControlServiceEvent.InvokeEApplicationControl(new ApplicationControlEventArgs(stageName, cameraList, "cameras"));
        }

        /// <summary>
        /// Raise Alarm.
        /// </summary>
        /// <param name="alarmName">Alarm Name</param>
        public void RaiseAlarm(string alarmName)
        {
            ApplicationControlServiceEvent.InvokeEApplicationControl(new ApplicationControlEventArgs(alarmName, "alarm", "alarm"));
        }

        /// <summary>
        /// ReceiveAlarm.
        /// </summary>
        /// <param name="allpendingcount">The all pending list count.</param>
        /// <param name="unconfirmpendingcount">The unconfirm pending list count.</param>
        public void ReceiveAlarm(string allpendingcount, string unconfirmpendingcount)
        {
            try
            {
                CommManager.Instance.ControlServer.RecvRequestNewAlarm(new AlarmRaisedEventParameter { IsTest = false });
            }
            catch (Exception ex)
            {
                InnotiveDebug.Log.Error(string.Format("[AlarmReceive Error] {0}", ex.Message));
            }
        }

        /// <summary>
        /// Set Fullscreen.
        /// </summary>
        /// <param name="stageName">The stage name.</param>
        public void SetFullscreen(string stageName)
        {
            ApplicationControlServiceEvent.InvokeEApplicationControl(new ApplicationControlEventArgs(stageName, "Fullscreen", "Fullscreen"));
        }

        /// <summary>
        /// Set NormalScreen.
        /// </summary>
        public void SetNormalscreen()
        {
            ApplicationControlServiceEvent.InvokeEApplicationControl(new ApplicationControlEventArgs(string.Empty, "Normalscreen", "Normalscreen"));
        }

        public void LoadLayout(string stageName, string layoutName)
        {
            ApplicationControlServiceEvent.InvokeEApplicationControl(new ApplicationControlEventArgs(stageName, layoutName, "LoadLayout"));
        }

        public void SetStageShow(string stageName, string stageShow)
        {
            ApplicationControlServiceEvent.InvokeEApplicationControl(new ApplicationControlEventArgs(stageName, stageShow, "SetStageShow"));
        }

        public void StageClear(string stageName)
        {
            ApplicationControlServiceEvent.InvokeEApplicationControl(new ApplicationControlEventArgs(stageName, "StageClear", "StageClear"));
        }
    }
}