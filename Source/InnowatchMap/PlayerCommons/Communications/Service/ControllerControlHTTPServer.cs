﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using Innotive.InnoWatch.CommonUtils.Log;

namespace Innotive.InnoWatch.PlayerCommons.Communications.Service
{
    public class ControllerControlHTTPServer
    {
        private bool serverFlag = true;


        private TcpListener tcpListener;
        private TcpClient tcpClient;
        public Thread listenThread;
        public Thread clientThread;

        private readonly byte[] responseString;

        public ControllerControlHTTPServer()
        {
            var encoding = new ASCIIEncoding();
            responseString = encoding.GetBytes("HTTP/1.1 200 OK\r\n\r\n");
        }

        public void Start(int port)
        {
            this.Stop();

            if (port > 0)
            {                
                this.serverFlag = true;

                this.tcpListener = new TcpListener(IPAddress.Any, port);
                this.listenThread = new Thread(new ThreadStart(ListenForClients));
                this.listenThread.Start();
            }
        }

        public void Stop()
        {

            if (this.tcpListener != null)
            {
                this.tcpListener.Stop();
            }
            this.serverFlag = false;
        }

        private void ListenForClients()
        {
            while (this.serverFlag)
            {
                try
                {
                    this.tcpListener.Start();
                }
                catch (Exception ex)
                {
                    InnotiveDebug.Log.Error("TcpListener Exception. ", ex);
                }

                while (this.serverFlag)
                {
                    try
                    {
                        //blocks until a client has connected to the server
                        this.tcpListener.Server.ReceiveTimeout = 1000;
                        this.tcpClient = this.tcpListener.AcceptTcpClient();
                    }
                    catch (Exception ex)
                    {
                        InnotiveDebug.Log.Error("ListenForClients Exception. ", ex);
                        this.tcpListener.Stop();
                        break;
                    }

                    //create a thread to handle communication 
                    //with connected client
                    this.clientThread = new Thread(new ParameterizedThreadStart(HandleClientComm));
                    this.clientThread.Start(this.tcpClient);
                }
            }            

            if (this.tcpClient != null)
            {
                this.tcpClient.Close();
            }
            this.tcpListener.Stop();
        }

        private void HandleClientComm(object client)
        {
            var tcpClient = (TcpClient)client;
            var clientStream = tcpClient.GetStream();

            var message = new byte[4096];

            int bytesRead;
            while (this.serverFlag)
            {
                bytesRead = 0;

                try
                {
                    bytesRead = clientStream.Read(message, 0, 4096);
                }
                catch
                {
                    break;
                }
                finally
                {
                    if (bytesRead != 0)
                    {
                        var encoder = new ASCIIEncoding();
                        ControllerControlServiceEvent.InvokeEControllerControl(new ControllerControlEventArgs(parseRequestURL(encoder.GetString(message, 0, bytesRead))));
                        clientStream.Write(responseString, 0, responseString.Length);                    }
                }
            }

            tcpClient.Close();
        }

        private string parseRequestURL(string receivedString)
        {
            Match match = Regex.Match(receivedString, @"rest/PTZControl/command\?([^ ]+) HTTP/.+", RegexOptions.IgnoreCase);

            if (match.Success)
                return match.Groups[1].Value;

            return string.Empty;
        }
    }
}