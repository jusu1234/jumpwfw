namespace Innotive.InnoWatch.PlayerCommons.Communications.Service
{
    using System.ServiceModel;
    using System.ServiceModel.Web;

    [ServiceContract]
    public interface IApplicationEventService
    {
        [OperationContract]
        [WebGet]
        void SetGridByFavorite(string stageName, string favoritePath);

        [OperationContract]
        [WebGet]
        void SetGridByCameras(string stageName, string cameraList);

        [OperationContract]
        [WebGet]
        void RaiseAlarm(string alarmName);

        [OperationContract]
        [WebGet(UriTemplate = "notifyalarm?allpendingcount={allpendingcount}&unconfirmpendingcount={unconfirmpendingcount}", RequestFormat = WebMessageFormat.Xml)]
        void ReceiveAlarm(string allpendingcount, string unconfirmpendingcount);

        [OperationContract]
        [WebGet]
        void SetFullscreen(string stageName);

        [OperationContract]
        [WebGet]
        void SetNormalscreen();

        [OperationContract]
        [WebGet]
        void LoadLayout(string stageName, string layoutName);

        [OperationContract]
        [WebGet]
        void SetStageShow(string stageName, string stageShow);

        [OperationContract]
        [WebGet]
        void StageClear(string stageName);
        
    }
}