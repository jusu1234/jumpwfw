﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Innotive.InnoWatch.PlayerCommons.Communications.Service
{
    public class ControllerControlEventArgs : EventArgs
    {
        public string RequestParam { get; set; }

        public ControllerControlEventArgs(string requestParam)
        {
            RequestParam = requestParam;
        }
    }
}
