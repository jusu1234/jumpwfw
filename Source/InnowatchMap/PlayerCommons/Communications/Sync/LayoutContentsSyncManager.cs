﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright company="Innotive Inc. Korea" file="LayoutContentsSyncManager.cs">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   The layout sync manager.
// </summary>
// 
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.PlayerCommons.Communications.Sync
{
    using System;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Media.Animation;
    using Innotive.InnoWatch.Commons.BaseControls;
    using Innotive.InnoWatch.Commons.Bookmarks;
    using Innotive.InnoWatch.Commons.GuidManagers;
    using Innotive.InnoWatch.CommonUtils.Log;
    using Innotive.InnoWatch.DLLs.LayoutControls;
    using Innotive.InnoWatch.PlayerCommons.DisplayAreas;
    using Innotive.InnoWatch.PlayerCommons.Layouts;
    using Innotive.InnoWatch.Commons.Publics;

	/// <summary>
	/// The layout sync manager.
	/// </summary>
	public class LayoutContentsSyncManager : LayoutContentManager
	{  
		#region Constructors and Destructors

		/// <summary>
		/// Initializes a new instance of the <see cref="LayoutContentsSyncManager"/> class.
		/// </summary>
		/// <param name="displayAreaManager">
		/// The display area manager.
		/// </param>
		public LayoutContentsSyncManager(PlayerDisplayAreaManager displayAreaManager)
			: base(displayAreaManager)
		{
		}

		#endregion

		#region Public Methods

	    /// <summary>
	    /// The element mouse over.
	    /// </summary>
	    /// <param name="elementGuid">
	    /// The element guid.
	    /// </param>
	    /// <param name="isMouseOver">
	    /// The is mouse over.
	    /// </param>
	    public void ElementMouseOver(Guid elementGuid, bool isMouseOver)
		{
			var layout = base.GetOwnerLayoutControl(elementGuid);

			// Console인 경우 외부로 Data를 보냄 !!
			// NOTE : 현재 아무것도 하지 않음. 최종단에서 주석처리 됨. (by jhlee)

            //현재 외부 Sync를 보낼수 있는 상태인지 확인 (SyncToggleButton 활성화 여부 등등)
            if (PlayingProcess.CanSendRemoteSync(layout))
                MainProcess.Instance.CommManager.ControlServer.SyncHelper.SendElementMouseOver(layout.SyncGUID, elementGuid, isMouseOver);
		}

		public void GoToLocation(
			Guid layoutGuid,
			Guid bookmarkGuid,
			string effect,
			double startTime,
			double duration,
			KeySpline keySpline)
		{
			var layoutControlList = MainProcess.Instance.ProjectManager.PlayerLayoutManager.GetLayoutControlList(layoutGuid);
			if (layoutControlList.Count < 1)
			{
				return;
			}

			//by blackRoot : Dispose를 했지만 LayoutList에 남아있는 layout이 존재하는 경우 0번 index의 layout을 가져오면 안됨 !! 
			//이미 dispose를 했기 때문에 bookmark가 null로 나옴 !!

			var bookmark = MainProcess.Instance.ProjectManager.BookmarkManager.GetBookmark(bookmarkGuid);

			if (bookmark == null)
			{
				InnotiveDebug.Log.Error("GoToLocation 종료. Bookmark를 못 찾음. bookmark guid = " + bookmarkGuid.ToString());
				return;
			}

			// 내부 Sync 진행함 !!
			base.GoToLocation(bookmark, effect, startTime, duration, keySpline);
		}

        public void GoToObject(
            Guid elementGuid,
            string effect,
            double startTime,
            double duration,
            double margin,
            KeySpline keySpline)
        {
            var elementList = GuidManager.GetSyncObjectList(elementGuid);
            if (elementList == null || elementList.Count <= 0)
                return;

            var firstControl = elementList[0] as BaseControl;
            if (firstControl == null)
                return;

            var parentLayoutControl = firstControl.ParentBaseLayoutControl as LayoutControl;
            if (parentLayoutControl == null)
                return;

            //GotoLocation이 bookmark를 인자로 받기 때문에 임시 bookmark를 생성해서 넘겨줌 !!
            var bookmark = new DataBookmark();
            var bookmarkItem = new DataBookmarkItem {DestLayoutGUID = parentLayoutControl.SyncGUID};


            //Object가 화면을 벗어나지 않는 기준을 정함 (넓이를 기준으로 할지, 높이를 기준으로 할지 결정)
            bool isApplyWidth;
            var heightZoomRatio = firstControl.Height / parentLayoutControl.OriginHeight;
            var heightRatioWidth = parentLayoutControl.OriginWidth * heightZoomRatio;
            
            var zoomRatio = 1.0;
            if (heightRatioWidth > firstControl.Width)
                isApplyWidth = false;
            else
                isApplyWidth = true;

            //Camera 바깥쪽 마진 설정 (%를 double값으로 변경함)
            //아래쪽 margin적용을 Rect의 반을 가지고 했기 때문에 실제 margin에 *2 를 해줌
            margin = margin * 0.01 * 2;

            double left, top, width, height;

            if (isApplyWidth)
            {
                double viewWidth = firstControl.Width * (1 + margin * 2);
                zoomRatio = parentLayoutControl.OriginWidth / viewWidth;

                width = parentLayoutControl.OriginWidth * zoomRatio;
                height = parentLayoutControl.OriginHeight * zoomRatio;

                //margin을 적용함 !! 가운데에 위치함 !!
                left = -(Canvas.GetLeft(firstControl) * zoomRatio) + (firstControl.Width * margin * zoomRatio);

                //top을 현재 화면에 보이는 영역의 중간으로 위치하게 함 !!
                double viewHeight = (parentLayoutControl.OriginHeight * viewWidth / parentLayoutControl.OriginWidth);
                double realHeight = firstControl.Height;

                top = -(Canvas.GetTop(firstControl) * zoomRatio);
                top += (viewHeight - realHeight) / 2 * zoomRatio;
            }
            else
            {
                double viewHeight = firstControl.Height * (1 + margin * 2);
                zoomRatio = parentLayoutControl.OriginHeight / viewHeight;

                width = parentLayoutControl.OriginWidth * zoomRatio;
                height = parentLayoutControl.OriginHeight * zoomRatio;

                //margin을 적용함 !! 가운데에 위치함 !!
                top = -(Canvas.GetTop(firstControl) * zoomRatio) + (firstControl.Height * margin * zoomRatio);

                //left를 현재 화면에 보이는 영역의 중간으로 위치하게 함 !!
                double viewWidth = (parentLayoutControl.OriginWidth * viewHeight / parentLayoutControl.OriginHeight);
                double realWidth = firstControl.Width;

                left = -(Canvas.GetLeft(firstControl) * zoomRatio);
                left += (viewWidth - realWidth) / 2 * zoomRatio;
            }

            bookmarkItem.ZoomRect = new Rect(left, top, width, height);

            bookmark.BookmarkItems.Add(bookmarkItem);

            // 내부 Sync 진행함 !!
            base.GoToLocation(bookmark, effect, startTime, duration, keySpline);
        }

	    /// <summary>
	    /// The panning start.
	    /// </summary>
	    /// <param name="layoutControl">
	    /// The layout.
	    /// </param>
	    public void PanningStart(LayoutControl layoutControl)
        {
            // 내부 Sync 진행함 !!
            base.PanningStart(layoutControl.SyncGUID);

            // 외부 Sync 는 사용하고 있지 않아서 삭제했음. ( SettingRect 나 End 만으로도 충분히 효과가 있어서 불필요할듯. )
        }

	    /// <summary>
	    /// The panning end.
	    /// </summary>
	    public void PanningEnd(LayoutControl layoutControl)
		{
			// 내부 Sync 진행함 !!
			base.PanningEnd(layoutControl.SyncGUID);

            var ownerLayout = base.GetOwnerLayoutControl(layoutControl.SyncGUID);
            
            //현재 외부 Sync를 보낼수 있는 상태인지 확인 (SyncToggleButton 활성화 여부 등등)
            if (PlayingProcess.CanSendRemoteSync(layoutControl))
                MainProcess.Instance.CommManager.ControlServer.SyncHelper.SendPanningEnd(ownerLayout.SyncGUID, layoutControl.SyncGUID, layoutControl.LayoutActualRect);
		}

	    /// <summary>
	    /// The setting rect.
	    /// </summary>
	    /// <param name="layoutControl">
	    /// The layout control.
	    /// </param>
	    /// <param name="rect">
	    /// The rect.
	    /// </param>
	    public void SettingRect(LayoutControl layoutControl, Rect rect)
		{
            bool isSendToSyncServer = true;
            if (!PlayingProcess.CanSendRemoteSync(layoutControl))
                isSendToSyncServer = false;

			SettingRect(layoutControl.SyncGUID, layoutControl.OriginGUID, rect, isSendToSyncServer);
		}

	    /// <summary>
	    /// The setting rect.
	    /// </summary>
	    /// <param name="layoutSyncGuid">
	    /// The layout sync guid.
	    /// </param>
	    /// <param name="layoutOriginGuid">
	    /// The layout origin guid.
	    /// </param>
	    /// <param name="rect">
	    /// The rect.
	    /// </param>
	    public new void SettingRect(Guid layoutSyncGuid, Guid layoutOriginGuid, Rect rect, bool isSendToSyncServer = true)
		{
	        try
	        {
                // 내부 Sync
                base.SettingRect(layoutSyncGuid, layoutOriginGuid, rect);

                var ownerLayoutControl = base.GetOwnerLayoutControl(layoutSyncGuid);
                if (ownerLayoutControl == null)
                    return;

                // 외부 Sync
                if (MainProcess.Instance.CommManager.ControlServer == null)
                {
                    return;
                }

                if (MainProcess.Instance.CommManager.ControlServer.SyncHelper == null)
                {
                    return;
                }

                //현재 외부 Sync를 보낼수 있는 상태인지 확인 (SyncToggleButton 활성화 여부 등등)
                if (isSendToSyncServer)
                    MainProcess.Instance.CommManager.ControlServer.SyncHelper.SendSettingRect(ownerLayoutControl.SyncGUID, layoutSyncGuid, rect);
	        }
	        catch (Exception ex)
	        {
	            InnotiveDebug.Log.Error("(Layout Sync Error)", ex);
	        }
		}

	    /// <summary>
	    /// The zoom in.
	    /// Layout Mouse Click시 발생하는 ZoomIn
	    /// </summary>
	    /// <param name="layoutControl">
	    /// The layout control.
	    /// </param>
	    /// <param name="desireRect">
	    /// The desire rect.
	    /// </param>
	    public void ZoomIn(LayoutControl layoutControl, Rect desireRect)
		{
			// 내부 Sync 진행함 !!
			base.ZoomIn(layoutControl.SyncGUID, layoutControl.OriginGUID, desireRect);

			var ownerLayoutControl = base.GetOwnerLayoutControl(layoutControl);
			if (ownerLayoutControl == null)
			{
				InnotiveDebug.Trace(4, "<PlayerCommons> ZoomIn() owner layout control이 존재하지 않습니다");
				return;
			}

			InnotiveDebug.Trace(6, "layoutControlName = {0}, owner layoutControlName = {1}", layoutControl.Name, ownerLayoutControl.Name);

            // 외부 Sync
            //현재 외부 Sync를 보낼수 있는 상태인지 확인 (SyncToggleButton 활성화 여부 등등)
            if (PlayingProcess.CanSendRemoteSync(layoutControl))
                MainProcess.Instance.CommManager.ControlServer.SyncHelper.SendZoomIn(ownerLayoutControl.SyncGUID, layoutControl.SyncGUID, desireRect);
		}

        public void MapControlZoomChanged(Guid mapControlSyncGuid, double zoomLevel, double centerLatitude, double centerLongitude)
        {
            if (Public.GetProgramType() != CommonUtils.Models.ProgramType.iCommand)
            {
                // 내부 Sync
                this.MapControlZoomChanged(mapControlSyncGuid, zoomLevel);
                this.MapControlPositionChanged(mapControlSyncGuid, centerLatitude, centerLongitude);
            }

            if (Public.GetProgramType() == CommonUtils.Models.ProgramType.iCommand)
            {
                // 외부 Sync
                //MainProcess.Instance.CommManager.ControlServer.SyncHelper.SendMapControlZoomChanged(mapControlSyncGuid, zoomLevel);
                MainProcess.Instance.CommManager.ControlServer.SyncHelper.SendMapControlZoomChanged(mapControlSyncGuid, zoomLevel, centerLatitude, centerLongitude);
            }
            
        }

        //public new void MapControlZoomChanged(Guid mapControlSyncGuid, double zoomLevel)
        //{
        //    // 내부 Sync
        //    base.MapControlZoomChanged(mapControlSyncGuid, zoomLevel);

        //    // 외부 Sync
        //    //MainProcess.Instance.CommManager.ControlServer.SyncHelper.SendMapControlZoomChanged(mapControlSyncGuid, zoomLevel);
        //    MainProcess.Instance.CommManager.ControlServer.SyncHelper.SendMapControlZoomChanged(mapControlSyncGuid, zoomLevel, );
        //}

        public new void MapControlPositionChanged(Guid mapControlSyncGuid, double mapLatitude, double mapLongtitude)
        {
            // 내부 Sync
            base.MapControlPositionChanged(mapControlSyncGuid, mapLatitude, mapLongtitude);

            // 외부 Sync 없음 !! ( 변경중일때는 외부 싱크는 보내지 않고 끝났을때만 보냄. )
        }


        public void MapControlPositionChangeEnd(Guid mapControlSyncGuid, double mapLatitude, double mapLongtitude)
        {
            // 내부 Sync
            base.MapControlPositionChanged(mapControlSyncGuid, mapLatitude, mapLongtitude);

            // 외부 Sync
            MainProcess.Instance.CommManager.ControlServer.SyncHelper.SendMapControlPositionChangeEnd(mapControlSyncGuid, mapLatitude, mapLongtitude);
        }

        public new void MapControlFullViewCamera(Guid mapControlSyncGuid, string cameraId, bool isFull)
        {
            // 내부 Sync
            base.MapControlFullViewCamera(mapControlSyncGuid, cameraId, isFull);

            // 외부 Sync
            MainProcess.Instance.CommManager.ControlServer.SyncHelper.SendMapControlFullViewCamera(mapControlSyncGuid, cameraId, isFull);
        }

	    /// <summary>
	    /// The zoom in.
	    /// 서버에서 싱크 Message를 받아서 처리하는 ZoomIn
	    /// </summary>
	    /// <param name="layoutSyncGuid">
	    /// The layout sync guid.
	    /// </param>
	    /// <param name="layoutOriginGuid">
	    /// The layout origin guid.
	    /// </param>
	    /// <param name="desireRect">
	    /// The desire rect.
	    /// </param>
	    public new void ZoomIn(Guid layoutSyncGuid, Guid layoutOriginGuid, Rect desireRect)
		{
			// 내부 Sync 진행함 !!
			base.ZoomIn(layoutSyncGuid, layoutOriginGuid, desireRect);
		}

	    /// <summary>
	    /// The zoom in.
	    /// </summary>
	    /// <param name="layoutSyncGuid">
	    /// The layout sync guid.
	    /// </param>
	    /// <param name="percentage">
	    /// The percentage.
	    /// </param>
	    /// <param name="duration">
	    /// The duration.
	    /// </param>
	    public new void ZoomIn(Guid layoutSyncGuid, double percentage, double duration)
		{
			var desireRect = base.ZoomIn(layoutSyncGuid, percentage, duration);
			if (desireRect == Rect.Empty)
			{
				return;
			}
		}

	    /// <summary>
	    /// The zoom out.
	    /// </summary>
	    /// <param name="layoutControl">
	    /// The layout control.
	    /// </param>
	    /// <param name="desireRect">
	    /// The desire rect.
	    /// </param>
	    public void ZoomOut(LayoutControl layoutControl, Rect desireRect)
		{
			// 내부 Sync 진행함 !!
			base.ZoomOut(layoutControl.SyncGUID, layoutControl.OriginGUID, desireRect);

			var ownerLayoutControl = base.GetOwnerLayoutControl(layoutControl);
			if (ownerLayoutControl == null)
			{
				InnotiveDebug.Trace(4, "<PlayerCommons> ZoomOut() owner layout control이 존재하지 않습니다");
				return;
			}

            //현재 외부 Sync를 보낼수 있는 상태인지 확인 (SyncToggleButton 활성화 여부 등등)
            if (PlayingProcess.CanSendRemoteSync(layoutControl))
                MainProcess.Instance.CommManager.ControlServer.SyncHelper.SendZoomOut(ownerLayoutControl.SyncGUID, layoutControl.SyncGUID, desireRect);
		}

	    /// <summary>
	    /// The zoom out.
	    /// </summary>
	    /// <param name="layoutSyncGuid">
	    /// The layout sync guid.
	    /// </param>
	    /// <param name="layoutOriginGuid">
	    /// The layout origin guid.
	    /// </param>
	    /// <param name="desireRect">
	    /// The desire rect.
	    /// </param>
	    public new void ZoomOut(Guid layoutSyncGuid, Guid layoutOriginGuid, Rect desireRect)
		{
			// 내부 Sync 진행함 !!
			base.ZoomOut(layoutSyncGuid, layoutOriginGuid, desireRect);
		}

	    /// <summary>
	    /// The zoom out.
	    /// </summary>
	    /// <param name="layoutSyncGuid">
	    /// The layout sync guid.
	    /// </param>
	    /// <param name="percentage">
	    /// The percentage.
	    /// </param>
	    /// <param name="duration">
	    /// The duration.
	    /// </param>
	    public new void ZoomOut(Guid layoutSyncGuid, double percentage, double duration)
		{
			var desireRect = base.ZoomOut(layoutSyncGuid, percentage, duration);
			if (desireRect == Rect.Empty)
			{
				return;
			}
		}

	    /// <summary>
	    /// Run Action XamlControlStoryboard.
	    /// </summary>
	    /// <param name="targetLayoutGuid">
	    /// The target layout guid.
	    /// </param>
	    /// <param name="targetElementGuid">
	    /// The target element guid.
	    /// </param>
	    /// <param name="targetElementStoryboardName">
	    /// The target element storyboard name.
	    /// </param>
	    public new void XamlControlStoryboard(Guid targetLayoutGuid, Guid targetElementGuid, string targetElementStoryboardName)
		{
			// 내부 액션
			base.XamlControlStoryboard(targetLayoutGuid, targetElementGuid, targetElementStoryboardName);
		}

		#endregion        
	}
}