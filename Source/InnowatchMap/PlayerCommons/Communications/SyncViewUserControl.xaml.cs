﻿
namespace Innotive.InnoWatch.PlayerCommons.Communications
{
    using System.Windows;
    using System.Windows.Controls;

    /// <summary>
    /// Interaction logic for SyncViewUserControl.xaml
    /// </summary>
    public partial class SyncViewUserControl : UserControl
    {
        public SyncViewUserControl()
        {
            InitializeComponent();

            this.Loaded += this.SyncViewUserControl_Loaded;
            this.Unloaded += this.SyncViewUserControl_Unloaded;
        }

        private void SyncViewUserControl_Loaded(object sender, RoutedEventArgs e)
        {
            this.xSyncListView.ItemsSource = CommManager.Instance.Syncss;

            CommManager.Instance.StartSyncMonitor();
        }

        private void SyncViewUserControl_Unloaded(object sender, RoutedEventArgs e)
        {
            CommManager.Instance.StopSyncMonitor();
        }
    }
}
