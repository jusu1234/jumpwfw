﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Innotive.Communication.ControlServerMsg;
using Innotive.InnoWatch.Commons.Publics;
using Innotive.InnoWatch.PlayerCommons.Communications.Models;
using log4net;
using System.Globalization;

namespace Innotive.InnoWatch.PlayerCommons.Communications.NetworkCore
{
    public class SyncNetworkBase
    {
        public static readonly string COMMAND_SYNCLIST      = "SYNCLIST";
        public static readonly string COMMAND_CUSTOM        = "CUSTOMPK";
        
        public static readonly int COMMAND_SIZE   = 8;
        public static readonly int TIMESTR_SIZE   = 23;
        public static readonly int LENGTH_SIZE    = 4;

        public static readonly int HEADER_SIZE = COMMAND_SIZE + TIMESTR_SIZE + LENGTH_SIZE;

        public static ClientIDData GetClientIDData(Public.ProgramType type)
        {
            return ClientIDData.MakeInstance(type);
        }

        public static Encoding Encoding = Encoding.ASCII;

        public static byte[] ByteArrayCopyMemory(byte [] src, int startindex, int count)
        {
            byte [] result = new byte[count];

            for (int i = startindex; i < startindex+count; i++)
            {
                result[i-startindex] = src[i];
            }

            return result;
        }

        private static string timeFormatStr = "yyyy/MM/dd-HH:mm:ss:fff";

        public static string GetTimeString(DateTime datetime)
        {
            CultureInfo provider = CultureInfo.InvariantCulture;
            string result = datetime.ToString(timeFormatStr, provider);

            if (result.Length != TIMESTR_SIZE)
            {
                System.Diagnostics.Debug.WriteLine("[AppSyncNetwork] TimeString Length and const value TIMESTR_SIZE is diffirent");
            }

            return result;
        }

        public static DateTime GetDateTime(string timeString)
        {
            DateTime result = DateTime.MinValue;
            CultureInfo provider = CultureInfo.InvariantCulture;
            try
            {
                result = DateTime.ParseExact(timeString, timeFormatStr, provider);
            }
            catch
            {
            }
            return result;
        }
    }

    public class SyncLogWriter
    {
        static public string MakeLogString(string kind, string msg)
        {
            return "[" + kind + "];" + msg + ";" +
                   DateTime.Now.ToLongDateString() + ":" +
                   DateTime.Now.ToLongTimeString() + ":" +
                   DateTime.Now.Millisecond.ToString();
        }
        static public ILog GetLog()
        {
            return MainProcess.Log;
        }

    }
}
