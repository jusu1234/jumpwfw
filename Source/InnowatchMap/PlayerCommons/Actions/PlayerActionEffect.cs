// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PlayerActionEffect.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   player action effect.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.PlayerCommons.Actions
{
    using System;
    using System.Windows;
    using System.Windows.Media;
    using System.Windows.Media.Animation;

    using Innotive.InnoWatch.Commons.Actions;
    using Innotive.InnoWatch.Commons.BaseControls;
    using Innotive.InnoWatch.Commons.EventArguments;

    /// <summary>
    /// player action effect.
    /// </summary>
    public class PlayerActionEffect : BaseActionEffect
    {
        #region Constants and Fields

        public static readonly RoutedEvent eColorEffectCompletedEvent;

        public static readonly RoutedEvent eDoubleEffectCompletedEvent;

        public static readonly RoutedEvent eSolidColorBrushEffectCompletedEvent;

        public static readonly RoutedEvent eZoomRectEffectCompletedEvent;

        private FrameworkElement _colorElement;

        private FrameworkElement _doubleElement;

        private FrameworkElement _solidColorBrushElement;

        private Storyboard _storyBoardColor; // color 형 property

        private Storyboard _storyBoardDouble; // double 형 property

        private Storyboard _storyBoardSolidColorBrush; // solid color brush 형 property

        private Storyboard _storyBoardZoomRect; // Zoom/Panning

        private FrameworkElement _zoomRectElement;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes static members of the <see cref="PlayerActionEffect"/> class.
        /// </summary>
        static PlayerActionEffect()
        {
            eZoomRectEffectCompletedEvent = EventManager.RegisterRoutedEvent(
                "eZoomRectEffectCompleted", RoutingStrategy.Bubble, typeof(EventHandler), typeof(PlayerActionEffect));
            eDoubleEffectCompletedEvent = EventManager.RegisterRoutedEvent(
                "eDoubleEffectCompleted",
                RoutingStrategy.Bubble,
                typeof(EffectCompletedRoutedEventHandler),
                typeof(PlayerActionEffect));
            eSolidColorBrushEffectCompletedEvent = EventManager.RegisterRoutedEvent(
                "eSolidColorBrushEffectCompleted",
                RoutingStrategy.Bubble,
                typeof(EffectCompletedRoutedEventHandler),
                typeof(PlayerActionEffect));
            eColorEffectCompletedEvent = EventManager.RegisterRoutedEvent(
                "eColorEffectCompleted", RoutingStrategy.Bubble, typeof(EventHandler), typeof(PlayerActionEffect));
        }

        #endregion

        #region Events

        /// <summary>
        /// e color effect completed.
        /// </summary>
        public event EventHandler eColorEffectCompleted
        {
            add
            {
                this.AddHandler(eColorEffectCompletedEvent, value);
            }

            remove
            {
                this.RemoveHandler(eColorEffectCompletedEvent, value);
            }
        }

        /// <summary>
        /// e double effect completed.
        /// </summary>
        public event EffectCompletedRoutedEventHandler eDoubleEffectCompleted
        {
            add
            {
                this.AddHandler(eDoubleEffectCompletedEvent, value);
            }

            remove
            {
                this.RemoveHandler(eDoubleEffectCompletedEvent, value);
            }
        }

        /// <summary>
        /// e solid color brush effect completed.
        /// </summary>
        public event EffectCompletedRoutedEventHandler eSolidColorBrushEffectCompleted
        {
            add
            {
                this.AddHandler(eSolidColorBrushEffectCompletedEvent, value);
            }

            remove
            {
                this.RemoveHandler(eSolidColorBrushEffectCompletedEvent, value);
            }
        }

        /// <summary>
        /// e zoom rect effect completed.
        /// </summary>
        public event EventHandler eZoomRectEffectCompleted
        {
            add
            {
                this.AddHandler(eZoomRectEffectCompletedEvent, value);
            }

            remove
            {
                this.RemoveHandler(eZoomRectEffectCompletedEvent, value);
            }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// begin double effect.
        /// </summary>
        /// <param name="element">
        /// The element.
        /// </param>
        /// <param name="property">
        /// The property.
        /// </param>
        /// <param name="newValue">
        /// The new value.
        /// </param>
        /// <param name="startTime">
        /// The start time.
        /// </param>
        /// <param name="duration">
        /// The duration.
        /// </param>
        public void BeginDoubleEffect(
            FrameworkElement element, DependencyProperty property, double newValue, double startTime, double duration)
        {
            if (element == null)
            {
                return;
            }

            if (this._storyBoardDouble != null)
            {
                this.ReleaseStoryBoard(this._storyBoardDouble, this._doubleElement);
                this._storyBoardDouble = null;
            }

            string registerName;
            if (string.IsNullOrEmpty(element.Name))
            {
                registerName = "DoubleEffect";
            }
            else
            {
                registerName = element.Name;
            }

            NameScope.SetNameScope(element, new NameScope());
            element.RegisterName(registerName, element);

            this._storyBoardDouble = new Storyboard();
            this._storyBoardDouble.Completed += this.doubleEffect_Completed;

            var keyFrame = new LinearDoubleKeyFrame
                               {
                                   Value = newValue, 
                                   KeyTime = KeyTime.FromTimeSpan(TimeSpan.FromSeconds(duration))
                               };

            var animation = new DoubleAnimationUsingKeyFrames
                                {
                                    BeginTime = TimeSpan.FromSeconds(startTime)
                                };

            animation.KeyFrames.Add(keyFrame);
            animation.FillBehavior = FillBehavior.HoldEnd;

            Storyboard.SetTargetName(animation, registerName);
            Storyboard.SetTargetProperty(animation, new PropertyPath(property));

            this._storyBoardDouble.Children.Add(animation);
            this._doubleElement = element;
            
            //Layout의 Double Animation인 경우 실행하기 전 모든 Camera를 멈춤 !! 마지막 화면이 표출됨 !!
            if (element is BaseLayoutControl)
            {
                Commons.Utils.ZoomPanSmoothManager.Start(element);
            }           

            this._storyBoardDouble.Begin(element);
        }

        /// <summary>
        /// begin solid color brush effect.
        /// </summary>
        /// <param name="element">
        /// The element.
        /// </param>
        /// <param name="property">
        /// The property.
        /// </param>
        /// <param name="newValue">
        /// The new value.
        /// </param>
        /// <param name="startTime">
        /// The start time.
        /// </param>
        /// <param name="duration">
        /// The duration.
        /// </param>
        public void BeginSolidColorBrushEffect(
            FrameworkElement element,
            DependencyProperty property,
            SolidColorBrush newValue,
            double startTime,
            double duration)
        {
            if (element == null)
            {
                return;
            }

            NameScope.SetNameScope(element, new NameScope());
            element.RegisterName("SolidColorBrushEffect", element);

            // 여기서 생성한 Brush를 control에 다시 대입하고 Brush의 Color에 Animation을 줌 !!
            var animationBrush = new SolidColorBrush();

            if (element is BaseLayoutControl)
            {
                var originBrush = (element as BaseLayoutControl).GetValue(property) as SolidColorBrush;
                animationBrush.Color = originBrush.Color;
                (element as BaseLayoutControl).SetValue(property, animationBrush);
            }
            else if (element is BaseControl)
            {
                var originBrush = (element as BaseControl).GetValue(property) as SolidColorBrush;

                animationBrush.Color = originBrush.Color;

                (element as BaseControl).SetValue(property, animationBrush);
            }

            var animation = new ColorAnimation
                                {
                                    BeginTime = TimeSpan.FromSeconds(startTime),
                                    From = animationBrush.Color,
                                    To = newValue.Color,
                                    Duration = new Duration(TimeSpan.FromSeconds(duration))
                                };

            this._solidColorBrushElement = element;
            animationBrush.BeginAnimation(SolidColorBrush.ColorProperty, animation);
        }

        /// <summary>
        /// release.
        /// </summary>
        public void Release()
        {
            this.ReleaseStoryBoard(this._storyBoardZoomRect, this._zoomRectElement);
            this.ReleaseStoryBoard(this._storyBoardDouble, this._doubleElement);
            this.ReleaseStoryBoard(this._storyBoardSolidColorBrush, this._solidColorBrushElement);
            this.ReleaseStoryBoard(this._storyBoardColor, this._colorElement);

            this._storyBoardZoomRect = null;
            this._storyBoardDouble = null;
            this._storyBoardSolidColorBrush = null;
            this._storyBoardColor = null;

            this._zoomRectElement = null;
            this._doubleElement = null;
            this._solidColorBrushElement = null;
            this._colorElement = null;
        }

        #endregion

        #region Methods

        private void doubleEffect_Completed(object sender, EventArgs e)
        {
            //Layout의 DoubleAnimation이 완료된 경우 Camera를 재시작함 !!
            if (this._doubleElement is BaseLayoutControl)
            {
                Commons.Utils.ZoomPanSmoothManager.End(this);
            }

            this.RaiseEvent(new EffectCompletedRoutedEventArgs(eDoubleEffectCompletedEvent, this, this._doubleElement));
        }

        private void ReleaseStoryBoard(Storyboard storyBoard, FrameworkElement storyBoardElement)
        {
            if (storyBoard != null && storyBoardElement != null)
            {
                storyBoard.Stop(storyBoardElement);
                storyBoard.Children.Clear();
            }
        }

        #endregion
    }
}