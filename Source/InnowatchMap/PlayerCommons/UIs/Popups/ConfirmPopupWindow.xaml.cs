﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ConfirmPopupWindow.xaml.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Interaction logic for ConfirmPopupWindow.xaml.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.PlayerCommons.UIs.Popups
{
    using System;
    using System.Timers;
    using System.Windows;
    using System.Windows.Threading;

    /// <summary>
    /// Interaction logic for ConfirmPopupWindow.xaml.
    /// </summary>
    public partial class ConfirmPopupWindow : Window
    {
        private readonly Timer _autoCloseTimer = null;

        private readonly long _windowOpenTime = 0;
        private readonly double _closeTime = 0;

        /// <summary>
        /// Initializes a new instance of the <see cref="ConfirmPopupWindow"/> class.
        /// </summary>
        /// <param name="title">
        /// The title.
        /// </param>
        /// <param name="message">
        /// The message.
        /// </param>
        public ConfirmPopupWindow(string title, string message)
        {
            InitializeComponent();

            this.xTitleTextBlock.Text = title;
            this.xMessageTextBlock.Text = message;
            this.xConfirmButtom.Click += this.xConfirmButtom_Click;
            this.xCloseButton.Click += this.xCloseButton_Click;

            this.MouseLeftButtonDown += this.ConfirmPopupWindow_MouseLeftButtonDown;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ConfirmPopupWindow"/> class.
        /// </summary>
        /// <param name="title">
        /// The title.
        /// </param>
        /// <param name="message">
        /// The message.
        /// </param>
        /// <param name="closeInterval">
        /// The close interval.
        /// </param>
        public ConfirmPopupWindow(string title, string message, double closeInterval)
            : this(title, message)
        {
            this._closeTime = closeInterval;
            this._windowOpenTime = this.ExtractMilliSeconds(System.DateTime.Now);

            this._autoCloseTimer = new Timer();
            this._autoCloseTimer.Interval = 1000;
            this._autoCloseTimer.Elapsed += new ElapsedEventHandler(this.CloseTimer_Tick);
            this._autoCloseTimer.Start();
        }

        /// <summary>
        /// Show Auto Close Confirm Popup Window.
        /// </summary>
        /// <param name="title">
        /// The title.
        /// </param>
        /// <param name="message">
        /// The message.
        /// </param>
        /// <param name="closeInterval">
        /// The close interval.
        /// </param>
        /// <returns>
        /// return DialogResult.
        /// </returns>
        public static bool? Show(string title, string message, double closeInterval)
        {
            var win = new ConfirmPopupWindow(title, message, closeInterval);

            return win.ShowDialog();
        }

        /// <summary>
        /// Normal Show Confirm Popup Window.
        /// </summary>
        /// <param name="title">
        /// The title.
        /// </param>
        /// <param name="message">
        /// The message.
        /// </param>
        /// <returns>
        /// return DialogResult.
        /// </returns>
        public static bool? Show(string title, string message)
        {
            var win = new ConfirmPopupWindow(title, message);

            return win.ShowDialog();
        }

        private long ExtractMilliSeconds(DateTime dateTime)
        {
            return (dateTime.Ticks - 621355968000000000) / 10000;
        }

        private void CloseTimer_Tick(object sender, ElapsedEventArgs e)
        {
            long now = this.ExtractMilliSeconds(System.DateTime.Now);

            if (this._closeTime + this._windowOpenTime < now)
            {
                this._autoCloseTimer.Elapsed -= new ElapsedEventHandler(this.CloseTimer_Tick);
                this._autoCloseTimer.Stop();

                Dispatcher.Invoke(DispatcherPriority.Normal, new Action(delegate { this.Close(); }));
            }
        }

        private void ConfirmPopupWindow_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            this.DragMove();
        }

        private void xCloseButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void xConfirmButtom_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}