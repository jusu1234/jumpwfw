﻿
namespace Innotive.InnoWatch.PlayerCommons.UIs.Controls
{
    using System;
    using System.Windows.Controls;


    /// <summary>
    /// Interaction logic for DisconnectIconsControl.xaml
    /// </summary>
    public partial class DisconnectIconsControl : UserControl
    {
        private bool use = false;
        private bool cameraControllerConnected = false;
        private bool contentSyncConnected = false;
        private bool controlDataConnected = false;

        public DisconnectIconsControl()
        {
            InitializeComponent();

            this.Refresh();
        }

        /// <summary>
        /// 컨트롤 사용 여부
        /// </summary>
        public bool Use
        {
            set
            {
                this.use = value;

                this.Refresh();
            }

            get
            {
                return this.use;
            }
        }

        /// <summary>
        /// Camera Controller 연결 여부
        /// </summary>
        public bool CameraControllerConnected
        {
            set
            {
                this.cameraControllerConnected = value;

                if (value)
                {
                    this.Dispatcher.BeginInvoke(new Action(() =>
                    {
                        this.xCameraControllerIcon.Visibility = System.Windows.Visibility.Collapsed;
                    }));
                }
                else
                {
                    this.Dispatcher.BeginInvoke(new Action(() =>
                    {
                        this.xCameraControllerIcon.Visibility = System.Windows.Visibility.Visible;
                    }));
                }

                this.Refresh();
            }

            get
            {
                return this.cameraControllerConnected;
            }
        }

        /// <summary>
        /// 컨텐츠 싱크 서버 연결 여부
        /// </summary>
        public bool ContentSyncConnected
        {
            set
            {
                this.contentSyncConnected = value;

                if (value)
                {
                    this.Dispatcher.BeginInvoke(new Action(() =>
                    {
                        this.xContentSyncIcon.Visibility = System.Windows.Visibility.Collapsed;
                    }));
                }
                else
                {
                    this.Dispatcher.BeginInvoke(new Action(() =>
                    {
                        this.xContentSyncIcon.Visibility = System.Windows.Visibility.Visible;
                    }));
                }

                this.Refresh();
            }

            get
            {
                return this.contentSyncConnected;
            }
        }

        /// <summary>
        /// 컨트롤 데이터 서버 연결 여부
        /// </summary>
        public bool ControlDataConnected
        {
            set
            {
                this.controlDataConnected = value;

                if (value)
                {
                    this.Dispatcher.BeginInvoke(new Action(() =>
                    {
                        this.xControlDataIcon.Visibility = System.Windows.Visibility.Collapsed;
                    }));
                }
                else
                {
                    this.Dispatcher.BeginInvoke(new Action(() =>
                    {
                        this.xControlDataIcon.Visibility = System.Windows.Visibility.Visible;
                    }));
                }

                this.Refresh();
            }

            get
            {
                return this.controlDataConnected;
            }
        }

        /// <summary>
        /// 새로 고침
        /// </summary>
        private void Refresh()
        {
            if (this.Use)
            {
                if (this.CameraControllerConnected &&
                this.contentSyncConnected &&
                this.ControlDataConnected)
                {
                    this.Dispatcher.BeginInvoke(new Action(() =>
                    {
                        this.Visibility = System.Windows.Visibility.Collapsed;
                    }));

                }
                else
                {
                    this.Dispatcher.BeginInvoke(new Action(() =>
                    {
                        this.Visibility = System.Windows.Visibility.Visible;
                    }));
                }
            }
            else
            {
                this.Dispatcher.BeginInvoke(new Action(() =>
                {
                    this.Visibility = System.Windows.Visibility.Collapsed;
                }));
            }
        }
    }
}
