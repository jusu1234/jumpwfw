﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PlayerCommonUseHardwareAccelerationNode.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Defines the PlayerCommonUseHardwareAccelerationNode type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.PlayerCommons.Configs.DeviceNodes.GraphicNodes
{
    using Commons.Configs;
    using System.Xml.Serialization;

    /// <summary>
    /// 하드웨어 가속을 사용할지 설정한다.
    /// 기본값: true.
    /// </summary>
    public class PlayerCommonUseHardwareAccelerationNode : ConvertBoolean
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PlayerCommonUseHardwareAccelerationNode"/> class.
        /// </summary>
        public PlayerCommonUseHardwareAccelerationNode()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PlayerCommonUseHardwareAccelerationNode"/> class.
        /// </summary>
        /// <param name="defaultValue">
        /// The default Value.
        /// </param>
        public PlayerCommonUseHardwareAccelerationNode(bool defaultValue) : base(defaultValue)
        {
            this.Comment = "하드웨어 가속 사용 여부를 결정합니다. 가급적 true로 설정하시기 바랍니다. (true/false)";
        }
    }
}
