﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PlayerCommonEthernetNode.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Defines the PlayerCommonEthernetNode type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.PlayerCommons.Configs.DeviceNodes.EthernetNodes
{
    /// <summary>
    /// 장비에 설치되어 있는 네트워크 카드의 용도를 설정한다.
    /// DataIP: 데이타 통신용 네트워크 카드의 IP.
    /// VideoIP: 영상 스트리밍 통신용 네트워크 카드의 IP.
    /// </summary>
    public class PlayerCommonEthernetNode
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PlayerCommonEthernetNode"/> class.
        /// </summary>
        public PlayerCommonEthernetNode()
        {
            this.VideoIP = new PlayerCommonVideoIPNode();
        }

        /// <summary>
        /// Gets or sets VideoIP.
        /// </summary>
        public PlayerCommonVideoIPNode VideoIP { get; set; }
    }
}