﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PlayerCommonVideoIPNode.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Defines the PlayerCommonVideoIPNode type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.PlayerCommons.Configs.DeviceNodes.EthernetNodes
{
    using System.Xml.Serialization;

    /// <summary>
    /// </summary>
    public class PlayerCommonVideoIPNode : Innotive.InnoWatch.Commons.Configs.AttributeTypes.BaseNode
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PlayerCommonVideoIPNode"/> class.
        /// </summary>
        public PlayerCommonVideoIPNode()
        {
            this.Value = "0.0.0.255";
            this.Comment = "영상 수신에 사용할 랜카드 주소를 설정합니다. (맨뒤에 255를 설정하면 그 대역에 주소를 자동으로 찾습니다.)";
            this.Type = "ip";
        }

        /// <summary>
        /// Gets or sets Value.
        /// </summary>
        [XmlAttribute]
        public string Value { get; set; }

    }
}
