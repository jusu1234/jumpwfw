﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PlayerCommonDeviceNode.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Defines the PlayerCommonDeviceNode type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------



namespace Innotive.InnoWatch.PlayerCommons.Configs.DeviceNodes
{
    using System.Xml.Serialization;
    using EthernetNodes;
    using GraphicNodes;
    using MouseNodes;

    /// <summary>
    /// 장비 디바이스 장치와 관련된 속성들을 설정한다.
    /// Mouse.
    /// Ethernet.
    /// Graphic.
    /// </summary>
    [XmlRoot(ElementName = "Device")]
    public class PlayerCommonDeviceNode
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PlayerCommonDeviceNode"/> class.
        /// </summary>
        public PlayerCommonDeviceNode()
        {
            // 이 생성자는 개발자가 사용하는 용도가 아님.
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PlayerCommonDeviceNode"/> class.
        /// </summary>
        /// <param name="cursorVisible">
        /// The cursor Visible.
        /// </param>
        public PlayerCommonDeviceNode(bool cursorVisible)
        {
            this.Mouse = new PlayerCommonMouseNode(cursorVisible);
            this.Ethernet = new PlayerCommonEthernetNode();
            this.Graphic = new PlayerCommonGraphicNode();
        }

        /// <summary>
        /// Gets or sets CursorVisible.
        /// </summary>
        public PlayerCommonMouseNode Mouse { get; set; }

        /// <summary>
        /// Gets or sets Ethernet.
        /// </summary>
        public PlayerCommonEthernetNode Ethernet { get; set; }

        /// <summary>
        /// Gets or sets Graphic.
        /// </summary>
        public PlayerCommonGraphicNode Graphic { get; set; }

    }
}
