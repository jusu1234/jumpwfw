﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PlayerCommonGridCellNode.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Defines the PlayerCommonGridCellNode type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.PlayerCommons.Configs.GridCellNodes
{
    using System.Xml.Serialization;
    using LineNodes;
    using ZoomNodes;

    /// <summary>
    /// </summary>
    [XmlRoot("GridCell")]
    public class PlayerCommonGridCellNode
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PlayerCommonGridCellNode"/> class.
        /// </summary>
        public PlayerCommonGridCellNode()
        {
            this.Line = new PlayerCommonLineNode();
            this.Zoom = new PlayerCommonZoomNode();
        }

        /// <summary>
        /// Gets or sets Line.
        /// </summary>
        public PlayerCommonLineNode Line { get; set; }

        /// <summary>
        /// Gets or sets Zoom.
        /// </summary>
        public PlayerCommonZoomNode Zoom { get; set; }
    }
}
