﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PlayerCommonZoomNode.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Defines the PlayerCommonZoomNode type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.PlayerCommons.Configs.GridCellNodes.ZoomNodes
{
    /// <summary>
    /// </summary>
    public class PlayerCommonZoomNode
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PlayerCommonZoomNode"/> class.
        /// </summary>
        public PlayerCommonZoomNode()
        {
            this.AnimationSpeed = new PlayerCommonAnimationSpeedNode();
            this.ManualSizePercentage = new PlayerCommonManualSizePercentageNode();
            this.UseManualSize = new PlayerCommonUseManualSizeNode(true);
        }

        /// <summary>
        /// Gets or sets AnimationSpeed.
        /// </summary>
        public PlayerCommonAnimationSpeedNode AnimationSpeed { get; set; }

        /// <summary>
        /// Gets or sets ManualSizePercentage.
        /// </summary>
        public PlayerCommonManualSizePercentageNode ManualSizePercentage { get; set; }

        /// <summary>
        /// Gets or sets UseManualSize.
        /// </summary>
        public PlayerCommonUseManualSizeNode UseManualSize { get; set; }
    }
}
