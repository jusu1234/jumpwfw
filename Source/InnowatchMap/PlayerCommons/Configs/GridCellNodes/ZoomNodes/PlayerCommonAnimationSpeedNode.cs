﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PlayerCommonAnimationSpeedNode.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Defines the PlayerCommonAnimationSpeedNode type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.PlayerCommons.Configs.GridCellNodes.ZoomNodes
{
    using System.Xml.Serialization;
    
    /// <summary>
    /// </summary>
    public class PlayerCommonAnimationSpeedNode : Innotive.InnoWatch.Commons.Configs.AttributeTypes.BaseNode
    {
        public PlayerCommonAnimationSpeedNode()
        {
            this.Value = 0.15;
            this.Comment = "Stage Cell Double Click으로 확대/축소 시 속도를 설정합니다. (단위: 초)";
        }

        [XmlAttribute]
        public double Value { get; set; }
    }
}
