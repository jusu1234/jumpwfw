﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PlayerCommonLineNode.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Defines the PlayerCommonLineNode type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.PlayerCommons.Configs.GridCellNodes.LineNodes
{
    using Commons.Configs.AttributeTypes;

    /// <summary>
    /// 그리드셀의 라인처리에 대한 속설 설정.
    /// </summary>
    public class PlayerCommonLineNode
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PlayerCommonLineNode"/> class.
        /// </summary>
        public PlayerCommonLineNode()
        {
            this.Visible = new VisibleNode(true, "Stage Cell 분할 시 Cell 간 경계선을 보일 지 설정합니다. (true/false)");
        }

        /// <summary>
        /// Gets or sets Visible.
        /// </summary>
        public VisibleNode Visible { get; set; }
    }
}
