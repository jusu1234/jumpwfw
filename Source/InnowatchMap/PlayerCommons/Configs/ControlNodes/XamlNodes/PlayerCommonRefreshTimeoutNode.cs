﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PlayerCommonRefreshTimeoutNode.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Defines the PlayerCommonRefreshTimeoutNode type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.PlayerCommons.Configs.ControlNodes.XamlNodes
{
    using System.Xml.Serialization;

    /// <summary>
    /// ms단위의 갱신 시간 을 설정한다.
    /// 기본값: 100.
    /// </summary>
    [XmlRoot("RefreshTimeout")]
    public class PlayerCommonRefreshTimeoutNode : Innotive.InnoWatch.Commons.Configs.AttributeTypes.BaseNode
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PlayerCommonRefreshTimeoutNode"/> class.
        /// </summary>
        public PlayerCommonRefreshTimeoutNode()
        {
            this.Value = 100;
            this.Comment = "컨트롤 서버로 부터 받은 ERP Date가 그려지는 Timeout을 설정합니다. (단위: msec)";
        }

        /// <summary>
        /// Gets or sets Value.
        /// </summary>
        [XmlAttribute]
        public int Value { get; set; }
    }
}
