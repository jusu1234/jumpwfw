﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PlayerCommonXamlNode.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Defines the PlayerCommonXamlNode type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.PlayerCommons.Configs.ControlNodes.XamlNodes.PollingLimitSize
{
    using System.Xml.Serialization;

    /// <summary>
    /// XamlViewerControl이 Polling을 실행할 최소 높이를 설정한다.
    /// </summary>
    public class PlayerCommonXamlPollingLimitHeight : Innotive.InnoWatch.Commons.Configs.AttributeTypes.BaseNode
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PlayerCommonXamlPollingLimitHeight"/> class.
        /// </summary>
        public PlayerCommonXamlPollingLimitHeight()
        {
            this.Value = 0;
            this.Comment = "XamlViewerControl이 Polling을 실행할 최소 높이 입니다. (이 크기보다 작으면 Polling 하지 않습니다.)";
        }

        /// <summary>
        /// Gets or sets Value.
        /// </summary>
        [XmlAttribute]
        public double Value { get; set; }
    }
}
