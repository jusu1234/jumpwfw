﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PlayerCommonXamlNode.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Defines the PlayerCommonXamlNode type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.PlayerCommons.Configs.ControlNodes.XamlNodes
{
    using Innotive.InnoWatch.PlayerCommons.Configs.ControlNodes.XamlNodes.PollingLimitSize;
    using Innotive.InnoWatch.Commons.Configs.AttributeTypes;

    /// <summary>
    /// xaml에 사용되는 속성들을 설정한다.
    /// </summary>
    public class PlayerCommonXamlNode
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PlayerCommonXamlNode"/> class.
        /// </summary>
        public PlayerCommonXamlNode()
        {
            this.RefreshTimeout = new PlayerCommonRefreshTimeoutNode();
            this.UrlPollingInterval = new PlayerCommonUrlPollingIntervalNode();
            this.UseDynamicLoading = new UseNode(true, "Xaml 동적 로딩 사용 여부를 설정한다. (true/false)");
            this.PollingLimitMinimumWidth = new PlayerCommonXamlPollingLimitWidth();
            this.PollingLimitMinimumHeight = new PlayerCommonXamlPollingLimitHeight();
        }

        /// <summary>
        /// Gets or sets RefreshTimeout.
        /// </summary>
        public PlayerCommonRefreshTimeoutNode RefreshTimeout { get; set; }

        /// <summary>
        /// Gets or sets UrlPollingInterval.
        /// </summary>
        public PlayerCommonUrlPollingIntervalNode UrlPollingInterval { get; set; }

        /// <summary>
        /// Gets or sets UseDynamicLoading.
        /// </summary>
        public UseNode UseDynamicLoading { get; set; }

        /// <summary>
        /// Gets or sets  PollingLimitMinimumWidth.
        /// </summary>
        public PlayerCommonXamlPollingLimitWidth PollingLimitMinimumWidth { get; set; }

        /// <summary>
        /// Gets or sets  PollingLimitMinimumHeight.
        /// </summary>
        public PlayerCommonXamlPollingLimitHeight PollingLimitMinimumHeight { get; set; }
    }
}
