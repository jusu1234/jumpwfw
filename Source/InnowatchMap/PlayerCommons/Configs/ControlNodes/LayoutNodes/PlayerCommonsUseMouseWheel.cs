﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PlayerCommonsUseMouseWheel.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Defines the PlayerCommonsUseMouseWheel type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.PlayerCommons.Configs.ControlNodes.LayoutNodes
{
    using System.Xml.Serialization;
    using Innotive.InnoWatch.Commons.Configs;

    /// <summary>
    /// MouseLeftDown 시 ZoomIn/Drag를 판단하기 위해 필요한 이동 거리 (Pixel 단위).
    /// </summary>
    public class PlayerCommonsUseMouseWheel : ConvertBoolean
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PlayerCommonsUseMouseWheel"/> class.
        /// </summary>
        public PlayerCommonsUseMouseWheel()
        {
            // 이 생성자는 Serialization에서 사용합니다.
            // 이 생성자는 개발자 작업용이 아닙니다. 
            // PlayerCommonsUseMouseWheel(bool defaltValue) 생성자를 사용합니다.

            this.Comment = "레이아웃에서 확대 축소시 마우스 휠 사용 여부를 설정 합니다. (true/false)";
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PlayerCommonsUseMouseWheel"/> class.
        /// </summary>
        /// <param name="defaultValue">
        /// The default value.
        /// </param>
        public PlayerCommonsUseMouseWheel(bool defaultValue)
            : base(defaultValue)
        {
            // base에서 구현되었음.

            this.Comment = "레이아웃에서 확대 축소시 마우스 휠 사용 여부를 설정 합니다. (true/false)";
        }
    }
}
