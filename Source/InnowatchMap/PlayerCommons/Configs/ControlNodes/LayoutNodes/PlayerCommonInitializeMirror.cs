﻿// -----------------------------------------------------------------------
// <copyright file="PlayerCommonsInitializeMirror.cs" company="Innotive Inc. Korea">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace Innotive.InnoWatch.PlayerCommons.Configs.ControlNodes.LayoutNodes
{
    using Innotive.InnoWatch.Commons.Configs;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class PlayerCommonsInitializeMirror : ConvertBoolean
    {
        public PlayerCommonsInitializeMirror()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PlayerCommonsInitializeMirror"/> class.
        /// </summary>
        /// <param name="defaultValue">
        /// The default Value.
        /// </param>
        public PlayerCommonsInitializeMirror(bool defaultValue) : base(defaultValue)
        {
            this.Comment = "레이아웃이 로드 시 내부 미러 컨트롤에 연결된 디스플레이 에어리어를 스타트 레이아웃으로 초기화 시킬지 여부를 설정합니다. (true/false)";
        }
    }
}
