﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PlayerCommonsDragRangePossiblePixel.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Defines the PlayerCommonsDragRangePossiblePixel type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.PlayerCommons.Configs.ControlNodes.LayoutNodes
{
    using System.Xml.Serialization;

    /// <summary>
    /// MouseLeftDown 시 ZoomIn/Drag를 판단하기 위해 필요한 이동 거리 (Pixel 단위).
    /// </summary>
    public class PlayerCommonsPanningStartPixel : Innotive.InnoWatch.Commons.Configs.AttributeTypes.BaseNode
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PlayerCommonsPanningStartPixel"/> class.
        /// </summary>
        public PlayerCommonsPanningStartPixel()
        {
            this.Value = 0;
            this.Comment = "마우스 드래그 시 최초 이동이 동작하는 거리를 설정합니다. (단위: Pixel)";
        }

        /// <summary>
        /// Gets or sets Value.
        /// </summary>
        [XmlAttribute]
        public double Value { get; set; }
    }
}
