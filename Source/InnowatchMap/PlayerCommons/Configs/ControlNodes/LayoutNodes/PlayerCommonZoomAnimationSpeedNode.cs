﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PlayerCommonZoomAnimationSpeedNode.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Defines the PlayerCommonZoomAnimationSpeedNode type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.PlayerCommons.Configs.ControlNodes.LayoutNodes
{
    using System.Xml.Serialization;

    /// <summary>
    /// 레이아웃의 줌에니메이션 속도를 설정한다.
    /// 기본값: 0.15.
    /// </summary>
    [XmlRoot(ElementName = "ZoomAnimationSpeed")]
    public class PlayerCommonZoomAnimationSpeedNode : Innotive.InnoWatch.Commons.Configs.AttributeTypes.BaseNode
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PlayerCommonZoomAnimationSpeedNode"/> class.
        /// </summary>
        public PlayerCommonZoomAnimationSpeedNode()
        {
            this.Value = 0.15;
            this.Comment = "확대/축소 동작 속도를 설정합니다. (단위: 초)";
        }

        /// <summary>
        /// Gets or sets Value.
        /// </summary>
        [XmlAttribute]
        public double Value { get; set; }
    }
}
