﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PlayerCommonLayoutNode.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Defines the PlayerCommonLayoutNode type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.PlayerCommons.Configs.ControlNodes.LayoutNodes
{
    using System.Xml.Serialization;

    /// <summary>
    /// 레이아웃의 줌에니메이션속도에 대한 속성을 설정한다.
    /// 기본값: PainningStartPixel = 0, ZoomAnimationSpeed = 0.15.
    /// </summary>
    [XmlRoot("Layout")]
    public class PlayerCommonLayoutNode
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PlayerCommonLayoutNode"/> class.
        /// </summary>
        public PlayerCommonLayoutNode()
        {
            this.PanningStartPixel = new PlayerCommonsPanningStartPixel();
            this.ZoomAnimationSpeed = new PlayerCommonZoomAnimationSpeedNode();
            this.UseMouseWheel = new PlayerCommonsUseMouseWheel(true);
            this.InitializeMirror = new PlayerCommonsInitializeMirror(false);
        }

        /// <summary>
        /// Gets or sets MouseDragRangePossiblePixel.
        /// </summary>
        public PlayerCommonsPanningStartPixel PanningStartPixel { get; set; }

        /// <summary>
        /// Gets or sets ZoomAnimationSpeed.
        /// </summary>
        public PlayerCommonZoomAnimationSpeedNode ZoomAnimationSpeed { get; set; }

        /// <summary>
        /// Gets or sets UseMouseWheel.
        /// </summary>
        public PlayerCommonsUseMouseWheel UseMouseWheel { get; set; }

        /// <summary>
        /// Gets or sets InitializeMirror.
        /// </summary>
        public PlayerCommonsInitializeMirror InitializeMirror { get; set; }
    }
}
