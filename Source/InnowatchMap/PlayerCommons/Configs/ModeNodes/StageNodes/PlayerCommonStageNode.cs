﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PlayerCommonStageNode.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Defines the PlayerCommonCameraNode type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.PlayerCommons.Configs.ModeNodes.StageNodes
{
    using Commons.Configs.AttributeTypes;

    /// <summary>
    /// </summary>
    public class PlayerCommonStageNode
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PlayerCommonStageNode"/> class.
        /// </summary>
        public PlayerCommonStageNode()
        {
            this.Use = new UseNode(true, "카메라 모드 사용 여부를 설정합니다. (true/false)");
        }

        /// <summary>
        /// Gets or sets Use.
        /// </summary>
        public UseNode Use { get; set; }
    }
}
