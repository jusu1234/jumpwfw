﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PlayerCommonModeNode.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Defines the PlayerCommonModeNode type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.PlayerCommons.Configs.ModeNodes
{
    using Innotive.InnoWatch.PlayerCommons.Configs.ModeNodes.ContentsNodes;
    using Innotive.InnoWatch.PlayerCommons.Configs.ModeNodes.StageNodes;

    /// <summary>
    /// </summary>
    public class PlayerCommonModeNode
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PlayerCommonModeNode"/> class.
        /// </summary>
        public PlayerCommonModeNode()
        {
            this.Contents = new PlayerCommonContentsNode();
            this.Stage = new PlayerCommonStageNode();
        }

        /// <summary>
        /// Gets or sets Contents.
        /// </summary>
        public PlayerCommonContentsNode Contents { get; set; }

        /// <summary>
        /// Gets or sets Camera.
        /// </summary>
        public PlayerCommonStageNode Stage { get; set; }
    }
}
