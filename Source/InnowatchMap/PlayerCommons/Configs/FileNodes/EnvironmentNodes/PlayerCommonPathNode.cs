﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PlayerCommonPathNode.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Defines the PlayerCommonPathNode type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.PlayerCommons.Configs.FileNodes.EnvironmentNodes
{
    using System.IO;
    using System.Xml.Serialization;

    /// <summary>
    /// 무조건 ExecutablePath + \QuickSettings.xml을 반환한다.
    /// </summary>
    [XmlRoot(ElementName = "Path")]
    public class PlayerCommonPathNode : Innotive.InnoWatch.Commons.Configs.AttributeTypes.BaseNode
    {
        private string path;

        /// <summary>
        /// Initializes a new instance of the <see cref="PlayerCommonPathNode"/> class.
        /// </summary>
        public PlayerCommonPathNode()
        {
            this.Value = this.GetFileName();
            this.Comment = "이값은 ReadOnly 속성입니다. 변경되지 않습니다.";
        }

        /// <summary>
        /// Gets or sets Value.
        /// </summary>
        [XmlAttribute]
        public string Value
        {
            get
            {
                return this.GetFileName();
            }

            set
            {
                this.path = value;
            }
        }
        
        private string GetFileName()
        {
            return Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath) + @"\QuickSettings.xml";
        }
    }
}
