﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PlayerCommonPTZServerPortNode.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Defines the PTZ Server Port.
// </summary>
// --------------------------------------------------------------------------------------------------------------------



namespace Innotive.InnoWatch.PlayerCommons.Configs.CameraNodes.PTZNodes
{
    using System.Xml.Serialization;

    /// <summary>
    /// 
    /// </summary>
    [XmlRoot(ElementName = "PTZServerPort")]
    public class PlayerCommonPTZServerPortNode : Innotive.InnoWatch.Commons.Configs.AttributeTypes.BaseNode
    {
        public PlayerCommonPTZServerPortNode()
        {
            this.Value = 36000;
            this.Comment = "PTZ 서버 포트를 설정 합니다. (기본값: 36000, 0이하 값은 서버 가동하지 않음)";
        }

        /// <summary>
        /// Gets or sets a value indicating whether Value.
        /// </summary>
        [XmlAttribute]
        public int Value { get; set; }
    }
}
