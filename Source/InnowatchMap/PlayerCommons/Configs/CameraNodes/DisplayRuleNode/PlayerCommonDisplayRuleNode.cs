﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PlayerCommonDisplayRuleNode.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Defines the PlayerCommonDisplayRuleNode type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.PlayerCommons.Configs.CameraNodes.DisplayRuleNode
{
    /// <summary> 
    /// 화면 표출 규칙.
    /// </summary>
    public class PlayerCommonDisplayRuleNode
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PlayerCommonDisplayRuleNode"/> class.
        /// </summary>
        public PlayerCommonDisplayRuleNode()
        {
            this.ShowLimitWidth = new PlayerCommonShowLimitWidthNode();
            this.ShowLimitHeight = new PlayerCommonShowLimitHeightNode();
        }
        
        /// <summary>
        /// Gets or sets ShowLimitWidth.
        /// </summary>
        public PlayerCommonShowLimitWidthNode ShowLimitWidth { get; set; }

        /// <summary>
        /// Gets or sets ShowLimitHeight.
        /// </summary>
        public PlayerCommonShowLimitHeightNode ShowLimitHeight { get; set; }
    }
}
