﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PlayerCommonShowLimitHeightNode.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Defines the DisplayRuleNode type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------



namespace Innotive.InnoWatch.PlayerCommons.Configs.CameraNodes.DisplayRuleNode
{
    using System.Xml.Serialization;

    /// <summary>
    /// 
    /// </summary>
    [XmlRoot(ElementName = "ShowLimitHeight")]
    public class PlayerCommonShowLimitHeightNode : Innotive.InnoWatch.Commons.Configs.AttributeTypes.BaseNode
    {
        public PlayerCommonShowLimitHeightNode()
        {
            this.Value = 10;
            this.Comment = "화면에 실제 표시되는 높이가 설정된 값보다 작으면 영상을 표시하지 않음. (단위: Pixel, 0이하 값이면 사용하지 않음.)";
        }

        /// <summary>
        /// Gets or sets a value indicating whether Value.
        /// </summary>
        [XmlAttribute]
        public int Value { get; set; }
    }
}
