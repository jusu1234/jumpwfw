﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PlayerCommonPauseCameraWhenMovingNode.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Defines the PlayerCommonPauseCameraWhenMovingNode type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.PlayerCommons.Configs.CameraNodes.DisplayRuleNode
{
    using Commons.Configs;
    using System.Xml.Serialization;

    /// <summary>
    /// Zoom, Panning, GotoLocation시 Camera화면을 일시적으로 멈출지 결정한다
    /// 여러대의 iDisplay가 하나의 큰 DisplayArea를 분할해서 보여줄때 각 iDisplay간 Sync가 동작하지 않는 문제가 있음
    /// </summary>
    public class PlayerCommonPauseCameraWhenMovingNode : ConvertBoolean
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PlayerCommonPauseCameraWhenMovingNode"/> class.
        /// </summary>
        public PlayerCommonPauseCameraWhenMovingNode()
        {
            // 이 생성자는 Serialization에서 사용합니다.
            // 이 생성자는 개발자 작업용이 아닙니다. 
            // NameLabelVisibleNode(bool defaltValue) 생성자를 사용합니다.
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PlayerCommonPauseCameraWhenMovingNode"/> class.
        /// </summary>
        /// <param name="defaultValue">
        /// The default value.
        /// </param>
        public PlayerCommonPauseCameraWhenMovingNode(bool defaultValue)
            : base(defaultValue)
        {
            // base에서 구현되었음.
            this.Comment = "화면 확대/축소 및 드래그시 영상을 정지할지 여부를 설정합니다. 사용하면 성능 향상이 있습니다. (true/false)";
        }
    }
}
