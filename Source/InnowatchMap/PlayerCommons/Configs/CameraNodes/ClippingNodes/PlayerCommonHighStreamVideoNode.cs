﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PlayerCommonHighStreamVideoNode.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   고화질용 (CameraFilter.cs에서 사용함)
//   카메라 테두리에 잡음이 생기는 경우를 대비해 카메라 각각의 모서리를 얼마만큼 자를 지 설정합니다.
//   의정부시청처럼 아날로그 카메라를 사용하는 경우 원본 영상 자체가 모서리에 간섭현상이 나타난다. 그런 경우 주변 화면을 제거하는 속성이다.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.PlayerCommons.Configs.CameraNodes.ClippingNodes
{
    using System.Windows;
    using System.Xml.Serialization;

    /// <summary>
    /// 고화질용 (CameraFilter.cs에서 사용함)
    /// 카메라 테두리에 잡음이 생기는 경우를 대비해 카메라 각각의 모서리를 얼마만큼 자를 지 설정합니다.
    /// 의정부시청처럼 아날로그 카메라를 사용하는 경우 원본 영상 자체가 모서리에 간섭현상이 나타난다. 그런 경우 주변 화면을 제거하는 속성이다.
    /// 기본값: 0,0,0,0.
    /// </summary>
    [XmlRoot(ElementName = "HighStreamVideo")]
    public class PlayerCommonHighStreamVideoNode : Innotive.InnoWatch.Commons.Configs.AttributeTypes.BaseNode
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PlayerCommonHighStreamVideoNode"/> class.
        /// </summary>
        public PlayerCommonHighStreamVideoNode()
        {
            this.Left = 0;
            this.Top = 0;
            this.Right = 0;
            this.Bottom = 0;

            this.Comment = "고화질 영상의 외곽에 여백이 있을 경우 자르는 크기를 설정합니다. (단위: Pixel)";
            this.Type = "none";
        }

        /// <summary>
        /// Gets or sets a value indicating whether Value.
        /// </summary>
        [XmlAttribute]
        public double Left { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether Value.
        /// </summary>
        [XmlAttribute]
        public double Top { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether Value.
        /// </summary>
        [XmlAttribute]
        public double Right { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether Value.
        /// </summary>
        [XmlAttribute]
        public double Bottom { get; set; }

        /// <summary>
        /// 속성들을 Rect 형식으로 반환한다.
        /// </summary>
        /// <returns>
        /// The Rect.
        /// </returns>
        public Thickness GetValue()
        {
            return new Thickness(this.Left, this.Top, this.Right, this.Bottom);
        }
    }
}
