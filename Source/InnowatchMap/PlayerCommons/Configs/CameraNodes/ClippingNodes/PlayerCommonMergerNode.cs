﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PlayerCommonMergerNode.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Merger 영상 아래 20 Pixel 추가한 부분에 대한 보간.
//   Merger 버그에 대한 예외처리였으며 아직 버그 대비용으로 가지고 있음.
//   현재 소스상에 남아있기는 하지만 값이 0으로 되어 있음.
// </summary>
// --------------------------------------------------------------------------------------------------------------------



namespace Innotive.InnoWatch.PlayerCommons.Configs.CameraNodes.ClippingNodes
{
    using System;
    using System.Windows;
    using System.Xml.Serialization;

    /// <summary>
    /// Merger 영상 아래 20 Pixel 추가한 부분에 대한 보간. 
    /// Merger 버그에 대한 예외처리였으며 아직 버그 대비용으로 가지고 있음.  
    /// 현재 소스상에 남아있기는 하지만 값이 0으로 되어 있음. 
    /// </summary>
    [XmlRoot(ElementName = "MergerBottom")]
    public class PlayerCommonMergerNode : Innotive.InnoWatch.Commons.Configs.AttributeTypes.BaseNode
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PlayerCommonMergerNode"/> class.
        /// </summary>
        public PlayerCommonMergerNode()
        {
            this.Left = 0;
            this.Top = 0;
            this.Right = 0;
            this.Bottom = 0;

            this.Comment = "Merger 전체 영상의 외곽에 여백이 있을 경우 자르는 크기를 설정합니다. Bottom 값만 사용합니다. (단위: %)";
            this.Type = "none";
        }

        /// <summary>
        /// Gets or sets a value indicating whether Value.
        /// </summary>
        [XmlAttribute]
        public double Left { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether Value.
        /// </summary>
        [XmlAttribute]
        public double Top { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether Value.
        /// </summary>
        [XmlAttribute]
        public double Right { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether Value.
        /// </summary>
        [XmlAttribute]
        public double Bottom { get; set; }

        /// <summary>
        /// 속성값들을 Rect로 반환한다.
        /// </summary>
        /// <returns>
        /// The Rect.
        /// </returns>
        public Rect GetValue()
        {
            return new Rect(this.Left, this.Top, this.Right, this.Bottom);
        }

        /// <summary>
        /// 더블형인 Bottom을 Int32형으로 반환한다.
        /// </summary>
        /// <returns>
        /// The Int32.
        /// </returns>
        public int BottomToInt()
        {
            return Convert.ToInt32(this.Bottom);
        }
    }
}
