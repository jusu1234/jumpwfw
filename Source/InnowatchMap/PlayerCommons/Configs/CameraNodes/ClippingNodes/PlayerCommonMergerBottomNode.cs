﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PlayerCommonMergerBottomNode.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Merger 영상 아래 20 Pixel 추가한 부분에 대한 보간.
//   Merger 버그에 대한 예외처리였으며 아직 버그 대비용으로 가지고 있음.
//   현재 소스상에 남아있기는 하지만 값이 0으로 되어 있음.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.PlayerCommons.Configs.CameraNodes.ClippingNodes
{
    using System.Xml.Serialization;

    /// <summary>
    /// Merger 영상 아래 20 Pixel 추가한 부분에 대한 보간. 
    /// Merger 버그에 대한 예외처리였으며 아직 버그 대비용으로 가지고 있음.  
    /// 현재 소스상에 남아있기는 하지만 값이 0으로 되어 있음. 
    /// </summary>
    [XmlRoot(ElementName = "MergerBottom")]
    public class PlayerCommonMergerBottomNode
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PlayerCommonMergerBottomNode"/> class.
        /// </summary>
        public PlayerCommonMergerBottomNode()
        {
            this.Value = 0;
        }

        /// <summary>
        /// Gets or sets a value indicating whether Value.
        /// </summary>
        [XmlAttribute]
        public int Value { get; set; }
    }
}
