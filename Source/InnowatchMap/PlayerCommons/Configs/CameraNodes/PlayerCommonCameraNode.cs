﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PlayerCommonCameraNode.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Defines the CameraNode type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.PlayerCommons.Configs.CameraNodes
{
    using System.Xml.Serialization;
    using ClippingNodes;
    using PTZNodes;
    using SwitchingRuleNodes;
    using UINodes;

    using Innotive.InnoWatch.Commons.Publics;
    using Innotive.InnoWatch.PlayerCommons.Configs.CameraNodes.DisplayRuleNode;

    /// <summary>
    /// 카메라 속성.
    /// </summary>
    [XmlRoot(ElementName = "Camera")]
    public class PlayerCommonCameraNode
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PlayerCommonCameraNode"/> class.
        /// </summary>
        public PlayerCommonCameraNode()
        {
            this.Clipping = new PlayerCommonClippingNode();
            this.DisplayRule = new PlayerCommonDisplayRuleNode();

            // iviewer 기능 변경으로 iViewer에서 PTZ 가능하도록 함.
            this.PTZ = new PlayerCommonPTZNode();

            this.SwitchingRule = new PlayerCommonSwitchingRuleNode();
            this.UI = new PlayerCommonUINode();
        }
        
        /// <summary>
        /// Gets or sets Clipping.
        /// </summary>
        public PlayerCommonClippingNode Clipping { get; set; }
        
        /// <summary>
        /// Gets or sets DisplayRule.
        /// </summary>
        public PlayerCommonDisplayRuleNode DisplayRule { get; set; }

        /// <summary>
        /// Gets or sets PTZ.
        /// </summary>
        public PlayerCommonPTZNode PTZ { get; set; }

        /// <summary>
        /// Gets or sets SwitchingRule.
        /// </summary>
        public PlayerCommonSwitchingRuleNode SwitchingRule { get; set; }

        /// <summary>
        /// Gets or sets UI.
        /// </summary>
        public PlayerCommonUINode UI { get; set; }
    }
}
