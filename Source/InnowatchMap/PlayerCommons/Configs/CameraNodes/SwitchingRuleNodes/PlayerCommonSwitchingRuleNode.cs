﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PlayerCommonSwitchingRuleNode.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Defines the SwichingRuleNode type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.PlayerCommons.Configs.CameraNodes.SwitchingRuleNodes
{
    using System.Xml.Serialization;

    /// <summary>
    /// 저해상도와 고해상도 전환에 관련된 속성들을 관리한다.
    /// LowToHighSpeed.
    /// HighToLowSpeed.
    /// </summary>
    [XmlRoot(ElementName = "SwitchingRule")]
    public class PlayerCommonSwitchingRuleNode
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PlayerCommonSwitchingRuleNode"/> class.
        /// </summary>
        public PlayerCommonSwitchingRuleNode()
        {
            this.SwitchingSpeed = new PlayerCommonSwitchingSpeedNode();
            this.SwitchingUsePause = new PlayerCommonSwitchingUsePause();
        }
        
        /// <summary>
        /// Gets or sets SwitchingSpeed.
        /// </summary>
        public PlayerCommonSwitchingSpeedNode SwitchingSpeed { get; set; }
        public PlayerCommonSwitchingUsePause SwitchingUsePause { get; set; }
    }
}
