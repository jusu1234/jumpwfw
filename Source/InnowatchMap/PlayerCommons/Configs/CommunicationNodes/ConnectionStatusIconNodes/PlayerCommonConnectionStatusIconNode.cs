﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PlayerCommonCameraControlServerNode.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Defines the PlayerCommonConnectionStatusIconNode type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.PlayerCommons.Configs.CommunicationNodes.ConnectionStatusIconNodes
{
    using System.Xml.Serialization;
    using Commons.Configs.AttributeTypes;

    /// <summary>
    /// 서버(카메라/싱크/컨트롤메시지/컨트롤데이터) 연결 상태 표시 여부를 설정을 할있는 클래스.
    /// </summary>
    [XmlRoot(ElementName = "ConnectionStatusIcon")]
    public class PlayerCommonConnectionStatusIconNode
    {
        public PlayerCommonConnectionStatusIconNode()
        {
            this.Use = new UseNode(true, "서버 연결 상태 표시 여부를 설정합니다. (true/false)");
        }

        /// <summary>
        /// Gets or sets Use.
        /// </summary>
        public UseNode Use { get; set; }
    }
}
