﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PlayerCommonPlaybackNode.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Defines the PlayerCommonPlaybackNode type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.PlayerCommons.Configs.CommunicationNodes.PlaybackNodes
{
    using System.Xml.Serialization;

    /// <summary>
    /// CMS Playback config.
    /// </summary>
    [XmlRoot(ElementName = "ControlManager")]
    public class PlayerCommonPlaybackNode
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PlayerCommonPlaybackNode"/> class.
        /// </summary>
        public PlayerCommonPlaybackNode()
        {
            this.PlaybackPath = new PlayerCommonsPlaybackPathNode();
        }

        /// <summary>
        /// Gets or sets Playback path.
        /// </summary>
        public PlayerCommonsPlaybackPathNode PlaybackPath { get; set; }
    }
}
