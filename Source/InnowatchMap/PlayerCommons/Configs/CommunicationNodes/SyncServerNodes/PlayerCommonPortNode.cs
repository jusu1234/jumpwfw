﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PlayerCommonPortNode.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Defines the PlayerCommonPortNode type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.PlayerCommons.Configs.CommunicationNodes.SyncServerNodes
{
    using System.Xml.Serialization;

    /// <summary>
    /// 동기화서버의 포트를 설정한다.
    /// </summary>
    [XmlRoot(ElementName = "Port")]
    public class PlayerCommonPortNode : Innotive.InnoWatch.Commons.Configs.AttributeTypes.BaseNode
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PlayerCommonPortNode"/> class.
        /// </summary>
        public PlayerCommonPortNode()
        {
            this.Value = 7000;
            this.Comment = "싱크 서버가 사용하는 포트를 설정합니다. (기본값: 7000)";
            this.Type = "port";
        }

        /// <summary>
        /// Gets or sets Value.
        /// </summary>
        [XmlAttribute]
        public int Value { get; set; }
    }
}
