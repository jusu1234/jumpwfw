﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PlayerCommonLowNode.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Defines the PlayerCommonLowNode type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.PlayerCommons.Configs.FilterNodes.LimitNodes
{
    using System.Xml.Serialization;

    /// <summary>
    /// </summary>
    public class PlayerCommonLowNode : Innotive.InnoWatch.Commons.Configs.AttributeTypes.BaseNode
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PlayerCommonLowNode"/> class.
        /// </summary>
        public PlayerCommonLowNode()
        {
            this.Value = -1;
            this.Comment = "내부적으로 가져올 저화질 영상 수를 설정합니다. (-1이면 등록된 전체 저화질 영상 수를 자동 계산)";
        }

        /// <summary>
        /// Gets or sets Value.
        /// </summary>
        [XmlAttribute]
        public int Value { get; set; }
    }
}
