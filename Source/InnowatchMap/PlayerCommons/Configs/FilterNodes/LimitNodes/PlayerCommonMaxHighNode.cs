﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PlayerCommonMiddleNode.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Defines the PlayerCommonMiddleNode type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.PlayerCommons.Configs.FilterNodes.LimitNodes
{
    using System.Xml.Serialization;

    /// <summary>
    /// </summary>
    public class PlayerCommonMaxHighNode : Innotive.InnoWatch.Commons.Configs.AttributeTypes.BaseNode
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PlayerCommonMaxHighNode"/> class.
        /// </summary>
        public PlayerCommonMaxHighNode()
        {
            this.Value = -1;
            this.Comment = "화면에 표시할 최대 고화질 영상 수를 설정합니다. (기본값 -1은 제한 없음. 0이면 카메라 표시하지 않음. 0보다 크면 순차적으로 개수 제한함.)";
        }

        /// <summary>
        /// Gets or sets Value.
        /// </summary>
        [XmlAttribute]
        public int Value { get; set; }
    }
}
