﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PlayerCommonLimitNode.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Defines the PlayerCommonLimitNode type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.PlayerCommons.Configs.FilterNodes.LimitNodes
{
    using Innotive.InnoWatch.Commons.Publics;

    /// <summary>
    /// </summary>
    public class PlayerCommonLimitNode
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PlayerCommonLimitNode"/> class.
        /// </summary>
        public PlayerCommonLimitNode()
        {
            this.MaxHigh = new PlayerCommonMaxHighNode();
            this.Low = new PlayerCommonLowNode();
        }

        /// <summary>
        /// Gets or sets High.
        /// </summary>
        public PlayerCommonMaxHighNode MaxHigh { get; set; }

        /// <summary>
        /// Gets or sets Low.
        /// </summary>
        public PlayerCommonLowNode Low { get; set; }
    }
}
