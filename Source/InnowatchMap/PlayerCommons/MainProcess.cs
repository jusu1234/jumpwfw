﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MainProcess.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   main process.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.PlayerCommons
{
    using System;
    using System.ComponentModel;
    using System.Reflection;
    using System.Windows;
    using System.Windows.Forms;
    using Innotive.InnoWatch.CommonUtils.Models;
    using Innotive.InnoWatch.Commons.BaseControls;
    using Innotive.InnoWatch.Commons.CameraManagers;
    using Innotive.InnoWatch.Commons.EventArguments;
    using Innotive.InnoWatch.Commons.Files;
    using Innotive.InnoWatch.Commons.Publics;
    using Innotive.InnoWatch.Commons.Utils;
    using Innotive.InnoWatch.CommonUtils.Models.Sync.Control;
    using Innotive.InnoWatch.PlayerCommons.Communications;
    using Innotive.InnoWatch.PlayerCommons.Environments;
    using Innotive.InnoWatch.PlayerCommons.Projects;    
    using log4net;

    /// <summary>
    /// main process.
    /// </summary>
    public class MainProcess
    {
        public static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        private Window mainWindow;

        /// <summary>
        /// Initializes a new instance of the <see cref="MainProcess"/> class.
        /// </summary>
        private MainProcess()
        {
            this.UserID = string.Empty;
            this.PlayerProcess = new PlayingProcess();
        }

        /// <summary>
        /// e external alarm raised.
        /// </summary>
        public event EventHandler<ExternalAlarmRaisedEventArgs> eExternalAlarmRaised = null;

        /// <summary>
        /// e show alarm border.
        /// </summary>
        public event EventHandler<ShowAlarmBorderEventArgs> eShowAlarmBorder = null;

        /// <summary>
        /// Gets Instance.
        /// </summary>
        public static MainProcess Instance
        {
            get
            {
                return Nested<MainProcess>.Instance;
            }
        }

        /// <summary>
        /// Gets a value indicating whether ApplicationIsConsoleType(Program Type이 서버에 보내기가 가능한지 Check함 !! (Console, ConsoleBasic 가능)).
        /// </summary>
        public bool ApplicationIsConsoleType
        {
            get
            {
                if (this.ProgramType == ProgramType.iCommand ||
                    this.ProgramType == ProgramType.iViewer)
                {
                    return true;
                }

                return false;
            }
        }

        /// <summary>
        /// Gets a value indicating whether ApplicationIsPlayerType(Program Type이 서버에서 오는 Data를 처리하는게 가능한지 Check함 !! (Player 가능)).
        /// </summary>
        public bool ApplicationIsPlayerType
        {
            get
            {
                if (this.ProgramType == ProgramType.iDisplay)
                {
                    return true;
                }

                return false;
            }
        }

        /// <summary>
        /// Gets CommManager.
        /// </summary>
        public CommManager CommManager { get; private set; }

        /// <summary>
        /// Gets EnvironmentManager.
        /// </summary>
        public EnvironmentManager EnvironmentManager { get; private set; }

        /// <summary>
        /// Gets or sets MainWindow.
        /// </summary>
        public Window MainWindow
        {
            get
            {
                return this.mainWindow;
            }

            set
            {
                this.mainWindow = value;
            }
        }

        /// <summary>
        /// Gets or sets ProgramType.
        /// </summary>
        public ProgramType ProgramType { get; set; }

        /// <summary>
        /// Gets ProjectManager.
        /// </summary>
        public ProjectManager ProjectManager { get; private set; }

        /// <summary>
        /// Gets or Sets UserID.
        /// </summary>
        public string UserID { get; set; }

        /// <summary>
        /// Gets or Sets UserPrivilege.
        /// </summary>
        public UserPrivilege UserPrivilege { get; set; }
        
        /// <summary>
        /// Gets or sets LicenseId.
        /// </summary>
        public string LicenseId { get; set; }

        /// <summary>
        /// Gets or sets MachineCode.
        /// </summary>
        public string MachineCode { get; set; }

        /// <summary>
        /// Gets or sets ActivationCode.
        /// </summary>
        public string ActivationCode { get; set; }

        /// <summary>
        /// Gets or sets PlayerProcess(플레이어의 마우스 및 키에 대한 행위를 정의).
        /// </summary>
        public BaseProcess PlayerProcess { get; set; }

        /// <summary>
        /// Player가 여러 모니터중 하나에 뜰때 어떤 Monitor에 뜰것인지 결정 !!
        /// </summary>
        public int PlayerVisibleMonitorIndex = -1;

        /// <summary>
        /// initialize all display areas.
        /// </summary>
        public void InitializeAllDisplayAreas()
        {
            // 서버로부터 오는 초기화 Message의 처리는 Player에서만 함 !!
            if (!Instance.ApplicationIsPlayerType)
            {
                return;
            }

            // 초기화가 완료되기 전까지 Sync Data를 처리하지 못하게 함 !!
            // OpenLayout이 완료된 시점에서 다시 처리하도록 함 !! (ShowStartLayouts 내부에서)
            //Instance.CommManager.ControlServer.IsHandleReceivedMsg = false;
            Instance.ProjectManager.PlayerLayoutManager.ClearAllFileDataLayouts();
            Instance.ProjectManager.OverlayDisplayAreaManager.InitializeAllOverlayDisplayAreas();
            Instance.ProjectManager.DisplayAreaOpenChangeLayoutSyncManager.ShowStartLayouts();
            Instance.ProjectManager.ChangeDisplayMode(ConsoleDisplayMode.ContentsMode);
        }

        /// <summary>
        /// load project info.
        /// </summary>
        /// <param name="projectFileName">
        /// The project file name.
        /// </param>
        /// <returns>
        /// The load project info.
        /// </returns>
        public bool LoadProjectInfo(InnoFilePath projectFileName)
        {
            return this.ProjectManager.OpenProject(projectFileName);
        }
        
        /// <summary>
        /// Raise External Event.
        /// </summary>
        /// <param name="eventArgs">
        /// The event args.
        /// </param>
        public void RaiseExternalEvent(EventArgs eventArgs)
        {
            if (eventArgs is ShowAlarmBorderEventArgs)
            {
                this.eShowAlarmBorder(this, eventArgs as ShowAlarmBorderEventArgs);
            }
            else if (eventArgs is ExternalAlarmRaisedEventArgs)
            {
                this.eExternalAlarmRaised(this, eventArgs as ExternalAlarmRaisedEventArgs);
            }
        }

        /// <summary>
        /// Raise Alarm Event.
        /// </summary>
        /// <param name="alarmName">Alarm Name.</param>
        public void RaiseAlarmEvent(string alarmName)
        {
            System.Windows.MessageBox.Show("외부 명령에서 알람이 왔지만, 아직 처리되지 않고 있습니다. " + alarmName);
        }

        /// <summary>
        /// save monitor driver.
        /// </summary>
        public void SaveMonitorDriver()
        {
        }

        /// <summary>
        /// show loaded project.
        /// </summary>
        /// <returns>
        /// The show loaded project.
        /// </returns>
        public bool ShowLoadedProject()
        {
            return this.ProjectManager.ShowLoadedProject();
        }

        /// <summary>
        /// start managers.
        /// </summary>
        public void StartManagers()
        {
            this.EnvironmentManager = new EnvironmentManager();
            this.ProjectManager = new ProjectManager();
            this.CommManager = CommManager.Instance;

            if (this.mainWindow != null)
            {
                this.mainWindow.Closing += _mainWindow_Closing;
            }
        }

        private static void _mainWindow_Closing(object sender, CancelEventArgs e)
        {
            AsyncWorker.Instance.Dispose();

            if (CameraManager.HasInstance)
            {
                if (CameraManager.Instance != null)
                {
                    CameraManager.Instance.Dispose();
                }
            }
        }

        private Rect GetMonitorArea()
        {
            var screens = Screen.AllScreens;
            Rect monitorRect = Rect.Empty;

            if (Public.GetProgramType() == ProgramType.iCommand || Public.GetProgramType() == ProgramType.iViewer)
            {
                foreach (var screen in screens)
                {
                    if (!screen.Primary)
                        continue;

                    monitorRect.Union(new Rect(screen.Bounds.X, screen.Bounds.Y, screen.Bounds.Width, screen.Bounds.Height));
                }
            }
            else if (Public.GetProgramType() == ProgramType.iDisplay)
            {
                foreach (var screen in screens)
                {
                    monitorRect.Union(new Rect(screen.Bounds.X, screen.Bounds.Y, screen.Bounds.Width, screen.Bounds.Height));
                }
            }

            return monitorRect;
        }
    }
}