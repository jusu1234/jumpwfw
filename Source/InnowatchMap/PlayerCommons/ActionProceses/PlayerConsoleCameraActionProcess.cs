// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PlayerConsoleActionProcess.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   player console action process.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.PlayerCommons.ActionProceses
{
    using System;

    using Innotive.InnoWatch.Commons.Actions;
    using Innotive.InnoWatch.Commons.Publics;
    using Innotive.InnoWatch.CommonUtils.Log;

    public class ConsoleCameraActionEventArgs : EventArgs
    {
        public DataConsoleCameraActionProcessItem Item { get; private set; }

        public ConsoleCameraActionEventArgs(DataConsoleCameraActionProcessItem item)
        {
            this.Item = item;
        }
    }

    /// <summary>
    /// player console action process.
    /// </summary>
    public class PlayerConsoleCameraActionProcess : PlayerActionProcess
    {
        private DataConsoleCameraActionProcessItem _item;

        /// <summary>
        /// Initializes a new instance of the <see cref="PlayerConsoleActionProcess"/> class.
        /// </summary>
        /// <param name="item">
        /// The item.
        /// </param>
        public PlayerConsoleCameraActionProcess(DataConsoleCameraActionProcessItem item)
        {
            this._startTime = item.StartTime;
            this._item = item;
        }

        public event EventHandler<ConsoleCameraActionEventArgs> OnExecuteCameraAction;
        
        /// <summary>
        /// execute.
        /// </summary>
        public override void Execute()
        {
            base.Execute();
        }

        /// <summary>
        /// do dispose.
        /// </summary>
        /// <param name="isManage">
        /// The is manage.
        /// </param>
        protected override void DoDispose(bool isManage)
        {
            if (isManage)
            {
            }

            this.OnExecuteCameraAction = null;

            base.DoDispose(isManage);
        }

        protected override void Execute_Internal()
        {
            InnotiveDebug.Trace("[Action] PlayerConsoleCameraActionProcess Excute() Name : {0} , GUID : {1}", this._item.Name, this._item.SyncGUID);

            if (this.OnExecuteCameraAction != null)
            {
                this.OnExecuteCameraAction(this, new ConsoleCameraActionEventArgs(this._item));
            }
        }
    }
}