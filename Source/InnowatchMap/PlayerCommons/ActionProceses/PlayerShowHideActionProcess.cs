// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PlayerShowHideActionProcess.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   player show hide action process.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.PlayerCommons.ActionProceses
{
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Windows;

    using Innotive.InnoWatch.Commons.Actions;
    using Innotive.InnoWatch.Commons.GuidManagers;
    using Innotive.InnoWatch.CommonUtils.Log;

    /// <summary>
    /// player show hide action process.
    /// </summary>
    public class PlayerShowHideActionProcess : PlayerActionProcess
    {
        #region Constants and Fields

        private readonly string HIDE_TYPE = "Hide";

        private readonly string SHOW_TYPE = "Show";

        private DataShowHideActionProcessItem _item;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="PlayerShowHideActionProcess"/> class.
        /// </summary>
        /// <param name="item">
        /// The item.
        /// </param>
        public PlayerShowHideActionProcess(DataShowHideActionProcessItem item)
        {
            this._startTime = item.StartTime;
            this._item = item;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// execute.
        /// </summary>
        public override void Execute()
        {
            base.Execute();
        }

        #endregion

        #region Methods

        /// <summary>
        /// do dispose.
        /// </summary>
        /// <param name="isManage">
        /// The is manage.
        /// </param>
        protected override void DoDispose(bool isManage)
        {
            if (isManage)
            {
            }

            base.DoDispose(isManage);
        }

        protected override void Execute_Internal()
        {
            InnotiveDebug.Trace("[Action] PlayerShowHideActionProcess Excute() Name : {0} , GUID : {1}", this._item.Name, this._item.SyncGUID);

            List<UIElement> list = GuidManager.GetSyncObjectList(this._item.TargetElementGuid);

            if (list == null)
            {
                return;
            }

            foreach (UIElement element in list)
            {
                // HWLee 11.27 :  ObjectAnimation 의 경우 PlayVisible 을 사용했기 때문에 ObjectAnimation에서
                // Visibility를 바꾸는 동작과 Show/Hide action 에서의 visibility를 바꾸는 동작이 
                // 상충되어 제대로 동작하지 않는 버그가 있었다. 둘다 PlayVisible 을 바꾸는 것으로 교체.
                bool ShowType = false;

                if (this._item.ShowHideType == this.SHOW_TYPE)
                {
                    ShowType = true;
                }
                else if (this._item.ShowHideType == this.HIDE_TYPE)
                {
                    ShowType = false;
                }

                PropertyDescriptorCollection collection = TypeDescriptor.GetProperties(element);

                // Class정보를 포함한체로 검색한다.
                string str = "PlayVisible";
                PropertyDescriptor pd = collection.Find(str, false);
                DependencyPropertyDescriptor dpd = DependencyPropertyDescriptor.FromProperty(pd);
                DependencyProperty dp = dpd.DependencyProperty;

                object obj = pd.Converter.ConvertFromString(ShowType.ToString());

                MainProcess.Instance.ProjectManager.LayoutContentsSyncManager.ChangeElementProperty(
                    this._item.TargetElementGuid,
                    dp,
                    obj,
                    this._item.ActionEffect,
                    this._item.StartTime,
                    this._item.Duration
                    );
            }
        }

        #endregion
    }
}