// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PlayerExecuteFileActionProcess.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   player execute file action process.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.PlayerCommons.ActionProceses
{
    using System.Diagnostics;
    using System.IO;
    using Innotive.InnoWatch.Commons.Actions;
    using Innotive.InnoWatch.CommonUtils.Log;

    /// <summary>
    /// player execute file action process.
    /// </summary>
    public class PlayerExecuteFileActionProcess : PlayerActionProcess
    {
        #region Constants and Fields

        private DataExecuteFileActionProcessItem _item;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="PlayerExecuteFileActionProcess"/> class.
        /// </summary>
        /// <param name="item">
        /// The item.
        /// </param>
        public PlayerExecuteFileActionProcess(DataExecuteFileActionProcessItem item)
        {
            this._startTime = item.StartTime;
            this._item = item;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// execute.
        /// </summary>
        public override void Execute()
        {
            base.Execute();
        }

        #endregion

        #region Methods

        /// <summary>
        /// do dispose.
        /// </summary>
        /// <param name="isManage">
        /// The is manage.
        /// </param>
        protected override void DoDispose(bool isManage)
        {
            base.DoDispose(isManage);
        }

        protected override void Execute_Internal()
        {
            InnotiveDebug.Trace("[Action] PlayerExecuteFileActionProcess Excute() Name : {0} , GUID : {1}", this._item.Name, this._item.SyncGUID);

            var process = new Process();
            process.StartInfo.FileName = this._item.FilePath;
            process.StartInfo.Arguments = this._item.Arguments;

            if (string.Compare(this._item.ProcessWindowStyle, ProcessWindowStyle.Hidden.ToString(), true) == 0)
            {
                process.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
            }
            if (string.Compare(this._item.ProcessWindowStyle, ProcessWindowStyle.Maximized.ToString(), true) == 0)
            {
                process.StartInfo.WindowStyle = ProcessWindowStyle.Maximized;
            }
            if (string.Compare(this._item.ProcessWindowStyle, ProcessWindowStyle.Minimized.ToString(), true) == 0)
            {
                process.StartInfo.WindowStyle = ProcessWindowStyle.Minimized;
            }
            if (string.Compare(this._item.ProcessWindowStyle, ProcessWindowStyle.Normal.ToString(), true) == 0)
            {
                process.StartInfo.WindowStyle = ProcessWindowStyle.Normal;
            }

            var executeFileInfo = new FileInfo(this._item.FilePath);
            if (!executeFileInfo.Exists)
            {
                return;
            }

            process.Start();
        }

        #endregion
    }
}