// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PlayerToggleChangePropertyActionProcess.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   player toggle change property action process.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.PlayerCommons.ActionProceses
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Windows;

    using Innotive.InnoWatch.Commons.Actions;
    using Innotive.InnoWatch.Commons.GuidManagers;
    using Innotive.InnoWatch.CommonUtils.Log;

    /// <summary>
    /// player toggle change property action process.
    /// </summary>
    public class PlayerToggleChangePropertyActionProcess : PlayerActionProcess
    {
        #region Constants and Fields

        private readonly List<TogglePropertyInfo> _list = new List<TogglePropertyInfo>();

        private readonly DataToggleChangePropertyActionProcessItem _item;

        private bool _toggle = false;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="PlayerToggleChangePropertyActionProcess"/> class.
        /// </summary>
        /// <param name="item">
        /// The item.
        /// </param>
        public PlayerToggleChangePropertyActionProcess(DataToggleChangePropertyActionProcessItem item)
        {
            this._startTime = item.StartTime;
            this._item = item;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets TargetElementGuid.
        /// </summary>
        public Guid TargetElementGuid { get; set; }

        #endregion

        #region Public Methods

        /// <summary>
        /// execute.
        /// </summary>
        public override void Execute()
        {
            base.Execute();
        }

        #endregion

        #region Methods

        /// <summary>
        /// do dispose.
        /// </summary>
        /// <param name="isManage">
        /// The is manage.
        /// </param>
        protected override void DoDispose(bool isManage)
        {
            if (isManage)
            {
            }

            this._list.Clear();
            base.DoDispose(isManage);
        }

        protected override void Execute_Internal()
        {
            InnotiveDebug.Trace("[Action] PlayerToggleChangePropertyActionProcess Excute() Name : {0} , GUID : {1}", this._item.Name, this._item.SyncGUID);

            var element = GuidManager.GetSyncObject(this.TargetElementGuid) as FrameworkElement;
            if (element == null)
            {
                return;
            }

            foreach (TogglePropertyInfo info in this._list)
            {
                try
                {
                    var collection = TypeDescriptor.GetProperties(element);

                    var pd = collection.Find(info.PropertyName, false);
                    if (pd == null)
                    {
                        continue;
                    }

                    object obj = null;

                    if (!this._toggle)
                    {
                        obj = pd.Converter.ConvertFromString(info.PropertyValue);
                    }
                    else
                    {
                        obj = pd.Converter.ConvertFromString(info.PropertyOldValue);
                    }

                    DependencyPropertyDescriptor dpd = DependencyPropertyDescriptor.FromProperty(pd);
                    DependencyProperty dp = dpd.DependencyProperty;
                    MainProcess.Instance.ProjectManager.LayoutContentsSyncManager.ChangeElementProperty(
                        this.TargetElementGuid,
                        dp,
                        obj,
                        this._item.ActionEffect,
                        this._item.StartTime,
                        this._item.Duration
                        );
                }
                catch
                {
                    // 예외 처리 후...
                    throw;
                }
            }

            this._toggle = !this._toggle;
        }

        private void AddPropertyInfo(TogglePropertyInfo info)
        {
            this._list.Add(info);
        }

        #endregion
    }
}