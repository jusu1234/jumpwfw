// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PlayerConsoleActionProcess.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   player console action process.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.PlayerCommons.ActionProceses
{
    using System;

    using Innotive.InnoWatch.Commons.Actions;
    using Innotive.InnoWatch.Commons.Publics;
    using Innotive.InnoWatch.CommonUtils.Log;

    public class ConsoleFavoriteActionEventArgs : EventArgs
    {
        public DataConsoleFavoriteActionProcessItem Item { get; private set; }

        public ConsoleFavoriteActionEventArgs(DataConsoleFavoriteActionProcessItem item)
        {
            this.Item = item;
        }
    }

    /// <summary>
    /// player console action process.
    /// </summary>
    public class PlayerConsoleFavoriteActionProcess : PlayerActionProcess
    {
        private DataConsoleFavoriteActionProcessItem _item;

        /// <summary>
        /// Initializes a new instance of the <see cref="PlayerConsoleActionProcess"/> class.
        /// </summary>
        /// <param name="item">
        /// The item.
        /// </param>
        public PlayerConsoleFavoriteActionProcess(DataConsoleFavoriteActionProcessItem item)
        {
            this._startTime = item.StartTime;
            this._item = item;
        }

        public event EventHandler<ConsoleFavoriteActionEventArgs> OnExecuteFavoriteAction;

        /// <summary>
        /// execute.
        /// </summary>
        public override void Execute()
        {
            base.Execute();
        }

        /// <summary>
        /// do dispose.
        /// </summary>
        /// <param name="isManage">
        /// The is manage.
        /// </param>
        protected override void DoDispose(bool isManage)
        {
            if (isManage)
            {
            }

            this.OnExecuteFavoriteAction = null;

            base.DoDispose(isManage);
        }

        protected override void Execute_Internal()
        {
            InnotiveDebug.Trace("[Action] PlayerConsoleFavoriteActionProcess Excute() Name : {0} , GUID : {1}", this._item.Name, this._item.SyncGUID);

            if (this.OnExecuteFavoriteAction != null)
            {
                this.OnExecuteFavoriteAction(this, new ConsoleFavoriteActionEventArgs(this._item));
            }
        }
    }
}