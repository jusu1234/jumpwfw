// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PlayerChangeLayoutActionProcess.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   player change layout action process.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.PlayerCommons.ActionProceses
{
    using System.Windows.Input;

    using Innotive.InnoWatch.Commons.Actions;
    using Innotive.InnoWatch.Commons.Configs;
    using Innotive.InnoWatch.CommonUtils.Log;

    /// <summary>
    /// player change layout action process.
    /// </summary>
    public class PlayerChangeLayoutActionProcess : PlayerActionProcess
    {
        #region Constants and Fields

        private readonly DataChangeLayoutActionProcessItem item;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="PlayerChangeLayoutActionProcess"/> class.
        /// </summary>
        /// <param name="item">
        /// The item.
        /// </param>
        public PlayerChangeLayoutActionProcess(DataChangeLayoutActionProcessItem item)
        {
            this._startTime = item.StartTime;
            this.item = item;
        }

        #endregion

        #region Events

        /// <summary>
        /// change layout event.
        /// </summary>
        public event ChangeLayoutActionEventHandler OnChangeLayoutActionEvent;

        #endregion

        #region Public Methods

        /// <summary>
        /// execute.
        /// </summary>
        public override void Execute()
        {
            base.Execute();
        }

        #endregion

        #region Methods

        /// <summary>
        /// do dispose.
        /// </summary>
        /// <param name="isManage">
        /// The is manage.
        /// </param>
        protected override void DoDispose(bool isManage)
        {
            if (isManage)
            {
            }

            this.OnChangeLayoutActionEvent = null;

            base.DoDispose(isManage);
        }

        /// <summary>
        ///  PlayerChangeLayoutAction execute.
        /// </summary>
        protected override void Execute_Internal()
        {
            InnotiveDebug.Trace("[Action] PlayerChangeLayoutActionProcess Excute() Name : {0} , GUID : {1}", this.item.Name, this.item.SyncGUID);

            if (CommonsConfig.Instance.IsShowMouseCursor)
            {
                Mouse.OverrideCursor = Cursors.Wait;
            }

            MainProcess.Instance.ProjectManager.DisplayAreaOpenChangeLayoutSyncManager.ChangeLayout(
                this.item.OriginLayoutGuid,
                this.item.NewLayoutGuid,
                this.item.ActionEffect,
                this.item.StartTime,
                this.item.Duration,
                this.item.SyncGUID);

            Mouse.OverrideCursor = null;

            //event를 발생시킴 !!
            //console stage에서 event를 받아서 처리함 !!
            if (this.OnChangeLayoutActionEvent != null)
            {
                this.OnChangeLayoutActionEvent(this, new ChangeLayoutActionEventArgs(this.item.OriginLayoutGuid, this.item.NewLayoutGuid, this.item.SyncGUID));
            }
        }

        #endregion
    }
}