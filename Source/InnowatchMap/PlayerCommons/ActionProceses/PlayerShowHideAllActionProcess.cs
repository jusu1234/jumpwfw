﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PlayerShowHideAllActionProcess.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   player show hide action process.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.PlayerCommons.ActionProceses
{
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Windows;
    using Innotive.InnoWatch.Commons.Actions;
    using Innotive.InnoWatch.Commons.BaseControls;
    using Innotive.InnoWatch.CommonUtils.Log;
    using Innotive.InnoWatch.DLLs.CameraControls;
    using Innotive.InnoWatch.DLLs.LayoutControls;

    /// <summary>
    /// player show hide action process.
    /// </summary>
    public class PlayerShowHideAllActionProcess : PlayerActionProcess
    {
        #region Constants and Fields

        private const string HIDE_TYPE = "Hide";

        private const string SHOW_TYPE = "Show";

        private DataShowHideAllActionProcessItem _item;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="PlayerShowHideAllActionProcess"/> class.
        /// </summary>
        /// <param name="item">
        /// The item.
        /// </param>
        public PlayerShowHideAllActionProcess(DataShowHideAllActionProcessItem item)
        {
            this._startTime = item.StartTime;
            this._item = item;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// execute.
        /// </summary>
        public override void Execute()
        {
            base.Execute();
        }

        #endregion

        #region Methods

        /// <summary>
        /// do dispose.
        /// </summary>
        /// <param name="isManage">
        /// The is manage.
        /// </param>
        protected override void DoDispose(bool isManage)
        {
            if (isManage)
            {
            }

            base.DoDispose(isManage);
        }

        protected override void Execute_Internal()
        {
            InnotiveDebug.Trace("[Action] PlayerShowHideAllActionProcess Excute() Name : {0}, LayoutGuid : {1}, ControlType : {2}, ShowHideType : {3}", this._item.Name, this._item.SyncGUID, _item.TargetControlType, _item.ShowHideType);

            var layoutControlList = MainProcess.Instance.ProjectManager.PlayerLayoutManager.GetLayoutControlList(_item.LayoutGuid);
            if (layoutControlList == null || layoutControlList.Count <= 0)
                return;

            var targetControlList = new List<BaseControl>();

            //첫번째 Layout에서 show/hide 해야할 element들의 list를 구함
            foreach (BaseControl baseControl in LayoutUtils.GetAllChildObjectList(layoutControlList[0]))
            {
                if (baseControl == null)
                    continue;

                if (_item.TargetControlType.ToUpper() == InnoControlType.Rds.ToString().ToUpper())
                {
                }
                else if (_item.TargetControlType.ToUpper() == InnoControlType.Camera.ToString().ToUpper())
                {
                }
                else if (_item.TargetControlType.ToUpper() == InnoControlType.XamlViewer.ToString().ToUpper())
                {
                }
            }

            foreach (BaseControl control in targetControlList)
            {
                // HWLee 11.27 :  ObjectAnimation 의 경우 PlayVisible 을 사용했기 때문에 ObjectAnimation에서
                // Visibility를 바꾸는 동작과 Show/Hide action 에서의 visibility를 바꾸는 동작이 
                // 상충되어 제대로 동작하지 않는 버그가 있었다. 둘다 PlayVisible 을 바꾸는 것으로 교체.
                var ShowType = false;

                if (this._item.ShowHideType == SHOW_TYPE)
                {
                    ShowType = true;
                }
                else if (this._item.ShowHideType == HIDE_TYPE)
                {
                    ShowType = false;
                }

                var collection = TypeDescriptor.GetProperties(control);

                // Class정보를 포함한체로 검색한다.
                var str = "PlayVisible";

                PropertyDescriptor pd = collection.Find(str, false);
                DependencyPropertyDescriptor dpd = DependencyPropertyDescriptor.FromProperty(pd);
                DependencyProperty dp = dpd.DependencyProperty;

                var obj = pd.Converter.ConvertFromString(ShowType.ToString());

                MainProcess.Instance.ProjectManager.LayoutContentsSyncManager.ChangeElementProperty(
                    control.SyncGUID,
                    dp,
                    obj,
                    this._item.ActionEffect,
                    this._item.StartTime,
                    this._item.Duration
                    );
            }
        }

        #endregion
    }
}