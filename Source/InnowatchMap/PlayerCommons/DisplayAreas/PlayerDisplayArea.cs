// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PlayerDisplayArea.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Diplay 영역 객체.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.PlayerCommons.DisplayAreas
{
    using System;
    using System.Windows;
    using System.Windows.Controls;
    using Innotive.InnoWatch.Commons.BaseControls;
    using Innotive.InnoWatch.Commons.Configs;
    using Innotive.InnoWatch.Commons.DisplayAreas;
    using Innotive.InnoWatch.Commons.Publics;
    using Innotive.InnoWatch.Commons.Utils;
    using Innotive.InnoWatch.DLLs.ActionEffects;
    using Innotive.InnoWatch.DLLs.AlarmBorderControls;
    using Innotive.InnoWatch.DLLs.LayoutControls;
    using Innotive.InnoWatch.PlayerCommons.Actions;
    using Innotive.InnoWatch.Commons.Environments;
    using Innotive.InnoWatch.PlayerCommons.UIs.Popups;
    using System.Windows.Threading;
    using Innotive.InnoWatch.CommonUtils.Log;
    using Innotive.InnoWatch.PlayerCommons.Configs;

    /// <summary>
    /// player display area.
    /// </summary>
    public class PlayerDisplayArea : BaseDisplayArea
    {
        //Contents Mode DisplayArea쪽 AlarmBorder 사용 !! (현재 Stage쪽 AlarmBorder는 사용하지 않음)
        private readonly AlarmBorderControl alarmBorderControl = new AlarmBorderControl();

        //IdleTime Action을 실행하기 위한 Timer
        private long lastCheckedIdleActionTimerTicks = 0;
        private readonly DispatcherTimer idleActionTimer = new DispatcherTimer();

        private PlayerActionEffect actionEffect;

        private PlayerDisplayArea()
        {
            this.DisplayCanvas = new Canvas();

            this.InitTimer();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PlayerDisplayArea"/> class.
        /// </summary>
        /// <param name="baseDisplayArea">
        /// The base display area.
        /// </param>
        public PlayerDisplayArea(BaseDisplayArea baseDisplayArea)
        {
            this.DisplayCanvas = new Canvas();
            this.Name = baseDisplayArea.Name;
            this.StartLayoutFileName = baseDisplayArea.StartLayoutFileName;
            this.StartLayoutName = baseDisplayArea.StartLayoutName;
            this.StartLayoutGuid = baseDisplayArea.StartLayoutGuid;
            this.ZIndex = baseDisplayArea.ZIndex;
            this.Rect = baseDisplayArea.Rect;
            this.SyncGuidForOverlayArea = baseDisplayArea.SyncGuidForOverlayArea;
            this.CurrentLayout = baseDisplayArea.CurrentLayout;
            this.IsEnabled = baseDisplayArea.IsEnabled;
            this.NewestLayoutSyncGuid = baseDisplayArea.NewestLayoutSyncGuid;
            this.IdleTimeMinutes = baseDisplayArea.IdleTimeMinutes;
            this.IdleTimeSeconds = baseDisplayArea.IdleTimeSeconds;
            this.IdleActionName = baseDisplayArea.IdleActionName;
            this.IdleActionRepeatForever = baseDisplayArea.IdleActionRepeatForever;

            this.InitTimer();
        }

        private void InitTimer()
        {
            if (string.IsNullOrWhiteSpace(this.IdleActionName))
                return;

            this.idleActionTimer.Tick += new EventHandler(idleActionTimer_Tick);
            this.idleActionTimer.Interval = TimeSpan.FromMilliseconds(1000);
            this.idleActionTimer.IsEnabled = false;
            //this.idleActionTimer.Start();

            this.lastCheckedIdleActionTimerTicks = DateTime.Now.Ticks;

            //Repeat Forever인 경우 처음부터 Timer를 동작시킴 !!
            //Repeat Forever가 아닌 경우 사용자의 Mouse동작이 있은 후 한번 Timer를 동작시킴 !!
            if (this.IdleActionRepeatForever)
                this.RestartTimer();
        }

        void idleActionTimer_Tick(object sender, EventArgs e)
        {
            //경과시간 체크 !!!
            long elapsedTicks = DateTime.Now.Ticks - this.lastCheckedIdleActionTimerTicks;
            TimeSpan elapsedSpan = new TimeSpan(elapsedTicks);
            
            int displayAreaTotalSeconds = this.IdleTimeMinutes * 60 + this.IdleTimeSeconds;
            if (displayAreaTotalSeconds <= 0)
                return;

            if (elapsedSpan.TotalSeconds > displayAreaTotalSeconds)
            {
                if (!string.IsNullOrEmpty(this.IdleActionName) && !string.IsNullOrWhiteSpace(this.IdleActionName))
                {
                    MainProcess.Instance.ProjectManager.PlayerActionManager.ExecuteFromIdleActionName(this.IdleActionName);
                }

                this.ResetTicks();

                //Repeat Forever가 아니면 Timer를 멈춤
                if (!this.IdleActionRepeatForever)
                    this.StopTimer();
            }
        }

        public void RestartTimer()
        {
            if (!PlayerCommonsConfig.Instance.EnableDisplayAreaIdleTimeAction)
                return;

            this.StopTimer();
            this.ResetTicks();

            this.idleActionTimer.IsEnabled = true;
            this.idleActionTimer.Start();

            InnotiveDebug.Trace(2, "[blackRoot38] Restart Timer !!");
        }

        public void StopTimer()
        {
            InnotiveDebug.Trace(2, "[blackRoot38] Stop Timer !!");

            this.idleActionTimer.Stop();
            this.idleActionTimer.IsEnabled = false;
        }

        private void ResetTicks()
        {
            InnotiveDebug.Trace(1, "[blackRoot38] Reset Tick !!");

            this.lastCheckedIdleActionTimerTicks = DateTime.Now.Ticks;
        }

        /// <summary>
        /// PlayerDisplayArea에서 최상위에 존재하는 Canvas이다. /
        /// </summary>
        public Canvas DisplayCanvas { get; private set; }

        /// <summary>
        /// Gets Layout Rect.
        /// </summary>
        public Rect LayoutRect
        {
            get
            {
                return new Rect(0, 0, this.Rect.Width, this.Rect.Height);
            }
        }

        #region Public Methods

        /// <summary>
        /// 모니터에 걸쳐있는 DisplayArea인 경우 걸쳐있는 만큼 생성되도록한다.
        /// currentLayout과 rleatedMirrorManager는 재 생성할 것이므로 복사하지 않는다.
        /// Colne:. 
        /// </summary>
        /// <returns>
        /// </returns>
        public PlayerDisplayArea Clone()
        {
            // Mirror와 CurrentLayout만 빼고 다 복제한다.
            var result = new PlayerDisplayArea
                {
                    Name = this.Name,
                    StartLayoutFileName = this.StartLayoutFileName,
                    StartLayoutGuid = this.StartLayoutGuid,
                    Rect = this.Rect,
                    SyncGuidForOverlayArea = this.SyncGuidForOverlayArea,
                    CurrentLayout = null,
                    IsEnabled = this.IsEnabled,
                    NewestLayoutSyncGuid = Guid.Empty,
                    IdleTimeMinutes = this.IdleTimeMinutes,
                    IdleTimeSeconds = this.IdleTimeSeconds,
                    IdleActionName = this.IdleActionName,
                    IdleActionRepeatForever = this.IdleActionRepeatForever
                };


            return result;
        }

        /// <summary>
        /// DisplayArea가 가지고 있는 Layout의 SyncGuid를 반환한다.
        /// </summary>
        public Guid GetCurrentLayoutGuid()
        {
            return this.NewestLayoutSyncGuid;
        }

        /// <summary>
        /// 알람 표시를 위한 테두리 객체를 Display 영역에 배치한다.
        /// </summary>
        public void MakeAlarmBorderControl()
        {
            // LayoutControl의 좌표를 Virtual좌표값과 연계된 값으로 세팅함 !!
            Canvas.SetLeft(this.alarmBorderControl, 0);
            Canvas.SetTop(this.alarmBorderControl, 0);
            this.alarmBorderControl.Width = this.Rect.Width;
            this.alarmBorderControl.Height = this.Rect.Height;

            this.DisplayCanvas.Children.Add(this.alarmBorderControl);
            Panel.SetZIndex(this.alarmBorderControl, 999);
            this.alarmBorderControl.Visibility = Visibility.Hidden;
        }

        /// <summary>
        /// 레이아웃을 화면에 표출할 위치를 잡는다. 
        /// </summary>
        /// <param name="newLayoutSyncGuid">
        /// 표출할 레이아웃의 싱크 GUID.
        /// </param>
        /// <param name="effect">
        /// 표출 효과를 설정한다. 효과가 없으면 String.Empty를 입력한다.
        /// </param>
        /// <param name="startTime">
        /// 효과 시작 시간을 설정한다.
        /// </param>
        /// <param name="duration">
        /// 효과 동작 시간을 설정한다.
        /// </param>
        /// <param name="loadedActionGuid">
        /// The loaded Action Guid.
        /// </param>
        /// <returns>
        /// 레이아웃을 반환한다.
        /// </returns>
        public override BaseLayoutControl OpenLayout(Guid newLayoutSyncGuid, string effect, double startTime, double duration, Guid loadedActionGuid)
        {
            if (MainProcess.Instance.ApplicationIsPlayerType)
            {
                var virtualMonitorStartPosition = GetVirtualMonitorStartPosition();
                //Canvas.SetLeft(this.DisplayCanvas, this.Rect.Left - virtualMonitorStartPosition.X - MainProcess.Instance.MainWindow.Left);
                //Canvas.SetTop(this.DisplayCanvas, this.Rect.Top - virtualMonitorStartPosition.Y - MainProcess.Instance.MainWindow.Top);
                Canvas.SetLeft(this.DisplayCanvas, this.Rect.Left - virtualMonitorStartPosition.X);
                Canvas.SetTop(this.DisplayCanvas, this.Rect.Top - virtualMonitorStartPosition.Y);
                this.DisplayCanvas.Width = this.Rect.Width;
                this.DisplayCanvas.Height = this.Rect.Height;
            }
            else
            {
                Canvas.SetLeft(this.DisplayCanvas, this.Rect.Left);
                Canvas.SetTop(this.DisplayCanvas, this.Rect.Top);
                this.DisplayCanvas.Width = this.Rect.Width;
                this.DisplayCanvas.Height = this.Rect.Height;
            }

            //if (MainProcess.Instance.ApplicationIsPlayerType)
            //{
            //    Canvas.SetLeft(this._contentsCanvas, 0);
            //    Canvas.SetTop(this._contentsCanvas, 0);
            //    this._contentsCanvas.Width = MainProcess.Instance.MainWindow.Width;
            //    this._contentsCanvas.Height = MainProcess.Instance.MainWindow.Height;
            //}
            //else
            //{
            //    Canvas.SetLeft(this._contentsCanvas, this.Rect.Left);
            //    Canvas.SetTop(this._contentsCanvas, this.Rect.Top);
            //    this._contentsCanvas.Width = this.Rect.Width;
            //    this._contentsCanvas.Height = this.Rect.Height;
            //}

            // IsEnabled가 false인 경우 실제 Layout을 Load하지 않음. SyncGuid만 가지고 있음 !!
            if (!this.IsEnabled || !CommonsConfig.Instance.EnableContentsMode)
            {
                this.CurrentLayout = null;
                this.NewestLayoutSyncGuid = newLayoutSyncGuid;

                this.NewestLayoutLoadedGuid = loadedActionGuid;
                return null;
            }

            var layoutControl = MainProcess.Instance.ProjectManager.PlayerLayoutManager.LoadLayout(newLayoutSyncGuid, loadedActionGuid);
            if (layoutControl == null)
            {
                return null;
            }

            // Animation이 진행중이었다면 전체 Stop함 !!
            if (this.actionEffect != null)
            {
                this.actionEffect.Release();
            }

            this.actionEffect = new PlayerActionEffect();

            LayoutControl oldLayoutControl = null;
            if (this.DisplayCanvas.Children.Count >= 1)
            {
                oldLayoutControl = this.DisplayCanvas.Children[0] as LayoutControl;
            }

            this.RemoveOldLayout();

            this.CurrentLayout = layoutControl;
            this.NewestLayoutSyncGuid = layoutControl.SyncGUID;

            // Clipping 효과를 위해 중간에 _canvasUI를 둠
            this.DisplayCanvas.ClipToBounds = true;

            // LayoutControl의 좌표를 Virtual좌표값과 연계된 값으로 세팅함 !!
            //var point = this.GetStartPosition();
            //Canvas.SetLeft(layoutControl, -point.X);
            //Canvas.SetTop(layoutControl, -point.Y);
            layoutControl.Width = this.Rect.Width;
            layoutControl.Height = this.Rect.Height;

            // 새로운 Layout을 첫번째 index에 추가함 !! (화면에 안보이게 하기 위해)
            this.DisplayCanvas.Children.Insert(0, layoutControl);

            // ZIndex 변경 !!
            if (oldLayoutControl != null)
            {
                Panel.SetZIndex(oldLayoutControl, 0);
            }

            if (layoutControl != null)
            {
                Panel.SetZIndex(layoutControl, 1);
            }

            // 여기서 effect 처리 !!
            // by jhlee at 2009.01.06
            if (effect == null)
            {
                int count = this.DisplayCanvas.Children.Count;

                // for (int i = 1; i < count; i++)
                for (int i = count - 1; i > 0; i--)
                {
                    this.RemoveLayout(this.DisplayCanvas.Children[i] as LayoutControl);
                }
                
                return this.CurrentLayout;
            }

            var layoutActionEffect = new ActionEffectStrategy(effect, layoutControl, oldLayoutControl, startTime, duration);

            layoutActionEffect.Begin(this.RemoveOldLayout);
            
            return this.CurrentLayout;
        }

        /// <summary>
        /// 모니터와 맵핑된 가상모니터중의 최좌상단을 구한다.
        /// </summary>
        /// <returns>
        /// Point를 반환한다.
        /// </returns>
        private static Point GetVirtualMonitorStartPosition()
        {
            //by blackRoot : Application이 Monitor 1개로 뜨는 구조로 변경되면서 소스도 변경함 !!

            //Application이 띄워진 Monitor의 Data를 가져옴 !!
            MonitorData monitorData = null;
            int selectedMonitorCount = MainProcess.Instance.EnvironmentManager.MonitorDataList.Count;
            for (int i = 0; i < selectedMonitorCount; i++)
            {
                MonitorData tempMonitorData = MainProcess.Instance.EnvironmentManager.MonitorDataList[i];
                if (tempMonitorData.MonitorIndex == MainProcess.Instance.PlayerVisibleMonitorIndex)
                {
                    monitorData = tempMonitorData;
                    break;
                }
            }

            if (monitorData == null)
            {
                ConfirmPopupWindow.Show("INNOWATCH 2.0", "잘못된 Monitor Index값입니다.\r\nQuickSetting에 할당되지 않은 Monitor Index값이거나 실제 존재하지 않는 Monitor의 Index값입니다.");
                return new Point(0, 0);
            }

            if (MainProcess.Instance.ProjectManager.MonitorManager.MonitorList.Count <= monitorData.VirtualMonitorIndex)
            {
                ConfirmPopupWindow.Show("INNOWATCH 2.0", "잘못된 Monitor Index값입니다.\r\nQuickSetting에 할당되지 않은 Monitor Index값이거나 실제 존재하지 않는 Monitor의 Index값입니다.");
                return new Point(0, 0);
            }

            // Application이 띄워진 모니터와 매칭되는 가상 모니터의 좌표를 구한다.
            var rect = MainProcess.Instance.ProjectManager.MonitorManager.MonitorList[monitorData.VirtualMonitorIndex].MonitorRect;

            // 가상 모니터의 전체의 시작점을 구한다.
            return new Point(rect.Left, rect.Top);


            /*
            // 첫번째 모니터를 가져온다.
            var monitorData = MainProcess.Instance.EnvironmentManager.MonitorDataList[0];

            // 첫번째 모니터와 매칭되는 가상 모니터의 좌표를 구한다.
            var rect = MainProcess.Instance.ProjectManager.MonitorManager.MonitorList[monitorData.VirtualMonitorIndex].MonitorRect;

            // 첫번째 모니터의 좌표를 구한다.
            var standardSystemMonitorRect = InnoMonitorUtil.GetMonitorRect(monitorData.MonitorIndex);

            // 시스템 모니터 전체의 시작점을 구한다.
            var mostStartPoint = InnoMonitorUtil.GetMostStartPoint();

            // 기준 모니터 시작점과 모니터 전체 시작점의 차이값을 구한다.
            var relativePoint = new Point(standardSystemMonitorRect.Left - mostStartPoint.X, standardSystemMonitorRect.Top - mostStartPoint.Y);
            
            // 가상 모니터의 전체의 시작점을 구한다.
            return new Point(rect.Left - relativePoint.X, rect.Top - relativePoint.Y);
            */
        }

        /// <summary>
        /// show alarm border control.
        /// </summary>
        /// <param name="borderAction">
        /// The border Action.
        /// </param>
        /// <param name="borderColor">
        /// The border Color.
        /// </param>
        public void ShowAlarmBorderControl(BorderActionType borderAction, string borderColor)
        {
            var action = new Action(() =>
            {
                switch (borderAction)
                {
                    case BorderActionType.Enable:
                        this.alarmBorderControl.BorderAction = AlarmBorderControl.BorderActionType.Enable;
                        break;
                    case BorderActionType.Disable:
                        this.alarmBorderControl.BorderAction = AlarmBorderControl.BorderActionType.Disable;
                        break;
                    case BorderActionType.Blink:
                        this.alarmBorderControl.BorderAction = AlarmBorderControl.BorderActionType.Blink;
                        break;
                }

                this.alarmBorderControl.BorderColor = borderColor;
            });

            if (this.alarmBorderControl.Dispatcher.CheckAccess())
            {
                action.Invoke();
            }
            else
            {
                this.alarmBorderControl.Dispatcher.BeginInvoke(action);
            }

            /*
            this._alarmBorderControl.Dispatcher.BeginInvoke(new Action(() =>
                {
                    switch (borderAction)
                    {
                        case BorderActionType.Enable:
                            this._alarmBorderControl.BorderAction = AlarmBorderControl.BorderActionType.Enable;
                            break;
                        case BorderActionType.Disable:
                            this._alarmBorderControl.BorderAction = AlarmBorderControl.BorderActionType.Disable;
                            break;
                        case BorderActionType.Blink:
                            this._alarmBorderControl.BorderAction = AlarmBorderControl.BorderActionType.Blink;
                            break;
                    }

                    this._alarmBorderControl.BorderColor = borderColor;        
                }));
            */
        }
            

        #endregion

        #region Methods

        internal bool IsClone(PlayerDisplayArea area)
        {
            bool result = area.Name == this.Name && area.StartLayoutFileName == this.StartLayoutFileName &&
                          area.StartLayoutGuid == this.StartLayoutGuid && area.Rect == this.Rect;
            return result;
        }

        private void _actionEffect_eFadeInEffectCompleted(object sender, EventArgs e)
        {
            this.RemoveOldLayout();
        }

        private void _actionEffect_eFadeOutEffectCompleted(object sender, EventArgs e)
        {
            this.RemoveOldLayout();
        }
         
        /// <summary>
        /// RemoveLayout:. 
        /// </summary>
        /// <param name="layoutControl">
        /// </param>
        private void RemoveLayout(LayoutControl layoutControl)
        {
            if (layoutControl == null)
            {
                return;
            }

            //by blackRoot : PlayerLayoutManager.LoadLayout()로 이동함 !!
            //this.DetachEventHandler(layoutControl);

            this.DisplayCanvas.Children.Remove(layoutControl);
            MainProcess.Instance.ProjectManager.PlayerLayoutManager.RemoveLayout(layoutControl);
        }

        /// <summary>
        /// 현재 사용되고 있는 Layout을 제외한 다른 레이아웃은 모두 제거한다.
        /// </summary>
        private void RemoveOldLayout()
        {
            // Animation인 경우만 처리하기 위함 !!
            // Animation이 아닌경우는 다른 부분에서 처리함 !!
            if (this.DisplayCanvas.Children.Count <= 1)
            {
                return;
            }

            for (int index = this.DisplayCanvas.Children.Count - 1; index > 0; index--)
            {
                this.RemoveLayout(this.DisplayCanvas.Children[index] as LayoutControl);
            }
        }

        public void Show(bool isShow)
        {
            if (isShow)
            {
                if (this.DisplayCanvas != null)
                    this.DisplayCanvas.Visibility = Visibility.Visible;

                if (this.CurrentLayout != null)
                    this.CurrentLayout.Visibility = Visibility.Visible;
            }
            else
            {
                if (this.DisplayCanvas != null)
                    this.DisplayCanvas.Visibility = Visibility.Hidden;

                if (this.CurrentLayout != null)
                    this.CurrentLayout.Visibility = Visibility.Hidden;
            }
        }

        #endregion
    }
}