﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="EnvironmentFile.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   environment data.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.PlayerCommons.Files
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Runtime.Serialization;
    using System.Text;
    using System.Xml;
    using System.Xml.Serialization;

    using Innotive.InnoWatch.Commons.Environments;
    using Innotive.InnoWatch.Commons.Files;
    using Innotive.InnoWatch.Commons.Utils;
    using Innotive.InnoWatch.CommonUtils.Log;

    /// <summary>
    /// environment data.
    /// </summary>
    [XmlRoot("EnvironmentData")]
    [DataContract]
    public class EnvironmentFile : BaseFile
    {
        #region Constants and Fields

        private const string EXT_NAME = ".xml";

        private string startProjectFilePath;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="EnvironmentFile"/> class.
        /// </summary>
        public EnvironmentFile()
        {
            this.StartProjectFile = new InnoFilePath();
            this.MonitorDataList = new List<MonitorData>();
            this.StageList = new List<Stage>();
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets EnvFile.
        /// </summary>
        public InnoFilePath EnvFile { get; set; }
        
        /// <summary>
        /// Gets or sets MonitorDataList.
        /// </summary>
        [XmlArray("MonitorDataList")]
        [XmlArrayItem("MonitorData", typeof(MonitorData))]
        [DataMember]
        public List<MonitorData> MonitorDataList { get; set; }

        /// <summary>
        /// Gets or sets StageList.
        /// </summary>
        [DataMember]
        public List<Stage> StageList { get; set; }

        /// <summary>
        /// Gets or sets StartProjectFile.
        /// </summary>
        [DataMember]
        public InnoFilePath StartProjectFile { get; set; }

        /// <summary>
        /// Gets or sets StartProjectFilePath.
        /// </summary>
        [XmlElement("StartProjectFilePath")]
        public string StartProjectFilePath
        {
            get
            {
                return this.startProjectFilePath;
            }

            set
            {
                this.startProjectFilePath = value;
                this.StartProjectFile = new InnoFilePath(this.startProjectFilePath);
            }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// deserialize from xml data.
        /// </summary>
        /// <param name="xmlData">
        /// The xml data.
        /// </param>
        /// <returns>
        /// EnvironmentFile 개체.
        /// </returns>
        [Obsolete("BaseFile.LoadBaseFile<T> 메서드를 사용해 주시기 바랍니다.")]
        public static EnvironmentFile DeserializeFromXmlData(string xmlData)
        {
            try
            {
                using (var stringReader = new StringReader(xmlData))
                {
                    var xmlReader = new XmlTextReader(stringReader);
                    var serializer = new XmlSerializer(typeof(EnvironmentFile));
                    var environmentFile = serializer.Deserialize(xmlReader) as EnvironmentFile;

                    xmlReader.Close();
                    return environmentFile;
                }
            }
            catch (Exception ex)
            {
                InnotiveDebug.Trace(4, "EnvironmentData.DeserializeFromXmlData() Exception발생 ==> {0}", ex.Message);

                return null;
            }
        }

        /// <summary>
        /// deserialize from xml file.
        /// </summary>
        /// <param name="fileName">
        /// The file name.
        /// </param>
        /// <returns>
        /// EnvironmentFile 개체.
        /// </returns>
        [Obsolete("BaseFile.LoadBaseFile<T> 메서드를 사용해 주시기 바랍니다.")]
        public static EnvironmentFile DeserializeFromXmlFile(string fileName)
        {
            if (File.Exists(fileName) == false)
            {
                return new EnvironmentFile();
            }

            using (TextReader streamReader = new StreamReader(fileName))
            {
                string xmlData = streamReader.ReadToEnd();

                return DeserializeFromXmlData(xmlData);
            }
        }

        /// <summary>
        /// get ext name.
        /// </summary>
        /// <returns>
        /// The get ext name.
        /// </returns>
        public override string GetExtName()
        {
            return EXT_NAME;
        }

        /// <summary>
        /// serialize class to xml data.
        /// </summary>
        /// <returns>
        /// The serialize class to xml data.
        /// </returns>
        [Obsolete("SaveToFile 메서드를 사용해 주시기 바랍니다.")]
        public string SerializeClassToXmlData()
        {
            // Tab, 줄바꿈
            var settings = new XmlWriterSettings();
            settings.Indent = true;
            settings.IndentChars = new string(' ', 4);

            // settings.NewLineOnAttributes = true;
            settings.Encoding = Encoding.UTF8;

            using (var memStream = new MemoryStream())
            {
                XmlWriter xmlWriter = XmlWriter.Create(memStream, settings);

                var serializer = new XmlSerializer(typeof(EnvironmentFile));
                serializer.Serialize(xmlWriter, this);

                xmlWriter.Close();
                memStream.Close();

                string xmlData = Encoding.UTF8.GetString(memStream.GetBuffer());
                xmlData = xmlData.Substring(xmlData.IndexOf('<'));
                xmlData = xmlData.Substring(0, xmlData.LastIndexOf('>') + 1);

                return xmlData;
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// 역직렬화가 끝나고 Display 정보(OverlayCameraAssignDisplayAreaList)를 채운다. 
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        [OnDeserialized]
        internal void OnDeserializedMethod(StreamingContext context)
        {
            InnoFilePath projectFilePath = this.StartProjectFile ?? new InnoFilePath();

            // StartProjectFilePath 설정
            this.StartProjectFilePath = projectFilePath.Path;
        }

        #endregion
    }
}