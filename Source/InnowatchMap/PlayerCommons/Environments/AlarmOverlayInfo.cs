// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AlarmOverlayInfo.cs" company="Innotive">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   alarm overlay info.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.PlayerCommons.Environments
{
    using System.Runtime.Serialization;
    using System.Xml.Serialization;

    /// <summary>
    /// alarm overlay info.
    /// </summary>
    [XmlRoot("AlarmOverlayInfo")]
    [DataContract]
    public class AlarmOverlayInfo
    {
        #region Constants and Fields

        private string _alarmLayoutName;

        private string _linkedDisplayAreaName;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="AlarmOverlayInfo"/> class.
        /// </summary>
        public AlarmOverlayInfo()
        {
            this._linkedDisplayAreaName = string.Empty;
            this._alarmLayoutName = string.Empty;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets AlarmLayoutName.
        /// </summary>
        [XmlAttribute("AlarmLayoutName")]
        [DataMember]
        public string AlarmLayoutName
        {
            get
            {
                return this._alarmLayoutName;
            }

            set
            {
                this._alarmLayoutName = value;
            }
        }

        /// <summary>
        /// Gets or sets LinkedDisplayAreaName.
        /// </summary>
        [XmlAttribute("LinkedDisplayAreaName")]
        [DataMember]
        public string LinkedDisplayAreaName
        {
            get
            {
                return this._linkedDisplayAreaName;
            }

            set
            {
                this._linkedDisplayAreaName = value;
            }
        }

        #endregion
    }
}