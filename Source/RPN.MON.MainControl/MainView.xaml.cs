﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;


using System.ComponentModel;
using RPN.Common.Core.ViewModel;
using RPN.Common.Core;

namespace RPN.MON.MainControl
{
    /// <summary>
    /// UserControl1.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class MainView : UserControl
    {
        ModuleFactoryLoader loader_;
        public MainView()
        {
            InitializeComponent();

            // 디자인 모드시 오류 발생 방지.
            if (!DesignerProperties.GetIsInDesignMode(this))
            {   
                loader_ = new ModuleFactoryLoader();
                loader_.Load(".\\");

                // Tab 추가
                Dictionary<string, TabItem> tabItemList = new Dictionary<string, TabItem>();

                foreach (var lazy in loader_.ViewModels)
                {
                    IViewModel vm = lazy.Value;
                    TabItem tabItem = new TabItem();
                    tabItem.Content = ((ViewModelBase)vm).ViewRef;
                    tabItem.Header = vm.ViewName;
                    tabItem.Padding = new Thickness(1, 30, 1, 1);
                    tabItem.Margin = new Thickness(-2);
                    tabItem.Style = (Style)this.FindResource("sTabItem_Main");
                    tabItemList.Add(vm.ViewName, tabItem);
                }

                this.Region.Items.Add(tabItemList["System"]);
                this.Region.Items.Add(tabItemList["Camera"]);
                this.Region.Items.Add(tabItemList["Report"]);
                this.Region.Items.Add(tabItemList["Alram"]);
            }
        }
    }
}
