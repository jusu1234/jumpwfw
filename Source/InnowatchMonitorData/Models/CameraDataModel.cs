﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InnowatchMonitorData.Models
{
    public class CameraDataModel
    {
        public const string CAMERA_ID = "CameraID";
        public const string RECORD_CAMERA_NUMBER = "RecordCameraNumber";
        public const string MEDIA_SERVER_ID = "MediaServerID";

        public string CameraID { get; private set; }
        public int? RecordCameraNumber { get; private set; }
        public string MediaServerID { get; private set; }

        public CameraDataModel(string cameraID, int? recordCameraNumber, string mediaServerId)
        {
            CameraID = cameraID;
            RecordCameraNumber = recordCameraNumber;
            MediaServerID = mediaServerId;
        }
    }
}


