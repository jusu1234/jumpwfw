﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Collections.ObjectModel;
using RPN.Common.Core;


namespace InnowatchMonitorData.Models
{
    public class CameraInfo
    {
        public string RecordStatus { get; set; }

        public CameraInfo(string recoardStatus)
        {
            RecordStatus = recoardStatus;
        }
    }

    public class CameraMonitor : NotificationObject, IEquatable<CameraMonitor>
    {
        public bool isChecked_;
        public string cameraID_;

        public bool IsChecked
        {
            get
            {
                return isChecked_;
            }
            
            set
            {
                isChecked_ = value;
                RaisePropertyChanged(() => IsChecked);
            }
        }

        public string CameraID
        {
            get
            {
                return cameraID_;
            }

            set
            {
                cameraID_ = value;
                RaisePropertyChanged(() => CameraID);
            }
        }

        public int? RecordNo { get; set; }
        public string Rc { get; set; }
        public string Md { get; set; }
        public int Ping { get; set; }        
        public string RecordStatus { get; set; }
        public DateTime CheckTime { get; set; }
        public CameraInfo Camera { get; set; }

        public bool Equals(CameraMonitor other)
        {
            return this.CameraID.CompareTo(other.CameraID) == 0;
        }
    }
}
