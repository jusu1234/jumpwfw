﻿using System;
using System.Collections.Generic;

namespace InnowatchMonitorData.Models
{
    public class ServerDataModelContainer
    {
        Dictionary<string, ServerDataModelBase> serverDataList_;

        private static ServerDataModelContainer instance_ = null;

        private ServerDataModelContainer()
        {
            Initialize();
        }

        public static ServerDataModelContainer GetInstance
        {
            get
            {
                if (instance_ == null)
                {
                    instance_ = new ServerDataModelContainer();
                }

                return instance_;
            }
        }

        private void Initialize()
        {
            serverDataList_ = new Dictionary<string, ServerDataModelBase>();


           

            List<DisplayServerData> displayServerDataList = MockServerDataModelFactory.GetDisplayServerData();

            foreach (DisplayServerData data in displayServerDataList)
            {
                serverDataList_.Add(data.ServerID, data);
            }

            List<MediaServerData> mediaServerDataList = MockServerDataModelFactory.GetMediaServerData();


            foreach (MediaServerData data in mediaServerDataList)
            {
                serverDataList_.Add(data.ServerID, data);
            }

            List<RecordServerData> recordServerDataList = MockServerDataModelFactory.GetRecoredServerData();

            foreach (RecordServerData data in recordServerDataList)
            {
                serverDataList_.Add(data.ServerID, data);
            }


            List<ViewServerData> viewServerDataList = MockServerDataModelFactory.GetViewServerData();

            foreach (ViewServerData data in viewServerDataList)
            {
                serverDataList_.Add(data.ServerID, data);
            }


          
        }

        public Dictionary<string, ServerDataModelBase> GetServerDataList()
        {

            return serverDataList_;
            
        }


    }

    
}
