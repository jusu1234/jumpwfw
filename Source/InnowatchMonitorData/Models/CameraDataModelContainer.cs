﻿using System;
using System.Collections.Generic;

namespace InnowatchMonitorData.Models
{
    public class CameraDataModelContainer
    {
        Dictionary<string, CameraDataModel> cameraDataList_;

        private static CameraDataModelContainer instance_ = null;

        private CameraDataModelContainer()
        {
            Initialize();
        }

        public static CameraDataModelContainer GetInstance
        {
            get
            {
                if (instance_ == null)
                {
                    instance_ = new CameraDataModelContainer();
                }

                return instance_;
            }
        }

        private void Initialize()
        {
            cameraDataList_ = new Dictionary<string, CameraDataModel>();

            List<CameraDataModel> cameraDataList = CameraDataModelFactory.GetCameraData();

            foreach (CameraDataModel data in cameraDataList)
            {
                cameraDataList_.Add(data.CameraID, data);
            }
        }

        public Dictionary<string, CameraDataModel> GetCameraDataList()
        {

            return cameraDataList_;
        }
    }
}
