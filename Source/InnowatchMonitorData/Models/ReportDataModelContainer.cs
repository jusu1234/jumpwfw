﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InnowatchMonitorData.Models
{
    class ReportDataModelContainer
    {

        Dictionary<string, ReportDataModel> _reportDataList;

        private static ReportDataModelContainer instance_ = null;

        public ReportDataModelContainer()
        {
        }
        public static ReportDataModelContainer GetInstance
        {
            get
            {
                if (instance_ == null)
                {
                    instance_ = new ReportDataModelContainer();
                }

                return instance_;
            }
        }

        private void Initialize()
        {
            _reportDataList = new Dictionary<string, ReportDataModel>();

            /*
            List<CameraDataModel> cameraDataList = CameraDataModelFactory.GetCameraData();

            foreach (CameraDataModel data in cameraDataList)
            {
                _reportDataList.Add(data.CameraID, data);
            }
             * */
        }

        public Dictionary<string, ReportDataModel> GetCameraDataList()
        {

            return _reportDataList;
        }
    }
}
