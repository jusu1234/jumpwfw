﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Collections.ObjectModel;
using RPN.Common.Core;


namespace InnowatchMonitorData.Models
{

    public class MemoryInfo
    {
        public int TotalMemory { get; private set; }
        public int AvailMemory { get; private set; }
        public int MemoryLoad { get; private set; }

        public MemoryInfo(int totalMemory, int availMemroy, int memoryLoad)
        {
            TotalMemory = totalMemory;
            AvailMemory = availMemroy;
            MemoryLoad = memoryLoad;
        }
    }

    public class HddInfo 
    {
        public string DriveLetter { get; private set; }
        public int FreeMByte { get; private set; }
        public int TotalMByte { get; private set; }

        public HddInfo(string driveLetter, int freeMByte, int totalByte)
        {
            DriveLetter = driveLetter;
            FreeMByte = freeMByte;
            TotalMByte = totalByte;
        }
    }

    public class SystemMonitor : NotificationObject, IEquatable<SystemMonitor>
    {
        private bool isChecked_;
        private string serverID_;


        public bool IsChecked 
        {
            get
            {
                return isChecked_;
            }

            set
            {
                isChecked_ = value;
                RaisePropertyChanged( () => IsChecked);
            }
        }

       
        public string ServerID 
        {
            get
            {
                return serverID_;
            }

            set
            {
                serverID_ = value;
                RaisePropertyChanged(() => ServerID);
            }
        }


        public int Ping { get; set; }
        public int Cpu { get; set; }
        public MemoryInfo Memory { get; set; }
        public IEnumerable<HddInfo> Hdds { get; set; }
        public DateTime Check_Time { get; set; }
        public bool IsCamera { get; set; }

        public bool Equals(SystemMonitor other)
        {

            return this.ServerID.CompareTo(other.ServerID) == 0;
        }

     
    }


    
}
