﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.IO;
using System.Configuration;

using RPN.Common.Util;
using InnowatchDataHandler;

namespace InnowatchMonitorData.Models
{
    /// <summary>
    /// 전체 카메라에 대한 정보를 DB/DataServier를 통해 가져온다.
    /// </summary>
    internal class CameraDataModelFactory
    {
        // AI 서버쪽에서 실행중인지...
        // AI 서버쪽에서 실행중이면 DB에서 데이터를 가져온다.
        private static bool isServer_ =
            Convert.ToString(ConfigurationManager.AppSettings["PROGRAM_MODE"]).Equals("Server");

        public static List<CameraDataModel> GetCameraData()
        {
            List<CameraDataModel> cameraDataList = new List<CameraDataModel>();

            DataTable dt = null;

            if (isServer_)
            {
                var resultString = DataManager.GetInstance.GetDataForConfiguration.GetAllCameraInfo();
                var bytes = Encoding.UTF8.GetBytes(resultString);

                MemoryStream ms = new MemoryStream(bytes);

                DataSet ds = new DataSet();
                ds.ReadXml(ms);


                dt = ds.Tables[0];
            }
            else
            {

                dt = RestServiceHelper.RequestToDataTable(RestServiceHelper.DATA_SERVICE_URL + "GetAllCameraInfoForConfiguration", false);
            }

            if (dt == null)
                return cameraDataList;

            foreach (DataRow row in dt.Rows)
            {
                
                object o1 = (string) row[CameraDataModel.CAMERA_ID];
                object o2 = row[CameraDataModel.RECORD_CAMERA_NUMBER];

                if (o2 is DBNull)
                    o2 = null;

                object o3 = row[CameraDataModel.MEDIA_SERVER_ID];

                if (o3 is DBNull)
                    o3 = string.Empty;
                
                CameraDataModel cameraInfo =
                    new CameraDataModel(
                        (string)o1,
                        (int?)o2,
                        (String)o3
                    );

                cameraDataList.Add(cameraInfo);
                 
            }

            return cameraDataList;
        }
    }
}
