﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Data;
using System.IO;

using RPN.Common.Util;

using InnowatchDataHandler;

namespace InnowatchMonitorData.Models
{
    /// <summary>
    /// 전체 서버에 대한 정보를 DB/DataServier를 통해 가져온다.
    /// </summary>
    internal class ServerDataModelFactory
    {
        // AI 서버쪽에서 실행중인지...
        // AI 서버쪽에서 실행중이면 DB에서 데이터를 가져온다.
        private static bool isServer_ =
            Convert.ToString(ConfigurationManager.AppSettings["PROGRAM_MODE"]).Equals("Server");


        public static List<MediaServerData> GetMediaServerData()
        {

            List<MediaServerData> mediaServerDataList = new List<MediaServerData>();

            DataTable dt = null;

            if (isServer_)
            {

                var resultString = DataManager.GetInstance.GetDataForConfiguration.GetMediaServerInfo();
                var bytes = Encoding.UTF8.GetBytes(resultString);

                MemoryStream ms = new MemoryStream(bytes);

                DataSet ds = new DataSet();
                ds.ReadXml(ms);


                dt = ds.Tables[0];
            }
            else
            {

                dt = RestServiceHelper.RequestToDataTable(RestServiceHelper.DATA_SERVICE_URL + "GetMediaServerInfo", false);

            }

            if (dt == null)
                return mediaServerDataList;


            foreach (DataRow row in dt.Rows)
            {


                MediaServerData serverInfo =
                    new MediaServerData(
                        (string)row[MediaServerData.MD_ID],
                        (string)row[MediaServerData.MD_IP],
                        (string)row[MediaServerData.MD_SERVICE_URL]
                    );

                mediaServerDataList.Add(serverInfo);
            }
            
           

            return mediaServerDataList;
        }



        public static List<DisplayServerData> GetDisplayServerData()
        {

            List<DisplayServerData> displayServerDataList = new List<DisplayServerData>();


            DataTable dt = null;
            if (isServer_)
            {
                var resultString = DataManager.GetInstance.GetDataForConfiguration.GetDisplayInfo();
                var bytes = Encoding.UTF8.GetBytes(resultString);

                MemoryStream ms = new MemoryStream(bytes);

                DataSet ds = new DataSet();
                ds.ReadXml(ms);


                dt = ds.Tables[0];
            }
            else
            {

                dt = RestServiceHelper.RequestToDataTable(RestServiceHelper.DATA_SERVICE_URL + "GetDisplayInfo", false);

                
            }

            if (dt == null)
                return displayServerDataList;

            foreach (DataRow row in dt.Rows)
            {
                DisplayServerData serverInfo =
                    new DisplayServerData(
                        (string)row[DisplayServerData.DP_ID],
                        (string)row[DisplayServerData.DP_IP],
                        (string)row[DisplayServerData.DP_SERVICE_URL]
                    );

                displayServerDataList.Add(serverInfo);
            }

            return displayServerDataList;
        }

        public static List<ViewServerData> GetViewServerData()
        {

            List<ViewServerData> viewServerDataList = new List<ViewServerData>();

            DataTable dt = null;
            if (isServer_)
            {
                var resultString = DataManager.GetInstance.GetDataForConfiguration.GetViewServerInfo();
                var bytes = Encoding.UTF8.GetBytes(resultString);

                MemoryStream ms = new MemoryStream(bytes);

                DataSet ds = new DataSet();
                ds.ReadXml(ms);


                dt = ds.Tables[0];
            }
            else
            {

                dt = RestServiceHelper.RequestToDataTable(RestServiceHelper.DATA_SERVICE_URL + "GetViewServerInfo", false);

                
            }

            if (dt == null)
                return viewServerDataList;


            foreach (DataRow row in dt.Rows)
            {
                ViewServerData serverInfo =
                    new ViewServerData(
                        (string)row[ViewServerData.VE_ID],
                        (string)row[ViewServerData.VE_IP],
                        (string)row[ViewServerData.VE_SERVICE_URL]
                    );

                viewServerDataList.Add(serverInfo);
            }

            return viewServerDataList;
        }


        public static List<RecordServerData> GetRecoredServerData()
        {

            List<RecordServerData> recordServerDataList = new List<RecordServerData>();

            DataTable dt = null;

            if (isServer_)
            {
                var resultString = DataManager.GetInstance.GetDataForConfiguration.GetRecorderInfo();
                var bytes = Encoding.UTF8.GetBytes(resultString);

                MemoryStream ms = new MemoryStream(bytes);

                DataSet ds = new DataSet();
                ds.ReadXml(ms);


                dt = ds.Tables[0];
            }
            else
            {

                dt = RestServiceHelper.RequestToDataTable(RestServiceHelper.DATA_SERVICE_URL + "GetRecorderInfo", false);

                
            }

            if (dt == null)
                return recordServerDataList;


            foreach (DataRow row in dt.Rows)
            {


                RecordServerData serverInfo =
                    new RecordServerData(
                        (string)row[RecordServerData.RC_ID],
                        (string)row[RecordServerData.RC_IP],
                        (string)row[RecordServerData.RC_SERVICE_URL]
                    );

                recordServerDataList.Add(serverInfo);
            }

            return recordServerDataList;
        }
    }
}
