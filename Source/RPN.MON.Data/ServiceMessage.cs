﻿using System;

using RPN.Common.Core.Message;

namespace RPN.MON.Data
{
    public class ServiceMessage : Messages
    {
        #region System

        #region DoRefresh SystemStatus

        public const string DoRefreshSystemStatus = "DoRefreshSystemStatus";
        public const string GetRefreshSystemStatus = "GetRefreshSystemStatus";

        #endregion

        #region BackGround Data Fetching

        public const string GetSystemStatus = "GetSystemStatus";
        public const string ReceviedSystemStatus = "ReceviedSystemStatus";

        #endregion

        #region SystemStatusService Control

        public const string ControlSystemStatusService = "ControlSystemStatusService";

        #endregion

        #region Server Status History

        public const string GetServerStatusHistory = "GetServerStatusHistory";
        public const string SearchSystemStatusHistory = "SearchSystemStatusHistory";

        #endregion

        #endregion

        #region Camera

        #region DoRefresh CameraStatus

        public const string DoRefreshCameraStatus = "DoRefreshCameraStatus";
        public const string GetRefreshCameraStatus = "GetRefreshCameraStatus";

        #endregion

        #region BackGround Data Fetching

        public const string GetCameraStatus = "GetCameraStatus";
        public const string ReceviedCameraStatus = "ReceviedCameraStatus";

        #endregion

        #region CameraStatusService Control

        public const string ControlCameraStatusService = "ControlCameraStatusService";

        #endregion

        #endregion
    }
}
