﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;
using System.Globalization;

using RPN.MON.Data.Models;

namespace RPN.MON.Data.Converters
{
    public class PingValueConverter : IValueConverter
    {
        object IValueConverter.Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            int ping = (int)value;

            string pingStr = string.Format("{0} m/s", ping);

            return (object)pingStr;
        }

        object IValueConverter.ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

    }

    public class CpuValueConverter : IValueConverter
    {
        object IValueConverter.Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            int cpu = (int)value;

            string cpuStr = string.Format("{0}%", cpu);

            return (object) cpuStr;
        }

        object IValueConverter.ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class MemoryInfoValueConverter : IValueConverter
    {
        object IValueConverter.Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {

            if (value == null)
                return (object) string.Empty;

            MemoryInfo memInfo = value as MemoryInfo;

            string memStr = string.Format("{0}% ({1}MB / {2}MB)",
                memInfo.MemoryLoad, memInfo.AvailMemory, memInfo.TotalMemory);

            return (object) memStr;
        }

        object IValueConverter.ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class HddInfoValueConverter : IValueConverter
    {
        object IValueConverter.Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
                return (object)string.Empty;

            IEnumerable<HddInfo> hddInfoList = (IEnumerable<HddInfo>)value;

            // Total만 꺼냄
            var hddInfos = ( from h in hddInfoList
                            where h.DriveLetter.Equals("Total")
                            select h).ToList();

            if( hddInfos.Count != 1)
                return (object) string.Empty;

            HddInfo info = hddInfos[0];
            int percent = Convert.ToInt32(
                Convert.ToDouble(info.FreeMByte) / ( Convert.ToDouble(info.TotalMByte) ) * 100);

            string hddStr = string.Format("{0}% ({1}MB / {2}MB)",
                percent, info.FreeMByte, info.TotalMByte);

            return (object) hddStr;

        }

        object IValueConverter.ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class DateTimeValueConverter : IValueConverter
    {
        object IValueConverter.Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            DateTime dt = (DateTime)value;

            string dtStr = dt.ToString("yyyy.MM.dd hh:mm:ss");

            return (object)dtStr;
        }

        object IValueConverter.ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class RecordStatusValueConverter : IValueConverter
    {
        object IValueConverter.Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string statusStr = (int)value == 0 ? "Off" : "On";

            return (object)statusStr;
        }

        object IValueConverter.ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

}
