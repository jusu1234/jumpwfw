﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using RPN.Common.Util;

using RPN.MON.Data.Models;

namespace RPN.MON.Data.Util
{
    public class ServiceWatcherHelper
    {
        public static void GetMemoryInfo(string xmlStr, out MemoryInfo memoryInfo)
        {
            MemoryInfoData mid = new MemoryInfoData();

            memoryInfo = mid.GetData(xmlStr);

        }

        public static void GetHddInfo(string xmlStr, out IEnumerable<HddInfo> hddInfos)
        {
            HddInfoData hid = new HddInfoData();

            hddInfos = hid.GetData(xmlStr);

        }

    }



    internal class MemoryInfoData : IXMLHandler
    {
        private int? totalMemory_;
        private int? availMemory_;
        private int? memoryLoad_;

        public MemoryInfo GetData(string xmlStr)
        {
            XMLParser parser = new XMLParser(this);

            if (!parser.parseString(xmlStr))
            {

                return null;
            }

            if (!totalMemory_.HasValue || !availMemory_.HasValue || !memoryLoad_.HasValue)
            {
                return null;
            }

            return new MemoryInfo(totalMemory_.Value,
                availMemory_.Value,
                memoryLoad_.Value);
        }


        public void startDocument() { }

        public void endDocument() { }

        public void startElement(String uri, String localName, String name, Attributes attributes)
        {
            if (name.Equals("TotalMemory"))
            {
                totalMemory_ = Convert.ToInt32(attributes.getValue("value"));
            }
            else if (name.Equals("AvailMemory"))
            {
                availMemory_ = Convert.ToInt32(attributes.getValue("value"));
            }
            else if (name.Equals("MemoryLoad"))
            {
                memoryLoad_ = Convert.ToInt32(attributes.getValue("value"));
            }
        }

        public void endElement(String uri, String localName, String name)
        {
        }

        public void characters(char[] ch, int start, int length)
        {
        }
    }


    internal class HddInfoData : IXMLHandler
    {
        private List<HddInfo> hddInfoList_;


        private string driveLetter_;
        private int? freeMByte_;
        private int? totalMByte_;

        public List<HddInfo> GetData(string xmlStr)
        {
            hddInfoList_ = new List<HddInfo>();

            XMLParser parser = new XMLParser(this);


            if (!parser.parseString(xmlStr))
            {

                return null;
            }

            return hddInfoList_;
        }


        public void startDocument() { }

        public void endDocument() { }

        public void startElement(String uri, String localName, String name, Attributes attributes)
        {
            if (name.Equals("HDD"))
            {
                driveLetter_ = attributes.getValue("drive");

            }

            if (name.Equals("FreeMByte"))
            {
                freeMByte_ = Convert.ToInt32(attributes.getValue("value"));
            }
            else if (name.Equals("TotalMByte"))
            {
                totalMByte_ = Convert.ToInt32(attributes.getValue("value"));
            }
        }

        public void endElement(String uri, String localName, String name)
        {
            if (name.Equals("HDD"))
            {
                if (string.IsNullOrEmpty(driveLetter_) || !freeMByte_.HasValue || !totalMByte_.HasValue)
                {
                    throw new InvalidOperationException("XML Data Error");
                }

                HddInfo hddInfo = new HddInfo(driveLetter_, freeMByte_.Value, totalMByte_.Value);

                hddInfoList_.Add(hddInfo);

                driveLetter_ = string.Empty;
                freeMByte_ = null;
                totalMByte_ = null;

            }
        }

        public void characters(char[] ch, int start, int length)
        {
        }
    }
}
