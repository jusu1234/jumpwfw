﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Collections.ObjectModel;


namespace RPN.MON.Data.Models
{
    public class MonitorSearchCondition
    {
        public int ViewCondIdx { get; set; }
        public string ViewCondVlu { get; set; }

        public DateTime StartDate { get; set; }
        public String StartTime { get; set; }
        public DateTime EndDate { get; set; }
        public string EndTime { get; set; }

        public string SystemId { get; set; }
    }
}
