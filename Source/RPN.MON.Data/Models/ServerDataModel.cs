﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RPN.MON.Data.Models
{

    public abstract class ServerDataModelBase
    {
        public string ServerType { get; private set; }
        public string ServerID { get; private set; }
        public string ServerIP { get; private set; }
        public string ServiceURL { get; private set; }

        protected ServerDataModelBase(string serverType, string serverID, string serverIP, string serviceURL)
        {
            ServerType = serverType;
            ServerID = serverID;
            ServerIP = serverIP;
            ServiceURL = serviceURL;
        }
    }

    public class MediaServerData : ServerDataModelBase
    {
        public const string MD_ID = "MediaServerID";
        public const string MD_IP = "MediaServerIP";
        public const string MD_SERVICE_URL = "StatusServiceUrl";


        public MediaServerData( string serverID, string serverIP, string serviceURL)
            : base("Media", serverID, serverIP, serviceURL)
        {
        }
    }


    public class RecordServerData : ServerDataModelBase
    {
        public const string RC_ID = "RecorderID";
        public const string RC_IP = "RecorderIP";
        public const string RC_SERVICE_URL = "StatusServiceUrl";

        public RecordServerData( string serverID, string serverIP, string serviceURL)
            : base("Recorder", serverID, serverIP, serviceURL)
        {
        }
    }

    public class DisplayServerData : ServerDataModelBase
    {
        public const string DP_ID = "DisplayID";
        public const string DP_IP = "DisplayIP";
        public const string DP_SERVICE_URL = "ServiceUrl";

        public DisplayServerData(string serverID, string serverIP, string serviceURL)
            : base("Display", serverID, serverIP, serviceURL)
        {
        }
    }


    public class ViewServerData : ServerDataModelBase
    {
        public const string VE_ID = "ViewServerID";
        public const string VE_IP = "ViewServerIP";
        public const string VE_SERVICE_URL = "ServiceUrl";

        public ViewServerData( string serverID, string serverIP, string serviceURL)
            : base("View", serverID, serverIP, serviceURL)
        {
        }
    }
}


