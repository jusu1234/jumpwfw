﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace RPN.MON.Data.Models
{
    public class ServerDataModelContainer
    {
        
        Dictionary<string, ServerDataModelBase> serverDataList_;

        Dictionary<string, IList<CameraDataModel>> carmeraMappingList_;

        private static ServerDataModelContainer instance_ = null;

        private ServerDataModelContainer()
        {
            Initialize();
        }

        public static ServerDataModelContainer GetInstance
        {
            get
            {
                if (instance_ == null)
                {
                    instance_ = new ServerDataModelContainer();
                }

                return instance_;
            }
        }

        private void Initialize()
        {

            carmeraMappingList_ = new Dictionary<string, IList<CameraDataModel>>();
            serverDataList_ = new Dictionary<string, ServerDataModelBase>();


            List<DisplayServerData> displayServerDataList = ServerDataModelFactory.GetDisplayServerData();

            foreach (DisplayServerData data in displayServerDataList)
            {
                serverDataList_.Add(data.ServerID, data);
            }

            List<MediaServerData> mediaServerDataList = ServerDataModelFactory.GetMediaServerData();


            foreach (MediaServerData data in mediaServerDataList)
            {
                serverDataList_.Add(data.ServerID, data);
            }

            List<RecordServerData> recordServerDataList = ServerDataModelFactory.GetRecoredServerData();

            foreach (RecordServerData data in recordServerDataList)
            {
                serverDataList_.Add(data.ServerID, data);
            }


            List<ViewServerData> viewServerDataList = ServerDataModelFactory.GetViewServerData();

            foreach (ViewServerData data in viewServerDataList)
            {
                serverDataList_.Add(data.ServerID, data);
            }
  
        }



        public Dictionary<string, ServerDataModelBase> GetServerDataList()
        {

            return serverDataList_;
            
        }

        public void CreteCameraMappingInfo()
        {

            foreach (CameraDataModel camera in CameraDataModelContainer.GetInstance.GetCameraDataList().Values)
            {

                string mdId = camera.MediaServerID;
                if (!string.IsNullOrEmpty(mdId) && serverDataList_.ContainsKey(mdId))
                {
                    IList<CameraDataModel> cameras;

                    bool isNew = !carmeraMappingList_.TryGetValue(mdId, out cameras);

                    if (isNew)
                    {
                        cameras = new List<CameraDataModel>();
                        carmeraMappingList_.Add(mdId, cameras);
                    }

                    cameras.Add(camera);

                }
            }
        }



        public string GetMediaServiceURL(string mdId)
        {
            if (serverDataList_.ContainsKey(mdId))
            {
                return serverDataList_[mdId].ServiceURL;
            }
            else
                return null;
        }


        public IEnumerable<CameraDataModel> GetCameraListAttachedMediaServer(string mdId)
        {
            if (carmeraMappingList_.ContainsKey(mdId))
            {
                return carmeraMappingList_[mdId];
            }
            else
                return null;
        }

        
    }

    
}
