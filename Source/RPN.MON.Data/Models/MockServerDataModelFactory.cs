﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RPN.MON.Data.Models
{
    /// <summary>
    ///  Test용 클래스...
    /// </summary>
    internal class MockServerDataModelFactory
    {
        static private int cnt_ = 100;
       
        public static List<MediaServerData> GetMediaServerData()
        {

            List<MediaServerData> list = new List<MediaServerData>();

            for (int i = 0; i < cnt_; i++)
            {
                MediaServerData data = new MediaServerData("MD" + Convert.ToString(i),
                    "127.0.0.1", "http://localhost:5000/rest/watcher/");

                list.Add(data);


            }

            return list;
        }


        public static List<DisplayServerData> GetDisplayServerData()
        {
            List<DisplayServerData> list = new List<DisplayServerData>();

            for (int i = 0; i < cnt_; i++)
            {
                DisplayServerData data = new DisplayServerData("DP" + Convert.ToString(i),
                    "127.0.0.1", "http://localhost:5000/rest/watcher/");

                list.Add(data);


            }

            return list;
        }


        public static List<ViewServerData> GetViewServerData()
        {
            List<ViewServerData> list = new List<ViewServerData>();

            for (int i = 0; i < cnt_; i++)
            {
                ViewServerData data = new ViewServerData("VW" + Convert.ToString(i),
                    "127.0.0.1", "http://localhost:5000/rest/watcher/");

                list.Add(data);


            }


            return list;
        }

        public static List<RecordServerData> GetRecoredServerData()
        {
            List<RecordServerData> list = new List<RecordServerData>();

            for (int i = 0; i < cnt_; i++)
            {
                RecordServerData data = new RecordServerData("RC" + Convert.ToString(i),
                    "127.0.0.1", "http://localhost:5000/rest/watcher/");

                list.Add(data);



            }
            return list;
        }

        

    }
}
