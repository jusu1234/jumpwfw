﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Collections.ObjectModel;


namespace RPN.MON.Data.Models
{
    public class CameraMonitor : IEquatable<CameraMonitor>
    {
        public bool IsChecked { get; set; }
        public string CameraID { get; set; }
        public string CameraIP { get; set; }
        public int? RecordNo { get; set; }
        public string Rc { get; set; }
        public string Md { get; set; }
        public int Ping { get; set; }
        public int RecordStatus { get; set; }
        public DateTime CheckTime { get; set; }

        public bool Equals(CameraMonitor other)
        {
            return this.CameraID.CompareTo(other.CameraID) == 0;
        }
    }
}
