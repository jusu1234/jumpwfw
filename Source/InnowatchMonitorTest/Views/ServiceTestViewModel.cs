﻿using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.ComponentModel;
using System.Collections.ObjectModel;

using System.Windows;
using System.Windows.Input;

using InnowatchMonitor;
using InnowatchDataHandler.Commons.DataService;
using InnowatchDataHandler;


using RPN.Common.Core;
using RPN.Common.Core.Command;
using RPN.Common.Logging;

namespace InnowatchMonitorTest.ViewModels
{
    public class ServiceTestViewModel : NotificationObject
    {
        //Logger logger = LoggerFactory.GetLogger( typeof(ServiceTestViewModel) );

        #region field
        ObservableCollection<string> urlBases_;
        string selectedUrlBase_;
        string urlString_;
       
        DataSet ds_;

        ICommand sendCommand_;
        ICommand exitCommand_;
        ICommand mouseLeftCommand_;
        #endregion

       #region binding value
       public ObservableCollection<string> URLBases
       {
           get { return urlBases_; }
           private set
           {
               urlBases_ = value;
               RaisePropertyChanged("URLBases");
           }
       }

         public string SelectedURLBase
       {
           get { return selectedUrlBase_; }
           set
           {
               selectedUrlBase_ = value;
               RaisePropertyChanged("SelectedURLBase");
           }
       }

       public string URLString
       {
           get { return urlString_; }
           set
           { 
               urlString_ = value;
               RaisePropertyChanged("URLString");
           }
       }

       public DataView DV
       {
           get 
           {
               if (ds_ == null || ds_.Tables.Count < 1)
               {
                   return new DataView();
               }

               return ds_.Tables[0].DefaultView; 
           }

   
        

       }
       #endregion

       #region Command
       public ICommand SendCommand
       {
           get
           {
               if (sendCommand_ == null)
               {
                   sendCommand_ = new DelegateCommand(SendMessage);
               }

               return sendCommand_;
           }

          
       }

        // 요청 메세지 전송
       public void SendMessage()
       {
         
           RequestParameter req = new RequestParameter();
           req.Url = SelectedURLBase + URLString;

           RequestParameter.CharacterSetEncodingOption encoding = RequestParameter.CharacterSetEncodingOption.UTF8;
           req.EncodingOption = encoding.ToString();

           string xmlString = DataServiceHandler.Request(req, false);
           var encodingOption = GetEncodingOption(req.EncodingOption);
           var stream = new MemoryStream(encodingOption.GetBytes(xmlString));

           if (!string.IsNullOrEmpty(xmlString))
           {
               ds_ = null;
               ds_ = new DataSet();


               ds_.ReadXml(stream);


               // string str = DataManager.GetInstance.GetDataForConfiguration.GetAllUserInfo();
               RaisePropertyChanged("DV");

               N3NLogger.Debug("111");
           }



       }

       public ICommand ExitCommand
       {
           get
           {
               if (exitCommand_ == null)
               {

 
                   exitCommand_ = new DelegateCommand<Window>((param) => ((Window)param).Close());
               }

               return exitCommand_;
           }
       }

       public ICommand MouseLeftCommand
       {
           get
           {
               if (mouseLeftCommand_ == null)
               {


                   mouseLeftCommand_ = new DelegateCommand<Window>((param) => ((Window)param).DragMove());
               }

               return mouseLeftCommand_;
           }
       }

       #endregion


       public ServiceTestViewModel()
       {
           urlBases_ = new ObservableCollection<string>();

           if (ConfigurationManager.AppSettings.Count > 0)
           {
               selectedUrlBase_ = ConfigurationManager.AppSettings.GetValues(1)[0];
               for (int i = 0; i < ConfigurationManager.AppSettings.Count; i++)
               {
                   URLBases.Add(ConfigurationManager.AppSettings.GetValues(i)[0]);
               }
           }


           URLString = "rest/data/GetAllUserInfo";

           /*
           RequestParameter req = new RequestParameter();
           req.Url = SelectedURLBase + URLString;

           RequestParameter.CharacterSetEncodingOption encoding = RequestParameter.CharacterSetEncodingOption.UTF8;
           req.EncodingOption = encoding.ToString();

           string xmlString = DataServiceHandler.Request(req, false);
           var encodingOption = GetEncodingOption(req.EncodingOption);
           var stream = new MemoryStream(encodingOption.GetBytes(xmlString));

           if (ds_ == null)
               ds_ = new DataSet();


           ds_.ReadXml(stream);
            * */

           N3NLogger.initLogger();
       }

       private static Encoding GetEncodingOption(string encondingOption)
       {
           Encoding encodingOption;

           // Contents에 지정된 EncodingOption을 가져와서 설정한다.
           switch (encondingOption)
           {
               case "ASCII":
                   encodingOption = Encoding.ASCII;
                   break;
               case "BigEndianUnicode":
                   encodingOption = Encoding.BigEndianUnicode;
                   break;
               case "Unicode":
                   encodingOption = Encoding.Unicode;
                   break;
               case "UTF32":
                   encodingOption = Encoding.UTF32;
                   break;
               case "UTF7":
                   encodingOption = Encoding.UTF7;
                   break;
               case "UTF8":
                   encodingOption = Encoding.UTF8;
                   break;
               case "Default":
                   encodingOption = Encoding.Default;
                   break;
               default:
                   encodingOption = Encoding.UTF8;
                   break;
           }

           return encodingOption;
       }
    }

}
