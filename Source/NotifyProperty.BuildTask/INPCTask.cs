﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;
using Microsoft.Build.Framework;
using Microsoft.Build.Utilities;

using Mono.Cecil;
using Mono.Cecil.Cil;

using N3N.WPF.Core;

namespace NotifyProperty.BuildTask
{



    /// http://justinangel.net/AutomagicallyImplementingINotifyPropertyChanged

    public class INPCTask : Task
    {
        public override bool Execute()
        {
            SearchForPropertiesAndAddMSIL();
            return true;
        }

        [Required]
        public string OutDir { get; set; }

        private void SearchForPropertiesAndAddMSIL()
        {
            List<PropertyDefinition> replaceProperties = new List<PropertyDefinition>();
          //  BuildEngine2.LogMessageEvent( new BuildMessageEventArgs(

            foreach (string assemblyPath in Directory.GetFiles(OutDir, "*.dll", SearchOption.AllDirectories))
            {
                if (assemblyPath.Contains("ShadowCopies"))
                    continue;

                if (assemblyPath.Contains("NotifyProperty.BuildTask.dll"))
                    continue;

                string shadowCopyPath = CreateCopy(assemblyPath);

                

                AssemblyDefinition sourceAssembly = AssemblyDefinition.ReadAssembly(shadowCopyPath);

                bool isDirty = false;

                foreach (TypeDefinition type in sourceAssembly.MainModule.Types)
                {
                    foreach (PropertyDefinition prop in type.Properties)
                    {
                        foreach (CustomAttribute attribute in prop.CustomAttributes)
                        {
                           
                            //  prop.SetMethod.Module.
                            if (attribute.Constructor.DeclaringType.FullName == typeof(NotifyAttribute).FullName)
                            {
                                ILProcessor MSILWorker = prop.SetMethod.Body.GetILProcessor();

                                MethodDefinition raisePropertyChanged = null;

                                foreach (MethodDefinition method in type.Methods)
                                {
                                    if (method.Name == "RaisePropertyChanged"
                                        && method.Parameters.Count == 1
                                        && method.Parameters[0].ParameterType.FullName == "System.String")
                                    {
                                        raisePropertyChanged = method;
                                        break;
                                    }

                                    
                                }

                                if (raisePropertyChanged == null)
                                {
                                    BuildEngine2.LogErrorEvent(new BuildErrorEventArgs("", "", "", 0, 0, 0, 0, "Could not find RaisePropertychanged(string) method in " 
                                        + type.FullName, "", ""));
                                    continue;
                                }


                                if (replaceProperties.Any(p => p.DeclaringType.FullName == prop.DeclaringType.FullName
                                                               && p.Name == prop.Name))
                                {
                                    sourceAssembly.Write( assemblyPath);
                                    continue;
                                }

                                Instruction ldarg0 = MSILWorker.Create(OpCodes.Ldarg_0);

                                Instruction propertyName = MSILWorker.Create(OpCodes.Ldstr, prop.Name);

                                Instruction callRaisePropertyChanged =
                                    MSILWorker.Create(OpCodes.Call, raisePropertyChanged);

                              
             
                                MSILWorker.InsertBefore(prop.SetMethod.Body.Instructions.ElementAt(0),
                                                        MSILWorker.Create(OpCodes.Nop));

                                MSILWorker.InsertBefore(prop.SetMethod.Body.Instructions.ElementAt(prop.SetMethod.Body.Instructions.Count - 1),
                                                        ldarg0);

                                MSILWorker.InsertAfter(ldarg0, propertyName);

                                MSILWorker.InsertAfter(propertyName, callRaisePropertyChanged);

                                MSILWorker.InsertAfter(callRaisePropertyChanged, MSILWorker.Create(OpCodes.Nop));
                                
                                BuildEngine2.LogMessageEvent(new BuildMessageEventArgs(string.Format("Added INotifyPropertyChanged invoke for {0}.{1}", prop.DeclaringType.FullName, prop.Name), "", "", MessageImportance.High));
                                replaceProperties.Add(prop);
                                

                            

                                isDirty = true;
                            }
                        }

                        if (isDirty)
                        {
                            sourceAssembly.Write(shadowCopyPath);
                            File.Copy(shadowCopyPath, assemblyPath, true);
                          
                        }
                    }
                }
            }

     

        }

        private string CreateCopy(string assemblyPath)
        {
            string mainDir = Path.Combine(OutDir, "ShadowCopies");
            if (Directory.Exists(mainDir) == false)
                Directory.CreateDirectory(mainDir);

            
            string fileTimeUTC = Path.Combine(mainDir, DateTime.Now.ToFileTimeUtc().ToString());
            Directory.CreateDirectory(fileTimeUTC);

            string finalPath = Path.Combine(fileTimeUTC, Path.GetFileName(assemblyPath));
            File.Copy(assemblyPath, finalPath);
            return finalPath;
            

          //  File.Copy(assemblyPath, mainDir);

           // return mainDir;
        }


       




    }
}
