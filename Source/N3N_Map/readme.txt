
01. Config 설정
  
 === Player
 
	<userSettings>
        <Innotive.InnoWatch.Player.Properties.Settings>
            <!-- 통신 사용 여부 속성 -->
            <setting name="UseControlServer" serializeAs="String">
                <value>True</value>
            </setting>
            <!-- 소프트웨어 그래픽 가속 사용 여부 -->
            <setting name="UseSoftwareRender" serializeAs="String">
                <value>False</value>
            </setting>
            <!-- 카메라 보이기 여부 (False시 Merger Manager를 사용하지 않음) -->
            <setting name="CameraVisible" serializeAs="String">
                <value>True</value>
            </setting>
            <!-- 싱크 로그 남기 여부 속성 -->
            <setting name="SyncLogEnable" serializeAs="String">
                <value>False</value>
            </setting>
            <!-- 환경 파일 경로 -->
            <setting name="EnvPath" serializeAs="String">
                <value />
            </setting>
            <!-- Player시작시 전체 Sync Data를 요청할지 결정 -->
            <setting name="RequestSyncDataWhenStartPlayer" serializeAs="String">
                <value>True</value>
            </setting>
            <!-- 메시지 서비스 주소 -->
            <setting name="MsgServiceEndpointAddress" serializeAs="String">
                <value>net.tcp://172.16.10.99:20000/CS</value>
            </setting>
        </Innotive.InnoWatch.Player.Properties.Settings>
        <Innotive.InnoWatch.Commons.Properties.Settings>
            <!-- 고화질 필터 개수  -->
            <setting name="High_Filter_Limit" serializeAs="String">
                <value>12</value>
            </setting>
            <!-- 저화질 필터 개수 (-1일 경우 선언되어 있는 만큼 자동으로 개수를 설정) -->
            <setting name="Low_Filter_Limit" serializeAs="String">
                <value>-1</value>
            </setting>
            <!-- 고화질/저화질 테두리 보이기 여부 속성 -->
            <setting name="Show_LowHigh_Borders" serializeAs="String">
                <value>False</value>
            </setting>
            <!-- 초기 필터 추가시 진행 보이기 여부 속성 -->
            <setting name="Show_Filter_Loading_Count" serializeAs="String">
                <value>True</value>
            </setting>
            <!-- 고화질 영상 변경 한계 (너비) -->
            <setting name="CameraSizeLowLimit_Width" serializeAs="String">
                <value>240</value>
            </setting>
            <!-- 고화질 영상 변경 한계 (높이) -->
            <setting name="CameraSizeLowLimit_Height" serializeAs="String">
                <value>180</value>
            </setting>
            <!-- 카메라 소스 변경시 애니메이션 속도 속성 (Low -> High) -->
            <setting name="CameraSource_LowToHigh_AnimationSpeed" serializeAs="String">
                <value>0.2</value>
            </setting>
            <!-- 카메라 소스 변경시 애니메이션 속도 속성 (High -> Low) -->
            <setting name="CameraSource_HighToLow_AnimationSpeed" serializeAs="String">
                <value>0.2</value>
            </setting>
            <!-- 마우스 커서 보이기 여부 속성 -->
            <setting name="ShowMouseCursor" serializeAs="String">
                <value>True</value>
            </setting>
        </Innotive.InnoWatch.Commons.Properties.Settings>
        <Innotive.InnoWatch.DLLs.GridControl.Properties.Settings>
            <!-- 그리드에서 카메라 표시시 비율 사용 여부 (False시 화면 가득 채움) -->
            <setting name="UseRatio" serializeAs="String">
                <value>True</value>
            </setting>
            <!-- 그리드에서 카메라 표시시 너비 비율 -->
            <setting name="WidthRatio" serializeAs="String">
                <value>4</value>
            </setting>
            <!-- 그리드에서 카메라 표시시 높이 비율 -->
            <setting name="HeightRatio" serializeAs="String">
                <value>3</value>
            </setting>
            <!-- 그리드 가이드 라인 보이기 여부 속성 -->
            <setting name="UseGridGuidLines" serializeAs="String">
                <value>False</value>
            </setting>
        </Innotive.InnoWatch.DLLs.GridControl.Properties.Settings>
    </userSettings>
 
 === Console
 
	<userSettings>
        <Innotive.InnoWatch.Console.Properties.Settings>
            <!-- 통신 사용 여부 속성 -->
            <setting name="UseControlServer" serializeAs="String">
                <value>True</value>
            </setting>
            <!-- 소프트웨어 그래픽 가속 사용 여부 속성 -->
            <setting name="UseSoftwareRender" serializeAs="String">
                <value>False</value>
            </setting>
            <!-- 카메라 보이기 여부 (False시 Merger Manager를 사용하지 않음) -->
            <setting name="CameraVisible" serializeAs="String">
                <value>True</value>
            </setting>
            <!-- 표시 언어 속성 (en-US / ko-KR) -->
            <setting name="Language" serializeAs="String">
                <value>en-US</value>
            </setting>
            <!-- 싱크 로그 남기 여부 속성 -->
            <setting name="SyncLogEnable" serializeAs="String">
                <value>False</value>
            </setting>
            <!-- 카메라 선택시 PTZ 카메라만 서버에게 보내기 여부 속성 -->
            <setting name="SendOnlyPTZSelectedCamera" serializeAs="String">
                <value>True</value>
            </setting>
            <!-- 환경 파일 경로 -->
            <setting name="EnvPath" serializeAs="String">
                <value />
            </setting>
            <!-- 데이터 서비스 주소 -->
            <setting name="DataServiceEndpointAddress" serializeAs="String">
                <value>http://172.16.10.99:15777/Service.svc</value>
            </setting>
            <!-- 메시지 서비스 주소 -->
            <setting name="MsgServiceEndpointAddress" serializeAs="String">
                <value>net.tcp://172.16.10.99:20000/CS</value>
            </setting>
        </Innotive.InnoWatch.Console.Properties.Settings>
        <Innotive.InnoWatch.Commons.Properties.Settings>
            <!-- 고화질 필터 개수  -->
            <setting name="High_Filter_Limit" serializeAs="String">
                <value>1</value>
            </setting>
            <!-- 저화질 필터 개수 (-1일 경우 선언되어 있는 만큼 자동으로 개수를 설정) -->
            <setting name="Low_Filter_Limit" serializeAs="String">
                <value>-1</value>
            </setting>
            <!-- 고화질/저화질 테두리 보이기 여부 속성 -->
            <setting name="Show_LowHigh_Borders" serializeAs="String">
                <value>False</value>
            </setting>
            <!-- 초기 필터 추가시 진행 보이기 여부 속성 -->
            <setting name="Show_Filter_Loading_Count" serializeAs="String">
                <value>True</value>
            </setting>
            <!-- 고화질 영상 변경 한계 (너비) -->
            <setting name="CameraSizeLowLimit_Width" serializeAs="String">
                <value>240</value>
            </setting>
            <!-- 고화질 영상 변경 한계 (높이) -->
            <setting name="CameraSizeLowLimit_Height" serializeAs="String">
                <value>180</value>
            </setting>
            <!-- 카메라 소스 변경시 애니메이션 속도 속성 (Low -> High) -->
            <setting name="CameraSource_LowToHigh_AnimationSpeed" serializeAs="String">
                <value>0.2</value>
            </setting>
            <!-- 카메라 소스 변경시 애니메이션 속도 속성 (High -> Low) -->
            <setting name="CameraSource_HighToLow_AnimationSpeed" serializeAs="String">
                <value>0.2</value>
            </setting>
        </Innotive.InnoWatch.Commons.Properties.Settings>
        <Innotive.InnoWatch.DLLs.GridControl.Properties.Settings>
            <!-- 그리드에서 카메라 표시시 비율 사용 여부 (False시 화면 가득 채움) -->
            <setting name="UseRatio" serializeAs="String">
                <value>True</value>
            </setting>
            <!-- 그리드에서 카메라 표시시 너비 비율 -->
            <setting name="WidthRatio" serializeAs="String">
                <value>4</value>
            </setting>
            <!-- 그리드에서 카메라 표시시 높이 비율 -->
            <setting name="HeightRatio" serializeAs="String">
                <value>3</value>
            </setting>
            <!-- 그리드 가이드 라인 보이기 여부 속성 -->
            <setting name="UseGridGuidLines" serializeAs="String">
                <value>False</value>
            </setting>
        </Innotive.InnoWatch.DLLs.GridControl.Properties.Settings>
    </userSettings>
 
 === ProcessWatcher
 
	<userSettings>
      <Innotive.InnoWatch.ProcessWatcher.Properties.Settings>
          <!-- 재시작 지연 시간 (초) -->
          <setting name="RestartDelay" serializeAs="String">
              <value>20</value>
          </setting>
          <!-- 시스템 시작시 플레이어 지연 시간 (초) -->
          <setting name="SystemUpDelay" serializeAs="String">
              <value>60</value>
          </setting>
          <!-- 데이터 서비스 주소 -->
          <setting name="DataServiceEndpointAddress" serializeAs="String">
              <value>http://172.16.10.99:15777/Service.svc</value>
          </setting>
          <!-- 메시지 서비스 주소 -->
          <setting name="MsgServiceEndpointAddress" serializeAs="String">
              <value>net.tcp://172.16.10.99:20000/CS</value>
          </setting>
      </Innotive.InnoWatch.ProcessWatcher.Properties.Settings>
	</userSettings>
 
 

02. Visual Studio Env 설정 (옵션)
 
 === Console
  - /EnvPath="C:\INNOWATCH\Env\Residential-iConsole.xml"
  
 === Player
  - /EnvPath="C:\INNOWATCH\Env\Residential-iDisplay01_index1.xml"
  - /EnvPath="C:\INNOWATCH\Env\Residential-iDisplay01.xml"

  
03. 환견 설정 파일 예제 
 
 === iConsole.xml
	<?xml version="1.0" encoding="utf-8"?>
	<ConsoleEnvironmentData xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
		<ProjectPath>C:\INNOWATCH\Contents\Residential\Residential.pjt</ProjectPath>
		<MonitorDataList>
			<MonitorData MonitorIndex="0" VirtualMonitorIndex="0" />
		</MonitorDataList>
		<StageGridOverlayInfoList>
			<StageGridOverlayInfo StageIndex="0" LinkedDisplayAreaName="DisplayArea01"/>
			<StageGridOverlayInfo StageIndex="1" LinkedDisplayAreaName="DisplayArea02"/>
		</StageGridOverlayInfoList>
		<StageAlarmOverlayInfoList>
			<StageAlarmOverlayInfo StageIndex="0" AlarmLayoutType="Layout" LinkedDisplayAreaName="" AlarmLayoutName="allmap"/>
			<StageAlarmOverlayInfo StageIndex="1" AlarmLayoutType="GridLayout" LinkedDisplayAreaName="DisplayArea02" AlarmLayoutName=""/>
		</StageAlarmOverlayInfoList>
	</ConsoleEnvironmentData>
 
 
 
 === iDisplay01.xml  
	<?xml version="1.0" encoding="utf-8"?>
	<PlayerEnvironmentData xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
		<ProjectPath>C:\INNOWATCH\Contents\Residential\Residential.pjt</ProjectPath>
		<MonitorDataList>
			<MonitorData MonitorIndex="1" VirtualMonitorIndex="1" />
		</MonitorDataList>
		<AlarmOverlayInfoList>
			<AlarmOverlayInfo LinkedDisplayAreaName="DisplayArea01" AlarmLayoutName="allmap"/>
		</AlarmOverlayInfoList>
	</PlayerEnvironmentData>
  