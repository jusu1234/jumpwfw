﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="EnvironmentManager.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   environment manager.
//   2009. 03. 19 - gcnam
//   PlayerCommons 통합 작업중에,
//   EnvironmentManager 를 합침.
//   OverlayDisplayArea 구조가 바뀔 예정이기에,
//   현재, EnvironmentData 의 상속구조는 고려하고 있지 않음.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.PlayerCommons.Environments
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Reflection;
    using System.Windows;

    using Innotive.InnoWatch.Commons.Environments;
    using Innotive.InnoWatch.Commons.Files;
    using Innotive.InnoWatch.Commons.Utils;
    using Innotive.InnoWatch.CommonUtils.Log;
    using Innotive.InnoWatch.PlayerCommons.DisplayAreas;
    using Innotive.InnoWatch.PlayerCommons.Files;

    using MessageBox = System.Windows.MessageBox;

    /// <summary>
    /// iDisplay, iCommand를 실행하는 데 필요한 프로젝트 정보와 모니터 정보를 가지고 있는 객체.
    /// </summary>
    public class EnvironmentManager
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EnvironmentManager"/> class.
        /// </summary>
        public EnvironmentManager()
        {
            this.Environment = new EnvironmentFile();
        }

        /// <summary>
        /// Gets or sets EnabledDisplayAreaList.
        /// </summary>
        public List<PlayerDisplayArea> EnabledDisplayAreaList { get; set; }

        /// <summary>
        /// Gets or sets Environment.
        /// </summary>
        public EnvironmentFile Environment { get; set; }

        /// <summary>
        /// Gets or sets MonitorDataList.
        /// </summary>
        public List<MonitorData> MonitorDataList
        {
            get
            {
                return this.Environment.MonitorDataList;
            }

            set
            {
                // TODO -blackRoot : 임시로 소스 추가함 !!
                if (this.Environment == null)
                {
                    this.Environment = new EnvironmentFile();
                }

                this.Environment.MonitorDataList = value;
            }
        }

        /// <summary>
        /// Gets or sets ProjectFile.
        /// </summary>
        public string ProjectFile
        {
            get
            {
                return this.Environment.StartProjectFilePath;
            }

            set
            {
                // TODO -blackRoot : 죽는 부분이 있어서 임시로 코드 추가함 !!
                if (this.Environment == null)
                {
                    this.Environment = new EnvironmentFile();
                }

                this.Environment.StartProjectFilePath = value;
                this.Environment.StartProjectFile = new InnoFilePath(this.Environment.StartProjectFilePath);
            }
        }

        /// <summary>
        /// clear.
        /// </summary>
        public void Clear()
        {
            this.ProjectFile = null;

            if (this.MonitorDataList != null)
            {
                this.MonitorDataList.Clear();
            }

            if (this.EnabledDisplayAreaList != null)
            {
                this.EnabledDisplayAreaList.Clear();
            }

            this.MonitorDataList = null;
            this.EnabledDisplayAreaList = null;
        }

        /// <summary>
        /// 속성에서 환경 파일 읽기.
        /// </summary>
        /// <param name="projPath">
        /// The proj Path.
        /// </param>
        /// <returns>
        /// 읽기 성공 여부.
        /// </returns>
        public bool LoadEnvironmentFile(string projPath)
        {
            string path = this.GetEnvPathOfProjectPath(projPath);

            if (string.IsNullOrEmpty(path))
            {
                return false;
            }

            if (false == File.Exists(path))
            {
                return false;
            }

            this.Environment = BaseFile.LoadBaseFile<EnvironmentFile>(new InnoFilePath(path));

            if (this.Environment == null)
            {
                return false;
            }

            ////////////////////////////
            // MonitorSettingWindow 내부에서 하고 있는 작업을 똑같이 함 !!

            if (!File.Exists(this.Environment.StartProjectFile.Path))
            {
                InnotiveDebug.Trace(4, "Env파일 내부 StartProjectFile이 존재하지 않습니다.\r\nPath = " + this.Environment.StartProjectFile.Path);
                MessageBox.Show("Env파일 내부 StartProjectFile이 존재하지 않습니다.\r\nPath = " + this.Environment.StartProjectFile.Path);

                this.Environment.StageList.Clear();
                this.Environment.MonitorDataList.Clear();
                this.Environment.StartProjectFile = null;
                this.Environment.StartProjectFilePath = string.Empty;

                return false;
            }

            MainProcess.Instance.LoadProjectInfo(this.Environment.StartProjectFile);

            return true;
        }

        public void CheckMonitors()
        {
            // 현재 컴퓨터에 연결된 Monitor의 갯수를 얻어옴 !!
            int realMonitorCount = InnoMonitorUtil.GetScreenCount();
            foreach (MonitorData monitorData in this.MonitorDataList)
            {
                if (MainProcess.Instance.ProjectManager.MonitorManager.MonitorList.Count <=
                    monitorData.VirtualMonitorIndex)
                {
                    InnotiveDebug.Trace(4, "초기화 파일에 저장된 Virtual Monitor의 Index가 Project에 저장된 Virtual Monitor의 갯수보다 큽니다");
                    MessageBox.Show("초기화 파일에 저장된 Virtual Monitor의 Index가 Project에 저장된 Virtual Monitor의 갯수보다 큽니다");
                    continue;
                }

                if (monitorData.MonitorIndex >= realMonitorCount)
                {
                    MessageBox.Show("초기화 파일의 MonitorIndex값이 실제 모니터 갯수보다 큽니다");
                    continue;
                }

                MainProcess.Instance.ProjectManager.MonitorManager.MonitorList[monitorData.VirtualMonitorIndex].
                    DriverIndex = monitorData.MonitorIndex;
            }
        }

        public void CalcEnabledDisplayAreaList()
        {
            var list = new List<PlayerDisplayArea>();

            var cnt = MainProcess.Instance.ProjectManager.DisplayAreaManager.Count;

            for (int i = 0; i < cnt; ++i)
            {
                var selectedDisplayArea = MainProcess.Instance.ProjectManager.DisplayAreaManager.GetDisplayArea(i) as PlayerDisplayArea;

                var isInclude = false;

                foreach (var monitorData in this.MonitorDataList)
                {
                    if (MainProcess.Instance.ProjectManager.MonitorManager.MonitorList.Count <=
                        monitorData.VirtualMonitorIndex)
                    {
                        InnotiveDebug.Trace(4, "초기화 파일에 저장된 Virtual Monitor의 Index가 Project에 저장된 Virtual Monitor의 갯수보다 큽니다");
                        MessageBox.Show("초기화 파일에 저장된 Virtual Monitor의 Index가 Project에 저장된 Virtual Monitor의 갯수보다 큽니다");
                        continue;
                    }

                    Rect monitorRect =
                        MainProcess.Instance.ProjectManager.MonitorManager.MonitorList[monitorData.VirtualMonitorIndex].
                            MonitorRect;
                    monitorRect.X += 1;
                    monitorRect.Y += 1;
                    monitorRect.Width -= 2;
                    monitorRect.Height -= 2;

                    Rect displayAreaRect = selectedDisplayArea.Rect;
                    displayAreaRect.X += 1;
                    displayAreaRect.Y += 1;
                    displayAreaRect.Width -= 2;
                    displayAreaRect.Height -= 2;

                    if (monitorRect.IntersectsWith(displayAreaRect))
                    {
                        isInclude = true;
                        break;
                    }
                }

                // 선택된 DisplayArea를 add함 !! 좌표값은 위에서 구했음 !!
                if (isInclude)
                {
                    list.Add(selectedDisplayArea);
                }
            }

            this.EnabledDisplayAreaList = list;
        }

        /// <summary>
        /// parse command line args.
        /// </summary>
        /// <param name="args">
        /// The args.
        /// </param>
        /// <returns>
        /// The parse command line args.
        /// </returns>
        public bool ParseCommandLineArgs(string[] args)
        {
            if (args.Length <= 0)
            {
                InnotiveDebug.Trace(4, "EnvironmentManager.ParseCommandLineArgs() ==> args의 Length가 1보다 작습니다");
                return false;
            }

            foreach (string arg in args)
            {
                try
                {
                    if (!this.ParseCommandLineArg(arg))
                    {
                        return false;
                    }
                }
                catch (Exception ex)
                {
                    InnotiveDebug.Trace(4, "EnvironmentHandler.ParseCommandLineArgs() 예외 발생 ==> {0}", ex.Message);
                }
            }

            return true;
        }

        /// <summary>
        /// save environment file.
        /// </summary>
        /// <param name="projPath">
        /// The proj Path.
        /// </param>
        /// <returns>
        /// The save environment file.
        /// </returns>
        public bool SaveEnvironmentFile(string projPath)
        {
            return this.Environment.SaveToFile(new InnoFilePath(projPath));
        }

        private string GetEnvPathOfProjectPath(string projPath)
        {
            return projPath;
        }

        private bool ParseCommandLineArg(string arg)
        {
            arg = arg.Trim();
            int index = arg.IndexOf("/");
            if (index != 0)
            {
                MessageBox.Show("잘못된 CommandLineArg입니다. \"/\"로 시작해야 합니다");
                return false;
            }

            index = arg.IndexOf("=");
            if (index < 0)
            {
                MessageBox.Show("잘못된 CommandLineArg입니다. \"=\"를 찾을수 없습니다.");
                return false;
            }

            string leftParam = arg.Substring(0, index);
            string rightParam = arg.Substring(index + 1, arg.Length - index - 1);

            if (string.Compare(leftParam, "/EnvPath", true) == 0)
            {
                if (!File.Exists(rightParam))
                {
                    MessageBox.Show("환경설정 파일이 존재하지 않습니다");
                    return false;
                }

                return this.LoadEnvironmentFile(rightParam);
            }

            return true;
        }
    }
}