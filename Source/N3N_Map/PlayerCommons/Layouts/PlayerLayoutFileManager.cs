﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PlayerLayoutFileManager.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   player layout file manager.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.PlayerCommons.Layouts
{
    using System;
    using System.Collections.Generic;
    using System.Windows;
    using Innotive.InnoWatch.Commons.Files;
    using Innotive.InnoWatch.Commons.Layouts;
    using Innotive.InnoWatch.Commons.Utils;
    using Innotive.InnoWatch.DLLs.LayoutControls;
    
    /// <summary>
    /// player layout file manager.
    /// </summary>
    public class PlayerLayoutFileManager : LayoutFileManager
    {
        #region Constants and Fields

        /// <summary>
        /// _player layout file data list.
        /// </summary>
        /// <remarks>
        /// 주의 : playerLayoutFileDataList 는 외부로 노출 시키지 말것. ask to gcnam.
        /// </remarks>
        private readonly List<PlayerLayoutFileData> playerLayoutFileDataList = new List<PlayerLayoutFileData>();

        #endregion

        #region Properties

        /// <summary>
        /// Gets Count.
        /// </summary>
        public int Count
        {
            get
            {
                return this.playerLayoutFileDataList.Count;
            }
        }

        #endregion

        #region Public Methods

        public PlayerLayoutFileManager()
        {
            AsyncWorker.Instance.eFirstWorkTimeSpanOccurred += new EventHandler<EventArgs>(AsyncWorkerInstance_eFirstWorkTimeSpanOccurred);
        }

        //by blackRoot : Console에서 처음 Project Loading시 IPQ 갱신이 안되는 문제가 있어서 최초 한번 IPQ Redraw를 강제로 해줌 !!
        //AsyncWorker에서 WorkTimeSpan이 처음 발생할때 event를 발생함 !!
        void AsyncWorkerInstance_eFirstWorkTimeSpanOccurred(object sender, EventArgs e)
        {
            var layoutList = new List<LayoutControl>();

            foreach (var layoutFileData in this.playerLayoutFileDataList)
            {
                layoutList.AddRange(layoutFileData.GetAllLayoutControlList());
            }

            foreach (var layoutControl in layoutList)
                layoutControl.UpdateViewArea(Rect.Empty, null);
        }

        /// <summary>
        /// the clear.
        /// </summary>
        public override void Clear()
        {
            foreach (var layoutFileData in this.playerLayoutFileDataList)
            {
                layoutFileData.Clear();
            }

            this.playerLayoutFileDataList.Clear();
        }

        /// <summary>
        /// clear all file data layouts.
        /// </summary>
        public void ClearAllFileDataLayouts()
        {
            foreach (var layoutFileData in this.playerLayoutFileDataList)
            {
                layoutFileData.Clear();
            }
        }

       

        /// <summary>
        /// get element list.
        /// </summary>
        /// <param name="layoutGUID">
        /// The layout guid.
        /// </param>
        /// <param name="elementGUID">
        /// The element guid.
        /// </param>
        /// <returns>
        /// FrameworkElement list.
        /// </returns>
        public List<FrameworkElement> GetElementList(Guid layoutGUID, Guid elementGUID)
        {
            if (layoutGUID == Guid.Empty)
            {
                return this.GetElementList(elementGUID);
            }

            var result = new List<FrameworkElement>();
            foreach (var layoutFileData in this.playerLayoutFileDataList)
            {
                var temp = layoutFileData.GetElementList(layoutGUID, elementGUID);
                if (temp != null && temp.Count > 0)
                {
                    result.AddRange(temp);
                }
            }

            return result;
        }

        /// <summary>
        /// get element list.
        /// </summary>
        /// <param name="elementGUID">
        /// The element guid.
        /// </param>
        /// <returns>
        /// FrameworkElement list.
        /// </returns>
        public List<FrameworkElement> GetElementList(Guid elementGUID)
        {
            var result = new List<FrameworkElement>();
            if (elementGUID == Guid.Empty)
            {
                return result;
            }

            foreach (var layoutFileData in this.playerLayoutFileDataList)
            {
                var temp = layoutFileData.GetElementList(elementGUID);

                if (temp != null && temp.Count > 0)
                {
                    result.AddRange(temp);
                }
            }

            return result;
        }

        /// <summary>
        /// get layout control list.
        /// </summary>
        /// <param name="layoutGUID">
        /// The layout guid.
        /// </param>
        /// <returns>
        /// LayoutControl list.
        /// </returns>
        public List<LayoutControl> GetLayoutControlList(Guid layoutGUID)
        {
            var result = new List<LayoutControl>();
            foreach (var layoutFileData in this.playerLayoutFileDataList)
            {
                var temp = layoutFileData.GetLayoutControlList(layoutGUID);
                if (temp != null && temp.Count > 0)
                {
                    result.AddRange(temp);
                }
            }

            return result;
        }

        public void DoGoHomeActionByTargetLayoutGuid(Guid layoutGuid, string effect, double start, double duration)
        {   
            foreach (var layoutFileData in this.playerLayoutFileDataList)
            {
                layoutFileData.DoGoHomeActionByTargetLayoutGuid(layoutGuid, effect, start, duration);
            }
        }

        public void GoToLocationByTargetLayoutGuid(Guid layoutGuid, Rect zoomRect, string effect, double startTime, double duration)
        {
            foreach (var layoutFileData in this.playerLayoutFileDataList)
            {
                layoutFileData.GoToLocationByTargetLayoutGuid(layoutGuid, zoomRect, effect, startTime, duration);
            }
        }

        /// <summary>
        /// get layout file data.
        /// </summary>
        /// <param name="guid">
        /// The guid.
        /// </param>
        /// <returns>
        /// the PlayerLayoutFileData.
        /// </returns>
        public PlayerLayoutFileData GetLayoutFileData(Guid guid)
        {
            foreach (var layoutFileData in this.playerLayoutFileDataList)
            {
                if (layoutFileData.SyncGUID == guid)
                {
                    return layoutFileData;
                }
            }

            return null;
        }

        /// <summary>
        /// get layout file data.
        /// </summary>
        /// <param name="guid">
        /// The guid.
        /// </param>
        /// <returns>
        /// the PlayerLayoutFileData.
        /// </returns>
        public PlayerLayoutFileData GetLayoutFileDataByLayoutName(string layoutName)
        {
            foreach (var layoutFileData in this.playerLayoutFileDataList)
            {
                if (string.Compare(layoutFileData.Name, layoutName, true) == 0)
                {
                    return layoutFileData;
                }
            }

            return null;
        }

        /// <summary>
        /// get layout file data.
        /// </summary>
        /// <param name="index">
        /// The index.
        /// </param>
        /// <returns>
        /// the PlayerLayoutFileData.
        /// </returns>
        public PlayerLayoutFileData GetLayoutFileData(int index)
        {
            if (index < 0 || index >= this.Count)
            {
                return null;
            }

            return this.playerLayoutFileDataList[index];
        }

        /// <summary>
        /// get layout file data.
        /// </summary>
        /// <param name="layoutFileName">
        /// The layout file name.
        /// </param>
        /// <returns>
        /// the PlayerLayoutFileData.
        /// </returns>
        public PlayerLayoutFileData GetLayoutFileData(InnoFilePath layoutFileName)
        {
            foreach (var layoutFileData in this.playerLayoutFileDataList)
            {
                if (string.Compare(layoutFileData.FileName.Path, layoutFileName.Path, true) == 0)
                {
                    return layoutFileData;
                }
            }

            return null;
        }

        /// <summary>
        /// Guid에 해당하는 LayoutFile의 InnoUri를 반환한다. 
        /// </summary>
        /// <param name="layoutGUID">
        /// The layout GUID.
        /// </param>
        /// <returns>
        /// Layout이 없으면 null을 반환한다.
        /// </returns>
        public InnoFilePath GetLayoutFileName(Guid layoutGUID)
        {
            foreach (var layoutFileData in this.playerLayoutFileDataList)
            {
                if (layoutFileData.SyncGUID == layoutGUID)
                {
                    return layoutFileData.FileName;
                }
            }

            return null;
        }

        /// <summary>
        /// get layout file name list.
        /// </summary>
        /// <returns>
        /// InnoFilePath list.
        /// </returns>
        public List<InnoFilePath> GetLayoutFileNameList()
        {
            var layoutFileNameList = new List<InnoFilePath>();

            foreach (var layoutFileData in this.playerLayoutFileDataList)
            {
                layoutFileNameList.Add(layoutFileData.FileName);
            }

            return layoutFileNameList;
        }

        public Guid GetLayoutSyncGuidFromLayoutName(string layoutName)
        {
            foreach (var layoutFileData in this.playerLayoutFileDataList)
            {
                if (string.Compare(layoutFileData.Name, layoutName, true) == 0)
                    return layoutFileData.SyncGUID;
            }

            return Guid.Empty;
        }
        
        /// <summary>
        /// load all file data.
        /// </summary>
        /// <remarks>
        /// open project할때 file info list를 생성함 !! 실제 Layout을 생성하지는 않음 !!
        /// LoadLayout을 할때 여기서 생성된 file info list에 해당 layout들이 붙음 !!.
        /// </remarks>
        /// <param name="layoutFileNameList">
        /// The layout file name list.
        /// </param>
        public void LoadAllFileData(List<InnoFilePath> layoutFileNameList)
        {
            this.playerLayoutFileDataList.Clear();

            foreach (var layoutFileName in layoutFileNameList)
            {
                var layoutFileData = new PlayerLayoutFileData(layoutFileName);
                this.playerLayoutFileDataList.Add(layoutFileData);
            }
        }

        /// <summary>
        /// DisplayArea 가 Layout을 생성할 때 호출한다.
        /// LayoutControl을 생성한 후 반환한다.
        /// </summary>
        /// <param name="layoutGuid">
        /// layout Guid.
        /// </param>
        /// <returns>
        /// The LayoutCotnrol.
        /// </returns>
        public LayoutControl LoadLayout(Guid layoutGuid, Guid loadedActionGuid, bool createVideoSurface = true)
        {
            var layoutFileData = this.GetLayoutFileData(layoutGuid);

            if (layoutFileData != null)
            {
                return layoutFileData.LoadLayoutControl(loadedActionGuid, createVideoSurface);
            }
            
            return null;
        }

        public LayoutControl LoadLayoutByLayoutName(string layoutName, bool createVideoSurface = true)
        {
            var layoutFileData = this.GetLayoutFileDataByLayoutName(layoutName);

            if (layoutFileData != null)
            {
                return layoutFileData.LoadLayoutControl(Guid.Empty, createVideoSurface);
            }

            return null;
        }

        /// <summary>
        /// Remove Layout. 
        /// </summary>
        /// <param name="layoutControl">
        /// layout Control.
        /// </param>
        public void RemoveLayout(LayoutControl layoutControl)
        {
            if (layoutControl == null)
            {
                return;
            }

            var layoutFileData = this.GetLayoutFileData(layoutControl.SyncGUID);

            if (layoutFileData == null)
            {
                return;
            }

            layoutFileData.RemoveLayoutControl(layoutControl);
        }

        /// <summary>
        /// save all layout xaml data.
        /// </summary>
        public void SaveAllLayoutXamlData()
        {
            foreach (var layoutFileData in this.playerLayoutFileDataList)
            {
                layoutFileData.SetXamlString();
            }
        }

        /// <summary>
        /// trace all layout list.
        /// </summary>
        public void TraceAllLayoutList()
        {
            foreach (var layoutFileData in this.playerLayoutFileDataList)
            {
                layoutFileData.TraceList();
            }
        }

        /// <summary>
        /// trace all layout list.
        /// </summary>
        public void TraceAllCameraBrushStatus()
        {
            foreach (var layoutFileData in this.playerLayoutFileDataList)
            {
                layoutFileData.TraceAllCameraBrushStatus();
            }
        }        

        #endregion
    }
}