﻿
namespace Innotive.InnoWatch.PlayerCommons.DeviceInfos
{
    public interface IDeviceInfoSettings
    {
        /// <summary>
        /// DeviceInfo Notify 시작알림 메시지 창
        /// </summary>
        void PopupWindowForStartReceiving();

        /// <summary>
        /// DeviceInfo 받기 실패 메시지 창
        /// </summary>
        void PopupWindowForRecvFailed();

        /// <summary>
        /// DeviceInfo 받은 후 진행되는 동작
        /// </summary>
        void ActionforReceivedDeviceInfo();
    }
}
