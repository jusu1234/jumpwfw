﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DeviceInfoProcess.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Player 와 Console 에서 DeviceInfo 데이터를 처리하는데 필요한 것들을 여기에 넣는다.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.PlayerCommons.DeviceInfos
{
    using System;
    using System.Diagnostics;
    using System.IO;
    using System.ServiceModel;
    using Innotive.InnoWatch.Commons.CameraManagers;
    using Innotive.InnoWatch.Commons.CameraManagers.DeviceInfo;
    using Innotive.InnoWatch.Commons.Configs;
    using Innotive.InnoWatch.Commons.Utils;
    using Innotive.InnoWatch.CommonUtils.Log;
    using Innotive.InnoWatch.CommonUtils.Network;
    using Innotive.InnoWatch.Commons.Utils.Http;
    using Innotive.InnoWatch.PlayerCommons.Configs;
    using Innotive.InnoWatch.PlayerCommons.UIs.Popups;

    /// <summary>
    /// DeviceInfo 데이터 처리에 필요한 부분 담당.
    /// </summary>
    public class DeviceInfoProcess
    {
        #region Constants and Fields

        /// <summary>
        /// DeviceInfo.log path.
        /// </summary>
        private static string deviceInfoLogFilename = AppDomain.CurrentDomain.BaseDirectory + "DeviceInfo.log";

        /// <summary>
        /// DeviceInfo.test path.
        /// </summary>
        private static string deviceInfoTestFilename = AppDomain.CurrentDomain.BaseDirectory + "DeviceInfo.test";

        /// <summary>
        /// VideoInformation.test path.
        /// </summary>
        private static string videoInfoTestFilename = AppDomain.CurrentDomain.BaseDirectory + "VideoInformation.test";

        /// <summary>
        /// VideoInformation.log path.
        /// </summary>
        private static string videoInfoLogFilename = AppDomain.CurrentDomain.BaseDirectory + "VideoInformation.log";

        /// <summary>
        /// VideoDevices.xml path.
        /// </summary>
        private static string _videoDevicesFilename = AppDomain.CurrentDomain.BaseDirectory + "VideoDevices.xml";

        #endregion

        #region Public Methods

        /// <summary>
        /// 장치 정보를 가져와서 적용함. (현재 사용 안 함.)
        /// </summary>
        /// <param name="useControlServer">
        /// 현재 통신 가능 여부.
        /// </param>
        /// <returns>
        /// 실패하면 false를 반환한다.
        /// </returns>
        public static bool RefreshDeviceInformation(bool useControlServer)
        {
            // TODO shwlee : CommEnable 이 False 인 상황에서 DeviceInfo.test를 읽어야하는 상황 포함.
            // if (!UseControlServer && !IsExistDeviceInfoTestFile())
            // 확장 Deviceinfo 파일 사용할 경우 
            if (!useControlServer && !IsExistDeviceInfoTestFile() && !IsExistVideoDevicesFile())
            {
                InnotiveDebug.Log.Error("통신이 설정되어 있지 않아 DeviceInfo를 가져오지 않습니다.");
                InnotiveDebug.Trace("통신이 설정되어 있지 않아 DeviceInfo를 가져오지 않습니다.");

                return true;
            }

            try
            {
                var deviceInfoString = GetDeviceInfoData();
                var cameraResolutionData = GetVideoInformationData();
                System.Diagnostics.Debug.WriteLine("[DeviceInfoProcess] Got DeviceInfo and Starting Add Filter.");
                //CameraManager.Instance.Initialize(deviceInfoString, cameraResolutionData);
            }
            catch (Exception ex)
            {
                InnotiveDebug.Log.Error("Could not get DeviceInfo.", ex);
                InnotiveDebug.Trace("Could not get DeviceInfo." + ex.Message);

                return false;
            }

            return true;
        }

        /// <summary>
        /// 장치 정보를 가져와서 적용함.
        /// </summary>
        /// <param name="useControlServer">
        /// 현재 통신 가능 여부.
        /// </param>
        /// <param name="showui"></param>
        /// <returns>
        /// 실패하면 false를 반환한다.
        /// </returns>
        public static bool ApplyDeviceInformation(bool useControlServer, bool showui = true)
        {
            return SetVideoInformationProcess(useControlServer);
        }

        /// <summary>
        /// VideoInformation 객체를 생성해서 FilterManager에 세팅한다.
        /// </summary>
        /// <param name="useControlServer">CT 사용 여부.</param>
        /// <returns></returns>
        private static bool SetVideoInformationProcess(bool useControlServer)
        {
            // VideoInformation 을 만들 수 없는 상황.
            if (!useControlServer && !IsExistVideoInfoTestFile())
            {
                InnotiveDebug.Log.Error("CT를 사용하지 않고 .test 파일이 없으므로 VideoInformation 을 만들지 않습니다.");
                InnotiveDebug.Trace("CT를 사용하지 않고 .test 파일이 없으므로 VideoInformation 을 만들지 않습니다.");

                return true;
            }

            try
            {
                var videoInformationData = GetVideoInformationData();
                System.Diagnostics.Debug.WriteLine("[DeviceInfoProcess] Got VideoInformation Failed.");

                CameraManager.Instance.Initialize(videoInformationData);
            }
            catch (Exception ex)
            {
                InnotiveDebug.Log.Error("Could not get VideoInformation.", ex);
                InnotiveDebug.Trace("Could not get VideoInformation." + ex.Message);

                if (MainProcess.Instance.ApplicationIsConsoleType)
                {
                    ConfirmPopupWindow.Show("Error", "Could not get VideoInformation.");
                }
                else if (MainProcess.Instance.ApplicationIsPlayerType)
                {
                    ConfirmPopupWindow.Show("Error", "Could not get VideoInformation.", 10000);
                }

                return false;
            }

            return true;
        }
        
        private static VideoInformation GetVideoInformationData()
        {
            try
            {
                // 테스트 파일에서 DeviceInfo 받기
                if (IsExistVideoInfoTestFile())
                {
                    return GetVideoInfoFromTestFile();
                }

                // CT에서 읽어오기
                if (!string.IsNullOrWhiteSpace(PlayerCommonsConfig.Instance.DataServiceUrl))
                {
                    return GetVideoInfoFromServer();
                }
            }
            catch (Exception ex)
            {
                InnotiveDebug.Log.Error(ex.Message);
            }
            
            // TODO - VideoDevices.xml 파일 같은 Web 설정 파일에서 읽어 오는 루틴 필요 by shwlee.

            return null;
        }
        
        /// <summary>
        /// Device Info Config 가져오기.
        /// </summary>
        /// <returns>
        /// The DeviceInfoConfig.
        /// </returns>
        public static DeviceInfoConfig GetDeviceInfoConfig()
        {
            string recvServerData = string.Empty;

            var projectId = CommonsConfig.Instance.ProjectID;

            // 테스트 파일에서 DeviceInfo 받기
            //recvServerData = IsExistDeviceInfoTestFile() ? GetDeviceInfoFromTestFile() : GetDeviceInfoFromServer(projectId);

            // log 파일 저장
            DeviceInfoConfig config = DeviceInfoConfig.ReadDataFromXML(recvServerData);
            config.SaveDataToXML(deviceInfoLogFilename);

            return config;
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Device Info 처리를 담당.
        /// </summary>
        /// <returns>
        /// The get device info data.
        /// </returns>
        private static DeviceInfoConfig GetDeviceInfoData()
        {
            // 테스트 파일에서 DeviceInfo 받기
            if (IsExistDeviceInfoTestFile())
            {
                return GetDeviceInfoFromTestFile();
            }

            if (!string.IsNullOrWhiteSpace(PlayerCommonsConfig.Instance.DataServiceUrl))
            {
                return GetDeviceInfoFromTestFile();
            }
            else
            {
                // GetDeviceinfoFromLocalDB();

                return GetDeviceinfoFromXmlFile();
            }
        }

        private static DeviceInfoConfig GetDeviceinfoFromXmlFile()
        {
            var config = DeviceInfoConfig.GetDeviceinfoFromVideoDevicesFile(_videoDevicesFilename);
            config.SaveDataToXML(deviceInfoLogFilename);
            return config;
        }

        private static VideoInformation GetVideoInfoFromServer()
        {
            var url = UrlFactory.GetUrlE(
                PlayerCommonsConfig.Instance.DataServiceUrl,
                "GetVideoInformation?",
                CommonsConfig.Instance.ProjectID);

            var data = RestService.GetMessage(url);
            if (string.IsNullOrWhiteSpace(data))
            {
                Debug.Assert(false, url + "에 대한 응답을 서버로 부터 못했습니다.");
                return null;
            }
            
            var videoInfo = VideoInformation.ReadDataFromXML(data);
            videoInfo.SaveDataToXML(videoInfoLogFilename);

            return videoInfo;
        }

        //private static string GetDeviceInfoFromServer(string projectName)
        //{
        //    // by jhlee (linker 삭제 작업)
        //    //var svrData = new ServiceClient();

        //    DeviceInfoConfig config = null;

        //    EndpointAddress endPoint = new EndpointAddress(PlayerCommonsConfig.Instance.DataServiceEndpointAddress);
        //    var svrData = new ServiceClient(InnoNetwork.NewBasicHttpBinding(), endPoint);

        //    string recvData = string.Empty;

        //    InnoNetwork.SafeWcfDoActionAndClose(svrData, client => { recvData = svrData.GetDeviceInfoXml(projectName); });

        //    config = DeviceInfoConfig.ReadDataFromXML(recvData);
        //    return config.SaveDataToXML(deviceInfoLogFilename);
        //}

        private static DeviceInfoConfig GetDeviceInfoFromTestFile()
        {
            var config = DeviceInfoConfig.ReadDataFromXMLFile(deviceInfoTestFilename);
            config.SaveDataToXML(deviceInfoLogFilename);
            return config;
        }

        //private static string GetDeviceInfoFromTestFile()
        //{
        //    DeviceInfoConfig deviceinfo = DeviceInfoConfig.ReadDataFromXMLFile(deviceInfoTestFilename);

        //    return deviceinfo.SaveDataToXML();
        //}

        private static VideoInformation GetVideoInfoFromTestFile()
        {
            var videoInfo = VideoInformation.ReadDataFromXMLFile(videoInfoTestFilename);
            videoInfo.SaveDataToXML(videoInfoLogFilename);

            return videoInfo;
        }

        //private static string GetCameraResolutionInfoFromTestFile()
        //{
        //    CameraResolutionInfo cameraResolutionInfo = CameraResolutionInfo.ReadDataFromXMLFile(cameraResolutionTestFilename);

        //    return cameraResolutionInfo.SaveDataToXML();
        //}

        /// <summary>
        /// Is exist device info file.
        /// </summary>
        /// <returns>
        /// The is exist device info test file.
        /// </returns>
        private static bool IsExistVideoInfoTestFile()
        {
            if (File.Exists(videoInfoTestFilename))
            {
                InnotiveDebug.Trace("VideoInfo 테스트 용 파일을 사용중입니다. 서버에서 카메라 목록을 받아오지 않고, 테스트 파일에 있는 카메라를 사용합니다. ");
                InnotiveDebug.Log.Info("VideoInfo 테스트 용 파일을 사용중입니다. 서버에서 카메라 목록을 받아오지 않고, 테스트 파일에 있는 카메라를 사용합니다. ");

                return true;
            }

            return false;
        }

        /// <summary>
        /// Is exist device info file.
        /// </summary>
        /// <returns>
        /// The is exist device info test file.
        /// </returns>
        private static bool IsExistDeviceInfoTestFile()
        {
            if (File.Exists(deviceInfoTestFilename))
            {
                InnotiveDebug.Trace("DeviceInfo 테스트 용 파일을 사용중입니다. 서버에서 카메라 목록을 받아오지 않고, 테스트 파일에 있는 카메라를 사용합니다. ");
                InnotiveDebug.Log.Info("DeviceInfo 테스트 용 파일을 사용중입니다. 서버에서 카메라 목록을 받아오지 않고, 테스트 파일에 있는 카메라를 사용합니다. ");

                return true;
            }

            return false;
        }

        private static bool IsExistVideoDevicesFile()
        {
            if (File.Exists(_videoDevicesFilename))
            {
                InnotiveDebug.Trace("ExistVideoDevices.xml  파일을 사용중입니다.");
                InnotiveDebug.Log.Info("ExistVideoDevices.xml  파일을 사용중입니다.");

                return true;
            }

            return false;
        }

        #endregion
    }
}