﻿#pragma checksum "..\..\..\..\UIs\Popups\ConfirmPopupWindow.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "CF01BECCC19C431438F5CD1CE25F2544"
//------------------------------------------------------------------------------
// <auto-generated>
//     이 코드는 도구를 사용하여 생성되었습니다.
//     런타임 버전:4.0.30319.34003
//
//     파일 내용을 변경하면 잘못된 동작이 발생할 수 있으며, 코드를 다시 생성하면
//     이러한 변경 내용이 손실됩니다.
// </auto-generated>
//------------------------------------------------------------------------------

using Microsoft.Windows.Themes;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace Innotive.InnoWatch.PlayerCommons.UIs.Popups {
    
    
    /// <summary>
    /// ConfirmPopupWindow
    /// </summary>
    public partial class ConfirmPopupWindow : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 374 "..\..\..\..\UIs\Popups\ConfirmPopupWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Border xTitleBorder;
        
        #line default
        #line hidden
        
        
        #line 376 "..\..\..\..\UIs\Popups\ConfirmPopupWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock xTitleTextBlock;
        
        #line default
        #line hidden
        
        
        #line 378 "..\..\..\..\UIs\Popups\ConfirmPopupWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button xCloseButton;
        
        #line default
        #line hidden
        
        
        #line 383 "..\..\..\..\UIs\Popups\ConfirmPopupWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock xMessageTextBlock;
        
        #line default
        #line hidden
        
        
        #line 387 "..\..\..\..\UIs\Popups\ConfirmPopupWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button xConfirmButtom;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/PlayerCommons;component/uis/popups/confirmpopupwindow.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\..\UIs\Popups\ConfirmPopupWindow.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.xTitleBorder = ((System.Windows.Controls.Border)(target));
            return;
            case 2:
            this.xTitleTextBlock = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 3:
            this.xCloseButton = ((System.Windows.Controls.Button)(target));
            return;
            case 4:
            this.xMessageTextBlock = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 5:
            this.xConfirmButtom = ((System.Windows.Controls.Button)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

