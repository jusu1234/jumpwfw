﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PlayerCommonsConfig.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Defines the PlayerCommonsConfig type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.PlayerCommons.Configs
{
    using Innotive.InnoWatch.Commons.BaseControls;

    /// <summary>
    /// PlayerCommons에서 정의하는 속성들.
    /// </summary>
    public class PlayerCommonsConfig
    {
        #region Construct

        private PlayerCommonsConfig()
        {
            // Singleton 생성자.
        }

        #endregion 

        #region Properties

        /// <summary>
        /// 싱글턴 생성용 static 함수.
        /// </summary>
        public static PlayerCommonsConfig Instance
        {
            get
            {
                return Nested<PlayerCommonsConfig>.Instance;
            }
        }

        /// <summary>
        /// 설명: Data Service
        /// 기본값: http://localhost:25000/. 
        /// </summary>
        public string DataServiceUrl { get; set; }

        /// <summary>
        /// 설명: Event Service
        /// 기본값: http://localhost:27000/. 
        /// </summary>
        public string EventServiceUrl { get; set; }
        
        /// <summary>
        /// 설명: Erp 데이터 업데이트시 타임아웃 시간.
        /// 형식: int
        /// 기본값 : 100
        /// </summary>
        public int ErpDataTimeout { get; set; }

        /// <summary>
        /// 설명: iCommand 실행 상태에서 iControlManager 실행 가능 여부
        /// 형식: bool
        /// 기본값: True
        /// </summary>
        public bool UseIControlManager { get; set; }

        /// <summary>
        /// 설명: iControlManager 설치 경로
        /// 형식: string
        /// 기본값: C:\Program Files\Innowatch\iControl Manager\iControlManager.exe
        /// </summary>
        public string IControlManagerPath { get; set; }

        /// <summary>
        /// 설명: App팀에서 만든 Sync Server IP, Port
        /// 형식: string
        /// 기본값 : 172.16.10.75:7000
        /// </summary>
        public string AppSyncServerIp { get; set; }

        /// <summary>
        /// 설명: App팀에서 만든 Sync Server Port
        /// 형식: int
        /// 기본값 : 7000
        /// </summary>
        public int AppSyncServerPort { get; set; }

        /// <summary>
        /// 설명: App팀에서 만든 Sync Server 사용 여부
        /// 형식: bool
        /// 기본값 : true
        /// </summary>
        public bool UseAppSyncServer { get; set; }
        
        /// <summary>
        /// Gets or sets UseHardwareAcceleration.
        /// </summary>
        public bool UseHardwareAcceleration { get; set; }

        /// <summary>
        /// Gets or sets envPath.
        /// </summary>
        public string EnvPath { get; set; }

        /// <summary>
        /// 설명: MouseLeftDown 시 Drag로 인식되기 위해 필요한 최소 이동 거리.
        /// 형식: string (pixel 단위)
        /// 기본값 : 0
        /// </summary>
        public double PanningStartPixel { get; set; }

        /// <summary>
        /// Layout의 Zoom In/Out시 마우스 Wheel 사용 여부.
        /// </summary>
        public bool LayoutUseMouseWheel { get; set; }

        /// <summary>
        /// 레이아웃이 로드 시 내부 미러 컨트롤에 연결된 디스플레이 에어리어를 스타트 레이아웃으로 초기화 시킬지 여부.
        /// </summary>
        public bool InitializeMirror { get; set; }

        /// <summary>
        /// 설명: Panning시 움직이면 끝났을때(MouseUp) 한번 Sync 메세지를 보냄 !!
        /// 형식: bool
        /// 기본값 : false
        /// </summary>
        public bool SendPanningMessageOnlyPanningEnd { get; set; }

        /// <summary>
        /// 설명: Zoom/Panning시 Camera Pause Message를 보낼지 여부
        /// 형식: bool
        /// 기본값 : false
        /// </summary>
        public bool PauseCameraBrush { get; set; }

        /// <summary>
        /// 설명: iHUB에서 호출할 MultiGrid 서비스와 CT에서 알람을 전송할 서비스의 IP.
        /// 형식: string
        /// </summary>
        public string ApplicationEventServiceUrl { get; set; }

        /// <summary>
        /// 설명: 사용자의 마우스 조작이 없을 경우 DisplayArea에 지정된 액션을 실행할지 여부를 설정.
        /// 형식: bool
        /// </summary>
        public bool EnableDisplayAreaIdleTimeAction { get; set; }

        /// <summary>
        /// 설명: Stage 오른쪽 위 Sync Toggle Button을 사용할지 여부
        /// 형식: bool
        /// </summary>
        public bool UseSyncToggleButton { get; set; }

        ///// <summary>
        ///// 설명 : 외부 장치에 의한 PTZ Control을 서비스하는 Rest 서버 포트입니다.
        ///// 기본값 : 36000
        ///// </summary>
        public int PTZControllerServicePort { get; set; }
        
        #endregion
    }
}