﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PlayerCommonStopScreenOutsideLowLevelVideoNode.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Defines the PlayerCommonStopScreenOutsideLowLevelVideoNode type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.PlayerCommons.Configs.CameraNodes.DisplayRuleNode
{
    using Commons.Configs;
    using System.Xml.Serialization;

    /// <summary>
    /// 화면 밖에 있는 영상인경우 저해상도 영상 재생을 잠시 멈출것인지 여부를 설정합니다.
    /// </summary>
    public class PlayerCommonStopScreenOutsideLowLevelVideoNode : ConvertBoolean
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PlayerCommonStopScreenOutsideLowLevelVideoNode"/> class.
        /// </summary>
        public PlayerCommonStopScreenOutsideLowLevelVideoNode()
        {
            // 이 생성자는 Serialization에서 사용합니다.
            // 이 생성자는 개발자 작업용이 아닙니다. 
            // NameLabelVisibleNode(bool defaltValue) 생성자를 사용합니다.
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PlayerCommonStopScreenOutsideLowLevelVideoNode"/> class.
        /// </summary>
        /// <param name="defaultValue">
        /// The default value.
        /// </param>
        public PlayerCommonStopScreenOutsideLowLevelVideoNode(bool defaultValue)
            : base(defaultValue)
        {
            // base에서 구현되었음.

            this.Comment = "화면 바깥 저해상도 영상을 백그라운드 재생 여부를 설정합니다. 사용하면 성능 향상이 있습니다. (true/false)";
        }
    }
}
