﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StatusLabelVisibleNode.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Defines the StatusLabelVisibleNode type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.PlayerCommons.Configs.CameraNodes.UINodes
{
    using System.Xml.Serialization;
    using Innotive.InnoWatch.Commons.Configs;

    /// <summary>
    /// 카메라 레이블을 카메라 위에 표시할 것인지 설정한다.
    /// H: 고해상도.
    /// L:저해상도.
    /// N:저해상도에서 고해상도로 변경 중.
    /// D: 데이타베이스에 있는 정보와 카메라의 해상도 정보가 일치하지 않음.
    /// E: 필터에서 에러가 발생함.
    /// 기본값: FALSE.
    /// </summary>
    [XmlRoot(ElementName = "StatusLabelVisible")]
    public class PlayerCommonStatusLabelVisibleNode : ConvertBoolean
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PlayerCommonStatusLabelVisibleNode"/> class.
        /// </summary>
        public PlayerCommonStatusLabelVisibleNode()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PlayerCommonStatusLabelVisibleNode"/> class.
        /// </summary>
        /// <param name="defaultValue">
        /// The default Value.
        /// </param>
        public PlayerCommonStatusLabelVisibleNode(bool defaultValue)
            : base(defaultValue)
        {
            this.Comment = "카메라 컨트롤에 저화질/고화질 여부 라벨을 표시합니다. (true/false)";
        }
    }
}
