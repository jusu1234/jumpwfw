﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PlayerCommonUINode.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Defines the CommandUINode type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.PlayerCommons.Configs.CameraNodes.UINodes
{
    using System.Xml.Serialization;

    /// <summary>
    /// Common의 UINode를 상속받았다.
    /// NameLabelVisible, NameLabelFormat, StatusLabelVisible을 가지고 있다.
    /// </summary>
    [XmlRoot(ElementName = "UI")]
    public class PlayerCommonUINode
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PlayerCommonUINode"/> class.
        /// </summary>
        public PlayerCommonUINode()
        {
            this.StatusLabelVisible = new PlayerCommonStatusLabelVisibleNode(false);    
        }

        /// <summary>
        /// Gets or sets StatusLabelVisible.
        /// </summary>
        public PlayerCommonStatusLabelVisibleNode StatusLabelVisible { get; set; }
    }
}
