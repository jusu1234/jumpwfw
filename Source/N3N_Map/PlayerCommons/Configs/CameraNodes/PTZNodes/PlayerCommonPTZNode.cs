﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PlayerCommonPTZNode.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   카메라 PTZ 제어를 사용할지 설정한다.
//   기본값: false.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.PlayerCommons.Configs.CameraNodes.PTZNodes
{
    using System.Xml.Serialization;
    using Innotive.InnoWatch.Commons.Configs.AttributeTypes;

    /// <summary>
    /// 카메라 PTZ 제어를 사용할지 설정한다.
    /// 기본값: false.
    /// </summary>
    [XmlRoot("PTZ")]
    public class PlayerCommonPTZNode
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PlayerCommonPTZNode"/> class.
        /// </summary>
        public PlayerCommonPTZNode()
        {
            this.Use = new UseNode(true, "카메라 PTZ 제어 사용 여부를 설정합니다. (true/false)");
            this.PTZServerPort = new PlayerCommonPTZServerPortNode();
        }

        /// <summary>
        /// Gets or sets Use.
        /// </summary>
        public UseNode Use { get; set; }

        public PlayerCommonPTZServerPortNode PTZServerPort { get; set; }
    }
}
