﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Innotive.InnoWatch.Commons.Configs;

namespace Innotive.InnoWatch.PlayerCommons.Configs.CameraNodes.SwitchingRuleNodes
{
    [XmlRoot(ElementName = "SwitchingUsePause")]
    public class PlayerCommonSwitchingUsePause : ConvertBoolean
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PlayerCommonSwitchingSpeedNode"/> class.
        /// </summary>
        public PlayerCommonSwitchingUsePause()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PlayerCommonSwitchingSpeedNode"/> class.
        /// </summary>
        /// <param name="defaultValue">
        /// The default Value.
        /// </param>
        public PlayerCommonSwitchingUsePause(bool defaultValue)
            : base(defaultValue)
        {
            this.Comment = "Zoom/Panning 시 기존 영상을 Pause 할지 여부를 설정 합니다. (false 시 Stop)";
        }
    }
}
