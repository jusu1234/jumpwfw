﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PlayerCommonHighToLowSpeedNode.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Defines the HighToLowSpeedNode type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------



namespace Innotive.InnoWatch.PlayerCommons.Configs.CameraNodes.SwitchingRuleNodes
{
    using System.Xml.Serialization;

    /// <summary>
    /// 고해상도에서 저해상도로 넘어가는 에니메이션 실행 시간을 설정한다.
    /// 초 단위를 사용한다.
    /// 0값이 설정되면 에니메이션을 실행하지 않는다.
    /// 기본값: 0.2.
    /// </summary>
    [XmlRoot(ElementName = "SwitchingSpeed")]
    public class PlayerCommonSwitchingSpeedNode : Innotive.InnoWatch.Commons.Configs.AttributeTypes.BaseNode
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PlayerCommonSwitchingSpeedNode"/> class.
        /// </summary>
        public PlayerCommonSwitchingSpeedNode()
        {
            this.Value = 0.2;
            this.Comment = "고화질에서 저화질로 전환되는 속도를 설정합니다. (단위: 초)";
        }

        /// <summary>
        /// Gets or sets a value indicating whether Value.
        /// </summary>
        [XmlAttribute]
        public double Value { get; set; }
    }
}
