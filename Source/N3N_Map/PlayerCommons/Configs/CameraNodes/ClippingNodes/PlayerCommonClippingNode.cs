﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PlayerCommonClippingNode.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Defines the ClippingNode type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.PlayerCommons.Configs.CameraNodes.ClippingNodes
{
    using System.Xml.Serialization;

    /// <summary>
    /// 카메라 영상을 잘라내는 속성들을 가지고 있는 클래스.
    /// </summary>
    [XmlRoot(ElementName = "Clipping")]
    public class PlayerCommonClippingNode
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PlayerCommonClippingNode"/> class.
        /// </summary>
        public PlayerCommonClippingNode()
        {
            //this.HighStreamVideo = new PlayerCommonHighStreamVideoNode();
            //this.Merger = new PlayerCommonMergerNode();
            this.MergerCell = new PlayerCommonMergerCellNode();
        }

        ///// <summary>
        ///// Gets or sets HighStreamVideo.
        ///// </summary>
        //public PlayerCommonHighStreamVideoNode HighStreamVideo { get; set; }

        ///// <summary>
        ///// Gets or sets MergerBottom.
        ///// </summary>
        //public PlayerCommonMergerNode Merger { get; set; }

        /// <summary>
        /// Gets or sets MergerCell.
        /// </summary>
        public PlayerCommonMergerCellNode MergerCell { get; set; }
    }
}
