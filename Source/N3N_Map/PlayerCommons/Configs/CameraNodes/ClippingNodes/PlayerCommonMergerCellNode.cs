﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PlayerCommonMergerCellNode.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   머징 영상 중 하나에 셀에 대해 각각 처리한다.
//   Left, Top, Right, Bottom.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.PlayerCommons.Configs.CameraNodes.ClippingNodes
{
    using System.Windows;
    using System.Xml.Serialization;

    /// <summary>
    /// 머징 영상 중 하나에 셀에 대해 각각 처리한다.
    /// Left, Top, Right, Bottom.
    /// 기본값: 5,1,5,0.
    /// </summary>
    [XmlRoot(ElementName = "MergerCell")]
    public class PlayerCommonMergerCellNode : Innotive.InnoWatch.Commons.Configs.AttributeTypes.BaseNode
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PlayerCommonMergerCellNode"/> class.
        /// </summary>
        public PlayerCommonMergerCellNode()
        {
            this.Left = 0;
            this.Top = 0;
            this.Right = 0;
            this.Bottom = 0;

            this.UseCropMergerCellByIndividualSetting = false;

            this.Comment = "저화질 개별 영상의 외곽에 여백이 있을 경우 자르는 크기를 설정합니다. (단위: %). UseCropMergerCellByIndividualSetting를 true로 설정하면 개별 카메라 설정되어 있는 짜르기 속성값을 사용하여 그 값에 대한 저화질 해상도 비율에 따라 영상을 자릅니다.";
            this.Type = "none";
        }

        /// <summary>
        /// Gets or sets a value indicating whether Value.
        /// </summary>
        [XmlAttribute]
        public double Left { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether Value.
        /// </summary>
        [XmlAttribute]
        public double Top { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether Value.
        /// </summary>
        [XmlAttribute]
        public double Right { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether Value.
        /// </summary>
        [XmlAttribute]
        public double Bottom { get; set; }


        /// <summary>
        /// Gets or sets a value indicating whether Value.
        /// </summary>
        [XmlAttribute]
        public bool UseCropMergerCellByIndividualSetting { get; set; }

        /// <summary>
        /// 속성들을 Rect 형식으로 반환한다.
        /// </summary>
        /// <returns>
        /// The Rect.
        /// </returns>
        public Thickness GetValue()
        {
            return new Thickness(this.Left, this.Top, this.Right, this.Bottom);
        }
    }
}
