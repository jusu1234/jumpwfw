﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PlayerCommonContentsNode.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Defines the PlayerCommonContentsNode type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.PlayerCommons.Configs.ModeNodes.ContentsNodes
{
    using Innotive.InnoWatch.Commons.Configs.AttributeTypes;

    /// <summary>
    /// </summary>
    public class PlayerCommonContentsNode
    {
        public PlayerCommonContentsNode()
        {
            this.Use = new UseNode(false, "컨텐츠 모드 사용 여부를 설정합니다. (true/false)");
        }

        public UseNode Use { get; set; }
    }
}
