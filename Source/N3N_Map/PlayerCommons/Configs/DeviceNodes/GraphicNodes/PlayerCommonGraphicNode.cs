﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PlayerCommonGraphicNode.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Defines the PlayerCommonGraphicNode type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.PlayerCommons.Configs.DeviceNodes.GraphicNodes
{
    /// <summary>
    /// 하드웨어 가속을 사용할지 설정한다.
    /// 특이상황이 아니면 하드웨어 가속을 항상 사용한다.
    /// </summary>
    public class PlayerCommonGraphicNode
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PlayerCommonGraphicNode"/> class.
        /// </summary>
        public PlayerCommonGraphicNode()
        {
            this.UseHardwareAcceleration = new PlayerCommonUseHardwareAccelerationNode(true);
        }

        /// <summary>
        /// Gets or sets UseHardwareAcceleration.
        /// </summary>
        public PlayerCommonUseHardwareAccelerationNode UseHardwareAcceleration { get; set; }
    }
}
