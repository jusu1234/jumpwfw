﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PlayerCommonCursorVisibleNode.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Defines the PlayerCommonCursorVisibleNode type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.PlayerCommons.Configs.DeviceNodes.MouseNodes
{
    using Commons.Configs;
    using System.Xml.Serialization;

    /// <summary> 
    /// 마우스 커서를 나타나게 할 것인지를 설정한다.
    /// 기본값: iCommand: true 
    /// 기본값: iDisplay: false.
    /// </summary>
    public class PlayerCommonCursorVisibleNode : ConvertBoolean
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PlayerCommonCursorVisibleNode"/> class.
        /// </summary>
        public PlayerCommonCursorVisibleNode()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PlayerCommonCursorVisibleNode"/> class.
        /// </summary>
        /// <param name="value">
        /// The value.
        /// </param>
        public PlayerCommonCursorVisibleNode(bool value) : base(value)
        {
            this.Comment = "마우스 커서 보이기 여부를 설정합니다.  (true/false)";
        }
    }
}
