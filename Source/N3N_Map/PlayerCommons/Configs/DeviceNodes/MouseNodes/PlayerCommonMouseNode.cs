﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PlayerCommonMouseNode.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Defines the PlayerCommonMouseNode type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.PlayerCommons.Configs.DeviceNodes.MouseNodes
{
    /// <summary>
    /// 장치중 마우스에 관련된 속성들을 관리하는 클래스이다.
    /// </summary>
    public class PlayerCommonMouseNode
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PlayerCommonMouseNode"/> class.
        /// </summary>
        public PlayerCommonMouseNode()
        {
            // 이 생성자는 개발자가 사용하는 용도가 아님.
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PlayerCommonMouseNode"/> class.
        /// </summary>
        /// <param name="cursorVisible">
        /// The cursor Visible.
        /// </param>
        public PlayerCommonMouseNode(bool cursorVisible)
        {
            this.CursorVisible = new PlayerCommonCursorVisibleNode(cursorVisible);
        }

        /// <summary>
        /// Gets or sets CursorVisible.
        /// </summary>
        public PlayerCommonCursorVisibleNode CursorVisible { get; set; }
    }
}
