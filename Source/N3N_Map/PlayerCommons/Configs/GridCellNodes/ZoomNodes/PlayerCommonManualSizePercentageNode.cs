﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PlayerCommonManualSizePercentageNode.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Defines the PlayerCommonManualSizePercentageNode type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.PlayerCommons.Configs.GridCellNodes.ZoomNodes
{
    using System.Windows;
    using System.Xml.Serialization;

    /// <summary>
    /// </summary>
    public class PlayerCommonManualSizePercentageNode : Innotive.InnoWatch.Commons.Configs.AttributeTypes.BaseNode
    {
        public PlayerCommonManualSizePercentageNode()
        {
            this.Left = 0.0;
            this.Top = 0.0;
            this.Width = 100.0;
            this.Height = 100.0;
            this.Comment = "Stage Cell Double Click 시 확대될 Cell의 크기를 설정합니다. (단위: Percent)";
            this.Type = "none";
        }

        [XmlAttribute]
        public double Left { get; set; }

        [XmlAttribute]
        public double Top { get; set; }

        [XmlAttribute]
        public double Width { get; set; }

        [XmlAttribute]
        public double Height { get; set; }

        /// <summary>
        /// 렉트 값을 반환한다.
        /// </summary>
        /// <returns>
        /// The Rect.
        /// </returns>
        public Rect GetValue()
        {
            return new Rect(this.Left, this.Top, this.Width, this.Height);
        }
    }
}
