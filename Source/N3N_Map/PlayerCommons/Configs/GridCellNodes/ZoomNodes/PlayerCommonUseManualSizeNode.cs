﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PlayerCommonUseManualSizeNode.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Defines the PlayerCommonUseManualSizeNode type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.PlayerCommons.Configs.GridCellNodes.ZoomNodes
{
    using Commons.Configs;
    using System.Xml.Serialization;

    /// <summary>
    /// Grid 줌에 대한 설정을 수동을 설정합니다.
    /// </summary>
    public class PlayerCommonUseManualSizeNode : ConvertBoolean
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PlayerCommonUseManualSizeNode"/> class.
        /// </summary>
        public PlayerCommonUseManualSizeNode()
        {
            // 이 생성자는 개발자가 사용하지 않습니다.
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PlayerCommonUseManualSizeNode"/> class.
        /// </summary>
        /// <param name="defaultValue">
        /// The default value.
        /// </param>
        public PlayerCommonUseManualSizeNode(bool defaultValue) : base(defaultValue)
        {
            this.Comment = "ManualSizePercentage를 사용할지 여부를 설정합니다. 사용하지 않으면 Stage 전체에 표시됩니다. (true/false)";
        }
    }
}
