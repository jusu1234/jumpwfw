﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DisplayCommonNode.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Defines the DisplayCommon type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.PlayerCommons.Configs
{
    using System.Xml.Serialization;
    using CameraNodes;
    using CommunicationNodes;
    using ControlNodes;
    using DeviceNodes;
    using FileNodes;
    using FilterNodes;
    using GridCellNodes;
    using ModeNodes;

    using Innotive.InnoWatch.Commons.Publics;
    using Innotive.InnoWatch.Commons.Configs;
    using Innotive.InnoWatch.PlayerCommons.Configs.DisplayAreaNodes;

    /// <summary>
    /// iDisplay와 iCommand가 공유하는 속성을 관리한다.
    /// </summary>
    [XmlRoot(ElementName = "DisplayCommon")]
    public class DisplayCommonNode
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DisplayCommonNode"/> class.
        /// </summary>
        public DisplayCommonNode()
        {
            // 개발자가 사용하는 생성자가 아님.
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DisplayCommonNode"/> class.
        /// </summary>
        /// <param name="cursorVisible">
        /// The cursor Visible.
        /// 마우스 커서를 보이게 할 것인지에 대한 기본 설정 값.
        /// </param>
        public DisplayCommonNode(bool cursorVisible)
        {
            this.Camera = new PlayerCommonCameraNode();
            this.Communication = new PlayerCommonCommunicationNode();
            this.Control = new PlayerCommonControlNode();
            this.Device = new PlayerCommonDeviceNode(cursorVisible);
            this.File = new PlayerCommonFileNode();
            this.Filter = new PlayerCommonFilterNode();
            this.GridCell = new PlayerCommonGridCellNode();
            this.Mode = new PlayerCommonModeNode();
            this.DisplayArea = new PlayerCommonDisplayAreaNode();
        }

        /// <summary>
        /// 다른 Config와 머지를 한다. default_config의 Editable 값 기준으로 함.
        /// </summary>
        /// <param name="default_config"></param>
        public void Merge(DisplayCommonNode default_config)
        {
            CommonsConfig.FindEditableAndSet(this, default_config);
        }

        /// <summary>
        /// Gets or sets CameraNode.
        /// </summary>
        public PlayerCommonCameraNode Camera { get; set; }

        /// <summary>
        /// Gets or sets Communication.
        /// </summary>
        public PlayerCommonCommunicationNode Communication { get; set; }

        /// <summary>
        /// Gets or sets Control.
        /// </summary>
        public PlayerCommonControlNode Control { get; set; }

        /// <summary>
        /// Gets or sets Device.
        /// </summary>
        public PlayerCommonDeviceNode Device { get; set; }

        /// <summary>
        /// Gets or sets File.
        /// </summary>
        public PlayerCommonFileNode File { get; set; }

        /// <summary>
        /// Gets or sets Filter.
        /// </summary>
        public PlayerCommonFilterNode Filter { get; set; }

        /// <summary>
        /// Gets or sets GridCell.
        /// </summary>
        public PlayerCommonGridCellNode GridCell { get; set; }

        /// <summary>
        /// Gets or sets Mode.
        /// </summary>
        public PlayerCommonModeNode Mode { get; set; }

        /// <summary>
        /// Gets or sets DisplayArea.
        /// </summary>
        public PlayerCommonDisplayAreaNode DisplayArea { get; set; }
    }
}
