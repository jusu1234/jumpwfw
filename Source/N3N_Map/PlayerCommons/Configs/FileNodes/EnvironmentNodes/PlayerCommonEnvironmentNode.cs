﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PlayerCommonEnvironmentNode.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Defines the PlayerCommonEnvironmentNode type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Xml.Serialization;

namespace Innotive.InnoWatch.PlayerCommons.Configs.FileNodes.EnvironmentNodes
{
    /// <summary>
    /// </summary>
    [XmlRoot(ElementName = "Environment")]
    public class PlayerCommonEnvironmentNode
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PlayerCommonEnvironmentNode"/> class.
        /// </summary>
        public PlayerCommonEnvironmentNode()
        {
            this.Path = new PlayerCommonPathNode();
        }

        /// <summary>
        /// Gets or sets Path.
        /// </summary>
        public PlayerCommonPathNode Path { get; set; }
    }
}
