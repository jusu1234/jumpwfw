﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PlayerCommonFileNode.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Defines the PlayerCommonFileNode type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Xml.Serialization;
using Innotive.InnoWatch.PlayerCommons.Configs.FileNodes.EnvironmentNodes;

namespace Innotive.InnoWatch.PlayerCommons.Configs.FileNodes
{
    /// <summary>
    /// </summary>
    [XmlRoot("File")]
    public class PlayerCommonFileNode
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PlayerCommonFileNode"/> class.
        /// </summary>
        public PlayerCommonFileNode()
        {
            this.Environment = new PlayerCommonEnvironmentNode();
        }

        /// <summary>
        /// Gets or sets Environment.
        /// </summary>
        public PlayerCommonEnvironmentNode Environment { get; set; }
    }
}
