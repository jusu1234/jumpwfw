﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PlayerCommonFilterNode.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Defines the PlayerCommonFilterNode type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.PlayerCommons.Configs.FilterNodes
{
    using System.Xml.Serialization;    
    using LimitNodes;

    /// <summary>
    /// DisplayCommon -> Filter.
    /// </summary>
    [XmlRoot(ElementName = "Filter")]
    public class PlayerCommonFilterNode
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PlayerCommonFilterNode"/> class.
        /// </summary>
        public PlayerCommonFilterNode()
        {
            this.Limit = new PlayerCommonLimitNode();
        }

        /// <summary>
        /// Gets or sets Limit.
        /// </summary>
        public PlayerCommonLimitNode Limit { get; set; }
    }
}
