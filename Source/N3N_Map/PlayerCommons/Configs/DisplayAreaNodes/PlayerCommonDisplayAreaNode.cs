﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PlayerCommonDisplayAreaNode.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Defines the PlayerCommonDisplayAreaNode type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Xml.Serialization;
using Innotive.InnoWatch.PlayerCommons.Configs.DisplayAreaNodes.IdleTimeActionNodes;
namespace Innotive.InnoWatch.PlayerCommons.Configs.DisplayAreaNodes
{
    /// <summary>
    /// </summary>
    [XmlRoot("DisplayArea")]
    public class PlayerCommonDisplayAreaNode
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PlayerCommonDisplayAreaNode"/> class.
        /// </summary>
        public PlayerCommonDisplayAreaNode()
        {
            this.IdleTimeAction = new PlayerCommonIdleTimeActionNode();
        }

        /// <summary>
        /// Gets or sets Environment.
        /// </summary>
        public PlayerCommonIdleTimeActionNode IdleTimeAction { get; set; }
    }
}
