﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PlayerCommonIdleTimeActionNode.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Defines the PlayerCommonIdleTimeActionNode type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.PlayerCommons.Configs.DisplayAreaNodes.IdleTimeActionNodes
{
    using Innotive.InnoWatch.Commons.Configs.AttributeTypes;

    /// <summary>
    /// </summary>
    public class PlayerCommonIdleTimeActionNode
    {
        public PlayerCommonIdleTimeActionNode()
        {
            this.Use = new UseNode(false, "사용자의 마우스 조작이 없을 경우 DisplayArea에 지정된 액션을 실행할지 여부를 설정합니다. (true/false)");
        }

        public UseNode Use { get; set; }
    }
}
