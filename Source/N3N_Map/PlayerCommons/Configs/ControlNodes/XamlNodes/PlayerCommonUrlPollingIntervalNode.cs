﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PlayerCommonUrlPollingIntervalNode.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Defines the PlayerCommonUrlPollingIntervalNode type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.PlayerCommons.Configs.ControlNodes.XamlNodes
{
    using System.Xml.Serialization;

    /// <summary>
    /// XamlViewerControl의 URL 폴링 주기를 ms단위로 설정한다. (기본값 500)
    /// </summary>
    [XmlRoot("UrlPollingInterval")]
    public class PlayerCommonUrlPollingIntervalNode : Innotive.InnoWatch.Commons.Configs.AttributeTypes.BaseNode
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PlayerCommonUrlPollingIntervalNode"/> class.
        /// </summary>
        public PlayerCommonUrlPollingIntervalNode()
        {
            this.Value = 500;
            this.Comment = "XamlViewerControl의 URL 폴링 주기를 ms단위로 설정한다. (기본값 500)";
        }

        /// <summary>
        /// Gets or sets Value.
        /// </summary>
        [XmlAttribute]
        public int Value { get; set; }
    }
}
