﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PlayerCommonControlNode.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Defines the PlayerCommonControlNode type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.PlayerCommons.Configs.ControlNodes
{
    using System.Xml.Serialization;
    using LayoutNodes;
    using XamlNodes;

    /// <summary>
    /// 컨트롤의 속성들을 정의한다.
    /// Layout.
    /// Xaml.
    /// </summary>
    [XmlRoot(ElementName = "Control")]
    public class PlayerCommonControlNode
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PlayerCommonControlNode"/> class.
        /// </summary>
        public PlayerCommonControlNode()
        {
            this.Layout = new PlayerCommonLayoutNode();
            this.Xaml = new PlayerCommonXamlNode();
        }

        /// <summary>
        /// Gets or sets Layout.
        /// </summary>
        public PlayerCommonLayoutNode Layout { get; set; }
        
        /// <summary>
        /// Gets or sets Xaml.
        /// </summary>
        public PlayerCommonXamlNode Xaml { get; set; }
    }
}
