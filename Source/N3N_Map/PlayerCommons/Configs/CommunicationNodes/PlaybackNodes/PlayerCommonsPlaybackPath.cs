﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PlayerCommonsPlaybackPath.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Defines the PlayerCommonsPlaybackPath type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.PlayerCommons.Configs.CommunicationNodes.PlaybackNodes
{
    using System.Xml.Serialization;

    /// <summary>
    /// CMS Playback path.
    /// </summary>
    [XmlRoot(ElementName = "PlaybackPath")]
    public class PlayerCommonsPlaybackPathNode : Innotive.InnoWatch.Commons.Configs.AttributeTypes.BaseNode
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PlayerCommonsPlaybackPathNode"/> class.
        /// </summary>
        public PlayerCommonsPlaybackPathNode()
        {
            this.Value = "C:\\Program Files\\Innowatch\\Playback\\PlayBackDialog.exe";
            this.Comment = "Playback CMS가 설치된 경로를 설정합니다. (경로에 파일이 없으면 자동으로 사용하지 않습니다.)";
        }
        /// <summary>
        /// Gets or sets Value.
        /// </summary>
        [XmlAttribute]
        public string Value { get; set; }
    }
}
