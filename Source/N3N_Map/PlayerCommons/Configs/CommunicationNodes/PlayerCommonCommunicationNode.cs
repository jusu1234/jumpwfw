﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PlayerCommonCommunicationNode.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Defines the PlayerCommonCommunicationNode type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.PlayerCommons.Configs.CommunicationNodes
{
    using System.Xml.Serialization;
    using ControlServerNodes;
    using Innotive.InnoWatch.Commons.Publics;
    using Innotive.InnoWatch.CommonUtils.Models;
    using Innotive.InnoWatch.PlayerCommons.Configs.CommunicationNodes.ConnectionStatusIconNodes;
    using Innotive.InnoWatch.PlayerCommons.Configs.CommunicationNodes.ControlManager;
    using Innotive.InnoWatch.PlayerCommons.Configs.CommunicationNodes.PlaybackNodes;
    using SyncServerNodes;

    /// <summary>
    /// 통신에 관련된 속성들을 가지고 있는 클래스.
    /// </summary>
    [XmlRoot(ElementName = "Communication")]
    public class PlayerCommonCommunicationNode
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PlayerCommonCommunicationNode"/> class.
        /// </summary>
        public PlayerCommonCommunicationNode()
        {
            this.ConnectionStatusIcon = new PlayerCommonConnectionStatusIconNode();
            this.ControlServer = new PlayerCommonControlServerNode();
            this.Playback = new PlayerCommonPlaybackNode();
            this.ControlManager = new PlayerCommonsControlManagerNode();

            // iViewer.config에 나오지 않게 하기 위해 막은 것.
            this.SyncServer = Public.GetProgramType() == ProgramType.iViewer ? null : new PlayerCommonSyncServerNode();
        }
        
        /// <summary>
        /// Gets or sets ConnectionStatusIcon.
        /// </summary>
        public PlayerCommonConnectionStatusIconNode ConnectionStatusIcon { get; set; }
        
        /// <summary>
        /// Gets or sets ControlServer.
        /// </summary>
        public PlayerCommonControlServerNode ControlServer { get; set; }

        /// <summary>
        /// Gets or sets SyncServer.
        /// </summary>
        public PlayerCommonSyncServerNode SyncServer { get; set; }

        /// <summary>
        /// Gets or sets ControlManager.
        /// </summary>
        public PlayerCommonsControlManagerNode ControlManager { get; set; }

        /// <summary>
        /// Gets or sets Playback.
        /// </summary>
        public PlayerCommonPlaybackNode Playback { get; set; }
    }
}
