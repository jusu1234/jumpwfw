﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PlayerCommonControlServerNode.cs" company="Innotive Inc. Korea">
//   Copyright (c) 2012 Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Defines the PlayerCommonControlServerNode type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.PlayerCommons.Configs.CommunicationNodes.ControlServerNodes
{
    using System.Xml.Serialization;
    using Commons.Configs.AttributeTypes;

    /// <summary>
    /// The control server setting class.
    /// </summary>
    [XmlRoot(ElementName = "ControlServer")]
    public class PlayerCommonControlServerNode
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PlayerCommonControlServerNode"/> class.
        /// </summary>
        public PlayerCommonControlServerNode()
        {
            this.BindingServiceUrl = new UrlNode("Binding Provider Service URL을 설정합니다. (빈문자열이 설정되어 있으면 자동으로 사용하지 않게 설정됩니다. http://localhost:26000/rest/binding/)");
            this.DataServiceUrl = new UrlNode("Data Service URL을 설정합니다. (빈문자열이 설정되어 있으면 자동으로 사용하지 않게 설정됩니다. http://localhost:25000/rest/data/)");
            this.EventServiceUrl = new UrlNode("Event Service URL을 설정합니다. (빈문자열이 설정되어 있으면 자동으로 사용하지 않게 설정됩니다. http://localhost:27000/rest/event/)");
            this.LogServiceUrl = new UrlNode("Log Service URL을 설정합니다. (빈문자열이 설정되어 있으면 자동으로 사용하지 않게 설정됩니다. http://localhost:28000/rest/log/)");
        }

        /// <summary>
        /// Gets or sets BindingService.
        /// </summary>
        public UrlNode BindingServiceUrl { get; set; }

        /// <summary>
        /// Gets or sets DataService.
        /// </summary>
        public UrlNode DataServiceUrl { get; set; }

        /// <summary>
        /// Gets or sets EventService.
        /// </summary>
        public UrlNode EventServiceUrl { get; set; }

        /// <summary>
        /// Gets or sets LogServiceUrl.
        /// </summary>
        public UrlNode LogServiceUrl { get; set; }

        /// <summary>
        /// The get binding service default url.
        /// </summary>
        /// <returns>
        /// The default url.
        /// </returns>
        public string GetBindingServiceDefaultUrl()
        {
            return this.BindingServiceUrl.GetUrl() + "/GetBindingProvider?id";
        }
    }
}
