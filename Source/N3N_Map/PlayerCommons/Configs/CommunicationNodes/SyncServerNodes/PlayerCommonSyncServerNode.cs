﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PlayerCommonSyncServerNode.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Defines the PlayerCommonSyncServerNode type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.PlayerCommons.Configs.CommunicationNodes.SyncServerNodes
{
    using System.Xml.Serialization;
    using Commons.Configs.AttributeTypes;

    /// <summary>
    /// iConsole & iDisplay 간 동기 서버에 관련된 설정들이 있는 클래스.
    /// </summary>
    [XmlRoot(ElementName = "SyncServer")]
    public class PlayerCommonSyncServerNode
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PlayerCommonSyncServerNode"/> class.
        /// </summary>
        public PlayerCommonSyncServerNode()
        {
            this.IP = new IPNode("싱크 서버가 실행 중인 IP를 설정합니다. (0.0.0.0이나 빈문자열이 설정되어 있으면 자동으로 사용하지 않게 설정됩니다.)");
            this.Port = new PlayerCommonPortNode();
        }

        /// <summary>
        /// Gets or sets IP.
        /// </summary>
        public IPNode IP { get; set; }

        /// <summary>
        /// Gets or sets Port.
        /// </summary>
        public PlayerCommonPortNode Port { get; set; }
    }
}
