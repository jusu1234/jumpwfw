﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PlayerCommonsControlManagerNode.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Defines the PlayerCommonsControlManagerNode type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.PlayerCommons.Configs.CommunicationNodes.ControlManager
{
    using System.Xml.Serialization;
    using Innotive.InnoWatch.Commons.Configs.AttributeTypes;

    /// <summary>
    /// ControlManager config.
    /// </summary>
    [XmlRoot(ElementName = "ControlManager")]
    public class PlayerCommonsControlManagerNode
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PlayerCommonsControlManagerNode"/> class.
        /// </summary>
        public PlayerCommonsControlManagerNode()
        {
            this.Path = new PlayerCommonsPathNode();
        }
        
        /// <summary>
        /// Gets or sets Path.
        /// </summary>
        public PlayerCommonsPathNode Path { get; set; }
    }
}
