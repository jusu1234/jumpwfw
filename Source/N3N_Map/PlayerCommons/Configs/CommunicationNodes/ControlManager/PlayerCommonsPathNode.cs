﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PlayerCommonsPathNode.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Defines the PlayerCommonsPathNode type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.PlayerCommons.Configs.CommunicationNodes.ControlManager
{
    using System.Xml.Serialization;

    /// <summary>
    /// ICM path.
    /// </summary>
    [XmlRoot(ElementName = "Path")]
    public class PlayerCommonsPathNode : Innotive.InnoWatch.Commons.Configs.AttributeTypes.BaseNode
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PlayerCommonsPathNode"/> class.
        /// </summary>
        public PlayerCommonsPathNode()
        {
            this.Value = "C:\\Program Files\\Innowatch\\iControl Manager\\iControlManager.exe";
            this.Comment = "iControlManager가 설치된 경로를 설정합니다. (경로에 파일이 없으면 자동으로 사용하지 않습니다.)";
        }

        /// <summary>
        /// Gets or sets Value.
        /// </summary>
        [XmlAttribute]
        public string Value { get; set; }
    }
}
