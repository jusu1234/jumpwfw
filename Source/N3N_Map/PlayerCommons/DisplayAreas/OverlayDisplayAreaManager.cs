﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="OverlayDisplayAreaManager.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   overlay display area manager.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.PlayerCommons.DisplayAreas
{
    using System;
    using System.Collections.Generic;
    using Innotive.InnoWatch.Commons.Configs;
    using Innotive.InnoWatch.CommonUtils.Models.Sync.Control;
    using Innotive.InnoWatch.CommonUtils.Models.Sync.Multigrid;    
    using Innotive.InnoWatch.PlayerCommons.Communications.SyncStrategy;

    /// <summary>
    /// overlay display area manager.
    /// </summary>
    public class OverlayDisplayAreaManager
    {
        #region Constants and Fields
        
        private List<OverlayDisplayArea> gridOverlayDisplayAreaList = new List<OverlayDisplayArea>();

        #endregion

        #region Properties

        /// <summary>
        /// Gets GridCount.
        /// </summary>
        public int GridCount
        {
            get
            {
                return this.gridOverlayDisplayAreaList.Count;
            }
        }

        #endregion

        #region Public Methods
        
        /// <summary>
        /// add grid overlay display area.
        /// </summary>
        /// <param name="gridOverlayDisplayArea">
        /// The grid overlay display area.
        /// </param>
        public void AddGridOverlayDisplayArea(OverlayDisplayArea gridOverlayDisplayArea)
        {
            // Player인 경우 Hidden인 상태로 시작을 해야 함 !! Console은 Visible상태로 시작 !!
            if (MainProcess.Instance.ApplicationIsPlayerType)
            {
                gridOverlayDisplayArea.Show(false);
            }

            this.gridOverlayDisplayAreaList.Add(gridOverlayDisplayArea);
        }

        /// <summary>
        /// OverlayDisplayArea 지우기.
        /// </summary>
        /// <param name="syncGuid">
        /// MultiGridControl의 SyncGuid.
        /// </param>
        public void RemoveGridOverlayDisplayArea(Guid syncGuid)
        {
            var removeList = new List<OverlayDisplayArea>();
            foreach (var overlayDisplayArea in this.gridOverlayDisplayAreaList)
            {
                
            }

            foreach (var overlayDisplayArea in removeList)
            {
                

                this.gridOverlayDisplayAreaList.Remove(overlayDisplayArea);
            }
        }

        /// <summary>
        /// change display mode.
        /// </summary>
        /// <param name="displayMode">
        /// The display mode.
        /// </param>
        public void ChangeDisplayMode(ConsoleDisplayMode displayMode)
        {
            if (displayMode == ConsoleDisplayMode.ContentsMode)
            {
                if (CommonsConfig.Instance.EnableContentsMode)
                {
                    this.HideAllDisplayAreas();
                }
            }
            else if (displayMode == ConsoleDisplayMode.ConsoleMode)
            {
                if (CommonsConfig.Instance.EnableCameraAssignMode)
                {
                    this.ShowGridDisplayAreas();
                }
            }
        }

        // For Player Only!!

        /// <summary>
        /// clear grid.
        /// </summary>
        public void ClearGrid()
        {
            this.gridOverlayDisplayAreaList.Clear();
        }
       
        /// <summary>
        /// get grid overlay display area.
        /// </summary>
        /// <param name="index">
        /// The index.
        /// </param>
        /// <returns>
        /// index에 대한 overlaydisplayArea.
        /// </returns>
        public OverlayDisplayArea GetGridOverlayDisplayArea(int index)
        {
            return this.gridOverlayDisplayAreaList[index];
        }

        /// <summary>
        /// DeserializeMultiGridTotalElementForSync
        /// </summary>
        /// <param name="data">MultiGridTotalElementSync</param>
        public void RecvSyncMultiGridStructureChanged(MultiGridTotalElementSync data)
        {
            foreach (OverlayDisplayArea overlayDisplayArea in this.gridOverlayDisplayAreaList)
            {
               
            }

            // data.Clear();
        }

        /// <summary>
        /// FullScreenTotalOverlayMultiGridControlForSync
        /// </summary>
        /// <param name="data">MultiGridFullScreenDataForSync</param>
        public void RecvSyncMultiGridFullScreenChanged(MultiGridFullScreenSync data)
        {
            foreach (OverlayDisplayArea overlayDisplayArea in this.gridOverlayDisplayAreaList)
            {
                
            }
        }

        /// <summary>
        /// CellSlideTotalOverlayMultiGridControlForSync
        /// </summary>
        /// <param name="data">MultiGridCellDataForSync</param>
        public void RecvSyncMultiGridCellSlideChanged(MultiGridCellSlideChangedSync data)
        {
            foreach (OverlayDisplayArea overlayDisplayArea in this.gridOverlayDisplayAreaList)
            {
               
            }
        }


        /// <summary>
        /// hide all display areas.
        /// </summary>
        public void HideAllDisplayAreas()
        {
            foreach (OverlayDisplayArea displayArea in this.gridOverlayDisplayAreaList)
            {
                //displayArea.UI.Visibility = Visibility.Hidden;
                displayArea.Show(false);
            }
        }

        /// <summary>
        /// initialize all overlay display areas.
        /// </summary>
        public void InitializeAllOverlayDisplayAreas()
        {
            var totalList = new List<OverlayDisplayArea>();
            totalList.AddRange(this.gridOverlayDisplayAreaList);

            foreach (OverlayDisplayArea overlayDisplayArea in totalList)
            {
                
            }
        }

        /// <summary>
        /// show grid display areas.
        /// </summary>
        public void ShowGridDisplayAreas()
        {
            foreach (OverlayDisplayArea displayArea in this.gridOverlayDisplayAreaList)
            {
                //displayArea.UI.Visibility = Visibility.Visible;
                displayArea.Show(true);
            }
        }

        /// <summary>
        /// show grid layout.
        /// </summary>
        /// <param name="isShow">
        /// The is show.
        /// </param>
        public void ShowGridLayout(bool isShow)
        {
            foreach (OverlayDisplayArea displayArea in this.gridOverlayDisplayAreaList)
            {
                if (displayArea == null)
                {
                    continue;
                }

                // Warning - blackRoot : displayArea.UI의 Visible 속성을 Collapsed로 하면 Grid의 Width가 0이 됨
                // Camera의 Width가 Grid의 Width와 연계되어서 0이 됨 ==> 간단히 말해서 Camera Size가 (0,0)이 되어서 안나옴 !!!
                if (isShow)
                {
                    //displayArea.UI.Visibility = Visibility.Visible;
                    displayArea.Show(true);
                }
                else
                {
                    // displayArea.UI.Visibility = Visibility.Collapsed;
                    //displayArea.UI.Visibility = Visibility.Hidden;
                    displayArea.Show(false);
                }
                
                // TODO : RedrawBaseControl 삭제 작업 (by jhlee)
                //displayArea.RedrawBaseControlAsync();
            }
        }

        #endregion
    }
}