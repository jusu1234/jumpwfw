// --------------------------------------------------------------------------------------------------------------------
// <copyright file="OverlayDisplayArea.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   overlay display area.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.PlayerCommons.DisplayAreas
{
    using System;
    using System.Windows;
    using System.Windows.Controls;
    using Commons.Publics;
    using DLLs.AlarmBorderControls;
    
    /// <summary>
    /// overlay display area.
    /// </summary>
    public class OverlayDisplayArea
    {
        #region Constants and Fields

        private Canvas _canvas = new Canvas();

        //현재 CM쪽 Stage의 AlarmBorder는 사용하지 않음 !! Contents Mode DisplayArea쪽 AlarmBorder만 사용함 !!
        //DP는 Contents Mode, Stage Mode 양쪽 다 AlarmBorder를 사용함 !!
        private AlarmBorderControl _alarmBorderControl = new AlarmBorderControl();

        private string _displayAreaName = string.Empty;
        
        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// make alarm border control.
        /// </summary>
        public void MakeAlarmBorderControl()
        {
            Canvas.SetLeft(this._alarmBorderControl, 0);
            Canvas.SetTop(this._alarmBorderControl, 0);
            this._alarmBorderControl.Width = this._canvas.Width;
            this._alarmBorderControl.Height = this._canvas.Height;
            
            if (!this._canvas.Children.Contains(this._alarmBorderControl))
                this._canvas.Children.Add(this._alarmBorderControl);

            Canvas.SetZIndex(this._alarmBorderControl, 999);
            this._alarmBorderControl.Visibility = Visibility.Hidden;
        }

        private void RefreshAlarmBorderControlSize()
        {
            if (!double.IsNaN(this._canvas.Width) && !double.IsNaN(this._canvas.Height))
            {
                this._alarmBorderControl.Width = this._canvas.Width;
                this._alarmBorderControl.Height = this._canvas.Height;
            }
            else
            {
                this._alarmBorderControl.Width = this._canvas.ActualWidth;
                this._alarmBorderControl.Height = this._canvas.ActualHeight;
            }
        }
        
        /// <summary>
        /// show alarm border control.
        /// </summary>
        /// <param name="borderAction">
        /// The border Action.
        /// </param>
        /// <param name="borderColor">
        /// The border Color.
        /// </param>
        public void ShowAlarmBorderControl(BorderActionType borderAction, string borderColor)
        {
            var action = new Action(() =>
            {
                switch (borderAction)
                {
                    case BorderActionType.Enable:
                        this.RefreshAlarmBorderControlSize();
                        this._alarmBorderControl.BorderAction = AlarmBorderControl.BorderActionType.Enable;
                        break;
                    case BorderActionType.Disable:
                        this._alarmBorderControl.Width = 0;
                        this._alarmBorderControl.Height = 0;
                        this._alarmBorderControl.BorderAction = AlarmBorderControl.BorderActionType.Disable;
                        break;
                    case BorderActionType.Blink:
                        this.RefreshAlarmBorderControlSize();
                        this._alarmBorderControl.BorderAction = AlarmBorderControl.BorderActionType.Blink;
                        break;
                }

                this._alarmBorderControl.BorderColor = borderColor;
            });

            if (this._alarmBorderControl.Dispatcher.CheckAccess())
            {
                action.Invoke();
            }
            else
            {
                this._alarmBorderControl.Dispatcher.BeginInvoke(action);
            }
        }

        #endregion

        #region Properties

        public Rect ViewRect
        {
            get
            {
                try
                {
                    if (PresentationSource.FromVisual(this._canvas) != null)
                    {
                        
                    }

                    return Rect.Empty;
                }
                catch (Exception)
                {
                    return Rect.Empty;
                }
            }
        }

        /// <summary>
        /// Gets DisplayAreaName.
        /// </summary>
        public string DisplayAreaName
        {
            get
            {
                return this._displayAreaName;
            }
        }
        
        /// <summary>
        /// Gets UI.
        /// </summary>
        public Canvas UI
        {
            get
            {
                return this._canvas;
            }
        }

        #endregion

        #region Methods

        private void _canvas_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            
        }

        public void Show(bool isShow)
        {
            if (isShow)
            {
                if (this.UI != null)
                    this.UI.Visibility = Visibility.Visible;

            }
            else
            {
                if (this.UI != null)
                    this.UI.Visibility = Visibility.Hidden;

            }
        }

        #endregion
    }
}