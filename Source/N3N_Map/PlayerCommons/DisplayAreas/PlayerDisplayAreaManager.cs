// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PlayerDisplayAreaManager.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   player display area manager.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.PlayerCommons.DisplayAreas
{
    using System;
    using System.Collections.Generic;
    using System.Windows;

    using Innotive.InnoWatch.Commons.DisplayAreas;
    using Innotive.InnoWatch.Commons.Files;

    /// <summary>
    /// player display area manager.
    /// </summary>
    public class PlayerDisplayAreaManager : IDisplayAreaManager
    {
        public event EventHandler eChangedList;

        /// <summary>
        /// Initializes a new instance of the <see cref="PlayerDisplayAreaManager"/> class.
        /// </summary>
        public PlayerDisplayAreaManager()
        {
            this.List = new List<BaseDisplayArea>();
        }

        /// <summary>
        /// Gets or sets List.
        /// </summary>
        public List<BaseDisplayArea> List { get; set; }

        /// <summary>
        /// Gets Count.
        /// </summary>
        public int Count
        {
            get
            {
                return this.List.Count;
            }

            set
            {
            }
        }

        /// <summary>
        /// add display area list.
        /// </summary>
        /// <param name="areaList">
        /// The area list.
        /// </param>
        public void AddDisplayAreaList(List<PlayerDisplayArea> areaList)
        {
            foreach (PlayerDisplayArea area in areaList)
            {
                int index = this.GetIndexOfClonedDisplayArea(area);
                if (index > -1)
                {
                    this.List.Insert(index + 1, area);
                }
                else
                {
                    this.List.Add(area);
                }
            }
        }

        /// <summary>
        /// clear.
        /// </summary>
        public void Clear()
        {
            this.List.Clear();
        }

        /// <summary>
        /// 이름으로 displayArea 인스턴스를 가져온다.
        /// </summary>
        /// <param name="displayAreaName">
        /// string.
        /// </param>
        /// <returns>
        /// displayArea.
        /// </returns>
        public PlayerDisplayArea GetDisplayArea(string displayAreaName)
        {
            foreach (PlayerDisplayArea displayArea in this.List)
            {
                if (string.Compare(displayArea.Name, displayAreaName, true) == 0)
                {
                    return displayArea;
                }
            }

            return null;
        }

        /// <summary>
        /// get display area.
        /// </summary>
        /// <param name="index">
        /// The index.
        /// </param>
        /// <returns>
        /// </returns>
        public BaseDisplayArea GetDisplayArea(int index)
        {
            if (index > this.List.Count)
            {
                return null;
            }

            return this.List[index];
        }

        /// <summary>
        /// get display area z index.
        /// </summary>
        /// <param name="area">
        /// The area.
        /// </param>
        /// <returns>
        /// The get display area z index.
        /// </returns>
        public int GetDisplayAreaZIndex(PlayerDisplayArea area)
        {
            return this.List.IndexOf(area);
        }

        /// <summary>
        /// hide all display areas.
        /// </summary>
        public void HideAllDisplayAreas()
        {
            foreach (PlayerDisplayArea displayArea in this.List)
            {
                displayArea.DisplayCanvas.Visibility = Visibility.Hidden;

                if (displayArea.CurrentLayout != null)
                {
                    displayArea.CurrentLayout.Visibility = Visibility.Hidden;
                }

                // TODO : RedrawBaseControl 삭제 작업 (by jhlee)
                // (displayArea.CurrentLayout as LayoutControl).RedrawBaseControlAsync();
            }
        }

        /// <summary>
        /// 파일을 읽어들여서 List를 생성한다.
        /// </summary>
        /// <param name="fileName">
        /// </param>
        public void Load(InnoFilePath fileName)
        {
            var displayAreaFile = BaseFile.LoadBaseFile<DisplayAreaFile>(fileName);

            this.List.Clear();
            for (int index = 0; index < displayAreaFile.Count; index++)
            {
                // layoutControlList.Add(displayAreaFile.GetDisplayArea(index));
                BaseDisplayArea displayArea = displayAreaFile.DisplayAreaList[index];
                this.List.Add(new PlayerDisplayArea(displayArea));
            }
        }

        /// <summary>
        /// set display area z index.
        /// </summary>
        /// <param name="area">
        /// The area.
        /// </param>
        /// <param name="index">
        /// The index.
        /// </param>
        public void SetDisplayAreaZIndex(PlayerDisplayArea area, int index)
        {
            int oldIndex = this.List.IndexOf(area);
            if (oldIndex < 0)
            {
                return;
            }

            this.List.RemoveAt(oldIndex);

            if (index > oldIndex)
            {
                --index;
            }

            this.List.Insert(index + 1, area);
        }

        /// <summary>
        /// show all display areas.
        /// </summary>
        public void ShowAllDisplayAreas()
        {
            foreach (PlayerDisplayArea displayArea in this.List)
            {
                displayArea.DisplayCanvas.Visibility = Visibility.Visible;

                if (displayArea.CurrentLayout != null)
                {
                    displayArea.CurrentLayout.Visibility = Visibility.Visible;
                }

                // TODO : RedrawBaseControl 삭제 작업 (by jhlee)
                // (displayArea.CurrentLayout as LayoutControl).RedrawBaseControlAsync();
            }
        }

        private int GetIndexOfClonedDisplayArea(PlayerDisplayArea area)
        {
            // BinarySearch로 바꿀까?...에이 귀찮타...
            for (int i = 0; i < this.List.Count; ++i)
            {
                if (((PlayerDisplayArea)this.List[i]).IsClone(area))
                {
                    return i;
                }
            }

            return -1;
        }
    }
}