﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Innotive.InnoWatch.PlayerCommons
{
    public class UserPrivilege
    {
        public void Initialize(string sync, string rdsControl, string ptzControl, string favoriteSave, string resourceCamera, string resourceLayout, string resourceMap, string resourceImage)
        {
            if (string.Compare(sync, "true", true) == 0)
            {
                this.Sync = true;
            }
            else
            {
                this.Sync = false;
            }

            if (string.Compare(rdsControl, "true", true) == 0)
            {
                this.RdsControl = true;
            }
            else
            {
                this.RdsControl = false;
            }

            if (string.Compare(ptzControl, "true", true) == 0)
            {
                this.PtzControl = true;
            }
            else
            {
                this.PtzControl = false;
            }

            if (string.Compare(favoriteSave, "true", true) == 0)
            {
                this.FavoriteSave = true;
            }
            else
            {
                this.FavoriteSave = false;
            }

            if (string.Compare(resourceCamera, "true", true) == 0)
            {
                this.ResourceCamera = true;
            }
            else
            {
                this.ResourceCamera = false;
            }

            if (string.Compare(resourceLayout, "true", true) == 0)
            {
                this.ResourceLayout = true;
            }
            else
            {
                this.ResourceLayout = false;
            }

            if (string.Compare(resourceMap, "true", true) == 0)
            {
                this.ResourceMap = true;
            }
            else
            {
                this.ResourceMap = false;
            }

            if (string.Compare(resourceImage, "true", true) == 0)
            {
                this.ResourceImage = true;
            }
            else
            {
                this.ResourceImage = false;
            }
        }

        public bool Sync { get; set; }
        public bool RdsControl { get; set; }
        public bool PtzControl { get; set; }
        public bool FavoriteSave { get; set; }
        public bool ResourceCamera { get; set; }
        public bool ResourceLayout { get; set; }
        public bool ResourceMap { get; set; }
        public bool ResourceImage { get; set; }
    }
}
