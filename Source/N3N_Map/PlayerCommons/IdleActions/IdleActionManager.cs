﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Threading;
using Innotive.InnoWatch.Commons.Utils;
using Innotive.InnoWatch.PlayerCommons;
using Innotive.InnoWatch.PlayerCommons.DisplayAreas;
using Innotive.InnoWatch.CommonUtils.Log;
using System.Windows;

namespace Innotive.InnoWatch.PlayerCommons.IdleActions
{
    public class IdleActionManager
    {
        //Animation이 동작할때 PreviewMouseMove가 계속 발생하는 문제가 있음.
        //마지막 MousePoint를 저장해뒀다가 MousePoint가 변경됐을때 IdleTimer를 동작하도록 하기 위함.
        public Point LastMouseMovePoint = new Point(-10000, -10000);
        public bool IsFirstMouseMove
        {
            get
            {
                if (this.LastMouseMovePoint.X == -10000 && this.LastMouseMovePoint.Y == -10000)
                    return true;

                return false;
            }
        }

        public IdleActionManager()
        {
        }

        //사용자의 마우스 움직임이 있는 경우 전체 Timer를 새로 시작함 !!
        public void RestartTimer()
        {
            for (int index = 0; index < MainProcess.Instance.ProjectManager.DisplayAreaManager.Count; index++)
            {
                PlayerDisplayArea displayArea = MainProcess.Instance.ProjectManager.DisplayAreaManager.GetDisplayArea(index) as PlayerDisplayArea;
                if (displayArea != null)
                    displayArea.RestartTimer();
            }
        }

        public void StopTimer()
        {
            for (int index = 0; index < MainProcess.Instance.ProjectManager.DisplayAreaManager.Count; index++)
            {
                PlayerDisplayArea displayArea = MainProcess.Instance.ProjectManager.DisplayAreaManager.GetDisplayArea(index) as PlayerDisplayArea;
                if (displayArea != null)
                    displayArea.StopTimer();
            }
        }
    }
}
