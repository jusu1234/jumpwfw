// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PlayerZoomOutActionProcess.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   player zoom out action process.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.PlayerCommons.ActionProceses
{
    using Innotive.InnoWatch.Commons.Actions;
    using Innotive.InnoWatch.CommonUtils.Log;

    /// <summary>
    /// player zoom out action process.
    /// </summary>
    public class PlayerZoomOutActionProcess : PlayerActionProcess
    {
        #region Constants and Fields

        private DataZoomOutActionProcessItem _item;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="PlayerZoomOutActionProcess"/> class.
        /// </summary>
        /// <param name="item">
        /// The item.
        /// </param>
        public PlayerZoomOutActionProcess(DataZoomOutActionProcessItem item)
        {
            this._startTime = item.StartTime;
            this._item = item;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// execute.
        /// </summary>
        public override void Execute()
        {
            base.Execute();
        }

        #endregion

        #region Methods

        /// <summary>
        /// do dispose.
        /// </summary>
        /// <param name="isManage">
        /// The is manage.
        /// </param>
        protected override void DoDispose(bool isManage)
        {
            if (isManage)
            {
            }

            base.DoDispose(isManage);
        }

        protected override void Execute_Internal()
        {
            InnotiveDebug.Trace("[Action] PlayerZoomOutActionProcess Excute() Name : {0} , GUID : {1}", this._item.Name, this._item.SyncGUID);

            MainProcess.Instance.ProjectManager.LayoutContentsSyncManager.ZoomOut(
                this._item.LayoutGuid, this._item.ZoomOutPercentage, this._item.Duration);
        }

        #endregion
    }
}