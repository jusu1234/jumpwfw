// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PlayerBringToFrontActionProcess.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   player bring to front action process.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.PlayerCommons.ActionProceses
{
    using Innotive.InnoWatch.Commons.Actions;
    using Innotive.InnoWatch.CommonUtils.Log;

    /// <summary>
    /// player bring to front action process.
    /// </summary>
    public class PlayerBringToFrontActionProcess : PlayerActionProcess
    {
        #region Constants and Fields

        private DataBringToFrontActionProcessItem _item;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="PlayerBringToFrontActionProcess"/> class.
        /// </summary>
        /// <param name="item">
        /// The item.
        /// </param>
        public PlayerBringToFrontActionProcess(DataBringToFrontActionProcessItem item)
        {
            this._startTime = item.StartTime;
            this._item = item;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// execute.
        /// </summary>
        public override void Execute()
        {
            base.Execute();
        }

        #endregion

        #region Methods

        /// <summary>
        /// do dispose.
        /// </summary>
        /// <param name="isManage">
        /// The is manage.
        /// </param>
        protected override void DoDispose(bool isManage)
        {
            if (isManage)
            {
            }

            base.DoDispose(isManage);
        }

        protected override void Execute_Internal()
        {
            InnotiveDebug.Trace("[Action] PlayerBringToFrontActionProcess Excute() Name : {0} , GUID : {1}", this._item.Name, this._item.SyncGUID);
   
            MainProcess.Instance.ProjectManager.LayoutContentsSyncManager.BringToFront(
                this._item.TargetElementGuid, this._item.ActionEffect, this._item.StartTime, this._item.Duration);
        }

        #endregion
    }
}