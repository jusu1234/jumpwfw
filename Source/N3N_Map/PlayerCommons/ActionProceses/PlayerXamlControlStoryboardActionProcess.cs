// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PlayerXamlControlStoryboardActionProcess.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   player console action process.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.PlayerCommons.ActionProceses
{
    using Innotive.InnoWatch.Commons.Actions;
    using Innotive.InnoWatch.CommonUtils.Log;

    /// <summary>
    /// Player Xaml Control Storyboard Action Process.
    /// </summary>
    public class PlayerXamlControlStoryboardActionProcess : PlayerActionProcess
    {
        #region Constants and Fields

        private DataXamlControlStoryboardActionProcessItem _item;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="PlayerXamlControlStoryboardActionProcess"/> class.
        /// </summary>
        /// <param name="item">
        /// The item.
        /// </param>
        public PlayerXamlControlStoryboardActionProcess(DataXamlControlStoryboardActionProcessItem item)
        {
            this._startTime = item.StartTime;
            this._item = item;
        }

        #endregion
        
        #region Public Methods

        /// <summary>
        /// Excute Action Process.
        /// </summary>
        public override void Execute()
        {
            base.Execute();
        }

        #endregion

        #region Methods

        /// <summary>
        /// do dispose.
        /// </summary>
        /// <param name="isManage">
        /// The is manage.
        /// </param>
        protected override void DoDispose(bool isManage)
        {
            if (isManage)
            {
            }
            
            base.DoDispose(isManage);
        }

        protected override void Execute_Internal()
        {
            InnotiveDebug.Trace("[Action] PlayerXamlControlStoryboardActionProcess Excute() Name : {0} , GUID : {1}", this._item.Name, this._item.SyncGUID);

            MainProcess.Instance.ProjectManager.LayoutContentsSyncManager.XamlControlStoryboard(this._item.TargetLayoutGuid, this._item.TargetElementGuid, this._item.TargetElementStoryboardName);
        }

        #endregion
       
    }
}