// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PlayerOpenLayoutActionProcess.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   player open layout action process.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.PlayerCommons.ActionProceses
{
    using System.Windows.Input;
    using Innotive.InnoWatch.Commons.Actions;
    using Innotive.InnoWatch.Commons.Configs;
    using Innotive.InnoWatch.CommonUtils.Log;

    /// <summary>
    /// player open layout action process.
    /// </summary>
    public class PlayerOpenLayoutActionProcess : PlayerActionProcess
    {
        #region Constants and Fields

        private readonly DataOpenLayoutActionProcessItem item;

        #endregion

        #region Events

        /// <summary>
        /// open layout event.
        /// </summary>
        public event OpenLayoutActionEventHandler OnOpenLayoutActionEvent;

        #endregion


        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="PlayerOpenLayoutActionProcess"/> class.
        /// </summary>
        /// <param name="item">
        /// The item.
        /// </param>
        public PlayerOpenLayoutActionProcess(DataOpenLayoutActionProcessItem item)
        {
            this._startTime = item.StartTime;
            this.item = item;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// DisplayAreaManager에게 OpenLayout의 실행을 명령한다.
        /// Execute: 1. 20081202.
        /// </summary>
        public override void Execute()
        {
            base.Execute();
        }

        #endregion

        #region Methods

        /// <summary>
        /// do dispose.
        /// </summary>
        /// <param name="isManage">
        /// The is manage.
        /// </param>
        protected override void DoDispose(bool isManage)
        {
            if (isManage)
            {
            }

            this.OnOpenLayoutActionEvent = null;

            base.DoDispose(isManage);
        }

        protected override void Execute_Internal()
        {
            InnotiveDebug.Trace("[Action] PlayerOpenLayoutActionProcess Excute() Name : {0} , GUID : {1}", this.item.Name, this.item.SyncGUID);

            if (CommonsConfig.Instance.IsShowMouseCursor)
            {
                Mouse.OverrideCursor = Cursors.Wait;
            }

            MainProcess.Instance.ProjectManager.DisplayAreaOpenChangeLayoutSyncManager.OpenLayout(
                this.item.TargetDisplayArea,
                this.item.LayoutGuid,
                this.item.ActionEffect,
                this.item.StartTime,
                this.item.Duration,
                this.item.SyncGUID);

            Mouse.OverrideCursor = null;

            //event를 발생시킴 !!
            //console stage에서 event를 받아서 처리함 !!
            if (this.OnOpenLayoutActionEvent != null)
            {
                this.OnOpenLayoutActionEvent(this, new OpenLayoutActionEventArgs(this.item.TargetDisplayArea, this.item.LayoutGuid, this.item.SyncGUID));
            }
        }

        #endregion
    }
}