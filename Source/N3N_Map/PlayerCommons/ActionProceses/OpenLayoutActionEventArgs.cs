﻿
namespace Innotive.InnoWatch.PlayerCommons.ActionProceses
{
    using System;

    public delegate void OpenLayoutActionEventHandler(object sender, OpenLayoutActionEventArgs e);

    public class OpenLayoutActionEventArgs : EventArgs
    {
        public string DisplayAreaName = string.Empty;
        public Guid NewLayoutGuid = Guid.Empty;
        public Guid LoadedActionGuid = Guid.Empty;

        public OpenLayoutActionEventArgs(string displayAreaName, Guid newLayoutGuid, Guid loadedActionGuid)
        {
            DisplayAreaName = displayAreaName;
            NewLayoutGuid = newLayoutGuid;
            LoadedActionGuid = loadedActionGuid;
        }
    }
}
