﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PlayerGoToObjectActionProcess.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   player go to object action process.
// </summary>
// --------------------------------------------------------------------------------------------------------------------


namespace Innotive.InnoWatch.PlayerCommons.ActionProceses
{
    using System.Windows.Media.Animation;
    using Innotive.InnoWatch.Commons.Actions;
    using Innotive.InnoWatch.CommonUtils.Log;

    /// <summary>
    /// player go to object action process.
    /// </summary>
    public class PlayerGoToObjectActionProcess : PlayerActionProcess
    {
        #region Constants and Fields

        private DataGoToObjectActionProcessItem _item;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="PlayerGoToObjectActionProcess"/> class.
        /// </summary>
        /// <param name="item">
        /// The item.
        /// </param>
        public PlayerGoToObjectActionProcess(DataGoToObjectActionProcessItem item)
        {
            this._startTime = item.StartTime;
            this._item = item;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// execute.
        /// </summary>
        public override void Execute()
        {
            base.Execute();
        }

        #endregion

        #region Methods

        /// <summary>
        /// do dispose.
        /// </summary>
        /// <param name="isManage">
        /// The is manage.
        /// </param>
        protected override void DoDispose(bool isManage)
        {
            if (isManage)
            {
            }

            base.DoDispose(isManage);
        }

        protected override void Execute_Internal()
        {
            InnotiveDebug.Trace("[Action] PlayerGoToObjectActionProcess Excute() Element Guid : {0}", this._item.TargetElementGuid);

            //KeySpline keySpline = new KeySpline(this._item.KeySplineX1, this._item.KeySplineY1, this._item.KeySplineX2, this._item.KeySplineY2);
            KeySpline keySpline = new KeySpline(0.4, 1, 1, 1);

            MainProcess.Instance.ProjectManager.LayoutContentsSyncManager.GoToObject(
                this._item.TargetElementGuid,
                this._item.ActionEffect,
                this._item.StartTime,
                this._item.Duration,
                this._item.Margin,
                keySpline);
        }

        #endregion
    }
}