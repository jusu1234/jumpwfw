﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PlayerActionProcess.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   player action process.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.PlayerCommons.ActionProceses
{
    using System;
    using System.Windows.Threading;

    /// <summary>
    /// player action process.
    /// </summary>
    public abstract class PlayerActionProcess : IDisposable
    {
        #region Constants and Fields

        private bool isDisposed = false;

        protected double _startTime = 0;
        private DispatcherTimer _startTimeTimer = new DispatcherTimer();

        #endregion

        #region Constructors and Destructors

        // Dispose 패턴 사용
        /// <summary>
        /// Finalizes an instance of the <see cref="PlayerActionProcess"/> class. 
        /// </summary>
        ~PlayerActionProcess()
        {
            this.Dispose(false);
        }

        #endregion

        #region Events

        /// <summary>
        /// Disposed가 발생할 때 생성되는 이벤트 핸들러.
        /// </summary>
        public event EventHandler eDisposed = null;

        #endregion

        #region Public Methods

        // 실제 Action을 동작시킴 !!
        protected abstract void Execute_Internal();

        /// <summary>
        /// StartTime을 처리해줌 !!
        /// </summary>
        public virtual void Execute()
        {
            // GoToLocationAction과 GoHomeAction은 Storyboard에서 처리 해줌.
            if (this is PlayerGoToLocationActionProcess || this is PlayerGoHomeActionProcess)
            {
                this.Execute_Internal();
            }
            else
            {
                if (this._startTime > 0)
                {
                    this._startTimeTimer.Tick += new EventHandler(this._startTimeTimer_Tick);

                    string startTimeString = this._startTime.ToString();
                    if (startTimeString.Contains("."))
                    {
                        int dotPoint = 0;

                        for (int i = 0; i < startTimeString.Length; i++)
                        {
                            if (string.Equals(startTimeString[i].ToString(), "."))
                            {
                                dotPoint = i;
                            }
                        }

                        int beforeDot = Convert.ToInt32(startTimeString.Substring(0, dotPoint));
                        int afterDot =
                            Convert.ToInt32(startTimeString.Substring(dotPoint + 1, startTimeString.Length - dotPoint - 1));

                        this._startTimeTimer.Interval = new TimeSpan(0, 0, 0, beforeDot, afterDot);
                    }
                    else
                    {
                        this._startTimeTimer.Interval = new TimeSpan(0, 0, (int)this._startTime);
                    }

                    this._startTimeTimer.Start();
                }
                else
                {
                    this._startTimeTimer.Stop();
                    this.Execute_Internal();
                }
            }
        }

        private void _startTimeTimer_Tick(object sender, EventArgs e)
        {
            this._startTimeTimer.Tick -= new EventHandler(this._startTimeTimer_Tick);
            this._startTimeTimer.Stop();

            this.Execute_Internal();
        }

        #endregion

        #region Implemented Interfaces

        #region IDisposable

        /// <summary>
        /// dispose.
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// do dispose.
        /// </summary>
        /// <param name="isManage">
        /// The is manage.
        /// </param>
        protected virtual void DoDispose(bool isManage)
        {
            if (isManage)
            {
                this._startTimeTimer.Stop();
                this._startTimeTimer.Tick -= new EventHandler(this._startTimeTimer_Tick);
                this._startTimeTimer = null;
            }

            if (this.eDisposed != null)
            {
                this.eDisposed(this, new EventArgs());
                this.eDisposed = null;
            }
        }

        private void Dispose(bool isManage)
        {
            if (this.isDisposed)
            {
                return;
            }

            this.isDisposed = true;

            this.DoDispose(isManage);
        }

        #endregion
    }
}