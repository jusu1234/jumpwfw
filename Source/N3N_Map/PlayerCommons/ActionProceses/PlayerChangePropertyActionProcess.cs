// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PlayerChangePropertyActionProcess.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   player change property action process.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.PlayerCommons.ActionProceses
{
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Windows;

    using Innotive.InnoWatch.Commons.Actions;
    using Innotive.InnoWatch.Commons.GuidManagers;
    using Innotive.InnoWatch.CommonUtils.Log;

    /// <summary>
    /// player change property action process.
    /// </summary>
    public class PlayerChangePropertyActionProcess : PlayerActionProcess
    {
        #region Constants and Fields

        private DataChangePropertyActionProcessItem _item;

        // Property List
        private List<PropertyInfo> _list = new List<PropertyInfo>();

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="PlayerChangePropertyActionProcess"/> class.
        /// </summary>
        /// <param name="item">
        /// The item.
        /// </param>
        public PlayerChangePropertyActionProcess(DataChangePropertyActionProcessItem item)
        {
            this._startTime = item.StartTime;

            this._item = item;

            if (this._item.DataPropertySetters != null)
            {
                lock (this._item.DataPropertySetters.SyncRoot)
                {
                    foreach (object propertySetter in this._item.DataPropertySetters)
                    {
                        if (propertySetter is DataPropertySetter)
                        {
                            var info = new PropertyInfo();

                            info.PropertyName = (propertySetter as DataPropertySetter).PropertyName;
                            info.PropertyValue = (propertySetter as DataPropertySetter).PropertyValue;

                            this.AddPropertyInfo(info);
                        }
                    }   
                }
            }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// execute.
        /// </summary>
        public override void Execute()
        {
            base.Execute();
        }

        #endregion

        #region Methods

        /// <summary>
        /// do dispose.
        /// </summary>
        /// <param name="isManage">
        /// The is manage.
        /// </param>
        protected override void DoDispose(bool isManage)
        {
            //base.DoDispose(isManage);

            if (isManage)
            {
                this._list.Clear();
                this._list = null;
            }

            base.DoDispose(isManage);
        }
        
        protected override void Execute_Internal()
        {
            InnotiveDebug.Trace("[Action] PlayerChangePropertyActionProcess Excute_Internal() Name : {0} , GUID : {1}", this._item.Name, this._item.SyncGUID);

            var element = GuidManager.GetSyncObject(this._item.TargetElementGuid) as FrameworkElement;
            if (element == null)
            {
                return;
            }

            foreach (PropertyInfo info in this._list)
            {
                try
                {
                    PropertyDescriptorCollection collection = TypeDescriptor.GetProperties(element);

                    // Class정보를 포함한체로 검색한다.
                    string str = info.PropertyName;
                    var descriptor = collection.Find(str, false);

                    if (descriptor == null)
                    {
                        // Classname + Name으로 Find할 수 없으면 Name만으로 다시 검색한다.
                        int idx = str.LastIndexOf('.');
                        if (idx > -1)
                        {
                            str = str.Substring(idx + 1, str.Length - idx - 1);
                        }

                        descriptor = collection.Find(str, false);
                    }

                    if (descriptor == null)
                    {
                        continue;
                    }

                    object propertyValue = descriptor.Converter.ConvertFromString(info.PropertyValue);

                    DependencyPropertyDescriptor dpDescriptor = DependencyPropertyDescriptor.FromProperty(descriptor);
                    DependencyProperty property = dpDescriptor.DependencyProperty;
                    MainProcess.Instance.ProjectManager.LayoutContentsSyncManager.ChangeElementProperty(
                        this._item.TargetElementGuid,
                        property,
                        propertyValue,
                        this._item.ActionEffect,
                        this._item.StartTime,
                        this._item.Duration
                        );
                }
                catch
                {
                    // 예외 처리 후...
                    throw;
                }
            }
        }

        private void AddPropertyInfo(PropertyInfo info)
        {
            this._list.Add(info);
        }

        #endregion
    }
}