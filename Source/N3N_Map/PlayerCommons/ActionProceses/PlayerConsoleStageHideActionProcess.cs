// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PlayerConsoleActionProcess.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   player console action process.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.PlayerCommons.ActionProceses
{
    using System;

    using Innotive.InnoWatch.Commons.Actions;
    using Innotive.InnoWatch.Commons.Publics;
    using Innotive.InnoWatch.CommonUtils.Log;

    public class ConsoleStageHideActionEventArgs : EventArgs
    {
        public DataConsoleStageHideActionProcessItem Item { get; private set; }

        public ConsoleStageHideActionEventArgs(DataConsoleStageHideActionProcessItem item)
        {
            this.Item = item;
        }
    }

    /// <summary>
    /// player console action process.
    /// </summary>
    public class PlayerConsoleStageHideActionProcess : PlayerActionProcess
    {
        private DataConsoleStageHideActionProcessItem _item;

        /// <summary>
        /// Initializes a new instance of the <see cref="PlayerConsoleActionProcess"/> class.
        /// </summary>
        /// <param name="item">
        /// The item.
        /// </param>
        public PlayerConsoleStageHideActionProcess(DataConsoleStageHideActionProcessItem item)
        {
            this._startTime = item.StartTime;
            this._item = item;
        }

        public event EventHandler<ConsoleStageHideActionEventArgs> OnExecuteStageHideAction;

        /// <summary>
        /// execute.
        /// </summary>
        public override void Execute()
        {
            base.Execute();
        }

        /// <summary>
        /// do dispose.
        /// </summary>
        /// <param name="isManage">
        /// The is manage.
        /// </param>
        protected override void DoDispose(bool isManage)
        {
            if (isManage)
            {
            }

            this.OnExecuteStageHideAction = null;

            base.DoDispose(isManage);
        }

        protected override void Execute_Internal()
        {
            InnotiveDebug.Trace("[Action] PlayerConsoleStageHideActionProcess Excute() Name : {0} , GUID : {1}", this._item.Name, this._item.SyncGUID);

            if (this.OnExecuteStageHideAction != null)
            {
                this.OnExecuteStageHideAction(this, new ConsoleStageHideActionEventArgs(this._item));
            }
        }
    }
}