// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PlayerExternalCommandActionProcess.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Player External Command Action Process.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Linq;
using Innotive.InnoWatch.Commons.ExternalCommandInfo;

namespace Innotive.InnoWatch.PlayerCommons.ActionProceses
{
    using Innotive.InnoWatch.Commons.Actions;
    using Innotive.InnoWatch.Commons.Utils;
    using Innotive.InnoWatch.CommonUtils.Log;
    using Innotive.InnoWatch.Commons.Utils.Http;
    using Innotive.InnoWatch.PlayerCommons.Configs;

    /// <summary>
    /// Player External Command Action Process.
    /// </summary>
    public class PlayerExternalCommandActionProcess : PlayerActionProcess
    {
        #region Constants and Fields

        private DataExternalCommandActionProcessItem _item;

        private readonly string externalDeviceServicePath = "/rest/externalcommand/execute";

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="PlayerXamlControlStoryboardActionProcess"/> class.
        /// </summary>
        /// <param name="item">
        /// The item.
        /// </param>
        public PlayerExternalCommandActionProcess(DataExternalCommandActionProcessItem item)
        {
            this._startTime = item.StartTime;
            this._item = item;
        }

        #endregion
        
        #region Public Methods

        /// <summary>
        /// Excute Action Process.
        /// </summary>
        public override void Execute()
        {
            base.Execute();
        }

        #endregion

        #region Methods

        /// <summary>
        /// do dispose.
        /// </summary>
        /// <param name="isManage">
        /// The is manage.
        /// </param>
        protected override void DoDispose(bool isManage)
        {
            if (isManage)
            {
            }

            base.DoDispose(isManage);
        }

        protected override void Execute_Internal()
        {
            InnotiveDebug.Trace("[Action] PlayerExternalCommandActionProcess Excute() Module : {0} , CommandClass : {1}", this._item.ExternalCommandModule, this._item.CommandClass);

            if(ExternalCommandExecution.Instance.ExternalCommandInfo.ServerInfos.Count == 0)
            {
                InnotiveDebug.Trace("[Execute_Internal Error]  ExternalCommandServiceUrl Empty. PlayerExternalCommandAction not Execute. ");
                return;
            }

            if(string.IsNullOrWhiteSpace(this._item.RequestExternalCommandID))
            {
                InnotiveDebug.Trace("[Execute_Internal Error]  Request Url is Empty. PlayerExternalCommandAction not Execute. ");
                return;
            }

            var serverInfo = 
                ExternalCommandExecution.Instance.ExternalCommandInfo.ServerInfos.FirstOrDefault(
                    item => string.CompareOrdinal(item.Id, this._item.RequestExternalCommandID) == 0);

            if(serverInfo == null)
            {
                InnotiveDebug.Trace("[Execute_Internal Error]  There is no matched request server url. PlayerExternalCommandAction not Execute. ");
                return;
            }
            
            var url = string.Format("{0}/{1}", serverInfo.ServiceUrl, this.externalDeviceServicePath);
            
            // iHUB에 REST로 외부장비 연동 요청.
            HttpRequest.RequestForRest(url, RequestMethod.Post, this._item.Data);
            
            System.Diagnostics.Debug.WriteLine("PlayerExternalCommandActionProcess::Excute : " + this._item.ExternalCommandModule + ", " + this._item.CommandClass + " Success!");
        }

        #endregion
       
    }
}