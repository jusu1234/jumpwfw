// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PlayerConsoleActionProcess.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   player console action process.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.PlayerCommons.ActionProceses
{
    using System;

    using Innotive.InnoWatch.Commons.Actions;
    using Innotive.InnoWatch.Commons.Publics;
    using Innotive.InnoWatch.CommonUtils.Log;

    /// <summary>
    /// player console action process.
    /// </summary>
    public class PlayerConsoleActionProcess : PlayerActionProcess
    {
        private DataConsoleActionProcessItem _item;

        /// <summary>
        /// Initializes a new instance of the <see cref="PlayerConsoleActionProcess"/> class.
        /// </summary>
        /// <param name="item">
        /// The item.
        /// </param>
        public PlayerConsoleActionProcess(DataConsoleActionProcessItem item)
        {
            this._startTime = item.StartTime;
            this._item = item;
        }

        /// <summary>
        /// on change camera mode event.
        /// </summary>
        public event EventHandler OnChangeCameraModeEvent;

        /// <summary>
        /// Gets or sets ConsoleActionType.
        /// </summary>
        public ConsoleActionType ConsoleActionType { get; set; }

        /// <summary>
        /// execute.
        /// </summary>
        public override void Execute()
        {
            base.Execute();
        }

        /// <summary>
        /// do dispose.
        /// </summary>
        /// <param name="isManage">
        /// The is manage.
        /// </param>
        protected override void DoDispose(bool isManage)
        {
            if (isManage)
            {
            }

            this.OnChangeCameraModeEvent = null;

            base.DoDispose(isManage);
        }

        protected override void Execute_Internal()
        {
            InnotiveDebug.Trace("[Action] PlayerConsoleActionProcess Excute() Name : {0} , GUID : {1}", this._item.Name, this._item.SyncGUID);

            switch (this.ConsoleActionType)
            {
                case ConsoleActionType.AccessiControl:
                    break;
                case ConsoleActionType.ChangeCameraMode:
                    this.ChangeCameraModeAction();
                    break;
                default:
                    break;
            }
        }
        
        private void ChangeCameraModeAction()
        {
            if (this.OnChangeCameraModeEvent != null)
            {
                this.OnChangeCameraModeEvent(this, new EventArgs());
            }
        }
    }
}