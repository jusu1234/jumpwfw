// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PlayerGoHomeActionProcess.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   player go home action process.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.PlayerCommons.ActionProceses
{
    using System;
    using Innotive.InnoWatch.Commons.Actions;
    using Innotive.InnoWatch.CommonUtils.Log;

    /// <summary>
    /// player go home action process.
    /// </summary>
    public class PlayerGoHomeActionProcess : PlayerActionProcess
    {
        #region Constants and Fields

        private DataGoHomeActionProcessItem _item;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="PlayerGoHomeActionProcess"/> class.
        /// </summary>
        /// <param name="item">
        /// The item.
        /// </param>
        public PlayerGoHomeActionProcess(DataGoHomeActionProcessItem item)
        {
            this._startTime = item.StartTime;
            this._item = item;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets LayoutGuid.
        /// </summary>
        public Guid LayoutGuid { get; set; }

        #endregion

        #region Public Methods

        /// <summary>
        /// execute.
        /// </summary>
        public override void Execute()
        {
            base.Execute();
        }

        #endregion

        #region Methods

        /// <summary>
        /// do dispose.
        /// </summary>
        /// <param name="isManage">
        /// The is manage.
        /// </param>
        protected override void DoDispose(bool isManage)
        {
            if (isManage)
            {
            }

            base.DoDispose(isManage);
        }

        protected override void Execute_Internal()
        {
            InnotiveDebug.Trace("[Action] PlayerGoHomeActionProcess Excute() Name : {0} , GUID : {1}", this._item.Name, this._item.SyncGUID);

            MainProcess.Instance.ProjectManager.PlayerLayoutManager.DoGoHomeActionByTargetLayoutGuid(this.LayoutGuid, _item.ActionEffect, _item.StartTime, _item.Duration);
        }

        #endregion
    }
}