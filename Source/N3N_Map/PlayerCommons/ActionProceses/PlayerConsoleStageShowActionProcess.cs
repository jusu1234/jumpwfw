// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PlayerConsoleActionProcess.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   player console action process.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.PlayerCommons.ActionProceses
{
    using System;

    using Innotive.InnoWatch.Commons.Actions;
    using Innotive.InnoWatch.Commons.Publics;
    using Innotive.InnoWatch.CommonUtils.Log;

    public class ConsoleStageShowActionEventArgs : EventArgs
    {
        public DataConsoleStageShowActionProcessItem Item { get; private set; }

        public ConsoleStageShowActionEventArgs(DataConsoleStageShowActionProcessItem item)
        {
            this.Item = item;
        }
    }

    /// <summary>
    /// player console action process.
    /// </summary>
    public class PlayerConsoleStageShowActionProcess : PlayerActionProcess
    {
        private DataConsoleStageShowActionProcessItem _item;

        /// <summary>
        /// Initializes a new instance of the <see cref="PlayerConsoleActionProcess"/> class.
        /// </summary>
        /// <param name="item">
        /// The item.
        /// </param>
        public PlayerConsoleStageShowActionProcess(DataConsoleStageShowActionProcessItem item)
        {
            this._startTime = item.StartTime;
            this._item = item;
        }

        public event EventHandler<ConsoleStageShowActionEventArgs> OnExecuteStageShowAction;

        /// <summary>
        /// execute.
        /// </summary>
        public override void Execute()
        {
            base.Execute();
        }

        /// <summary>
        /// do dispose.
        /// </summary>
        /// <param name="isManage">
        /// The is manage.
        /// </param>
        protected override void DoDispose(bool isManage)
        {
            if (isManage)
            {
            }

            this.OnExecuteStageShowAction = null;

            base.DoDispose(isManage);
        }

        protected override void Execute_Internal()
        {
            InnotiveDebug.Trace("[Action] PlayerConsoleStageShowActionProcess Excute() Name : {0} , GUID : {1}", this._item.Name, this._item.SyncGUID);

            if (this.OnExecuteStageShowAction != null)
            {
                this.OnExecuteStageShowAction(this, new ConsoleStageShowActionEventArgs(this._item));
            }
        }
    }
}