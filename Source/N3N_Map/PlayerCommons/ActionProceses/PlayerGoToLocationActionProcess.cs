// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PlayerGoToLocationActionProcess.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   player go to location action process.
// </summary>
// --------------------------------------------------------------------------------------------------------------------


namespace Innotive.InnoWatch.PlayerCommons.ActionProceses
{
    using System.Windows.Media.Animation;
    using Innotive.InnoWatch.Commons.Actions;
    using Innotive.InnoWatch.CommonUtils.Log;

    /// <summary>
    /// player go to location action process.
    /// </summary>
    public class PlayerGoToLocationActionProcess : PlayerActionProcess
    {
        #region Constants and Fields

        private DataGoToLocationActionProcessItem _item;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="PlayerGoToLocationActionProcess"/> class.
        /// </summary>
        /// <param name="item">
        /// The item.
        /// </param>
        public PlayerGoToLocationActionProcess(DataGoToLocationActionProcessItem item)
        {
            this._startTime = item.StartTime;
            this._item = item;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// execute.
        /// </summary>
        public override void Execute()
        {
            base.Execute();
        }

        #endregion

        #region Methods

        /// <summary>
        /// do dispose.
        /// </summary>
        /// <param name="isManage">
        /// The is manage.
        /// </param>
        protected override void DoDispose(bool isManage)
        {
            if (isManage)
            {
            }

            base.DoDispose(isManage);
        }

        protected override void Execute_Internal()
        {
            InnotiveDebug.Trace("[Action] PlayerGoToLocationActionProcess Excute() Name : {0} , GUID : {1}", this._item.Name, this._item.SyncGUID);

            KeySpline keySpline = new KeySpline(this._item.KeySplineX1, this._item.KeySplineY1, this._item.KeySplineX2, this._item.KeySplineY2);

            MainProcess.Instance.ProjectManager.LayoutContentsSyncManager.GoToLocation(
                this._item.LayoutGuid,
                this._item.BookMarkGuid,
                this._item.ActionEffect,
                this._item.StartTime,
                this._item.Duration, 
                keySpline);
        }

        #endregion
    }
}