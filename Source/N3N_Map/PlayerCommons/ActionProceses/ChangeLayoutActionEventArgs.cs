﻿
namespace Innotive.InnoWatch.PlayerCommons.ActionProceses
{
    using System;

    public delegate void ChangeLayoutActionEventHandler(object sender, ChangeLayoutActionEventArgs e);

    public class ChangeLayoutActionEventArgs : EventArgs
    {
        public Guid OriginLayoutGuid = Guid.Empty;
        public Guid NewLayoutGuid = Guid.Empty;
        public Guid LoadedActionGuid = Guid.Empty;

        public ChangeLayoutActionEventArgs(Guid originLayoutGuid, Guid newLayoutGuid, Guid loadedActionGuid)
        {
            OriginLayoutGuid = originLayoutGuid;
            NewLayoutGuid = newLayoutGuid;
            LoadedActionGuid = loadedActionGuid;
        }
    }
}
