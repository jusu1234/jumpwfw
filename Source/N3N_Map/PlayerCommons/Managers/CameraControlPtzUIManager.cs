﻿
namespace Innotive.InnoWatch.PlayerCommons.Managers
{
    using System;
    using Innotive.InnoWatch.Commons.CameraController;
    using Innotive.InnoWatch.Commons.EventArguments;
    using Innotive.InnoWatch.DLLs.CameraControls;

    public class CameraControlPtzUIManager
    {
        public CameraControl LastClickedCameraControl { get; set; }
        public event EventHandler<RefreshCameraControllerEventArgs> eRefreshCameraController = null;

        public CameraControlPtzUIManager()
        {
            PTZManager.Instance.RefreshCameraControllerEventHandler += this.CameraControllerEventHandlerManagerRefreshCameraControllerEventHandler;
        }
        
        // CameraControl의 제어모드를 관리하기 위함
        // CameraControl이 제어모드로 들어간 경우 이전 제어모드를 해제해줌
        private void CameraControllerEventHandlerManagerRefreshCameraControllerEventHandler(object sender, RefreshCameraControllerEventArgs e)
        {
            if (e.IsEnabled)
            {
                if (this.LastClickedCameraControl != e.CameraControl)
                {
                    if (this.LastClickedCameraControl != null)
                    {
                        this.LastClickedCameraControl.EnablePTZControlMode(false);
                        this.LastClickedCameraControl.ShowPTZControlButton(false);
                    }
                }

                this.LastClickedCameraControl = e.CameraControl as CameraControl;
            }
            else
            {
                if (this.LastClickedCameraControl == e.CameraControl)
                    this.LastClickedCameraControl = null;
            }

            // event 발생시킴 !! iCommand의 CameraController UI를 제어하기 위한 event
            var temp = this.eRefreshCameraController;
            if (temp != null)
            {
                temp(this, new RefreshCameraControllerEventArgs(e.CameraControl, e.IsEnabled, e.PtzInformation));
            }
        }
    }
}
