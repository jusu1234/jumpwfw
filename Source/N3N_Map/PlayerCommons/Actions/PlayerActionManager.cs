﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PlayerActionManager.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   player action manager.
// </summary>
// --------------------------------------------------------------------------------------------------------------------


using Innotive.InnoWatch.CommonUtils.Models;

namespace Innotive.InnoWatch.PlayerCommons.Actions
{
    using System;
    using System.Collections.Generic;
    using System.Windows;

    using Innotive.InnoWatch.Commons.Actions;
    using Innotive.InnoWatch.Commons.BaseControls;
    using Innotive.InnoWatch.Commons.Publics;
    using Innotive.InnoWatch.CommonUtils.Log;
    using Innotive.InnoWatch.PlayerCommons.ActionProceses;

    /// <summary>
    /// player action manager.
    /// </summary>
    public class PlayerActionManager
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PlayerActionManager"/> class. 
        /// 클래스 생성자.
        /// </summary>
        public PlayerActionManager()
        {
            ActionExecuter.eActionExecute += this.ActionExecuter_eActionExecute;
        }

        /// <summary>
        /// action raised event.
        /// </summary>
        //public event EventHandler OnContentsActionRaisedEvent;
        
        /// <summary>
        /// on change camera mode event.
        /// </summary>
        public event EventHandler OnChangeCameraModeEvent;

        public event EventHandler<ConsoleFavoriteActionEventArgs> OnExcuteFavoriteActionEvent;

        public event EventHandler<ConsoleCameraActionEventArgs> OnExcuteCameraActionEvent;

        public event EventHandler<ConsoleLayoutActionEventArgs> OnExcuteLayoutActionEvent;

        public event EventHandler<ConsoleFullscreenActionEventArgs> OnExcuteFullscreenActionEvent;
        
        public event EventHandler<ConsoleNormalscreenActionEventArgs> OnExcuteNormalscreenActionEvent;

        public event EventHandler<ConsoleStageShowActionEventArgs> OnExcuteStageShowActionEvent;

        public event EventHandler<ConsoleStageHideActionEventArgs> OnExcuteStageHideActionEvent;

        public event EventHandler<ConsoleStageClearActionEventArgs> OnExcuteStageClearActionEvent;
                
        /// <summary>
        /// open layout event.
        /// </summary>
        public event OpenLayoutActionEventHandler OnOpenLayoutActionEvent;

        /// <summary>
        /// open layout event.
        /// </summary>
        public event ChangeLayoutActionEventHandler OnChangeLayoutActionEvent;

        /// <summary>
        /// Gets Actions.
        /// </summary>
        public DataActions Actions { get; private set; }

        /// <summary>
        /// load.
        /// </summary>
        /// <param name="filename">
        /// The filename.
        /// </param>
        public void Load(string filename)
        {
            this.Actions = DataActions.Read(filename);
        }

        /// <summary>
        /// mhkim : 컨텐츠 액션 실행을 위해 외부에서 접근 할 수 있도록 만듬.
        /// </summary>
        /// <param name="actionProcessSyncGuid">
        /// The action process sync guid.
        /// </param>
        public void ExcuteFromEvent(Guid actionProcessSyncGuid)
        {
            this.Excute(actionProcessSyncGuid);
        }

        public void ExecuteFromIdleActionName(string idleActionName)
        {
            var excuteList = this.Actions.GetActionProcessSyncGUIDbyName(idleActionName);

            foreach (var guid in excuteList)
            {
                this.Excute(guid);
            }
        }

        /// <summary>
        /// 가지고 있는 ActionProcessItem 들을 실행한다. 
        /// </summary>
        /// <param name="actionProcessSyncGuid">
        /// Item들을 소유한 부모의 SyncGuid ( ActionProcess 또는 ActionProcessItem ).
        /// </param>
        /// <param name="isFromSyncServer">
        /// 싱크 서버에서 오는지 여부.
        /// </param>
        internal void Excute(Guid actionProcessSyncGuid, bool isFromSyncServer = false, bool isSendToSyncServer = true)
        {
            if (this.Actions == null)
            {
                InnotiveDebug.Trace("[Action] PlayerActionManager.Actions가 null임 !!");
                return;
            }

            var processItems = this.Actions.GetActionProcessItems(actionProcessSyncGuid);

            InnotiveDebug.Trace("[Action] PlayerActionManager Excute() GUID : {0}", actionProcessSyncGuid);
            
            if (processItems == null)
            {
                return;
            }

            InnotiveDebug.Trace("[Action] PlayerActionManager Excute() Count : {0}", processItems.Count);

            // 컨텐츠 Sync 기능은 Console, ConsoleBasic에만 있음 !!
            // iViewer 추가로 iCommand만 보내는 것으로 변경함.
            if (MainProcess.Instance.ProgramType == ProgramType.iCommand && isSendToSyncServer == true)
            {
                // ChangeLayout이 존재하는 경우 LoadedActionGuid때문에 Execute()를 한번 더 탐 !!!
                // 그래서 ActionProcess Class인지 Check를 해서 ActionProcess Class인 경우 Server에 Action 발생을 알림 !!!
                var actionProcess = this.Actions.GetActionProcess(actionProcessSyncGuid);
                if (actionProcess != null)
                {
                    InnotiveDebug.Trace(1, "PlayerActionManager Excute() ActionProcess 정보를 SendBuffer에 보냄 !! Guid = {0}, Name = {1}", actionProcess.SyncGUID, actionProcess.Name);

                    MainProcess.Instance.CommManager.ControlServer.SyncHelper.SendAction(actionProcess.SyncGUID, actionProcess.Name);
                }
            }

            foreach (var item in processItems)
            {
                if (item is DataOpenLayoutActionProcessItem)
                {
                    var actionProcess = new PlayerOpenLayoutActionProcess(item as DataOpenLayoutActionProcessItem);

                    actionProcess.OnOpenLayoutActionEvent += this.PlayerActionManager_OnOpenLayoutActionEvent;

                    actionProcess.Execute();
                }
                else if (item is DataChangeLayoutActionProcessItem)
                {
                    var actionProcess = new PlayerChangeLayoutActionProcess(item as DataChangeLayoutActionProcessItem);

                    actionProcess.OnChangeLayoutActionEvent += this.PlayerActionManager_OnChangeLayoutActionEvent;

                    actionProcess.Execute();
                }
                else if (item is DataBringToFrontActionProcessItem)
                {
                    var actionProcess = new PlayerBringToFrontActionProcess(item as DataBringToFrontActionProcessItem);
                    actionProcess.Execute();
                }
                else if (item is DataChangePropertyActionProcessItem)
                {
                    var actionProcess =
                        new PlayerChangePropertyActionProcess(item as DataChangePropertyActionProcessItem);
                    actionProcess.Execute();
                }
                else if (item is DataXamlControlStoryboardActionProcessItem)
                {
                    var actionProcess =
                        new PlayerXamlControlStoryboardActionProcess(item as DataXamlControlStoryboardActionProcessItem);
                    actionProcess.Execute();
                }
                else if (item is DataExternalCommandActionProcessItem)
                {
                    // 싱크 서버에서 오는 Server Message Action은 실행하면 중복 실행하기 때문에 실행하면 안된다. (by jhlee) - 알았죠 김현근 과장님!!!!!
                    if (!isFromSyncServer)
                    {
                        var actionProcess = new PlayerExternalCommandActionProcess(item as DataExternalCommandActionProcessItem);
                        actionProcess.Execute();
                    }
                }
                else if (item is DataConsoleActionProcessItem)
                {
                    //SyncServer에서 온 메세지인 경우 실행을 안함 !!
                    if (!isFromSyncServer)
                    {
                        var actionProcess = new PlayerConsoleActionProcess(item as DataConsoleActionProcessItem);

                        actionProcess.OnChangeCameraModeEvent += this.PlayerActionManager_OnChangeCameraModeEvent;

                        string actionType = (item as DataConsoleActionProcessItem).ConsoleActionType;

                        actionProcess.ConsoleActionType = (ConsoleActionType)Enum.Parse(typeof(ConsoleActionType), actionType, true);

                        actionProcess.Execute();
                    }
                }
                else if (item is DataConsoleFavoriteActionProcessItem)
                {
                    //SyncServer에서 온 메세지인 경우 실행을 안함 !!
                    if (!isFromSyncServer)
                    {
                        var actionProcess = new PlayerConsoleFavoriteActionProcess(item as DataConsoleFavoriteActionProcessItem);

                        actionProcess.OnExecuteFavoriteAction += this.PlayerActionManager_OnExecuteFavoriteEvent;

                        actionProcess.Execute();
                    }
                }
                else if (item is DataConsoleCameraActionProcessItem)
                {
                    //SyncServer에서 온 메세지인 경우 실행을 안함 !!
                    if (!isFromSyncServer)
                    {
                        var actionProcess = new PlayerConsoleCameraActionProcess(item as DataConsoleCameraActionProcessItem);

                        actionProcess.OnExecuteCameraAction += this.PlayerActionManager_OnExecuteCameraEvent;

                        actionProcess.Execute();
                    }
                }
                else if (item is DataConsoleLayoutActionProcessItem)
                {
                    //SyncServer에서 온 메세지인 경우 실행을 안함 !!
                    if (!isFromSyncServer)
                    {
                        var actionProcess = new PlayerConsoleLayoutActionProcess(item as DataConsoleLayoutActionProcessItem);

                        actionProcess.OnExecuteLayoutAction += this.PlayerActionManager_OnExecuteLayoutEvent;

                        actionProcess.Execute();
                    }
                }
                else if (item is DataConsoleFullscreenActionProcessItem)
                {
                    //SyncServer에서 온 메세지인 경우 실행을 안함 !!
                    if (!isFromSyncServer)
                    {
                        var actionProcess = new PlayerConsoleFullscreenActionProcess(item as DataConsoleFullscreenActionProcessItem);

                        actionProcess.OnExecuteFullscreenAction += this.PlayerActionManager_OnExecuteFullscreenEvent;

                        actionProcess.Execute();
                    }
                }
                else if (item is DataConsoleNormalscreenActionProcessItem)
                {
                    //SyncServer에서 온 메세지인 경우 실행을 안함 !!
                    if (!isFromSyncServer)
                    {
                        var actionProcess = new PlayerConsoleNormalscreenActionProcess(item as DataConsoleNormalscreenActionProcessItem);

                        actionProcess.OnExecuteNormalscreenAction += this.PlayerActionManager_OnExecuteNormalscreenEvent;

                        actionProcess.Execute();
                    }
                }
                else if (item is DataConsoleStageShowActionProcessItem)
                {
                    //SyncServer에서 온 메세지인 경우 실행을 안함 !!
                    if (!isFromSyncServer)
                    {
                        var actionProcess = new PlayerConsoleStageShowActionProcess(item as DataConsoleStageShowActionProcessItem);

                        actionProcess.OnExecuteStageShowAction += this.PlayerActionManager_OnExecuteStageShowEvent;

                        actionProcess.Execute();
                    }
                }
                else if (item is DataConsoleStageHideActionProcessItem)
                {
                    //SyncServer에서 온 메세지인 경우 실행을 안함 !!
                    if (!isFromSyncServer)
                    {
                        var actionProcess = new PlayerConsoleStageHideActionProcess(item as DataConsoleStageHideActionProcessItem);

                        actionProcess.OnExecuteStageHideAction += this.PlayerActionManager_OnExecuteStageHideEvent;

                        actionProcess.Execute();
                    }
                }
                else if (item is DataConsoleStageClearActionProcessItem)
                {
                    //SyncServer에서 온 메세지인 경우 실행을 안함 !!
                    if (!isFromSyncServer)
                    {
                        var actionProcess = new PlayerConsoleStageClearActionProcess(item as DataConsoleStageClearActionProcessItem);

                        actionProcess.OnExecuteStageClearAction += this.PlayerActionManager_OnExecuteStageClearEvent;

                        actionProcess.Execute();
                    }
                }
                else if (item is DataExecuteFileActionProcessItem)
                {
                    //SyncServer에서 온 메세지인 경우 파일 실행을 안함 !!
                    //ex. Console이 두대인경우 두번째 Console을 실행을 안함. Display전체는 실행을 안함 !!
                    if (!isFromSyncServer)
                    {
                        var actionProcess = new PlayerExecuteFileActionProcess(item as DataExecuteFileActionProcessItem);
                        actionProcess.Execute();
                    }
                }
                else if (item is DataGoHomeActionProcessItem)
                {
                    var actionProcess = new PlayerGoHomeActionProcess(item as DataGoHomeActionProcessItem);
                    actionProcess.Execute();
                }
                else if (item is DataGoToLocationActionProcessItem)
                {
                    var actionProcess = new PlayerGoToLocationActionProcess(item as DataGoToLocationActionProcessItem);
                    actionProcess.Execute();
                }
                else if (item is DataGoToObjectActionProcessItem)
                {
                    var actionProcess = new PlayerGoToObjectActionProcess(item as DataGoToObjectActionProcessItem);
                    actionProcess.Execute();
                }
                else if (item is DataShowHideActionProcessItem)
                {
                    var actionProcess = new PlayerShowHideActionProcess(item as DataShowHideActionProcessItem);
                    actionProcess.Execute();
                }
                else if (item is DataShowHideAllActionProcessItem)
                {
                    var actionProcess = new PlayerShowHideAllActionProcess(item as DataShowHideAllActionProcessItem);
                    actionProcess.Execute();
                }
                else if (item is DataToggleChangePropertyActionProcessItem)
                {
                    var actionProcess =
                        new PlayerToggleChangePropertyActionProcess(item as DataToggleChangePropertyActionProcessItem);
                    actionProcess.Execute();
                }
                else if (item is DataZoomInActionProcessItem)
                {
                    var actionProcess = new PlayerZoomInActionProcess(item as DataZoomInActionProcessItem);
                    actionProcess.Execute();
                }
                else if (item is DataZoomOutActionProcessItem)
                {
                    var actionProcess = new PlayerZoomOutActionProcess(item as DataZoomOutActionProcessItem);
                    actionProcess.Execute();
                }
            }
        }

        /// <summary>
        /// 발생한 Event 의 데이터를 가지고, ActionProcess 를 실행한다. 
        /// </summary>
        /// <param name="element">
        /// 발생한 Element.
        /// </param>
        /// <param name="actionTriggerType">
        /// 발생한 Trigger Type.
        /// </param>
        /// <returns>
        /// 성공유무.
        /// </returns>
        internal bool ExcuteActionProcess(FrameworkElement element, TriggerEventType actionTriggerType, bool isSendToSyncServer = true)
        {
            if (this.Actions == null)
            {
                return false;
            }

            if (actionTriggerType == TriggerEventType.MouseClick)
            {
                if (element is BaseControl)
                {
                    var process = this.Actions.GetActionProcess((element as BaseControl).MouseClickGUID);

                    if (process != null && process.ActionProcessItems != null)
                    {
                        this.Excute(process.SyncGUID, false, isSendToSyncServer);

                        return true;
                    }
                }
            }

            return false;
        }

        /// <summary>
        /// 일반 Action에 대한 외부 event를 발생시킴
        /// Console에서 사용함
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        ///
        private void PlayerActionManager_OnOpenLayoutActionEvent(object sender, OpenLayoutActionEventArgs e)
        {
            if (this.OnOpenLayoutActionEvent != null)
            {
                this.OnOpenLayoutActionEvent(this, new OpenLayoutActionEventArgs(e.DisplayAreaName, e.NewLayoutGuid, e.LoadedActionGuid));
            }
        }

        private void PlayerActionManager_OnChangeLayoutActionEvent(object sender, ChangeLayoutActionEventArgs e)
        {
            if (this.OnChangeLayoutActionEvent != null)
            {
                this.OnChangeLayoutActionEvent(this, new ChangeLayoutActionEventArgs(e.OriginLayoutGuid, e.NewLayoutGuid, e.LoadedActionGuid));
            }
        }


        /// <summary>
        /// XamlViewer 컨트롤에서 오는 액션 실행 명령.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        ///
        private void ActionExecuter_eActionExecute(object sender, ActionExecuterEventArgs e)
        {
            List<Guid> excuteList = this.Actions.GetActionProcessSyncGUIDbyName(e.ActionName);

            foreach (Guid guid in excuteList)
            {
                this.Excute(guid);
            }
        }
        
        private void PlayerActionManager_OnChangeCameraModeEvent(object sender, EventArgs e)
        {
            if (this.OnChangeCameraModeEvent != null)
            {
                this.OnChangeCameraModeEvent(this, new EventArgs());
            }
        }

        private void PlayerActionManager_OnExecuteFavoriteEvent(object sender, ConsoleFavoriteActionEventArgs e)
        {
            if (this.OnExcuteFavoriteActionEvent != null)
            {
                this.OnExcuteFavoriteActionEvent(this, e);
            }
        }

        private void PlayerActionManager_OnExecuteCameraEvent(object sender, ConsoleCameraActionEventArgs e)
        {
            if (this.OnExcuteCameraActionEvent != null)
            {
                this.OnExcuteCameraActionEvent(this, e);
            }
        }

        private void PlayerActionManager_OnExecuteLayoutEvent(object sender, ConsoleLayoutActionEventArgs e)
        {
            if (this.OnExcuteLayoutActionEvent != null)
            {
                this.OnExcuteLayoutActionEvent(this, e);
            }
        }

        private void PlayerActionManager_OnExecuteFullscreenEvent(object sender, ConsoleFullscreenActionEventArgs e)
        {
            if (this.OnExcuteFullscreenActionEvent != null)
            {
                this.OnExcuteFullscreenActionEvent(this, e);
            }
        }

        private void PlayerActionManager_OnExecuteNormalscreenEvent(object sender, ConsoleNormalscreenActionEventArgs e)
        {
            if (this.OnExcuteNormalscreenActionEvent != null)
            {
                this.OnExcuteNormalscreenActionEvent(this, e);
            }
        }

        private void PlayerActionManager_OnExecuteStageShowEvent(object sender, ConsoleStageShowActionEventArgs e)
        {
            if (this.OnExcuteStageShowActionEvent != null)
            {
                this.OnExcuteStageShowActionEvent(this, e);
            }
        }

        private void PlayerActionManager_OnExecuteStageHideEvent(object sender, ConsoleStageHideActionEventArgs e)
        {
            if (this.OnExcuteStageHideActionEvent != null)
            {
                this.OnExcuteStageHideActionEvent(this, e);
            }
        }

        private void PlayerActionManager_OnExecuteStageClearEvent(object sender, ConsoleStageClearActionEventArgs e)
        {
            if (this.OnExcuteStageClearActionEvent != null)
            {
                this.OnExcuteStageClearActionEvent(this, e);
            }
        }
    }
}