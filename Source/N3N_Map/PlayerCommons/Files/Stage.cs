﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Stage.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   stage.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.PlayerCommons.Files
{
    /// <summary>
    /// 스테이지 정보를 표현.
    /// </summary>
    public class Stage
    {
        #region Properties

        /// <summary>
        /// Gets or sets Index.
        /// </summary>
        public int Index { get; set; }

        /// <summary>
        /// Gets or sets LinkedDisplayAreaName.
        /// </summary>
        public string LinkedDisplayAreaName { get; set; }

        /// <summary>
        /// Gets or sets Name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets Description.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets Alias.
        /// </summary>
        public string Alias { get; set; }

        /// <summary>
        /// Gets or set Show.
        /// </summary>
        public bool Show { get; set; }

        /// <summary>
        /// Gets or sets ParentRelativeX.
        /// </summary>
        public double ParentRelativeX { get; set; }

        /// <summary>
        /// Gets or sets ParentRelativeY.
        /// </summary>
        public double ParentRelativeY { get; set; }

        /// <summary>
        /// Gets or sets ParentRelativeWidth.
        /// </summary>
        public double ParentRelativeWidth { get; set; }

        /// <summary>
        /// Gets or sets ParentRelativeHeight.
        /// </summary>
        public double ParentRelativeHeight { get; set; }

        /// <summary>
        /// Gets or sets ChildGridRatioWidth.
        /// </summary>
        public double ChildGridRatioWidth { get; set; }

        /// <summary>
        /// Gets or sets ChildGridRatioHeight.
        /// </summary>
        public double ChildGridRatioHeight { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether IsStretch.
        /// </summary>
        public bool IsStretch { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether IsAlwaysSync.
        /// </summary>
        public bool IsAlwaysSync { get; set; }

        #endregion
    }
}