// --------------------------------------------------------------------------------------------------------------------
// <copyright company="Innotive Inc. Korea" file="DisplayAreaOpenChangeLayoutSyncManager.cs">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   The display area sync manager.
// </summary>
// 
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.PlayerCommons.Communications.Sync
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using Innotive.InnoWatch.Commons.BaseControls;
    using Innotive.InnoWatch.Commons.Files;
    using Innotive.InnoWatch.CommonUtils.Log;
    using Innotive.InnoWatch.DLLs.ActionEffects;
    using Innotive.InnoWatch.DLLs.LayoutControls;
    
    using Innotive.InnoWatch.PlayerCommons.Configs;
    using Innotive.InnoWatch.PlayerCommons.DisplayAreas;
    using Innotive.InnoWatch.PlayerCommons.Layouts;

    /// <summary>
    /// The display area sync manager.
    /// </summary>
    public class DisplayAreaOpenChangeLayoutSyncManager
    {
        #region Constants and Fields

        private PlayerDisplayAreaManager _displayAreaManager;
        private PlayerLayoutFileManager _playerLayoutManager;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="DisplayAreaOpenChangeLayoutSyncManager"/> class. 
        /// =====생성자=====.
        /// </summary>
        /// <param name="displayAreaManager">
        /// </param>
        /// <param name="playerLayoutManager">
        /// </param>
        public DisplayAreaOpenChangeLayoutSyncManager(
            PlayerDisplayAreaManager displayAreaManager, PlayerLayoutFileManager playerLayoutManager)
        {
            this._displayAreaManager = displayAreaManager;
            this._playerLayoutManager = playerLayoutManager;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// 화면에 떠 있는 특정 Layout들 전체를 다른 Layout으로 변경함 !!
        /// Button을 눌렀을때 발생하는 Action !!.
        /// </summary>
        /// <param name="originLayoutGUID">
        /// The origin Layout GUID.
        /// </param>
        /// <param name="newLayoutGUID">
        /// The new Layout GUID.
        /// </param>
        /// <param name="effect">
        /// The effect.
        /// </param>
        /// <param name="startTime">
        /// The start Time.
        /// </param>
        /// <param name="duration">
        /// The duration.
        /// </param>
        /// <param name="loadedActionGuid">
        /// The loaded Action Guid.
        /// </param>
        public void ChangeLayout(
            Guid originLayoutGUID,
            Guid newLayoutGUID,
            string effect,
            double startTime,
            double duration,
            Guid loadedActionGuid)
        {
            this.ChangeLayout_ContentsMode(originLayoutGUID, newLayoutGUID, effect, startTime, duration, loadedActionGuid);
            this.ChangeLayout_CameraMode(originLayoutGUID, newLayoutGUID, effect, startTime, duration, loadedActionGuid);
        }

        /// <summary>
        /// Contents Mode에 ChangeLayout을 적용함
        /// </summary>
        /// <param name="originLayoutGUID">
        /// The origin Layout GUID.
        /// </param>
        /// <param name="newLayoutGUID">
        /// The new Layout GUID.
        /// </param>
        /// <param name="effect">
        /// The effect.
        /// </param>
        /// <param name="startTime">
        /// The start Time.
        /// </param>
        /// <param name="duration">
        /// The duration.
        /// </param>
        /// <param name="loadedActionGuid">
        /// The loaded Action Guid.
        /// </param>
        private void ChangeLayout_ContentsMode(
            Guid originLayoutGUID,
            Guid newLayoutGUID,
            string effect,
            double startTime,
            double duration,
            Guid loadedActionGuid)
        {
            InnotiveDebug.Trace(3, "===============================================================");
            InnotiveDebug.Trace(
                3,
                "======= ChangeLayout 시작 !! originLayoutGuid = {0}, newLayoutGuid = {1}",
                originLayoutGUID,
                newLayoutGUID);
            InnotiveDebug.Trace(3, "===============================================================");

            // 바뀌는 DisplayArea List, 안바뀌는 DisplayArea List
            var changedDisplayAreaList = new List<PlayerDisplayArea>();
            var noChangedDisplayAreaList = new List<PlayerDisplayArea>();

            if (originLayoutGUID == newLayoutGUID)
            {
                InnotiveDebug.Trace(4, "DisplayAreaOpenChangeLayoutSyncManager.OpenLayout() OriginLayoutGuid와 NewLayoutGuid가 같습니다.");
                return;
            }

            InnoFilePath layoutFullPath = this._playerLayoutManager.GetLayoutFileName(newLayoutGUID);
            if (layoutFullPath == null || !File.Exists(layoutFullPath.Path))
            {
                InnotiveDebug.Trace(4, "DisplayAreaOpenChangeLayoutSyncManager.OpenLayout() ==> Layout 파일이 존재하지 않습니다.");
                return;
            }

            //실제 OpenLayout을 하기 전에 바뀔 DisplayArea의 최종 결과값을 미리 저장해둠 !! Mirror가 이 값을 사용함 !!
            for (int index = 0; index < this._displayAreaManager.Count; index++)
            {
                var displayArea = this._displayAreaManager.GetDisplayArea(index) as PlayerDisplayArea;

                bool isFindLayout = false;

                if (displayArea.IsEnabled && displayArea.CurrentLayout != null)
                {
                    if (displayArea.CurrentLayout.SyncGUID == originLayoutGUID)
                    {
                        isFindLayout = true;
                    }
                }
                else
                {
                    if (displayArea.NewestLayoutSyncGuid == originLayoutGUID)
                    {
                        isFindLayout = true;
                    }
                }

                if (isFindLayout)
                {
                    changedDisplayAreaList.Add(displayArea);
                    displayArea.NewestLayoutSyncGuid = newLayoutGUID;
                    displayArea.NewestLayoutLoadedGuid = loadedActionGuid;
                }
                else
                {
                    noChangedDisplayAreaList.Add(displayArea);
                }
            }

            // 바뀐 Layout이 하나도 없는 경우는 exception 발생시킴 !!
            if (changedDisplayAreaList.Count <= 0)
            {
                InnotiveDebug.Trace(4, "DisplayAreaOpenChangeLayoutSyncManager.ChangeLayout_ContentsMode() ==> 바뀐 Layout이 없습니다.");
                return;
            }

            // 안바뀌는 DisplayArea List 중에서 Mirror가 있으면 먼저 OpenLayout을 해줌 !!
            foreach (var noChangedDisplayArea in noChangedDisplayAreaList)
            {
                if (!noChangedDisplayArea.IsEnabled)
                {
                    continue;
                }
                
                RefreshChildMirrorLayout(noChangedDisplayArea.CurrentLayout, changedDisplayAreaList);
            }

            // 실제 OpenLayout을 실행함 !!
            foreach (var changedDisplayArea in changedDisplayAreaList)
            {
                var changedLayoutControl = changedDisplayArea.OpenLayout(newLayoutGUID, effect, startTime, duration, loadedActionGuid) as LayoutControl;
                if (changedLayoutControl != null)
                {
                    var allDisplayAreaExceptMeList = new List<PlayerDisplayArea>();

                    for (int index = 0; index < this._displayAreaManager.Count; index++)
                    {
                        var tempDisplayArea = this._displayAreaManager.GetDisplayArea(index) as PlayerDisplayArea;
                        if (tempDisplayArea == changedDisplayArea)
                            continue;

                        allDisplayAreaExceptMeList.Add(tempDisplayArea);
                    }

                    RefreshChildMirrorLayout(changedLayoutControl, allDisplayAreaExceptMeList);
                }
            }
        }

        /// <summary>
        /// Camera Assign Mode에 ChangeLayout을 적용함
        /// </summary>
        /// <param name="originLayoutGUID">
        /// The origin Layout GUID.
        /// </param>
        /// <param name="newLayoutGUID">
        /// The new Layout GUID.
        /// </param>
        /// <param name="effect">
        /// The effect.
        /// </param>
        /// <param name="startTime">
        /// The start Time.
        /// </param>
        /// <param name="duration">
        /// The duration.
        /// </param>
        /// <param name="loadedActionGuid">
        /// The loaded Action Guid.
        /// </param>
        private void ChangeLayout_CameraMode(
            Guid originLayoutGUID,
            Guid newLayoutGUID,
            string effect,
            double startTime,
            double duration,
            Guid loadedActionGuid)
        {
            
        }

        /// <summary>
        /// 여러 Layout을 동시에 Open함.
        /// </summary>
        /// <param name="openLayoutInfoList">
        /// The open Layout Info List.
        /// </param>
        public void GroupOpenLayout(List<OpenLayoutInfo> openLayoutInfoList)
        {
            // 바뀌는 DisplayArea List, 안바뀌는 DisplayArea List
            var changedDisplayAreaList = new List<PlayerDisplayArea>();
            var noChangedDisplayAreaList = new List<PlayerDisplayArea>();

            foreach (var info in openLayoutInfoList)
            {
                var layoutFullPath = this._playerLayoutManager.GetLayoutFileName(info.LayoutGuid);
                if (layoutFullPath == null || !File.Exists(layoutFullPath.Path))
                {
                    InnotiveDebug.Trace(4, "DisplayAreaOpenChangeLayoutSyncManager.OpenLayout() ==> Layout 파일이 존재하지 않습니다.");
                    continue;
                }

                //실제 OpenLayout을 하기 전에 바뀔 DisplayArea의 최종 결과값을 미리 저장해둠 !! Mirror가 이 값을 사용함 !!
                for (int index = 0; index < this._displayAreaManager.Count; index++)
                {
                    var displayArea = this._displayAreaManager.GetDisplayArea(index) as PlayerDisplayArea;

                    if (string.Compare(displayArea.Name, info.DisplayAreaName, true) == 0)
                    {
                        changedDisplayAreaList.Add(displayArea);
                        displayArea.NewestLayoutSyncGuid = info.LayoutGuid;
                    }
                }
            }

            // 바뀐 Layout이 하나도 없는 경우는 exception 발생시킴 !!
            if (changedDisplayAreaList.Count <= 0)
            {
                InnotiveDebug.Trace(4, "DisplayAreaOpenChangeLayoutSyncManager.OpenLayout() ==> 바뀐 Layout이 없습니다.");
                return;
            }

            // 바뀌지 않은 DisplayAreaList
            for (int index = 0; index < this._displayAreaManager.Count; index++)
            {
                var displayArea = this._displayAreaManager.GetDisplayArea(index) as PlayerDisplayArea;

                foreach (var changedDisplayArea in changedDisplayAreaList)
                {
                    if (displayArea == changedDisplayArea)
                    {
                        continue;
                    }

                    noChangedDisplayAreaList.Add(displayArea);
                }
            }

            // 안바뀌는 DisplayArea List 중에서 Mirror가 있으면 먼저 OpenLayout을 해줌 !!
            foreach (var noChangedDisplayArea in noChangedDisplayAreaList)
            {
                if (!noChangedDisplayArea.IsEnabled)
                {
                    continue;
                }
                
                RefreshChildMirrorLayout(noChangedDisplayArea.CurrentLayout, changedDisplayAreaList);
            }

            // 실제 OpenLayout을 실행함 !!
            foreach (var changedDisplayArea in changedDisplayAreaList)
            {
                var changedLayoutControl = changedDisplayArea.OpenLayout(changedDisplayArea.NewestLayoutSyncGuid, string.Empty, 0, 0, Guid.Empty) as LayoutControl;

                if (changedLayoutControl != null)
                {
                    var allDisplayAreaExceptMeList = new List<PlayerDisplayArea>();
                    for (int index = 0; index < this._displayAreaManager.Count; index++)
                    {
                        var tempDisplayArea = this._displayAreaManager.GetDisplayArea(index) as PlayerDisplayArea;
                        if (tempDisplayArea == changedDisplayArea)
                            continue;

                        allDisplayAreaExceptMeList.Add(tempDisplayArea);
                    }

                    RefreshChildMirrorLayout(changedLayoutControl, allDisplayAreaExceptMeList);
                }
            }
        }

        /// <summary>
        /// 지정된 displayArea의 Layout을 바꿈 !!
        /// Button을 눌렀을때 발생하는 Action !!.
        /// </summary>
        /// <param name="displayAreaName">
        /// The display Area Name.
        /// </param>
        /// <param name="layoutGuid">
        /// The layout Guid.
        /// </param>
        /// <param name="effect">
        /// The effect.
        /// </param>
        /// <param name="startTime">
        /// The start Time.
        /// </param>
        /// <param name="duration">
        /// The duration.
        /// </param>
        /// <param name="loadedActionGuid">
        /// The loaded Action Guid.
        /// </param>
        public void OpenLayout(
            string displayAreaName,
            Guid layoutGuid,
            string effect,
            double startTime,
            double duration,
            Guid loadedActionGuid)
        {
            this.OpenLayout_ContentsMode(displayAreaName, layoutGuid, effect, startTime, duration, loadedActionGuid);
            this.OpenLayout_CameraMode(displayAreaName, layoutGuid, effect, startTime, duration, loadedActionGuid);
        }

        /// <summary>
        /// DisplayArea에 StartLayout을 연다.
        /// player 로딩 시 ProjectManager.OpenProject에서 사용된다.
        /// </summary>
        public void ShowStartLayouts()
        {
            InnotiveDebug.Trace(2, "SynchronizeShowStartLayouts 시작 !!");

            var openLayoutInfoList = new List<OpenLayoutInfo>();

            for (int index = 0; index < this._displayAreaManager.Count; index++)
            {
                var displayArea = this._displayAreaManager.GetDisplayArea(index) as PlayerDisplayArea;
                var info = new OpenLayoutInfo(displayArea.Name, displayArea.StartLayoutGuid, ActionEffects.None, 0, 0, displayArea.Rect);
                openLayoutInfoList.Add(info);
            }

            this.GroupOpenLayout(openLayoutInfoList);

            InnotiveDebug.Trace(2, "SynchronizeShowStartLayouts 종료 !!");
        }

        #endregion

        #region Methods

        /// <summary>
        /// OpenLayout을 실제로 진행함.
        /// </summary>
        /// <param name="displayAreaName">
        /// The display Area Name.
        /// </param>
        /// <param name="layoutGuid">
        /// The layout Guid.
        /// </param>
        /// <param name="effect">
        /// The effect.
        /// </param>
        /// <param name="startTime">
        /// The start Time.
        /// </param>
        /// <param name="duration">
        /// The duration.
        /// </param>
        /// <param name="loadedActionGuid">
        /// The loaded Action Guid.
        /// </param>
        private void OpenLayout_CameraMode(
            string displayAreaName,
            Guid layoutGuid,
            string effect,
            double startTime,
            double duration,
            Guid loadedActionGuid)
        {
            
        }

        /// <summary>
        /// OpenLayout을 실제로 진행함.
        /// </summary>
        /// <param name="displayAreaName">
        /// The display Area Name.
        /// </param>
        /// <param name="layoutGuid">
        /// The layout Guid.
        /// </param>
        /// <param name="effect">
        /// The effect.
        /// </param>
        /// <param name="startTime">
        /// The start Time.
        /// </param>
        /// <param name="duration">
        /// The duration.
        /// </param>
        /// <param name="loadedActionGuid">
        /// The loaded Action Guid.
        /// </param>
        private void OpenLayout_ContentsMode(
            string displayAreaName,
            Guid layoutGuid,
            string effect,
            double startTime,
            double duration,
            Guid loadedActionGuid)
        {
            InnotiveDebug.Trace(3, "===============================================================");
            InnotiveDebug.Trace(
                3, "======= OpenLayout 시작 !! displayArea Name = {0}, layoutGuid = {1}", displayAreaName, layoutGuid);
            InnotiveDebug.Trace(3, "===============================================================");

            // 1. 바뀌는 DisplayAreaList, 안바뀌는 DisplayAreaList
            // 2. 안바뀌는 DisplayArea에 포함된 Mirror 갱신
            // -> 1번에서 바뀐 DisplayArea를 가리키는 Mirror만 갱신함
            // 2-1. 갱신된 Mirror Layout을 List에 추가
            // 2-2. 갱신된 Mirror Layout의 Loaded Check
            // 2-3. Loaded 내부에서 List에서 자신 삭제
            // 3. 실제 Layout 갱신
            // 4. 3번 Layout의 Loaded에서 Mirror 갱신

            // 바뀌는 DisplayArea List, 안바뀌는 DisplayArea List
            var changedDisplayAreaList = new List<PlayerDisplayArea>();
            var noChangedDisplayAreaList = new List<PlayerDisplayArea>();

            InnoFilePath layoutFullPath = this._playerLayoutManager.GetLayoutFileName(layoutGuid);
            if (layoutFullPath == null || !File.Exists(layoutFullPath.Path))
            {
                InnotiveDebug.Trace(4, "DisplayAreaOpenChangeLayoutSyncManager.OpenLayout() ==> Layout 파일이 존재하지 않습니다.");
                return;
            }

            // Layout을 변경하기 전에 모든 Layout의 xaml data를 저장함 !!
            // TODO : OpenLayout은 Layout이 하나만 바뀌기 때문에 이 부분의 주석을 해제해도 될듯 !! 아래 ChangeLayout을 주석처리했기 때문에 여기도 일단 주석처리함 !!
            // _playerLayoutManager.SaveAllLayoutXamlData();

            //실제 OpenLayout을 하기 전에 바뀔 DisplayArea의 최종 결과값을 미리 저장해둠 !! Mirror가 이 값을 사용함 !!
            for (int index = 0; index < this._displayAreaManager.Count; index++)
            {
                PlayerDisplayArea displayArea = this._displayAreaManager.GetDisplayArea(index) as PlayerDisplayArea;
                if (string.Compare(displayArea.Name, displayAreaName, true) == 0)
                {
                    changedDisplayAreaList.Add(displayArea);
                    displayArea.NewestLayoutSyncGuid = layoutGuid;
                    displayArea.NewestLayoutLoadedGuid = loadedActionGuid;
                }
                else
                {
                    noChangedDisplayAreaList.Add(displayArea);
                }
            }

            // 바뀐 Layout이 하나도 없는 경우는 exception 발생시킴 !!
            if (changedDisplayAreaList.Count <= 0)
            {
                InnotiveDebug.Trace(4, "DisplayAreaOpenChangeLayoutSyncManager.OpenLayout() ==> 바뀐 Layout이 없습니다.");
                return;
            }

            // 안바뀌는 DisplayArea List 중에서 Mirror가 있으면 먼저 OpenLayout을 해줌 !!
            foreach (PlayerDisplayArea noChangedDisplayArea in noChangedDisplayAreaList)
            {
                if (!noChangedDisplayArea.IsEnabled)
                {
                    continue;
                }

                RefreshChildMirrorLayout(noChangedDisplayArea.CurrentLayout, changedDisplayAreaList);
            }

            // 실제 OpenLayout을 실행함 !!
            foreach (var changedDisplayArea in changedDisplayAreaList)
            {
                var changedLayoutControl = changedDisplayArea.OpenLayout(layoutGuid, effect, startTime, duration, loadedActionGuid) as LayoutControl;

                if (changedLayoutControl != null)
                {
                    List<PlayerDisplayArea> allDisplayAreaExceptMeList = new List<PlayerDisplayArea>();
                    for (int index = 0; index < this._displayAreaManager.Count; index++)
                    {
                        PlayerDisplayArea tempDisplayArea = this._displayAreaManager.GetDisplayArea(index) as PlayerDisplayArea;
                        if (tempDisplayArea == changedDisplayArea)
                            continue;

                        allDisplayAreaExceptMeList.Add(tempDisplayArea);
                    }

                    RefreshChildMirrorLayout(changedLayoutControl, allDisplayAreaExceptMeList);
                }
            }
        }

        /// <summary>
        /// 자식중 MirrorControl이 있으면 갱신함
        /// </summary>
        private void RefreshChildMirrorLayout(BaseControl baseControl, List<PlayerDisplayArea> comparedDisplayAreaList)
        {
            if (baseControl == null)
                return;

        }

        /// <summary>
        /// 자식중 MirrorControl이 있으면 갱신함
        /// </summary>
        public void RefreshChildMirrorLayout(BaseControl baseControl, Guid loadedActionGuid, BaseProcess process, bool aUseStartLayout)
        {
            if (baseControl == null)
            {
                return;
            }
        }

        /// <summary>
        /// 자식들의 process를 변경함.
        /// </summary>
        public void SetProcessModeOfBaseControlAndChildren(BaseControl baseControl, BaseProcess process)
        {
            if (baseControl == null)
            {
                return;
            }

            baseControl.Process = process;

            foreach (var child in LayoutUtils.GetAllChildObjectList(baseControl))
            {
                child.Process = process;

                
            }
        }

        #endregion
    }
}