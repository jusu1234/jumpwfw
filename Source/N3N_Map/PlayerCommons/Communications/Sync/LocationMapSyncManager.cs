﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright company="Innotive Inc. Korea" file="LocationMapSyncManager.cs">
//   Copyright (c) 2012 Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   The layout sync manager.
// </summary>
// --------------------------------------------------------------------------------------------------------------------


using System.Linq;

namespace Innotive.InnoWatch.PlayerCommons.Communications.Sync
{
    using System;
    using System.IO;
    using Innotive.InnoWatch.Commons.Configs;
    using Innotive.InnoWatch.Commons.Utils.Http;
    using Innotive.InnoWatch.DLLs.LayoutControls;
    using Innotive.InnoWatch.DLLs.MapControls;
    using Innotive.InnoWatch.PlayerCommons.Configs;

    /// <summary>
	/// The LocationMapSyncManager
	/// </summary>
	public class LocationMapSyncManager
	{
        public LocationMapSyncManager()
        {
            this.GetLocationMapData();
        }

        /// <summary>
        /// Location Map 파일을 체크해서 동기화 한다. 
        /// ( Sync 로 받은 GUID 와 저장된 파일의 GUID 가 다르면 서버에서 새로 수신한다.  )
        /// </summary>
        /// <param name="currentSaveLocationMapGuid">
        /// The current save location map guid.
        /// </param>
        public void LocationMapFileSync(Guid currentSaveLocationMapGuid)
        {
            var guidFromFile = MapControlRepository.GetLocationMapSyncGuid();

            if (guidFromFile.Equals(currentSaveLocationMapGuid) == false)
            {
                this.GetLocationMapData();

                this.UpdateAllMapLayout();
            }
        }

        /// <summary>
        /// CT 에 접속해서 저장된 Map Layout 을 Download 한다. 
        /// </summary>
        /// <returns>
        /// 성공 유무.
        /// </returns>
        private bool GetLocationMapData()
        {
            try
            {
                var url = UrlFactory.GetUrlE(
                    PlayerCommonsConfig.Instance.DataServiceUrl,
                    "GetMapLayout?",
                    CommonsConfig.Instance.ProjectID);
                var data = RestService.GetMessage(url);

                //File.WriteAllText(MapControlRepository.GetFilePath(), data);
                File.WriteAllText(MapControlRepository.FilePath.Path, data);
            }
            catch (Exception ex)
            {
                MainProcess.Log.Error(ex);
                return false;
            }

            return true;
        }

        /// <summary>
        ///  저장되어 있는 MapType 을 전체 멀티 그리드에 반영한다.
        /// </summary>
        private void UpdateAllMapLayout()
        {
            
        }
	}
}