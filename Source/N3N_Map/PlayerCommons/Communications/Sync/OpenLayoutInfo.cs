// --------------------------------------------------------------------------------------------------------------------
// <copyright file="OpenLayoutInfo.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   The open layout info.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.PlayerCommons.Communications.Sync
{
    using System;
    using System.Windows;

    using Innotive.InnoWatch.DLLs.ActionEffects;

    /// <summary>
    /// The open layout info.
    /// </summary>
    public class OpenLayoutInfo
    {
        #region Constants and Fields

        private string _displayAreaName;

        private double _duration;

        private string _effect;

        private Guid _layoutGuid;

        private Rect _rect;

        private double _startTime;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="OpenLayoutInfo"/> class.
        /// </summary>
        /// <param name="displayAreaName">
        /// The display area name.
        /// </param>
        /// <param name="layoutGuid">
        /// The layout guid.
        /// </param>
        public OpenLayoutInfo(string displayAreaName, Guid layoutGuid)
        {
            this._displayAreaName = displayAreaName;
            this._layoutGuid = layoutGuid;
            this._rect = new Rect(0, 0, 0, 0);
            this._effect = ActionEffects.None.ToString();
            this._startTime = 0;
            this._duration = 0;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="OpenLayoutInfo"/> class.
        /// </summary>
        /// <param name="displayAreaName">
        /// The display area name.
        /// </param>
        /// <param name="layoutGuid">
        /// The layout guid.
        /// </param>
        /// <param name="effect">
        /// The effect.
        /// </param>
        /// <param name="startTime">
        /// The start time.
        /// </param>
        /// <param name="duration">
        /// The duration.
        /// </param>
        /// <param name="rect">
        /// The rect.
        /// </param>
        public OpenLayoutInfo(
            string displayAreaName, Guid layoutGuid, ActionEffects effect, double startTime, double duration, Rect rect)
        {
            this._displayAreaName = displayAreaName;
            this._layoutGuid = layoutGuid;
            this._effect = effect.ToString();
            this._startTime = startTime;
            this._duration = duration;
            this._rect = rect;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets DisplayAreaName.
        /// </summary>
        public string DisplayAreaName
        {
            get
            {
                return this._displayAreaName;
            }
        }

        /// <summary>
        /// Gets Duration.
        /// </summary>
        public double Duration
        {
            get
            {
                return this._duration;
            }
        }

        /// <summary>
        /// Gets Effect.
        /// </summary>
        public string Effect
        {
            get
            {
                return this._effect;
            }
        }

        /// <summary>
        /// Gets LayoutGuid.
        /// </summary>
        public Guid LayoutGuid
        {
            get
            {
                return this._layoutGuid;
            }
        }

        /// <summary>
        /// Gets Rect.
        /// </summary>
        public Rect Rect
        {
            get
            {
                return this._rect;
            }
        }

        /// <summary>
        /// Gets StartTime.
        /// </summary>
        public double StartTime
        {
            get
            {
                return this._startTime;
            }
        }

        #endregion
    }
}