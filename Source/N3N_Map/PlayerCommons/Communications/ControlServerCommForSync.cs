﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ControlServerCommForSync.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Control Server Comm For Sync Class.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.PlayerCommons.Communications
{
    using System;
    using System.Collections.Generic;
    using System.Windows;
    using System.Windows.Threading;
    using Innotive.InnoWatch.Commons.Publics;
    using Innotive.InnoWatch.Commons.Utils;
    using Innotive.InnoWatch.CommonUtils.Log;
    using Innotive.InnoWatch.CommonUtils.Models;
    using Innotive.InnoWatch.CommonUtils.Models.Sync;
    using Innotive.InnoWatch.CommonUtils.Models.Sync.Content;
    using Innotive.InnoWatch.CommonUtils.Models.Sync.Control;
    using Innotive.InnoWatch.CommonUtils.Models.Sync.Multigrid;
    using Innotive.InnoWatch.PlayerCommons.Communications.SyncStrategy;
    using Innotive.InnoWatch.PlayerCommons.Configs;
    using Innotive.InnoWatch.PlayerCommons.DisplayAreas;

    /// <summary>
    /// Control Server Comm For Sync Class.
    /// </summary>
    public class ControlServerCommForSync
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ControlServerCommForSync"/> class.
        /// </summary>
        public ControlServerCommForSync()
        {
        }

        /// <summary>
        /// 스테이지 싱크를 위한 Guid 리스트.
        /// 등록된 Guid를 가진 스테이지(MultiGridControl)만 싱크를 받을 수 있음.
        /// </summary>
        private readonly List<Guid> syncGuidListForStage = new List<Guid>();

        /// <summary>
        /// 스테이지 싱크를 위하여 Guid를 등록.
        /// </summary>
        /// <param name="syncGuid">
        /// The sync guid.
        /// </param>
        public void AddSyncedGuid(Guid syncGuid)
        {
            if (this.syncGuidListForStage.Contains(syncGuid))
            {
                return;
            }

            this.syncGuidListForStage.Add(syncGuid);
        }

        /// <summary>
        /// 스테이지의 싱크 해제를 위하여 Guid를 제거.
        /// </summary>
        /// <param name="syncGuid">
        /// The sync guid.
        /// </param>
        public void RemoveSyncedGuid(Guid syncGuid)
        {
            this.syncGuidListForStage.Remove(syncGuid);
        }

        //CM쪽 Alarm Border를 보이거나 숨김
        //Contents Mode는 무조건 보임
        //Stage Mode는 Alarm Popup창에서 설정한 Stage만 Border를 보여줌
        private void ShowAlarmBorderForCM(BorderActionType borderActionType, string borderColor, List<Guid> overlayDisplayAreaSyncGuidListForCM)
        {
            //Console Content Mode쪽 DisplayArea는 Alarm Stroke을 무조건 보여줌 !!
            for (int index = 0; index < MainProcess.Instance.ProjectManager.DisplayAreaManager.Count; ++index)
            {
                var displayArea = MainProcess.Instance.ProjectManager.DisplayAreaManager.GetDisplayArea(index) as PlayerDisplayArea;
                if (displayArea != null)
                {
                    displayArea.ShowAlarmBorderControl(borderActionType, borderColor);
                }
            }

            //Console Stage Mode쪽 Overlay DisplayArea Alarm Border는 Alarm등록창에서 보일지 안보일지 설정을 함
            for (int i = 0; i < MainProcess.Instance.ProjectManager.OverlayDisplayAreaManager.GridCount; i++)
            {
                var overlayDisplayArea = MainProcess.Instance.ProjectManager.OverlayDisplayAreaManager.GetGridOverlayDisplayArea(i);
                if (overlayDisplayArea != null)
                {
                    //BorderActionType이 Disable이면 모든 Stage의 Border를 숨김
                    if (borderActionType == BorderActionType.Disable)
                    {
                        overlayDisplayArea.ShowAlarmBorderControl(BorderActionType.Disable, borderColor);
                    }
                    else
                    {
                        if (overlayDisplayAreaSyncGuidListForCM != null)
                        {
                            bool isExistSyncGuid = false;
                            for (int j = 0; j < overlayDisplayAreaSyncGuidListForCM.Count; j++)
                            {
                                Guid syncGuid = overlayDisplayAreaSyncGuidListForCM[j];
                               
                            }

                            //Alarm 등록창에서 해당 Stage쪽 Border Visible에 Check되어 있으면 Border를 보여줌
                            if (isExistSyncGuid)
                            {
                                overlayDisplayArea.ShowAlarmBorderControl(borderActionType, borderColor);
                            }
                            //Check되어 있지 않으면 무조건 Border를 숨김
                            else
                            {
                                overlayDisplayArea.ShowAlarmBorderControl(BorderActionType.Disable, borderColor);
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// 알람이 발생시 Player 의 화면 테두리에 강조 표시를 한다. 
        /// </summary>
        /// <param name="borderActionType">
        /// The border Action Type.
        /// </param>
        /// <param name="borderColor">
        /// The border Color.
        /// </param>
        public void SendAlarmStroke(BorderActionType borderActionType, string borderColor, List<Guid> overlayDisplayAreaSyncGuidForCM, List<Guid> overlayDisplayAreaSyncGuidForDP)
        {
            //Player쪽 Contents Mode DisplayArea의 Alarm Border는 항상 보여줌
            //Player쪽 Overlay DisplayArea의 Alarm Border에서 대한 설정은 SyncData에 같이 보냄
            var data = new AlarmStrokeSync
                {
                    BorderAction = (byte)borderActionType, 
                    BorderColor = borderColor, 
                    TargetDisplayAreaName = string.Empty, 
                    TargetLayoutGuid = Guid.Empty, 
                    SyncCommandType = SyncCommandType.AlarmStroke,
                    OverlayDisplayAreaSyncGuidList = overlayDisplayAreaSyncGuidForDP
                };

            //Command쪽 Alarm Border에 대한 처리 (Show/Hide)
            if (Application.Current.Dispatcher.CheckAccess())
            {
                this.ShowAlarmBorderForCM(borderActionType, borderColor, overlayDisplayAreaSyncGuidForCM);
            }
            else
            {
                Application.Current.Dispatcher.BeginInvoke(
                    new Action(() =>
                    this.ShowAlarmBorderForCM(borderActionType, borderColor, overlayDisplayAreaSyncGuidForCM)));
            }           

            //Sync Data를 Queue에 추가함 !!
            CommManager.Instance.SyncDataGroupingManager.Add(data);
        }

        /// <summary>
        /// 카메라 정보 새로고침 명령 보내기.
        /// </summary>
        public void SendCameraInformationRefresh()
        {
            // 메시지 보내기
            CommManager.Instance.SyncDataGroupingManager.Add(new CameraInformationRefreshSync());

            // 자기 자신 실행
            var sync = new SyncStrategyForCameraInformationRefresh();
            sync.DoSync(new CameraInformationRefreshSync());
        }

        /// <summary>
        /// send display mode changed.
        /// </summary>
        /// <param name="displayMode">
        /// The display mode.
        /// </param>
        public void SendDisplayModeChanged(ConsoleDisplayMode displayMode)
        {
            MainProcess.Log.Info("Console DisplayMode Changed, mode = " + displayMode.ToString());

            var data = new DisplayModeSync
                {
                    DisplayMode = displayMode, 
                    TargetDisplayAreaName = string.Empty, 
                    TargetLayoutGuid = Guid.Empty, 
                    SyncCommandType = SyncCommandType.DisplayMode
                };

            CommManager.Instance.SyncDataGroupingManager.Add(data);
        }

        /// <summary>
        /// Element 의 마우스 오버 상태를 전송 ( 마우스 오버시 버튼 색상 변경 등의 용도 )
        /// </summary>
        /// <param name="ownerLayoutSyncGuid">부모 레이아웃의 Sync Guid</param>
        /// <param name="elementSyncGuid">대상 Element 의 Sync Guid</param>
        /// <param name="isMouseOver">마우스 Over 상태</param>
        public void SendElementMouseOver(Guid ownerLayoutSyncGuid, Guid elementSyncGuid, bool isMouseOver)
        {
            // TODO : 성능상 문제가 있어서 기능을 막아둠 !!
            // 현재 객체내부에 Mouse가 올라가면 무조건 보내고 있음 !!
            // 객체 내부에서 변화가 일어난 경우 서버에 메세지를 전송하는 구조로 변경해야 함 !!

            /*
			ElementMouseOverData data = new ElementMouseOverData();
			data.ElementGuid = elementGuid;
			data.IsMouseOver = isMouseOver;

			// 싱크 구조 개선 (by jhlee)
			// SendSyncDataToServer(string.Empty, ownerLayoutGuid, SyncCommandType.ElementMouseOver, data);
			CommManager.Instance.SyncDataGroupingManager.Add(data);
			*/
        }

        /// <summary>
        /// send multi grid cell data.
        /// </summary>
        /// <param name="multiGridSyncGuid">
        /// The multi grid sync guid.
        /// </param>
        /// <param name="data">
        /// The data.
        /// </param>
        public void SendMultiGridCellSlideChangedData(Guid multiGridSyncGuid, MultiGridCellSlideChangedSync data)
        {
            data.TargetDisplayAreaName = string.Empty;
            data.TargetLayoutGuid = Guid.Empty;
            data.SyncCommandType = SyncCommandType.MultiGridCell;

            CommManager.Instance.SyncDataGroupingManager.Add(data);
        }

        /// <summary>
        /// send multi grid full screen data.
        /// </summary>
        /// <param name="multiGridSyncGuid">
        /// The multi grid sync guid.
        /// </param>
        /// <param name="cellSyncGuid">
        /// The cell sync guid.
        /// </param>
        public void SendMultiGridFullScreenData(Guid multiGridSyncGuid, Guid cellSyncGuid)
        {
            var data = new MultiGridFullScreenSync
                {
                    GridControlSyncGuid = multiGridSyncGuid, 
                    CellSyncGuid = cellSyncGuid, 
                    TargetDisplayAreaName = string.Empty, 
                    TargetLayoutGuid = Guid.Empty, 
                    SyncCommandType = SyncCommandType.MultiGridFullScreen
                };

            CommManager.Instance.SyncDataGroupingManager.Add(data);
        }

        /// <summary>
        /// send multi grid layout data.
        /// </summary>
        /// <param name="multiGridSyncGuid">
        /// The multi grid sync guid.
        /// </param>
        /// <param name="data">
        /// The data.
        /// </param>
        public void SendMultiGridLayoutData(Guid multiGridSyncGuid, MultiGridTotalElementSync data)
        {
            data.TargetDisplayAreaName = string.Empty;
            data.TargetLayoutGuid = Guid.Empty;
            data.SyncCommandType = SyncCommandType.MultiGrid;

            CommManager.Instance.SyncDataGroupingManager.Add(data);
            // data.Clear();
        }

        /// <summary>
        /// 패닝 멈춤 상태를 전송
        /// </summary>
        /// <param name="ownerLayoutSyncGuid">부모 Layout 의 Sync Guid</param>
        /// <param name="layoutSyncGuid">패닝이 끝난 상태가 발생한 Layout 의 Sync Guid</param>
        /// <param name="rect">움직인 영역</param>
        public void SendPanningEnd(Guid ownerLayoutSyncGuid, Guid layoutSyncGuid, Rect rect)
        {
            var data = new PanningEndSync
                {
                    LayoutGuid = layoutSyncGuid, 
                    Rect = InnoConvertUtil.ToRectangle(rect), 
                    TargetDisplayAreaName = string.Empty, 
                    TargetLayoutGuid = ownerLayoutSyncGuid, 
                    SyncCommandType = SyncCommandType.PanningEnd
                };

            CommManager.Instance.SyncDataGroupingManager.Add(data);
        }

        /// <summary>
        /// 패닝 상태를 전송
        /// </summary>
        /// <param name="layoutSyncGuid">패닝이 발생한 Layout 의 Sync Guid</param>
        /// <param name="rect">움직인 영역</param>
        /// <param name="ownerLayoutSyncGuid">부모 Layout 의 Sync Guid</param>
        public void SendSettingRect(Guid ownerLayoutSyncGuid, Guid layoutSyncGuid, Rect rect)
        {
            // PanningEnd Option이 설정되어 있는경우 MouseMove시의 Panning Message는 보내지 않음 !!
            if (PlayerCommonsConfig.Instance.SendPanningMessageOnlyPanningEnd)
            {
                return;
            }

            var data = new SettingRectSync
                {
                    LayoutGuid = layoutSyncGuid, 
                    Rect = InnoConvertUtil.ToRectangle(rect), 
                    TargetDisplayAreaName = string.Empty, 
                    TargetLayoutGuid = ownerLayoutSyncGuid, 
                    SyncCommandType = SyncCommandType.SettingRect
                };

            CommManager.Instance.SyncDataGroupingManager.Add(data);
        }

        /// <summary>
        /// 줌인이 발생
        /// </summary>
        /// <param name="ownerLayoutSyncGuid">부모 Layout Control</param>
        /// <param name="layoutSyncGuid">줌인이 발생한 Layout Control의 Sync Guid</param>
        /// <param name="destRect">최종으로 맞추어질 위치</param>
        public void SendZoomIn(Guid ownerLayoutSyncGuid, Guid layoutSyncGuid, Rect destRect)
        {
            var data = new ZoomInSync
                {
                    LayoutGuid = layoutSyncGuid, 
                    DesireRect = InnoConvertUtil.ToRectangle(destRect), 
                    TargetDisplayAreaName = string.Empty, 
                    TargetLayoutGuid = ownerLayoutSyncGuid, 
                    SyncCommandType = SyncCommandType.ZoomIn
                };


            CommManager.Instance.SyncDataGroupingManager.Add(data);
        }

        /// <summary>
        /// 대상 MapControl 의 Zoom Level 을 변경한다.
        /// </summary>
        /// <param name="mapControlSyncGuid">대상 MapControl 의 Sync Guid</param>
        /// <param name="zoomLevel">변경할 Zoom Level</param>
        public void SendMapControlZoomChanged(Guid mapControlSyncGuid, double zoomLevel, double centerLatitude, double centerLongitude)
        {
            var data = new MapControlZoomChangedSync
            {
                MapControlGuid = mapControlSyncGuid,
                SyncCommandType = SyncCommandType.MapControlZoomChanged,
                TargetDisplayAreaName = string.Empty,
                TargetLayoutGuid = Guid.Empty,
                ZoomLevel = zoomLevel,
                CenterLatitude = centerLatitude,
                CenterLongitude = centerLongitude
            };

            CommManager.Instance.SyncDataGroupingManager.Add(data);
        }

        /// <summary>
        /// 대상 MapControl 의 위치를 변경한다.
        /// </summary>
        /// <param name="mapControlSyncGuid">대상 MapControl 의 Sync Guid</param>
        /// <param name="mapLatitude">변경할 위도</param>
        /// <param name="mapLongitude">변경할 경도</param>
        public void SendMapControlPositionChangeEnd(Guid mapControlSyncGuid, double mapLatitude, double mapLongitude)
        {
            var data = new MapControlPositionChangeEndSync
            {
                MapControlGuid = mapControlSyncGuid,
                SyncCommandType = SyncCommandType.MapControlPositionChangeEnd,
                TargetDisplayAreaName = string.Empty,
                TargetLayoutGuid = Guid.Empty,
                Latitude = mapLatitude,
                Longtitude = mapLongitude
            };

            CommManager.Instance.SyncDataGroupingManager.Add(data);
        }

        /// <summary>
        /// 대상 MapControl 의 해당 Camera 를 전체 화면으로 변경한다. 
        /// </summary>
        /// <param name="mapControlSyncGuid">
        /// 대상 MapControl 의 Sync Guid
        /// </param>
        /// <param name="cameraId">
        /// The camera Id.
        /// </param>
        public void SendMapControlFullViewCamera(Guid mapControlSyncGuid, string cameraId, bool isFull)
        {
            var data = new MapControlFullViewCameraSync
                {
                    MapControlGuid = mapControlSyncGuid,
                    SyncCommandType = SyncCommandType.MapControlFullCamera,
                    CameraId = cameraId,
                    IsFull = isFull
                };

            CommManager.Instance.SyncDataGroupingManager.Add(data);
        }

        /// <summary>
        /// 줌아웃이 발생
        /// </summary>
        /// <param name="ownerLayoutSyncGuid">부모 Layout Control의 Guid</param>
        /// <param name="layoutSyncGuid">줌아웃이 발생한 Layout Control의 Sync Guid</param>
        /// <param name="destRect">최종으로 맞추어질 위치</param>
        public void SendZoomOut(Guid ownerLayoutSyncGuid, Guid layoutSyncGuid, Rect destRect)
        {
            var data = new ZoomOutSync
                {
                    LayoutGuid = layoutSyncGuid, 
                    DesireRect = InnoConvertUtil.ToRectangle(destRect), 
                    TargetDisplayAreaName = string.Empty, 
                    TargetLayoutGuid = ownerLayoutSyncGuid, 
                    SyncCommandType = SyncCommandType.ZoomOut
                };

            CommManager.Instance.SyncDataGroupingManager.Add(data);
        }

        /// <summary>
        /// Content에 저장된 Action의 Sync 를 맞추도록 함
        /// </summary>
        /// <param name="actionGuid">ActionProcess 의 Sync Guid</param>
        /// <param name="actionName">ActionProcess 의 Name</param>
        public void SendAction(Guid actionGuid, string actionName)
        {
            var data = new ActionSync
                {
                    ActionGuid = actionGuid, 
                    ActionName = actionName, 
                    TargetDisplayAreaName = string.Empty, 
                    TargetLayoutGuid = Guid.Empty, 
                    SyncCommandType = SyncCommandType.Action
                };

            CommManager.Instance.SyncDataGroupingManager.Add(data);
        }

        public void SendRequestLastMultiGridSyncData(Guid syncGuid)
        {
            var requestData = RequestLastMultiGridSyncData.MakeInstance(syncGuid);

            // DP 에서도 요청이 가능함. ( 별도 수행 )
            if (CommManager.Instance.AppSyncClient != null)
            {
                CommManager.Instance.AppSyncClient.SendDataObject(requestData);
            }
            
        }

        /// <summary>
        /// Sync 서버에게 싱크 데이터 보내기.
        /// </summary>
        /// <param name="syncList">
        /// The sync List.
        /// </param>
        public void SendSyncDataToServer(BaseSyncList syncList)
        {
            // 서버로 메세지를 보내는 기능은 iCommand에만 있음.
            if (MainProcess.Instance.ProgramType != ProgramType.iCommand)
            {
                for (int i = syncList.Count - 1; i >= 0; i--)
                {
                    var sync = syncList[i];

                    if (sync is IDisposable)
                    {
                        (sync as IDisposable).Dispose();
                    }
                }

                syncList.Clear();
                syncList = null;

                InnotiveDebug.Trace("[Sync] 콘솔이 아니라 싱크를 보내지 않음.");
                return;
            }

            if (CommManager.Instance.ControlServer.SendEnabled == false)
            {
                for (int i = syncList.Count - 1; i >= 0; i--)
                {
                    var sync = syncList[i];

                    if (sync is IDisposable)
                    {
                        (sync as IDisposable).Dispose();
                    }
                }

                syncList.Clear();
                syncList = null;

                InnotiveDebug.Trace("[Sync] 싱크를 보내지 않도록 설정되어 있음.");
                return;
            }

            if (syncList != null)
            {
                InnotiveDebug.Trace(string.Format("[Sync] SendSyncDataToServer Start : List Size = {0}", syncList.Count));

                //if (CommManager.Instance.IsSyncMonitorWorking)
                //{
                //    CommManager.Instance.Syncss.Add("Send", "Group", "Adding Group : " + syncList.Count);
                //}

                CommManager.Instance.ControlServer.SendManager.Add(
                    new Action(
                        () =>
                            {
                                //if (CommManager.Instance.IsSyncMonitorWorking)
                                //{
                                //    CommManager.Instance.Syncss.Add(
                                //        "Send", "Group", "Start Sending Group : " + syncList.Count);
                                //}

                                if (CommManager.Instance.AppSyncClient != null)
                                {
                                    CommManager.Instance.AppSyncClient.SendDataObject(syncList);
                                }

                                //if (CommManager.Instance.IsSyncMonitorWorking)
                                //{
                                //    CommManager.Instance.Syncss.Add(
                                //        "Send", "Group", "End Sending Group : " + syncList.Count);
                                //}

                                for (int i = syncList.Count - 1; i >= 0; i--)
                                {
                                    var sync = syncList[i];

                                    if (sync is IDisposable)
                                    {
                                        (sync as IDisposable).Dispose();
                                    }

                                    sync = null;
                                }

                                syncList.Clear();
                                syncList = null;
                            }));

                InnotiveDebug.Trace(string.Format("[Sync] SendSyncDataToServer End : List Size = {0}", syncList.Count));

                // syncList.Clear();
            }
        }

        public void SendLocationMapSaveSync(LocationMapSaveSync data)
        {
            CommManager.Instance.SyncDataGroupingManager.Add(data);
        }

        /// <summary>
        /// 서버에서 오는 데이터 파싱.
        /// </summary>
        /// <param name="sync">
        /// 싱크 데이터.
        /// </param>
        public void RecvSyncDataFromServer(BaseSync sync)
        {
            if (sync == null)
            {
                return;
            }

            if (CommManager.Instance.IsSyncMonitorWorking)
            {
                CommManager.Instance.Syncss.Add("Recv", sync.SyncCommandType.ToString(), "Parsing");
            }

            // 스테이지 싱크 사용 여부 검사.
            if (MainProcess.Instance.ProgramType == ProgramType.iCommand)
            {
                var syncGuid = Guid.Empty;

                // 싱크 Guid 추출
                switch (sync.SyncCommandType)
                {
                    case SyncCommandType.MultiGrid:
                        var multiGridSync = sync as MultiGridTotalElementSync;
                        if (multiGridSync != null)
                        {
                            syncGuid = multiGridSync.GridControlSyncGuid;
                        }

                        break;

                    case SyncCommandType.MultiGridFullScreen:
                        var multiGridSync2 = sync as MultiGridFullScreenSync;
                        if (multiGridSync2 != null)
                        {
                            syncGuid = multiGridSync2.GridControlSyncGuid;
                        }

                        break;

                    case SyncCommandType.MultiGridCell:
                        var multiGridSync3 = sync as MultiGridCellSlideChangedSync;
                        if (multiGridSync3 != null)
                        {
                            syncGuid = multiGridSync3.GridControlSyncGuid;
                        }
                        break;
                }

                // 싱크 Guid 검사
                if (!this.syncGuidListForStage.Contains(syncGuid))
                {
                    // 스테이지 싱크 OFF
                    return;
                }
            }
            
            CommManager.Instance.ControlServer.ReceiveManager.Add(
                delegate
                    {
                        var syncManager = new SyncStrategyManager(sync);
                        InnotiveDebug.Trace("[3. Sync_Act(BeginInvoke)] " + sync.SyncCommandType);
                        if (CommManager.Instance.IsSyncMonitorWorking)
                        {
                            CommManager.Instance.Syncss.Add("Recv", sync.SyncCommandType.ToString(), "Execute");
                        }

                        // IPQ와 Camera 업데이트를 잠시 멈춤 (방어적인 코드)    
                        AsyncWorker.SyncQueueIsBusy = true;

                        syncManager.DoSync();

                        // UI 작업 끝날때까지 대기한다.
                        if (MainProcess.Instance.ProgramType == ProgramType.iDisplay)
                        {
                            MainProcess.Instance.MainWindow.Dispatcher.Invoke(
                                new Action(delegate { }), DispatcherPriority.Background);
                        }

                        // IPQ와 Camera 업데이트를 시작 (방어적인 코드)
                        AsyncWorker.SyncQueueIsBusy = false;

                        if (sync is IDisposable)
                        {
                            (sync as IDisposable).Dispose();
                        }

                        sync = null;
                    }, 
                sync.SyncCommandType.ToString(), 
                sync);
        }
    }
}