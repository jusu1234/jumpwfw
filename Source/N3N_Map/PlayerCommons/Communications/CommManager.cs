// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CommManager.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   통신 관리자 클래스.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using Innotive.InnoWatch.CommonUtils.Log;
using Innotive.InnoWatch.Commons.BaseControls;

namespace Innotive.InnoWatch.PlayerCommons.Communications
{
    using System.ServiceModel;
    using Commons.Publics;
    using CommonUtils.Network.NetworkCore;
    using Configs;
    using Models;

    /// <summary>
    /// 통신 관리자 클래스.
    /// </summary>
    public sealed class CommManager
    {
        #region Constructors and Destructors

        /// <summary>
        /// Prevents a default instance of the <see cref="CommManager"/> class from being created. 
        /// </summary>
        private CommManager()
        {
            // Singleton 생성자.

            this.IsSyncMonitorWorking = false;

            this.ControlServer = new ControlServerComm();
            this.SyncDataGroupingManager = new SyncDataGroupingManager();
            this.SyncDataGroupingManager.Start();
            
            this.AppSyncClient = null;

            if (PlayerCommonsConfig.Instance.UseAppSyncServer)
            {
                this.AppSyncClient =
                    new InnoSyncClient(
                                "0.0.0.0",
                                0,
                                PlayerCommonsConfig.Instance.AppSyncServerIp,
                                PlayerCommonsConfig.Instance.AppSyncServerPort,
                                null,
                                Public.GetProgramType(),
                                this.ControlServer)
                    {
                        UseReconnect = true
                    };

                this.AppSyncClient.Connect();
            }
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets Instance.
        /// </summary>
        public static CommManager Instance
        {
            get
            {
                return Nested<CommManager>.Instance;
            }
        }

        /// <summary>
        /// Gets ControlServer.
        /// 컨트롤 서버 통신 인터페이스.
        /// </summary>
        public ControlServerComm ControlServer { get; private set; }

        /// <summary>
        /// Gets SyncDataGroupingManager.
        /// 싱크 데이터 그룹 관리자.
        /// </summary>
        public SyncDataGroupingManager SyncDataGroupingManager { get; private set; }
        
        /// <summary>
        /// Gets AppSyncClient.
        /// App Sync Client Core.
        /// </summary>
        public InnoSyncClient AppSyncClient { get; private set; }

        #endregion

        /// <summary>
        /// </summary>
        public void Release()
        {
            if (this.AppSyncClient != null)
            {
                this.AppSyncClient.Dispose();
                this.AppSyncClient = null;
                InnotiveDebug.Log.Info("[SyncTrace] AppSyncClient is Disposed.");
            }
            
            if (this.SyncDataGroupingManager != null)
            {
                //this.SyncDataGroupingManager.Stop();
                this.SyncDataGroupingManager.Dispose();
            }

            if (this.ControlServer != null)
            {
                this.ControlServer.Dispose();
            }
        }
        
        #region Sync Monitor

        /// <summary>
        /// 싱크 목록 리스트.
        /// </summary>
        public readonly Syncss Syncss = new Syncss();

        /// <summary>
        /// Sync Monitor 사용 여부.
        /// </summary>
        public bool IsSyncMonitorWorking { get; set; }

        /// <summary>
        /// Start Sync Monitor.
        /// </summary>
        public void StartSyncMonitor()
        {
            this.Syncss.Clear();
            this.IsSyncMonitorWorking = true;
        }

        /// <summary>
        /// Stop Sync Monitor.
        /// </summary>
        public void StopSyncMonitor()
        {
            this.IsSyncMonitorWorking = false;
            this.Syncss.Clear();
        }

        #endregion // Sync Monitor
    }
}