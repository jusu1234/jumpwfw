﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ContentSyncGroupingManager.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   컨텐츠 싱크 그룹 관리자.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.PlayerCommons.Communications
{
    using System;
    using System.Timers;
    using System.Collections.Concurrent;
    using Innotive.InnoWatch.CommonUtils.Models.Sync;
    using Innotive.InnoWatch.CommonUtils.Models.Sync.Control;
    using Innotive.InnoWatch.PlayerCommons.Configs;
    using Innotive.InnoWatch.CommonUtils.Log;

    /// <summary>
    /// 통신 연결 관리자 클래스.
    /// </summary>
    public class SyncDataGroupingManager : IDisposable
    {
        #region Constants and Fields
        
        /// <summary>
        /// 싱크 리스트 속성.
        /// </summary>
        private ConcurrentQueue<BaseSync> baseSyncQueue = new ConcurrentQueue<BaseSync>();

        /// <summary>
        /// 메인 쓰레드.
        /// 스레드 풀에서 실행되는 .net 스레드 타이머로 교체(성능 향상).
        /// </summary>
        private Timer mainTimer = null;

        /// <summary>
        /// 싱크 보내기 간격 (정상 상태 : 큐가 10개 미만)
        /// </summary>
        private const int SyncGroupSendingIntervalForNormal = 20;

        ///// <summary>
        ///// 싱크 보내기 간격 (천천히 보내기 상태 : 큐가 10개 이상)
        ///// </summary>
        //private const int SyncGroupSendingIntervalForCoolDown = 200;
        
        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="SyncDataGroupingManager"/> class. 
        /// </summary>
        public SyncDataGroupingManager()
        {
            this.mainTimer = new Timer(SyncGroupSendingIntervalForNormal);
            this.mainTimer.Elapsed += MainTimerOnElapsed;
        }

        private void MainTimerOnElapsed(object sender, ElapsedEventArgs elapsedEventArgs)
        {
            this.Do();
        }
        
        #endregion

        #region Properies
        
        #endregion

        #region Public Methods

        /// <summary>
        /// 항목 추가.
        /// </summary>
        /// <param name="sync">싱크 데이터.</param>
        public void Add(BaseSync sync)
        {
            if (PlayerCommonsConfig.Instance.UseAppSyncServer == false)
            {
                InnotiveDebug.Log.Error("[SyncTrace] Use AppSyncServer is false.");
                return;
            }

            if (CommManager.Instance.IsSyncMonitorWorking)
            {
                CommManager.Instance.Syncss.Add("Send", sync.SyncCommandType.ToString(), "Adding");
            }

            // PanningEnd Option이 설정되어 있는경우 MouseMove시의 Panning Message는 보내지 않음 !!
            if (PlayerCommonsConfig.Instance.SendPanningMessageOnlyPanningEnd)
            {
                if (sync is SettingRectSync)
                {
                    InnotiveDebug.Log.Info("[SyncTrace] Sync is SettingRect.");
                    return;
                }
            }

            this.baseSyncQueue.Enqueue(sync);            

            // 너무 많은 디버깅 정보. 삭제대기 - by shwlee.
            //InnotiveDebug.Log.Info(string.Format("[SyncTrace] baseSyncQueue Enqueue. count : {0}", this.baseSyncQueue.Count));
        }
        
        /// <summary>
        /// 관리자 시작.
        /// </summary>
        public void Start()
        {
            try
            {
                if (PlayerCommonsConfig.Instance.UseAppSyncServer == false)
                {
                    return;
                }

                this.mainTimer.AutoReset = false;
                this.mainTimer.Enabled = true;
                //InnotiveDebug.Log.Info("[SyncTrace] Manager Start.");
            }
            catch (Exception ex)
            {
                InnotiveDebug.Log.Error(string.Format("[SyncTrace] Manager Start Failed. : {0}", ex));
            }
        }

        /// <summary>
        /// 관리자 종료.
        /// </summary>
        public void Stop()
        {
            if (PlayerCommonsConfig.Instance.UseAppSyncServer == false)
            {
                return;
            }

            try
            {
                if (this.mainTimer != null)
                {
                    this.mainTimer.Stop();
                    //InnotiveDebug.Log.Error("[SyncTrace] Manager Stopped.");
                }
            }
            catch (Exception ex)
            {
                MainProcess.Log.Error("[SyncTrace] mainTimer Stop Exception!", ex);

                if (this.mainTimer != null)
                {
                    this.mainTimer.Elapsed -= MainTimerOnElapsed;
                    this.mainTimer = null;
                }

                this.mainTimer = new Timer(SyncGroupSendingIntervalForNormal);
                this.mainTimer.Elapsed += MainTimerOnElapsed;
            }
        }

        #endregion

        #region Methods
        
        /// <summary>
        /// 주 작업.
        /// </summary>
        private void Do()
        {
            try
            {
                this.Stop();

                try
                {
                    if (this.baseSyncQueue.Count > 0)
                    {
                        // 너무 많은 디버깅 정보. 삭제대기 - by shwlee.
                        //InnotiveDebug.Log.Info("[SyncTrace] Sync 루틴 진입 !!!");

                        var localBaseSyncList = new BaseSyncList();

                        while (!this.baseSyncQueue.IsEmpty)
                        {
                            BaseSync localvalue = null;

                            if (this.baseSyncQueue.TryDequeue(out localvalue))
                            {
                                if (localvalue != null)
                                {
                                    localBaseSyncList.Add(localvalue);
                                }
                            }
                        }

                        CommManager.Instance.ControlServer.SyncHelper.SendSyncDataToServer(localBaseSyncList);

                        // localBaseSyncList.Clear();
                        // 너무 많은 디버깅 정보. 삭제대기 - by shwlee.
                        //InnotiveDebug.Log.Info("[SyncTrace] Sync 루틴 통과 계속 진행!!!");
                    }
                }
                catch (Exception ex)
                {
                    MainProcess.Log.Error("ContentSyncGroupingManager Do Exception!", ex);
                }
            }
            catch (Exception ex)
            {
                MainProcess.Log.Error("[SyncTrace] mainTimer Do Exception!", ex);
            }
            finally
            {
                this.Start();
            }
        }

        #endregion

        public void Dispose()
        {
            this.Stop();
            this.mainTimer.Elapsed -= MainTimerOnElapsed;
            this.baseSyncQueue = null;

            MainProcess.Log.Info("[SyncTrace] SyncDataGroupingManager Dispose!");
        }
    }
}