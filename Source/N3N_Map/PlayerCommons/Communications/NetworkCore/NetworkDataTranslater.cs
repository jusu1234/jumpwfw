﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Innotive.Communication.ControlServerMsg;
using Innotive.InnoWatch.PlayerCommons.Communications.Models;

namespace Innotive.InnoWatch.PlayerCommons.Communications.NetworkCore
{
    public interface INetworkDataTranslater
    {
        string ToString(byte [] data);
        byte[] ToByte(object data);
        object ToObject(byte[] data);
    }

    public class UTF8Translater : INetworkDataTranslater
    {
        public string ToString(byte[] data)
        {
            return Encoding.UTF8.GetString(data);
        }

        public byte[] ToByte(object data)
        {
            return Encoding.UTF8.GetBytes((string)data);
        }

        public object ToObject(byte[] data)
        {
            return Encoding.UTF8.GetString(data);
        }
    }

    public class BaseSyncTranslater : INetworkDataTranslater
    {
        public string ToString(byte[] data)
        {
            string result = string.Empty;

            using (MemoryStream ms = new MemoryStream())
            {
                ms.Write(data, SyncNetworkBase.HEADER_SIZE, data.Length - SyncNetworkBase.HEADER_SIZE);
                BaseSyncList baseSyncList = SyncDataConverter.ArrayByteToBaseSyncList(ms.ToArray());

                foreach (BaseSync sync in baseSyncList)
                {
                    if (sync != null)
                    {
                        if (sync is ActionSyncData)
                            result += "(Action)" + (sync as ActionSyncData).ActionName + " ";
                        else
                            result += sync.SyncCommandType.ToString() + " ";
                    }
                    else
                    {
                        System.Diagnostics.Debug.WriteLine("[AppSyncNetwork] BaseSyncTranslater.ToString() Sync is null in BaseSyncList ");
                    }
                }
            }

            return result;
        }

        public byte[] ToByte(object data)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                byte[] dataArray = SyncDataConverter.BaseSyncListToByteArray((BaseSyncList)data);
                byte[] commandArray = SyncNetworkBase.Encoding.GetBytes(SyncNetworkBase.COMMAND_SYNCLIST);
                ms.Write(commandArray, 0, commandArray.Length);
                byte[] timeArray = SyncNetworkBase.Encoding.GetBytes(SyncNetworkBase.GetTimeString(DateTime.Now));
                ms.Write(timeArray, 0, timeArray.Length);
                int size = dataArray.Length;
                byte[] sizeArray = BitConverter.GetBytes(size);
                ms.Write(sizeArray, 0, sizeArray.Length);
                ms.Write(dataArray, 0, size);

                return ms.ToArray();
            }
        }

        public object ToObject(byte[] data)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                ms.Write(data, SyncNetworkBase.HEADER_SIZE, data.Length - SyncNetworkBase.HEADER_SIZE);
                return SyncDataConverter.ArrayByteToBaseSyncList(ms.ToArray());
            }
        }
    }

    public class CustomTranslater : INetworkDataTranslater
    {
        public string ToString(byte[] data)
        {
            string result = string.Empty;

            using (MemoryStream ms = new MemoryStream())
            {
                ms.Write(data, SyncNetworkBase.HEADER_SIZE, data.Length - SyncNetworkBase.HEADER_SIZE);
                CustomData customData = CustomDataConverter.ArrayByteToCustomData(ms.ToArray());

                switch (customData.DataType)
                {
                    case CustomDataType.ClientIDData :
                        result = customData.DataType.ToString();
                        break;
                    case CustomDataType.HandShakingData:
                        result = customData.DataType.ToString();
                        break;
                    default:
                        result = "Unknown CustomData Type";
                        break;
                }
            }

            return result;
        }

        public byte[] ToByte(object data)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                byte[] dataArray = CustomDataConverter.CustomDataToByteArray((CustomData)data);
                byte[] command = SyncNetworkBase.Encoding.GetBytes(SyncNetworkBase.COMMAND_CUSTOM);
                ms.Write(command, 0, command.Length);
                byte[] timeArray = SyncNetworkBase.Encoding.GetBytes(SyncNetworkBase.GetTimeString(DateTime.Now));
                ms.Write(timeArray, 0, timeArray.Length);
                int size = dataArray.Length;
                byte[] sizeArray = BitConverter.GetBytes(size);
                ms.Write(sizeArray, 0, sizeArray.Length);
                ms.Write(dataArray, 0, size);
                return ms.ToArray();
            }
        }

        public object ToObject(byte[] data)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                ms.Write(data, SyncNetworkBase.HEADER_SIZE, data.Length - SyncNetworkBase.HEADER_SIZE);
                return CustomDataConverter.ArrayByteToCustomData(ms.ToArray());
            }
        }
    }

    public class CommandTranslater : INetworkDataTranslater
    { 
        private CustomTranslater customTranslater = new CustomTranslater();
        private BaseSyncTranslater syncTranslater = new BaseSyncTranslater();

        public string ToTimeString(byte[] data)
        {
            return SyncNetworkBase.Encoding.GetString(data, SyncNetworkBase.COMMAND_SIZE, SyncNetworkBase.TIMESTR_SIZE);
        }

        public DateTime ToDateTime(byte[] data)
        {
            return SyncNetworkBase.GetDateTime(SyncNetworkBase.Encoding.GetString(data, SyncNetworkBase.COMMAND_SIZE, SyncNetworkBase.TIMESTR_SIZE));
        }

        public string ToString(byte[] data)
        {
            string result = "";

            try
            {
                string command = SyncNetworkBase.Encoding.GetString(data, 0, SyncNetworkBase.COMMAND_SIZE);

                if (command.CompareTo(SyncNetworkBase.COMMAND_SYNCLIST) == 0)
                {
                    result = syncTranslater.ToString(data);
                }
                else if (command.CompareTo(SyncNetworkBase.COMMAND_CUSTOM) == 0)
                {
                    result = customTranslater.ToString(data);
                }
                else
                {
                    result = "Unknown Command";
                }
            }
            catch (Exception ex)
            {
                SyncLogWriter.GetLog().Error("[AppSyncServer] CommandTranslater Error =" + ex.Message); 
            }

            return result;
        }

        public byte[] ToByte(object data)
        {
            byte[] result = null;

            try
            {
                if (data is BaseSyncList)
                {
                    result = syncTranslater.ToByte(data);
                }
                else if (data is CustomData)
                {
                    result = customTranslater.ToByte(data);
                }
                else
                {
                    result = null;
                }
            }
            catch (Exception ex)
            {
                SyncLogWriter.GetLog().Error("[AppSyncServer] CommandTranslater Error =" + ex.Message); 
            }

            return result;
        }

        public object ToObject(byte[] data)
        {
            object result = null;

            try
            {
                string command = SyncNetworkBase.Encoding.GetString(data, 0, SyncNetworkBase.COMMAND_SIZE);

                if (command.CompareTo(SyncNetworkBase.COMMAND_SYNCLIST) == 0)
                {
                    result = syncTranslater.ToObject(data);
                }
                else if (command.CompareTo(SyncNetworkBase.COMMAND_CUSTOM) == 0)
                {
                    result = customTranslater.ToObject(data);
                }
                else
                {
                    result = null;
                }
            }
            catch (Exception ex)
            {
                SyncLogWriter.GetLog().Error("[AppSyncServer] CommandTranslater Error =" + ex.Message); 
            }

            return result;
        }
    }
}
