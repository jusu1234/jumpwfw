// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SyncDataConverter.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   SyncData Converter.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.PlayerCommons.Communications.NetworkCore
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Runtime.Serialization.Formatters.Binary;
    using Innotive.Communication.ControlServerMsg;
    using Innotive.InnoWatch.PlayerCommons.Communications.Models;
    
    internal class DataConverter
    {
        // MemoryStream을 Object로 변환
        static public Object MemoryStreamToObject(MemoryStream ms)
        {
            BinaryFormatter binFmtr = new BinaryFormatter();

            ms.Position = 0;
            Object obj = binFmtr.Deserialize(ms) as Object;

            return obj;
        }

        // MemoryStream을 byte[]로 변환
        static public byte[] MemoryStreamToByte(MemoryStream ms)
        {
            ms.Position = 0;
            byte[] buffer = new byte[ms.Length];
            ms.Read(buffer, 0, (int)ms.Length);

            return buffer;
        }

        // MemoryStream을 Base64String로 변환
        static public string MemoryStreamToBase64String(MemoryStream ms)
        {
            ms.Position = 0;
            byte[] buffer = new byte[ms.Length];
            ms.Read(buffer, 0, (int)ms.Length);
            string base64EncodedString = Convert.ToBase64String(buffer);

            return base64EncodedString;
        }

        // Object를 MemoryStream으로 변환
        static public MemoryStream ObjectToMemoryStream(Object obj)
        {
            BinaryFormatter binFmtr = new BinaryFormatter();
            MemoryStream ms = new MemoryStream();

            binFmtr.Serialize(ms, obj);

            ms.Position = 0;
            byte[] buffer = new byte[ms.Length];
            ms.Read(buffer, 0, (int)ms.Length);
            ms.Position = 0;

            return ms;
        }

        // Object를 byte[]로 변환
        static public byte[] ObjectToByte(Object obj)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                BinaryFormatter binFmtr = new BinaryFormatter();

                binFmtr.Serialize(ms, obj);

                ms.Position = 0;
                byte[] buffer = new byte[ms.Length];
                ms.Read(buffer, 0, (int)ms.Length);
            
                return buffer;
            }
        }

        // Object를 Base64String로 변환
        static public string ObjectToBase64String(Object obj)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                BinaryFormatter binFmtr = new BinaryFormatter();

                binFmtr.Serialize(ms, obj);

                ms.Position = 0;
                byte[] buffer = new byte[ms.Length];
                ms.Read(buffer, 0, (int)ms.Length);


                string base64EncodedString = Convert.ToBase64String(buffer);

                return base64EncodedString;
            }
        }


        // byte[]를 MemoryStream 으로 변환
        static public MemoryStream ByteToMemoryStream(byte[] buffer)
        {
            MemoryStream ms = new MemoryStream();
            ms.Write(buffer, 0, buffer.Length);
            ms.Position = 0;

            return ms;
        }

        // byte[]를 Object로 변환
        static public Object ByteToObject(byte[] buffer)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                BinaryFormatter binFmtr = new BinaryFormatter();

                ms.Write(buffer, 0, buffer.Length);
                ms.Position = 0;
                Object obj = binFmtr.Deserialize(ms) as Object;
                return obj;
            }
        }

        // byte[]를 Base64String 으로 변환
        static public string ByteToBase64String(byte[] buffer)
        {
            string base64EncodedString = Convert.ToBase64String(buffer);
            return base64EncodedString;
        }

        // Base64String을 MemoryStream로 변환
        static public MemoryStream Base64StringToMemoryStream(string base64EncodedString)
        {
            MemoryStream ms = new MemoryStream();

            byte[] buffer = Convert.FromBase64String(base64EncodedString);

            ms.Write(buffer, 0, buffer.Length);
            ms.Position = 0;

            return ms;
        }

        // Base64String을 Object로 변환
        static public Object Base64StringToObject(string base64EncodedString)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                BinaryFormatter binFmtr = new BinaryFormatter();

                byte[] buffer = Convert.FromBase64String(base64EncodedString);

                ms.Write(buffer, 0, buffer.Length);
                ms.Position = 0;
                Object obj = binFmtr.Deserialize(ms) as Object;

                return obj;
            }
        }

        // Base64String을 byte[]로 변환
        static public byte[] Base64StringToByte(string base64EncodedString)
        {
            return Convert.FromBase64String(base64EncodedString);
        }
    }

    public class SyncDataConverter
    {
        public static byte [] BaseSyncListToByteArray(BaseSyncList syncList)
        {
            return DataConverter.ObjectToByte(syncList);
        }

        public static BaseSyncList ArrayByteToBaseSyncList(byte [] syncByteArray)
        {
            return (BaseSyncList)DataConverter.ByteToObject(syncByteArray);
        }
    }

    public class CustomDataConverter
    {
        public static byte[] CustomDataToByteArray(CustomData syncList)
        {
            return DataConverter.ObjectToByte(syncList);
        }

        public static CustomData ArrayByteToCustomData(byte[] syncByteArray)
        {
            return (CustomData)DataConverter.ByteToObject(syncByteArray);
        }
    }
}
