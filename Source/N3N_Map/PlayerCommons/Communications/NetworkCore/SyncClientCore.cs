// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SyncClientCore.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Sync Client Core
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.PlayerCommons.Communications.NetworkCore
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Text;
    using System.Timers;
    using System.Threading;
    using System.Windows.Threading;
    using Innotive.Communication.ControlServerMsg;
    using Innotive.InnoWatch.Commons.Publics;
    using Innotive.InnoWatch.PlayerCommons.Communications.EDAS;
    using Innotive.InnoWatch.PlayerCommons.Communications.Models;
    
    public interface ISyncClientUI
    {
        void SendData(byte[] data);
        void ReceiveData(byte[] data);
        void OnConnected();
        void OnDisConnected();
        void OnError(Exception ex, string msg);
        void OnUserException(IWorkingSocket sock, Exception ex);
    }

    public class DummyClientUI : ISyncClientUI
    {
        public void SendData(byte[] data) { }
        public void ReceiveData(byte[] data) { }
        public void OnConnected() { }
        public void OnDisConnected() { }
        public void OnError(Exception ex, string msg) { }
        public void OnUserException(IWorkingSocket sock, Exception ex) { }
    }

    public class DebugTraceUI : ISyncClientUI
    {
        CommandTranslater dataTranslater = new CommandTranslater();

        public void SendData(byte[] data)
        {
            string debugString = SyncLogWriter.MakeLogString("AppSyncClient", "OnSendData Data =" + dataTranslater.ToString(data));
            System.Diagnostics.Debug.WriteLine(debugString);
            MainProcess.Log.Info(debugString);
        }

        public void ReceiveData(byte[] data)
        {
            DateTime nowTime = DateTime.Now;
            DateTime sendedTime = dataTranslater.ToDateTime(data);
            TimeSpan delayTime = nowTime - sendedTime;
            string debugString = SyncLogWriter.MakeLogString("AppSyncClient", "OnReceiveData Data =" + dataTranslater.ToString(data) + " SendedTime = " + dataTranslater.ToTimeString(data) + "delayTime = " + delayTime.TotalMilliseconds);

            System.Diagnostics.Debug.WriteLine(debugString);
            MainProcess.Log.Info(debugString);

            if (delayTime.TotalMilliseconds < 50)
            {
                System.Diagnostics.Debug.WriteLine("[AppSyncDelay] delayTime < 50 delaytime = " + delayTime.TotalMilliseconds.ToString());
            }
            else if (delayTime.TotalMilliseconds < 100)
            {
                System.Diagnostics.Debug.WriteLine("[AppSyncDelay] delayTime < 100 delaytime = " + delayTime.TotalMilliseconds.ToString());
            }
            else if (delayTime.TotalMilliseconds < 200)
            {
                System.Diagnostics.Debug.WriteLine("[AppSyncDelay] delayTime < 200 delaytime = " + delayTime.TotalMilliseconds.ToString());
            }
            else if (delayTime.TotalMilliseconds < 500)
            {
                System.Diagnostics.Debug.WriteLine("[AppSyncDelay] delayTime < 500 delaytime = " + delayTime.TotalMilliseconds.ToString());
            }
            else if (delayTime.TotalMilliseconds < 1000)
            {
                System.Diagnostics.Debug.WriteLine("[AppSyncDelay] delayTime < 1000 delaytime = " + delayTime.TotalMilliseconds.ToString());
            }
            else
            {
                System.Diagnostics.Debug.WriteLine("[AppSyncDelay] delayTime >= 1000 delaytime = " + delayTime.TotalMilliseconds.ToString());
            }
        }

        public void OnConnected()
        {
            MainProcess.Log.Info("[AppSyncClient] OnConnect!!");
            System.Diagnostics.Debug.WriteLine(SyncLogWriter.MakeLogString("AppSyncClient", "OnConnect!!"));
        }

        public void OnDisConnected()
        {
            MainProcess.Log.Info("[AppSyncClient] OnDisConnect!!");
            System.Diagnostics.Debug.WriteLine(SyncLogWriter.MakeLogString("AppSyncClient", "OnDisConnect!!"));
        }

        public void OnError(Exception ex, string msg)
        {
            string s = "";

            if (ex != null) s += "ex.message = " + ex.Message;
            s += ", message = " + msg;

            if (ex != null)
                MainProcess.Log.Error("[AppSyncClient] OnException!!! " + s, ex);
            else
                MainProcess.Log.Error("[AppSyncClient] OnException!!! " + s);

            System.Diagnostics.Debug.WriteLine(SyncLogWriter.MakeLogString("AppSyncClient", s));
        }

        public void OnUserException(IWorkingSocket sock, Exception ex)
        {
            if (ex != null)
            {
                System.Diagnostics.Debug.WriteLine(SyncLogWriter.MakeLogString("AppSyncClient", ex.Message));
                MainProcess.Log.Error("[AppSyncClient] OnUserException!!! ", ex);
            }
            else
                MainProcess.Log.Error("[AppSyncClient] OnUserException!!! ");
        }
    }

    public class SyncClientCore : IDisposable
    {
        protected ISyncClientUI syncClientUI = new DebugTraceUI();

        private string srcIp;
        private int srcPort;
        
        private string dstIp;
        private int dstPort;

        protected InnoAsyncSocket sock = null;
        protected bool isConnecting = false;
        protected bool isDestroying = false;
        
        public SyncClientCore(string srcIp, int srcPort, string dstIp, int dstPort, ISyncClientUI syncClientUI)
        {
            this.srcIp = srcIp;
            this.srcPort = srcPort;

            this.dstIp = dstIp;
            this.dstPort = dstPort;

            if (syncClientUI != null) this.syncClientUI = syncClientUI;

            isConnecting = false;
        }

        public virtual void Dispose()
        {
            this.isDestroying = true;
            this.DestroySocket();
        }

        private void DestroySocket()
        {
            try
            {
                this.sock.Disconnect();
                if (this.sock.Processor != null)
                {
                    (this.sock.Processor as UseProcessor).PackageReceived -= new Action<IWorkingSocket, byte[]>(this.OnReceiveData);
                }
                this.sock.Dispose();
                this.sock.Connected -= new Action<AsyncSocket>(this.OnConnected);
                this.sock.OnError -= new Action<Exception, string>(this.OnError);
                this.sock.Disconnected -= new Action<long>(this.OnDisconnected);
                this.sock = null;
                this.isConnecting = false;
            }
            catch(Exception ex)
            {
                MainProcess.Log.Error("[AppSyncClient] raised Error in DestroySocket", ex);
            }
        }

        public void Connect()
        {
            if (this.sock != null)
                this.DestroySocket();

            this.sock = new InnoAsyncSocket(this.srcIp, this.srcPort);
            this.sock.Connected += new Action<AsyncSocket>(this.OnConnected);
            this.sock.OnError += new Action<Exception, string>(this.OnError);

            this.isConnecting = true;
            this.sock.ConnectTo(new IPEndPoint(IPAddress.Parse(this.dstIp), this.dstPort));
        }

        protected virtual void OnConnected(AsyncSocket sock)
        {
            this.isConnecting = false;

            sock.Disconnected += new Action<long>(this.OnDisconnected);

            UseProcessor proc = new UseProcessor();
            proc.Init(sock);
            proc.AnalyzeLength = bs =>
            {
                int result = 0;

                try
                {
                    if (bs.Length > SyncNetworkBase.HEADER_SIZE)
                    {
                        byte[] payloadLengthByteArray;

                        payloadLengthByteArray = SyncNetworkBase.ByteArrayCopyMemory(bs, SyncNetworkBase.COMMAND_SIZE + SyncNetworkBase.TIMESTR_SIZE , SyncNetworkBase.LENGTH_SIZE);

                        int payloadLength = BitConverter.ToInt32(payloadLengthByteArray, 0);

                        if (bs.Length < SyncNetworkBase.HEADER_SIZE + payloadLength)
                        {
                            result = 0;
                        }

                        result = SyncNetworkBase.HEADER_SIZE + payloadLength;
                    }
                    else
                    {
                        result = 0;
                    }
                }
                catch (Exception ex)
                {
                    SyncLogWriter.GetLog().Error("[AppSyncClient] Raised Exception", ex);
                }

                return result;
            };

            proc.PackageReceived += new Action<IWorkingSocket, byte[]>(this.OnReceiveData);

            sock.Processor = proc;
            sock.WaitReceive();
            this.syncClientUI.OnConnected();
        }

        protected virtual void OnDisconnected(long sockID)
        {
            this.isConnecting = false;
            this.syncClientUI.OnDisConnected();
        }

        protected virtual void OnReceiveData(IWorkingSocket sock, byte[] data)
        {
            this.syncClientUI.ReceiveData(data);
        }

        protected virtual void OnError(Exception exception, string msg)
        {
            this.isConnecting = false;
            this.syncClientUI.OnError(exception, msg);
        }

        public void SendData(byte [] data)
        {
            sock.Send(data);
            this.syncClientUI.SendData(data);
        }

        public void Disconnect()
        {
            DestroySocket();
        }
    }

    public class InnoSyncClient : SyncClientCore
    {
        private System.Timers.Timer reconnectTimer = null;
        private object reconnectLockObj = new object();

        private System.Timers.Timer handShakingTimer = null;
        private object handShakingLockObj = new object();

        private DateTime preDoHandShakingTime = DateTime.Now;
        private TimeSpan doHandShakingInterval = TimeSpan.FromMilliseconds(5000);

        private bool waitingResponseHandShakingData = false;
        private DateTime sendHandShakingTime = DateTime.MaxValue;
        private TimeSpan allowHandShakingWorkingTime = TimeSpan.FromMilliseconds(5000);
        private object handShakingVarLockObj = new object();

        private ClientIDData idData = new ClientIDData();
        protected INetworkDataTranslater dataTranslater = new CommandTranslater();

        public bool UseReconnect { get; set; }

        public InnoSyncClient(string srcIp, int srcPort, string dstIp, int dstPort, ISyncClientUI syncClientUI, Public.ProgramType type)
            : base(srcIp, srcPort, dstIp, dstPort, syncClientUI)
        {
            idData = SyncNetworkBase.GetClientIDData(type);
            
            UseReconnect = false;

            reconnectTimer = new System.Timers.Timer();
            reconnectTimer.Interval = 3000;
            reconnectTimer.Elapsed += new ElapsedEventHandler(this.reconnectTimer_Tick);

            handShakingTimer = new System.Timers.Timer();
            handShakingTimer.Interval = 1000;
            handShakingTimer.Elapsed += new ElapsedEventHandler(this.handShakingTimer_Tick);
            handShakingTimer.Start();
        }

        public override void Dispose()
        {
            base.Dispose();

            reconnectTimer.Stop();
            reconnectTimer.Elapsed -= new ElapsedEventHandler(this.reconnectTimer_Tick);
            reconnectTimer = null;

            handShakingTimer.Stop();
            handShakingTimer.Elapsed -= new ElapsedEventHandler(this.handShakingTimer_Tick);
            handShakingTimer = null;
        }

        private void reconnectTimer_Tick(Object sender, ElapsedEventArgs e)
        {
            if (Monitor.TryEnter(this.reconnectLockObj) == false)
                return;

            try
            {
                MainProcess.Log.Info(SyncLogWriter.MakeLogString("AppSyncClient", "Try reconnect"));
                this.Connect();

                this.StopReconnectTimer();
            }
            finally
            {
                Monitor.Exit(this.reconnectLockObj);
            }
        }

        private void handShakingTimer_Tick(Object sender, ElapsedEventArgs e)
        {
            if (Monitor.TryEnter(this.handShakingLockObj) == false)
                return;

            try
            {
                if (this.isConnecting == true)
                    return;

                lock (handShakingVarLockObj)
                {
                    TimeSpan gapHandShakingTime = DateTime.Now - this.preDoHandShakingTime;
                
                    if (gapHandShakingTime > this.doHandShakingInterval && waitingResponseHandShakingData == false)
                    {
                        System.Diagnostics.Debug.WriteLine("[AppSyncClient] Send HandShaking Data");
                        MainProcess.Log.Info("[AppSyncClient] Send HandShaking Data");
                        this.SendDataObject(HandShakingData.MakeInstance());
                        this.preDoHandShakingTime = DateTime.Now;
                        this.sendHandShakingTime = DateTime.Now;
                        this.waitingResponseHandShakingData = true;
                    }
                }

                if (this.waitingResponseHandShakingData == true)
                    return;

                TimeSpan gapHandShakingWorkTime = DateTime.Now - this.sendHandShakingTime;

                if (gapHandShakingWorkTime > this.allowHandShakingWorkingTime)
                {
                    System.Diagnostics.Debug.WriteLine("[AppSyncClient] do not received HandShaking Data from SyncServer so socket disconnect");
                    MainProcess.Log.Error("[AppSyncClient] do not received HandShaking Data from SyncServer so socket disconnect");
                    this.Disconnect();
                }
            }
            finally
            {
                Monitor.Exit(this.handShakingLockObj);
            }
        }

        private void StartReconnectTimer()
        {
            if (this.UseReconnect == true)
            {
                this.reconnectTimer.Start();
            }
        }

        private void StopReconnectTimer()
        {
            this.reconnectTimer.Stop();
        }

        protected override void OnConnected(AsyncSocket sock)
        {
            base.OnConnected(sock);
            this.StopReconnectTimer();
            
            lock (handShakingVarLockObj)
            {
                this.sendHandShakingTime = DateTime.MaxValue;
                this.waitingResponseHandShakingData = false;
            }
            this.SendData(dataTranslater.ToByte(idData));            
        }

        protected override void OnDisconnected(long sockID)
        {
            if (this.isDestroying == false)
            {
                base.OnDisconnected(sockID);
                this.StartReconnectTimer();
            }
        }

        protected override void OnError(Exception exception, string msg)
        {
            base.OnError(exception, msg);

            if (exception != null)
            {
                if (exception is ConnectException)
                {
                    this.StartReconnectTimer();
                }
            }
        }

        protected override void OnReceiveData(IWorkingSocket sock, byte[] data)
        {
            object ReceivedObj = dataTranslater.ToObject(data);
            if (ReceivedObj is HandShakingData)
            {
                lock (handShakingVarLockObj)
                {
                    this.waitingResponseHandShakingData = false;
                    this.sendHandShakingTime = DateTime.Now;
                    System.Diagnostics.Debug.WriteLine("[AppSyncClient] Received HandShaking Data");
                    MainProcess.Log.Info("[AppSyncClient] Received HandShaking Data");
                }
            }
            else if (ReceivedObj is BaseSyncList)
            {
                this.SendDataObject(SyncReceiveOKData.MakeInstance());
            }

            base.OnReceiveData(sock, data);
        }

        public void SendDataObject(object data)
        {
            byte[] arrayByteData = dataTranslater.ToByte(data);
            sock.Send(arrayByteData);
            this.syncClientUI.SendData(arrayByteData);
        }
    }

    public class InnoWatchSyncClient : InnoSyncClient
    {
        private ControlServerComm constrolServer = null;

        public InnoWatchSyncClient(string srcIp, int srcPort, string dstIp, int dstPort, ISyncClientUI syncClientUI,
                                    Public.ProgramType type, ControlServerComm constrolServer)
            : base(srcIp, srcPort, dstIp, dstPort, syncClientUI, type)
        {
            this.constrolServer = constrolServer;
        }

        protected override void OnReceiveData(IWorkingSocket sock, byte[] data)
        {
            base.OnReceiveData(sock, data);
            object receiveObject = dataTranslater.ToObject(data);
            if (receiveObject is BaseSyncList)
                this.constrolServer.RecvContentSync((BaseSyncList)receiveObject);
        }
    }
}