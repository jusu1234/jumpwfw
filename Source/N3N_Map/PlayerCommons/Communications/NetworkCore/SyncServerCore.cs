// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SyncServerCore.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Sync Server Core
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.PlayerCommons.Communications.NetworkCore
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Net.Sockets;
    using System.Text;
    using Innotive.InnoWatch.PlayerCommons.Communications.EDAS;
    using Innotive.Communication.ControlServerMsg;
    using Innotive.InnoWatch.PlayerCommons.Communications.Models;
    using Innotive.InnoWatch.Commons.Publics;
    
    public class UseProcessor : PackageProcessor
    { }

    public interface ISyncServerUI
    {
        void OnException(Exception ex, string msg);
        void OnUserException(IWorkingSocket sock, Exception ex);
        void AcceptClient(long id, string clientIP, int clientPort);
        void DisconnectClient(long id);
        void OnReceiveData(long id, byte[] data, bool sendtoClientAll);
        void OnSendData(long id, byte[] data);
        void OnServerStart(string msg);
    }

    public class DummySyncServerUI : ISyncServerUI
    {
        public void OnException(Exception ex, string msg) { }
        public void OnUserException(IWorkingSocket sock, Exception ex) { }
        public void AcceptClient(long id, string clientIP, int clientPort) { }
        public void DisconnectClient(long id) { }
        public void OnReceiveData(long id, byte[] data, bool sendtoClientAll) { }
        public void OnSendData(long id, byte[] data) { }
        public void OnServerStart(string msg) { }
    }

    public class SyncServerCore : IDisposable
    {
        private ISyncServerUI syncServerUI = new DummySyncServerUI();
        private Acceptor acp = null;

        protected Object clientListLockObj = new Object();
        protected List<IWorkingSocket> clientList = new List<IWorkingSocket>();

        public SyncServerCore(int listenPort, ISyncServerUI syncServerUI)
        {
            if (syncServerUI != null) this.syncServerUI = syncServerUI;

            acp = new Acceptor(listenPort);
            acp.OnError += new Action<Exception, string>(this.OnError);
            acp.Accepted += new Action<Socket, long, int>(this.OnAccept);
            acp.WaitAsyncAccept();
            
            this.syncServerUI.OnServerStart("listenport = "+listenPort);
        }

        public void Dispose()
        {
            acp.OnError -= new Action<Exception, string>(this.OnError);
            acp.Accepted -= new Action<Socket, long, int>(this.OnAccept);
            acp.Close();
            acp = null;

            this.RemoveAllClient();

            clientListLockObj = null;
        }

        private void OnAccept(Socket sock, long SockID, int bufferSize)
        {
            InnoAsyncSocket sk = new InnoAsyncSocket(sock, bufferSize);
            sk.ID = SockID;
            sk.OnError += new Action<Exception, string>(this.OnError);
            sk.OnUserException += new Action<IWorkingSocket, Exception>(this.OnUserException);
            sk.Disconnected += new Action<long>(this.OnDisconnected);

            UseProcessor proc = new UseProcessor();
            proc.Init(sk);
            proc.AnalyzeLength = bs =>
            {
                int result = 0;

                try
                {
                    if (bs.Length > SyncNetworkBase.HEADER_SIZE)
                    {
                        byte[] payloadLengthByteArray;

                        payloadLengthByteArray = SyncNetworkBase.ByteArrayCopyMemory(bs, SyncNetworkBase.COMMAND_SIZE + SyncNetworkBase.TIMESTR_SIZE, SyncNetworkBase.LENGTH_SIZE);

                        int payloadLength = BitConverter.ToInt32(payloadLengthByteArray, 0);

                        if (bs.Length < SyncNetworkBase.HEADER_SIZE + payloadLength)
                        {
                            result = 0;
                        }

                        result = SyncNetworkBase.HEADER_SIZE + payloadLength;
                    }
                    else
                    {
                        result = 0;
                    }
                }
                catch (Exception ex)
                {
                    SyncLogWriter.GetLog().Error("[AppSyncServer] Raised Exception", ex);
                }

                return result;
            };

            proc.PackageReceived += new Action<IWorkingSocket, byte[]>(this.OnReceiveData);
          
            sk.Processor = proc;

            lock (clientListLockObj)
            {
                clientList.Add(sk);
            }

            sk.ClientIp = (sk.worker.RemoteEndPoint as IPEndPoint).Address.ToString();
            sk.ClientPort = (sk.worker.RemoteEndPoint as IPEndPoint).Port;

            this.syncServerUI.AcceptClient(SockID, sk.ClientIp, sk.ClientPort);
            sk.WaitReceive();
        }

        private void OnError(Exception exception, string msg)
        {
            this.syncServerUI.OnException(exception, msg);
        }

        public void OnUserException(IWorkingSocket sock, Exception ex)
        {
            this.syncServerUI.OnUserException(sock, ex);
        }

        private void OnDisconnected(long sockID)
        {
            InnoAsyncSocket sock = this.GetSocket(sockID);

            if (sock != null)
            {
                this.syncServerUI.DisconnectClient(sockID);
                this.RemoveClient(sockID);
            }
            else
            {
                System.Diagnostics.Debug.WriteLine("[SyncServer] Disconnect But have not ClientList!!!!");
            }
        }

        private void OnReceiveData(IWorkingSocket sock, byte[] data)
        {
            bool sendtoClientAll = this.DoWork(sock, data);
            this.syncServerUI.OnReceiveData(sock.ID, data, sendtoClientAll);
        }

        protected void SendData(IWorkingSocket sock, byte[] data)
        {
            //(sock as AsyncSocket).SendToQueue(data);
            (sock as AsyncSocket).Send(data);
            this.syncServerUI.OnSendData(sock.ID, data);
        }

        private void DestroySocket(InnoAsyncSocket sock)
        {
            try
            {
                sock.Disconnect();
                (sock as AsyncSocket).OnError -= new Action<Exception, string>(this.OnError);
                (sock as AsyncSocket).OnUserException -= new Action<IWorkingSocket, Exception>(this.OnUserException);
                (sock as AsyncSocket).Disconnected -= new Action<long>(this.OnDisconnected);
                ((sock as AsyncSocket).Processor as UseProcessor).PackageReceived -= new Action<IWorkingSocket, byte[]>(OnReceiveData);
                (sock as AsyncSocket).Dispose();
            }
            catch (Exception ex)
            {
                MainProcess.Log.Error("[AppSyncServer] raised Error in DestroySocket", ex);
            }
        }

        public void RemoveClientByClientGuid(Guid guid)
        {
            lock (clientListLockObj)
            {
                for (int i = this.clientList.Count - 1; i >= 0; i--)
                {
                    InnoAsyncSocket sock = (InnoAsyncSocket)this.clientList[i];
                    if (sock.IDData.GUID.CompareTo(guid) == 0)
                    {
                        this.clientList.Remove(sock);
                        this.DestroySocket(sock);
                        return;
                    }
                }
            }
        }

        private void RemoveClient(long sockID)
        {
            lock(clientListLockObj)
            {
                for (int i = this.clientList.Count - 1; i >= 0; i--)
                {
                    InnoAsyncSocket sock = (InnoAsyncSocket)this.clientList[i];
                    if (sock.ID == sockID)
                    {
                        this.clientList.Remove(sock);
                        this.DestroySocket(sock);
                        return;
                    }
                }
            }
        }

        private InnoAsyncSocket GetSocket(long sockID)
        {
            lock (clientListLockObj)
            {
                foreach (InnoAsyncSocket sock in this.clientList)
                {
                    if (sock.ID == sockID)
                        return sock;
                }
            }

            return null;
        }

        private void RemoveAllClient()
        {
            lock (clientListLockObj)
            {
                for (int i = this.clientList.Count - 1; i >= 0; i--)
                {
                    InnoAsyncSocket sock = (InnoAsyncSocket)this.clientList[i];
                    this.clientList.Remove(sock);
                    this.DestroySocket(sock);
                }
            }
        }

        protected virtual bool DoWork(IWorkingSocket sock, byte[] data)
        {
            return false;
        }
    }

    public class InnoSyncServer : SyncServerCore
    {
        private INetworkDataTranslater dataTranslater = new CommandTranslater();

        public InnoSyncServer(int listenPort, ISyncServerUI syncServerUI): base(listenPort, syncServerUI)
        {
        }

        protected override bool DoWork(IWorkingSocket sock, byte[] data)
        {
            object receiveObject = dataTranslater.ToObject(data);

            if (receiveObject is BaseSyncList)
            {
                BaseSyncList synclist = receiveObject as BaseSyncList;

                //아무 데이타가 없다면 아무 할일도 없다.
                if (synclist.Count < 1) return false;

                //플레이어라면 받은데이타를 그냥 무시한다.
                if ((sock as InnoAsyncSocket).IDData.ProgramType == Public.ProgramType.Player)
                    return false;

                lock (clientListLockObj)
                {
                    foreach (IWorkingSocket socket in this.clientList)
                    {
                        //System.Diagnostics.Debug.WriteLine("===========================Server Send Enter==========================");
                        if ((socket as InnoAsyncSocket).IsReceivedCount < 3)
                        {
                            this.SendData(socket, data);
                            (socket as InnoAsyncSocket).IsReceivedCount++;
                        }
                        else
                        {
                            socket.SendToQueue(data);
                        }
                        //System.Diagnostics.Debug.WriteLine("===========================Server Send Leave==========================");
                    }

                    return true;
                }
            }
            else if (receiveObject is CustomData)
            {
                CustomData customData = receiveObject as CustomData;

                switch(customData.DataType)
                {
                    case CustomDataType.ClientIDData :
                        // 만약에 같은 GUID로 클라이언트 소켓이 생성되어있다면 삭제한다.
                        // 계속 소켓이 증가되는걸 방지하는 코드
                        this.RemoveClientByClientGuid((customData as ClientIDData).GUID);

                        (sock as InnoAsyncSocket).IDData = customData as ClientIDData;
                        return false;
                        
                    case CustomDataType.HandShakingData :
                        this.SendData(sock, data);
                        return false;
                    case CustomDataType.SyncReceiveOKData :
                        lock (clientListLockObj)
                        {
                            (sock as InnoAsyncSocket).IsReceivedCount--;
                            (sock as AsyncSocket).WakeSendProcess();
                        }
                        return false;
                    default :
                        MainProcess.Log.Error("[AppSyncServer] Received Unknown CustomData");
                        return false;
                }
            }

            return false;
        }
    }
}