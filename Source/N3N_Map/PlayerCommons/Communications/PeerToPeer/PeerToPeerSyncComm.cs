﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PeerToPeerSyncComm.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Peer To Peer Communication Helper Class.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.PlayerCommons.Communications.PeerToPeer
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.ServiceModel;
    using System.ServiceModel.Description;
    using System.Text;

    using Innotive.Communication.ControlServerMsg;
    using Innotive.InnoWatch.Commons.Utils;
    using Innotive.InnoWatch.CommonUtils.Network;

    /// <summary>
    /// Peer To Peer Sync Communication Class.
    /// </summary>
    public class PeerToPeerSyncComm
    {
        private IPeerToPeerSyncChannel syncChannel; 
        private InstanceContext instanceContextForSync;
        private NetPeerTcpBinding netPeerTcpBinding; 
        private ChannelFactory<IPeerToPeerSyncChannel> channelFactoryForSync; 
        private IOnlineStatus onlineStatusForSync;
        private string endpointaddress = string.Empty;

        /// <summary>
        /// Start Peer To Peer Sync Communication.
        /// </summary>
        public void Start(object implementation, string endpointAddress)
        {
            if (NetPeerTcpBinding.IsPnrpAvailable)
            {
                this.endpointaddress = endpointAddress;
                this.instanceContextForSync = new InstanceContext(implementation);
                this.netPeerTcpBinding = InnoNetwork.NewNetPeerTcpBinding();
                this.channelFactoryForSync = new DuplexChannelFactory<IPeerToPeerSyncChannel>(
                                                                                    this.instanceContextForSync,
                                                                                    new ServiceEndpoint(
                                                                                        ContractDescription.GetContract(typeof(IPeerToPeerSync)),
                                                                                        this.netPeerTcpBinding,
                                                                                        new EndpointAddress(this.endpointaddress)));

                this.syncChannel = this.channelFactoryForSync.CreateChannel();

                ((ICommunicationObject)this.syncChannel).Open();

                this.onlineStatusForSync = this.syncChannel.GetProperty<IOnlineStatus>();
                this.onlineStatusForSync.Online += this.OnlineStatusForSync_Online;
                this.onlineStatusForSync.Offline += this.OnlineStatusForSync_Offline;

                InnoTrace.Trace("[PeerToPeerSync] Start - " + this.endpointaddress);    
            }
            else
            {
                InnoTrace.Trace("[PeerToPeerSync] Not Available! - " + this.endpointaddress);
                InnoTrace.Log.Info("[PeerToPeerSync] Not Available! - " + this.endpointaddress);
            }
            
        }

        /// <summary>
        /// Stop Peer To Peer Sync Communication.
        /// </summary>
        public void Stop()
        {
            ((ICommunicationObject)this.syncChannel).Close();

            if (this.syncChannel != null)
            {
                this.syncChannel.Close();
            }

            InnoTrace.Trace("[PeerToPeerSync] Stop - " + this.endpointaddress);
        }

        public void SendPeerToPeerSync(BaseSyncList synclist)
        {
            if (this.syncChannel != null)
            {
                InnoTrace.Trace("[PeerToPeerSync] Send Sync Start!");
                this.syncChannel.ReceivePeerToPeerSync(synclist);
                InnoTrace.Trace("[PeerToPeerSync] Send Sync End!");
            }
        }

        private void OnlineStatusForSync_Online(object sender, EventArgs e)
        {
            InnoTrace.Trace("[PeerToPeerSync] Online.");
            InnoTrace.Log.Info("[PeerToPeerSync] Online.");
        }

        private void OnlineStatusForSync_Offline(object sender, EventArgs e)
        {
            InnoTrace.Trace("[PeerToPeerSync] Offline.");
            InnoTrace.Log.Info("[PeerToPeerSync] Offline.");
        }
    }
}
