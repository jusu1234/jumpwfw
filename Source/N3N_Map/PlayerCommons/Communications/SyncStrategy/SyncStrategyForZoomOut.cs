﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SyncStrategyForZoomOut.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Zoom Out 싱크 클래스.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.PlayerCommons.Communications.SyncStrategy
{
    using System;
    using Innotive.InnoWatch.Commons.Utils;
    using Innotive.InnoWatch.CommonUtils.Log;
    using Innotive.InnoWatch.CommonUtils.Models.Sync.Control;

    /// <summary>
    /// Zoom Out 싱크 클래스.
    /// </summary>
    public class SyncStrategyForZoomOut : ISyncStrategy
    {
        #region Implemented Interfaces

        #region ISyncStrategy

        /// <summary>
        /// do sync.
        /// </summary>
        /// <param name="item">
        /// The item.
        /// </param>
        public void DoSync(object item)
        {
            var data = item as ZoomOutSync;
         
            if(data != null)
            {
                DoSync_Core(data);
            }
        }

        #region ZoomOutData

        static private void DoSync_Core(ZoomOutSync data)
        {
            InnotiveDebug.Trace(7, "===== DoSync() ZoomOut Rect = {0}", data.DesireRect);
            MainProcess.Instance.ProjectManager.LayoutContentsSyncManager.ZoomOut(data.LayoutGuid, Guid.Empty, InnoConvertUtil.ToRect(data.DesireRect));
        }

        #endregion // ZoomOutData

        #endregion

        #endregion
    }
}