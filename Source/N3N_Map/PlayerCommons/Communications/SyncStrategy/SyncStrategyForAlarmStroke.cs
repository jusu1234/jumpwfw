﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SyncStrategyForAlarmStroke.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Alarm Stroke 싱크 클래스.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.PlayerCommons.Communications.SyncStrategy
{
    using System;
    using Commons.EventArguments;
    using Innotive.InnoWatch.Commons.Publics;
    using Innotive.InnoWatch.CommonUtils.Log;
    using Innotive.InnoWatch.CommonUtils.Models.Sync.Control;

    /// <summary>
    /// Alarm Stroke 싱크 클래스.
    /// </summary>
    public class SyncStrategyForAlarmStroke : ISyncStrategy
    {
        #region Implemented Interfaces

        #region ISyncStrategy

        /// <summary>
        /// do sync.
        /// </summary>
        /// <param name="item">
        /// The item.
        /// </param>
        public void DoSync(object item)
        {
            var data = item as AlarmStrokeSync;

            if(data != null)
            {
                MainProcess.Instance.MainWindow.Dispatcher.BeginInvoke(
                    new Action(() => DoSync_Core(data)));
            }
        }

        #endregion        

        #region AlarmStroke

        static private void DoSync_Core(AlarmStrokeSync data)
        {
            InnotiveDebug.Trace(5, "===== DoSync() AlarmStroke 호출함 !!");

            MainProcess.Instance.RaiseExternalEvent(new ShowAlarmBorderEventArgs((BorderActionType)data.BorderAction, (string)data.BorderColor, data.OverlayDisplayAreaSyncGuidList));
        }

        #endregion // AlarmStroke

        #endregion
    }
}