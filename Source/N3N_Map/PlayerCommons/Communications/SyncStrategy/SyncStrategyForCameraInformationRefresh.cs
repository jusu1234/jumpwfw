﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SyncStrategyForCameraInformationRefresh.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Camera Information Refresh 싱크 클래스.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.PlayerCommons.Communications.SyncStrategy
{
    using System;
    using Commons.EventArguments;
    using Innotive.InnoWatch.Commons.Publics;
    using Innotive.InnoWatch.CommonUtils.Log;
    using Innotive.InnoWatch.CommonUtils.Models.Sync.Control;

    /// <summary>
    /// Camera Information Refresh 싱크 클래스.
    /// </summary>
    public class SyncStrategyForCameraInformationRefresh : ISyncStrategy
    {
        #region Implemented Interfaces

        #region ISyncStrategy

        /// <summary>
        /// do sync.
        /// </summary>
        /// <param name="item">
        /// The item. (사용하지 않음)
        /// </param>
        public void DoSync(object item)
        {
            var data = item as CameraInformationRefreshSync;
            if (data != null)
            {
                MainProcess.Instance.MainWindow.Dispatcher.BeginInvoke(
                    new Action(() => DoSync_Core(data)));
            }            
        }

        #endregion        

        #region SyncStrategyForCameraInformationRefresh

        static private void DoSync_Core(CameraInformationRefreshSync data)
        {
            InnotiveDebug.Trace(5, "===== DoSync() SyncStrategyForCameraInformationRefresh 호출함 !!");
            
            if (!Innotive.InnoWatch.PlayerCommons.DeviceInfos.DeviceInfoProcess.RefreshDeviceInformation(
                            !string.IsNullOrWhiteSpace(Innotive.InnoWatch.PlayerCommons.Configs.PlayerCommonsConfig.Instance.DataServiceUrl)))
            {
                MainProcess.Log.Error("[RecvMustUpdateCameraInformation->RefreshDeviceInformation] Apply failed!");
            }
        }

        #endregion // SyncStrategyForCameraInformationRefresh

        #endregion
    }
}