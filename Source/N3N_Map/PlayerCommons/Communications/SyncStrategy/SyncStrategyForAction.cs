﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SyncStrategyForAction.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Action 싱크 클래스.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.PlayerCommons.Communications.SyncStrategy
{
    using System;
    using Innotive.InnoWatch.CommonUtils.Models.Sync.Content;

    /// <summary>
    /// Action 싱크 클래스.
    /// </summary>
    public class SyncStrategyForAction : ISyncStrategy
    {
        #region Implemented Interfaces

        #region ISyncStrategy

        /// <summary>
        /// do sync.
        /// </summary>
        /// <param name="item">
        /// The item.
        /// </param>
        public void DoSync(object item)
        {
            var data = item as ActionSync;
            if (data != null)
            {
                MainProcess.Instance.MainWindow.Dispatcher.BeginInvoke(
                            new Action(() => DoSync_Core(data)));
            }
        }

        #endregion        

        #region Private Functions

        static private void DoSync_Core(ActionSync data)
        {           
            MainProcess.Instance.ProjectManager.PlayerActionManager.Excute(data.ActionGuid, true);
        }

        #endregion // Private Functions

        #endregion
    }
}