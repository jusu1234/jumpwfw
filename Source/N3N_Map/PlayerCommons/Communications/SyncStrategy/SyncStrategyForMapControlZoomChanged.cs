﻿
namespace Innotive.InnoWatch.PlayerCommons.Communications.SyncStrategy
{
    using Innotive.InnoWatch.CommonUtils.Log;
    using Innotive.InnoWatch.CommonUtils.Models.Sync.Control;

    /// <summary>
    /// MapControlChanged 싱크 클래스.
    /// </summary>
    public class SyncStrategyForMapControlZoomChanged : ISyncStrategy
    {
        /// <summary>
        /// do sync.
        /// </summary>
        /// <param name="item">
        /// The item.
        /// </param>
        public void DoSync(object item)
        {
            var data = item as MapControlZoomChangedSync;

            if (data != null)
            {
                DoSync_Core(data);
            }
        }

        static private void DoSync_Core(MapControlZoomChangedSync data)
        {
            var zoomLevel = data.ZoomLevel;
            var centerLatitude = data.CenterLatitude;
            var centerLongitude = data.CenterLongitude;
            var mapControlGuid = data.MapControlGuid;

            InnotiveDebug.Trace(7, "===== DoSync() MapControlChangedSync Zoom = {0}", zoomLevel);
            MainProcess.Instance.ProjectManager.LayoutContentsSyncManager.MapControlZoomChanged(mapControlGuid, zoomLevel, centerLatitude, centerLongitude);
        }
    }
}