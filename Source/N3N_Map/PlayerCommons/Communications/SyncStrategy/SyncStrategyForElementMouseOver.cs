﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SyncStrategyForElementMouseOver.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   ElementMouseOver 싱크 클래스.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.PlayerCommons.Communications.SyncStrategy
{
    using System;
    using Innotive.InnoWatch.CommonUtils.Models.Sync.Control;


    /// <summary>
    /// ElementMouseOver 싱크 클래스.
    /// </summary>
    public class SyncStrategyForElementMouseOver : ISyncStrategy
    {
        #region Implemented Interfaces

        #region ISyncStrategy

        /// <summary>
        /// do sync.
        /// </summary>
        /// <param name="item">
        /// The item.
        /// </param>
        public void DoSync(object item)
        {
            var data = item as ElementMouseOverSync;

            if (data != null)
            {
                MainProcess.Instance.MainWindow.Dispatcher.BeginInvoke(
                    new Action(() => DoSync_Core(data)));
            }
        }

        #endregion

        #endregion

        #region ElementMouseOverData

        static private void DoSync_Core(ElementMouseOverSync data)
        {

        }
        
        #endregion // ElementMouseOverData
    }
}