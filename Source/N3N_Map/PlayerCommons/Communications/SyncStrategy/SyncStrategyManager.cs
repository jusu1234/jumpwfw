﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SyncStrategyManager.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   싱크 전략 패턴 인터페이스.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.PlayerCommons.Communications.SyncStrategy
{
    using Innotive.InnoWatch.CommonUtils.Models.Sync.Content;
    using Innotive.InnoWatch.CommonUtils.Models.Sync.Control;
    using Innotive.InnoWatch.CommonUtils.Models.Sync.Multigrid;

    /// <summary>
    /// 싱크 전략 패턴 인터페이스.
    /// </summary>
    internal interface ISyncStrategy
    {
        #region Public Methods

        /// <summary>
        /// do sync.
        /// </summary>
        /// <param name="item">
        /// The item.
        /// </param>
        void DoSync(object item);

        #endregion
    }

    /// <summary>
    /// 싱크 전략 패턴 관리자.
    /// </summary>
    public class SyncStrategyManager
    {
        #region Constants and Fields

        private object _item = null;

        private ISyncStrategy _strategy = null;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="SyncStrategyManager"/> class. 
        /// 클래스 생성자.
        /// </summary>
        /// <param name="item">
        /// The item.
        /// </param>
        public SyncStrategyManager(object item)
        {
            this._item = item;

            // CONTENT SYNC
            if (item is ElementMouseOverSync)
            {
                this._strategy = new SyncStrategyForElementMouseOver();
            }

            if (item is ZoomInSync)
            {
                this._strategy = new SyncStrategyForZoomIn();
            }

            if (item is ZoomOutSync)
            {
                this._strategy = new SyncStrategyForZoomOut();
            }

            if (item is PanningEndSync)
            {
                this._strategy = new SyncStrategyForPanningEnd();
            }

            if (item is SettingRectSync)
            {
                this._strategy = new SyncStrategyForSettingRect();
            }

            // MODE CHANGE SYNC
            if (item is DisplayModeSync)
            {
                this._strategy = new SyncStrategyForDisplayMode();
            }

            // ALARM SYNC
            if (item is AlarmStrokeSync)
            {
                this._strategy = new SyncStrategyForAlarmStroke();
            }

            if (item is SyncStrategyForCameraInformationRefresh)
            {
                this._strategy = new SyncStrategyForCameraInformationRefresh();
            }

            if (item is DisplayAreaInitSync)
            {
                this._strategy = new SyncStrategyForDisplayAreaInit();
            }

            // Action Sync

            if (item is ActionSync)
            {
                this._strategy = new SyncStrategyForAction();
            }

            // Map Sync

            if (item is LocationMapSaveSync)
            {
                this._strategy = new SyncStrategyForLocationMapSave();
            }

            if (item is MapControlZoomChangedSync)
            {
                this._strategy = new SyncStrategyForMapControlZoomChanged();
            }

            if (item is MapControlPositionChangeEndSync)
            {
                this._strategy = new SyncStrategyForMapControlPositionChangeEnd();
            }

            if (item is MapControlFullViewCameraSync)
            {
                this._strategy = new SyncStrategyForMapControlFullViewCamera();
            }
        }


        #endregion

        #region Public Methods
        

        /// <summary>
        /// 싱크 수행.
        /// </summary>
        public void DoSync()
        {
            if (this._strategy != null)
            {
                this._strategy.DoSync(this._item);                
            }
        }

        #endregion
    }
}