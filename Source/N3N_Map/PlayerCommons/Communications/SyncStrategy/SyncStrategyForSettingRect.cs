﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SyncStrategyForSettingRect.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Setting Rect 싱크 클래스.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.PlayerCommons.Communications.SyncStrategy
{
    using System;
    using Innotive.InnoWatch.Commons.Utils;
    using Innotive.InnoWatch.CommonUtils.Models.Sync.Control;

    /// <summary>
    /// Setting Rect 싱크 클래스.
    /// </summary>
    public class SyncStrategyForSettingRect : ISyncStrategy
    {
        #region Implemented Interfaces

        #region ISyncStrategy

        /// <summary>
        /// do sync.
        /// </summary>
        /// <param name="item">
        /// The item.
        /// </param>
        public void DoSync(object item)
        {
            var data = item as SettingRectSync;

            if(data != null)
            {
                MainProcess.Instance.MainWindow.Dispatcher.BeginInvoke(
                    new Action(() => DoSync_Core(data)));
            }
        }

        #region SettingRectData

        static private void DoSync_Core(SettingRectSync data)
        {
            // 외부 Sync
            if (MainProcess.Instance.CommManager.ControlServer == null)
            {
                return;
            }

            if (MainProcess.Instance.CommManager.ControlServer.SyncHelper == null)
            {
                return;
            }

            if (data.Rect != null)
            {
                MainProcess.Instance.ProjectManager.LayoutContentsSyncManager.SettingRect(data.LayoutGuid, Guid.Empty, InnoConvertUtil.ToRect(data.Rect));
            }
        }

        #endregion // SettingRectData

        #endregion

        #endregion
    }
}