﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SyncStrategyForZoomIn.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
// SyncStrategyForLocationMapSave 싱크 클래스.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.PlayerCommons.Communications.SyncStrategy
{
    using Innotive.InnoWatch.CommonUtils.Log;
    using Innotive.InnoWatch.CommonUtils.Models.Sync.Control;

    public class SyncStrategyForLocationMapSave : ISyncStrategy
    {
        /// <summary>
        /// do sync.
        /// </summary>
        /// <param name="item">
        /// The item.
        /// </param>
        public void DoSync(object item)
        {
            var data = item as LocationMapSaveSync;

            if (data != null)
            {
                DoSync_Core(data);
            }
        }

        private static void DoSync_Core(LocationMapSaveSync data)
        {
            InnotiveDebug.Trace(7, "===== DoSync() LocationMapSaveSync Guid = {0}", data.LocationMapSyncGuid);
            MainProcess.Instance.ProjectManager.LocationMapSyncManager.LocationMapFileSync(data.LocationMapSyncGuid);
        }
    }
}