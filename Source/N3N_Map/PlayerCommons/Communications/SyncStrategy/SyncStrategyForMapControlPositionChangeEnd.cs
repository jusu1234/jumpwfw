﻿
namespace Innotive.InnoWatch.PlayerCommons.Communications.SyncStrategy
{
    using Innotive.InnoWatch.CommonUtils.Log;
    using Innotive.InnoWatch.CommonUtils.Models.Sync.Control;

    /// <summary>
    /// MapControlChanged 싱크 클래스.
    /// </summary>
    public class SyncStrategyForMapControlPositionChangeEnd : ISyncStrategy
    {
        /// <summary>
        /// do sync.
        /// </summary>
        /// <param name="item">
        /// The item.
        /// </param>
        public void DoSync(object item)
        {
            var data = item as MapControlPositionChangeEndSync;
            
            if(data != null)
            {
                DoSync_Core(data);
            }
        }

        static private void DoSync_Core(MapControlPositionChangeEndSync data)
        {
            InnotiveDebug.Trace(7, "===== DoSync() MapControlPositionChangedSync Lat = {0}, Lng = {1}", data.Latitude, data.Longtitude);
            MainProcess.Instance.ProjectManager.LayoutContentsSyncManager.MapControlPositionChanged(data.MapControlGuid, data.Latitude, data.Longtitude);
            
        }
    }
}