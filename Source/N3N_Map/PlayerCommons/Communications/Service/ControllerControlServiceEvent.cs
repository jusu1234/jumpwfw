﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Innotive.InnoWatch.PlayerCommons.Communications.Service
{
    /// <summary>
    /// The controller control service event.
    /// </summary>
    public class ControllerControlServiceEvent
    {
        /// <summary>
        /// The event handler.
        /// </summary>
        public static EventHandler<ControllerControlEventArgs> eControllerControl = null;

        /// <summary>
        /// The invoke eapplication control.
        /// </summary>
        /// <param name="e">
        /// The e.
        /// </param>
        public static void InvokeEControllerControl(ControllerControlEventArgs e)
        {
            EventHandler<ControllerControlEventArgs> handler = eControllerControl;

            if(handler == null)
            {
                // Error!
                return;
            }
            else
            {
                handler(null, e);
            }
        }
    }
}
