﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ApplicationControlServiceEvent.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Defines the ApplicationControlServiceEvent type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using Innotive.InnoWatch.CommonUtils.Log;

namespace Innotive.InnoWatch.PlayerCommons.Communications.Service
{
    using System;

    /// <summary>
    /// The application control service event.
    /// </summary>
    public class ApplicationControlServiceEvent
    {
        /// <summary>
        /// The event handler.
        /// </summary>
        public static event EventHandler<ApplicationControlEventArgs> eApplicationControl = null;

        /// <summary>
        /// The invoke eapplication control.
        /// </summary>
        /// <param name="e">
        /// The e.
        /// </param>
        public static void InvokeEApplicationControl(ApplicationControlEventArgs e)
        {
            EventHandler<ApplicationControlEventArgs> handler = eApplicationControl;

            if(handler == null)
            {
                InnotiveDebug.Log.Error(string.Format("[Invoke Error] StageName : {0}, Data: {1}, Type : {2}", e.StageId, e.Data, e.Type));
                return;
            }

            if (handler != null)
            {
                handler(null, e);
            }
        }
    }
}
