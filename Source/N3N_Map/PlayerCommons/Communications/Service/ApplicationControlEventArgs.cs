﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ApplicationControlEventArgs.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Defines the ApplicationControlEventArgs type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.PlayerCommons.Communications.Service
{
    using System;

    /// <summary>
    /// The application control event args.
    /// </summary>
    public class ApplicationControlEventArgs : EventArgs
    {
        /// <summary>
        /// 스테이지 아이디.
        /// </summary>
        public string StageId { get; protected set; }

        /// <summary>
        /// 스테이지 그리드 내용.
        /// </summary>
        public string Data { get; protected set; }

        /// <summary>
        /// 서비스 타입.
        /// </summary>
        public string Type { get; protected set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="ApplicationControlEventArgs"/> class. 
        /// 클래스 생성자.
        /// </summary>
        /// <param name="stageId">
        /// The stage Id.
        /// </param>
        /// <param name="data">
        /// The data.
        /// </param>
        /// <param name="type">
        /// The type.
        /// </param>
        public ApplicationControlEventArgs(string stageId, string data, string type)
        {
            this.StageId = stageId;
            this.Data = data;
            this.Type = type;
        }
    }
}
