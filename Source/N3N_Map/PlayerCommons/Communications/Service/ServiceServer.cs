﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ServiceServer.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   외부에서 HTTP 방식으로 Command / Display를 Control 할 때 사용하는 Service.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.PlayerCommons.Communications.Service
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.ServiceModel;
    using System.ServiceModel.Description;
    using System.ServiceModel.Web;
    using System.Xml;
    using Innotive.InnoWatch.CommonUtils.Log;
    using Innotive.InnoWatch.PlayerCommons.Configs;

    /// <summary>
    /// 외부에서 HTTP 방식으로 Command / Display를 Control 할 때 사용하는 Service.
    /// </summary>
    public class ServiceServer : IDisposable
    {
        private readonly List<ServiceHost> hosts = new List<ServiceHost>();
        private readonly ControllerControlHTTPServer ptzServer = new ControllerControlHTTPServer();

        /// <summary>
        ///   Initializes a new instance of the <see cref = "ServiceServer" /> class.
        /// </summary>
        public ServiceServer()
        {
            this.AddServiceHost(hosts, typeof(IApplicationEventService), typeof(ApplicationEventService), PlayerCommonsConfig.Instance.ApplicationEventServiceUrl);
        }

        /// <summary>
        ///   Gets BaseAddresses.
        /// </summary>
        public List<Uri> BaseAddresses
        {
            get
            {
                if (this.hosts == null || this.hosts.Count == 0)
                {
                    return null;
                }

                var addresses = new List<Uri>();

                foreach (var serviceHost in this.hosts)
                {
                    addresses.AddRange(serviceHost.BaseAddresses);
                }

                return addresses;
            }
        }

        /// <summary>
        /// The run.
        /// </summary>
        public void Run()
        {
            if (this.hosts == null || this.hosts.Count == 0)
            {
                InnotiveDebug.Log.Error("Run Server Fail. There are no host Exists.");
            }
            
            foreach (var serviceHost in this.hosts)
            {
                try
                {
                    serviceHost.Open();
                    serviceHost.Description.Endpoints.AsParallel().ForAll(ep => InnotiveDebug.Log.InfoFormat("Service is Running on {0}", ep.Address));
                }
                catch (Exception ex)
                {
                    InnotiveDebug.Log.Error(ex);
                }
            }

            ptzServer.Start(Configs.PlayerCommonsConfig.Instance.PTZControllerServicePort);
        }

        private void AddServiceHost(List<ServiceHost> serviceHosts, Type implementedContract, Type serviceType, string uri)
        {
            if(string.IsNullOrWhiteSpace(uri))
                return;

            var host = new WebServiceHost(serviceType, new Uri(uri));

            var webHttpBinding = new WebHttpBinding
            {
                CrossDomainScriptAccessEnabled = true,
                MaxBufferSize = int.MaxValue,
                MaxReceivedMessageSize = int.MaxValue,
                MaxBufferPoolSize = int.MaxValue,
                ReaderQuotas = new XmlDictionaryReaderQuotas
                {
                    MaxArrayLength = int.MaxValue,
                    MaxBytesPerRead = int.MaxValue,
                    MaxDepth = int.MaxValue,
                    MaxNameTableCharCount = int.MaxValue,
                    MaxStringContentLength = int.MaxValue
                }
            };

            var endpoint = host.AddServiceEndpoint(implementedContract, webHttpBinding, new Uri(uri));
            endpoint.Behaviors.Add(new WebHttpBehavior
            {
                HelpEnabled = true,
                AutomaticFormatSelectionEnabled = false,
            });

            serviceHosts.Add(host);
        }


        public void Dispose()
        {
            this.ptzServer.Stop();

            foreach (var serviceHost in this.hosts)
            {
                try
                {
                    serviceHost.Close();
                }
                catch (Exception ex)
                {
                    InnotiveDebug.Log.Error(ex);
                }
            }
        }
    }
}
