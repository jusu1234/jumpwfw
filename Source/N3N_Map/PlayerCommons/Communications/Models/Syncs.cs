﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Syncs.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Syncs Model Class.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.PlayerCommons.Communications.Models
{
    using System;
    using System.ComponentModel;
    using Innotive.InnoWatch.Commons.Collections.ObjectModel;

    [System.SerializableAttribute()]
    public class Syncs : INotifyPropertyChanged
    {
        private DateTime startDateTime = DateTime.MinValue;
        private string sendReceive = string.Empty;
        private string syncCommand = string.Empty;
        private string message = string.Empty;

        public DateTime StartDateTime
        {
            get { return this.startDateTime; }
            set
            {
                this.startDateTime = value;
                this.NotifyPropertyChanged("StartDateTime");
            }
        }
        public string SendReceive
        {
            get { return this.sendReceive; }
            set
            {
                this.sendReceive = value;
                this.NotifyPropertyChanged("SendReceive");
            }
        }
        public string SyncCommand
        {
            get { return this.syncCommand; }
            set
            {
                this.syncCommand = value;
                this.NotifyPropertyChanged("SyncCommand");
            }
        }
        public string Message
        {
            get { return this.message; }
            set
            {
                this.message = value;
                this.NotifyPropertyChanged("Message");
            }
        }
        

        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// The notify property changed.
        /// </summary>
        /// <param name="info">
        /// The info.
        /// </param>
        protected void NotifyPropertyChanged(string info)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }

        #endregion // INotifyPropertyChanged
    }

    [System.SerializableAttribute()]
    public class Syncss : DispatchedObservableCollection<Syncs>
    {
        public void Add(string sendReceive, string syncCommand, string message)
        {
            this.Add(new Syncs()
                         {
                             StartDateTime = DateTime.Now,
                             SendReceive = sendReceive,
                             SyncCommand = syncCommand,
                             Message = message,
                         });
        }
    }
}
