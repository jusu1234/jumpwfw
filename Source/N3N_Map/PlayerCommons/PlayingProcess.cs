﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PlayingProcess.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Defines the PlayingProcess type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------


namespace Innotive.InnoWatch.PlayerCommons
{
    using System;
    using System.Collections.Generic;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;
    using Commons.BaseControls;
    using Commons.EventArguments;
    using Commons.GuidManagers;
    using Commons.Publics;
    using Configs;
    using DLLs.LayoutControls;
    using Innotive.InnoWatch.Commons.CameraManagers;
    using Innotive.InnoWatch.Commons.Utils;
    using Innotive.InnoWatch.CommonUtils.Log;
    using Innotive.InnoWatch.DLLs.CameraControls;    
    using Innotive.InnoWatch.DLLs.MapControls;

    /// <summary>
    /// 레이아웃이 마우스에 반응하는 기본 프로세스.
    /// </summary>
    public class PlayingProcess : BaseProcess
    {
        /// <summary>
        /// MousePointer가 가리키는 최종 위치 (MouseMove 시 계속 변경됨).
        /// </summary>
        private Point lastMousePoint;

        /// <summary>
        /// MouseLeftDown이 발생한 최종 위치.
        /// </summary>
        private Point downMousePoint;

        /// <summary>
        /// Panning 을 해야하는지 여부.
        /// </summary>
        private bool isMouseMove;

        /// <summary>
        /// The MouseLeftButtonDown.
        /// </summary>
        private bool isMouseLButtonDowned;

        /// <summary>
        /// the triggerElement.
        /// </summary>
        private BaseControl triggerElement;

        #region Properties

        //public Rect WholeMonitor { get; set; }

        #endregion

        #region Right Button Down

        /// <summary>
        /// 마우스 오른쪽 클릭에 대한 정의.
        /// 줌아웃을 발생시키기 위한 준비 작업.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public override void MouseRightButtonDownProcess(object sender, MouseButtonEventArgs e)
        {
            if (sender is MapControl)
            {
                e.Handled = true;
                return;
            }

            var layoutControl = sender as LayoutControl;

            if (layoutControl == null)
            {
                return;
            }

            if (!layoutControl.IsMouseCaptured)
            {
                layoutControl.CaptureMouse();
            }

            e.Handled = true;

            this.DoPlayMouseRightButtonDown(layoutControl, e);
        }

        /// <summary>
        /// 줌아웃을 발생시킨다.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        public override void MouseRightButtonUpProcess(object sender, MouseButtonEventArgs e)
        {
            if (sender is MapControl)
            {
                e.Handled = true;
                return;
            }

            var layoutControl = sender as LayoutControl;

            if (layoutControl == null)
            {
                return;
            }

            e.Handled = true;

            try
            {
                this.DoPlayMouseRightButtonUp(layoutControl, e);
            }
            finally
            {
                if (layoutControl.IsMouseCaptured)
                {
                    layoutControl.ReleaseMouseCapture();
                }
            }
        }

        #endregion

        #region Left Button Down

        /// <summary>
        /// 왼쪽 마우스 버튼 다운 구현.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public override void MouseLeftButtonDownProcess(object sender, MouseButtonEventArgs e)
        {
            if (sender is MapControl)
            {
                e.Handled = true;
                return;
            }

            var layoutControl = sender as LayoutControl;

            if (layoutControl == null)
            {
                return;
            }

            if (!layoutControl.IsMouseCaptured)
            {
                layoutControl.CaptureMouse();
            }

            e.Handled = true;

            this.DoPlayMouseLeftButtonDown(layoutControl, e);
        }

        public override void StylusDownProcess(object sender, StylusDownEventArgs e)
        {
            if (e.InAir == false)
            {
                if (sender is MapControl)
                {
                    e.Handled = true;
                    return;
                }

                var layoutControl = sender as LayoutControl;

                if (layoutControl == null)
                {
                    return;
                }

                if (!layoutControl.IsStylusCaptured)
                {
                    layoutControl.CaptureStylus();
                }

                e.Handled = true;

                this.DoPlayStylusDown(layoutControl, e);
            }
        }

        public override void TouchDownProcess(object sender, TouchEventArgs e)
        {
            if (sender is MapControl)
            {
                e.Handled = true;
                return;
            }

            var layoutControl = sender as LayoutControl;

            if (layoutControl == null)
            {
                return;
            }

            if (!layoutControl.IsMouseCaptured)
            {
                layoutControl.CaptureMouse();
            }

            e.Handled = true;

            this.DoPlayTouchDown(layoutControl, e);
        }

        /// <summary>
        /// 마우스 움직일 때 구현.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public override void MouseMoveProcess(object sender, MouseEventArgs e)
        {
            if(sender is MapControl)
            {
                e.Handled = true;
                return;
            }

            var layoutControl = sender as LayoutControl;

            if (layoutControl == null)
            {
                return;
            }

            e.Handled = true;

            this.DoPlayMouseMove(layoutControl, e);
        }

        public override void StylusMoveProcess(object sender, StylusEventArgs e)
        {
            if (e.InAir == false)
            {
                if (sender is MapControl)
                {
                    e.Handled = true;
                    return;
                }

                var layoutControl = sender as LayoutControl;

                if (layoutControl == null)
                {
                    return;
                }

                e.Handled = true;

                this.DoStylusMove(layoutControl, e);
            }
        }

        public override void TouchMoveProcess(object sender, TouchEventArgs e)
        {
            if (sender is MapControl)
            {
                e.Handled = true;
                return;
            }

            var layoutControl = sender as LayoutControl;

            if (layoutControl == null)
            {
                return;
            }

            e.Handled = true;

            this.DoTouchMove(layoutControl, e);
        }

        /// <summary>
        /// 왼쪽 마우스 버튼 업 구현.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public override void MouseLeftButtonUpProcess(object sender, MouseButtonEventArgs e)
        {
            if (sender is MapControl)
            {
                e.Handled = true;
                return;
            }

            var layoutControl = sender as LayoutControl;

            if (layoutControl == null)
            {
                return;
            }

            e.Handled = true;
            try
            {
                this.DoPlayMouseLeftButtonUp(layoutControl, e);
            }
            catch (Exception ex)
            {
                InnotiveDebug.Log.Error("PlayingProcess.cs - MouseLeftButtonUpProcess()", ex);
            }
            finally
            {
                if (layoutControl.IsMouseCaptured)
                {
                    layoutControl.ReleaseMouseCapture();
                }
            }

            // by Alvin (100920) : 레이아웃의 마우스 캡쳐때문에 이미지 컨트롤의 마우스 리브와 마우스 버튼 업 이벤트가 일어나지 않아
            // 이미지 컨트롤에 한하여 마우스 오버 체크하는 동작을 호출하여 처리함. 이미지 컨트롤이 많을 시 문제가 발생할 수도......
            List<UIElement> elementCollection = InnoControlUtil.GetAllLogicalChildren(layoutControl);
            foreach (var uiElement in elementCollection)
            {
                
            }
        }

        public override void StylusUpProcess(object sender, StylusEventArgs e)
        {
            if (e.InAir == false)
            {
                if (sender is MapControl)
                {
                    e.Handled = true;
                    return;
                }

                var layoutControl = sender as LayoutControl;

                if (layoutControl == null)
                {
                    return;
                }

                e.Handled = true;
                try
                {
                    this.DoPlayStylusUp(layoutControl, e);
                }
                catch (Exception ex)
                {
                    InnotiveDebug.Log.Error("PlayingProcess.cs - StylusUpProcess()", ex);
                }
                finally
                {
                    if (layoutControl.IsStylusCaptured)
                    {
                        layoutControl.ReleaseStylusCapture();
                    }
                }

                // by Alvin (100920) : 레이아웃의 마우스 캡쳐때문에 이미지 컨트롤의 마우스 리브와 마우스 버튼 업 이벤트가 일어나지 않아
                // 이미지 컨트롤에 한하여 마우스 오버 체크하는 동작을 호출하여 처리함. 이미지 컨트롤이 많을 시 문제가 발생할 수도......
                List<UIElement> elementCollection = InnoControlUtil.GetAllLogicalChildren(layoutControl);
                foreach (var uiElement in elementCollection)
                {
                    
                }
            }
        }

        public override void TouchUpProcess(object sender, TouchEventArgs e)
        {
            if (sender is MapControl)
            {
                e.Handled = true;
                return;
            }

            var layoutControl = sender as LayoutControl;

            if (layoutControl == null)
            {
                return;
            }

            e.Handled = true;

            try
            {
                this.DoPlayTouchUp(layoutControl, e);
            }
            catch (Exception ex)
            {
                InnotiveDebug.Log.Error("PlayingProcess.cs - TouchUpProcess()", ex);
            }
            finally
            {
                if (layoutControl.IsMouseCaptured)
                {
                    layoutControl.ReleaseMouseCapture();
                }
            }

            // by Alvin (100920) : 레이아웃의 마우스 캡쳐때문에 이미지 컨트롤의 마우스 리브와 마우스 버튼 업 이벤트가 일어나지 않아
            // 이미지 컨트롤에 한하여 마우스 오버 체크하는 동작을 호출하여 처리함. 이미지 컨트롤이 많을 시 문제가 발생할 수도......
            List<UIElement> elementCollection = InnoControlUtil.GetAllLogicalChildren(layoutControl);
            foreach (var uiElement in elementCollection)
            {
                
            }
        }

        /// <summary>
        /// 마우스 휠 구현.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public override void MouseWheelProcess(object sender, MouseWheelEventArgs e)
        {
            if(sender is MapControl)
            {
                e.Handled = true;
                return;
            }


            var layoutControl = sender as LayoutControl;

            if (layoutControl == null)
            {
                return;
            }

            e.Handled = true;

            if (PlayerCommonsConfig.Instance.LayoutUseMouseWheel)
            {
                try
                {
                    this.DoPlayMouseWheel(layoutControl, e);
                }
                catch (Exception ex)
                {
                    InnotiveDebug.Log.Error("playingProcess.cs - MouseWheelProcess()", ex);
                }
                finally
                {
                    if (layoutControl.IsMouseCaptured)
                    {
                        layoutControl.ReleaseMouseCapture();
                    }
                }
            }

            // by Alvin (100920) : 레이아웃의 마우스 캡쳐때문에 이미지 컨트롤의 마우스 리브와 마우스 버튼 업 이벤트가 일어나지 않아
            // 이미지 컨트롤에 한하여 마우스 오버 체크하는 동작을 호출하여 처리함. 이미지 컨트롤이 많을 시 문제가 발생할 수도......
            List<UIElement> elementCollection = InnoControlUtil.GetAllLogicalChildren(layoutControl);
            foreach (var uiElement in elementCollection)
            {
                
            }
        }

        #endregion

        #region Touch

        public override void ManipulationStarting(object sender, ManipulationStartingEventArgs e)
        {
            if (sender is MapControl)
            {
                e.Handled = true;
                return;
            }

            var layoutControl = sender as LayoutControl;

            if (layoutControl == null)
            {
                return;
            }

            e.ManipulationContainer = layoutControl;
            e.Handled = true;

            this.DoPlayManipulationStarting(layoutControl, e);
        }

        public override void ManipulationDelta(object sender, ManipulationDeltaEventArgs e)
        {
            if (sender is MapControl)
            {
                e.Handled = true;
                return;
            }

            var layoutControl = sender as LayoutControl;

            if (layoutControl == null)
            {
                return;
            }

            e.Handled = true;

            this.DoPlayManipulationDelta(layoutControl, e);
        }

        public override void ManipulationInertiaStarting(object sender, ManipulationInertiaStartingEventArgs e)
        {
            if (sender is MapControl)
            {
                e.Handled = true;
                return;
            }

            var layoutControl = sender as LayoutControl;

            if (layoutControl == null)
            {
                return;
            }

            e.Handled = true;

            this.DoPlayManipulationInertiaStarting(layoutControl, e);
        }

        #endregion // Touch

        /// <summary>
        /// The ActionRaise.
        /// </summary>
        /// <param name="lc">
        /// The layoutControl.
        /// </param>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="triggerEventType">
        /// The trigger event type.
        /// </param>
        /// <returns>
        /// the args.IsExcuted.
        /// </returns>
        protected bool ActionRaise(LayoutControl layoutControl, object sender, TriggerEventType triggerEventType)
        {
            //내부 또는 외부 Sync를 보낼수 있는 상태인지 Check (Stage쪽 Sync Button 활성화 여부)
            //if (!CanSendLocalAndRemoteSync(layoutControl))
            //    return false;

            var element = sender as BaseControl;

            if (element == null)
            {
                return false;
            }

            Guid trigerElementGuid = GuidManager.GetSyncGuid(element);
            
            if (trigerElementGuid == Guid.Empty)
            {
                return false;
            }

            ActionEventArgs args = null;
            if (CanSendRemoteSync(layoutControl))
                args = new ActionEventArgs(triggerEventType);
            else
                args = new ActionEventArgs(triggerEventType, false);
            
            layoutControl.ActionRaise(sender, args);

            return args.IsExcuted;
        }

        private static void DoPanningStarted(BaseControl element)
        {
            //내부 또는 외부 Sync를 보낼수 있는 상태인지 Check (Stage쪽 Sync Button 활성화 여부)
            //여기서 CanSendRemoteSync()를 호출하게 되면 내부 Sync도 동작하지 않음 !! 내부 Sync를 동작하게 하기 위해서 Event를 받는 부분에서 외부 Sync만 막음 !!
            //if (CanSendLocalAndRemoteSync(element as LayoutControl))
            //{
                element.RaiseEvent(new PanningStartedRoutedEventArgs(ZoomControl.ePanningStartedEvent, element));
            //}
        }

        private static void DoPanningEnded(BaseControl element)
        {
            //내부 또는 외부 Sync를 보낼수 있는 상태인지 Check (Stage쪽 Sync Button 활성화 여부)
            //여기서 CanSendRemoteSync()를 호출하게 되면 내부 Sync도 동작하지 않음 !! 내부 Sync를 동작하게 하기 위해서 Event를 받는 부분에서 외부 Sync만 막음 !!
            //if (CanSendLocalAndRemoteSync(element as LayoutControl))
            //{
                element.RaiseEvent(new PanningEndedRoutedEventArgs(ZoomControl.ePanningEndedEvent, element));
            //}

            ZoomPanSmoothManager.End(element);
        }

        private static void DoZoomIn(BaseControl element)
        {
            Point clickPoint = ((LayoutControl)element).GetMousePosition();
            Rect desireRect = ((LayoutControl)element).ZoomIn(clickPoint);

            if (Rect.Empty != desireRect)
            {
                //내부 또는 외부 Sync를 보낼수 있는 상태인지 Check (Stage쪽 Sync Button 활성화 여부)
                //여기서 CanSendRemoteSync()를 호출하게 되면 내부 Sync도 동작하지 않음 !! 내부 Sync를 동작하게 하기 위해서 Event를 받는 부분에서 외부 Sync만 막음 !!
                //if (CanSendLocalAndRemoteSync(element as LayoutControl))
                //{
                    element.RaiseEvent(new RectRoutedEventArgs(LayoutControl.eZoomInEvent, element, desireRect));
                //}
            }
        }

        private static void DoZoomOut(BaseControl element)
        {
            Point clickPoint = ((LayoutControl)element).GetMousePosition();
            Rect desireRect = ((LayoutControl)element).ZoomOut(clickPoint);

            if (Rect.Empty != desireRect)
            {
                //내부 또는 외부 Sync를 보낼수 있는 상태인지 Check (Stage쪽 Sync Button 활성화 여부)
                //여기서 CanSendRemoteSync()를 호출하게 되면 내부 Sync도 동작하지 않음 !! 내부 Sync를 동작하게 하기 위해서 Event를 받는 부분에서 외부 Sync만 막음 !!
                //if (CanSendLocalAndRemoteSync(element as LayoutControl))
                {
                    // 줌아웃 이벤트를 발생시킨다.
                    element.RaiseEvent(new RectRoutedEventArgs(LayoutControl.eZoomOutEvent, element, desireRect));
                }
            }
        }

        #region Do Play Right Button Down

        private void DoPlayMouseRightButtonDown(LayoutControl layoutControl, MouseButtonEventArgs e)
        {
            if (layoutControl == null)
            {
                return;
            }
            
            this.lastMousePoint = layoutControl.GetMousePosition();
            this.isMouseMove = false;
        }

        private void DoPlayMouseRightButtonUp(LayoutControl layoutControl, MouseButtonEventArgs e)
        {
            if (layoutControl == null)
            {
                return;
            }

            // 마우스가 움직인 적이 없다면 액션을 발생시킨다.
            if (!this.isMouseMove)
            {
                var isAction = this.ActionRaise(layoutControl, this.triggerElement, TriggerEventType.MouseClick);
                this.triggerElement = null;

                if (isAction)
                {
                    this.isMouseMove = false;
                    this.isMouseLButtonDowned = false;
                    return;
                }
            }

            // 줌을 막은 레이아웃이면 빠져나간다.
            if (!layoutControl.IsZoomEnabled)
            {
                return;
            }

            // LockType이 설정되어 있다면 ZoomOut을 실행하지 않는다.
            // by jhlee (BaseLayoutControl.LockType)
            if (BaseLayoutControl.GetLockType(layoutControl) != LockTypes.None)
            {
                return;
            }

            // 마우스가 움직였다면 클릭이 아니므로 빠져나간다.
            if (this.isMouseMove)
            {
                return;
            }

            DoZoomOut(layoutControl);

            this.isMouseMove = false;
            this.isMouseLButtonDowned = false;
        }

        #endregion

        #region Do Play Left Button Down

        private void DoPlayLeftButtonDown(LayoutControl layoutControl, object source)
        {
            if (layoutControl == null)
            {
                return;
            }

            this.triggerElement = source as BaseControl;

            this.downMousePoint = this.lastMousePoint = layoutControl.GetMousePosition();
            this.isMouseMove = false;
            this.isMouseLButtonDowned = true;
        }

        private void DoPlayMouseLeftButtonDown(LayoutControl layoutControl, MouseButtonEventArgs e)
        {
            this.DoPlayLeftButtonDown(layoutControl, e.Source);
        }

        private void DoPlayStylusDown(LayoutControl layoutControl, StylusDownEventArgs e)
        {
            this.DoPlayLeftButtonDown(layoutControl, e.Source);
        }

        private void DoPlayTouchDown(LayoutControl layoutControl, TouchEventArgs e)
        {
            this.DoPlayLeftButtonDown(layoutControl, e.Source);
        }

        private void DoPlayMove(LayoutControl layoutControl)
        {
            if (layoutControl == null)
            {
                return;
            }

            if (this.isMouseLButtonDowned == false)
            {
                return;
            }

            // LockType이 설정되어 있다면 ZoomIn을 실행하지 않는다.
            // by jhlee (BaseLayoutControl.LockType)
            if (BaseLayoutControl.GetLockType(layoutControl) != LockTypes.None)
            {
                return;
            }

            if (this.isMouseMove == false)
            {
                Point currentPoint = layoutControl.GetMousePosition();

                double gapX = Math.Abs(currentPoint.X - this.downMousePoint.X);
                double gapY = Math.Abs(currentPoint.Y - this.downMousePoint.Y);

                if (gapX < PlayerCommonsConfig.Instance.PanningStartPixel && gapY < PlayerCommonsConfig.Instance.PanningStartPixel)
                {
                    return;
                }

                //Panning 시작임 !!
                DoPanningStarted(layoutControl);
            }

            var movePosition = this.lastMousePoint;
            this.lastMousePoint = layoutControl.GetMousePosition();
            movePosition.Offset(-this.lastMousePoint.X, -this.lastMousePoint.Y);

            if ((movePosition.X == 0) && (movePosition.Y == 0))
            {
                return;
            }

            this.isMouseMove = true;

            if (layoutControl.IsZoomEnabled == false)
            {
                return;
            }

            Viewbox viewBox = layoutControl.InsideViewbox;
            var desireLeft = Canvas.GetLeft(viewBox) - movePosition.X;
            var desireTop = Canvas.GetTop(viewBox) - movePosition.Y;
            layoutControl.LayoutActualRect = new Rect(desireLeft, desireTop, viewBox.Width, viewBox.Height);

            //내부 또는 외부 Sync를 보낼수 있는 상태인지 Check (Stage쪽 Sync Button 활성화 여부)
            //여기서 CanSendRemoteSync()를 호출하게 되면 내부 Sync도 동작하지 않음 !! 내부 Sync를 동작하게 하기 위해서 Event를 받는 부분에서 외부 Sync만 막음 !!
            //if (CanSendLocalAndRemoteSync(layoutControl))
            {
                // LayoutActualRect Setter에 범위 벗어난 패닝 거르는 코드가 있으므로 다시 가져오면 걸러진 영역이 반환된다.
                layoutControl.RaiseEvent(new RectRoutedEventArgs(LayoutControl.eSettingRectEvent, layoutControl, layoutControl.LayoutActualRect));
            }
        }

        private void DoPlayMouseMove(LayoutControl layoutControl, MouseEventArgs e)
        {
            this.DoPlayMove(layoutControl);
        }

        private void DoStylusMove(LayoutControl layoutControl, StylusEventArgs e)
        {
            this.DoPlayMove(layoutControl);
        }

	    private void DoTouchMove(LayoutControl layoutControl, TouchEventArgs e)
        {
            this.DoPlayMove(layoutControl);
        }

        private void DoPlayLeftButtonUp(LayoutControl layoutControl)
        {
            if (layoutControl == null)
            {
                return;
            }

            // 마우스가 움직인 적이 없다면 액션을 발생시킨다.
            if (!this.isMouseMove)
            {
                bool isAction = this.ActionRaise(layoutControl, this.triggerElement, TriggerEventType.MouseClick);
                this.triggerElement = null;

                if (isAction)
                {
                    this.isMouseMove = false;
                    this.isMouseLButtonDowned = false;
                    return;
                }
            }

            if (!layoutControl.IsZoomEnabled)
            {
                this.isMouseMove = false;
                this.isMouseLButtonDowned = false;
                return;
            }

            if (!this.isMouseLButtonDowned)
            {
                this.isMouseMove = false;
                this.isMouseLButtonDowned = false;
                return;
            }

            // by jhlee (BaseLayoutControl.LockType)
            if (BaseLayoutControl.GetLockType(layoutControl) != LockTypes.None)
            {
                this.isMouseMove = false;
                this.isMouseLButtonDowned = false;
                return;
            }

            if (this.isMouseMove)
            {
                DoPanningEnded(layoutControl);
            }
            else
            {
                DoZoomIn(layoutControl);
            }

            this.isMouseMove = false;
            this.isMouseLButtonDowned = false;
        }

        
        private void DoPlayMouseLeftButtonUp(LayoutControl layoutControl, MouseButtonEventArgs e)
        {
            this.DoPlayLeftButtonUp(layoutControl);
        }

        private void DoPlayStylusUp(LayoutControl layoutControl, StylusEventArgs e)
        {
            this.DoPlayLeftButtonUp(layoutControl);
        }

        private void DoPlayTouchUp(LayoutControl layoutControl, TouchEventArgs e)
        {
            this.DoPlayLeftButtonUp(layoutControl);
        }
        

        private void DoPlayMouseWheel(LayoutControl layoutControl, MouseWheelEventArgs e)
        {
            if (layoutControl == null)
            {
                return;
            }

            if (e.Delta > 0)
            {
                DoZoomIn(layoutControl);
            }
            else
            {
                DoZoomOut(layoutControl);
            }
        }

        //public static bool CanSendLocalAndRemoteSync(LayoutControl layoutControl)
        //Layout과 부모 MultiGrid, 그리고 외부 조건을 체크해서 DP쪽에 Sync Data를 보낼수 있는 상황인지 검사함
        public static bool CanSendRemoteSync(LayoutControl layoutControl)
        {
            if (layoutControl == null)
                return true;

            //iCommand, iViewer가 아닌 경우 Local Sync가 무조건 동작해야 함 !!
            if (Public.GetProgramType() != CommonUtils.Models.ProgramType.iCommand && Public.GetProgramType() != CommonUtils.Models.ProgramType.iViewer)
                return true;

            //config쪽 SyncToggleButton의 Visible이 false인 경우 무조건 Sync를 보내야 함
            if (!PlayerCommonsConfig.Instance.UseSyncToggleButton)
                return true;

            

            return false;
        }

        #endregion

        #region Do Play Touch

        private void DoPlayManipulationStarting(LayoutControl layoutControl, ManipulationStartingEventArgs e)
        {
            this.triggerElement = e.Source as BaseControl;

            e.ManipulationContainer = layoutControl;
            e.Handled = true;

            //this.downMousePoint = this.lastMousePoint = layoutControl.GetMousePosition();
            this.isMouseMove = false;
            this.isMouseLButtonDowned = true;
        }

        private void DoPlayManipulationDelta(LayoutControl layoutControl, ManipulationDeltaEventArgs e)
        {
            System.Diagnostics.Debug.WriteLine("DoPlayManipulationDelta - DeltaManipulation.Translation : " + e.DeltaManipulation.Translation.X + ", " + e.DeltaManipulation.Translation.Y);
            System.Diagnostics.Debug.WriteLine("DoPlayManipulationDelta - ManipulationOrigin : " + e.ManipulationOrigin.X + ", " + e.ManipulationOrigin.Y);
            System.Diagnostics.Debug.WriteLine("DoPlayManipulationDelta - DeltaManipulation.Scale : " + e.DeltaManipulation.Scale.X + ", " + e.DeltaManipulation.Scale.Y);

            if (layoutControl == null)
            {
                return;
            }

            if (this.isMouseLButtonDowned == false)
            {
                return;
            }

            // LockType이 설정되어 있다면 ZoomIn을 실행하지 않는다.
            // by jhlee (BaseLayoutControl.LockType)
            if (BaseLayoutControl.GetLockType(layoutControl) != LockTypes.None)
            {
                return;
            }

            if (this.isMouseMove == false)
            {
                //Point currentPoint = layoutControl.GetMousePosition();

                if (e.DeltaManipulation.Translation.X < PlayerCommonsConfig.Instance.PanningStartPixel && 
                    e.DeltaManipulation.Translation.Y < PlayerCommonsConfig.Instance.PanningStartPixel)
                {
                    return;
                }

                //Panning 시작임 !!
                DoPanningStarted(layoutControl);
            }

            //var movePosition = this.lastMousePoint;
            //this.lastMousePoint = new Point(e.DeltaManipulation.Translation.X, e.DeltaManipulation.Translation.Y);
            //movePosition.Offset(-this.lastMousePoint.X, -this.lastMousePoint.Y);

            if ((e.DeltaManipulation.Translation.X == 0) && (e.DeltaManipulation.Translation.Y == 0))
            {
                return;
            }

            this.isMouseMove = true;

            if (layoutControl.IsZoomEnabled == false)
            {
                return;
            }

            Viewbox viewBox = layoutControl.InsideViewbox;
            var desireLeft = Canvas.GetLeft(viewBox) + e.DeltaManipulation.Translation.X;
            var desireTop = Canvas.GetTop(viewBox) + e.DeltaManipulation.Translation.Y;
            layoutControl.LayoutActualRect = new Rect(desireLeft, desireTop, viewBox.Width, viewBox.Height);
            
            //내부 또는 외부 Sync를 보낼수 있는 상태인지 Check (Stage쪽 Sync Button 활성화 여부)
            //여기서 CanSendRemoteSync()를 호출하게 되면 내부 Sync도 동작하지 않음 !! 내부 Sync를 동작하게 하기 위해서 Event를 받는 부분에서 외부 Sync만 막음 !!
            //if (CanSendLocalAndRemoteSync(layoutControl))
            {
                // LayoutActualRect Setter에 범위 벗어난 패닝 거르는 코드가 있으므로 다시 가져오면 걸러진 영역이 반환된다.
                layoutControl.RaiseEvent(new RectRoutedEventArgs(LayoutControl.eSettingRectEvent, layoutControl, layoutControl.LayoutActualRect));
            }

            e.Handled = true;
        }

        private void DoPlayManipulationInertiaStarting(LayoutControl layoutControl, ManipulationInertiaStartingEventArgs e)
        {            
            System.Diagnostics.Debug.WriteLine("DoPlayManipulationInertiaStarting - ManipulationOrigin : " + e.ManipulationOrigin.X + ", " + e.ManipulationOrigin.Y);

            if (layoutControl == null)
            {
                return;
            }

            // 마우스가 움직인 적이 없다면 액션을 발생시킨다.
            if (!this.isMouseMove)
            {
                bool isAction = this.ActionRaise(layoutControl, this.triggerElement, TriggerEventType.MouseClick);
                this.triggerElement = null;

                if (isAction)
                {
                    this.isMouseMove = false;
                    this.isMouseLButtonDowned = false;
                    return;
                }
            }

            if (!layoutControl.IsZoomEnabled)
            {
                this.isMouseMove = false;
                this.isMouseLButtonDowned = false;
                return;
            }

            if (!this.isMouseLButtonDowned)
            {
                this.isMouseMove = false;
                this.isMouseLButtonDowned = false;
                return;
            }

            // by jhlee (BaseLayoutControl.LockType)
            if (BaseLayoutControl.GetLockType(layoutControl) != LockTypes.None)
            {
                this.isMouseMove = false;
                this.isMouseLButtonDowned = false;
                return;
            }

            if (this.isMouseMove)
            {
                DoPanningEnded(layoutControl);
            }
            else
            {                
                DoZoomIn(layoutControl);
            }

            this.isMouseMove = false;
            this.isMouseLButtonDowned = false;

            e.Handled = true;
        }

        #endregion // Do Play Touch
    }
}

