﻿
namespace GMap.NET.MapProviders
{
    using System;

    /// <summary>
    /// ArcGIS_World_Topo_Map provider, http://server.arcgisonline.com
    /// </summary>
    public class ArcGIS_Userdefined_MapProvider : ArcGISMapMercatorProviderBase
    {
        public static readonly ArcGIS_Userdefined_MapProvider Instance;
        public const string DEFAULT_ARCGIS_URL = "http://server.arcgisonline.com/ArcGIS/rest/services/World_Topo_Map/MapServer/tile/{0}/{1}/{2}";

        ArcGIS_Userdefined_MapProvider()
        {
        }

        static ArcGIS_Userdefined_MapProvider()
        {
            Instance = new ArcGIS_Userdefined_MapProvider();

            // TODO : Custom Url 을 사용할 경우 {0}{1}{2} (ZoomLevel, PositionX, PositionY) 를 반드시 Url에 포함해야함. 문서화 필요. - by shwlee
            UrlFormat = DEFAULT_ARCGIS_URL;
        }

        #region GMapProvider Members

        readonly Guid id = new Guid("82F8256D-087D-44A9-B5CB-295FE3C38F7A");
        public override Guid Id
        {
            get
            {
                return id;
            }
        }

        readonly string name = "ArcGIS_Userdefine_Map";
        public override string Name
        {
            get
            {
                return name;
            }
        }

        public override PureImage GetTileImage(GPoint pos, int zoom)
        {
            string url = MakeTileImageUrl(pos, zoom, LanguageStr);

            return GetTileImageUsingHttp(url);
        }

        #endregion

        string MakeTileImageUrl(GPoint pos, int zoom, string language)
        {
            // http://services.arcgisonline.com/ArcGIS/rest/services/World_Topo_Map/MapServer/tile/0/0/0jpg

            return string.Format(UrlFormat, zoom, pos.Y, pos.X);
        }

        static public string UrlFormat { get; set; }
    }
}