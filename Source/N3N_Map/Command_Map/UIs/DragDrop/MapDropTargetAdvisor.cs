// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MapDropTargetAdvisor.cs" company="Innotive Inc. Korea">
//   Copyright ⓒ Innotive inc. Korea 2011
// </copyright>
// <summary>
//   Defines the MapDropTargetAdvisorDropCompletedEventArgs type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.Console.UIs.DragDrop
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Media;
    using System.Windows.Media.Animation;
    using System.Windows.Shapes;

    using Innotive.InnoWatch.Commons.Utils;
    using Innotive.InnoWatch.Console.Models;
    using Innotive.InnoWatch.DLLs.LayoutControls;
    using Innotive.InnoWatch.DLLs.MapControls;

    /// <summary>
    /// LocationSettingPopupWindow에서 사용하는 드랍 타겟 어드바이저.
    /// </summary>
    public class MapDropTargetAdvisor : IDropTargetAdvisor
    {
        private static DataFormat supportedFormatForCamera = DataFormats.GetDataFormat("Innotive.Camera");

        /// <summary>
        /// Initializes a new instance of the <see cref="MapDropTargetAdvisor"/> class.
        /// </summary>
        public MapDropTargetAdvisor()
        {
            this.ApplyMouseOffset = false;
        }

        /// <summary>
        /// DropCompleted event.
        /// </summary>
        public event EventHandler<MapDropTargetAdvisorDropCompletedEventArgs> DropCompleted = null;

        /// <summary>
        /// Gets or sets TargetUI.
        /// 실제로는 MapControl 이다.
        /// </summary>
        public UIElement TargetUI { get; set; }

        /// <summary>
        /// Gets a value indicating whether ApplyMouseOffset.
        /// </summary>
        public bool ApplyMouseOffset { get; private set; }

        /// <summary>
        /// 드래그 유효성 검사.
        /// </summary>
        /// <param name="obj">
        /// The obj.
        /// </param>
        /// <returns>
        /// 검사 결과.
        /// </returns>
        public bool IsValidDataObject(IDataObject obj)
        {
            if (!obj.GetDataPresent(supportedFormatForCamera.Name))
            {
                return false;
            }

            var layoutControl = this.TargetUI as LayoutControl;
            if (layoutControl == null)
            {
                return false;
            }

            foreach (var mapControl in layoutControl.GetElements().OfType<MapControl>())
            {
                var cameraList = obj.GetData(supportedFormatForCamera.Name) as List<Camera>;
                if (cameraList != null)
                {
                    var camera = cameraList[0];

                    foreach (var cameraControl in mapControl.GetCameraElements())
                    {
                        if (string.Compare(cameraControl.ID, camera.Id) == 0)
                        {
                            return false;
                        }
                    }
                }
            }

            return true;
        }

        /// <summary>
        /// </summary>
        /// <param name="obj">
        /// The obj.
        /// </param>
        /// <param name="dropPoint">
        /// The drop point.
        /// </param>
        public void OnDropCompleted(IDataObject obj, Point dropPoint)
        {
            if (this.DropCompleted != null)
            {
                this.DropCompleted(this.TargetUI, new MapDropTargetAdvisorDropCompletedEventArgs(obj.GetData(supportedFormatForCamera.Name), dropPoint));
            }
        }

        /// <summary>
        /// </summary>
        /// <param name="obj">
        /// The obj.
        /// </param>
        /// <returns>
        /// </returns>
        public UIElement GetVisualFeedback(IDataObject obj)
        {
            var element = this.ExtractElement(obj);
            var rect = new Rectangle();

            
            if (element is TextBlock)
            {
                rect.Fill = new VisualBrush(element);
                rect.Width = (element as TextBlock).Width;
                rect.Height = (element as TextBlock).Height;
            }

            rect.Opacity = 0.5;
            rect.IsHitTestVisible = false;

            var anim = new DoubleAnimation(0.75, new Duration(TimeSpan.FromMilliseconds(500)))
                { From = 0.25, AutoReverse = true, RepeatBehavior = RepeatBehavior.Forever };
            rect.BeginAnimation(UIElement.OpacityProperty, anim);

            return rect;
        }

        /// <summary>
        /// </summary>
        /// <returns>
        /// </returns>
        public UIElement GetTopContainer()
        {
            return Application.Current.MainWindow.Content as UIElement;
        }

        private FrameworkElement ExtractElement(IDataObject obj)
        {
            return obj.GetData(supportedFormatForCamera.Name) as FrameworkElement;
        }
    }

    /// <summary>
    /// MapDropTargetAdvisorDropCompletedEventArgs Class.
    /// </summary>
    public class MapDropTargetAdvisorDropCompletedEventArgs : EventArgs
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MapDropTargetAdvisorDropCompletedEventArgs"/> class.
        /// </summary>
        /// <param name="source">
        /// The source.
        /// </param>
        /// <param name="targetDropPoint">
        /// The target drop point.
        /// </param>
        public MapDropTargetAdvisorDropCompletedEventArgs(object source, Point targetDropPoint)
        {
            this.Source = source;
            this.TargetDropPoint = targetDropPoint;
        }

        /// <summary>
        /// Gets or sets Source.
        /// </summary>
        public object Source { get; protected set; }

        /// <summary>
        /// Gets or sets TargetDropPoint.
        /// </summary>
        public Point TargetDropPoint { get; protected set; }
    }
}