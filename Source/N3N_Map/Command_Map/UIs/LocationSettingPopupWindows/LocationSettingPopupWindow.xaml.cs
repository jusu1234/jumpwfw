﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LocationSettingPopupWindow.xaml.cs" company="Innotive Inc. Korea">
//   Copyright ⓒ Innotive inc. Korea 2011
// </copyright>
// <summary>
//   Interaction logic for LocationSettingPopupWindow.xaml.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.Console.UIs.LocationSettingPopupWindows
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;
    using GMap.NET.MapProviders;
    using Innotive.InnoWatch.Commons.BaseControls;
    using Innotive.InnoWatch.Commons.Configs;
    using Innotive.InnoWatch.Commons.EventArguments;
    using Innotive.InnoWatch.Commons.Map;
    using Innotive.InnoWatch.Commons.Utils;
    using Innotive.InnoWatch.Commons.Utils.Http;
    using Innotive.InnoWatch.CommonUtils.Models.Sync.Control;
    using Innotive.InnoWatch.Console.Configs;
    using Innotive.InnoWatch.Console.DataAccess;
    using Innotive.InnoWatch.Console.Models;
    using Innotive.InnoWatch.Console.UIs.DragDrop;
    using Innotive.InnoWatch.Console.UIs.LocationSettingPopupWindows.Trackers;
    using Innotive.InnoWatch.DLLs.BookmarkControls;
    using Innotive.InnoWatch.DLLs.CameraControls;
    using Innotive.InnoWatch.DLLs.LayoutControls;
    using Innotive.InnoWatch.DLLs.MapControls;    
    using Innotive.InnoWatch.PlayerCommons;
    using Innotive.InnoWatch.PlayerCommons.Configs;
    using TreeListBoxControls;
    using Innotive.InnoWatch.FileImport.Model;
    using Innotive.InnoWatch.FileImport.View;

    /// <summary>
    /// Interaction logic for LocationSettingPopupWindow.xaml.
    /// </summary>
    public partial class LocationSettingPopupWindow
    {
        /// <summary>
        ///   카메라 리스트.
        /// </summary>
        private readonly Cameras cameras = new Cameras();
        private readonly XamlInfoList _xamlInfoList = new XamlInfoList();

       


        /// <summary>
        ///   Initializes a new instance of the <see cref = "LocationSettingPopupWindow" /> class.
        /// </summary>
        public LocationSettingPopupWindow()
        {
            this.InitializeComponent();

            this.xMapCanvas.Loaded += this.XMapCanvasLoaded;
            this.xMapCanvas.ClipToBounds = true;

            this.xCameraGroupList.MouseDoubleClick += this.XCameraGroupListUserControlMouseDoubleClick;

            this.SelectedElements = new SelectedElements(this);
            this.TrackerManager = new TrackerManager(this, this.xMapCanvas);
        }

        /// <summary>
        ///   Gets MapControl.
        /// </summary>
        public MapControl MapControl { get; private set; }

        /// <summary>
        ///   Gets MapLayoutControl.
        /// </summary>
        public LayoutControl MapLayoutControl { get; private set; }

        /// <summary>
        ///   Gets SelectedElements.
        /// </summary>
        public SelectedElements SelectedElements { get; private set; }

        /// <summary>
        ///   Gets TrackerManager.
        /// </summary>
        public TrackerManager TrackerManager { get; private set; }

        //public XamlInfoList XamlInfoList
        //{
        //    get { return _xamlInfoList; }
        //} 

        /// <summary>
        /// Gets or sets a value indicating whether ShowPinObject.
        /// </summary>
        public bool ShowPinObject
        {
            get
            {
                Debug.Assert(this.MapControl != null, "맵 컨트롤이 NULL입니다.");
                return this.MapControl.ShowPinObject;
            }

            set
            {
                Debug.Assert(this.MapControl != null, "맵 컨트롤이 NULL입니다.");
                this.MapControl.ShowPinObject = value;
            }
        }

        /// <summary>
        /// The direct move.
        /// </summary>
        /// <param name="element">
        /// The element.
        /// </param>
        /// <param name="startInFocusedLayout">
        /// The start in focused layout.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        public void DirectMove(BaseControl element, Point startInFocusedLayout, MouseEventArgs e)
        {
            var currentPointInFocusedLayout = e.GetPosition(this.xMapCanvas);

            // mouse이동량을 overlay canvas의 좌표계에서 viewbox좌표계로 변경함 !!
            var ratioX = InnoConvertUtil.GetRatioElementX(element, this);
            var ratioY = InnoConvertUtil.GetRatioElementY(element, this);

            var movePointInfocusedLayout = new Point(
                (currentPointInFocusedLayout.X - startInFocusedLayout.X) * ratioX, 
                (currentPointInFocusedLayout.Y - startInFocusedLayout.Y) * ratioY);

            this.SelectedElements.Move(movePointInfocusedLayout);

            if (element.ParentBaseLayoutControl is MapControl)
            {
                foreach (var selectedElement in this.SelectedElements)
                {
                    var gisRect = CanvasToGis(
                        this.MapControl, 
                        selectedElement.Left, 
                        selectedElement.Top, 
                        selectedElement.Left + selectedElement.Width, 
                        selectedElement.Top + selectedElement.Height);

                    selectedElement.LeftGis = gisRect.Left;
                    selectedElement.TopGis = gisRect.Top;
                    selectedElement.RightGis = gisRect.Right;
                    selectedElement.BottomGis = gisRect.Bottom;
                }

                this.SelectedElements.SetGisPosition();
            }
        }

        /// <summary>
        /// 싱글 셀렉션.
        /// </summary>
        /// <param name="control">
        /// The control.
        /// </param>
        public void ElementSelect(BaseControl control)
        {
            // 선택된 엘레멘트이 FocusedLayout이면 선택된 모든 엘레멘트을 해제하고 FocusedBaseLayout 만 선택된다.
            if (this.MapLayoutControl.Equals(control))
            {
                this.SelectedElements.Clear();
                this.SelectedElements.Add(control);
                this.TrackerManager.AddElements(this.SelectedElements);
                return;
            }

            if (control.IsLockEdit || !control.EditVisible)
            {
                this.SelectedElements.Clear();
                this.SelectedElements.Add(this.MapLayoutControl);
                this.TrackerManager.AddElements(this.SelectedElements);
                return;
            }

            // 좌측 컨트롤키가 눌려있을 경우...
            if (Keyboard.IsKeyDown(Key.LeftCtrl))
            {
                // 이미 선택되어있던 엘레멘트이면 선택을 해제시킨다.
                if (this.SelectedElements.Contains(control))
                {
                    this.SelectedElements.Remove(control);
                    this.TrackerManager.AddElements(this.SelectedElements);
                    return;
                }

                // 북마크 컨트롤이면 아무 처리도 하지 않는다.
                if (control as BookmarkControl != null)
                {
                    return;
                }

                if (this.SelectedElements.Count > 0)
                {
                    // 이미 선택되어있는 엘레먼트가 북마크 컨트롤이면 아무 처리도 하지 않는다.
                    if ((this.SelectedElements[0] as BookmarkControl) != null)
                    {
                        return;
                    }

                    // 이미 선택되어있는 엘레먼트가 FocusedBaseLayout 이면 현재 엘레멘트만 선택된다.
                    if (this.SelectedElements[0] == this.MapLayoutControl)
                    {
                        this.SelectedElements.Clear();
                    }
                }
            }
            else
            {
                // 이미 포함되어 있는 엘레멘트이면 아무 처리도 하지 않는다. (선택된 모든 객체를 드래그 하기 위해서)
                if (this.SelectedElements.Contains(control))
                {
                    this.TrackerManager.AddElements(this.SelectedElements);
                    return;
                }

                this.SelectedElements.Clear();
            }

            this.SelectedElements.Add(control);
            this.TrackerManager.AddElements(this.SelectedElements);
        }

        /// <summary>
        /// 키보드 다운 이벤트.
        /// </summary>
        /// <param name="e">
        /// The e.
        /// </param>
        protected override void OnKeyDown(KeyEventArgs e)
        {
            base.OnKeyDown(e);

            // Ctrl 키만 눌려졌을 경우에 대한 처리
            if (Keyboard.IsKeyDown(Key.LeftCtrl) && !Keyboard.IsKeyDown(Key.LeftShift) &&
                !Keyboard.IsKeyDown(Key.LeftAlt))
            {
                return;
            }

            // Shift 키만 눌려졌을 경우에 대한 처리
            if (!Keyboard.IsKeyDown(Key.LeftCtrl) && Keyboard.IsKeyDown(Key.LeftShift) &&
                !Keyboard.IsKeyDown(Key.LeftAlt))
            {
                return;
            }

            // Alt 키만 눌려졌을 경우에 대한 처리
            if (!Keyboard.IsKeyDown(Key.LeftCtrl) && !Keyboard.IsKeyDown(Key.LeftShift) &&
                Keyboard.IsKeyDown(Key.LeftAlt))
            {
                return;
            }

            // 제어키가 안눌려졌을 경우에 대한 처리
            if (!Keyboard.IsKeyDown(Key.LeftCtrl) && !Keyboard.IsKeyDown(Key.LeftShift) &&
                !Keyboard.IsKeyDown(Key.LeftAlt))
            {
                if (e.Key == Key.Delete)
                {
                    foreach (CameraControl element in SelectedElements)
                    {
                        if (element == null)
                        {
                            continue;
                        }

                        foreach (Camera camera in this.xCameraGroupList.GetAllCameraListExceptFolder())
                        {
                            if (camera.Id == element.ID)
                            {
                                camera.IsLoadedMap = false;
                            }

                        }
                    }

                    this.SelectedElements.DeleteAll();
                    this.TrackerManager.Clear();
                }
            }
        }

        /// <summary>
        /// Canvas 의 Rect 좌표를 입력 받아서, GIS 좌표로 변환.
        /// </summary>
        /// <param name="parentMap">
        /// 부모 MapControl.
        /// </param>
        /// <param name="left">
        /// 좌측 위치.
        /// </param>
        /// <param name="top">
        /// 상단 위치.
        /// </param>
        /// <param name="right">
        /// 우측 위치.
        /// </param>
        /// <param name="bottom">
        /// 하단 위치.
        /// </param>
        /// <returns>
        /// GIS 좌표상의 Rect 값.
        /// </returns>
        private static GisRect CanvasToGis(MapControl parentMap, double left, double top, double right, double bottom)
        {
            var gisLeftTop = parentMap.Map.FromLocalToLatLng(Convert.ToInt32(left), Convert.ToInt32(top));
            var gisRightBottom = parentMap.Map.FromLocalToLatLng(Convert.ToInt32(right), Convert.ToInt32(bottom));

            var gisrect = new GisRect
                {
                   Left = gisLeftTop.Lng, Top = gisLeftTop.Lat, Right = gisRightBottom.Lng, Bottom = gisRightBottom.Lat 
                };

            return gisrect;
        }

        /// <summary>
        /// 그라운드레이아웃과 하위 레이아웃에 이벤트를 연결한다.
        /// </summary>
        /// <param name="groundLayoutControl">
        /// The Layoutcontrol.
        /// </param>
        private void AssignGroundLayoutEventHandler(LayoutControl groundLayoutControl)
        {
            this.AssignLayoutEventHandler(groundLayoutControl);

            foreach (var element in LogicalTreeHelper.GetChildren(groundLayoutControl))
            {
                if (!(element is LayoutControl))
                {
                    continue;
                }

                this.AssignLayoutEventHandler(element as LayoutControl);
            }
        }

        /// <summary>
        /// The assign layout event handler.
        /// </summary>
        /// <param name="layoutControl">
        /// The layout control.
        /// </param>
        private void AssignLayoutEventHandler(LayoutControl layoutControl)
        {
            Debug.Assert(layoutControl != null, "layoutControl is null");

            layoutControl.PreviewMouseDown += this.LastLayoutControlPreviewMouseDown;
            layoutControl.LayoutUpdated += this.LastLayoutControlLayoutUpdated;
            layoutControl.eSelectedElementsChanged += this.LastLayoutControlESelectedElementsChanged;
            layoutControl.eChanged += this._lastLayoutControl_eChanged;
            layoutControl.eZoomEnded += this._lastLayoutControl_eZoomEnded;
            layoutControl.eFocusedLayoutChanged += this._lastLayoutControl_eFocusedLayoutChanged;
        }

        /// <summary>
        /// The control panel check box_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void ControlPanelCheckBoxClick(object sender, RoutedEventArgs e)
        {
            this.MapControl.ShowControlPanel = this.xShowControlPanelCheckBox.IsChecked == true;
        }

        /// <summary>
        /// 카메라 객체 생성 함수.
        /// </summary>
        /// <param name="camera">
        /// The camera.
        /// </param>
        /// <returns>
        /// The create camera control.
        /// </returns>
        private CameraControl CreateCameraControl(Camera camera)
        {
            var cam = new CameraControl
                {
                    ID = camera.Id, 
                    CameraName = camera.Name, 
                    PlayOnControlMode = CommandConfigExecution.GetInstance().Config.Command.StageMode.Stage.PlayOnControlMode.BooleanValue(), 
                    IsInStage = true, 
                    Process = this.MapLayoutControl.Process,
                    PTZEnabled = camera.PTZEnabled ? PTZEnableState.Always : PTZEnableState.None
                };

            return cam;
        }

        /// <summary>
        /// Map Contorl 위치 이동.
        /// </summary>
        /// <param name="lat">
        /// The lat.
        /// </param>
        /// <param name="lng">
        /// The lng.
        /// </param>
        private void GoToMapLocation(double lat, double lng)
        {
            if (this.MapControl != null)
            {
                this.MapControl.GoToPinLocation(lat, lng);
                //this.MapControl.MapLatitude = lat;
                //this.MapControl.MapLongitude = lng;
            }
        }

        /// <summary>
        /// The grid_ mouse left button down.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void GridMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            this.DragMove();
        }

        /// <summary>
        /// The icon check box_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        //private void IconCheckBoxClick(object sender, RoutedEventArgs e)
        //{
        //    this.MapControl.ShowPinObject = !this.MapControl.ShowPinObject;
        //}

        /// <summary>
        /// Map Contirol 초기화.
        /// </summary>
        private void LoadMapControl()
        {
            this.DataContext = this;
            this.MapControl = MapControlRepository.CreateMapControl();
            this.ShowPinObject = true;
            this.MapLayoutControl = new LayoutControl
                {
                    Name = "DynamicMapLayout", 
                    Width = this.xMapCanvas.ActualWidth, 
                    Height = this.xMapCanvas.ActualHeight, 
                    OriginWidth = this.xMapCanvas.ActualWidth, 
                    OriginHeight = this.xMapCanvas.ActualHeight
                };

            this.MapLayoutControl.SetProcess(ProcessMode.Edit);
            this.MapLayoutControl.Process = new EditingProcess(this);
            this.MapLayoutControl.Loaded += this.MapLayoutControlLoaded;

            this.MapControl.Width = this.xMapCanvas.ActualWidth;
            this.MapControl.Height = this.xMapCanvas.ActualHeight;
            this.MapControl.Loaded += this.MapControlLoaded;

            this.MapLayoutControl.AddElement(this.MapControl);

            foreach (var type in this.MapControl.MapTypeList)
            {
                this.xMapProviderComboBox.Items.Add(type);
            }

            this.xMapCanvas.Children.Add(this.MapLayoutControl);

            CameraRepository.LoadCameraGroupInfo(this.cameras, this.xCameraGroupList);
            this.xCameraGroupList.OpenCameras(this.cameras);
            this.xSearch.ItemDoubleClick += this.SearchItemDoubleClick;
            this.xMapProviderComboBox.SelectedItem = this.MapControl.MapType;
            this.xMapProviderComboBox.SelectionChanged += this.XMapProviderComboBoxSelectionChanged;

            var targetDropAdvisor = new MapDropTargetAdvisor();
            targetDropAdvisor.DropCompleted += this.TargetDropAdvisorEDropCompleted;
            DragDropManager.SetDropTargetAdvisor(this.MapLayoutControl, targetDropAdvisor);

            XamlRepository.LoadXamlInfoList(_xamlInfoList, xKmlTreeView);
        }

        /// <summary>
        /// The map control_ loaded.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void MapControlLoaded(object sender, RoutedEventArgs e)
        {
            this.MapControl.ShowControlPanel = false;
            this.MapControl.CameraFullView = false;
            this.MapControl.EnableMouseEvent = true;

            var cameraElements = this.MapControl.GetCameraElements();
            foreach (var cameraElement in cameraElements)
            {
                cameraElement.Process = this.MapLayoutControl.Process;
                this.xCameraGroupList.HighlightItemIdList.Add(cameraElement.ID);
            }
        }

        /// <summary>
        /// The map layout control_ loaded.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void MapLayoutControlLoaded(object sender, RoutedEventArgs e)
        {
            this.MapLayoutControl.Loaded -= this.MapLayoutControlLoaded;
            this.AssignGroundLayoutEventHandler(this.MapLayoutControl);
        }

        /// <summary>
        /// The search location.
        /// </summary>
        private void SearchLocation()
        {
            this.xSearch.Search(this.xTextBox.Text);

            var count = 0;
            this.MapControl.ClearPinObject();
            foreach (var item in this.xSearch.Locations)
            {
                this.MapControl.AddPinObject(count.ToString(), item.Title, item.Lat, item.Lng);
                count++;
            }

            if (this.xSearch.Locations.Count > 0)
            {
                this.MapControl.MapLatitude = this.xSearch.Locations[0].Lat;
                this.MapControl.MapLongitude = this.xSearch.Locations[0].Lng;
            }
        }

        /// <summary>
        /// 컨테이너에 마우스와 키보드 포커스를 제공합니다.
        /// </summary>
        private void SetFocus()
        {
            this.Focusable = true;
            this.IsEnabled = true;

            this.Focus();

            Keyboard.Focus(this);
            FocusManager.SetIsFocusScope(this, true);
        }

        /// <summary>
        /// 수정한 MapControl 을 서버에 전송해서 저장한다.
        /// </summary>
        /// <returns>
        /// 성공 유무.
        /// </returns>
        private bool SetMapAtDB()
        {
            var url = UrlFactory.GetUrlC(
                PlayerCommonsConfig.Instance.DataServiceUrl,
                "SetMapLayout?",
                CommonsConfig.Instance.ProjectID,
                MainProcess.Instance.UserID,
                "command");

            //TODO : Xaml MapControl
            RestService.PostMessage(url, MapControlRepository.ReadXamlString());
            return true;
        }

        /// <summary>
        /// The _last layout control_ layout updated.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void LastLayoutControlLayoutUpdated(object sender, EventArgs e)
        {
            this.TrackerManager.RedrawAsync();
        }

        /// <summary>
        /// The _last layout control_ preview mouse down.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void LastLayoutControlPreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            this.SetFocus();
        }

        /// <summary>
        /// The _last layout control_e changed.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void _lastLayoutControl_eChanged(object sender, RoutedEventArgs e)
        {
            var sourceLayoutControl = e.Source as BaseLayoutControl;

            if (sourceLayoutControl == null)
            {
                return;
            }
        }

        /// <summary>
        /// The _last layout control_e focused layout changed.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void _lastLayoutControl_eFocusedLayoutChanged(object sender, FocusedLayoutChangedRoutedEventArgs e)
        {
            e.Handled = true;
        }

        /// <summary>
        /// The _last layout control_e selected elements changed.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void LastLayoutControlESelectedElementsChanged(
            object sender, SelectedElementsChangedRoutedEventArgs e)
        {
            var layoutControl = e.Source as BaseLayoutControl;
            if (layoutControl == null)
            {
                return;
            }

            this.TrackerManager.Clear();

            e.Handled = true;
        }

        /// <summary>
        /// The _last layout control_e zoom ended.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void _lastLayoutControl_eZoomEnded(object sender, ZoomEndedRoutedEventArgs e)
        {
            return;
        }

        /// <summary>
        /// The target drop advisor_e drop completed.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void TargetDropAdvisorEDropCompleted(object sender, MapDropTargetAdvisorDropCompletedEventArgs e)
        {
            var datas = e.Source as List<Camera>;
            if (datas == null)
            {
                return;
            }

            var elementList = datas.Select(this.CreateCameraControl).ToList();

            var mousePosition = e.TargetDropPoint;

            var leftTopGis = this.MapControl.GetMouseGisPosition(new Point(mousePosition.X - 50, mousePosition.Y - 30));
            var rightBottomGis =
                this.MapControl.GetMouseGisPosition(new Point(mousePosition.X + 50, mousePosition.Y + 30));

            foreach (var cameraControl in elementList)
            {
                cameraControl.TopGis = leftTopGis.Lat;
                cameraControl.LeftGis = leftTopGis.Lng;
                cameraControl.BottomGis = rightBottomGis.Lat;
                cameraControl.RightGis = rightBottomGis.Lng;

                this.MapControl.AddElement(cameraControl);
                this.xCameraGroupList.HighlightItemIdList.Add(cameraControl.ID);
            }
        }

        /// <summary>
        /// The x camera group list user control_ mouse double click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void XCameraGroupListUserControlMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var info = this.xCameraGroupList.xTreeView_CameraList.SelectedItem as TreeListBoxInfo;

            if (info == null)
            {
                return;
            }

            var camera = info.DataItem as Camera;
            if (camera == null)
            {
                return;
            }

            if (camera.IsFolder)
            {
                return;
            }

            this.MapControl.GotoCamera(camera.Id);
        }

        /// <summary>
        /// The x close button_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void XCloseButtonClick(object sender, RoutedEventArgs e)
        {
            //this.DialogResult = false;
            if (this.MapControl != null)
            {
                this.MapControl.Dispose();
            }

            this.Close();
        }

        /// <summary>
        /// The x confirm button_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void XConfirmButtonClick(object sender, RoutedEventArgs e)
        {
            this.MapControl.ClearPinObject();
            this.MapControl.SyncGUID = Guid.NewGuid();

            MapControlRepository.SaveMapControl(this.MapControl);

            if (this.SetMapAtDB() == false)
            {
                return;
            }

            var data = new LocationMapSaveSync
                {
                   LocationMapSyncGuid = this.MapControl.SyncGUID, MapType = this.MapControl.MapType 
                };

            MainProcess.Instance.CommManager.ControlServer.SyncHelper.SendLocationMapSaveSync(data);

            this.DialogResult = true;
            if (this.MapControl != null)
            {
                this.MapControl.Dispose();
            }

            this.Close();
        }

        /// <summary>
        /// The x location search button_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void XLocationSearchButtonClick(object sender, RoutedEventArgs e)
        {
            this.SearchLocation();
        }

        /// <summary>
        /// The x map canvas_ loaded.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void XMapCanvasLoaded(object sender, RoutedEventArgs e)
        {
            this.LoadMapControl();
        }

        /// <summary>
        /// The x map provider combo box_ selection changed.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void XMapProviderComboBoxSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var mapControl = this.MapControl;
            if (mapControl != null)
            {
                mapControl.MapType = e.AddedItems[0].ToString();
            }
        }

        /// <summary>
        /// The x search_ item double click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void SearchItemDoubleClick(object sender, EventArgs e)
        {
            var location = this.xSearch.xLocationList.SelectedItem as GiocodeData;
            if (location == null)
            {
                return;
            }

            this.GoToMapLocation(location.Lat, location.Lng);
        }

        /// <summary>
        /// The x text box_ key down.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void XTextBoxKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                this.SearchLocation();
            }
        }

        /// <summary>
        /// GIS 영역.
        /// </summary>
        private struct GisRect
        {
            /// <summary>
            ///   The bottom.
            /// </summary>
            public double Bottom;

            /// <summary>
            ///   The left.
            /// </summary>
            public double Left;

            /// <summary>
            ///   The right.
            /// </summary>
            public double Right;

            /// <summary>
            ///   The top.
            /// </summary>
            public double Top;
        }

        private void xKeyButton_Click(object sender, RoutedEventArgs e)
        {
            var mapkeys = new MapKeysWindow();
            mapkeys.Owner = this;
            mapkeys.xArcGISUrlText.IsEnabled = 
                this.MapControl != null && 
                String.CompareOrdinal(this.MapControl.MapType, ArcGIS_Userdefined_MapProvider.Instance.Name) == 0;

            if (mapkeys.xArcGISUrlText.IsEnabled)
            {
                mapkeys.xArcGISUrlText.Text = ArcGIS_Userdefined_MapProvider.UrlFormat;
            }

            mapkeys.ShowDialog();
        }

        private void xKmlLoad_Click(object sender, RoutedEventArgs e)
        {
            var dlg = new ImportFrmView();
            dlg.Show();
        }
    }
}