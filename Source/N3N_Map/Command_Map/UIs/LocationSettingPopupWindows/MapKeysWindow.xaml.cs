﻿
namespace Innotive.InnoWatch.Console.UIs.LocationSettingPopupWindows
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Shapes;
    using System.IO;
    using GMap.NET.MapProviders;


    /// <summary>
    /// Interaction logic for MapKeysWindow.xaml
    /// </summary>
    public partial class MapKeysWindow : Window
    {
        public MapKeysWindow()
        {
            InitializeComponent();

            if (File.Exists(@"BingMapProvider.Key"))
            {
                this.xBlingMap.Text = File.ReadAllText(@"BingMapProvider.Key");
            }

            if (File.Exists(@"GoogleMapProvider.Key"))
            {
                this.xGoogleMap.Text = File.ReadAllText(@"GoogleMapProvider.Key");
            }
        }

        private void xConfirmButton_Click(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(this.xBlingMap.Text))
            {
                File.WriteAllText(@"BingMapProvider.Key", this.xBlingMap.Text);
            }
            else
            {
                File.Delete(@"BingMapProvider.Key");
            }

            if (!string.IsNullOrWhiteSpace(this.xGoogleMap.Text))
            {
                File.WriteAllText(@"GoogleMapProvider.Key", this.xGoogleMap.Text);
            }
            else
            {
                File.Delete(@"GoogleMapProvider.Key");
            }

            if (!string.IsNullOrWhiteSpace(this.xArcGISUrlText.Text))
            {
                // Custom Url 을 사용할 경우 {0}{1}{2}(ZoomLevel, PositionX, PositionY) 를 반드시 Url에 포함해야함. 문서화 필요. - by shwlee
                ArcGIS_Userdefined_MapProvider.UrlFormat = this.xArcGISUrlText.Text;
            }
        }

        private void xCancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
            this.Close();
        }

        private void xCloseButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
            this.Close();
        }

        private void Grid_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            this.DragMove();
        }

        private void xDefaultUrlButton_Click(object sender, RoutedEventArgs e)
        {
            if( this.xArcGISUrlText.IsEnabled)
            {
                this.xArcGISUrlText.Text = ArcGIS_Userdefined_MapProvider.DEFAULT_ARCGIS_URL;
            }
        }
    }
}
