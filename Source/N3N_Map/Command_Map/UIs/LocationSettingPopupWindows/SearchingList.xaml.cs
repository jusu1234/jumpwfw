﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SearchingList.xaml.cs" company="Innotive">
//   Innotive Inc. Koreas
// </copyright>
// <summary>
//   Interaction logic for LocationSearchWithBookmarkUserControl.xaml.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.Console.UIs.LocationSettingPopupWindows
{
    using System;
    using System.Collections.ObjectModel;
    using Innotive.InnoWatch.Commons.Map;
    using Innotive.InnoWatch.Console.ViewModels;

    /// <summary>
    /// Interaction logic for LocationSearchWithBookmarkUserControl.xaml.
    /// </summary>
    public partial class SearchingList
    {
        /// <summary>
        /// 뷰 모델.
        /// </summary>
        private readonly LocationSearchViewModel viewModel = new LocationSearchViewModel();

        /// <summary>
        /// Initializes a new instance of the <see cref="SearchingList"/> class. 
        /// </summary>
        public SearchingList()
        {
            InitializeComponent();
            this.DataContext = this.viewModel;
        }

        /// <summary>
        /// 아이템 더블클릭 이벤트.
        /// </summary>
        public event EventHandler ItemDoubleClick;

        /// <summary>
        /// Gets Locations.
        /// </summary>
        public ObservableCollection<GiocodeData> Locations
        {
            get
            {
                return this.viewModel.SearchResultData;
            }
        }

        /// <summary>
        /// 검색 수행.
        /// </summary>
        /// <param name="content">
        /// The content.
        /// </param>
        public void Search(string content)
        {
            this.viewModel.GoogleSearch = content;
        }

        /// <summary>
        /// 마우스 더블 클릭에 대한 이벤트 발생.
        /// </summary>
        private void InvokeItemDoubleClickEventHandler()
        {
            var handler = this.ItemDoubleClick;
            if (handler != null)
            {
                handler(this, new EventArgs());
            }
        }

        /// <summary>
        /// 좌측 마우스 두번 클릭.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void ListItemMouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (e.ClickCount == 2)
            {
                this.InvokeItemDoubleClickEventHandler();
            }
        }
    }
}