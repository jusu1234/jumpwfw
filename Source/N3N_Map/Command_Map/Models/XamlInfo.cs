﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using System.ComponentModel;
using GMap.NET;
using Innotive.InnoWatch.Commons.Models;
using System.Windows.Media;
using System.Xml;

namespace Innotive.InnoWatch.Console.Models
{
    public class XamlInfo : BaseModel
    {
        #region Member Variable

        //private string _name;
        private string _xaml;

        #endregion

        #region Constructors and Destructors

        public XamlInfo() { }

        public XamlInfo(string name, string xaml)
        {
            Name = name;
            Xaml = xaml;
        }

        #endregion
        
        #region Properties

        public string Xaml
        {
            get { return _xaml; }
            set { _xaml = value; }
        }

        #endregion
    }

}
