﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using System.ComponentModel;
using RPN.Common.Core.ViewModel;
using GMap.NET;
using Innotive.InnoWatch.Commons.Models;
using System.Windows.Media;
using System.Xml;

namespace Innotive.InnoWatch.Console.Models
{
    public class PointAndInfo : BaseModel//, IEquatable<PointAndInfo>, IHasDepth
    {
        #region Member Variable

        //private string _name;
        //private PointLatLng _point;
        PointAndInfoList _Items = new PointAndInfoList();
        #endregion

        #region Constructors and Destructors

        public PointAndInfo()
        {
        }

        #endregion

        #region Properties

        //public String Name
        //{
        //    get
        //    {
        //        return this._name;
        //    }

        //    set
        //    {
        //        this._name = value;
        //        this.NotifyPropertyChanged("Name");
        //    }
        //}

        public float Lat { get; set; }

        public float Lon { get; set; }

        public int Alt { get; set; }

        public string Desc { get; set; }

        public string StyleUrl { get; set; }

        public bool IsFolder { get; set; }

        public PointAndInfo Parent { get; set; }

        public PointAndInfoList Items 
        {
            get { return _Items; }
            set {_Items = value;} }
        }

        #endregion
}
