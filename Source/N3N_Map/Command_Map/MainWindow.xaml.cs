﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Innotive.InnoWatch.Console
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            this.xShowMapSettingButton.Click += xShowMapSettingButton_Click;
        }

        private void xShowMapSettingButton_Click(object sender, RoutedEventArgs e)
        {
            var dlg = new UIs.LocationSettingPopupWindows.LocationSettingPopupWindow();
            dlg.Show();
        }
    }
}
