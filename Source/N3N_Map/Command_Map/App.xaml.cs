﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Windows;
using Innotive.InnoWatch.Commons.Configs;
using Innotive.InnoWatch.CommonUtils.Log;
using Innotive.InnoWatch.CommonUtils.Network;
using Innotive.InnoWatch.Console.Configs;
using Innotive.InnoWatch.PlayerCommons.Configs;
using log4net;
using log4net.Config;

namespace Innotive.InnoWatch.Console
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        /// <summary>
        /// 로그포넷을 사용하기 위한 클래스.
        /// </summary>
        public static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// 제품명.
        /// </summary>
        public static string ProductName = "INNOWATCH";

        /// <summary>
        /// Console 시작시 원도우 설정.
        /// Login 성공시 Login 원도우 닫고 메인 원도우 시작.
        /// Login 실패시 Login 원도우 닫고 메인 원도우 시작하지 않음.
        /// </summary>
        /// <param name="e">
        /// The e is StartupEventArgs.
        /// </param>
        protected override void OnStartup(StartupEventArgs e)
        {
            StartProcess();
        }

        /// <summary>
        /// OnStartUp에서 프로세스를 시작한다.
        /// </summary>
        public static void StartProcess()
        {
            // ShoutDown을 호출하기 전까지 프로그램이 종료되지 않도록 속성을 조절한다.
            Current.ShutdownMode = ShutdownMode.OnExplicitShutdown;

            // 1. 속성 정보를 실행한다.
            XmlConfigurator.Configure(new FileInfo(Process.GetCurrentProcess().MainModule.FileName + ".config"));
            if (!ExecucteConfig())
            {   
                Current.Shutdown();
                return;
            }

        }

        /// <summary>
        /// Config에 선언된 속성중 Commons에 있는 녀석들.
        /// </summary>
        /// <returns>
        /// The execucte config.
        /// </returns>
        private static bool ExecucteConfig()
        {
            try
            {
                // 사용자 수정 가능한 Config 파일 읽기
                if (!CommandConfigExecution.GetInstance().Do())
                {
                    InnotiveDebug.Log.Error("Config load failed!");
                    return false;
                }

                var config = CommandConfigExecution.GetInstance().Config;

                // 제품 이름 설정.
                ProductName = config.Command.Resources.ProductName.Value;

                CommonsConfig.Instance.UseWindowMode = config.Command.Control.WindowState.WindowMode.Use.BooleanValue();
                CommonsConfig.Instance.UseMultiMonitorForFullScreen = config.Command.Control.WindowState.MultiMonitorForFullScreen.Use.BooleanValue();


                // Common / Information /  Client 
                CommonsConfig.Instance.ClientID = config.Common.Infomation.Client.Id.Value;

                // Common / Information / Project 
                CommonsConfig.Instance.ProjectID = config.Common.Infomation.Project.Id.Value;

                // Common / Camera / UI
                CommonsConfig.Instance.CameraLabelColor = config.Common.Camera.UI.NameLabelFontColor.Value; // Settings.Default.Camera_Label_Color;
                CommonsConfig.Instance.CameraLabelHeight = config.Common.Camera.UI.NameLabelFontSizeRatio.Value; // Settings.Default.Camera_Label_Height;
                CommonsConfig.Instance.IsShowCameraLabel = config.Common.Camera.UI.NameLabelVisible.BooleanValue();             // Settings.Default.Camera_Label_Format
                CommonsConfig.Instance.CameraLabelFormat = config.Common.Camera.UI.NameLabelFormat.Value;
                CommonsConfig.Instance.CameraLabelFont = config.Common.Camera.UI.NameLabelFontFamily.Value;
                CommonsConfig.Instance.CameraLabelFontStyle = config.Common.Camera.UI.NameLabelFontStyle.Value;
                CommonsConfig.Instance.CameraLabelFontWeight = config.Common.Camera.UI.NameLabelFontWeight.Value;
                CommonsConfig.Instance.CameraLabelUseDropShadowEffect = config.Common.Camera.UI.NameLabelDropShadowEffect.Use;
                CommonsConfig.Instance.CameraLabelDropShadowDepth = config.Common.Camera.UI.NameLabelDropShadowEffect.Depth;
                CommonsConfig.Instance.CameraLabelDropShadowDirection = config.Common.Camera.UI.NameLabelDropShadowEffect.Direction;
                CommonsConfig.Instance.CameraLabelDropShadowColor = config.Common.Camera.UI.NameLabelDropShadowEffect.Color;
                CommonsConfig.Instance.CameraLabelDropShadowOpacity = config.Common.Camera.UI.NameLabelDropShadowEffect.Opacity;
                CommonsConfig.Instance.CameraLabelDropShadowBlurRadius = config.Common.Camera.UI.NameLabelDropShadowEffect.BlurRadius;
                CommonsConfig.Instance.CameraLabelUseFourWayShadow = config.Common.Camera.UI.NameLabelFourWayShadow.Use;
                CommonsConfig.Instance.CameraLabelFourWayShadowDistance = config.Common.Camera.UI.NameLabelFourWayShadow.Distance;
                CommonsConfig.Instance.CameraLabelFourWayShadowColor = config.Common.Camera.UI.NameLabelFourWayShadow.Color;
                CommonsConfig.Instance.CameraLabelFourWayShadowOpacity = config.Common.Camera.UI.NameLabelFourWayShadow.Opacity;
                CommonsConfig.Instance.CameraLabelAlignment = config.Common.Camera.UI.NameLabelAlignment.Value;
                if (string.Compare(config.Common.Camera.UI.ResizeQuality.Value, "Fant", true) == 0)
                {
                    CommonsConfig.Instance.CameraResizeQuality = System.Windows.Media.BitmapScalingMode.Fant;
                }
                else if (string.Compare(config.Common.Camera.UI.ResizeQuality.Value, "NearestNeighbor", true) == 0)
                {
                    CommonsConfig.Instance.CameraResizeQuality = System.Windows.Media.BitmapScalingMode.NearestNeighbor;
                }
                else
                {
                    CommonsConfig.Instance.CameraResizeQuality = System.Windows.Media.BitmapScalingMode.Linear;
                }
                CommonsConfig.Instance.UseOnlyBypass = config.Common.Camera.UI.UseOnlyBypass.BooleanValue();
                CommonsConfig.Instance.UseLocalMD = config.Common.Camera.UI.UseLocalMD.BooleanValue();

                // Common / Map / Center
                CommonsConfig.Instance.MapLatitude = config.Common.Map.Center.LatitudeNode.Value;
                CommonsConfig.Instance.MapLongitude = config.Common.Map.Center.LongitudeNode.Value;

                // Camer.Clipping
                // MergerClippingArea만 사용함. 나머지 삭제대기  - by shwlee.
                //CommonsConfig.Instance.CameraClippingArea = config.DisplayCommon.Camera.Clipping.HighStreamVideo.GetValue(); // Settings.Default.Camera_Clipping_Area;
                CommonsConfig.Instance.MergerClippingArea = config.DisplayCommon.Camera.Clipping.MergerCell.GetValue(); // Settings.Default.Merger_Bottom_Clip;
                //CommonsConfig.Instance.MergerBottomClip = config.DisplayCommon.Camera.Clipping.Merger.BottomToInt(); // Settings.Default.Merger_Clipping_Area;

                CommonsConfig.Instance.UseDeviceInfoMergerClippingArea = config.DisplayCommon.Camera.Clipping.MergerCell.UseCropMergerCellByIndividualSetting;

                // Camera.DispalyRule
                CommonsConfig.Instance.ShowLimitWidth = config.DisplayCommon.Camera.DisplayRule.ShowLimitWidth.Value;
                CommonsConfig.Instance.ShowLimitHeight = config.DisplayCommon.Camera.DisplayRule.ShowLimitHeight.Value;

                // Camera.PTZ
                CommonsConfig.Instance.CameraPtzEnable = config.DisplayCommon.Camera.PTZ.Use.BooleanValue(); // Settings.Default.CameraPtzEnable;

                // Camera.SwitchingRule 
                CommonsConfig.Instance.CameraSourceSwitchingAnimationSpeed = config.DisplayCommon.Camera.SwitchingRule.SwitchingSpeed.Value; // Settings.Default.CameraSource_HighToLow_AnimationSpeed;
                CommonsConfig.Instance.CameraSourceSwitchingUsePause = config.DisplayCommon.Camera.SwitchingRule.SwitchingUsePause.BooleanValue();

                // Camera.UI
                CommonsConfig.Instance.IsShowLowHighBorder = config.DisplayCommon.Camera.UI.StatusLabelVisible.BooleanValue();         // Settings.Default.Show_LowHigh_Borders;

                // ControlManager
                PlayerCommonsConfig.Instance.UseIControlManager = false;
                PlayerCommonsConfig.Instance.IControlManagerPath = config.DisplayCommon.Communication.ControlManager.Path.Value;
                if (File.Exists(config.DisplayCommon.Communication.ControlManager.Path.Value))
                {
                    PlayerCommonsConfig.Instance.UseIControlManager = true;
                }

                CommonsConfig.Instance.PlaybackPath = config.DisplayCommon.Communication.Playback.PlaybackPath.Value;

                // Communication.SyncServer
                PlayerCommonsConfig.Instance.UseAppSyncServer = false;
                PlayerCommonsConfig.Instance.AppSyncServerIp = string.Empty;
                PlayerCommonsConfig.Instance.AppSyncServerPort = 7000;
               
                // Communication.ControlServer
                PlayerCommonsConfig.Instance.DataServiceUrl = string.Empty;
                PlayerCommonsConfig.Instance.EventServiceUrl = string.Empty;
                CommonsConfig.Instance.BindingServiceUrl = string.Empty;
                CommonsConfig.Instance.LogServiceUrl = string.Empty;
                if (config.DisplayCommon.Communication.ControlServer.DataServiceUrl.CanUse())
                {
                    PlayerCommonsConfig.Instance.DataServiceUrl = config.DisplayCommon.Communication.ControlServer.DataServiceUrl.GetUrl();
                }
                if (config.DisplayCommon.Communication.ControlServer.EventServiceUrl.CanUse())
                {
                    PlayerCommonsConfig.Instance.EventServiceUrl = config.DisplayCommon.Communication.ControlServer.EventServiceUrl.GetUrl();
                }
                if (config.DisplayCommon.Communication.ControlServer.BindingServiceUrl.CanUse())
                {
                    CommonsConfig.Instance.BindingServiceUrl = config.DisplayCommon.Communication.ControlServer.GetBindingServiceDefaultUrl();
                }
                if (config.DisplayCommon.Communication.ControlServer.LogServiceUrl.CanUse())
                {
                    CommonsConfig.Instance.LogServiceUrl = config.DisplayCommon.Communication.ControlServer.LogServiceUrl.GetUrl();
                }

                CommonsConfig.Instance.LayoutZoomAnimationSpeed = config.DisplayCommon.Control.Layout.ZoomAnimationSpeed.Value; // Settings.Default.LayoutZoomAnimationSpeed;
                CommonsConfig.Instance.LayoutUseMouseWheel = config.DisplayCommon.Control.Layout.UseMouseWheel.BooleanValue();
                PlayerCommonsConfig.Instance.LayoutUseMouseWheel = config.DisplayCommon.Control.Layout.UseMouseWheel.BooleanValue();

                CommonsConfig.Instance.InitializeMirror = config.DisplayCommon.Control.Layout.InitializeMirror.BooleanValue();
                PlayerCommonsConfig.Instance.InitializeMirror = config.DisplayCommon.Control.Layout.InitializeMirror.BooleanValue();

                // Alarm
                CommonsConfig.Instance.ViewAlarmActivatedCount = config.Command.Alarm.ActivatedList.RowCountNode.Value;
                CommonsConfig.Instance.UseAlarm = config.Command.Alarm.Connection.Use.BooleanValue();
                if (CommonsConfig.Instance.UseAlarm == true)
                {
                    if (string.IsNullOrWhiteSpace(config.Common.Infomation.Client.Id.Value) == true)
                    {
                        Log.Info("Warning!!!!!!!! No Client Id is defined but useing Alarm.");
                        Debug.WriteLine("Warning!!!!!!!! No Client Id is defined but useing Alarm.");
                    }
                }
                CommonsConfig.Instance.UseAlarmPopupWindow = config.Command.Alarm.AlarmPopupWindow.Use.BooleanValue();

                // Control.Rds
                CommonsConfig.Instance.UseRDSOperating = config.Command.Control.RDS.Operating.BooleanValue();
                CommonsConfig.Instance.InputProtocolType = config.Command.Control.RDS.InputProtocolType.Value;
                CommonsConfig.Instance.VncPassword = config.Command.Control.RDS.VncPassword.Value;
                CommonsConfig.Instance.VncPort = config.Command.Control.RDS.VncPort.Value;

                // Control.Xaml
                PlayerCommonsConfig.Instance.ErpDataTimeout = config.DisplayCommon.Control.Xaml.RefreshTimeout.Value; // Settings.Default.ErpDataTimeout;
                CommonsConfig.Instance.MinimumXamlViewerWidthForPolling = config.DisplayCommon.Control.Xaml.PollingLimitMinimumWidth.Value;
                CommonsConfig.Instance.MinimumXamlViewerHeightForPolling = config.DisplayCommon.Control.Xaml.PollingLimitMinimumHeight.Value;


                // Device.Mouse
                CommonsConfig.Instance.IsShowMouseCursor = config.DisplayCommon.Device.Mouse.CursorVisible.BooleanValue();
                PlayerCommonsConfig.Instance.PanningStartPixel = config.DisplayCommon.Control.Layout.PanningStartPixel.Value;

                // Device.Ethernet
                CommonsConfig.Instance.VideoStreamIP = InnoNetwork.FindVideoStreamIP(config.DisplayCommon.Device.Ethernet.VideoIP.Value); // InnoNetwork.FindVideoStreamIP(Settings.Default.VideoStreamIP);
                Log.Info("Using Video String IP: " + CommonsConfig.Instance.VideoStreamIP);
                if (CheckCanUseNetwork.AreUReadyVideoNetwork(CommonsConfig.Instance.VideoStreamIP))
                {
                    Log.Info("Exists Video Stream IP: " + CommonsConfig.Instance.VideoStreamIP);
                    Debug.WriteLine("Exists Video Stream IP: " + CommonsConfig.Instance.VideoStreamIP);
                }
                else
                {
                    Log.Info("Warning!!!!!!!! Not Exists Video Stream IP: " + CommonsConfig.Instance.VideoStreamIP);
                    Debug.WriteLine("Warning!!!!!!!! Not Exists Video Stream IP: " + CommonsConfig.Instance.VideoStreamIP);
                }

                // Device.Graphic
                PlayerCommonsConfig.Instance.UseHardwareAcceleration = config.DisplayCommon.Device.Graphic.UseHardwareAcceleration.BooleanValue(); // Settings.Default.UseSoftwareRender

                // File.Environment
                PlayerCommonsConfig.Instance.EnvPath = config.DisplayCommon.File.Environment.Path.Value; // Settings.Default.EnvPath

                // File.Limit
                CommonsConfig.Instance.MaxHighResSourceCount = config.DisplayCommon.Filter.Limit.MaxHigh.Value; // Settings.Default.Middle_Filter_Limit;
                CommonsConfig.Instance.LowResSourceCount = config.DisplayCommon.Filter.Limit.Low.Value; // Settings.Default.Low_Filter_Limit;

                // GridCell.Line
                CommonsConfig.Instance.UseGridGuidLines = config.DisplayCommon.GridCell.Line.Visible.BooleanValue(); // Settings.Default.UseGridGuidLines;

                // GridCell.Zoom
                CommonsConfig.Instance.CellFullScreenAnimationSpeed = config.DisplayCommon.GridCell.Zoom.AnimationSpeed.Value; // Settings.Default.CellFullScreenAnimationSpeed;
                CommonsConfig.Instance.UseCellAutoFullScreen = !config.DisplayCommon.GridCell.Zoom.UseManualSize.BooleanValue(); // Settings.Default.EnableCellAutoFullScreenRect;
                CommonsConfig.Instance.CellDoubleClickRatio = config.DisplayCommon.GridCell.Zoom.ManualSizePercentage.GetValue(); // Settings.Default.CellManualFullScreenRect;

                // Mode.Stage & Contents
                CommonsConfig.Instance.EnableContentsMode = config.DisplayCommon.Mode.Contents.Use.BooleanValue(); // Settings.Default.EnableContentsMode;
                CommonsConfig.Instance.EnableCameraAssignMode = config.DisplayCommon.Mode.Stage.Use.BooleanValue(); // Settings.Default.EnableCameraAssignMode;

                // DisplayArea Idle Time Action 사용 여부
                PlayerCommonsConfig.Instance.EnableDisplayAreaIdleTimeAction = config.DisplayCommon.DisplayArea.IdleTimeAction.Use.BooleanValue();

                // Command.StageMode.StageCell
                CommonsConfig.Instance.ClearSelectionAfterAction = config.Command.StageMode.StageCell.AutoClearSelection.BooleanValue();
                CommonsConfig.Instance.IsSelectionVisibility = config.Command.StageMode.StageCell.MouseSelectBorderVisible.BooleanValue();
                CommonsConfig.Instance.IsHighlightVisibility = config.Command.StageMode.StageCell.MouseOverBorderVisible.BooleanValue();

                // Command.Stage.PlayouOnControlMode
                CommonsConfig.Instance.PlayOnControlMode = config.Command.StageMode.Stage.PlayOnControlMode.BooleanValue();

                // DisplayCommon.Control.Xaml.UseDynamicLoading
                CommonsConfig.Instance.UseXamlDynamicLoading = config.DisplayCommon.Control.Xaml.UseDynamicLoading.BooleanValue();

                // DisplayCommon.Control.Xaml.UrlPollingInterval
                CommonsConfig.Instance.UrlPollingInterval = config.DisplayCommon.Control.Xaml.UrlPollingInterval.Value;

                // UseSyncToggleButton
                PlayerCommonsConfig.Instance.UseSyncToggleButton = config.Command.StageMode.Stage.UseSyncToggleButton.BooleanValue();

                // PTZControllerServicePort
                PlayerCommonsConfig.Instance.PTZControllerServicePort = config.DisplayCommon.Camera.PTZ.PTZServerPort.Value;
            }
            catch (Exception)
            {
                MessageBox.Show("Config 파일을 읽는 도중 오류가 발생했습니다. Config 파일이 갱신됐는지 확인해 주십시오.");
                return false;
            }

            return true;
        }
    }
}
