﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LocationSearchViewModel.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Defines the LocationSearchViewModel type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.Console.ViewModels
{
    using System;
    using System.Collections.ObjectModel;
    using System.ComponentModel;
    using System.IO;
    using System.Net;
    using System.Windows;
    using System.Xml;
    using System.Xml.Serialization;

    using Innotive.InnoWatch.CommonUtils.Log;
    using Innotive.InnoWatch.Commons.Configs;
    using Innotive.InnoWatch.Commons.Map;
    using Innotive.InnoWatch.Commons.Utils;
    using Innotive.InnoWatch.Commons.Utils.Http;
    using Innotive.InnoWatch.Console.LocationBookmarkXmls;
    using Innotive.InnoWatch.PlayerCommons;
    using Innotive.InnoWatch.PlayerCommons.Configs;

    /// <summary>
    /// LocationSearch ViewModel Class.
    /// </summary>
    public class LocationSearchViewModel : INotifyPropertyChanged
    {
        private readonly ObservableCollection<GiocodeData> searchResultData = new ObservableCollection<GiocodeData>();
        private LocationBookmark locationBookmark;
        private string googleSearch;

        /// <summary>
        /// Property Changed.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Gets or sets GoogleSearch.
        /// </summary>
        public string GoogleSearch
        {
            get
            {
                return this.googleSearch;
            }

            set
            {
                this.googleSearch = value;
                this.DoGoogleSearch(value);
                this.OnPropertyChanged("GoogleSearch");
            }
        }

        /// <summary>
        /// Gets SearchResultData.
        /// </summary>
        public ObservableCollection<GiocodeData> SearchResultData
        {
            get
            {
                return this.searchResultData;
            }
        }

        public LocationSearchViewModel()
        {
            this.RefreshBookmarkData();
            this.SearchResultData.CollectionChanged += this.SearchResultData_CollectionChanged;
        }

        public void RefreshBookmarkData()
        {
            this.locationBookmark = this.ReceiveLocationBookmarkData();
        }

        public void CheckFavoriteButtonState()
        {
            foreach (var item in this.searchResultData)
            {
                if (this.ExistFavorite(item.Lat, item.Lng))
                {
                    item.IsFavorite = true;
                }
            }
        }

        public void DoGoogleSearch(string keyword)
        {
            var searchApi = string.Format("http://maps.google.com/maps/api/geocode/xml?address={0}&language=ko&sensor=false", keyword);

            var request = WebRequest.Create(searchApi);

            var response = request.GetResponse();

            var dataStream = response.GetResponseStream();

            if (dataStream != null)
            {
                var reader = new StreamReader(dataStream);

                string responseFromServer = reader.ReadToEnd();

                reader.Close();
                dataStream.Close();

                var result = new GoogleGiocode();
                result.Parse(responseFromServer);

                this.SearchResultData.Clear();

                foreach (var resultObject in result.Results)
                {
                    this.SearchResultData.Add(resultObject);
                }
            }

            response.Close();

            this.CheckFavoriteButtonState();
        }

        /// <summary>
        /// 지역 북마크를 추가합니다.
        /// </summary>
        /// <param name="title"></param>
        /// <param name="content"></param>
        /// <param name="lat"></param>
        /// <param name="lng"></param>
        public void AddLocationBookmark(string title, string content, double lat, double lng)
        {
            if (this.locationBookmark == null)
            {
                this.locationBookmark = new LocationBookmark();
            }

            if (!this.ExistFavorite(lat, lng))
            {
                var item = new Item { Title = title, Content = content, Lat = lat, Lng = lng, Index = this.locationBookmark.Items.Count };
                this.locationBookmark.Items.Add(item);
            }

            LocationBookmarkXml.Write(this.locationBookmark);

            this.SendLocationBookmarkData();
        }

        /// <summary>
        /// 지역 북마크에서 해당 위치를 가진 아이템을 제거합니다.
        /// </summary>
        /// <param name="lat"></param>
        /// <param name="lng"></param>
        public void RemoveLocationBookmark(double lat, double lng)
        {
            Item removeItem = null;

            foreach (var item in this.locationBookmark.Items)
            {
                if (Convert.ToDouble(item.Lat).Equals(lat) && Convert.ToDouble(item.Lng).Equals(lng))
                {
                    removeItem = item;
                }
            }

            if (removeItem != null)
            {
                this.locationBookmark.Items.Remove(removeItem);
            }

            LocationBookmarkXml.Write(this.locationBookmark);

            this.SendLocationBookmarkData();
        }

        private bool SendLocationBookmarkData()
        {
            var url = UrlFactory.GetUrlC(
                PlayerCommonsConfig.Instance.DataServiceUrl,
                "SetLocationBookmark?",
                CommonsConfig.Instance.ProjectID,
                MainProcess.Instance.UserID,
                "command");
            RestService.PostMessage(url, LocationBookmarkXml.GetString());

            return true;
        }


        /// <summary>
        /// CT 에 접속해서 저장된 Map Layout 을 Download 한다. 
        /// </summary>
        /// <returns>성공 유무</returns>
        private LocationBookmark ReceiveLocationBookmarkData()
        {
            try
            {
                var url = UrlFactory.GetUrlC(
                    PlayerCommonsConfig.Instance.DataServiceUrl,
                    "GetLocationBookmark?",
                    CommonsConfig.Instance.ProjectID,
                    MainProcess.Instance.UserID,
                    "command");

                var data = RestService.GetMessage(url);

                var result = new LocationBookmark();
                if (!string.IsNullOrWhiteSpace(data))
                {
                    var read = new StringReader(data);
                    var serializer = new XmlSerializer(typeof(LocationBookmark));

                    XmlReader reader = new XmlTextReader(read);
                    result = (LocationBookmark) serializer.Deserialize(reader);
                }

                return result;
            }
            catch (Exception ex)
            {
                MainProcess.Log.Error(ex);
                return null;
            }
        }


        private bool ExistFavorite(double lat, double lng)
        {
            if (this.locationBookmark == null)
            {
                return false;
            }

            foreach (var item in this.locationBookmark.Items)
            {
                if (Convert.ToDouble(item.Lat).Equals(lat) && Convert.ToDouble(item.Lng).Equals(lng))
                {
                    return true;
                }
            }

            return false;
        }

        private void SearchResultData_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            var counter = 0;
            foreach (var item in this.SearchResultData)
            {
                item.ItemOrder = counter;
                counter++;
            }
        }

        private void OnPropertyChanged(string name)
        {
            var handler = this.PropertyChanged;
            if (handler != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }
    }

    /// <summary>
    /// GoogleGiocode Class.
    /// </summary>
    public class GoogleGiocode
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GoogleGiocode"/> class.
        /// </summary>
        public GoogleGiocode()
        {
        }

        /// <summary>
        /// Gets or sets a value indicating whether Status.
        /// </summary>
        public bool Status { get; set; }

        /// <summary>
        /// Gets or sets Count.
        /// </summary>
        public int Count { get; set; }

        public ObservableCollection<GiocodeData> Results = new ObservableCollection<GiocodeData>();

        /// <summary>
        /// xml parse.
        /// </summary>
        /// <param name="xmlData">
        /// The xml data.
        /// </param>
        /// <returns>
        /// true or false.
        /// </returns>
        public bool Parse(string xmlData)
        {
            this.Count = 0;

            try
            {
                var xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(xmlData);

                var resultNode = xmlDoc.SelectSingleNode("/GeocodeResponse/status");

                if (resultNode != null && resultNode.InnerText == "OK")
                {
                    this.Status = true;
                }
                else
                {
                    this.Status = false;
                }

                var resultNodeList = xmlDoc.SelectNodes("/GeocodeResponse/result");

                if (resultNodeList != null)
                {
                    this.Count = resultNodeList.Count;
                }

                if (resultNodeList == null)
                {
                    return false;
                }

                foreach (XmlNode result in resultNodeList)
                {
                    var resultObject = new GiocodeData();
                    var formattedAddress = result["formatted_address"];
                    if (formattedAddress != null)
                    {
                        resultObject.Title = formattedAddress.InnerText;
                    }

                    var lat = result["geometry"]["location"]["lat"];
                    if (lat != null)
                    {
                        resultObject.Lat = Convert.ToDouble(lat.InnerText);
                    }

                    var lng = result["geometry"]["location"]["lng"];
                    if (lng != null)
                    {
                        resultObject.Lng = Convert.ToDouble(lng.InnerText);
                    }

                    this.Results.Add(resultObject);
                }
            }
            catch (Exception)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Clear Result.
        /// </summary>
        public void Clear()
        {
            this.Status = false;
            this.Count = 0;
            this.Results.Clear();
        }
    }
}
