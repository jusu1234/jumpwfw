﻿// -----------------------------------------------------------------------
// <copyright file="LocationBookmarkUserControlViewModel.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace Innotive.InnoWatch.Console.Controls.Locations
{
    using System;
    using System.Collections.ObjectModel;
    using System.ComponentModel;
    using System.IO;
    using System.Xml;
    using System.Xml.Serialization;

    using Innotive.InnoWatch.Commons.Configs;
    using Innotive.InnoWatch.Commons.Utils;
    using Innotive.InnoWatch.Commons.Utils.Http;
    using Innotive.InnoWatch.Console.LocationBookmarkXmls;
    using Innotive.InnoWatch.PlayerCommons;
    using Innotive.InnoWatch.PlayerCommons.Configs;

    public class LocationBookmarkUserControlViewModel : INotifyPropertyChanged
    {
        private ObservableCollection<Item> items = new ObservableCollection<Item>();

        public ObservableCollection<Item> Items
        {
            get
            {
                return this.items;
            }

            set
            {
                this.items = value;
                this.OnPropertyChanged("Items");
            }
        }

        public LocationBookmarkUserControlViewModel()
        {
            var bookmark = this.ReceiveLocationBookmarkData();

            this.Items = null;
            if (bookmark != null)
            {
                this.Items = bookmark.Items;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void ReloadBookmark()
        {
            var bookmark = LocationBookmarkXml.Read();

            if (bookmark != null)
            {
                this.Items = bookmark.Items;
            }
        }

        public void OnPropertyChanged(string name)
        {
            if (this.PropertyChanged != null) this.PropertyChanged(this, new PropertyChangedEventArgs(name));
        }

        public void DeleteItem(Item item)
        {
            this.Items.Remove(item);
            var bookmark = new LocationBookmark { Items = this.Items };
            LocationBookmarkXml.Write(bookmark);

            this.SendLocationBookmarkData();
        }

        private void SendLocationBookmarkData()
        {
            var url = UrlFactory.GetUrlC(
                PlayerCommonsConfig.Instance.DataServiceUrl,
                "SetLocationBookmark?",
                CommonsConfig.Instance.ProjectID,
                MainProcess.Instance.UserID,
                "command");
            RestService.PostMessage(url, LocationBookmarkXml.GetString());
        }


        /// <summary>
        /// CT 에 접속해서 저장된 Map Layout 을 Download 한다. 
        /// </summary>
        /// <returns>성공 유무</returns>
        private LocationBookmark ReceiveLocationBookmarkData()
        {
            try
            {
                var url = UrlFactory.GetUrlC(
                    PlayerCommonsConfig.Instance.DataServiceUrl,
                    "GetLocationBookmark?", 
                    CommonsConfig.Instance.ProjectID,
                    MainProcess.Instance.UserID,
                    "command");
                var data = RestService.GetMessage(url);

                var read = new StringReader(data);
                var serializer = new XmlSerializer(typeof(LocationBookmark));

                XmlReader reader = new XmlTextReader(read);
                return (LocationBookmark)serializer.Deserialize(reader);
            }
            catch (Exception ex)
            {
                MainProcess.Log.Error(ex);
                return null;
            }
        }
    }
}
