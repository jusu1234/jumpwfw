﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LocationListEventArgs.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   The image list mode.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.Console.Controls.Locations
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// LocationSearchedEvent Args.
    /// </summary>
    public class LocationListEventArgs : EventArgs
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LocationListEventArgs"/> class.
        /// </summary>
        public LocationListEventArgs(bool showPinObject)
        {
            this.LocationInfoList = new List<SearchedLocationInfo>();
            this.ShowPinObject = showPinObject;
        }

        /// <summary>
        /// Gets or sets LocationInfoList.
        /// </summary>
        public List<SearchedLocationInfo> LocationInfoList { get; set; }

        public bool ShowPinObject { get; set; }

        /// <summary>
        /// Searched Location Info.
        /// </summary>
        public struct SearchedLocationInfo
        {
            public int Index;
            public string Content;
            public double Latitude;
            public double Longitude;
        }
    }

    /// <summary>
    /// LocationSearchItemEvent Args.
    /// </summary>
    public class ShowPinObjectEventArgs : EventArgs
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LocationEventArgs"/> class. 
        /// </summary>
        /// <param name="showPinObject"> </param>
        public ShowPinObjectEventArgs(bool showPinObject)
        {
            this.ShowPinObject = showPinObject;
        }
        public bool ShowPinObject { get; set; }
    }
    /// <summary>
    /// LocationSearchItemEvent Args.
    /// </summary>
    public class LocationEventArgs : EventArgs
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LocationEventArgs"/> class. 
        /// </summary>
        /// <param name="lat">
        /// The lat.
        /// </param>
        /// <param name="lng">
        /// The lng.
        /// </param>
        public LocationEventArgs(double lat, double lng)
        {
            this.Latitude = lat;
            this.Longitude = lng;
        }

        /// <summary>
        /// Gets or sets Latitude.
        /// </summary>
        public double Latitude { get; protected set; }

        /// <summary>
        /// Gets or sets Longitude.
        /// </summary>
        public double Longitude { get; protected set; }
    }

    /// <summary>
    /// LocationSearchItemEvent Args.
    /// </summary>
    public class LocationCameraEventArgs : EventArgs
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LocationCameraEventArgs"/> class. 
        /// </summary>
        /// <param name="cameraId">
        /// The camera Id.
        /// </param>
        public LocationCameraEventArgs(string cameraId)
        {
            this.CameraId = cameraId;
        }

             /// <summary>
        /// Gets or sets Camera.
        /// </summary>
        public string CameraId { get; protected set; }
    }
}
