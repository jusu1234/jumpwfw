﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LocationSearchUserControl.xaml.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Interaction logic for LocationSearchWithBookmarkUserControl.xaml
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.Console.Controls.Locations
{
    using System;
    using System.Collections.ObjectModel;
    using System.Windows.Controls.Primitives;

    using Innotive.InnoWatch.Commons.Map;
    using Innotive.InnoWatch.Console.Controls.DragDrop;
    using Innotive.InnoWatch.Console.ViewModels;

    /// <summary>
    /// Interaction logic for LocationSearchWithBookmarkUserControl.xaml.
    /// </summary>
    public partial class LocationSearchUserControl
    {
        /// <summary>
        /// View Model.
        /// </summary>
        private readonly LocationSearchViewModel viewModel = new LocationSearchViewModel();

        /// <summary>
        /// Initializes a new instance of the <see cref="LocationSearchUserControl"/> class.
        /// </summary>
        public LocationSearchUserControl()
        {
            InitializeComponent();
            this.DataContext = this.viewModel;

            //Commons.Utils.DragDropManager.SetDragSourceAdvisor(this.xLocationList, new LocationSearchDragSourceAdvisor());
        }

        /// <summary>
        /// 아이템 더블클릭 이벤트.
        /// </summary>
        public event EventHandler<LocationEventArgs> LocationItemDoubleClick;

        /// <summary>
        /// Gets Locations.
        /// </summary>
        public ObservableCollection<GiocodeData> Locations
        {
            get
            {
                return this.viewModel.SearchResultData;
            }
        }

        /// <summary>
        /// 검색 수행.
        /// </summary>
        /// <param name="content">
        /// The content.
        /// </param>
        public void Search(string content)
        {
            this.viewModel.GoogleSearch = content;
        }

        /// <summary>
        /// Refresh List.
        /// </summary>
        public void RefreshLocationBookmark()
        {
            this.viewModel.RefreshBookmarkData();
        }

        /// <summary>
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void InvokeLocationItemDoubleClick(object sender, LocationEventArgs e)
        {
            var handler = this.LocationItemDoubleClick;
            if (handler != null)
            {
                handler(sender ?? this, e);
            }
        }

        private void xFavoriteToggleButton_Unchecked(object sender, System.Windows.RoutedEventArgs e)
        {
            var toggleButton = sender as ToggleButton;
            if (toggleButton != null)
            {
                var item = toggleButton.DataContext as GiocodeData;
                if (item != null)
                {
                    this.viewModel.RemoveLocationBookmark(item.Lat, item.Lng);
                }
            }
        }

        private void xFavoriteToggleButton_Checked(object sender, System.Windows.RoutedEventArgs e)
        {
            var toggleButton = sender as ToggleButton;
            if (toggleButton != null)
            {
                var item = toggleButton.DataContext as GiocodeData;
                if (item != null)
                {
                    this.viewModel.AddLocationBookmark(item.Title, item.Content, item.Lat, item.Lng);
                }
            }
        }

        private void xLocationList_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            var location = this.xLocationList.SelectedItem as GiocodeData;
            if (location == null)
            {
                return;
            }

            this.InvokeLocationItemDoubleClick(sender, new LocationEventArgs(location.Lat, location.Lng));
        }
    }
}
