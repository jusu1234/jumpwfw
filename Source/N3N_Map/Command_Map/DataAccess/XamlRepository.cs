﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CameraRepository.cs" company="Innotive Inc. Korea">
//   Copyright ⓒ Innotive inc. Korea 2011
// </copyright>
// <summary>
//   카메라 정보
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.Console.DataAccess
{
    using System;
    using System.IO;
    using System.Linq;
    using System.Threading;
    using System.Windows;
    using System.Xml;
    using Innotive.InnoWatch.FileImport.Model;
    //using Innotive.InnoWatch.Commons.CameraManagers;
    using Innotive.InnoWatch.Commons.Configs;
    //using Innotive.InnoWatch.Commons.Utils;
    using Innotive.InnoWatch.Commons.Utils.Http;
    //using Innotive.InnoWatch.CommonUtils.Log;
    //using Innotive.InnoWatch.Console.Controls;
    //using Innotive.InnoWatch.Console.Models;
    using Innotive.InnoWatch.PlayerCommons;
    using Innotive.InnoWatch.PlayerCommons.Configs;

    internal class LoadThreadXamlWorkerParam
    {
        public XamlInfoList Items { get; set; }
    }

    /// <summary>
    /// 카메라 정보.
    /// </summary>
    public class XamlRepository
    {
        private XamlInfoList _xamlList;
        /// <summary>
        /// </summary>
        /// <param name="cameras">
        /// The cameras.
        /// </param>
        /// <param name="element">
        /// The element.
        /// </param>
        /// <param name="aType"> </param>
        public static void LoadXamlInfoList(XamlInfoList list , FrameworkElement element)//, CameraGroupTypes aType = CameraGroupTypes.Custom)
        {

            list.Clear();

            var param = new LoadThreadXamlWorkerParam { Items = list };

            // 데이터 로드
            if (element == null)
            {
                var worker = new Thread(LoadThreadWorker);
                worker.Start(param);
            }
            else
            {
                element.Dispatcher.BeginInvoke(new Action(() => LoadThreadWorker(param)));
            }
        }

        private static void LoadThreadWorker(object param)
        {
            var url = UrlFactory.GetUrlC(
                                       PlayerCommonsConfig.Instance.DataServiceUrl,
                                       "GetMapXaml?",
                                       CommonsConfig.Instance.ProjectID,
                                       MainProcess.Instance.UserID,
                                       "command");

            //var response = RestService.GetStream(url);

            InnowatchService.DataService svc = new InnowatchService.DataService();

            Stream response = svc.GetMapXaml(string.Empty, string.Empty, string.Empty);

            // Data Parsing.
            var xmlDoc = new XmlDocument();
            xmlDoc.Load(response);

            XmlNodeList list = xmlDoc.SelectNodes("Document/Data");
            if (list == null)
            {
                return;
            }

            var xamlInfoList = param as XamlInfoList;

            foreach (XmlNode item in list)
            { 
                xamlInfoList.Add
                (
                    new XamlInfo() { Name = item["name"].InnerText, Xaml = item["XamalData"].InnerText}
                );
            }
        }


    }
}
