﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CameraRepository.cs" company="Innotive Inc. Korea">
//   Copyright ⓒ Innotive inc. Korea 2011
// </copyright>
// <summary>
//   카메라 정보
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.Console.DataAccess
{
    using System;
    using System.IO;
    using System.Linq;
    using System.Threading;
    using System.Windows;
    using System.Xml;

    using Innotive.InnoWatch.Commons.CameraManagers;
    using Innotive.InnoWatch.Commons.Configs;
    using Innotive.InnoWatch.Commons.Utils;
    using Innotive.InnoWatch.Commons.Utils.Http;
    using Innotive.InnoWatch.CommonUtils.Log;
    using Innotive.InnoWatch.Console.Controls;
    using Innotive.InnoWatch.Console.Models;
    using Innotive.InnoWatch.PlayerCommons;
    using Innotive.InnoWatch.PlayerCommons.Configs;

    internal class LoadThreadWorkerParam
    {
        public CameraRepository.CameraGroupTypes CameraGroupType { get; set; }

        public Cameras Cameras { get; set; }
    }

    /// <summary>
    /// 카메라 정보.
    /// </summary>
    public class CameraRepository
    {
        public enum CameraGroupTypes
        {
            Custom = 0,
            MediaServer = 1,
            Recorder = 2
        }

        /// <summary>
        /// Root Folder ID.
        /// </summary>
        private const string CameraGroupRootFolderId = "root";
        
        /// <summary>
        /// VideoInformation.test File Path.
        /// </summary>
        private static readonly string VIDEOINFOTEST = AppDomain.CurrentDomain.BaseDirectory + "VideoInformation.test";

        #region 데이터 읽기

        private static void AddCameraGroupFromVideoInfoTestFile(Cameras cameras, Camera root)
        {
            SetCameraGroupByVideoInfo(cameras, root);
        }

        private static void SetCameraGroupByVideoInfo(Cameras cameras, Camera root)
        {
            var device = CameraManager.Instance.VideoInfo;
            if (device == null)
            {
                return;
            }

            foreach (var info in device.CameraInfo)
            {
                var camera = new Camera
                                 {
                                     Id = info.CameraId,
                                     Name = info.CameraName,
                                     ControlType = info.ControlType,
                                     RdsHost = info.RdsHost,
                                     RdsPort = info.RdsPort,
                                     IsFolder = false
                                 };

                // RDS일 경우 제어 화면 크기에 사용할 CameraWidth & CameraHeight를 설정해야 한다. 
                // 만약 여러 해상도를 가질 경우 제일 큰 해상도를 사용해야 한다.
                if (string.Compare(camera.ControlType, "RDS", true) == 0)
                {
                    var bigWidth = 0;
                    var bigHeight = 0;
                    foreach (var item in info.ResolutionInfos)
                    {
                        var resWidth = 0;
                        var resHeight = 0;

                        if (Int32.TryParse(item.Width, out resWidth) == true &&
                            Int32.TryParse(item.Height, out resHeight) == true)
                        {
                            if (bigWidth * bigHeight < resWidth * resHeight)
                            {
                                bigWidth = resWidth;
                                bigHeight = resHeight;
                            }
                        }
                    }

                    camera.CameraWidth = bigWidth;
                    camera.CameraHeight = bigHeight;
                }
                
                root.Items.Add(camera);
            }

            // default folder create
            cameras.Add(root);
        }

        /// <summary>
        /// 카메라 폴더 리스트에서 ID로 해당 폴더를 찾는다.
        /// </summary>
        /// <param name="cameraId">
        /// 카메라 폴더 ID.
        /// </param>
        /// <param name="cameras">
        /// 카메라 컬렉션.
        /// </param>
        /// <returns>
        /// The find camera folder by id.
        /// </returns>
        private static Camera FindCameraFolderById(string cameraId, Cameras cameras)
        {
            foreach (var camera in cameras)
            {
                if (camera.IsFolder)
                {
                    if (string.Compare(camera.Id, cameraId) == 0)
                    {
                        return camera;
                    }

                    var child_camera = FindCameraFolderById(cameraId, camera.Items);

                    if (child_camera != null)
                    {
                        return child_camera;
                    }
                }
            }

            return null;
        }

        #endregion // 데이터 읽기

        #region Ct100

        /// <summary>
        /// </summary>
        /// <param name="cameras">
        /// The cameras.
        /// </param>
        /// <param name="element">
        /// The element.
        /// </param>
        /// <param name="aType"> </param>
        public static void LoadCameraGroupInfo(Cameras cameras, FrameworkElement element, CameraGroupTypes aType = CameraGroupTypes.Custom)
        {
            cameras.Clear();
            cameras.DataLoadComplete = false;

            var param = new LoadThreadWorkerParam { CameraGroupType = aType, Cameras = cameras };

            // 카메라 데이터 로드
            if (element == null)
            {
                var cameraListLoadThread = new Thread(LoadThreadWorker);
                cameraListLoadThread.Start(param);
            }
            else
            {
                element.Dispatcher.BeginInvoke(new Action(() => LoadThreadWorker(param)));
            }
        }

        /// <summary>
        /// </summary>
        /// <param name="cameras">
        /// The cameras.
        /// </param>
        /// <param name="element">
        /// The element.
        /// </param>
        public static void SaveCameraGroupInfo(Cameras cameras, FrameworkElement element)
        {
            // 카메라 데이터 저장
            if (element == null)
            {
                var cameraListSaveThread = new Thread(SaveThreadWorker);
                cameraListSaveThread.Start(cameras);
            }
            else
            {
                element.Dispatcher.BeginInvoke(new Action(() => SaveThreadWorker(cameras)));
            }
        }

        /// <summary>
        /// 카메라 아이템 목록 읽기.
        /// </summary>
        /// <param name="cameras">
        /// The cameras.
        /// </param>
        /// <param name="element">
        /// The element.
        /// </param>
        public static void LoadItemCameraInfo(Cameras cameras, FrameworkElement element)
        {
            if (cameras == null)
            {
                return;
            }

            cameras.Clear();

            if (CameraManager.Instance.VideoInfo == null)
            {
                return;
            }

            foreach (var item in CameraManager.Instance.VideoInfo.CameraInfo)
            {
                cameras.Add(new Camera { Id = item.CameraId, Name = item.CameraName });
            }
        }

        /// <summary>
        /// 데이터 읽기 쓰레드 작업자.
        /// </summary>
        /// <param name="param">
        /// The param.
        /// </param>
        private static void LoadThreadWorker(object param)
        {
            var loadThreadWorkerParam = param as LoadThreadWorkerParam;

            if (loadThreadWorkerParam == null)
            {
                return;
            }

            if (loadThreadWorkerParam.Cameras == null)
            {
                return;
            }

            loadThreadWorkerParam.Cameras.Clear();

            // Root 생성.
            var root = new Camera
            {
                Id = CameraGroupRootFolderId,
                Name = CommonsConfig.Instance.ProjectID,
                IsFolder = true,
                IsCheckingEnabled = true
            };

            // VideoInformation Loading
            if (File.Exists(VIDEOINFOTEST))
            {
                AddCameraGroupFromVideoInfoTestFile(loadThreadWorkerParam.Cameras, root);
            }
            else if (!string.IsNullOrWhiteSpace(PlayerCommonsConfig.Instance.DataServiceUrl))
            {
                LoadCameraGroupFromCt100(loadThreadWorkerParam.Cameras, loadThreadWorkerParam.CameraGroupType);
            }
            else
            {
                // VideoDivices.xml같은 수동 설정 파일 있을 경우.
            }

            // 읽기 완료 이벤트 호출
            loadThreadWorkerParam.Cameras.DataLoadComplete = true;
            loadThreadWorkerParam.Cameras.RaiseLoadCompletedEvent();
            loadThreadWorkerParam.Cameras = null;
        }

        private static void LoadCameraGroupFromCt100(Cameras cameras, CameraGroupTypes aType)
        {
            try
            {
                string url = string.Empty;

                switch (aType)
                {
                    case CameraGroupTypes.Custom:
                        url = UrlFactory.GetUrlC(
                            PlayerCommonsConfig.Instance.DataServiceUrl,
                            "GetCameraGroupInfo?",
                            CommonsConfig.Instance.ProjectID,
                            MainProcess.Instance.UserID,
                            "command");
                        break;

                    case CameraGroupTypes.MediaServer:
                        url = UrlFactory.GetUrlD(
                            PlayerCommonsConfig.Instance.DataServiceUrl,
                            "GetCameraGroupInfoByGrouping?",
                            CommonsConfig.Instance.ProjectID,
                            "MD");
                        break;

                    case CameraGroupTypes.Recorder:
                        url = UrlFactory.GetUrlD(
                            PlayerCommonsConfig.Instance.DataServiceUrl,
                            "GetCameraGroupInfoByGrouping?",
                            CommonsConfig.Instance.ProjectID,
                            "RC");
                        break;
                }

                var response = RestService.GetStream(url);
                
                // Data Parsing.
                var xmlDoc = new XmlDocument();
                xmlDoc.Load(response);
                var cameraNodeList = xmlDoc.SelectNodes("CameraGroupInfoList/CameraGroupInfo");
                if (cameraNodeList == null)
                {
                    return;
                }

                foreach (XmlNode cameraInfo in cameraNodeList)
                {
                    if (cameraInfo.Attributes == null)
                    {
                        continue;
                    }

                    var isFolder = cameraInfo.Attributes["IsFolder"].Value;
                    var parentId = cameraInfo.Attributes["ParentID"].Value;

                    Camera camera;

                    if (string.Compare(isFolder, "true") == 0)
                    {
                        var id = cameraInfo.Attributes["ID"].Value;
                        var groupName = cameraInfo.Attributes["CameraGroupName"].Value;
                        camera = CreateCameraFromVideoInfo(true, id, groupName);
                    }
                    else
                    {
                        var id = cameraInfo.Attributes["CameraID"].Value;
                        var name = cameraInfo.Attributes["CameraName"].Value;
                        camera = CreateCameraFromVideoInfo(false, id, name);
                    }

                    if (string.Compare(parentId, "-1") == 0)
                    {
                        cameras.Add(camera);
                    }
                    else
                    {
                        var parent = FindCameraFolderById(parentId, cameras);
                        if (parent != null)
                        {
                            parent.Items.Add(camera);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                App.Log.Error("Camera Load Failed!", ex);
                InnotiveDebug.Trace(4, "Console.Camera.LoadThreadWorker() Camera Load Exception ==> {0}", ex.Message);
            }
        }

        private static Camera CreateCameraFromVideoInfo(bool isFolder, string id, string name)
        {
            Camera camera;
            if (isFolder)
            {
                camera = new Camera { IsFolder = true, Id = id, Name = name };
            }
            else
            {
                camera = new Camera { IsFolder = false, Id = id, Name = name };

                var device = CameraManager.Instance.VideoInfo;
                foreach (var info in device.CameraInfo.Where(info => info.CameraId == camera.Id))
                {
                    camera.ControlType = info.ControlType;

                    // RDS일 경우 제어 화면 크기에 사용할 CameraWidth & CameraHeight를 설정해야 한다. 
                    // 만약 여러 해상도를 가질 경우 제일 큰 해상도를 사용해야 한다.
                    if (string.Compare(camera.ControlType, "RDS", true) == 0)
                    {
                        var bigWidth = 0;
                        var bigHeight = 0;
                        foreach (var item in info.ResolutionInfos)
                        {
                            var resWidth = 0;
                            var resHeight = 0;

                            if (Int32.TryParse(item.Width, out resWidth) == true
                                && Int32.TryParse(item.Height, out resHeight) == true)
                            {
                                if (bigWidth * bigHeight < resWidth * resHeight)
                                {
                                    bigWidth = resWidth;
                                    bigHeight = resHeight;
                                }
                            }
                        }

                        camera.CameraWidth = bigWidth;
                        camera.CameraHeight = bigHeight;
                    }
                }
            }
            return camera;
        }

        public static void SetCameraFromVideoInfo(Camera camera)
        {
            if (camera.IsFolder)
            {
                return;
            }

            var device = CameraManager.Instance.VideoInfo;
            foreach (var info in device.CameraInfo.Where(info => info.CameraId == camera.Id))
            {
                camera.ControlType = info.ControlType;
                camera.RdsHost = info.RdsHost;
                camera.RdsPort = info.RdsPort;

                // RDS일 경우 제어 화면 크기에 사용할 CameraWidth & CameraHeight를 설정해야 한다. 
                // 만약 여러 해상도를 가질 경우 제일 큰 해상도를 사용해야 한다.
                if (string.Compare(camera.ControlType, "RDS", true) == 0)
                {
                    var bigWidth = 0;
                    var bigHeight = 0;
                    foreach (var item in info.ResolutionInfos)
                    {
                        var resWidth = 0;
                        var resHeight = 0;

                        if (Int32.TryParse(item.Width, out resWidth) == true
                            && Int32.TryParse(item.Height, out resHeight) == true)
                        {
                            if (bigWidth * bigHeight < resWidth * resHeight)
                            {
                                bigWidth = resWidth;
                                bigHeight = resHeight;
                            }
                        }
                    }

                    camera.CameraWidth = bigWidth;
                    camera.CameraHeight = bigHeight;
                }
            }
        }

        /// <summary>
        /// 데이터 저장 쓰레드 작업자.
        /// </summary>
        /// <param name="param">
        /// The param.
        /// </param>
        private static void SaveThreadWorker(object param)
        {
            // TODO : 데이터 저장 코드 추가
            var cameras = param as Cameras;

            if (cameras == null)
            {
                return;
            }

            try
            {
                // VideoInformation Loading
                if (File.Exists(VIDEOINFOTEST))
                {
                    MessageBox.Show(VIDEOINFOTEST + " 파일이 존재하면 해당 기능이 동작하지 않습니다.");
                }
                else if (!string.IsNullOrWhiteSpace(PlayerCommonsConfig.Instance.DataServiceUrl))
                {
                    SaveCameraGroupToCt100(cameras);
                }
                else
                {
                    // VideoDivices.xml같은 수동 설정 파일 있을 경우.
                }
            }
            catch (Exception ex)
            {
                App.Log.Error("StageGrid GetXamlString Failed!", ex);
                InnotiveDebug.Trace(4, "Console => StageGrid Save Exception ==> {0}", ex.Message);
            }

            // 저장 완료 이벤트 호출   
            cameras.RaiseSaveCompletedEvent();
        }

        private static void SaveCameraGroupToCt100(Cameras cameras)
        {
            var xmlDocument = SetXmlToCameraGroup(cameras);

            var url = UrlFactory.GetUrlC(
                PlayerCommonsConfig.Instance.DataServiceUrl,
                "AddCameraGroupInfo?",
                CommonsConfig.Instance.ProjectID,
                MainProcess.Instance.UserID,
                "command");
            RestService.PostMessage(url, xmlDocument.InnerXml);
        }

        private static XmlDocument SetXmlToCameraGroup(Cameras cameras)
        {
            var xmlDocument = new XmlDocument();

            var xmlDeclaration = xmlDocument.CreateXmlDeclaration("1.0", "utf-8", null);
            xmlDocument.AppendChild(xmlDeclaration);

            var root = xmlDocument.CreateElement("CameraGroupInfoList");
            xmlDocument.AppendChild(root);

            var cameraList = cameras.Traverse(c => c.Items);
            var idCounter = 0;
            foreach (var camera in cameraList)
            {
                var cameraGroupInfo = xmlDocument.CreateElement("CameraGroupInfo");
                root.AppendChild(cameraGroupInfo);

                var attributeIsFolder = xmlDocument.CreateAttribute("IsFolder");
                var attributeId = xmlDocument.CreateAttribute("ID");
                var attributeCameraGroupName = xmlDocument.CreateAttribute("CameraGroupName");
                var attributeParentId = xmlDocument.CreateAttribute("ParentID");
                var attributeItemOrder = xmlDocument.CreateAttribute("ItemOrder");
                var attributeCameraId = xmlDocument.CreateAttribute("CameraID");
                var attributeCameraName = xmlDocument.CreateAttribute("CameraName");

                // Folder 정보
                if (camera.IsFolder)
                {
                    camera.Id = idCounter.ToString();

                    attributeIsFolder.Value = "true";
                    attributeId.Value = camera.Id;
                    attributeCameraGroupName.Value = camera.Name;
                    attributeParentId.Value = camera.Parent.Parent == null ? "-1" : camera.Parent.Parent.Id;
                    attributeItemOrder.Value = camera.ItemOrder.ToString();
                    attributeCameraId.Value = string.Empty;
                    attributeCameraName.Value = string.Empty;

                    idCounter++;
                }
                else
                {
                    attributeIsFolder.Value = "false";
                    attributeId.Value = string.Empty;
                    attributeCameraGroupName.Value = string.Empty;
                    attributeParentId.Value = camera.Parent.Parent == null ? "-1" : camera.Parent.Parent.Id;
                    attributeItemOrder.Value = camera.ItemOrder.ToString();
                    attributeCameraId.Value = camera.Id;
                    attributeCameraName.Value = camera.Name;
                }

                cameraGroupInfo.Attributes.Append(attributeIsFolder);
                cameraGroupInfo.Attributes.Append(attributeId);
                cameraGroupInfo.Attributes.Append(attributeCameraGroupName);
                cameraGroupInfo.Attributes.Append(attributeParentId);
                cameraGroupInfo.Attributes.Append(attributeItemOrder);
                cameraGroupInfo.Attributes.Append(attributeCameraId);
                cameraGroupInfo.Attributes.Append(attributeCameraName);
            }

            return xmlDocument;
        }

        #endregion // Ct100
    }
}
