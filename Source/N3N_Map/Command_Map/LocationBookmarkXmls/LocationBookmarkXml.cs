﻿// -----------------------------------------------------------------------
// <copyright file="LocationBookmarkXml.cs" company="">
// Innotive Inc. Korea
// </copyright>
// -----------------------------------------------------------------------

namespace Innotive.InnoWatch.Console.LocationBookmarkXmls
{
    using System;
    using System.Diagnostics;
    using System.IO;
    using System.Text;
    using System.Windows;
    using System.Xml;
    using System.Xml.Serialization;

    // 1. 파일을 읽어들인다.

    /// <summary>
    /// LacationBookmark.xml을 관리하는 클래스.
    /// </summary>
    public class LocationBookmarkXml
    {
        // 실행 파일 경로.
        private static readonly string executeFilePath = Process.GetCurrentProcess().MainModule.FileName;

        /// <summary>.
        /// 파일 전체 경로.
        /// 실행파일 이름이 "C:\ProgramFiles\INNOWATCH\Editor.exe" 라면 fileName = "C:\ProgramFiles\LocationBookmark.xml".
        /// </summary>
        private static readonly string xmlFileName = GetFileName(executeFilePath);

        public static LocationBookmark Read()
        {
            return Read(xmlFileName);
        }


        /// <summary>
        /// 저장된 LocationBookmark.xml 을 문자열로 반환한다.
        /// </summary>
        /// <returns>
        /// string 형태의 파일 DATA.
        /// </returns>
        public static string GetString()
        {
            if (!File.Exists(xmlFileName))
            {
                return string.Empty;
            }

            using (var stream = new StreamReader(xmlFileName))
            {
                return stream.ReadToEnd();
            }
        }

        public static string Write(LocationBookmark locationBookmark)
        {
            return Write(locationBookmark, xmlFileName);
        }

        private static LocationBookmark Read(string fileName)
        {
            if (!File.Exists(fileName))
            {
                return null;
            }

            using (var streamReader = new StreamReader(fileName))
            {
                return ReadXml(streamReader.ReadToEnd());
            }
        }


        /// <summary>
        /// 문자열을 LocationBookmark 객체로 변환한다. 
        /// </summary>
        /// <param name="xmlData">
        /// CameraResolutionInfo 문자열 데이터.
        /// </param>
        /// <returns>
        /// CameraResolutionInfo 객체.
        /// </returns>
        private static LocationBookmark ReadXml(string xmlData)
        {
            var serializer = new XmlSerializer(typeof(LocationBookmark));

            LocationBookmark result;

            using (var stringReader = new StringReader(xmlData))
            {
                using (var xmlReader = new XmlTextReader(stringReader))
                {
                    result = serializer.Deserialize(xmlReader) as LocationBookmark;
                }
            }

            return result;
        }

        private static string DataToXml(LocationBookmark lacationBookmark)
        {
            var serializer = new XmlSerializer(typeof(LocationBookmark));
            using (var memStream = new MemoryStream())
            {
                var settings = new XmlWriterSettings
                {
                    Indent = true,
                    IndentChars = new string(' ', 4),
                    NewLineOnAttributes = false,
                    Encoding = Encoding.UTF8
                };

                using (var xmlWriter = XmlWriter.Create(memStream, settings))
                {
                    var namespaces = new XmlSerializerNamespaces();
                    namespaces.Add(string.Empty, string.Empty);
                    serializer.Serialize(xmlWriter, lacationBookmark, namespaces);
                }
                var xmlData = Encoding.UTF8.GetString(memStream.GetBuffer());
                xmlData = xmlData.Substring(xmlData.IndexOf('<'));
                xmlData = xmlData.Substring(0, xmlData.LastIndexOf('>') + 1);
                return xmlData;
            }
        }

        /// <summary>
        /// 현재 DeviceInfo 를 XML (file) 로 저장한다. 
        /// </summary>
        /// <param name="locationBookmark"></param>
        /// <param name="fileName">
        /// 저장될 파일명을 지정.
        /// </param>
        /// <returns>
        /// 저장 성공 유무를 반환.
        /// </returns>
        private static string Write(LocationBookmark locationBookmark, string fileName)
        {
            string xmlData;
            try
            {
                xmlData = DataToXml(locationBookmark);
                TextWriter streamWriter = new StreamWriter(fileName, false, Encoding.UTF8);
                streamWriter.Write(xmlData);
                streamWriter.Flush();
                streamWriter.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return string.Empty;
            }

            return xmlData;
        }

        /// <summary>
        /// 실행파일명을 통해서 실행파일경로명+ "LocationBookmark.xml"을 반환한다.
        /// </summary>
        /// <returns>
        /// 파일 전체 경로를 반환한다.
        /// </returns>
        private static string GetFileName(string exePath)
        {
            var path = Path.GetDirectoryName(exePath);
            return path + @"\" + "LocationBookmark.xml";
        }
    }
}
