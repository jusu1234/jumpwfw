﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CameraListNameConverter.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Defines the CameraListNameIdConverter type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using Innotive.InnoWatch.Console.Configs;

namespace Innotive.InnoWatch.Console.Converters
{
    using System;
    using System.Globalization;
    using System.Windows.Data;
    using Innotive.InnoWatch.Console.Properties;

    /// <summary>
    /// Console에 있는 Camera List에 Name과 Id를 표출한다.
    /// </summary>
    public class CameraListNameConverter : IMultiValueConverter
    {
        /// <summary>
        /// Console에 있는 Camera List에 Name과 Id를 표출한다.
        /// </summary>
        /// <param name="values">
        /// The values.
        /// </param>
        /// <param name="targetType">
        /// The target type.
        /// </param>
        /// <param name="parameter">
        /// The parameter: 사용하지 않음.
        /// </param>
        /// <param name="culture">
        /// The culture.
        /// </param>
        /// <returns>
        /// The object: 스트링을 반환한다.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            // string cameraListNameId = Settings.Default.CameraListNameFormat;
            var cameraListNameId = CommandConfigExecution.GetInstance().Config.Command.StageMode.CameraList.NameFormat.Value;

            var name = values[0] as string;
            cameraListNameId = !string.IsNullOrEmpty(name) ? cameraListNameId.Replace("{NAME}", name) : cameraListNameId.Replace("{NAME}", string.Empty);

            var id = values[1] as string;
            cameraListNameId = !string.IsNullOrEmpty(id) ? cameraListNameId.Replace("{ID}", id) : cameraListNameId.Replace("{ID}", string.Empty);

            return cameraListNameId;    
        }

        /// <summary>
        /// The ConvertBack.
        /// </summary>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <param name="targetTypes">
        /// The target types.
        /// </param>
        /// <param name="parameter">
        /// The parameter.
        /// </param>
        /// <param name="culture">
        /// The culture.
        /// </param>
        /// <returns>
        /// The objec[].
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}