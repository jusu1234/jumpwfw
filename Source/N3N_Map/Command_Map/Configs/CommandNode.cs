﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CommandNode.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Defines the CommandNode type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.Console.Configs
{
    using System.Xml.Serialization;
    using Alarm;
    using Control;
    using Innotive.InnoWatch.Commons.Configs;
    using SettingWindow;
    using StageMode;
    using Resources;
    
    /// <summary>
    /// </summary>
    [XmlRoot(ElementName = "Command")]
    public class CommandNode
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CommandNode"/> class.
        /// </summary>
        public CommandNode()
        {
            this.Alarm = new CommandAlarmNode();
            this.Control = new CommandControlNode();
            this.SettingWindow = new CommandSettingWindowNode();
            this.StageMode = new CommandStageModeNode();
            this.Resources = new CommandResourcesNode();
        }

        /// <summary>
        /// 다른 Config와 머지를 한다. default_config의 Editable 값 기준으로 함.
        /// </summary>
        /// <param name="defaultConfig"></param>
        public void Merge(CommandNode defaultConfig)
        {
            CommonsConfig.FindEditableAndSet(this, defaultConfig);
        }

        /// <summary>
        /// Gets or sets Alarm.
        /// </summary>
        public CommandAlarmNode Alarm { get; set; }
        
        /// <summary>
        /// Gets or sets Control.
        /// </summary>
        public CommandControlNode Control { get; set; }

        /// <summary>
        /// Gets or sets SettingWindow.
        /// </summary>
        public CommandSettingWindowNode SettingWindow { get; set; }

        /// <summary>
        /// Gets or sets StageMode.
        /// </summary>
        public CommandStageModeNode StageMode { get; set; }

        /// <summary>
        /// Gets or sets Resources.
        /// </summary>
        public CommandResourcesNode Resources { get; set; }
    }
}
