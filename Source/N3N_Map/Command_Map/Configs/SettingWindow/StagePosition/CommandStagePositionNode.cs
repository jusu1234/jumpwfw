﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CommandStagePositionNode.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Defines the StagePositionSettingNode type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.Console.Configs.SettingWindow.StagePosition
{
    using System.Xml.Serialization;
    using Commons.Configs.AttributeTypes;

    /// <summary>
    /// Setting Windows에 있는 Stage Position 설정을 사용한다.
    /// </summary>
    [XmlRoot(ElementName = "StagePosition")]
    public class CommandStagePositionNode
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CommandStagePositionNode"/> class.
        /// </summary>
        public CommandStagePositionNode()
        {
            this.Auto = new AutoNode(false, "Stage 설정 시 크기 위치 지정을 자동으로 할 지 설정합니다. 자동으로 사용하지 않으면 사용자가 지정한 위치에 표시합니다. (true/false)");
        }

        /// <summary>
        /// Gets or sets Use.
        /// True: 스테이지의 크기와 좌표가 자동으로 설정된다.
        /// </summary>
        public AutoNode Auto { get; set; }
    }
}
