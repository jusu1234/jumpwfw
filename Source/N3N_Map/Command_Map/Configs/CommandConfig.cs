﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CommandConfig.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Defines the CommandConfig type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.Console.Configs
{
    using System.Xml.Serialization;
    using Commons.Configs;
    using PlayerCommons.Configs;

    /// <summary>
    /// iCommand에서 사용되는 Config 파일.
    /// </summary>
    [XmlRoot(ElementName = "CommandConfig")]
    public class CommandConfig : BaseConfig
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CommandConfig"/> class.
        /// </summary>
        public CommandConfig()
        {
            this.Common = new CommonNode();
            this.DisplayCommon = new DisplayCommonNode(true);
            this.Command = new CommandNode();
        }

        /// <summary>
        /// 다른 Config와 머지를 한다. default_config의 Editable 값 기준으로 함.
        /// </summary>
        /// <param name="default_config"></param>
        public void Merge(CommandConfig default_config)
        {
            this.Common.Merge(default_config.Common);
            this.DisplayCommon.Merge(default_config.DisplayCommon);
            this.Command.Merge(default_config.Command);
        }

        /// <summary>
        /// Gets or sets Common.
        /// </summary>
        public CommonNode Common { get; set; }

        /// <summary>
        /// Gets or sets DisplayCommon.
        /// </summary>
        public DisplayCommonNode DisplayCommon { get; set; }

        /// <summary>
        /// Gets or sets Command.
        /// </summary>
        public CommandNode Command { get; set; }
    }
}
