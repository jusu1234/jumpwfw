﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CommandKeyboardNode.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Defines the CommandKeyboardNode type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.Console.Configs.StageMode.Keyboard
{
    /// <summary>
    /// 
    /// </summary>
    public class CommandKeyboardNode
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CommandKeyboardNode"/> class.
        /// </summary>
        public CommandKeyboardNode()
        {
            this.UseControlKeyForShortcuts = new CommandUseControlKeyForShortcutsNode(false);
            
        }

        /// <summary>
        /// Gets or sets LoadSameLayout.
        /// </summary>
        public CommandUseControlKeyForShortcutsNode UseControlKeyForShortcuts { get; set; }

    }
}
