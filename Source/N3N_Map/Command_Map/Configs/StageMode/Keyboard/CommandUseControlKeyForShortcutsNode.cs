﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CommandUseControlKeyForShortcutsNode.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Defines the CommandUseControlKeyForShortcutsNode type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.Console.Configs.StageMode.Keyboard
{
    using Commons.Configs;
    using System.Xml.Serialization;

    /// <summary>
    /// 펀션키 단축키 사용시 Control 키 사용 여부.
    /// </summary>
    public class CommandUseControlKeyForShortcutsNode : ConvertBoolean
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CommandUseControlKeyForShortcutsNode"/> class.
        /// </summary>
        public CommandUseControlKeyForShortcutsNode()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CommandUseControlKeyForShortcutsNode"/> class.
        /// </summary>
        /// <param name="defaultValue">
        /// The default Value.
        /// </param>
        public CommandUseControlKeyForShortcutsNode(bool defaultValue)
            : base(defaultValue)
        {
            this.Comment = "펀션키 단축키 사용시 Control 키 사용 여부를 설정합니다. (true/false)";
        }

        /// <summary>
        /// Gets or sets Comment.
        /// </summary>
        [XmlAttribute]
        public string Comment { get; set; }
    }
}