﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="NameFormatNode.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Defines the NameFormatNode type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.Console.Configs.StageMode.CameraList
{
    using System.Xml.Serialization;

    /// <summary>
    /// 카메라 리스트에 표시되는 문자열 포맷을 설정한다.
    /// </summary>
    public class CommandNameFormatNode : Innotive.InnoWatch.Commons.Configs.AttributeTypes.BaseNode
    {
        public CommandNameFormatNode()
        {
            this.Value = "{NAME} ({ID})";
            this.Comment = "카메라 목록 탭에 표시되는 카메라 레이블 형식을 설정합니다. 이름은 {NAME}, 아이디는 {ID}를 입력하면 됩니다.";
        }

        /// <summary>
        /// Gets or sets Value.
        /// </summary>
        [XmlAttribute]
        public string Value { get; set; }
    }
}
