﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CommandPlayOnControlMode.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Defines the CommandPlayOnControlMode type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.Console.Configs.StageMode.Stage
{
    using Innotive.InnoWatch.Commons.Configs;

    /// <summary>
    /// 스테이지 모드에서 제어 상태인 컨트롤만 영상을 재생시킨다.
    /// </summary>
    public class CommandPlayOnControlMode : ConvertBoolean
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CommandPlayOnControlMode"/> class.
        /// </summary>
        public CommandPlayOnControlMode()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CommandPlayOnControlMode"/> class.
        /// </summary>
        /// <param name="defaultValue">
        /// The default Value.
        /// </param>
        public CommandPlayOnControlMode(bool defaultValue) : base(defaultValue)
        {
            this.Comment = "제어 모드일 경우에만 영상을 출력할 지 여부를 설정합니다.(true/false)";
        }
    }
}
