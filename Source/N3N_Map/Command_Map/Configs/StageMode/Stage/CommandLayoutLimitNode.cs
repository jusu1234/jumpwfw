﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CommandLayoutLimitNode.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Defines the CommandLayoutLimitNode type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.Console.Configs.StageMode.Stage
{
    using System.Xml.Serialization;

    /// <summary>
    /// 스테이지당 레이아웃이 들어갈 수 있는 개수를 설정한다.
    /// </summary>
    public class CommandLayoutLimitNode : Innotive.InnoWatch.Commons.Configs.AttributeTypes.BaseNode
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CommandLayoutLimitNode"/> class.
        /// </summary>
        public CommandLayoutLimitNode()
        {
            this.Value = 3;
            this.Comment = "Stage당 추가할 수 있는 최대 레이아웃 개수를 설정합니다.";
        }

        /// <summary>
        /// Gets or sets Value.
        /// </summary>
        [XmlAttribute]
        public int Value { get; set; }
    }
}
