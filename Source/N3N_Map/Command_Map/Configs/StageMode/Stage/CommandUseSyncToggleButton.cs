﻿namespace Innotive.InnoWatch.Console.Configs.StageMode.Stage
{
    using Commons.Configs;

    /// <summary>
    /// Sync Toggle Button 사용 여부를 설정합니다.
    /// </summary>
    public class CommandUseSyncToggleButton : ConvertBoolean
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CommandUseSyncToggleButton"/> class.
        /// </summary>
        public CommandUseSyncToggleButton()
        {
        }

        public CommandUseSyncToggleButton(bool defaultValue) : base(defaultValue)
        {
            this.Comment = "Sync Toggle Button 사용 여부를 설정합니다.(true/false)";
        }
    }
}
