﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CommandMouseOverBorderVisible.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Defines the CommandMouseOverBorderVisible type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.Console.Configs.StageMode.StageCell
{
    using Commons.Configs;
    using System.Xml.Serialization;

    /// <summary>
    /// 마우스가 셀 위에 올라가면 노란 테두리 표시 유무.
    /// </summary>
    public class CommandMouseOverBorderVisibleNode : ConvertBoolean
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CommandMouseOverBorderVisibleNode"/> class.
        /// </summary>
        public CommandMouseOverBorderVisibleNode()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CommandMouseOverBorderVisibleNode"/> class.
        /// </summary>
        /// <param name="defaultValue">
        /// The default Value.
        /// </param>
        public CommandMouseOverBorderVisibleNode(bool defaultValue) : base(defaultValue)
        {
            this.Comment = "Stage의 Cell 위에 마우스를 올렸을 때 테두리를 보여줄지 여부를 설정합니다. (true/false)";
        }
    }
}
