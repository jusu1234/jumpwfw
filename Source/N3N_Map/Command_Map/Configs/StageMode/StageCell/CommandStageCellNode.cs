﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CommandStageCellNode.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Defines the CommandStageCellNode type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.Console.Configs.StageMode.StageCell
{
    /// <summary>
    /// </summary>
    public class CommandStageCellNode
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CommandStageCellNode"/> class.
        /// </summary>
        public CommandStageCellNode()
        {
            this.AutoClearSelection = new CommandAutoClearSelectionNode(false);
            this.MouseOverBorderVisible = new CommandMouseOverBorderVisibleNode(true);
            this.MouseSelectBorderVisible = new CommandMouseSelectBorderVisibleNode(true);
        }

        /// <summary>
        /// Gets or sets AutoClearSelection.
        /// </summary>
        public CommandAutoClearSelectionNode AutoClearSelection { get; set; }

        /// <summary>
        /// Gets or sets MouseOverBorderVisible.
        /// </summary>
        public CommandMouseOverBorderVisibleNode MouseOverBorderVisible { get; set; }

        /// <summary>
        /// Gets or sets MouseSelectBorderVisible.
        /// </summary>
        public CommandMouseSelectBorderVisibleNode MouseSelectBorderVisible { get; set; }
    }
}
