﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CommandImageNode.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Defines the CommandImageNode type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.Console.Configs.StageMode.Image
{
    using Commons.Configs.AttributeTypes;

    /// <summary>
    /// 스테이지 모드에서 이미지 사용 설정.
    /// </summary>
    public class CommandImageNode
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CommandImageNode"/> class.
        /// </summary>
        public CommandImageNode()
        {
            this.Use = new UseNode(true, "이미지 사용 유무를 설정합니다. (true/false)");
        }

        /// <summary>
        /// Gets or sets Value.
        /// 스테이지 모드에서 레이아웃 목록을 사용할지 설정한다.
        /// 기본값: true.
        /// </summary>
        public UseNode Use { get; set; }
    }
}
