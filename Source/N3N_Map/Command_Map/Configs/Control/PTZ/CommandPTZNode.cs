﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Innotive.InnoWatch.Console.Configs.Control.PTZ
{
    public class CommandPTZNode
    {
        public CommandPTZNode()
        {
            this.SerialJoystick = new CommandSerialJoystickNode();
        }

        public CommandSerialJoystickNode SerialJoystick { get; set; }
    }
}
