﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Innotive.InnoWatch.Console.Configs.Control.PTZ
{
    public class CommandSerialJoystickNode : Innotive.InnoWatch.Commons.Configs.AttributeTypes.BaseNode
    {
        public CommandSerialJoystickNode()
        {
            this.Use = true;
            this.Protocol = "Sungjin";
            this.PortName = "COM3";
            this.BaudRate = 2400;
            this.DataBits = 8;
            this.ServiceUrl = "http://localhost:36000/rest/PTZControl/Command?";
            this.Parity = Parity.Mark;
            this.StopBits = StopBits.One;
            
            this.Comment = "PTZ 시리얼 조이스틱 설정을 합니다.";
        }

        [XmlAttribute]
        public bool Use { get; set; }

        [XmlAttribute]
        public string Protocol { get; set; }

        [XmlAttribute]
        public string PortName { get; set; }

        [XmlAttribute]
        public int BaudRate { get; set; }

        [XmlAttribute]
        public int DataBits { get; set; }

        [XmlAttribute]
        public string ServiceUrl { get; set; }

        [XmlAttribute]
        public Parity Parity { get; set; }

        [XmlAttribute]
        public StopBits StopBits { get; set; }
    }
}
