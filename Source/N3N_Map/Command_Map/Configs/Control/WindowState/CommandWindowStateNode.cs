﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Innotive.InnoWatch.Console.Configs.Control.WindowState
{
    public class CommandWindowStateNode
    {
        public CommandWindowStateNode()
        {
            this.WindowMode = new CommandWindowModeNode();
            this.MultiMonitorForFullScreen = new CommandMultiMonitorForFullScreenNode();
        }
        
        /// <summary>
        /// Gets or set FullScreenMode.
        /// </summary>
        public CommandWindowModeNode WindowMode { get; set; }

        public CommandMultiMonitorForFullScreenNode MultiMonitorForFullScreen { get; set; }
    }
}
