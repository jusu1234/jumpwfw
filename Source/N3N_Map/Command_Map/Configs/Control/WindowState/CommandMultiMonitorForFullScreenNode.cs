﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Innotive.InnoWatch.Commons.Configs.AttributeTypes;

namespace Innotive.InnoWatch.Console.Configs.Control.WindowState
{
    public class CommandMultiMonitorForFullScreenNode
    {
        public CommandMultiMonitorForFullScreenNode()
        {
            this.Use = new UseNode(false, "전체화면 사용시 멀티 모니터를 이용 여부.(true/false)");
        }

        public UseNode Use { get; set; }
    }
}
