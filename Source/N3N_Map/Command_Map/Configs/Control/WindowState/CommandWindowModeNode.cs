﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Innotive.InnoWatch.Commons.Configs.AttributeTypes;

namespace Innotive.InnoWatch.Console.Configs.Control.WindowState
{
    public class CommandWindowModeNode
    {
        public CommandWindowModeNode()
        {
            this.Use = new UseNode(false, "윈도우 모드 사용 여부를 결정 합니다. (true/false)");
        }

        public UseNode Use { get; set; }
    }
}
