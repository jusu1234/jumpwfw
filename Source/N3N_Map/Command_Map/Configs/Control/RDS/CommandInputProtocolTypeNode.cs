﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CommandInputProtocolTypeNode.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   기본값: 1
//   0: 기존 사내 프로토콜을 사용하여 RDS의 INPUT을 제어한다.
//   1: VNC 프토토콜을 사용하여 RDS의 INPUT을 제어한다.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.Console.Configs.Control.RDS
{
    using System.Xml.Serialization;

    /// <summary>
    /// 기본값: 1
    /// 0: 기존 사내 프로토콜을 사용하여 RDS의 INPUT을 제어한다.
    /// 1: VNC 프토토콜을 사용하여 RDS의 INPUT을 제어한다.
    /// </summary>
    public class CommandInputProtocolTypeNode : Innotive.InnoWatch.Commons.Configs.AttributeTypes.BaseNode
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CommandInputProtocolTypeNode"/> class.
        /// </summary>
        public CommandInputProtocolTypeNode()
        {
            this.Value = 0;
            this.Comment = "RDS 제어 시 사용할 프로토콜을 설정합니다. (0: InnotiveRDS, 1: VNC)";
            this.Type = "enum=0,1";
        }

        /// <summary>
        /// Gets or sets Value.
        /// </summary>
        [XmlAttribute]
        public int Value { get; set; }
    }
}
