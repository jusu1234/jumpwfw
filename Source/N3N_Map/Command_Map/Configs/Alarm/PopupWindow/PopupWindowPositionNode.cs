﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PopupWindowPositionNode.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   알람 팝업 창 위치를 설정한다.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.Console.Configs.Alarm.PopupWindow
{
    using System.Xml.Serialization;

    /// <summary>
    /// 알람 팝업 창 위치를 설정한다
    /// </summary>
    public class PopupWindowPositionNode : Innotive.InnoWatch.Commons.Configs.AttributeTypes.BaseNode
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PopupWindowPositionNode"/> class.
        /// </summary>
        public PopupWindowPositionNode()
        {
            this.Top = -1;
            this.Left = -1;
            this.Comment = "알람 팝업 창 위치를 설정합니다. (기본값 Top: -1, Left: -1, 둘중 하나라도 음수값을 가지면 자동으로 설정한다.)";
            this.Type = "none";
        }

        /// <summary>
        /// Gets or sets a value indicating whether Value.
        /// </summary>
        [XmlAttribute]
        public int Top { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether Value.
        /// </summary>
        [XmlAttribute]
        public int Left { get; set; }
    }
}
