﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PopupWindowNode.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Defines the PopupWindowNode type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------



namespace Innotive.InnoWatch.Console.Configs.Alarm.PopupWindow
{
    using Innotive.InnoWatch.Commons.Configs.AttributeTypes;

    public class PopupWindowNode
    {
         /// <summary>
        /// Initializes a new instance of the <see cref="PopupWindowNode"/> class.
        /// </summary>
        public PopupWindowNode()
        {
            this.Use = new UseNode(true, "알람 팝업 윈도우 사용 여부를 설정합니다. (true/false)");
            this.Position = new PopupWindowPositionNode();
        }
        
        /// <summary>
        /// Gets or sets UseNode.
        /// 알람 기능을 사용할 것인지 설정한다.
        /// 기본값: true.
        /// </summary>
        public UseNode Use { get; set; }

        /// <summary>
        /// 알람 팝업 위치를 설정한다.
        /// 기본값 : Top 0, Left 0.
        /// </summary>
        public PopupWindowPositionNode Position { get; set; }
    }
}
