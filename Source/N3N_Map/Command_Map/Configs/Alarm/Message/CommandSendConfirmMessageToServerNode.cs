﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SendConfirmMessageToServerNode.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Defines the SendConfirmMessageToServerNode type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.Console.Configs.Alarm.Message
{
    using Commons.Configs;
    using System.Xml.Serialization;

    /// <summary>
    /// Alarm이 왔을때 Confirm Message를 서버에 보낼지 결정한다.
    /// </summary>
    public class CommandSendConfirmMessageToServerNode : ConvertBoolean
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CommandSendConfirmMessageToServerNode"/> class.
        /// </summary>
        public CommandSendConfirmMessageToServerNode()
        { 
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CommandSendConfirmMessageToServerNode"/> class.
        /// </summary>
        /// <param name="defaultValue">
        /// The default Value.
        /// </param>
        public CommandSendConfirmMessageToServerNode(bool defaultValue) : base(defaultValue)
        {
            this.Comment = "서버에 알람 확인 메시지를 전송할 지 설정합니다. (true/false)";
        }
    }
}