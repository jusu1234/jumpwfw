﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CommandProductNameNode.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.Console.Configs.Resources.Product
{
    using System.Xml.Serialization;

    /// <summary>
    /// 화면에 표시할 제품 이름 속성.
    /// </summary>
    public class CommandProductNameNode : Innotive.InnoWatch.Commons.Configs.AttributeTypes.BaseNode
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CommandProductNameNode"/> class.
        /// </summary>
        public CommandProductNameNode()
        {
            this.Value = "INNOWATCH";
            this.Comment = "화면에 표시할 제품 이름을 설정합니다. (없으면 기본값 사용)";
            this.Type = "string";
        }

        /// <summary>
        /// Gets or sets Value.
        /// </summary>
        [XmlAttribute]
        public string Value { get; set; }
    }
}
