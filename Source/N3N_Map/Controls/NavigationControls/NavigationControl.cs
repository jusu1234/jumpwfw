﻿using System.Windows;
using System.Windows.Media;

using Innotive.InnoWatch.Commons.BaseControls;
using System.Windows.Input;
using System.ComponentModel;

namespace Innotive.InnoWatch.DLLs.NavigationControls
{
    class NavigationControl : BaseControl
    {
        static NavigationControl()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(NavigationControl), new FrameworkPropertyMetadata(typeof(NavigationControl)));
        }

        public NavigationControl()
        {

        }
    }
}
