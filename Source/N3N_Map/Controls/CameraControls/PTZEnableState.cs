// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PTZEnableState.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   PTZ를 언제 사용할지를 설정한다.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.DLLs.CameraControls
{
    /// <summary>
    /// PTZ를 언제 사용할지를 설정한다.
    /// </summary>
    public enum PTZEnableState
    {
        /// <summary>
        /// PTZ 를 항시 사용.
        /// </summary>
        Always,

        /// <summary>
        /// PTZ 항시 사용안함.
        /// </summary>
        None,

        /// <summary>
        /// 레이아웃의 줌 레벨에 따라 PTZ를 사용한다.
        /// </summary>
        ZoomLevel
    }
}