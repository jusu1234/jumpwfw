// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CameraControl.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Defines the Camera Control
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.DLLs.CameraControls
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Threading;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Controls.Primitives;
    using System.Windows.Data;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Animation;
    using System.Windows.Shapes;
    using Commons.BaseControls;
    using Commons.CameraManagers;
    using Commons.Configs;
    using Commons.Converters;
    using Commons.Publics;
    using Innotive.InnoWatch.DLLs.MapControls;
    using Innotive.InnoWatch.DLLs.LayoutControls;
    using Innotive.InnoWatch.CommonUtils.Models;
    using Innotive.InnoWatch.Commons.Utils;
    using Innotive.InnoWatch.Commons.CameraController;
    using Innotive.InnoWatch.Commons.CameraController.CameraControlInfos;
    using Innotive.InnoWatch.Commons.CameraManagers.DeviceInfo;
    using Innotive.InnoWatch.Commons.CameraManagers.InnoVideoImages;
    using Innotive.InnoWatch.CommonUtils.Log;
    using InnoVideoLib;
    using System.Windows.Media.Imaging;

    /// <summary>
    /// CameraControl class.
    /// </summary>
    [TemplatePart(Name = "xGrid", Type = typeof(Grid))]
    [TemplatePart(Name = "xVideoRect", Type = typeof(Rectangle))]
    [TemplatePart(Name = "xFadeOutVideoRect", Type = typeof(Rectangle))]
    [TemplatePart(Name = "xTextBlock", Type = typeof(TextBlock))]
    [TemplatePart(Name = "xToggleButtonShowControlUIForCamera", Type = typeof(ToggleButton))]
    [TemplatePart(Name = "xViewBoxToggleButtonShowControlUIForCamera", Type = typeof(Viewbox))]
    public class CameraControl : BaseControl, ICameraPropertyChanged
    {
        #region Constants and Fields

        /// <summary>
        /// 제어 모드 일 경우만 고화질 재생.
        /// </summary>
        [Category("Camera Settings")]
        public static readonly DependencyProperty PlayOnControlModeProperty = DependencyProperty.Register(
            "PlayOnControlMode", typeof(bool), typeof(CameraControl), new FrameworkPropertyMetadata(false));

        /// <summary>
        /// 항상 고화질 우선 요청 여부.
        /// </summary>
        [Category("Camera Settings")]
        public static readonly DependencyProperty AlwaysHighResProperty = DependencyProperty.Register(
            "AlwaysHighRes", typeof(bool), typeof(CameraControl), new FrameworkPropertyMetadata(false));

        /// <summary>
        /// 테두리에 들어갈 이미지를 선택한다.
        /// </summary>
        [Category("Camera Settings")]
        public static readonly DependencyProperty BorderImageSourceProperty =
            DependencyProperty.Register(
                "BorderImageSource", typeof(string), typeof(CameraControl), new FrameworkPropertyMetadata(string.Empty));

        /// <summary>
        /// 카메라영상표출영역과 카메라 컨트롤간의 간격(Margin).
        /// </summary>
        [Category("Camera Settings")]
        public static readonly DependencyProperty BorderMarginProperty = DependencyProperty.Register(
            "BorderMargin", typeof(Thickness), typeof(CameraControl), new FrameworkPropertyMetadata(new Thickness()));

        /// <summary>
        /// Player/console화면상에 표시되는 CameraName.
        /// </summary>
        [Category("Camera Settings")]
        public static readonly DependencyProperty CameraNameProperty = DependencyProperty.Register(
            "CameraName", typeof(string), typeof(CameraControl), new FrameworkPropertyMetadata(string.Empty));

        /// <summary>
        /// Focus Control 가능 여부.
        /// </summary>
        [Category("Camera Settings")]
        public static readonly DependencyProperty FocusControlableProperty =
            DependencyProperty.Register(
                "FocusControlable", typeof(bool), typeof(CameraControl), new FrameworkPropertyMetadata(true));

        /// <summary>
        /// Camara 를 가져올수 있는 ID를 정의한다.
        /// </summary>
        [Category("Camera Settings")]
        public static readonly DependencyProperty IDProperty = DependencyProperty.Register(
            "ID", typeof(string), typeof(CameraControl), new FrameworkPropertyMetadata(string.Empty, new PropertyChangedCallback(OnChangedID)));

        /// <summary>
        /// Light Control 가능 여부.
        /// </summary>
        [Category("Camera Settings")]
        public static readonly DependencyProperty LightControlableProperty =
            DependencyProperty.Register(
                "LightControlable", typeof(bool), typeof(CameraControl), new FrameworkPropertyMetadata(false));

        /// <summary>
        /// Pan/Tilt Control 가능 여부.
        /// </summary>
        [Category("Camera Settings")]
        public static readonly DependencyProperty PanTiltControlableProperty =
            DependencyProperty.Register(
                "PanTiltControlable", typeof(bool), typeof(CameraControl), new FrameworkPropertyMetadata(true));

        /// <summary>
        /// Preset Control 가능 여부.
        /// </summary>
        [Category("Camera Settings")]
        public static readonly DependencyProperty PresetControlableProperty =
            DependencyProperty.Register(
                "PresetControlable", typeof(bool), typeof(CameraControl), new FrameworkPropertyMetadata(false));

        /// <summary>
        /// PTZ 가능 여부.
        /// </summary>
        [Category("Camera Settings")]
        public static readonly DependencyProperty PTZEnabledProperty = DependencyProperty.Register(
            "PTZEnabled",
            typeof(PTZEnableState),
            typeof(CameraControl),
            new FrameworkPropertyMetadata(PTZEnableState.None, new PropertyChangedCallback(OnChangedPTZEnabled)));

        /// <summary>
        /// PTZ의 MinLevel이 Camera Parent's MaxLevel을 따라갈 것인지 결정하는 프로퍼티 (Canvas 의 MaxZoomLevel에서만 PTZ 가 보인다).
        /// </summary>
        [Category("Camera Settings")]
        public static readonly DependencyProperty PTZLockToParentsMinLevelProperty =
            DependencyProperty.Register(
                "PTZLockToParentsMinLevel", typeof(bool), typeof(CameraControl), new FrameworkPropertyMetadata(false));

        /// <summary>
        /// PTZ Control MinLevel.
        /// </summary>
        [Category("Camera Settings")]
        public static readonly DependencyProperty PTZMinLevelProperty = DependencyProperty.Register(
            "PTZMinLevel", typeof(double), typeof(CameraControl), new FrameworkPropertyMetadata(0.0));

        /// <summary>
        /// Player/console화면상에 표시되는 CameraName의 색상.
        /// </summary>
        [Category("Camera Settings")]
        public static readonly DependencyProperty TextBrushProperty = DependencyProperty.Register(
            "TextBrush", typeof(Brush), typeof(CameraControl), new FrameworkPropertyMetadata(Brushes.Yellow));

        /// <summary>
        /// CameraName의 위치(Margin).
        /// </summary>
        [Category("Camera Settings")]
        public static readonly DependencyProperty TextMarginProperty = DependencyProperty.Register(
            "TextMargin",
            typeof(Thickness),
            typeof(CameraControl),
            new FrameworkPropertyMetadata(new Thickness(0, 0, 0, 0)));

        /// <summary>
        /// CameraName의 정렬(수평정렬).
        /// </summary>
        [Category("Camera Settings")]
        public static readonly DependencyProperty TextHorizontalAlignProperty = DependencyProperty.Register(
            "HorizontalAlign",
            typeof(TextAlignment),
            typeof(CameraControl),
            new FrameworkPropertyMetadata(new TextAlignment()));

        /// <summary>
        /// CameraName의 정렬(수직정렬).
        /// </summary>
        [Category("Camera Settings")]
        public static readonly DependencyProperty TextVerticalAlignProperty = DependencyProperty.Register(
            "VerticalAlign",
            typeof(VerticalAlignment),
            typeof(CameraControl),
            new FrameworkPropertyMetadata(new VerticalAlignment()));

        /// <summary>
        /// CameraName의 FontFamily.
        /// </summary>
        [Category("Camera Settings")]
        public static readonly DependencyProperty TextFontProperty = DependencyProperty.Register(
            "TextFont",
            typeof(FontFamily),
            typeof(CameraControl),
            new FrameworkPropertyMetadata(new FontFamily("Arial")));

        /// <summary>
        /// CameraName의 FontStyle.
        /// </summary>
        [Category("Camera Settings")]
        public static readonly DependencyProperty TextStyleProperty = DependencyProperty.Register(
            "TextStyle",
            typeof(FontStyle),
            typeof(CameraControl),
            new FrameworkPropertyMetadata(FontStyles.Normal));

        /// <summary>
        /// CameraName의 FontWeight.
        /// </summary>
        [Category("Camera Settings")]
        public static readonly DependencyProperty TextWeightProperty = DependencyProperty.Register(
            "TextWeight",
            typeof(FontWeight),
            typeof(CameraControl),
            new FrameworkPropertyMetadata(FontWeights.Black));

        /// <summary>
        /// CameraName의 Four Way Shadow Visibility.
        /// </summary>
        [Category("Camera Settings")]
        public static readonly DependencyProperty TextFourWayShadowVisibilityProperty = DependencyProperty.Register(
            "TextFourWayShadowVisibility",
            typeof(Visibility),
            typeof(CameraControl),
            new FrameworkPropertyMetadata(Visibility.Collapsed));

        /// <summary>
        /// CameraName의 Four Way Shadow Distance.
        /// </summary>
        [Category("Camera Settings")]
        public static readonly DependencyProperty TextFourWayShadowDistanceProperty = DependencyProperty.Register(
            "TextFourWayShadowDistance",
            typeof(double),
            typeof(CameraControl),
            new FrameworkPropertyMetadata(1.0));

        /// <summary>
        /// CameraName의 Four Way Shadow Brush.
        /// </summary>
        [Category("Camera Settings")]
        public static readonly DependencyProperty TextFourWayShadowBrushProperty = DependencyProperty.Register(
            "TextFourWayShadowBrush",
            typeof(Brush),
            typeof(CameraControl),
            new FrameworkPropertyMetadata(Brushes.Black));

        /// <summary>
        /// CameraName의 Four Way Shadow Opacity.
        /// </summary>
        [Category("Camera Settings")]
        public static readonly DependencyProperty TextFourWayShadowOpacityProperty = DependencyProperty.Register(
            "TextFourWayShadowOpacity",
            typeof(double),
            typeof(CameraControl),
            new FrameworkPropertyMetadata(1.0));

        /// <summary>
        /// Wiper Control 가능 여부.
        /// </summary>
        [Category("Camera Settings")]
        public static readonly DependencyProperty WiperControlableProperty =
            DependencyProperty.Register(
                "WiperControlable", typeof(bool), typeof(CameraControl), new FrameworkPropertyMetadata(false));

        /// <summary>
        /// Zoom Control  가능 여부.
        /// </summary>
        [Category("Camera Settings")]
        public static readonly DependencyProperty ZoomControlableProperty =
            DependencyProperty.Register(
                "ZoomControlable", typeof(bool), typeof(CameraControl), new FrameworkPropertyMetadata(true));

        internal static readonly DependencyProperty CurrentCameraBrushProperty =
            DependencyProperty.Register(
                "CurrentCameraBrush", typeof(Brush), typeof(CameraControl), new FrameworkPropertyMetadata(null));

        internal static readonly DependencyProperty NonSelectBorderBrushProperty =
            DependencyProperty.Register(
                "NonSelectBorderBrush", typeof(Brush), typeof(CameraControl), new UIPropertyMetadata(Brushes.White));

        internal static readonly DependencyProperty NonSelectBorderThicknessProperty =
            DependencyProperty.Register(
                "NonSelectBorderThickness", typeof(double), typeof(CameraControl), new UIPropertyMetadata(0.0));

        /// <summary>
        /// Player/console화면상에 표시되는 CameraName의 Outline색상.
        /// </summary>
        internal static readonly DependencyProperty OutlineBrushProperty = DependencyProperty.Register(
            "OutlineBrush", typeof(Brush), typeof(CameraControl), new FrameworkPropertyMetadata(Brushes.White));

        /// <summary>
        /// Player/console화면상에 표시되는 CameraName의 Outline의 굵기.
        /// </summary>
        internal static readonly DependencyProperty OutlineThicknessProperty =
            DependencyProperty.Register(
                "OutlineThickness", typeof(double), typeof(CameraControl), new FrameworkPropertyMetadata(0.0));

        internal static readonly DependencyProperty SelectBorderBrushProperty =
            DependencyProperty.Register(
                "SelectBorderBrush",
                typeof(Brush),
                typeof(CameraControl),
                new UIPropertyMetadata(new SolidColorBrush(Color.FromArgb(255, 0, 255, 38))));

        internal static readonly DependencyProperty SelectBorderThicknessProperty =
            DependencyProperty.Register(
                "SelectBorderThickness", typeof(double), typeof(CameraControl), new UIPropertyMetadata(5.0));

        private readonly Storyboard _storyBoardFadeOut = new Storyboard();

        private TextBlock _debugLowHighBorderTextBlock;

        private Rectangle _fadeOutVideoRect;

        private Grid _grid;

        private bool _isAppliedTemplate;

        private TextBlock _textBlock;

        private TextBlock _textBlockLeftShadow;

        private TextBlock _textBlockRightShadow;

        private TextBlock _textBlockTopShadow;

        private TextBlock _textBlockBottomShadow;

        private ToggleButton _toggleButtonShowControlUI;

        private Viewbox _viewBoxToggleButtonShowControlUIForCamera;

        protected Rectangle _videoRect;

        private bool isFit = true;

        private bool isEditingType = Public.IsEditingType();

        /// <summary>
        /// 현재 보이는 화면에 보이는 CameraControl 크기.
        /// </summary>
        private Rect viewPortRect;

        /// <summary>
        /// 화면 내,외부와 상관없는 CameraControl 전체 크기
        /// VideoElement에 Margin을 적용을 위해 BorderMargin 적용된 영상의 화면 영역으로 사용함. - Alvin 2012.06.25
        /// </summary>
        private Rect fullRect;

        /// <summary>
        /// 이전 UpdateViewArea()에서 측정했던 fullRect.
        /// </summary>
        private Rect oldFullRect;

        /// <summary>
        /// 할당된 MediaServer의 Ip 정보.
        /// </summary>
        private string mediaServerIp;

        /// <summary>
        /// VideoInformation에 입력되어 있는 자신의 카메라 정보.
        /// </summary>
        private CameraInfo cameraInfo;

        /// <summary>
        /// HighMiddleSurface에 그려질 자신의 VideoElement.
        /// </summary>
        protected CInnoVideoElement videoElement;

        protected bool isControlMode = false;

        private Dictionary<string, string> resolutions;

        /// <summary>
        /// CameraControl의 VideoElement가 생성된 VideoSurface.
        /// </summary>
        private VideoSurface ownerVideoSurface;

        private BaseControl parent;

        /// <summary>
        /// 드래그 시, 기존 마지막 갱신 Point
        /// </summary>
        private Point _lastDragPoint;

        /// <summary>
        /// 드래그 모드 구분을 위한 mouse Down flag
        /// </summary>
        private bool _isMouseDown = false;

        /// <summary>
        /// 현재 Zoom이 되어 있는지 flag
        /// </summary>
        private bool _isDigitalZoommed = false;

        #endregion

        #region Event

        // PTZControl쪽 UI가 눌려졌음. MultiGrid에서 Selection을 처리하기 위함 !!
        internal event EventHandler<EventArgs> ePTZControlUIClicked;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes static members of the <see cref="CameraControl"/> class.
        /// </summary>
        static CameraControl()
        {
            DefaultStyleKeyProperty.OverrideMetadata(
                typeof(CameraControl), new FrameworkPropertyMetadata(typeof(CameraControl)));
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CameraControl"/> class.
        /// </summary>
        public CameraControl()
        {
            RenderOptions.SetBitmapScalingMode(this, CommonsConfig.Instance.CameraResizeQuality);

            this.IsEnableToggleButtonShowControlUI = true;

            this.Background = Brushes.Black;
            this.OriginGUID = Guid.NewGuid();

            this.MouseEnter += CameraControl_MouseEnter;
            this.MouseLeave += CameraControl_MouseLeave;
            this.MouseWheel += new MouseWheelEventHandler(CameraControl_MouseWheel);

            this.MouseDown += new MouseButtonEventHandler(CameraControl_MouseDown);
            this.MouseUp += new MouseButtonEventHandler(CameraControl_MouseUp);
            this.MouseMove += new MouseEventHandler(CameraControl_MouseMove);

            this.PreviewKeyDown += CameraControl_PreviewKeyDown;

            this.IsManipulationEnabled = true;
            this.ManipulationStarting += CameraControl_ManipulationStarting;
            this.ManipulationDelta += CameraControl_ManipulationDelta;
            this.ManipulationInertiaStarting += CameraControl_ManipulationInertiaStarting;
        }

        #endregion

        #region Properties

        public bool PlayOnControlMode
        {
            get
            {
                return (bool)this.GetValue(PlayOnControlModeProperty);
            }

            set
            {
                this.SetValue(PlayOnControlModeProperty, value);
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether AlwaysHighRes.
        /// </summary>
        public bool AlwaysHighRes
        {
            get
            {
                return (bool)this.GetValue(AlwaysHighResProperty);
            }

            set
            {
                this.SetValue(AlwaysHighResProperty, value);
            }
        }

        /// <summary>
        /// Gets or sets BorderImageSource.
        /// </summary>
        public string BorderImageSource
        {
            get
            {
                return (string)this.GetValue(BorderImageSourceProperty);
            }

            set
            {
                this.SetValue(BorderImageSourceProperty, value);
            }
        }

        /// <summary>
        /// Gets or sets BorderMargin.
        /// </summary>
        public Thickness BorderMargin
        {
            get
            {
                return (Thickness)this.GetValue(BorderMarginProperty);
            }

            set
            {
                this.SetValue(BorderMarginProperty, value);
            }
        }

        /// <summary>
        /// Gets or sets CameraName.
        /// </summary>
        public string CameraName
        {
            get
            {
                return (string)this.GetValue(CameraNameProperty);
            }

            set
            {
                this.SetValue(CameraNameProperty, value);
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether FocusControlable.
        /// </summary>
        public bool FocusControlable
        {
            get
            {
                return (bool)this.GetValue(FocusControlableProperty);
            }

            set
            {
                this.SetValue(FocusControlableProperty, value);
            }
        }

        /// <summary>
        /// Gets or sets ID.
        /// </summary>
        public string ID
        {
            get
            {
                return (string)this.GetValue(IDProperty);
            }

            set
            {
                this.SetValue(IDProperty, value);
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether LightControlable.
        /// </summary>
        public bool LightControlable
        {
            get
            {
                return (bool)this.GetValue(LightControlableProperty);
            }

            set
            {
                this.SetValue(LightControlableProperty, value);
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether PanTiltControlable.
        /// </summary>
        public bool PanTiltControlable
        {
            get
            {
                return (bool)this.GetValue(PanTiltControlableProperty);
            }

            set
            {
                this.SetValue(PanTiltControlableProperty, value);
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether PresetControlable.
        /// </summary>
        public bool PresetControlable
        {
            get
            {
                return (bool)this.GetValue(PresetControlableProperty);
            }

            set
            {
                this.SetValue(PresetControlableProperty, value);
            }
        }

        /// <summary>
        /// Gets or sets PTZEnabled.
        /// </summary>
        public PTZEnableState PTZEnabled
        {
            get
            {
                return (PTZEnableState)this.GetValue(PTZEnabledProperty);
            }

            set
            {
                this.SetValue(PTZEnabledProperty, value);
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether PTZLockToParentsMinLevel.
        /// </summary>
        public bool PTZLockToParentsMinLevel
        {
            get
            {
                return (bool)this.GetValue(PTZLockToParentsMinLevelProperty);
            }

            set
            {
                this.SetValue(PTZLockToParentsMinLevelProperty, value);
            }
        }

        /// <summary>
        /// Gets or sets PTZMinLevel.
        /// </summary>
        public double PTZMinLevel
        {
            get
            {
                return (double)this.GetValue(PTZMinLevelProperty);
            }

            set
            {
                this.SetValue(PTZMinLevelProperty, value);
            }
        }

        /// <summary>
        /// Gets or sets TextBrush.
        /// </summary>
        public Brush TextBrush
        {
            get
            {
                return (Brush)this.GetValue(TextBrushProperty);
            }

            set
            {
                this.SetValue(TextBrushProperty, value);
            }
        }

        /// <summary>
        /// Gets or sets TextMargin.
        /// </summary>
        public Thickness TextMargin
        {
            get
            {
                return (Thickness)this.GetValue(TextMarginProperty);
            }

            set
            {
                this.SetValue(TextMarginProperty, value);
            }
        }

        /// <summary>
        /// Gets or sets HorizontalAlign.
        /// </summary>
        public TextAlignment HorizontalAlign
        {
            get
            {
                return (TextAlignment)this.GetValue(TextHorizontalAlignProperty);
            }

            set
            {
                this.SetValue(TextHorizontalAlignProperty, value);
            }
        }

        /// <summary>
        /// Gets or sets VerticalAlign.
        /// </summary>
        public VerticalAlignment VerticalAlign
        {
            get
            {
                return (VerticalAlignment)this.GetValue(TextVerticalAlignProperty);
            }

            set
            {
                this.SetValue(TextVerticalAlignProperty, value);
            }
        }

        /// <summary>
        /// Gets or sets TextFont.
        /// </summary>
        public FontFamily TextFont
        {
            get
            {
                return (FontFamily)this.GetValue(TextFontProperty);
            }

            set
            {
                this.SetValue(TextFontProperty, value);
            }
        }

        /// <summary>
        /// Gets or sets TextStyle.
        /// </summary>
        public FontStyle TextStyle
        {
            get
            {
                return (FontStyle)this.GetValue(TextStyleProperty);
            }

            set
            {
                this.SetValue(TextStyleProperty, value);
            }
        }

        /// <summary>
        /// Gets or sets TextWeight.
        /// </summary>
        public FontWeight TextWeight
        {
            get
            {
                return (FontWeight)this.GetValue(TextWeightProperty);
            }

            set
            {
                this.SetValue(TextWeightProperty, value);
            }
        }

        /// <summary>
        /// Gets or sets TextFourWayShadowVisibility.
        /// </summary>
        public Visibility TextFourWayShadowVisibility
        {
            get
            {
                return (Visibility)this.GetValue(TextFourWayShadowVisibilityProperty);
            }

            set
            {
                this.SetValue(TextFourWayShadowVisibilityProperty, value);
            }
        }

        /// <summary>
        /// Gets or sets TextFourWayShadowDistance.
        /// </summary>
        public double TextFourWayShadowDistance
        {
            get
            {
                return (double)this.GetValue(TextFourWayShadowDistanceProperty);
            }

            set
            {
                this.SetValue(TextFourWayShadowDistanceProperty, value);
            }
        }

        /// <summary>
        /// Gets or sets TextFourWayShadowBrush.
        /// </summary>
        public Brush TextFourWayShadowBrush
        {
            get
            {
                return (Brush)this.GetValue(TextFourWayShadowBrushProperty);
            }

            set
            {
                this.SetValue(TextFourWayShadowBrushProperty, value);
            }
        }

        /// <summary>
        /// Gets or sets TextFourWayShadowOpacity.
        /// </summary>
        public double TextFourWayShadowOpacity
        {
            get
            {
                return (double)this.GetValue(TextFourWayShadowOpacityProperty);
            }

            set
            {
                this.SetValue(TextFourWayShadowOpacityProperty, value);
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether WiperControlable.
        /// </summary>
        public bool WiperControlable
        {
            get
            {
                return (bool)this.GetValue(WiperControlableProperty);
            }

            set
            {
                this.SetValue(WiperControlableProperty, value);
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether ZoomControlable.
        /// </summary>
        public bool ZoomControlable
        {
            get
            {
                return (bool)this.GetValue(ZoomControlableProperty);
            }

            set
            {
                this.SetValue(ZoomControlableProperty, value);
            }
        }

        public string ControlType { get; set; }

        /// <summary>
        /// 저화질 전용 Brush.
        /// </summary>
        public Brush LowVideoCameraBrush { get; set; }

        /// <summary>
        /// 중, 고화질 전용 Brush.
        /// </summary>
        public Brush HighMiddleCameraBrush { get; set; }

        ///// <summary>
        ///// Stage 에서 Cell을 더블클릭으로 확대하였을 때 사용할 Brush.
        ///// </summary>
        //public Brush ZoomInBrush { get; set; }

        /// <summary>
        /// 저화질이 없는 경우 사용할 Snapshot ImageBrush.
        /// </summary>
        public Brush BufferBrush { get; set; }

        /// <summary>
        /// CameraControl이 Stage에서 생성되었는지 여부.
        /// </summary>
        public bool IsInStage { get; set; }

        internal Brush CurrentCameraBrush
        {
            get
            {
                return (Brush)this.GetValue(CurrentCameraBrushProperty);
            }
            set
            {
                this.SetValue(CurrentCameraBrushProperty, value);
                Debug.WriteLine(string.Format("ID : {0}   SetValue : {1}", this.ID, value));
            }
        }

        internal Brush NonSelectBorderBrush
        {
            get
            {
                return (Brush)this.GetValue(NonSelectBorderBrushProperty);
            }

            set
            {
                this.SetValue(NonSelectBorderBrushProperty, value);
            }
        }

        internal double NonSelectBorderThickness
        {
            get
            {
                return (double)this.GetValue(NonSelectBorderThicknessProperty);
            }

            set
            {
                this.SetValue(NonSelectBorderThicknessProperty, value);
            }
        }

        internal Brush OutlineBrush
        {
            get
            {
                return (Brush)this.GetValue(OutlineBrushProperty);
            }

            set
            {
                this.SetValue(OutlineBrushProperty, value);
            }
        }

        internal double OutlineThickness
        {
            get
            {
                return (double)this.GetValue(OutlineThicknessProperty);
            }

            set
            {
                this.SetValue(OutlineThicknessProperty, value);
            }
        }

        internal Brush SelectBorderBrush
        {
            get
            {
                return (Brush)this.GetValue(SelectBorderBrushProperty);
            }

            set
            {
                this.SetValue(SelectBorderBrushProperty, value);
            }
        }

        internal double SelectBorderThickness
        {
            get
            {
                return (double)this.GetValue(SelectBorderThicknessProperty);
            }

            set
            {
                this.SetValue(SelectBorderThicknessProperty, value);
            }
        }

        public bool IsEnableToggleButtonShowControlUI
        {
            get;
            set;
        }

        public ToggleButton ToggleButtonShowControlUI
        {
            get
            {
                return this._toggleButtonShowControlUI;
            }
        }

        public Viewbox ViewboxToggleButtonShowControlUIForCamera
        {
            get
            {
                return this._viewBoxToggleButtonShowControlUIForCamera;
            }
        }

        public string VideoSurfaceId { get; set; }

        /// <summary>
        /// Cell을 더블클릭 하여 ZoomIn 상태로 만들었는지 여부.
        /// </summary>
        public bool IsZoomInCell { get; set; }

        public bool IsAdded { get; set; }

        #endregion

        #region Public Methods

        /// <summary>
        /// PTZ Control 레이아웃 생성.
        /// </summary>
        public PTZControl CreatePTZControl()
        {
            var ptzParentGrid = this.Template.FindName("xPTZParentGrid", this) as Grid;
            if (ptzParentGrid != null)
            {
                if (ptzParentGrid.Children.Count == 0)
                {
                    Debug.WriteLine("[CameraControl] Create PTZ Control");

                    var ptzControl = new PTZControl(this.ID);
                    ptzParentGrid.Children.Add(ptzControl);
                    this.ptzControlBinding(ptzControl);
                    ptzControl.ePTZControlUIClicked += ptzControl_ePTZControlUIClicked;

                    return ptzControl;
                }
            }

            return null;
        }

        /// <summary>
        /// PTZ Control 레이아웃 삭제.
        /// </summary>
        public void DestoryPTZControl()
        {
            var ptzParentGrid = this.Template.FindName("xPTZParentGrid", this) as Grid;

            if (ptzParentGrid != null)
            {
                if (ptzParentGrid.Children.Count != 0)
                {
                    this.PtzControlClearBinding();

                    for (var i = ptzParentGrid.Children.Count - 1; i >= 0; i--)
                    {
                        var element = ptzParentGrid.Children[i];
                        if (element is IDisposable)
                        {
                            (element as IDisposable).Dispose();
                        }
                    }

                    Debug.WriteLine("[CameraControl] Destory PTZ Control");

                    ptzParentGrid.Children.Clear();
                }
            }
        }

        /// <summary>
        /// The get camera brush.
        /// </summary>
        /// <returns>
        /// </returns>
        public Brush GetCameraBrush()
        {
            return this.CurrentCameraBrush;
        }

        /// <summary>
        /// PTZ Control 가져오기.
        /// </summary>
        /// <returns>
        /// PtzControl을 반환한다.
        /// </returns>
        public PTZControl GetChildPTZControl()
        {
            if (this.Template == null)
            {
                return null;
            }

            var ptzParentGrid = this.Template.FindName("xPTZParentGrid", this) as Grid;

            if (ptzParentGrid != null)
            {
                if (ptzParentGrid.Children.Count != 0)
                {
                    foreach (FrameworkElement element in ptzParentGrid.Children)
                    {
                        if (element is PTZControl)
                        {
                            return element as PTZControl;
                        }
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// The get icon.
        /// </summary>
        /// <returns>
        /// </returns>
        public override FrameworkElement GetIcon()
        {
            return new IconUserControl();
        }

        /// <summary>
        /// The on apply template.
        /// </summary>
        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            this._grid = this.Template.FindName("xGrid", this) as Grid;
            this._videoRect = this.Template.FindName("xVideoRect", this) as Rectangle;
            this._fadeOutVideoRect = this.Template.FindName("xFadeOutVideoRect", this) as Rectangle;
            this._textBlock = this.Template.FindName("xTextBlock", this) as TextBlock;
            this._textBlockLeftShadow = this.Template.FindName("xTextBlockLeftShadow", this) as TextBlock;
            this._textBlockRightShadow = this.Template.FindName("xTextBlockRightShadow", this) as TextBlock;
            this._textBlockTopShadow = this.Template.FindName("xTextBlockTopShadow", this) as TextBlock;
            this._textBlockBottomShadow = this.Template.FindName("xTextBlockBottomShadow", this) as TextBlock;
            this._toggleButtonShowControlUI = this.Template.FindName("xToggleButtonShowControlUIForCamera", this) as ToggleButton;
            this._viewBoxToggleButtonShowControlUIForCamera = this.Template.FindName("xViewBoxToggleButtonShowControlUIForCamera", this) as Viewbox;

            if (this._grid != null)
            {
                this._grid.SizeChanged += this.CameraControl_SizeChanged;
            }

            if (this._toggleButtonShowControlUI != null)
            {
                this._toggleButtonShowControlUI.Click += this._toggleButtonShowControlUI_Click;
                this._toggleButtonShowControlUI.StylusUp += this._toggleButtonShowControlUI_StylusUp;
                this._toggleButtonShowControlUI.MouseEnter += this._toggleButtonShowControlUI_MouseEnter;                
                this._toggleButtonShowControlUI.Checked += this._toggleButtonShowControlUI_Checked;
                this._toggleButtonShowControlUI.Unchecked += _toggleButtonShowControlUI_Unchecked;
            }

            if (!CameraManager.CameraControls.ContainsKey(this.OriginGUID))
            {
                CameraManager.CameraControls.Add(this.OriginGUID, this);
            }
            else
            {
                CameraManager.CameraControls[this.OriginGUID] = this;
            }

            this._isAppliedTemplate = true;

            this.ControlType = VideoInformation.GetControlType(CameraManager.Instance.VideoInfo, this.ID);

            // CameraFont setting.
            this.SetCameraLabel();

            this.parent = this.GetCurrentParent(this);
            //this.oldCameraState = new CameraState();
        }

        public void ToggleSelected()
        {
            var isChecked = !(bool)(this._toggleButtonShowControlUI).IsChecked;
            this.ToggleButtonClicked(isChecked);
            this.ShowPTZControlButton(isChecked);
        }

        public void ToggleButtonClicked(bool isChecked)
        {
            // 버튼이 토글 될때 마다 디지털 확대/축소 초기화
            if (this.CurrentCameraBrush != null)
            {
                var rect = (this.CurrentCameraBrush as TileBrush).Viewport;
                if (rect.X != 0 || rect.Y != 0 || rect.Width != 1 || rect.Height != 1)
                {
                    (this.CurrentCameraBrush as TileBrush).Viewport = new Rect(0, 0, 1, 1);
                }
            }

            EnablePTZControlMode(isChecked);

            //제어모드 또는 제어모드해제와 상관없이 UI 초기화를 함
            if (this.GetChildPTZControl() != null)
                this.GetChildPTZControl().InitUI();

            PTZManager.Instance.ShowCameraControllerUI(this, this.ID, isChecked);

            //PTZControl에서 MouseDown시 무조건 Cell의 빨간색 테두리가 보여지게 함
            this.RaisePTZControlUIClickedEvent();
        }

        public void ZoomLevelChanged(bool isFit)
        {
            this.isFit = isFit;
            if (this.IsMouseOver)
                this.ShowPTZControlButton(isFit);

            if (!isFit)
            {
                this.ShowPTZControlButton(false);
                this.EnablePTZControlMode(false);
            }
        }

        public void ShowPTZControlButton(bool isShow)
        {
            if (isShow)
            {
                //Stage에서 Layout이나 RDS 제어중이지 않은 경우
                if (IsEnableToggleButtonShowControlUI && this.ToggleButtonShowControlUI != null)
                {
                    if (this.PTZEnabled == PTZEnableState.Always)
                    {
                        this.ToggleButtonShowControlUI.Visibility = Visibility.Visible;
                        this.ViewboxToggleButtonShowControlUIForCamera.Visibility = System.Windows.Visibility.Visible;
                    }
                    else if (this.PTZEnabled == PTZEnableState.ZoomLevel)
                    {
                        if (this.isFit)
                        {
                            this.ToggleButtonShowControlUI.Visibility = Visibility.Visible;
                            this.ViewboxToggleButtonShowControlUIForCamera.Visibility = System.Windows.Visibility.Visible;
                        }
                    }
                }
            }
            else
            {
                if (this.ToggleButtonShowControlUI != null)
                {
                    this.ToggleButtonShowControlUI.Visibility = Visibility.Collapsed;
                    this.ViewboxToggleButtonShowControlUIForCamera.Visibility = System.Windows.Visibility.Collapsed;
                }
            }
        }

        public void EnablePTZControlMode(bool isEnable)
        {
            if (this.ToggleButtonShowControlUI != null)
                this.ToggleButtonShowControlUI.IsChecked = isEnable;

            //제어버튼을 해제했을때 Button 자체가 사라지는 문제가 발생함. 그래서 주석처리함 !!
            //this.PTZVisibility = isEnable;
            var ptzParentGrid = this.Template.FindName("xPTZParentGrid", this) as Grid;
            if (ptzParentGrid != null)
            {
                if (isEnable && this.isFit)
                    ptzParentGrid.Visibility = System.Windows.Visibility.Visible;
                else
                    ptzParentGrid.Visibility = System.Windows.Visibility.Collapsed;
            }

            var ptzControl = this.GetChildPTZControl() ?? this.CreatePTZControl();

            if (this.GetChildPTZControl() != null)
            {
                ptzControl.ChangePTZControlVisible(isEnable);
                ptzControl.UpdatePtzControlSize();
            }

            if (!isEnable)
            {
                AudioManager.Instance.StopPlay();
                AudioManager.Instance.StopRecord();
            }
        }

        public void HandleMouseOver(bool isMouseOver)
        {
            if (this._toggleButtonShowControlUI == null)
                return;

            //PTZ 제어를 false로 한 경우 Button을 Collapsed 함
            if (!CommonsConfig.Instance.CameraPtzEnable)
            {
                this.ShowPTZControlButton(false);
                return;
            }

            // DeviceInfo의 ControlType이 PTZ가 아니면  토글버튼을 숨김.
            if (string.Compare(ControlType.ToUpper(), "PTZ") != 0)
            {
                this.ShowPTZControlButton(false);
                return;
            }

            //CameraControllerInfo.log 에 포함이 안된 Camera인 경우 Button을 Collapsed 함
            PTZInformation ptzInformation = null;
            if (CameraControlInfoExecution.Instance.CameraControlInfo != null)
                ptzInformation = CameraControlInfoExecution.Instance.CameraControlInfo.GetPtzInformation(this.ID);

            if (ptzInformation == null)
            {
                this.ShowPTZControlButton(false);
                return;
            }

            if (isMouseOver)
            {
                this.ShowPTZControlButton(true);
            }
            else
            {
                //제어중이 아닌 경우..
                if (!(bool)this._toggleButtonShowControlUI.IsChecked)
                {
                    this.ShowPTZControlButton(false);
                }
            }
        }

        // 삭제대기 - by shwlee.
        ///// <summary>
        ///// The set camera brush.
        ///// </summary>
        ///// <param name="brush">
        ///// The brush.
        ///// </param>
        //public void SetCameraBrush(Brush brush)
        //{
        //    this._isManualBrushSet = true;
        //    this.ImmediateDraw(brush);

        //    //this.sourceStatus = SourceStatus.Unknown;
        //    //if (CommonsConfig.Instance.IsShowLowHighBorder)
        //    //{
        //    //    this.ShowStatusLabel(SourceStatus.Unknown, string.Empty);
        //    //}
        //}

        /// <summary>
        /// Update View Area Override.
        /// 부모중에 Visible이 꺼져 있는 경우가 있으면 Camera의 Rect를 Empty로 설정함 !!.
        /// </summary>
        /// <param name="parentCanvasScreenRegion">
        /// Parent Canvas Screen Region.
        /// </param>
        /// <param name="parentsScreenRegion">
        /// All Parent Elements Screen Region.
        /// </param>
        public override void UpdateViewArea(Rect parentCanvasScreenRegion, List<Rect> parentsScreenRegion)
        {
            if (Public.GetProgramType() == ProgramType.iEditor)
                return;

            // 부모와 연결되어 있지 않으면 처리하지 않음.
            if (PresentationSource.FromVisual(this) == null)
            {
                return;
            }

            if (!this.IsVisible || this.Visibility != Visibility.Visible || this.Opacity == 0 || parentsScreenRegion == null ||
                parentsScreenRegion.Count == 0 || parentCanvasScreenRegion.IsEmpty)
            {
                this.SetViewPort(Rect.Empty, Rect.Empty);
                if (this.videoElement != null)
                {
                    // [elrha] 옵션 확인 후 Pause 혹은 Stop
                    InnerCheckStop();
                    if (this.LowVideoCameraBrush != null)
                    {
                        this._fadeOutVideoRect.Opacity = 1;

                        //Animation에 할당된 DP 기본값 초기화.
                        this._videoRect.BeginAnimation(OpacityProperty, null);
                        this._videoRect.Opacity = 0;
                    }
                }

                return;
            }

            // 1. 실제 화면상의 크기 적용
            Point elementTopLeft = this.PointToScreen(new Point(0, 0));
            Point elementBottomRight = this.PointToScreen(new Point(this.ActualWidth, this.ActualHeight));
            var elementRect = new Rect(elementTopLeft, elementBottomRight);

            // 2-1. Layout의 중첩구조를 모두 검사하여 실제 화면에 표출되고 있는 영역을 계산한다.
            Rect intersectRect = Rect.Intersect(parentCanvasScreenRegion, elementRect);

            foreach (var parentScreenRegion in parentsScreenRegion)
            {
                intersectRect = Rect.Intersect(intersectRect, parentScreenRegion);
                if (intersectRect.IsEmpty)
                {
                    break;
                }
            }

            if (intersectRect.IsEmpty)
            {
                this.SetViewPort(Rect.Empty, Rect.Empty);
                this._grid.Visibility = System.Windows.Visibility.Collapsed;
            }
            else
            {
                this._grid.Visibility = System.Windows.Visibility.Visible;
                Rect resultRect = Rect.Offset(intersectRect, -elementTopLeft.X, -elementTopLeft.Y);
                this.SetViewPort(resultRect, elementRect);
            }

            CameraManager.Instance.RedrawCameraControlAsync(this.IsInStage);
        }

        #endregion

        #region Methods

        #region VideoSurface Methods

        /// <summary>
        /// 저화질 영상을 가져와서 할당하고 VideoElement를 생성해 고화질 영상을 할당한다.
        /// </summary>
        /// <returns>The result bool.</returns>
        public bool InitializeVideoElement()
        {
            if (this._isAppliedTemplate == false) return false;

            try
            {
                if (CommonsConfig.Instance.MaxHighResSourceCount == 0)
                {
                    InnotiveDebug.Log.Error(string.Format("[Video Element Create Error] {0} ID : {1}", "MaxHighResSourceCount == 0", this.ID));
                    return false;
                }

                if (CameraManager.Instance.VideoInfo == null)
                {
                    InnotiveDebug.Log.Error(string.Format("[Video Element Create Error] {0} ID : {1}", "VideoInfo == null", this.ID));
                    return false;
                }

                // 화면 바깥 이미지 저화질 표출을 위해 순서를 바꾼다.
                this._videoRect.SetValue(Panel.ZIndexProperty, 2);
                this._fadeOutVideoRect.SetValue(Panel.ZIndexProperty, 1);

                if (!this.SetCameraInformation())
                {
                    this.HighMiddleCameraBrush = null;
                    this.videoElement = null;

                    InnotiveDebug.Log.Error(string.Format("[Video Element Create Error] {0} ID : {1}", "Camera Infomation setting failed.", this.ID));
                    return false;
                }

                // 최초 생성 시 저화질 연결한다.
                // MD에 등록되지 않아 LowVideoCameraBrush가 null로 초기화되면 
                // CameraControl의 크기와 관계없이 저화질 연결하지 않고 무조건 고화질로 연결한다. - by shwlee
                this.SetLowBrush();

                this.SetCameraResolutionLevel();

                // 저화질 개수를 0으로 설정 시 this.LowVideoCameraBrush = null임.
                // 저화질 브러쉬가 null 이면 연결 안 함.
                if (this.LowVideoCameraBrush != null)
                {
                    this.DirectSetBrush(this._fadeOutVideoRect, this.LowVideoCameraBrush, "Low");
                }

                if (this.resolutions == null)
                {
                    InnotiveDebug.Log.Error(string.Format("[Video Element Create Error] {0} ID : {1}", "Camera Resolution == null", this.ID));
                    return false;
                }

                // HighMiddleBrush 연결.
                // VideoSurfaceId 가 없으면 MainVideoSurface. 있으면 LayoutSurface. 
                var videoSurface = string.IsNullOrWhiteSpace(this.VideoSurfaceId) ?
                    VideoSurfaceManager.Instance.HighMiddleSurface :
                    VideoSurfaceManager.Instance.LayoutSurfaceList.Find(surface => string.Compare(surface.ID, this.VideoSurfaceId) == 0);
                if (videoSurface == null)
                {
                    InnotiveDebug.Log.Error(string.Format("[Video Element Create Error] {0} ID : {1}", "Created VideoSurface == null", this.ID));
                    return false;
                }

                this.ownerVideoSurface = videoSurface;
                this.videoElement = this.CreateVideoElement(this.resolutions, this.ownerVideoSurface, this.cameraInfo.ImageClippingAreaThickness);
                this.videoElement.OnChangedStatus += videoElement_OnChangedStatus;

                this.HighMiddleCameraBrush = this.ownerVideoSurface.GetTileBrush();
            }
            catch (Exception ex)
            {
                InnotiveDebug.Log.Error(string.Format("[Video Element Create Error] {0} ID : {1}", ex.Message, this.ID));
                return false;
            }

            return true;
        }

        /// <summary>
        /// VideoSurface로 부터 VideoElement를 생성해서 반환한다.
        /// </summary>
        /// <param name="dictionary">다단계 영상 요청에 필요한 영상 소스 주소 dictionary.</param>
        /// <param name="videoSurface">요청할 VideoSurface.</param>
        /// <param name="thickness">특정 영역을 작를 ClippingArea.</param>
        /// <returns></returns>
        private CInnoVideoElement CreateVideoElement(Dictionary<string, string> dictionary, VideoSurface videoSurface, Thickness thickness)
        {
            var connectInfo = new ConnectInfo
            {
                sConnectId = this.cameraInfo.ConnectId,
                sConnectPassword = this.cameraInfo.ConnectPassword,
                sConnectMode = this.cameraInfo.ConnectMode,
                sID = this.cameraInfo.CameraId,
                sUseAudio = cameraInfo.ShowAudioLevel.ToString()
            };

            InnotiveDebug.Trace(3, "[shwlee] Create VideoElement ID : {0}", this.ID);
            return videoSurface.CreateVideoElement(dictionary, connectInfo, thickness);
        }

        private void SetCameraResolutionLevel()
        {
            if (CameraManager.Instance.VideoInfo == null)
                return;

            //if (this.resolutions != null)
            //    return;

            this.resolutions =
                VideoInformation.GetResolutionDictionary(CameraManager.Instance.VideoInfo, this.ID);
        }

        private void SetLowBrush()
        {
            this.LowVideoCameraBrush = null;

            if (VideoSurfaceManager.Instance.MergerVideoSurfaceList == null)
                return;

            if (CameraManager.Instance.VideoInfo == null)
                return;

            if (this.cameraInfo == null)
                return;

            // 자신이 포함되어있는 Merging 영상을 찾아 Brush 영역을 가져온다.
            var containMerger = VideoInformation.GetMediaServerInfo(CameraManager.Instance.VideoInfo, this.cameraInfo.MediaServerId);

            // MergerImageInfo와 일치하는 MergingImageSurface를 찾는다.
            var videoSurfaces = VideoSurfaceManager.Instance.MergerVideoSurfaceList;

            // null Check.
            if (containMerger == null || videoSurfaces.Count == 0 || !this.SetCameraInformation())
                return;

            var mergerSurface = videoSurfaces.Find(surface => string.Compare(surface.Id, containMerger.MediaServerId) == 0);
            if (mergerSurface == null)
                return;

            var isMD200 = !(string.Compare(containMerger.Width, string.Empty, StringComparison.OrdinalIgnoreCase) == 0 ||
                            string.Compare(containMerger.Width, "0", StringComparison.OrdinalIgnoreCase) == 0 ||
                            string.Compare(containMerger.Height, string.Empty, StringComparison.OrdinalIgnoreCase) == 0 ||
                            string.Compare(containMerger.Height, "0", StringComparison.OrdinalIgnoreCase) == 0);

            if (!isMD200)
                return;

            var width = Convert.ToInt32(containMerger.Width) / Convert.ToInt32(containMerger.ColumnCount);
            var height = Convert.ToInt32(containMerger.Height) / Convert.ToInt32(containMerger.RowCount);
            var left = width * Convert.ToInt32(this.cameraInfo.Column);
            var top = height * Convert.ToInt32(this.cameraInfo.Row);

            var currentPosition = new Rect(left, top, width, height);

            // Brush Cropping.
            if (CommonsConfig.Instance.UseDeviceInfoMergerClippingArea)
            {
                if (!(this.cameraInfo.ImageClippingAreaThickness.Left == 0 &&
                    this.cameraInfo.ImageClippingAreaThickness.Top == 0 &&
                    this.cameraInfo.ImageClippingAreaThickness.Right == 0 &&
                    this.cameraInfo.ImageClippingAreaThickness.Bottom == 0))
                {
                    currentPosition =
                        this.GetMergerImageCroppedRect(currentPosition, this.cameraInfo.ImageClippingAreaThickness);
                }
            }
            else
            {
                if (!(CommonsConfig.Instance.MergerClippingArea.Left == 0 &&
                  CommonsConfig.Instance.MergerClippingArea.Top == 0 &&
                  CommonsConfig.Instance.MergerClippingArea.Right == 0 &&
                  CommonsConfig.Instance.MergerClippingArea.Bottom == 0))
                {
                    currentPosition =
                       this.GetMergerImageCroppedRect(currentPosition, CommonsConfig.Instance.MergerClippingArea);
                }
            }

            // 해당 Surface에서 TileBrush를 가져와서 지정된 영역만큼 잘라낸다.
            var wholeBrush = mergerSurface.TileBrush;
            wholeBrush.ViewboxUnits = BrushMappingMode.Absolute;
            wholeBrush.Stretch = Stretch.Fill;
            wholeBrush.Viewbox = currentPosition;

            this.LowVideoCameraBrush = wholeBrush;
        }

        private bool SetCameraInformation()
        {
            if (CameraManager.Instance.VideoInfo == null)
                return false;

            //if (this.cameraInfo == null)
            {
                this.cameraInfo = VideoInformation.GetCameraInfo(CameraManager.Instance.VideoInfo, this.ID);
                if (cameraInfo == null)
                    return false;

                var mediaServer =
                    VideoInformation.GetMediaServerInfo(CameraManager.Instance.VideoInfo, this.cameraInfo.MediaServerId);

                if (mediaServer != null)
                {
                    // MD200 이면 mediaServerIp를 주지 않는다.
                    var isMd300 = (string.IsNullOrWhiteSpace(mediaServer.Width) ||
                                   string.CompareOrdinal(mediaServer.Width, "0") == 0 ||
                                   string.IsNullOrWhiteSpace(mediaServer.Height) ||
                                   string.CompareOrdinal(mediaServer.Height, "0") == 0);

                    this.mediaServerIp = isMd300 ? mediaServer.MediaServerIp : string.Empty;
                }
            }

            return this.cameraInfo != null;
        }

        private void DirectSetBrush(Rectangle rectangle, Brush brush, string level)
        {
            rectangle.Fill = brush;

            if (CommonsConfig.Instance.IsShowLowHighBorder)
                this.ShowStatusLabel(level, string.Empty);
        }

        private void ReadyToStillBrush(bool isInStage)
        {
            //this.BufferBrush = null;

            //if (!isInStage)
            //{
            //    this.BufferBrush = this.ownerVideoSurface.GetStillBrush();
            //    this.SetBrushViewbox(this.BufferBrush, this.oldFullRect);
            //}
            //this.oldFullRect = this.fullRect;
        }

        private void SetBrushViewbox(Brush brush, Rect rect)
        {
            var targetBrush = brush as TileBrush;
            if (targetBrush == null)
                return;

            targetBrush.ViewboxUnits = BrushMappingMode.Absolute;
            targetBrush.Stretch = Stretch.Fill;
            targetBrush.Viewbox = rect;
        }

        private void VideoElementSetDisplayRect(object requestCamera)
        {
            if (this.videoElement == null)
            {
                return;
            }

            var param = requestCamera as RequestCamera;
            if (param == null)
                return;

            //this.TraceToCorrectRegion(param.CurrentRegion, param.CameraId);

            var rect = param.CurrentRegion;

            this.videoElement.SetDisplayRect((int)rect.X, (int)rect.Y, (int)rect.Width, (int)rect.Height, 0);
            this.Dispatcher.BeginInvoke(new Action(() =>
            {
                InnotiveDebug.Trace(1, "[shwlee] SetDisplayRect : {0} ({1})", rect, this.ID);
            }));
        }

        [Conditional("DEBUG")]
        private void TraceToCorrectRegion(Rect region, string id)
        {
            var monitor = InnoMonitorUtil.GetMonitorRect(0);
            var intersect = Rect.Intersect(region, monitor);
            if (!intersect.IsEmpty)
                return;

            // 너무 많은 디버그 정보
            var info = string.Format("[shwlee] Screen Out RequestCamera ID : {0}, Rect : {1}", id, region);
            Debug.WriteLine(info);
            //InnotiveDebug.Log.Debug(info);
        }

        private void SetCurrentCameraBrush(bool useSmoothDraw, string status)
        {

            if (CommonsConfig.Instance.IsShowLowHighBorder)
                this.ShowStatusLabel(status, string.Empty);

            if (string.Compare(status, "Error") == 0 || string.Compare(status, "Fail") == 0)
                return;

            // 같은 영상 사이즈 변경시 까만 화면 나오는 현상 방지 처리 - 명길
            if (this.CurrentCameraBrush != null &&
                (this.CurrentCameraBrush == this.HighMiddleCameraBrush))
            {
                this.CurrentCameraBrush = this.HighMiddleCameraBrush;
                this.SetBrushViewbox(this.HighMiddleCameraBrush, this.fullRect);

                return;
            }

            if (useSmoothDraw)
            {
                this.GoDrawAnimation(this.HighMiddleCameraBrush);
            }
            else
            {
                this.SetBrushViewbox(this.HighMiddleCameraBrush, this.fullRect);
                //if (this.CurrentCameraBrush == null)
                this.CurrentCameraBrush = this.HighMiddleCameraBrush;
            }

            //this.oldLevel = status;
        }

        private void GoDrawAnimation(Brush targetBrush)
        {
            //Animation에 할당된 DP 기본값 초기화.
            this._videoRect.BeginAnimation(OpacityProperty, null);

            var duration = CommonsConfig.Instance.CameraSourceSwitchingAnimationSpeed;
            if (duration <= 0)
            {
                return;
            }

            NameScope.SetNameScope(this._videoRect, new NameScope());
            this._videoRect.RegisterName("FadeInAnimation", this._videoRect);

            var opacityKeyFrame = new DoubleAnimationUsingKeyFrames();

            var startKeyFrame = new SplineDoubleKeyFrame();
            startKeyFrame.KeyTime = KeyTime.FromTimeSpan(TimeSpan.FromSeconds(0));
            startKeyFrame.Value = 0.0;

            opacityKeyFrame.KeyFrames.Add(startKeyFrame);

            var middleKeyFrame = new SplineDoubleKeyFrame();
            middleKeyFrame.KeyTime = KeyTime.FromTimeSpan(TimeSpan.FromSeconds(duration * 0.7));
            middleKeyFrame.Value = 0.0;

            opacityKeyFrame.KeyFrames.Add(middleKeyFrame);

            var endKeyFrame = new SplineDoubleKeyFrame();
            endKeyFrame.KeyTime = KeyTime.FromTimeSpan(TimeSpan.FromSeconds(duration));
            endKeyFrame.Value = 1.0;

            opacityKeyFrame.KeyFrames.Add(endKeyFrame);

            Storyboard.SetTargetName(opacityKeyFrame, "FadeInAnimation");
            Storyboard.SetTargetProperty(opacityKeyFrame, new PropertyPath(OpacityProperty));

            Storyboard.SetTargetName(opacityKeyFrame, "FadeInAnimation");
            Storyboard.SetTargetProperty(opacityKeyFrame, new PropertyPath(OpacityProperty));

            this._storyBoardFadeOut.Children.Clear();
            opacityKeyFrame.Freeze();
            this._storyBoardFadeOut.Completed += this.VideoElement_storyBoardFadeOut_Completed;
            this._storyBoardFadeOut.Children.Add(opacityKeyFrame);
            this._storyBoardFadeOut.FillBehavior = FillBehavior.HoldEnd;

            this.SetBrushViewbox(targetBrush, this.fullRect);

            //this.oldFullRect = this.fullRect;
            this.CurrentCameraBrush = targetBrush;
            this._storyBoardFadeOut.Begin(this._videoRect, true);
        }

        private Rect GetMergerImageCroppedRect(Rect rect, Thickness clippingArea)
        {
            rect.X += rect.Width * clippingArea.Left / 100;
            rect.Y += rect.Height * clippingArea.Top / 100;
            rect.Width -= rect.Width * (clippingArea.Left + clippingArea.Right) / 100;
            rect.Height -= rect.Height * (clippingArea.Top + clippingArea.Bottom) / 100;

            return rect;
        }

        #endregion

        /// <summary>
        /// The on changed ptz visibilty.
        /// </summary>
        /// <param name="o">
        /// The o.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected static void OnChangedPtzVisibilty(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            if ((bool)e.NewValue)
            {
                return;
            }

            ((CameraControl)o).TurnOffPTZState();
        }

        /// <summary>
        /// The do dispose.
        /// </summary>
        /// <param name="isManage">
        /// The is manage.
        /// </param>
        protected override void DoDispose(bool isManage)
        {
            InnotiveDebug.Trace(2, "[blackRoot19] CameraControl.DoDispose() 시작 !! Guid = {0}", this.OriginGUID);

            //Camera 제어 중인 경우 제어 해제를 함
            if (this._toggleButtonShowControlUI != null && (bool)this._toggleButtonShowControlUI.IsChecked)
            {
                PTZManager.Instance.ShowCameraControllerUI(this, this.ID, false);
                this.IsEnableToggleButtonShowControlUI = false;
            }

            if (CameraManager.CameraControls.ContainsKey(this.OriginGUID))
            {
                InnotiveDebug.Trace(1, "[blackRoot19] CameraControl.DoDispose() List에서 Camera 삭제 !! Guid = {0}", this.OriginGUID);
                CameraManager.CameraControls.Remove(this.OriginGUID);
            }

            Guid originGuid = Guid.Empty;
            this.Dispatcher.Invoke(
                new Action(
                    () =>
                    {
                        originGuid = this.OriginGUID;
                        this.DestoryPTZControl();
                    }));

            CameraManager.Instance.UnregisterCameraDisplayInfo(originGuid);

            if (this._grid != null)
            {
                this._grid.SizeChanged -= this.CameraControl_SizeChanged;
            }

            if (this._toggleButtonShowControlUI != null)
            {
                this._toggleButtonShowControlUI.Click -= _toggleButtonShowControlUI_Click;
                this._toggleButtonShowControlUI.StylusUp -= this._toggleButtonShowControlUI_StylusUp;
                this._toggleButtonShowControlUI.MouseEnter -= this._toggleButtonShowControlUI_MouseEnter;
                this._toggleButtonShowControlUI.Checked -= this._toggleButtonShowControlUI_Checked;
                this._toggleButtonShowControlUI.Unchecked -= _toggleButtonShowControlUI_Unchecked;
            }

            if (this.videoElement != null)
            {
                this.videoElement.OnChangedStatus -= videoElement_OnChangedStatus;
                this.videoElement.Dispose();
                this.videoElement = null;
            }

            this.ownerVideoSurface = null;
            this.parent = null;


            base.DoDispose(isManage);

            InnotiveDebug.Trace(2, "[blackRoot19] CameraControl.DoDispose() 종료 !! Guid = {0}", this.OriginGUID);
        }

        private static void OnChangedPTZEnabled(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
        }

        private static void OnChangedID(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            var cameraControl = (CameraControl)o;
            cameraControl.InitializeVideoElement();
            cameraControl.ControlType = VideoInformation.GetControlType(CameraManager.Instance.VideoInfo, cameraControl.ID);

            if (string.Compare(cameraControl.ControlType.ToUpper(), "RDS", StringComparison.OrdinalIgnoreCase) == 0)
            {
                
            }

            if (string.Compare(cameraControl.ControlType.ToUpper(), "PTZ", StringComparison.OrdinalIgnoreCase) != 0)
            {
                return;
            }

            var ptzControl = cameraControl.GetChildPTZControl();
            if (ptzControl != null)
            {
                ptzControl.ParentCamreaID = (string)e.NewValue;
            }
        }

        private static void Set_PTZControl_Binding(
            FrameworkElement sourceElement,
            FrameworkElement targetElement,
            DependencyProperty sourceProperty,
            DependencyProperty targetProperty,
            IValueConverter converter,
            object converterParameter)
        {
            var binding = new Binding();
            binding.Source = sourceElement;
            binding.Mode = BindingMode.TwoWay;
            binding.NotifyOnSourceUpdated = true;
            binding.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;
            binding.Path = new PropertyPath(sourceProperty);
            if (converter != null)
            {
                binding.Converter = converter;
                binding.ConverterParameter = converterParameter;
            }

            targetElement.SetBinding(targetProperty, binding);
        }



        /// <summary>
        /// The set view port.
        /// </summary>
        /// <param name="rect">
        /// The Rect.
        /// </param>
        /// <param name="screenRegion">
        /// The Screen Rect.
        /// </param>
        private void SetViewPort(Rect rect, Rect screenRegion)
        {
            if (this.IsDisposed)
            {
                InnotiveDebug.Trace(1, "Camera id = {0}, Register를 하려고 했지만 이미 Dispose 상태임 !!", this.ID);
                return;
            }

            this.fullRect = screenRegion;

            // margin 적용 : PointToScrren 사용으로 성능에 영향을 미칠 수 있음. - Alvin 2012.06.25
            if (this.BorderMargin.Left > 0 || this.BorderMargin.Top > 0 || this.BorderMargin.Right > 0 || this.BorderMargin.Bottom > 0)
            {
                var lt = this.PointToScreen(new Point(this.BorderMargin.Left, this.BorderMargin.Top));
                var rb = this.PointToScreen(new Point(this.ActualWidth - this.BorderMargin.Right, this.ActualHeight - this.BorderMargin.Bottom));
                this.fullRect = new Rect(lt, rb);
            }

            // 모니터 위치에 따른 좌표 보정 (0 ,0 기준으로...)
            if (this.fullRect != Rect.Empty)
            {
                this.fullRect.X = this.fullRect.X - CameraManager.Instance.CurrentWindowRegion.X;
                this.fullRect.Y = this.fullRect.Y - CameraManager.Instance.CurrentWindowRegion.Y;
            }

            //this.TraceToCorrectRegion(this.fullRect, this.ID);

            if (rect.IsEmpty || Double.IsNaN(rect.Width) || Double.IsNaN(rect.Height) || rect.Height <= 0 ||
                    rect.Width <= 0 || this.Visibility != Visibility.Visible)
            {
                this.viewPortRect = Rect.Empty;
                CameraManager.Instance.RegisterCameraDisplayInfo(this.OriginGUID, Rect.Empty, fullRect, this.ID, this.AlwaysHighRes);
            }
            else
            {
                if (this.PlayOnControlMode && !this.isControlMode)
                {
                    this.viewPortRect = Rect.Empty;
                    CameraManager.Instance.RegisterCameraDisplayInfo(this.OriginGUID, Rect.Empty, this.fullRect, this.ID, this.AlwaysHighRes);
                }
                else
                {
                    var finalRect = new Rect(this.fullRect.TopLeft.X + rect.X, this.fullRect.TopLeft.Y + rect.Y, rect.Width, rect.Height);

                    this.viewPortRect = finalRect;
                    CameraManager.Instance.RegisterCameraDisplayInfo(this.OriginGUID, finalRect, this.fullRect, this.ID, this.AlwaysHighRes);
                }
            }

            // PTZ 버튼 크기 업데이트
            this.ChangeToggleButtonShowControlUISize(screenRegion);
        }

        /// <summary>
        /// The set view port.
        /// </summary>
        /// <param name="rect">
        /// The Rect.
        /// </param>
        private void SetViewPort(Rect rect)
        {
            if (PresentationSource.FromVisual(this) != null)
            {
                Point camTopLeft = this.PointToScreen(new Point(0, 0));

                this.SetViewPort(rect, new Rect(camTopLeft, camTopLeft));
            }
        }

        /// <summary>
        /// 카메라 화면이 LowRes에서 HighRes로 전환될때 화면상에서 느껴지는 위화감을 최소화하기 위해 에니메이션효과를 줌.
        /// </summary>
        private void BeginFadeOutEffect()
        {
            if (!this._isAppliedTemplate || this.CurrentCameraBrush == null)
            {
                return;
            }

            double duration = CommonsConfig.Instance.CameraSourceSwitchingAnimationSpeed;
            if (duration <= 0)
            {
                return;
            }

            Brush br = this._videoRect.Fill;
            if (br == null)
            {
                return;
            }

            this._fadeOutVideoRect.Fill = br;
            this._fadeOutVideoRect.Opacity = 1.0;
            this._fadeOutVideoRect.Visibility = Visibility.Visible;

            NameScope.SetNameScope(this._fadeOutVideoRect, new NameScope());
            this._fadeOutVideoRect.RegisterName("FadeOutAnimation", this._fadeOutVideoRect);

            var opacityKeyFrame = new DoubleAnimationUsingKeyFrames();

            var startKeyFrame = new SplineDoubleKeyFrame();
            startKeyFrame.KeyTime = KeyTime.FromTimeSpan(TimeSpan.FromSeconds(0));
            startKeyFrame.Value = 1.0;

            opacityKeyFrame.KeyFrames.Add(startKeyFrame);

            var endKeyFrame = new SplineDoubleKeyFrame();
            endKeyFrame.KeyTime = KeyTime.FromTimeSpan(TimeSpan.FromSeconds(duration));
            endKeyFrame.Value = 0.0;

            opacityKeyFrame.KeyFrames.Add(endKeyFrame);

            Storyboard.SetTargetName(opacityKeyFrame, "FadeOutAnimation");
            Storyboard.SetTargetProperty(opacityKeyFrame, new PropertyPath(OpacityProperty));

            this._storyBoardFadeOut.Children.Clear();
            opacityKeyFrame.Freeze();
            this._storyBoardFadeOut.Completed += this._storyBoardFadeOut_Completed;
            this._storyBoardFadeOut.Children.Add(opacityKeyFrame);
            this._storyBoardFadeOut.FillBehavior = FillBehavior.Stop;
            this._storyBoardFadeOut.Begin(this._fadeOutVideoRect, true);
        }

        /// <summary>
        /// 컨트롤 크기가 변경되었을때 제어 버튼 크기를 조정함.
        /// </summary>
        public void ChangeToggleButtonShowControlUISize(Rect screenSize)
        {
            if (this._toggleButtonShowControlUI == null)
                return;

            // 부모와 연결되어 있지 않으면 처리하지 않음.
            if (PresentationSource.FromVisual(this._grid) == null)
            {
                return;
            }

            var control_width = this._grid.ActualWidth;
            var control_height = this._grid.ActualHeight;

            //Point control_screen_topleft = this._grid.PointToScreen(new Point(0.0, 0.0));
            //Point control_screen_bottomright = this._grid.PointToScreen(new Point(control_width, control_height));

            var control_screen_width = screenSize == Rect.Empty ? 0 : screenSize.Width; //control_screen_bottomright.X - control_screen_topleft.X;
            var control_screen_height = screenSize == Rect.Empty ? 0 : screenSize.Height; //control_screen_bottomright.Y - control_screen_topleft.Y;

            double scaleX = control_screen_width / control_width;
            double scaleY = control_screen_height / control_height;

            if (scaleX <= 0.0) scaleX = 1.0;
            if (scaleY <= 0.0) scaleY = 1.0;

            double targetWidth = control_screen_width; // control_width;
            double targetHeight = control_screen_height; // control_height;

            // 카메라 선택 버튼 크기 수정 (by jhlee)
            if (targetWidth >= 320 && targetHeight >= 320)
            {
                this._viewBoxToggleButtonShowControlUIForCamera.Width = 32 / scaleX;
                this._viewBoxToggleButtonShowControlUIForCamera.Height = 32 / scaleY;
            }
            else if (targetWidth < 320 && targetHeight >= 320)
            {
                this._viewBoxToggleButtonShowControlUIForCamera.Width = targetWidth / 10 / scaleX;
                this._viewBoxToggleButtonShowControlUIForCamera.Height = targetWidth / 10 / scaleY;
            }
            else if (targetWidth >= 320 && targetHeight < 320)
            {
                this._viewBoxToggleButtonShowControlUIForCamera.Width = targetHeight / 10 / scaleX;
                this._viewBoxToggleButtonShowControlUIForCamera.Height = targetHeight / 10 / scaleY;
            }
            else if (targetWidth < 320 && targetHeight < 320)
            {
                if (targetHeight < targetWidth)
                {
                    this._viewBoxToggleButtonShowControlUIForCamera.Width = targetHeight / 10 / scaleX;
                    this._viewBoxToggleButtonShowControlUIForCamera.Height = targetHeight / 10 / scaleY;
                }
                else if (targetHeight >= targetWidth)
                {
                    this._viewBoxToggleButtonShowControlUIForCamera.Width = targetWidth / 10 / scaleX;
                    this._viewBoxToggleButtonShowControlUIForCamera.Height = targetWidth / 10 / scaleY;
                }
            }
        }

        private void CameraControl_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            if (!this._isAppliedTemplate)
            {
                return;
            }

            if (CommonsConfig.Instance.CameraLabelHeight > 0 &&
                CommonsConfig.Instance.CameraLabelHeight < 100)
            {
                // Text의 FontSize 값은 0 과 35791.39407 사이의 값이어야 한다. 
                double fontSize = this.ActualHeight * CommonsConfig.Instance.CameraLabelHeight / 100.0;

                if (fontSize >= 35791)
                {
                    fontSize = 35791;
                }
                else if (fontSize <= 0.1)
                {
                    fontSize = 0.1;
                }

                this._textBlock.FontSize = fontSize;
                this._textBlock.Height = fontSize + fontSize * 0.3;

                this._textBlockLeftShadow.FontSize = fontSize;
                this._textBlockLeftShadow.Height = fontSize + fontSize * 0.3;

                this._textBlockRightShadow.FontSize = fontSize;
                this._textBlockRightShadow.Height = fontSize + fontSize * 0.3;

                this._textBlockTopShadow.FontSize = fontSize;
                this._textBlockTopShadow.Height = fontSize + fontSize * 0.3;

                this._textBlockBottomShadow.FontSize = fontSize;
                this._textBlockBottomShadow.Height = fontSize + fontSize * 0.3;

                this.TextFourWayShadowDistance = this.ActualHeight * (CommonsConfig.Instance.CameraLabelFourWayShadowDistance / 100);
            }
        }

        protected override void OnPropertyChanged(DependencyPropertyChangedEventArgs e)
        {
            base.OnPropertyChanged(e);

            //Debug.WriteLine(string.Format("(shwlee) Camera Property Changed : {0}, {1}", e.Property.Name, e.NewValue));

            if (this.IsCameraChanged(e))
            {
                // 부모와 연결되어 있지 않으면 처리하지 않음.
                if (PresentationSource.FromVisual(this) == null)
                {
                    return;
                }

                // MapControl이 ZoomPanning 하면 CameraControl의 Left, Top, Width, Height 역시 실시간으로 변경됨.
                // MapControl 조작 시 AsyncWorker로 Update 요청 하지 않게 변경함.(요청 시 두번 씩 Update 발생함.) - by shwlee.
                if (this.parent == null || this.parent is MapControl)
                    return;

                if (this.Visibility == Visibility.Visible && IsVisible)
                {
                    // 슬라이드 확대 후 카메라 컨트롤의 실제 크기 반영 안되는 문제 때문에 수정 - by Alvin
                    var newRect = new Rect(this.PointToScreen(new Point(0, 0)), this.PointToScreen(new Point(this.ActualWidth, ActualHeight)));
                    this.SetViewPort(new Rect(0, 0, newRect.Width, newRect.Height), newRect);
                }
                else
                {
                    this.SetViewPort(Rect.Empty);
                }

                if (this._isAppliedTemplate)
                {
                    //if(this.IsAdded)
                    //{
                    //    if (this.parent is MultiGridControl)
                    //    {
                    //        var multiGridControl = this.parent as MultiGridControl;
                    //        multiGridControl.MustUpdateCameraGuids.Add(this.OriginGUID.ToString());
                    //        this.IsAdded = false;
                    //    }    
                    //}

                    //Debug.WriteLine(string.Format("(shwlee) Camera OnPropertyChanged : {0}", this.parent.OriginGUID));
                    AsyncWorker.Instance.UpdateParent(this.parent, new Action(() => this.RequestUpdateToParent(parent)));
                }

                // UpdateViewArea 없이 Redraw 할 경우 잘못된 좌표 전달됨. - by shwlee.
                //CameraManager.Instance.RedrawCameraControlAsync(this.IsInStage);
            }
        }

        private BaseControl GetCurrentParent(CameraControl camera)
        {
            DependencyObject currentParent = camera;
            while ((currentParent != null))
            {
                currentParent = VisualTreeHelper.GetParent(currentParent);
                if (currentParent is BaseLayoutControl)
                {
                    if (currentParent is MapControl)
                    {
                        return currentParent as MapControl;
                    }

                    return currentParent as LayoutControl;
                }
            }

            return null;
        }

        private void RequestUpdateToParent(BaseControl control)
        {
            if (control is LayoutControl)
            {
                var layoutControl = control as LayoutControl;
                layoutControl.InsideCanvas.UpdateViewArea();
                return;
            }
        }

        private bool IsCameraChanged(DependencyPropertyChangedEventArgs e)
        {
            if (this.isEditingType)
            {
                return false;
            }

            if (string.Compare(e.Property.Name, "CurrentCameraBrush", StringComparison.OrdinalIgnoreCase) == 0)
            {
                return false;
            }

            var property = string.Compare(e.Property.Name, "Left", true) == 0 ||
                   string.Compare(e.Property.Name, "Top", true) == 0 ||
                   string.Compare(e.Property.Name, "ActualWidth", true) == 0 ||
                   string.Compare(e.Property.Name, "ActualHeight", true) == 0 ||
                   string.Compare(e.Property.Name, "ID", true) == 0 ||
                   string.Compare(e.Property.Name, "Visibility", true) == 0 ||
                   string.Compare(e.Property.Name, "IsVisible", true) == 0;

            return property;
        }

        private void EndFadeOutEffect()
        {
            if (!this._isAppliedTemplate)
            {
                return;
            }

            this._fadeOutVideoRect.Visibility = Visibility.Hidden;

            this._storyBoardFadeOut.Completed -= this._storyBoardFadeOut_Completed;
            this._storyBoardFadeOut.Children.Clear();
        }

        private void ptzControlBinding(PTZControl ptzControl)
        {
            Set_PTZControl_Binding(
                this,
                ptzControl.PanTiltButton,
                PanTiltControlableProperty,
                VisibilityProperty,
                new BooleanVisibilityConverter(),
                null);

            Set_PTZControl_Binding(
                this,
                ptzControl.FocusButton,
                FocusControlableProperty,
                VisibilityProperty,
                new BooleanVisibilityConverter(),
                null);

            Set_PTZControl_Binding(
                this,
                ptzControl.ZoomButton,
                ZoomControlableProperty,
                VisibilityProperty,
                new BooleanVisibilityConverter(),
                null);
        }

        private void PtzControlClearBinding()
        {
            BindingOperations.ClearAllBindings(this);
        }

        private void ShowStatusLabel(string status, string message)
        {
            if (!this._isAppliedTemplate)
            {
                return;
            }

            if (this._debugLowHighBorderTextBlock == null)
            {
                this._debugLowHighBorderTextBlock = new TextBlock
                {
                    TextWrapping = TextWrapping.Wrap,
                    Foreground = Brushes.White,
                    HorizontalAlignment = HorizontalAlignment.Left,
                    VerticalAlignment = VerticalAlignment.Bottom,
                    FontSize = 1,
                    Visibility = Visibility.Hidden
                };
                Panel.SetZIndex(this._debugLowHighBorderTextBlock, 4);
                this._grid.Children.Add(this._debugLowHighBorderTextBlock);
            }

            if (double.IsNaN(this.ActualHeight) || this.ActualHeight < 10.0)
            {
                this._debugLowHighBorderTextBlock.FontSize = 2;
            }
            else
            {
                double fontSize = this.ActualWidth / 17;
                if (fontSize >= 35791)
                {
                    fontSize = 35791;
                }
                else if (fontSize <= 0)
                {
                    fontSize = 0.1;
                }

                this._debugLowHighBorderTextBlock.FontSize = fontSize;
            }

            this._debugLowHighBorderTextBlock.FontWeight = FontWeights.Bold;

            var str = string.Empty;
            Brush strokeBrush;

            var info = status.Split(new[] { '/' }, StringSplitOptions.None);


            if (info.Length > 1)
            {
                var origin = info[0];
                var resize = string.CompareOrdinal(info[1], "0x0") == 0 ? "Bypass" : "POD";//info[1];


                strokeBrush = Brushes.Navy;

                this.NonSelectBorderBrush = strokeBrush;
                InnotiveDebug.Trace(2, "[shwlee] SetBorder Brush1 {0} : {1}", this.ID, strokeBrush.ToString());
                this.NonSelectBorderThickness = this.ActualWidth / 80;
                str = string.Format("Size:{0}{1}{2}", origin, Environment.NewLine, resize);
            }
            else
            {
                switch (status)
                {
                    case "Waiting":
                        str = "N";
                        strokeBrush = Brushes.Pink;
                        break;
                    case "High":
                        str = "H";
                        strokeBrush = Brushes.Green;
                        break;
                    case "Middle":
                        str = "M";
                        strokeBrush = Brushes.Purple;
                        break;
                    case "Low":
                        str = "L";
                        strokeBrush = Brushes.Blue;
                        break;
                    case "Fail":
                        str = "F";
                        strokeBrush = Brushes.Brown;
                        break;

                    case "Error":
                        str = "E";
                        strokeBrush = Brushes.Red;
                        break;
                    default:
                        str = "U";
                        strokeBrush = Brushes.Gray;
                        break;
                }

                this.NonSelectBorderBrush = strokeBrush;
                InnotiveDebug.Trace(2, "[shwlee] SetBorder Brush2 {0}, {1}", this.ID, strokeBrush.ToString());
                this.NonSelectBorderThickness = this.ActualWidth / 80;
            }

            this._debugLowHighBorderTextBlock.Text = str;
            this._debugLowHighBorderTextBlock.ToolTip = string.IsNullOrEmpty(message) ? null : message;
            this._debugLowHighBorderTextBlock.Background = strokeBrush;
            this._debugLowHighBorderTextBlock.Visibility = Visibility.Visible;
        }

        private void ImmediateDraw(Brush brush)
        {
            this.EndFadeOutEffect();
            this.CurrentCameraBrush = brush;
        }

        private void SmoothDraw(Brush brush)
        {
            this.EndFadeOutEffect();
            this.BeginFadeOutEffect();
            this.CurrentCameraBrush = brush;
        }

        //PTZControl에서 MouseDown시 무조건 Cell의 빨간색 테두리가 보여지게 함
        private void RaisePTZControlUIClickedEvent()
        {
            var tempEvent = this.ePTZControlUIClicked;
            if (tempEvent != null)
                tempEvent(this, new EventArgs());
        }

        private void TurnOffPTZState()
        {
            PTZControl ptzControl = this.GetChildPTZControl();
            if (ptzControl != null)
            {
                ptzControl.PTZState = PTZControl.PTZStates.None;
            }
        }

        //PTZControl에서 MouseDown시 무조건 Cell의 빨간색 테두리가 보여지게 함
        private void ptzControl_ePTZControlUIClicked(object sender, EventArgs e)
        {
            this.RaisePTZControlUIClickedEvent();
        }

        private void _toggleButtonShowControlUI_StylusUp(object sender, StylusEventArgs e)
        {
            (sender as ToggleButton).IsChecked = !(sender as ToggleButton).IsChecked;
            bool isChecked = (bool)(sender as ToggleButton).IsChecked;
            this.ToggleButtonClicked(isChecked);
        }

        private void _toggleButtonShowControlUI_Click(object sender, RoutedEventArgs e)
        {
            bool isChecked = (bool)(sender as ToggleButton).IsChecked;
            this.ToggleButtonClicked(isChecked);
        }

        private void _toggleButtonShowControlUI_Checked(object sender, RoutedEventArgs e)
        {
            ControlModeChangeForPlayOnControlMode(true);
        }

        private void _toggleButtonShowControlUI_Unchecked(object sender, RoutedEventArgs e)
        {
            ControlModeChangeForPlayOnControlMode(false);
        }

        private void _toggleButtonShowControlUI_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
        {
            if ((bool)this._toggleButtonShowControlUI.IsChecked)
            {
                this.GetChildPTZControl().ChangePTZControlVisible(true);
            }
        }

        private void SetCameraLabel()
        {
            // Camera Label Color Setting
            if (!string.IsNullOrEmpty(CommonsConfig.Instance.CameraLabelColor))
            {
                var converter = new BrushConverter();
                this.TextBrush = converter.ConvertFromString(CommonsConfig.Instance.CameraLabelColor) as SolidColorBrush;
            }

            // Camera Label Color Setting
            if (!string.IsNullOrEmpty(CommonsConfig.Instance.CameraLabelFourWayShadowColor))
            {
                var converter = new BrushConverter();
                this.TextFourWayShadowBrush = converter.ConvertFromString(CommonsConfig.Instance.CameraLabelFourWayShadowColor) as SolidColorBrush;
            }

            // Camera Label Font Family Setting
            if (!string.IsNullOrEmpty(CommonsConfig.Instance.CameraLabelFont))
            {
                this.TextFont = new FontFamily(CommonsConfig.Instance.CameraLabelFont);
            }

            // Camera Label 글꼴 스타일 설정
            switch (CommonsConfig.Instance.CameraLabelFontStyle)
            {
                case "Italic":
                    this.TextStyle = FontStyles.Italic;
                    break;
                case "Oblique":
                    this.TextStyle = FontStyles.Oblique;
                    break;
                default:
                    this.TextStyle = FontStyles.Normal;
                    break;
            }

            // Camera Label 글꼴 무게 설정
            switch (CommonsConfig.Instance.CameraLabelFontWeight)
            {
                case "Black":
                    this.TextWeight = FontWeights.Black;
                    break;
                case "Bold":
                    this.TextWeight = FontWeights.Bold;
                    break;
                case "DemiBold":
                    this.TextWeight = FontWeights.DemiBold;
                    break;
                case "ExtraBlack":
                    this.TextWeight = FontWeights.ExtraBlack;
                    break;
                case "ExtraBold":
                    this.TextWeight = FontWeights.ExtraBold;
                    break;
                case "ExtraLight":
                    this.TextWeight = FontWeights.ExtraLight;
                    break;
                case "Heavy":
                    this.TextWeight = FontWeights.Heavy;
                    break;
                case "Light":
                    this.TextWeight = FontWeights.Light;
                    break;
                case "Medium":
                    this.TextWeight = FontWeights.Medium;
                    break;
                case "Regular":
                    this.TextWeight = FontWeights.Regular;
                    break;
                case "SemiBold":
                    this.TextWeight = FontWeights.SemiBold;
                    break;
                case "Thin":
                    this.TextWeight = FontWeights.Thin;
                    break;
                case "UltraBlack":
                    this.TextWeight = FontWeights.UltraBlack;
                    break;
                case "UltraBold":
                    this.TextWeight = FontWeights.UltraBold;
                    break;
                case "UltraLight":
                    this.TextWeight = FontWeights.UltraLight;
                    break;
                default:
                    this.TextWeight = FontWeights.Normal;
                    break;
            }

            // Camera Label 정렬 설정
            switch (CommonsConfig.Instance.CameraLabelAlignment)
            {
                case "TopLeft":
                    this.HorizontalAlign = TextAlignment.Left;
                    this.VerticalAlign = VerticalAlignment.Top;
                    break;
                case "BottomLeft":
                    this.HorizontalAlign = TextAlignment.Left;
                    this.VerticalAlign = VerticalAlignment.Bottom;
                    break;
                case "TopRight":
                    this.HorizontalAlign = TextAlignment.Right;
                    this.VerticalAlign = VerticalAlignment.Top;
                    break;
                case "BottomRight":
                    this.HorizontalAlign = TextAlignment.Right;
                    this.VerticalAlign = VerticalAlignment.Bottom;
                    break;
                default:
                    this.HorizontalAlign = TextAlignment.Left;
                    this.VerticalAlign = VerticalAlignment.Top;
                    break;
            }

            // Camera Label 그림자 설정
            if (CommonsConfig.Instance.CameraLabelUseDropShadowEffect)
            {
                Color? shadowcolor;
                if (!string.IsNullOrEmpty(CommonsConfig.Instance.CameraLabelDropShadowColor))
                {
                    shadowcolor = ColorConverter.ConvertFromString(CommonsConfig.Instance.CameraLabelDropShadowColor) as Color?;
                }
                else
                {
                    shadowcolor = Colors.Black;
                }

                if (!shadowcolor.HasValue)
                {
                    shadowcolor = Colors.Black;
                }

                var dropshadoweffect = new System.Windows.Media.Effects.DropShadowEffect
                {
                    ShadowDepth = CommonsConfig.Instance.CameraLabelDropShadowDepth,
                    Direction = CommonsConfig.Instance.CameraLabelDropShadowDirection,
                    Color = shadowcolor.Value,
                    Opacity = CommonsConfig.Instance.CameraLabelDropShadowOpacity,
                    BlurRadius = CommonsConfig.Instance.CameraLabelDropShadowBlurRadius,
                };

                if (this._textBlock != null)
                {
                    this._textBlock.Effect = dropshadoweffect;
                }

            }

            if (CommonsConfig.Instance.CameraLabelUseFourWayShadow)
            {
                this.TextFourWayShadowVisibility = Visibility.Visible;

                this.TextFourWayShadowDistance = this.ActualHeight * (CommonsConfig.Instance.CameraLabelFourWayShadowDistance / 100);
                this.TextFourWayShadowOpacity = CommonsConfig.Instance.CameraLabelFourWayShadowOpacity;
            }
            else
            {
                this.TextFourWayShadowVisibility = Visibility.Collapsed;
            }
        }

        protected void ControlModeChangeForPlayOnControlMode(bool value)
        {
            if (!this.PlayOnControlMode)
            {
                return;
            }

            isControlMode = value;

            if (value)
            {
                // 부모와 연결되어 있지 않으면 처리하지 않음.
                if (PresentationSource.FromVisual(this) == null)
                {
                    return;
                }

                var newRect = new Rect(this.PointToScreen(new Point(0, 0)), this.PointToScreen(new Point(this.ActualWidth, ActualHeight)));
                this.SetViewPort(new Rect(0, 0, newRect.Width, newRect.Height), newRect);
            }
            else
            {
                this.SetViewPort(Rect.Empty);
            }

            CameraManager.Instance.RedrawCameraControlAsync(this.IsInStage);
        }

        #endregion

        #region Event Handlers

        private void _storyBoardFadeOut_Completed(object sender, EventArgs e)
        {
            this.EndFadeOutEffect();
        }

        private void VideoElement_storyBoardFadeOut_Completed(object sender, EventArgs e)
        {
            if (!this._isAppliedTemplate)
            {
                return;
            }

            this._fadeOutVideoRect.Opacity = 0;
            this._storyBoardFadeOut.Completed -= this.VideoElement_storyBoardFadeOut_Completed;
            this._storyBoardFadeOut.Children.Clear();
        }

        void videoElement_OnChangedStatus(object sender, EventCode eventCode, string sLabel)
        {
            if (this.IsZoomInCell)
                return;

            var level = sLabel;
            if (eventCode == EventCode.Error || eventCode == EventCode.Fail)
                level = eventCode.ToString();

            // 중, 고화질 적용.
            if (this.CheckAccess())
            {
                if (!this.fullRect.IsEmpty)
                {
                    this.SetCurrentCameraBrush(true, level);
                }
            }
            else
            {
                this.Dispatcher.BeginInvoke(new Action(() =>
                {
                    if (!this.fullRect.IsEmpty)
                    {
                        InnotiveDebug.Trace(2, "[shwlee] OnChangedStatus : {0}, {1}", this.ID, level);
                        this.SetCurrentCameraBrush(true, level);
                    }
                }));
            }
        }

        void CameraControl_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            //PTZControl UI가 떴을때 Tab키를 누르면 Focus를 사라지기 때문에 Tab키를 막음
            if (e.Key == Key.Tab)
            {
                e.Handled = true;
                return;
            }
        }

        void CameraControl_MouseEnter(object sender, MouseEventArgs e)
        {
            HandleMouseOver(true);
        }

        void CameraControl_MouseLeave(object sender, MouseEventArgs e)
        {
            HandleMouseOver(false);

            // 패닝시 마우스 포커스 밖으로 나갈경우 패닝 중지
            this._isMouseDown = false;
        }

        public bool IsDigitalZoommed
        {
            get
            {
                return this._isDigitalZoommed;
            }
        }

        private void CameraControl_MouseWheel(object sender, MouseWheelEventArgs e)
        {
            // 영상이 없으면 작동하지 않음.
            if (this.CurrentCameraBrush == null) return;
            // 제어중이면 작동하지 않음.
            if (this.ToggleButtonShowControlUI != null && this.ToggleButtonShowControlUI.IsChecked == true)
            {
                // 제어중이면 카메라 광학 줌이 일어남.
                var ptzcontrol = this.GetChildPTZControl();
                if (ptzcontrol != null)
                {
                    ptzcontrol.OpticalZoomFromMouseWheel(e.Delta);
                }
                return;
            }
            
            var current_mouse_poition = e.GetPosition(this); // 0,0 기준임.
        }

        void CameraControl_MouseDown(object sender, MouseButtonEventArgs e)
        {
            this._isMouseDown = true;
        }

        void CameraControl_MouseUp(object sender, MouseButtonEventArgs e)
        {
            this._isMouseDown = false;
        }

        void CameraControl_MouseMove(object sender, MouseEventArgs e)
        {
            try{
            if (this._isMouseDown)
            {
                if (this.IsZoomInCell)
                {
                   
                }
                else
                {
                    var currentPoint = e.GetPosition(this);
                    videoElement.Pan(currentPoint.X - this._lastDragPoint.X, currentPoint.Y - this._lastDragPoint.Y);
                    this._lastDragPoint = currentPoint;
                }
            }
            }catch(Exception ee)
            {
                return;
            }
        }

        private void CameraControl_ManipulationStarting(object sender, ManipulationStartingEventArgs e)
        {
            // 레이아웃일 경우 작동하지 않음
            if (this.ParentBaseLayoutControl != null) return;

            e.ManipulationContainer = this;
            e.Handled = true;
        }

        private void CameraControl_ManipulationDelta(object sender, ManipulationDeltaEventArgs e)
        {
            // 레이아웃일 경우 작동하지 않음
            if (this.ParentBaseLayoutControl != null)
            {
                InnotiveDebug.Log.Info("CameraControl_ManipulationDelta... this.ParentBaseLayoutControl != null return");

                return;
            }

            // 영상이 없으면 작동하지 않음.
            if (this.CurrentCameraBrush == null)
            {
                InnotiveDebug.Log.Info("CameraControl_ManipulationDelta... this.CurrentCameraBrush == null return");

                return;
            }
            // 제어중이면 작동하지 않음.
            if (this.ToggleButtonShowControlUI != null && this.ToggleButtonShowControlUI.IsChecked == true)
            {
                InnotiveDebug.Log.Info("CameraControl_ManipulationDelta... this.ToggleButtonShowControlUI != null && this.ToggleButtonShowControlUI.IsChecked == true return");

                // 제어중이면 카메라 광학 줌이 일어남.
                //var ptzcontrol = this.GetChildPTZControl();
                //if (ptzcontrol != null)
                //{
                //    ptzcontrol.OpticalZoomFromMouseWheel((int)e.DeltaManipulation.Scale.X);
                //}
                return;
            }


            // 패닝
            if (this.IsZoomInCell)
            {
                
            }
            else
            {
                InnotiveDebug.Log.Info("CameraControl_ManipulationDelta... Translation 2 : " + e.DeltaManipulation.Translation.X + "," + e.DeltaManipulation.Translation.Y);
                videoElement.Pan(e.DeltaManipulation.Translation.X, e.DeltaManipulation.Translation.Y);
            }

            // 확대 축소
            if (e.DeltaManipulation.Scale.X != 1.0)
            {
                var current_mouse_poition = new Point(e.ManipulationOrigin.X, e.ManipulationOrigin.Y);

                if (this.IsZoomInCell)
                {
                   
                }
                else
                {
                    InnotiveDebug.Log.Info("CameraControl_ManipulationDelta... Zoom 2");
                    this._isDigitalZoommed = videoElement.Zoom(Convert.ToInt32(current_mouse_poition.X), Convert.ToInt32(current_mouse_poition.Y), (int)e.DeltaManipulation.Scale.X > 0 ? 1 : -1);
                }
            }

            e.Handled = true;
        }

        private void CameraControl_ManipulationInertiaStarting(object sender, ManipulationInertiaStartingEventArgs e)
        {
            // 레이아웃일 경우 작동하지 않음
            if (this.ParentBaseLayoutControl != null) return;

            e.Handled = true;
        }

        #endregion

        #region ICameraPropertyChanaged Interface

        public void SetZoomInVideoBrush(Brush brush, string level)
        {
            if (CommonsConfig.Instance.IsShowLowHighBorder)
                this.ShowStatusLabel(level, string.Empty);

            if (this.CurrentCameraBrush != null && (Equals(this.CurrentCameraBrush, brush)))
            {
                this.CurrentCameraBrush = brush;
                this.SetBrushViewbox(brush, this.fullRect);

                return;
            }

            this.GoDrawAnimation(brush);
        }

        public void NameChange(string name)
        {
            if (this.Dispatcher.CheckAccess())
            {
                if (string.Compare(this.CameraName, name) == 0)
                    return;

                this.CameraName = name;
            }
            else
            {
                this.Dispatcher.BeginInvoke(new Action(() =>
                {
                    if (string.Compare(this.CameraName, name) == 0)
                        return;

                    this.CameraName = name;
                }));
            }
        }

        public void SetCurrentBrushToBufferBrush(bool isInStage)
        {
            //this.BufferBrush = null;
            //this._fadeOutVideoRect.Fill = null;

            if (this.viewPortRect.IsEmpty)
            {
                this.oldFullRect = this.fullRect.IsEmpty ? Rect.Empty : this.fullRect;
                this.DirectSetBrush(this._fadeOutVideoRect, this.LowVideoCameraBrush, "Low");
                return;
            }

            if (this.videoElement == null)
            {
                if (!this.InitializeVideoElement())
                {
                    InnotiveDebug.Log.Error(string.Format("[Initialize VideoElement Error] Create Failed!  ID : {0} ", this.ID));
                    return;
                }
            }

            //if (this.LowVideoCameraBrush == null)
            //{
            //    this.ReadyToStillBrush(isInStage);
            //}

            if (this.LowVideoCameraBrush == null)
            {
                ImageBrush ib = new ImageBrush();
                RenderTargetBitmap bmp = new RenderTargetBitmap((int)this.ActualWidth, (int)this.ActualHeight, 96, 96, PixelFormats.Pbgra32);
                bmp.Render(this._videoRect);
                ib.ImageSource = bmp;
                ib.Freeze();

                this.CurrentCameraBrush = ib;
                this._fadeOutVideoRect.Margin = new Thickness(0.0);
                this._fadeOutVideoRect.Visibility = Visibility.Visible;
                this._fadeOutVideoRect.Opacity = 1.0;
                this._fadeOutVideoRect.Fill = ib;
            }
            else
            {
                this._fadeOutVideoRect.Visibility = Visibility.Visible;
                this._fadeOutVideoRect.Opacity = 1.0;
                //this._fadeOutVideoRect.Fill = this.LowVideoCameraBrush ?? this.BufferBrush;
            }

            // 이전 surface 영상이 나오지 않게 처리.
            this.CurrentCameraBrush = null;

            this.StopVideo();


            if (CommonsConfig.Instance.IsShowLowHighBorder)
            {
                this.ShowStatusLabel("Low", string.Empty);
                InnotiveDebug.Trace(2, "[shwlee] ShowStatusLabel Set Low : {0}", this.ID);
            }
        }

        public void RequestVideo(ResolutionLevel switchinglevel, Rect changedRect, ViewMode currentViewMode)
        {
            if (this.videoElement == null)
            {
                if (!this.InitializeVideoElement())
                {
                    InnotiveDebug.Log.Error(string.Format("[Initialize VideoElement Error] Create Failed!  ID : {0} ", this.ID));
                    return;
                }
            }

            if (this.viewPortRect.IsEmpty)
            {
                this.CurrentCameraBrush = null;
                this.DirectSetBrush(this._fadeOutVideoRect, this.LowVideoCameraBrush, "Low");

                // 모드 전환 시 영상이 남아서 CPU 점유하는 문제 때문에 일단 막음. - by shwlee.
                //if (this.IsInStage && currentViewMode == ViewMode.Camera)
                //{
                //    if (this.videoElement != null)
                //    {
                //        this.videoElement.Pause();
                //    }
                //}
                //else
                //{
                //    if (this.videoElement != null)
                //    {
                //        this.videoElement.SetDisplayRect(0, 0, 0, 0, 0);
                //    }
                //}

                if (this.videoElement != null)
                {
                    this.StopVideo();

                    // ViewBox의 변경이 없으면 요청하지 않게 처리함 - 명길
                    var targetBrush = this.CurrentCameraBrush as TileBrush;
                    if (targetBrush == null)
                        return;

                    targetBrush.Viewbox = Rect.Empty;
                }

                return;
            }

            //SourceStatus level;
            //if (!Enum.TryParse(switchinglevel.ToString(), out level))
            //    level = SourceStatus.Unknown;

            // 저화질 / 중,고화질 상황별 video요청.
            if (switchinglevel == ResolutionLevel.LowRes)
            {
                if (this.LowVideoCameraBrush != null)
                {
                    //if (this.oldLevel == level)
                    //    return;

                    if (CommonsConfig.Instance.IsShowLowHighBorder)
                        this.ShowStatusLabel("Low", string.Empty);

                    if (this.videoElement != null)
                    {
                        this.videoElement.SetDisplayRect(0, 0, 0, 0, 0);
                        this.videoElement.Stop();

                        // ViewBox의 변경이 없으면 요청하지 않게 처리함 - 명길
                        var targetBrush = this.CurrentCameraBrush as TileBrush;
                        if (targetBrush == null)
                            return;
                        targetBrush.Viewbox = Rect.Empty;
                    }

                    this._videoRect.Opacity = 0.0;
                    this.CurrentCameraBrush = null;
                    return;
                }
            }

            if (this.CurrentCameraBrush != null)
            {
                // ViewBox의 변경이 없으면 요청하지 않게 처리함 - 명길
                var targetBrush = this.CurrentCameraBrush as TileBrush;
                if (targetBrush == null)
                    return;

                //if(CameraManager.Instance.CurrentViewMode == ViewMode.Camera)
                //{
                //    if (targetBrush.Viewbox.Equals(changedRect) && (!this.oldFullRect.Equals(targetBrush.Viewbox)))
                //    {
                //        return;
                //    }    
                //}

                /*
                if (targetBrush.Viewbox.Left == changedRect.Left && targetBrush.Viewbox.Top == changedRect.Top &&
                    targetBrush.Viewbox.Right == changedRect.Right && targetBrush.Viewbox.Bottom == changedRect.Bottom
                    ) return;
                 */

                this.oldFullRect = targetBrush.Viewbox;

                // TODO :  테스트 후 이상 없으면 메서드 삭제 -by shwlee.
                //this.ReadyToStillBrush(false);
                //this.CurrentCameraBrush = this.BufferBrush;
            }

            // 화면에 표시할 최소크기 확인.
            if (this.CheckLimitCameraSize())
                return;

            if (CommonsConfig.Instance.IsShowLowHighBorder)
                this.ShowStatusLabel("Waiting", string.Empty);

            // Stage에서 더블 클릭 했을 때는 StageSurface로 영상을 요청한다.
            if (this.IsZoomInCell)
            {
                this.StopVideo();
                this.videoElement.Clear();
                this.RequestZoomInCamera(changedRect);
                return;
            }

            ThreadPool.QueueUserWorkItem(this.VideoElementSetDisplayRect, new RequestCamera { CameraId = this.ID, CurrentRegion = changedRect });
            this._videoRect.Opacity = 0.0;
        }

        /// <summary>
        /// ZoomIn된 Cell 표출되고 있는 영상을 StageSurface로 전환 요청한다.
        /// </summary>
        /// <param name="changedRect">표시될 영상 크기.</param>
        private void RequestZoomInCamera(Rect changedRect)
        {
            if (this.parent != null)
            {
                
            }
        }

        /// <summary>
        /// 화면에 표시할 최소 크기를 확인힌다.
        /// </summary>
        /// <returns>최소크기 이하이면 true / 이상이면 false.</returns>
        private bool CheckLimitCameraSize()
        {
            var isLimited = false;
            if (CommonsConfig.Instance.ShowLimitWidth > 0)
            {
                if (this.viewPortRect != Rect.Empty && this.viewPortRect.Width < CommonsConfig.Instance.ShowLimitWidth)
                {
                    isLimited = true;
                }
            }

            if (CommonsConfig.Instance.ShowLimitHeight > 0)
            {
                if (this.viewPortRect != Rect.Empty && this.viewPortRect.Height < CommonsConfig.Instance.ShowLimitHeight)
                {
                    isLimited = true;
                }
            }

            if (CommonsConfig.Instance.IsShowLowHighBorder && isLimited)
            {
                this.ShowStatusLabel("Low", string.Empty);
            }

            return isLimited;
        }

        private void InnerCheckStop()
        {
            if (CommonsConfig.Instance.CameraSourceSwitchingUsePause)
            {
                Debug.WriteLine(string.Format("[elrha] Camera Pause: {0}", this.ID));
                this.videoElement.Pause();
            }
            else
            {
                Debug.WriteLine(string.Format("[elrha] Camera Stop: {0}", this.ID));
                this.videoElement.Stop();
            }
        }

        public void StopVideo()
        {
            if (this.videoElement == null)
            {
                return;
            }

            // [elrha] 옵션 확인 후 Pause 혹은 Stop
            InnerCheckStop();
            //this.videoElement.Stop();
        }

        public void VideoClear()
        {
            if (this.videoElement == null)
                return;

            InnotiveDebug.Trace(2, "[shwlee] Video Clear. ID : {0}", this.ID);
            //this.videoElement.SetDisplayRect(0, 0, 0, 0, 0);

            this.CurrentCameraBrush = null;
            this.videoElement.Stop();

            //this.videoElement.Clear();
        }

        #endregion // ICameraPropertyChanaged Interface

        #region Request Video Parameter

        class RequestCamera
        {
            public string CameraId { get; set; }
            public Rect CurrentRegion { get; set; }
        }

        #endregion
    }
}