﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CameraIDValidationRule.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   The camera id validation rule.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.DLLs.CameraControls.ValidationRule
{
    using System.Globalization;
    using System.Windows.Controls;

    /// <summary>
    /// The camera id validation rule.
    /// </summary>
    internal class CameraIDValidationRule : System.Windows.Controls.ValidationRule
    {
        /// <summary>
        /// The validate.
        /// </summary>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <param name="cultureInfo">
        /// The culture info.
        /// </param>
        /// <returns>
        /// </returns>
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            if (((string) value).StartsWith(" "))
            {
                return new ValidationResult(false, "문자열 앞에 공백 문자를 입력할 수 없습니다.");
            }

            if (((string) value).EndsWith(" "))
            {
                return new ValidationResult(false, "문자열 뒤에 공백 문자를 입력할 수 없습니다.");
            }

            return ValidationResult.ValidResult;
        }
    }
}