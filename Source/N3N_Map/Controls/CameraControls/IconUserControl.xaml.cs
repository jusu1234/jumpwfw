﻿
namespace Innotive.InnoWatch.DLLs.CameraControls
{
    using System.Windows.Controls;

    /// <summary>
    /// Interaction logic for IconUserControl.xaml
    /// </summary>
    public partial class IconUserControl : UserControl
    {
        public IconUserControl()
        {
            InitializeComponent();
        }
    }
}
