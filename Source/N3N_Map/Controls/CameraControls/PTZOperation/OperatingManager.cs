﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="OperatingManager.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Defines the OperatingManager type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.DLLs.CameraControls.PTZOperation
{
    using System.Windows;
    using System.Windows.Controls.Primitives;

    using Commons.CameraController;
    using Commons.CameraController.CameraControlInfos;
    using Commons.CameraController.PresetSequence;

    public class OperatingManager
    {
        private string cameraID = string.Empty;
        private PTZControl ptzControl = null;

        public OperatingManager(string cameraID, PTZControl ptzControl)
        {
            this.cameraID = cameraID;
            this.ptzControl = ptzControl;

            this.ptzControl.xButtonAutoFocusOn.Click += this.xButtonAutoFocusOn_Click;
            this.ptzControl.xButtonAutoFocusOff.Click += this.xButtonAutoFocusOff_Click;

            this.ptzControl.xButtonAutoIrisOn.Click += this.xButtonAutoIrisOn_Click;
            this.ptzControl.xButtonAutoIrisOff.Click += this.xButtonAutoIrisOff_Click;

            this.ptzControl.xToggleButtonMicOnOff.Click += this.xToggleButtonMicOnOff_Click;
            this.ptzControl.xToggleButtonSpeakerOnOff.Click += this.xToggleButtonSpeakerOnOff_Click;

            this.ptzControl.xButtonPowerCameraOn.Click += this.xButtonPowerCameraOn_Click;
            this.ptzControl.xButtonPowerCameraOff.Click += this.xButtonPowerCameraOff_Click;
            this.ptzControl.xButtonPowerLightOn.Click += this.xButtonPowerLightOn_Click;
            this.ptzControl.xButtonPowerLightOff.Click += this.xButtonPowerLightOff_Click;
            this.ptzControl.xButtonPowerAux1On.Click += this.xButtonPowerAux1On_Click;
            this.ptzControl.xButtonPowerAux1Off.Click += this.xButtonPowerAux1Off_Click;
            this.ptzControl.xButtonPowerAux2On.Click += this.xButtonPowerAux2On_Click;
            this.ptzControl.xButtonPowerAux2Off.Click += this.xButtonPowerAux2Off_Click;

            this.ptzControl.xButtonPresetGo.Click += this.xButtonPresetGo_Click;
            this.ptzControl.xButtonSequenceStart.Click += this.xButtonSequenceStart_Click;
            this.ptzControl.xButtonSequenceStop.Click += this.xButtonSequenceStop_Click;


            this.ptzControl.xButtonAutoFocusOn.StylusUp += this.xButtonAutoFocusOn_StylusUp;
            this.ptzControl.xButtonAutoFocusOff.StylusUp += this.xButtonAutoFocusOff_StylusUp;

            this.ptzControl.xButtonAutoIrisOn.StylusUp += this.xButtonAutoIrisOn_StylusUp;
            this.ptzControl.xButtonAutoIrisOff.StylusUp += this.xButtonAutoIrisOff_StylusUp;

            this.ptzControl.xToggleButtonMicOnOff.StylusUp += this.xToggleButtonMicOnOff_StylusUp;
            this.ptzControl.xToggleButtonSpeakerOnOff.StylusUp += this.xToggleButtonSpeakerOnOff_StylusUp;

            this.ptzControl.xButtonPowerCameraOn.StylusUp += this.xButtonPowerCameraOn_StylusUp;
            this.ptzControl.xButtonPowerCameraOff.StylusUp += this.xButtonPowerCameraOff_StylusUp;
            this.ptzControl.xButtonPowerLightOn.StylusUp += this.xButtonPowerLightOn_StylusUp;
            this.ptzControl.xButtonPowerLightOff.StylusUp += this.xButtonPowerLightOff_StylusUp;
            this.ptzControl.xButtonPowerAux1On.StylusUp += this.xButtonPowerAux1On_StylusUp;
            this.ptzControl.xButtonPowerAux1Off.StylusUp += this.xButtonPowerAux1Off_StylusUp;
            this.ptzControl.xButtonPowerAux2On.StylusUp += this.xButtonPowerAux2On_StylusUp;
            this.ptzControl.xButtonPowerAux2Off.StylusUp += this.xButtonPowerAux2Off_StylusUp;

            this.ptzControl.xButtonPresetGo.StylusUp += this.xButtonPresetGo_StylusUp;
            this.ptzControl.xButtonSequenceStart.StylusUp += this.xButtonSequenceStart_StylusUp;
            this.ptzControl.xButtonSequenceStop.StylusUp += this.xButtonSequenceStop_StylusUp;


            PTZInformation ptzInformation = GetPTZInformation(cameraID);
            if (ptzInformation != null)
            {
                //preset 추가
                this.ptzControl.xComboBoxPreset.ItemsSource = null;
                this.ptzControl.xComboBoxPreset.Items.Clear();
                for (int i = 0; i < ptzInformation.PresetCount; i++)
                {
                    this.ptzControl.PresetSource.Add((i + 1).ToString());
                }

                this.ptzControl.xComboBoxPreset.ItemsSource = this.ptzControl.PresetSource;

                //sequence 추가
                this.ptzControl.xComboBoxSequence.ItemsSource = null;
                this.ptzControl.xComboBoxSequence.Items.Clear();

                Sequences sequences = null;
                PTZManager.Instance.Sequence.SequenceDictionary.TryGetValue(this.cameraID, out sequences);
                if (sequences != null)
                {
                    this.ptzControl.xComboBoxSequence.ItemsSource = sequences.SequenceList;
                }
            }
        }

        private void xButtonAutoFocusOn_StylusUp(object sender, System.Windows.Input.StylusEventArgs e)
        {
            PTZInformation ptzInformation = this.GetPTZInformation(this.cameraID);
            if (ptzInformation != null)
            {
                PTZManager.Instance.RequestToCameraControllerAction(ptzInformation, PtzControlCommand.AutoFocus);
            }
        }

        private void xButtonAutoFocusOff_StylusUp(object sender, System.Windows.Input.StylusEventArgs e)
        {
            PTZInformation ptzInformation = this.GetPTZInformation(this.cameraID);
            if (ptzInformation != null)
            {
                PTZManager.Instance.RequestToCameraControllerAction(ptzInformation, PtzControlCommand.AutoFocus, "off");
            }
        }

        private void xButtonAutoIrisOn_StylusUp(object sender, System.Windows.Input.StylusEventArgs e)
        {
            PTZInformation ptzInformation = this.GetPTZInformation(this.cameraID);
            if (ptzInformation != null)
            {
                PTZManager.Instance.RequestToCameraControllerAction(ptzInformation, PtzControlCommand.AutoIris);
            }
        }

        private void xButtonAutoIrisOff_StylusUp(object sender, System.Windows.Input.StylusEventArgs e)
        {
            PTZInformation ptzInformation = this.GetPTZInformation(this.cameraID);
            if (ptzInformation != null)
            {
                PTZManager.Instance.RequestToCameraControllerAction(ptzInformation, PtzControlCommand.AutoIris, "off");
            }
        }

        private void xToggleButtonMicOnOff_StylusUp(object sender, System.Windows.Input.StylusEventArgs e)
        {
            var ptzInformation = this.GetPTZInformation(this.cameraID);

            if (ptzInformation == null)
                return;

            var toggleButton = e.Source as ToggleButton;
            if (toggleButton == null)
                return;

            PTZManager.Instance.MicSpeakerOnOff(toggleButton, ptzInformation, true);
        }

        private void xToggleButtonSpeakerOnOff_StylusUp(object sender, System.Windows.Input.StylusEventArgs e)
        {
            var ptzInformation = this.GetPTZInformation(this.cameraID);

            if (ptzInformation == null)
                return;

            var toggleButton = e.Source as ToggleButton;
            if (toggleButton == null)
                return;

            PTZManager.Instance.MicSpeakerOnOff(toggleButton, ptzInformation, false);
        }

        private void xButtonPowerCameraOn_StylusUp(object sender, System.Windows.Input.StylusEventArgs e)
        {
            PTZInformation ptzInformation = this.GetPTZInformation(this.cameraID);
            if (ptzInformation != null)
            {
                PTZManager.Instance.RequestToCameraControllerAction(ptzInformation, PtzControlCommand.PowerCamera, "on");
            }
        }

        private void xButtonPowerCameraOff_StylusUp(object sender, System.Windows.Input.StylusEventArgs e)
        {
            PTZInformation ptzInformation = this.GetPTZInformation(this.cameraID);
            if (ptzInformation != null)
            {
                PTZManager.Instance.RequestToCameraControllerAction(ptzInformation, PtzControlCommand.PowerCamera, "off");
            }
        }

        private void xButtonPowerLightOn_StylusUp(object sender, System.Windows.Input.StylusEventArgs e)
        {
            PTZInformation ptzInformation = this.GetPTZInformation(this.cameraID);
            if (ptzInformation != null)
            {
                PTZManager.Instance.RequestToCameraControllerAction(ptzInformation, PtzControlCommand.PowerLight, "on");
            }
        }

        private void xButtonPowerLightOff_StylusUp(object sender, System.Windows.Input.StylusEventArgs e)
        {
            PTZInformation ptzInformation = this.GetPTZInformation(this.cameraID);
            if (ptzInformation != null)
            {
                PTZManager.Instance.RequestToCameraControllerAction(ptzInformation, PtzControlCommand.PowerLight, "off");
            }
        }

        private void xButtonPowerAux1On_StylusUp(object sender, System.Windows.Input.StylusEventArgs e)
        {
            PTZInformation ptzInformation = this.GetPTZInformation(this.cameraID);
            if (ptzInformation != null)
            {
                PTZManager.Instance.RequestToCameraControllerAction(ptzInformation, PtzControlCommand.PowerAux1, "on");
            }
        }

        private void xButtonPowerAux1Off_StylusUp(object sender, System.Windows.Input.StylusEventArgs e)
        {
            PTZInformation ptzInformation = this.GetPTZInformation(this.cameraID);
            if (ptzInformation != null)
            {
                PTZManager.Instance.RequestToCameraControllerAction(ptzInformation, PtzControlCommand.PowerAux1, "off");
            }
        }

        private void xButtonPowerAux2On_StylusUp(object sender, System.Windows.Input.StylusEventArgs e)
        {
            PTZInformation ptzInformation = this.GetPTZInformation(this.cameraID);
            if (ptzInformation != null)
            {
                PTZManager.Instance.RequestToCameraControllerAction(ptzInformation, PtzControlCommand.PowerAux2, "on");
            }
        }

        private void xButtonPowerAux2Off_StylusUp(object sender, System.Windows.Input.StylusEventArgs e)
        {
            PTZInformation ptzInformation = this.GetPTZInformation(this.cameraID);
            if (ptzInformation != null)
            {
                PTZManager.Instance.RequestToCameraControllerAction(ptzInformation, PtzControlCommand.PowerAux2, "off");
            }
        }

        private void xButtonPresetGo_StylusUp(object sender, System.Windows.Input.StylusEventArgs e)
        {
            PTZInformation ptzInformation = this.GetPTZInformation(this.cameraID);
            if (ptzInformation == null)
                return;

            if (this.ptzControl.xComboBoxPreset.SelectedIndex == -1)
                return;

            PTZManager.Instance.RequestPresetAction(ptzInformation, this.ptzControl.xComboBoxPreset.SelectedIndex);
        }
        private void xButtonSequenceStart_StylusUp(object sender, System.Windows.Input.StylusEventArgs e)
        {
            PTZInformation ptzInformation = this.GetPTZInformation(this.cameraID);
            if (ptzInformation == null)
                return;

            if (this.ptzControl.xComboBoxSequence.SelectedIndex == -1)
                return;

            var selectedItem = this.ptzControl.xComboBoxSequence.SelectedItem;

            PTZManager.Instance.RequestSequenceAction(ptzInformation, selectedItem);
        }

        private void xButtonSequenceStop_StylusUp(object sender, System.Windows.Input.StylusEventArgs e)
        {
            PTZInformation ptzInformation = this.GetPTZInformation(this.cameraID);

            if (ptzInformation != null)
            {
                PTZManager.Instance.RequestToCameraControllerAction(ptzInformation, PtzControlCommand.SequenceStop);
            }
        }

        void xButtonPresetGo_Click(object sender, RoutedEventArgs e)
        {
            PTZInformation ptzInformation = this.GetPTZInformation(this.cameraID);
            if (ptzInformation == null)
                return;

            if (this.ptzControl.xComboBoxPreset.SelectedIndex == -1)
                return;

            PTZManager.Instance.RequestPresetAction(ptzInformation, this.ptzControl.xComboBoxPreset.SelectedIndex);
        }

        void xButtonSequenceStart_Click(object sender, RoutedEventArgs e)
        {
            PTZInformation ptzInformation = this.GetPTZInformation(this.cameraID);
            if (ptzInformation == null)
                return;

            if (this.ptzControl.xComboBoxSequence.SelectedIndex == -1)
                return;

            var selectedItem = this.ptzControl.xComboBoxSequence.SelectedItem;

            PTZManager.Instance.RequestSequenceAction(ptzInformation, selectedItem);
        }

        void xButtonSequenceStop_Click(object sender, RoutedEventArgs e)
        {
            PTZInformation ptzInformation = this.GetPTZInformation(this.cameraID);

            if (ptzInformation != null)
            {
                PTZManager.Instance.RequestToCameraControllerAction(ptzInformation, PtzControlCommand.SequenceStop);
            }
        }

        public void RequestPTZCommand(PtzControlCommand ptzControlCommand, string speed)
        {
            PTZInformation ptzInformation = this.GetPTZInformation(this.cameraID);
            if (ptzInformation != null)
            {
                PTZManager.Instance.RequestToCameraControllerAction(ptzInformation, ptzControlCommand, speed);
            }
        }

        public void SelectCamera()
        {
            PTZInformation ptzInformation = this.GetPTZInformation(this.cameraID);
            if (ptzInformation != null)
            {
                PTZManager.Instance.RequestToCameraControllerAction(ptzInformation, PtzControlCommand.SelectCamera);
            }
        }

        private PTZInformation GetPTZInformation(string cameraId)
        {
            PTZInformation ptzInformation = null;
            if (CameraControlInfoExecution.Instance.CameraControlInfo != null)
                ptzInformation = CameraControlInfoExecution.Instance.CameraControlInfo.GetPtzInformation(cameraId);

            return ptzInformation;
        }

        void xButtonAutoFocusOn_Click(object sender, RoutedEventArgs e)
        {
            PTZInformation ptzInformation = this.GetPTZInformation(this.cameraID);
            if (ptzInformation != null)
            {
                PTZManager.Instance.RequestToCameraControllerAction(ptzInformation, PtzControlCommand.AutoFocus);
            }
        }
        void xButtonAutoFocusOff_Click(object sender, RoutedEventArgs e)
        {
            PTZInformation ptzInformation = this.GetPTZInformation(this.cameraID);
            if (ptzInformation != null)
            {
                PTZManager.Instance.RequestToCameraControllerAction(ptzInformation, PtzControlCommand.AutoFocus, "off");
            }
        }

        void xButtonAutoIrisOn_Click(object sender, RoutedEventArgs e)
        {
            PTZInformation ptzInformation = this.GetPTZInformation(this.cameraID);
            if (ptzInformation != null)
            {
                PTZManager.Instance.RequestToCameraControllerAction(ptzInformation, PtzControlCommand.AutoIris);
            }
        }
        void xButtonAutoIrisOff_Click(object sender, RoutedEventArgs e)
        {
            PTZInformation ptzInformation = this.GetPTZInformation(this.cameraID);
            if (ptzInformation != null)
            {
                PTZManager.Instance.RequestToCameraControllerAction(ptzInformation, PtzControlCommand.AutoIris, "off");
            }
        }

        void xToggleButtonMicOnOff_Click(object sender, RoutedEventArgs e)
        {
            var ptzInformation = this.GetPTZInformation(this.cameraID);

            if (ptzInformation == null)
                return;

            var toggleButton = e.Source as ToggleButton;
            if (toggleButton == null)
                return;

            PTZManager.Instance.MicSpeakerOnOff(toggleButton, ptzInformation, true);
        }
        void xToggleButtonSpeakerOnOff_Click(object sender, RoutedEventArgs e)
        {
            var ptzInformation = this.GetPTZInformation(this.cameraID);

            if (ptzInformation == null)
                return;
            
            var toggleButton = e.Source as ToggleButton;
            if (toggleButton == null)
                return;

            PTZManager.Instance.MicSpeakerOnOff(toggleButton, ptzInformation, false);
        }

        void xButtonPowerCameraOn_Click(object sender, RoutedEventArgs e)
        {
            PTZInformation ptzInformation = this.GetPTZInformation(this.cameraID);
            if (ptzInformation != null)
            {
                PTZManager.Instance.RequestToCameraControllerAction(ptzInformation, PtzControlCommand.PowerCamera, "on");
            }
        }
        void xButtonPowerCameraOff_Click(object sender, RoutedEventArgs e)
        {
            PTZInformation ptzInformation = this.GetPTZInformation(this.cameraID);
            if (ptzInformation != null)
            {
                PTZManager.Instance.RequestToCameraControllerAction(ptzInformation, PtzControlCommand.PowerCamera, "off");
            }
        }

        void xButtonPowerLightOn_Click(object sender, RoutedEventArgs e)
        {
            PTZInformation ptzInformation = this.GetPTZInformation(this.cameraID);
            if (ptzInformation != null)
            {
                PTZManager.Instance.RequestToCameraControllerAction(ptzInformation, PtzControlCommand.PowerLight, "on");
            }
        }
        void xButtonPowerLightOff_Click(object sender, RoutedEventArgs e)
        {
            PTZInformation ptzInformation = this.GetPTZInformation(this.cameraID);
            if (ptzInformation != null)
            {
                PTZManager.Instance.RequestToCameraControllerAction(ptzInformation, PtzControlCommand.PowerLight, "off");
            }
        }

        void xButtonPowerAux1On_Click(object sender, RoutedEventArgs e)
        {
            PTZInformation ptzInformation = this.GetPTZInformation(this.cameraID);
            if (ptzInformation != null)
            {
                PTZManager.Instance.RequestToCameraControllerAction(ptzInformation, PtzControlCommand.PowerAux1, "on");
            }
        }
        void xButtonPowerAux1Off_Click(object sender, RoutedEventArgs e)
        {
            PTZInformation ptzInformation = this.GetPTZInformation(this.cameraID);
            if (ptzInformation != null)
            {
                PTZManager.Instance.RequestToCameraControllerAction(ptzInformation, PtzControlCommand.PowerAux1, "off");
            }
        }

        void xButtonPowerAux2On_Click(object sender, RoutedEventArgs e)
        {
            PTZInformation ptzInformation = this.GetPTZInformation(this.cameraID);
            if (ptzInformation != null)
            {
                PTZManager.Instance.RequestToCameraControllerAction(ptzInformation, PtzControlCommand.PowerAux2, "on");
            }
        }
        void xButtonPowerAux2Off_Click(object sender, RoutedEventArgs e)
        {
            PTZInformation ptzInformation = this.GetPTZInformation(this.cameraID);
            if (ptzInformation != null)
            {
                PTZManager.Instance.RequestToCameraControllerAction(ptzInformation, PtzControlCommand.PowerAux2, "off");
            }
        }

        public void Stop(PTZControl.PTZStates ptzState)
        {
            PTZInformation ptzInformation = this.GetPTZInformation(this.cameraID);

            if (ptzInformation == null)
            {
                return;
            }

            switch (ptzState)
            {
                case PTZControl.PTZStates.PanTilt:
                    PTZManager.Instance.RequestToCameraControllerAction(ptzInformation, PtzControlCommand.PantiltStop);
                    break;
                case PTZControl.PTZStates.Zoom:
                    PTZManager.Instance.RequestToCameraControllerAction(ptzInformation, PtzControlCommand.ZoomStop);
                    break;
                case PTZControl.PTZStates.Focus:
                    PTZManager.Instance.RequestToCameraControllerAction(ptzInformation, PtzControlCommand.FocusStop);
                    break;
                case PTZControl.PTZStates.Iris:
                    PTZManager.Instance.RequestToCameraControllerAction(ptzInformation, PtzControlCommand.IrisStop);
                    break;
            }
        }
    }
}
