﻿
namespace Innotive.InnoWatch.DLLs.CameraControls.Converters
{
    using System;
    using System.Windows.Data;

    public class CameraPTZEnableStateConverter : IValueConverter
    {
        /// <summary>
        ///  Radio 버튼 3가지 (Always On, Always Off, On At ZoomLevel) 과 PTZEnableState(Always, None, ZoomLevel)을 연결한다. 
        /// </summary>
        /// <param name="value">해당 카메라의 PTZEnableState</param>
        /// <param name="targetType">Boolean (RadioButton 의 isCheckedProperty)</param>
        /// <param name="parameter">라디오버튼에 연결할 PTZEnableState Enum 의 string 값</param>
        /// <param name="culture"></param>
        /// <returns>해당 라디오버튼의 IsChecked Property</returns>
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            //카메라의 PTZEnabled Enum Property 를 string 형식으로 변환한다. 
            var valuePTZEnableState = Enum.GetName(typeof(PTZEnableState), value);

            //해당 라디오버튼이 어떠한 기능에 연결되어있는지를 parameter로 받고 이를 string 으로 변환한다. 
            var parameterPTZEnableState = parameter.ToString();

            //두값이 같다면 true(RadioButton 선택), 아니라면 false (RadioButton해제)
            if (string.Equals(valuePTZEnableState, parameterPTZEnableState))
            {
                return true;
            }

            return false;
        }

        /// <summary>
        ///  Radio 버튼 3가지 (Always On, Always Off, On At ZoomLevel) 과 PTZEnableState(Always, None, ZoomLevel)을 연결한다. 
        /// </summary>
        /// <param name="value">RadioButton 의 선택 상태 </param>
        /// <param name="targetType">Boolean (RadioButton 의 isCheckedProperty)</param>
        /// <param name="parameter">라디오버튼에 연결할 PTZEnableState Enum 의 string 값</param>
        /// <param name="culture"></param>
        /// <returns>해당 라디오버튼의 IsChecked Property</returns>
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            bool isSelected = (bool)value;

            //주의!!!
            //isSelected가 아닌 경우에는 ConvertBack에 들어오지 못한다. 

            //ConvertBack 을 진행하고 있는 RadioButton 이 선택상태일 경우 
            if (isSelected)
            {
                //PTZEnableState의 모든 경우를 string 배열로 저장한다. 
                var enumStates = Enum.GetNames(typeof(PTZEnableState));

                //해당 라디오버튼이 어떠한 기능에 연결되어있는지를 parameter로 받고 이를 string 으로 변환한다. 
                var parameterPTZEnableState = parameter.ToString();
                
                //해당 라디오버튼이 어떠한 enum 값인지 string으로 비교하여 같은경우 해당 Enum을 리턴해준다 . 
                for (var i = 0; i < enumStates.Length; i++)
                {
                    if (string.Equals(enumStates[i], parameterPTZEnableState))
                        return Enum.GetValues(typeof(PTZEnableState)).GetValue(i);        
                }

                return null;
            }

            return null;
        }
    }
}
