﻿namespace Innotive.InnoWatch.DLLs.CameraControls.Converters
{
    using System;
    using System.Globalization;
    using System.Windows.Data;


    public class CameraEditorPTZUIEnableStateConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            bool isZoomLevelRadioSelected = (bool)values[0];
            bool isLockedToParentsMinLevelChecked = (bool)values[1];

            if (isZoomLevelRadioSelected && !isLockedToParentsMinLevelChecked)
            {
                return true;
            }

            return false;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}
