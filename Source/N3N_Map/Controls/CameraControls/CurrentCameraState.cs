﻿
using System.Windows;

namespace Innotive.InnoWatch.DLLs.CameraControls
{
    public struct CameraState
    {
        public double Left { get; set; }

        public double Top { get; set; }

        public double ActualWidth { get; set; }

        public double ActualHeight { get; set; }

        public string ID { get; set; }

        public Visibility Visibility { get; set; }

        public bool IsVisible { get; set; }
    }
}
