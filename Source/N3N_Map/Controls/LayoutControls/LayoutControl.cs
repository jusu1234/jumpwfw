﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LayoutControl.cs" company="Innotive Inc. Korea">
//   Copyright (c) 2012 Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   The process mode.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.DLLs.LayoutControls
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;
    using System.Windows.Media;

    using Innotive.InnoWatch.Commons.BaseControls;
    using Innotive.InnoWatch.Commons.CameraManagers;
    using Innotive.InnoWatch.Commons.CameraManagers.InnoVideoImages;
    using Innotive.InnoWatch.Commons.EventArguments;
    using Innotive.InnoWatch.Commons.Publics;
    using Innotive.InnoWatch.Commons.Xamls;
    using Innotive.InnoWatch.CommonUtils.Log;
    using Innotive.InnoWatch.DLLs.LayoutControls.Elements;
    using Innotive.InnoWatch.DLLs.LayoutControls.Processes;
    using Innotive.InnoWatch.DLLs.CameraControls;

    /// <summary>
    /// The layout type.
    /// </summary>
    public enum LayoutType
    {
        /// <summary>
        /// The layout control.
        /// </summary>
        LayoutControl,

        /// <summary>
        /// The i canvas.
        /// </summary>
        iCanvas
    }

    /// <summary>
    /// The layout control.
    /// </summary>
    public class LayoutControl : ZoomControl, IBaseLayoutControl
    {
        public static readonly DependencyProperty LayoutTypeProperty =
            DependencyProperty.Register(
                "LayoutType",
                typeof(LayoutType),
                typeof(LayoutControl),
                new FrameworkPropertyMetadata(LayoutType.LayoutControl, null));

        public static readonly DependencyProperty LayoutDescriptionProperty =
            DependencyProperty.Register(
                "LayoutDescription",
                typeof(string),
                typeof(LayoutControl),
                new FrameworkPropertyMetadata(string.Empty, null));

        public static readonly DependencyProperty LayoutShowToListProperty =
            DependencyProperty.Register(
                "LayoutShowToList",
                typeof(bool),
                typeof(LayoutControl),
                new FrameworkPropertyMetadata(true, null));

        public static readonly DependencyProperty InsideBackgroundProperty =
            DependencyProperty.Register(
                "InsideBackground",
                typeof(Brush),
                typeof(LayoutControl),
                new FrameworkPropertyMetadata(Brushes.White, new PropertyChangedCallback(OnChangeInsideBackground)));

        public static readonly DependencyProperty OutsideBackgroundProperty =
            DependencyProperty.Register(
                "OutsideBackground",
                typeof(Brush),
                typeof(LayoutControl),
                new FrameworkPropertyMetadata(Brushes.White, new PropertyChangedCallback(OnChangeOutsideBackground)));

        public static RoutedEvent eSelectedElementsChangedEvent =
           EventManager.RegisterRoutedEvent(
               "eSelectedElementsChanged",
               RoutingStrategy.Bubble,
               typeof(SelectedElementsChangedRoutedEventHandler),
               typeof(LayoutControl));

        public static RoutedEvent eFocusedLayoutChangedEvent =
            EventManager.RegisterRoutedEvent(
                "eFocusedLayoutChanged",
                RoutingStrategy.Bubble,
                typeof(FocusedLayoutChangedRoutedEventHandler),
                typeof(LayoutControl));

        public static RoutedEvent eZoomInEvent =
            EventManager.RegisterRoutedEvent(
                "eZoomIn",
                RoutingStrategy.Bubble,
                typeof(RectRoutedEventHandler),
                typeof(LayoutControl));

        public static RoutedEvent eZoomOutEvent =
            EventManager.RegisterRoutedEvent(
                "eZoomOut",
                RoutingStrategy.Bubble,
                typeof(RectRoutedEventHandler),
                typeof(LayoutControl));

        // Panning 을 담당
        public static RoutedEvent eSettingRectEvent =
            EventManager.RegisterRoutedEvent(
                "eSettingRect",
                RoutingStrategy.Bubble,
                typeof(RectRoutedEventHandler),
                typeof(LayoutControl));

        private bool _isFocusedLayout = false;

        private ElementManager _elementManager;

        private string OwnVideoSurfaceId { get; set; }

        /// <summary>
        /// Initializes static members of the <see cref="LayoutControl"/> class.
        /// </summary>
        static LayoutControl()
        {
        }


        /// <summary>
        /// Initializes a new instance of the <see cref="LayoutControl"/> class.
        /// </summary>
        public LayoutControl() : base()
        {
            this.IsTriggerObjectPressed = false;
            if (Application.Current != null)
            {
                PackageVersion = Application.Current.GetType().Assembly.GetName().Version.ToString();
            }

            this.OwnVideoSurfaceId = string.Empty;
            this.DoCreate();

            Background = new SolidColorBrush(Colors.Transparent);
        }

        public LayoutControl(string layoutName, string layoutDescription, Size layoutSize, ProcessMode processMode, LayoutType layoutType, BaseProcess process, Brush insideBackground, Brush outsideBackground, bool layoutShowToList)
            : base(layoutSize)
        {
            this.IsTriggerObjectPressed = false;
            PackageVersion = GetPackageVersion();
            Process = process;
            Name = layoutName;
            LayoutType = layoutType;
            this.LayoutDescription = layoutDescription;
            ProcessMode = processMode;
            this.InsideBackground = insideBackground;
            this.OutsideBackground = outsideBackground;
            this.LayoutShowToList = layoutShowToList;

            this.OwnVideoSurfaceId = string.Empty;
            this.DoCreate();

            Background = new SolidColorBrush(Colors.Transparent);
        }

        public void SetOwnVideoSurfaceId(string id)
        {
            this.OwnVideoSurfaceId = id;
        }

        private static void OnChangeInsideBackground(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var layoutControl = d as LayoutControl;

            if (layoutControl == null)
            {
                return;
            }

            layoutControl.InsideCanvas.Background = (Brush)e.NewValue;
        }

        private static void OnChangeOutsideBackground(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var layoutControl = d as LayoutControl;

            if (layoutControl == null)
            {
                return;
            }

            layoutControl.OutsideCanvas.Background = (Brush)e.NewValue;
        }

        private static string GetPackageVersion()
        {
            if (Application.Current == null)
            {
                return "0.0.0.0";
            }

            return Application.Current.GetType().Assembly.GetName().Version.ToString();
        }

        /// <summary>
        /// The action event handler.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="actionEventArgs">
        /// The action event args.
        /// </param>
        public delegate void ActionEventHandler(object sender, ActionEventArgs actionEventArgs);

        /// <summary>
        /// The e focused layout changed.
        /// </summary>
        public event FocusedLayoutChangedRoutedEventHandler eFocusedLayoutChanged
        {
            add
            {
                AddHandler(eFocusedLayoutChangedEvent, value);
            }

            remove
            {
                RemoveHandler(eFocusedLayoutChangedEvent, value);
            }
        }

        /// <summary>
        /// The e selected elements changed.
        /// </summary>
        public event SelectedElementsChangedRoutedEventHandler eSelectedElementsChanged
        {
            add
            {
                AddHandler(eSelectedElementsChangedEvent, value);
            }

            remove
            {
                RemoveHandler(eSelectedElementsChangedEvent, value);
            }
        }

        /// <summary>
        /// The e setting rect.
        /// </summary>
        public event RectRoutedEventHandler eSettingRect
        {
            add
            {
                AddHandler(eSettingRectEvent, value);
            }

            remove
            {
                RemoveHandler(eSettingRectEvent, value);
            }
        }

        /// <summary>
        /// The e trigger action event.
        /// </summary>
        public event ActionEventHandler eTriggerActionEvent;

        /// <summary>
        /// The e zoom in.
        /// </summary>
        public event RectRoutedEventHandler eZoomIn
        {
            add
            {
                AddHandler(eZoomInEvent, value);
            }

            remove
            {
                RemoveHandler(eZoomInEvent, value);
            }
        }

        /// <summary>
        /// The e zoom out.
        /// </summary>
        public event RectRoutedEventHandler eZoomOut
        {
            add
            {
                AddHandler(eZoomOutEvent, value);
            }

            remove
            {
                RemoveHandler(eZoomOutEvent, value);
            }
        }

        public bool IsFocusedLayout
        {
            get
            {
                return _isFocusedLayout;
            }
            set
            {
                _isFocusedLayout = value;

                if (_isFocusedLayout)
                {
                    SetSelectedRectangle(FrameType.Focused);
                }
                else
                {
                    SetSelectedRectangle(FrameType.Normal);
                }
            }
        }

        /// <summary>
        /// Gets or sets LayoutType.
        /// </summary>
        public LayoutType LayoutType
        {
            get
            {
                return (LayoutType)GetValue(LayoutTypeProperty);
            }

            set
            {
                SetValue(LayoutTypeProperty, value);
            }
        }

        /// <summary>
        /// Gets or sets LayoutDescription.
        /// </summary>
        public string LayoutDescription
        {
            get
            {
                return (string)GetValue(LayoutDescriptionProperty);
            }

            set
            {
                SetValue(LayoutDescriptionProperty, value);
            }
        }

        /// <summary>
        /// Gets or sets LayoutShowToList
        /// </summary>
        public bool LayoutShowToList
        {
            get
            {
                return (bool)GetValue(LayoutShowToListProperty);
            }

            set
            {
                SetValue(LayoutShowToListProperty, value);
            }
        }

        /// <summary>
        /// Gets or sets InsideBackground.
        /// </summary>
        public Brush InsideBackground
        {
            get
            {
                return (Brush)GetValue(InsideBackgroundProperty);
            }

            set
            {
                SetValue(InsideBackgroundProperty, value);
            }
        }

        /// <summary>
        /// Gets or sets OutsideBackground.
        /// </summary>
        public Brush OutsideBackground
        {
            get
            {
                return (Brush)GetValue(OutsideBackgroundProperty);
            }

            set
            {
                SetValue(OutsideBackgroundProperty, value);
            }
        }

        /// <summary>
        /// Layout Loading 이 끝난 뒤에 실행 되는 ActionProcessItem 의 Guid.
        /// </summary>
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public Guid LoadedActionGuid { get; set; }

        /// <summary>
        /// Gets ProcessMode.
        /// </summary>
        public ProcessMode ProcessMode { get; private set; }

        /// <summary>
        /// Gets ThumbnailBrush.
        /// </summary>
        public Brush ThumbnailBrush
        {
            get
            {
                return new VisualBrush(this);
            }
        }

        internal bool IsTriggerObjectPressed { get; set; }

        internal Process MainProcess { get; private set; }

        /// <summary>
        /// 자기 자신을 복제한다.
        /// </summary>
        /// <param name="isMaintainGuid">
        /// isMaintainGuid가 True이면 현재 Guid를 유지한다.
        /// </param>
        /// <returns>
        /// 복제한 객체를 반환한다.
        /// </returns>
        public LayoutControl Clone(bool isMaintainGuid)
        {
            var result = LayoutUtils.LoadFromXamlData(GetXamlString());

            if (!isMaintainGuid)
            {
                result.SyncGUID = Guid.NewGuid();
            }

            return result;
        }

        /// <summary>
        /// The clone.
        /// </summary>
        /// <returns>
        /// </returns>
        public LayoutControl Clone()
        {
            return Clone(false);
        }



        /// <summary>
        /// 현재 자신의 정보를 Xaml로 반환한다.
        /// </summary>
        /// <returns>
        /// The get xaml string.
        /// </returns>
        public string GetXamlString()
        {
            return InnoXamlWriter.GetXamlString(this);
        }

        /// <summary>
        /// The origin resize.
        /// </summary>
        /// <param name="newSize">
        /// The new size.
        /// </param>
        public override void OriginResize(Size newSize)
        {
            if (LayoutType == LayoutType.LayoutControl)
            {
                Width = newSize.Width;
                Height = newSize.Height;
            }

            RedrawLayoutFrame();

            base.OriginResize(newSize);
        }

        /// <summary>
        /// The set process.
        /// </summary>
        /// <param name="processMode">
        /// The process mode.
        /// </param>
        public void SetProcess(ProcessMode processMode)
        {
            ProcessMode = processMode;

            if (MainProcess != null)
            {
                _elementManager.ClearElementEventHandler();
                _elementManager.ClearElementEventHandler(this);

                MainProcess = null;
            }

            if (IsMouseCaptured)
            {
                ReleaseMouseCapture();
            }

            switch (processMode)
            {
                case ProcessMode.Edit:
                    MainProcess = new EditProcess(this);
                    break;

                case ProcessMode.Play:
                    MainProcess = new PlayProcess(this);
                    break;

                case ProcessMode.Record:
                    MainProcess = new RecordProcess(this);
                    break;
            }

            _elementManager.AssignElementEventHandler();
            _elementManager.AssignElementEventHandler(this);

            // 하위 Layout Mode 설정
            _elementManager.SetProcess(ProcessMode);
        }

        public override int GetObjectCount()
        {
            return _elementManager.GetObjectCount();
        }

        public void ActionRaise(object sender, ActionEventArgs args)
        {
            if (eTriggerActionEvent != null)
            {
                eTriggerActionEvent(sender, args);
            }
        }

        public void ChildrenSetFocused(bool focused)
        {
            IsFocusedLayout = focused;
            _elementManager.ChildrenSetFocused(focused);
        }


        /// <summary>
        /// The do dispose.
        /// </summary>
        /// <param name="isManage">
        /// The is manage.
        /// </param>
        protected override void DoDispose(bool isManage)
        {
            if (this.Dispatcher.CheckAccess())
            {
                this.Internal_DoDispose();
            }
            else
            {
                this.Dispatcher.Invoke(
                    new Action(
                        () =>
                        {
                            this.Internal_DoDispose();
                        }));
            }

            base.DoDispose(isManage);
        }

        private void Internal_DoDispose()
        {
            if (_elementManager != null)
            {
                _elementManager.Clear();
            }

            for (var i = Children.Count - 1; i >= 0; i--)
            {
                var element = this.Children[i];
                if (element is IDisposable)
                {
                    (element as IDisposable).Dispose();
                }
            }

            Children.Clear();

            // 생성 시 만들어졌던 VideoSurface도 함께 Dispose 한다.
            if (!string.IsNullOrWhiteSpace(this.OwnVideoSurfaceId))
            {
                VideoSurfaceManager.Instance.DisposeVideoSurface(this.OwnVideoSurfaceId);
                this.OwnVideoSurfaceId = string.Empty;
            }
        }

        /// <summary>
        /// The measure override.
        /// </summary>
        /// <param name="constraint">
        /// The constraint.
        /// </param>
        /// <returns>
        /// </returns>
        protected override Size MeasureOverride(Size constraint)
        {
            if (_elementManager != null)
            {
                _elementManager.AdjustElementLocation();
            }
            return base.MeasureOverride(constraint);
        }

        /// <summary>
        /// The on render size changed.
        /// </summary>
        /// <param name="sizeInfo">
        /// The size info.
        /// </param>
        protected override void OnRenderSizeChanged(SizeChangedInfo sizeInfo)
        {
            base.OnRenderSizeChanged(sizeInfo);

            RedrawLayoutFrame();
        }

        public Action ExcuteActionAfterLoaded { get; set; }

        private void LayoutControl_Loaded(object sender, RoutedEventArgs e)
        {
            var layoutControl = sender as LayoutControl;

            if (layoutControl != null)
            {
                layoutControl.Loaded -= LayoutControl_Loaded;
            }

            RedrawLayoutFrame();

            _elementManager.AdjustElementLocation();

            // iEditor에서는 카메라를 실제로 사용하지 않는다.
            if (!Public.IsEditingType())
            {
				var elements = this.GetElements();
                foreach (var element in elements)
                {
                    if (element is CameraControl)
                    {
                        if (CameraManager.CameraToLayoutGuid.ContainsKey((element as CameraControl).OriginGUID) == false)
                        {
                            CameraManager.CameraToLayoutGuid.Add((element as CameraControl).OriginGUID, this.OriginGUID);
                        }
                    }
                }

                if (this.ExcuteActionAfterLoaded != null)
                {
                    this.Dispatcher.BeginInvoke(new Action(() =>
                    {
                        ExcuteActionAfterLoaded.Invoke();
                        ExcuteActionAfterLoaded = null;
                    }));
                }

                CameraManager.Instance.RedrawCameraControlAsync(false);
            }
        }

        private void DoCreate()
        {
            _elementManager = new ElementManager(this);

            if (Public.IsEditingType())
                ProcessMode = ProcessMode.Edit;
            else
                ProcessMode = ProcessMode.Play;

            SetProcess(ProcessMode);
            Loaded += LayoutControl_Loaded;
        }

        public void AddElement(FrameworkElement element)
        {
            this._elementManager.AddElement(element);
        }

        public void RemoveElement(FrameworkElement element)
        {
            this._elementManager.RemoveElement(element);
        }

        public void ArrangeElement(FrameworkElement targetElement, ElementArrangeType arrangeType)
        {
            this._elementManager.ArrangeElement(targetElement, arrangeType);
        }

        public void BringToFront(FrameworkElement element)
        {
            this._elementManager.BringToFront(element);
        }

        public void BringForward(FrameworkElement element)
        {
            this._elementManager.BringForward(element);
        }

        public void InsertElement(int index, FrameworkElement element)
        {
            this._elementManager.InsertElement(index, element);
        }

        public void RemoveElements(Type type, bool includeSubLayout)
        {
            this._elementManager.RemoveElements(type, includeSubLayout);
        }

        public bool IsContain(FrameworkElement element)
        {
            return this._elementManager.Elements.Contains(element);
        }

        public int GetZIndex(FrameworkElement element)
        {
            return this._elementManager.GetZIndex(element);
        }

        public void SetZIndex(FrameworkElement element, int zIndex)
        {
            this._elementManager.SetZIndex(element, zIndex);
        }

        public void SendToBack(FrameworkElement element)
        {
            this._elementManager.SendToBack(element);
        }

        public void SendBackward(FrameworkElement element)
        {
            this._elementManager.SendBackward(element);
        }

        public int GetMaxZIndex()
        {
            return this._elementManager.GetMaxZIndex();
        }

        public List<FrameworkElement> GetElements()
        {
            return this._elementManager.Elements;
        }

        public int GetElementsCounts()
        {
            return this._elementManager.GetElementsCounts();
        }

        public FrameworkElement GetFirstElement()
        {
            return this._elementManager.GetFirstElement();
        }

        public void UpdateLayoutSizeChangeForVisibility(LayoutControl layout)
        {
            this._elementManager.UpdateLayoutSizeChangeForVisibility(layout);
        }

        public void UpdateLayoutSizeChange(LayoutControl layout)
        {
            this._elementManager.UpdateLayoutSizeChange(layout);
        }

        public UIElementCollection GetUnlockedElements()
        {
            return this._elementManager.UnLockedElements;
        }

        public UIElementCollection GetLockedElements()
        {
            return this._elementManager.LockedElements;
        }

        public void AllocationCanvas(FrameworkElement element)
        {
            this._elementManager.AllocationCanvas(element);
        }

        public Point GetMousePosition()
        {
            return Mouse.GetPosition(OutsideCanvas);
        }

        public void SetCurrentZoomRatio(double value)
        {
            if (value >= this.ZoomRatio)
            {
                this.ZoomIn(value / this.ZoomRatio, double.NaN);
            }
            else
            {
                this.ZoomOut(this.ZoomRatio / value, double.NaN);
            }

        }
        
        /// <summary>
        /// 생성된 Layout에 VideoSurface를 만들어 추가하고 Layout의 자식 중 CameraControl을 찾아 VideoSurface의 ID를 할당한다.
        /// 재귀 호출로 중첩 Layout을 모두 찾아 적용한다.
        /// </summary>
        /// <param name="control">The target LayoutControl.</param>
        static public void AddVideoSurface(object control)
        {
            if (!(control is DependencyObject))
            {
                return;
            }

            var videoSurfaceId = string.Empty;

            if (control is LayoutControl)
            {
                var layoutControl = control as LayoutControl;

                // 레이아웃에 카메라 컨트롤이 한개라도 있으면 VideoSurface를 생성한다.
                foreach (var item in layoutControl.Children)
                {                    
                    if (item is CameraControl)
                    {                        
                        videoSurfaceId = VideoSurfaceManager.Instance.AddLayoutVideoSurface(layoutControl.OriginGUID.ToString());
                        if(!string.IsNullOrWhiteSpace(videoSurfaceId))
                        {
                            layoutControl.SetOwnVideoSurfaceId(videoSurfaceId);
                            break;
                        }
                    }
                }
            }

            foreach (var child in LogicalTreeHelper.GetChildren(control as DependencyObject))
            {
                if (child is OutsideViewbox ||
                    child is InnoCanvas ||
                    child is InsideViewbox ||
                    child is LayoutControl)
                {
                    AddVideoSurface(child);
                }

                if (child is CameraControl)
                {
                    var camera = child as CameraControl;
                    if (!string.IsNullOrWhiteSpace(videoSurfaceId))
                        camera.VideoSurfaceId = videoSurfaceId;
                }
            }
        }
    }
}