// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LayoutUtils.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   The element property info.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.DLLs.LayoutControls
{
    using System;
    using System.Collections.Generic;
    using System.Windows;
    using System.Windows.Media;
    using Commons.BaseControls;
    using Commons.GuidManagers;
    using Commons.Utils;
    using Commons.Xamls;

    /// <summary>
    /// The new guid target.
    /// </summary>
    public enum GuidType
    {
        /// <summary>
        /// The sync guid.
        /// </summary>
        SyncGuid,

        /// <summary>
        /// The all except event guid.
        /// </summary>
        AllExceptEventGuid,

        /// <summary>
        /// The all except sync guid event guid book mark guid.
        /// </summary>
        AllExceptSyncGuidEventGuidBookMarkGuid
    }

    /// <summary>
    /// The element property info.
    /// </summary>
    internal struct ElementPropertyInfo
    {
        #region Constants and Fields

        internal FrameworkElement OwnerElement;

        internal DependencyProperty Property;

        #endregion
    }

    /// <summary>
    /// The element property info group.
    /// </summary>
    internal class ElementPropertyInfoGroup
    {
        #region Constants and Fields

        internal List<ElementPropertyInfo> InfoList = new List<ElementPropertyInfo>();

        internal object Value;

        #endregion
    }

    /// <summary>
    /// The layout utils.
    /// </summary>
    public class LayoutUtils
    {
        #region Public Methods

        /// <summary>
        /// iCanvas의 Child Elements들의 Name이 parentLayout의 Child Elements들의 Name과 중복되지 않도록 체크.
        /// </summary>
        /// <param name="iCanvas">
        /// </param>
        /// <param name="parentLayout">
        /// </param>
        /// <param name="type">
        /// </param>
        public static void CheckCanvasDuplicateName(LayoutControl iCanvas, LayoutControl parentLayout, RenameType type)
        {
            // 1. parentLayout의 Element목록을 가져온다.
            var parentElementList = GetAllChildObjectList(parentLayout);
            parentElementList.Add(parentLayout);

            // 2. iCanvas의 Element목록을 가져온다.
            var canvasElementList = GetAllChildObjectList(iCanvas);
            canvasElementList.Add(iCanvas);

            // 3. 이미 iCanvas가 parentLayout의 Children으로 추가되었을 경우 iCanvas와 iCanvas의 Children은 목록에서 제거한다.
            parentElementList.RemoveAll(
                delegate(BaseControl element) { return canvasElementList.Contains(element); });

            // 4. 검색의 기준이 되는 이름 목록 생성
            var nameList = new HashSet<string>();
            foreach (var parentElement in parentElementList)
            {
                string name = parentElement.Name;
                if (string.IsNullOrEmpty(name))
                {
                    continue;
                }

                nameList.Add(name);
            }

            // 5. iCanvas 및 iCanvas의 Children의 이름 중복체크
            foreach (FrameworkElement canvasElement in canvasElementList)
            {
                CheckDuplicateName(canvasElement, nameList, type);
            }
        }

        /// <summary>
        /// The check element duplicate name.
        /// </summary>
        /// <param name="element">
        /// The element.
        /// </param>
        /// <param name="parentLayout">
        /// The parent layout.
        /// </param>
        /// <param name="type">
        /// The type.
        /// </param>
        public static void CheckElementDuplicateName(
            FrameworkElement element, LayoutControl parentLayout, RenameType type)
        {
            // 1. parentLayout의 Element목록을 가져온다.
            var parentElementList = GetAllChildObjectList(parentLayout);
            parentElementList.Add(parentLayout);

            // 2. 이미 element가 parentLayout의 Children으로 추가되었을 경우 목록에서 제거한다.
            parentElementList.RemoveAll(delegate(BaseControl curElement) { return curElement == element; });

            // 3. 검색의 기준이 되는 이름 목록 생성
            var nameList = new HashSet<string>();
            foreach (var parentElement in parentElementList)
            {
                string name = parentElement.Name;
                if (string.IsNullOrEmpty(name))
                {
                    continue;
                }

                nameList.Add(name);
            }

            // 4. element의 이름 중복체크
            CheckDuplicateName(element, nameList, type);
        }

        public static HashSet<string> GetAllChildNames(BaseLayoutControl baseLayout, FrameworkElement exceptionElement)
        {
            var result = new HashSet<string>();

            var baseLayoutControl = baseLayout as IBaseLayoutControl;
            if (baseLayoutControl == null)
            {
                return result;
            }

            foreach (var element in baseLayoutControl.GetElements())
            {
                if (element == exceptionElement)
                {
                    continue;
                }

                result.Add(element.Name);

                if (element is LayoutControl)
                {
                    result.UnionWith(GetAllChildNames(element as LayoutControl, exceptionElement));
                }
            }

            return result;
        }

        /// <summary>
        /// 중첩구조의 모든 자식들의 목록을 반환한다. (LogicalTreeHelper이용).
        /// </summary>
        /// <param name="obj">
        /// The obj.
        /// </param>
        /// <returns>
        /// </returns>
        public static List<BaseControl> GetAllChildObjectList(object obj)
        {
            List<BaseControl> result = GetAllChildObjectList_internal(obj);

            // 자기 자신은 목록에서 삭제한다.
            if (obj is BaseControl)
            {
                var element = obj as BaseControl;
                if (result.Contains(element))
                {
                    result.Remove(element);
                }
            }

            return result;
        }

        /// <summary>
        /// 중첩구조의 모든 FrameworkElement들의 목록을 반환한다. (LogicalTreeHelper이용).
        /// </summary>
        /// <param name="obj">
        /// The obj.
        /// </param>
        /// <returns>
        /// </returns>
        public static List<FrameworkElement> GetAllFrameworkElementList_LogicalTree(object obj)
        {
            List<FrameworkElement> result = GetAllFrameworkElementList_LogicalTree_Internal(obj);

            // 자기 자신은 목록에서 삭제한다.
            if (obj is FrameworkElement)
            {
                var element = obj as FrameworkElement;
                if (result.Contains(element))
                {
                    result.Remove(element);
                }
            }

            return result;
        }

        /// <summary>
        /// 중첩구조의 모든 FrameworkElement들의 목록을 반환한다. (VisualTreeHelper이용).
        /// </summary>
        /// <param name="obj">
        /// The obj.
        /// </param>
        /// <returns>
        /// </returns>
        public static List<FrameworkElement> GetAllFrameworkElementList_VisualTree(object obj)
        {
            List<FrameworkElement> result = GetAllFrameworkElementList_VisualTree_Internal(obj);

            // 자기 자신은 목록에서 삭제한다.
            if (obj is FrameworkElement)
            {
                var element = obj as FrameworkElement;
                if (result.Contains(element))
                {
                    result.Remove(element);
                }
            }

            return result;
        }

        /// <summary>
        /// 중첩구조의 모든 자식들 중에 MirrorControlM의 목록을 반환한다. (LogicalTreeHelper이용).
        /// </summary>
        /// <param name="obj">
        /// The obj.
        /// </param>
        /// <returns>
        /// </returns>
        public static List<BaseControl> GetAllChildMirrorObjectList(object obj)
        {
            List<BaseControl> result = GetAllChildMirrorObjectList_internal(obj);

            // 자기 자신은 목록에서 삭제한다.
            if (obj is BaseControl)
            {
                var element = obj as BaseControl;
                if (result.Contains(element))
                {
                    result.Remove(element);
                }
            }

            return result;
        }

        /// <summary>
        /// 중첩구조의 모든 자식들의 이름을 반환한다. (LogicalTreeHelper이용).
        /// </summary>
        /// <param name="obj">
        /// The obj.
        /// </param>
        /// <returns>
        /// </returns>
        public static List<string> GetAllChildObjectNameList(object obj)
        {
            List<string> result = GetAllChildObjectNameList_internal(obj);

            // 자기 자신은 목록에서 삭제한다.
            if (obj is FrameworkElement)
            {
                var element = obj as FrameworkElement;
                if (result.Contains(element.Name))
                {
                    result.Remove(element.Name);
                }
            }

            return result;
        }



        /// <summary>
        /// 조상 LayoutControl의 목록을 가져온다.
        /// </summary>
        /// <param name="layoutControl">
        /// </param>
        /// <returns>
        /// </returns>
        public static List<LayoutControl> GetAncestryLayoutControls(LayoutControl layoutControl)
        {
            var result = new List<LayoutControl> {layoutControl};

            var baseLayoutControl = layoutControl.ParentBaseLayoutControl;
            if (baseLayoutControl != null && baseLayoutControl is LayoutControl)
            {
                result.AddRange(GetAncestryLayoutControls(baseLayoutControl as LayoutControl));
            }

            return result;
        }
        
        // InnoCavas 등은 BaseControl 이 아니므로, FrameworkElement를 가지고 찾아야 한다. (2011.08.25)
        public static BaseLayoutControl GetParentBaseLayoutControl(FrameworkElement element)
        {
            if (element == null)
            {
                return null;
            }

            return FindBaseLayoutControlAtParent(element);
        }

        private static BaseLayoutControl FindBaseLayoutControlAtParent(FrameworkElement element)
        {
            if (element == null)
            {
                return null;
            }

            if (element.Parent is BaseLayoutControl)
            {
                return element.Parent as BaseLayoutControl;
            }

            return FindBaseLayoutControlAtParent(element.Parent as FrameworkElement);
        }



        /// <summary>
        /// 중첩구조의 모든 자식 Layout들의 목록을 반환한다. (LogicalTreeHelper이용).
        /// </summary>
        /// <param name="obj">
        /// The obj.
        /// </param>
        /// <returns>
        /// </returns>
        public static List<LayoutControl> GetProgenyLayoutControls(object obj)
        {
            List<LayoutControl> result = GetProgenyLayoutControls_internal(obj);

            // 자기 자신은 목록에서 삭제한다.
            if (obj is LayoutControl)
            {
                var selfLayoutControl = obj as LayoutControl;
                if (result.Contains(selfLayoutControl))
                {
                    result.Remove(selfLayoutControl);
                }
            }

            return result;
        }

        /// <summary>
        /// GroundLayout을 반환한다.
        /// </summary>
        /// <param name="element">
        /// The element.
        /// </param>
        /// <returns>
        /// </returns>
        public static LayoutControl GetRootLayoutControl(FrameworkElement element)
        {
            if (element == null)
            {
                return null;
            }

            return FindRootLayoutControlAtParent(element);
        }

        /// <summary>
        /// layoutControl의 Element들의 Name중에 Unique한 Name를 반환한다.
        /// </summary>
        /// <param name="exceptionElement">
        /// The exception Element.
        /// </param>
        /// <param name="desireName">
        /// 사용되기를 기대하는 Name.
        /// </param>
        /// <param name="layoutControl">
        /// Name Duplication을 Check할 기준 Layout.
        /// </param>
        /// <param name="baseName">
        /// Unique한 Name을 생성할때 사용할 기준 Name.
        /// </param>
        /// <returns>
        /// Unique Name.
        /// </returns>
        public static string GetUniqueName(
            FrameworkElement exceptionElement, string desireName, BaseLayoutControl baseLayoutControl, string baseName)
        {
            if (string.IsNullOrEmpty(desireName))
            {
                return desireName;
            }

            HashSet<string> elementNameList = GetAllChildNames(baseLayoutControl, exceptionElement);
            int count = 0;
            while (elementNameList.Contains(desireName))
            {
                desireName = baseName + (++count).ToString();
            }

            return desireName;
        }

        /// <summary>
        /// layoutControl의 Element들의 Name중에 Unique한 Name를 반환한다.
        /// </summary>
        /// <param name="guid">
        /// The exception Element.
        /// </param>
        /// <param name="desireName">
        /// 사용되기를 기대하는 Name.
        /// </param>
        /// <param name="layoutControl">
        /// Name Duplication을 Check할 기준 Layout.
        /// </param>
        /// <returns>
        /// Unique Name.
        /// </returns>
        public static bool IsUniqName(Guid guid, string desireName, LayoutControl layoutControl)
        {
            var exceptionElement = (FrameworkElement)GuidManager.GetSyncObject(guid);
            if (string.IsNullOrEmpty(desireName))
            {
                return false;
            }

            var elementNameList = GetAllChildNames(layoutControl, exceptionElement);

            while (elementNameList.Contains(desireName))
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// The is rect selected.
        /// </summary>
        /// <param name="resultRect">
        /// The result rect.
        /// </param>
        /// <param name="childRect">
        /// The child rect.
        /// </param>
        /// <returns>
        /// The is rect selected.
        /// </returns>
        public static bool IsRectSelected(Rect resultRect, Rect childRect)
        {
            // 1. Selector에 100% 다 포함됐으면...
            return resultRect.Contains(childRect);

            // 2. Selector에 일부분이라도 포함됐으면...
             //return resultRect.IntersectsWith(childRect);

            // 3. Selector에 50%이상 포함됐으면...
            // Rect rt = Rect.Intersect(resultRect, childRect);
            // return (!rt.IsEmpty && (childRect.Width * childRect.Height / 2) < (rt.Width * rt.Height));
        }

        /// <summary>
        /// The load from xaml data.
        /// </summary>
        /// <param name="xamlData">
        /// The xaml data.
        /// </param>
        /// <returns>
        /// </returns>
        public static LayoutControl LoadFromXamlData(string xamlData)
        {
            LayoutControl layoutControl;

            try
            {
                layoutControl = (LayoutControl)InnoXamlReader.Load(xamlData);
            }
            catch
            {
                layoutControl = null;
            }

            return layoutControl;
        }

        /// <summary>
        /// 레이아웃 내 유니크한 Name을 반환한다. 
        /// </summary>
        /// <param name="obj">
        /// </param>
        /// <param name="elementType">
        /// </param>
        /// <returns>
        /// The make unique name.
        /// </returns>
        public static string MakeUniqueName(object obj, string elementType)
        {
            var nameList = new List<string>();

            int nameAppend = 1;

            nameList.AddRange(GetAllChildObjectNameList(obj));

            while (nameList.Contains(elementType + nameAppend.ToString()))
            {
                nameAppend++;
            }

            return elementType + nameAppend.ToString();
        }

        /// <summary>
        /// The make unique name_ copy.
        /// </summary>
        /// <param name="obj">
        /// The obj.
        /// </param>
        /// <param name="elementName">
        /// The element name.
        /// </param>
        /// <returns>
        /// The make unique name_ copy.
        /// </returns>
        public static string MakeUniqueName_Copy(object obj, string elementName)
        {
            var nameList = new List<string>();

            int nameAppend = 1;
            string copyAppend = string.Empty;

            nameList.AddRange(GetAllChildObjectNameList(obj));

            while (nameList.Contains(elementName + copyAppend))
            {
                if (string.IsNullOrEmpty(copyAppend))
                {
                    copyAppend += "_Copy";
                }
                else
                {
                    copyAppend = "_Copy" + nameAppend.ToString();
                    nameAppend++;
                }
            }

            return elementName + copyAppend;
        }

        /// <summary>
        /// 파라미터로 넘어온 elementList의 SyncGUID를 새로 생성합니다.
        /// </summary>
        /// <param name="elementList">
        /// The element list.
        /// </param>
        /// <param name="type">
        /// The target.
        /// </param>
        /// <returns>
        /// 새로운 SyncGuid를 가지고 있는 엘리먼트 목록을 반환합니다.
        /// NewGuid는 SyncGuid와 동일합니다.
        /// OldGuid는 이전 SyncGuid입니다.
        /// </returns>
        public static RenewElementGuidInfoList RenewGUID(List<BaseControl> elementList, GuidType type)
        {
            var groupList = new List<ElementPropertyInfoGroup>();

            foreach (var element in elementList)
            {
                GetElementGuidPropertyList(element, groupList);
            }

            return SetNewGuid(groupList, type);
        }

        /// <summary>
        /// The renew guid.
        /// </summary>
        /// <param name="layout">
        /// The layout.
        /// </param>
        /// <param name="type">
        /// The target.
        /// </param>
        /// <returns>
        /// </returns>
        public static RenewElementGuidInfoList RenewGUID(BaseLayoutControl layout, GuidType type)
        {
            var groupList = new List<ElementPropertyInfoGroup>();

            GetLayoutGuidList(layout, groupList);
            return SetNewGuid(groupList, type);
        }

        #endregion

        #region Methods

        private static void CheckDuplicateName(FrameworkElement element, HashSet<string> nameList, RenameType type)
        {
            string curName = element.Name;

            if (string.IsNullOrEmpty(curName))
            {
                return;
            }

            string baseName = string.Empty;
            switch (type)
            {
                case RenameType.CopyNumber:
                    baseName = curName + "_copy";
                    break;

                case RenameType.TypeName:
                    baseName = InnoElementNameUtil.GetControlTypeName(element);
                    break;
            }

            int count = 0;
            while (nameList.Contains(curName))
            {
                curName = baseName + (++count).ToString();
            }

            if (count > 0)
            {
                element.Name = curName;
            }

            nameList.Add(curName);
        }

        private static ElementPropertyInfoGroup FindExistsGuidPropertyByValue(
            List<ElementPropertyInfoGroup> list, object value)
        {
            // 같은 value가 있으면 리턴
            foreach (ElementPropertyInfoGroup group in list)
            {
                if (InnoPropertyUtil.IsSameObjectValue(group.Value, value))
                {
                    return group;
                }
            }

            return null;
        }

        private static LayoutControl FindRootLayoutControlAtParent(FrameworkElement element)
        {
            LayoutControl layoutControl;

            if (element == null)
            {
                layoutControl = null;
            }
            else if (element is LayoutControl && (element as LayoutControl).LayoutType == LayoutType.LayoutControl)
            {
                layoutControl = element as LayoutControl;
            }
            else if (element.Parent is LayoutControl &&
                     (element.Parent as LayoutControl).LayoutType == LayoutType.LayoutControl)
            {
                layoutControl = element.Parent as LayoutControl;
            }
            else
            {
                layoutControl = FindRootLayoutControlAtParent(element.Parent as FrameworkElement);
            }

            return layoutControl;
        }

        private static List<BaseControl> GetAllChildObjectList_internal(object obj)
        {
            var result = new List<BaseControl>();

            if (!(obj is DependencyObject))
            {
                return result;
            }

            if (obj is BaseControl)
            {
                result.Add(obj as BaseControl);
            }

            foreach (object child in LogicalTreeHelper.GetChildren(obj as DependencyObject))
            {
                result.AddRange(GetAllChildObjectList_internal(child));
            }

            return result;
        }

        private static List<FrameworkElement> GetAllFrameworkElementList_LogicalTree_Internal(object obj)
        {
            var result = new List<FrameworkElement>();

            if (!(obj is DependencyObject))
            {
                return result;
            }

            if (obj is FrameworkElement)
            {
                result.Add(obj as FrameworkElement);
            }

            foreach (object child in LogicalTreeHelper.GetChildren(obj as DependencyObject))
            {
                result.AddRange(GetAllFrameworkElementList_LogicalTree_Internal(child));
            }

            return result;
        }

        private static List<FrameworkElement> GetAllFrameworkElementList_VisualTree_Internal(object obj)
        {
            var result = new List<FrameworkElement>();

            if (!(obj is DependencyObject))
            {
                return result;
            }

            if (obj is FrameworkElement)
            {
                result.Add(obj as FrameworkElement);
            }

            int count = VisualTreeHelper.GetChildrenCount(obj as DependencyObject);
            for (int i = 0; i < count; i++)
            {
                DependencyObject child = VisualTreeHelper.GetChild(obj as DependencyObject, i);
                result.AddRange(GetAllFrameworkElementList_VisualTree_Internal(child));
            }

            return result;
        }

        private static List<BaseControl> GetAllChildMirrorObjectList_internal(object obj)
        {
            var result = new List<BaseControl>();

            if (!(obj is DependencyObject))
            {
                return result;
            }

            foreach (object child in LogicalTreeHelper.GetChildren(obj as DependencyObject))
            {
                result.AddRange(GetAllChildMirrorObjectList_internal(child));
            }

            return result;
        }

        private static List<string> GetAllChildObjectNameList_internal(object obj)
        {
            var result = new List<string>();

            if (!(obj is DependencyObject))
            {
                return result;
            }

            if (obj is BaseControl)
            {
                result.Add((obj as FrameworkElement).Name);
            }

            foreach (object child in LogicalTreeHelper.GetChildren(obj as DependencyObject))
            {
                result.AddRange(GetAllChildObjectNameList_internal(child));
            }

            return result;
        }

        private static void GetElementGuidPropertyList(FrameworkElement element, List<ElementPropertyInfoGroup> list)
        {
            var dpList = InnoPropertyUtil.GetDependencyPropertyList(element, typeof(Guid));
            foreach (var dp in dpList)
            {
                var value = element.GetValue(dp);
                var group = FindExistsGuidPropertyByValue(list, value);
                if (group == null)
                {
                    group = new ElementPropertyInfoGroup();
                    list.Add(group);
                }

                group.Value = value;

                var info = new ElementPropertyInfo
                               {
                                   OwnerElement = element, Property = dp
                               };
                group.InfoList.Add(info);
            }
        }

        private static void GetLayoutChildElementGuidPropertyList(
            BaseLayoutControl layout, List<ElementPropertyInfoGroup> list)
        {
            foreach (var element in GetAllChildObjectList(layout))
            {
                if (element is BaseLayoutControl)
                {
                    GetLayoutGuidList(element as BaseLayoutControl, list);
                }
                else
                {
                    GetElementGuidPropertyList(element, list);
                }
            }
        }

        private static void GetLayoutGuidList(BaseLayoutControl layout, List<ElementPropertyInfoGroup> list)
        {
            GetElementGuidPropertyList(layout, list);
            GetLayoutChildElementGuidPropertyList(layout, list);
        }

        private static List<LayoutControl> GetProgenyLayoutControls_internal(object obj)
        {
            var result = new List<LayoutControl>();

            if (!(obj is DependencyObject))
            {
                return result;
            }

            if (obj is LayoutControl)
            {
                result.Add(obj as LayoutControl);
            }

            foreach (object child in LogicalTreeHelper.GetChildren(obj as DependencyObject))
            {
                result.AddRange(GetProgenyLayoutControls_internal(child));
            }

            return result;
        }

        private static void RenewTargetGuid(ElementPropertyInfoGroup group, RenewElementGuidInfoList infoList)
        {
            Guid oldGuid, newGuid;

            oldGuid = (Guid)group.Value;
            group.Value = Guid.NewGuid();
            newGuid = (Guid)group.Value;

            foreach (ElementPropertyInfo info in group.InfoList)
            {
                info.OwnerElement.SetValue(info.Property, group.Value);
                infoList.Add(info.OwnerElement, oldGuid, newGuid);
            }
        }

        /// <summary>
        /// 받은 목록의 Guid 들의 값을 새로 부여한다.
        /// </summary>
        /// <param name="groupList">
        /// Guid 대상 List.
        /// </param>
        /// <param name="type">
        /// The target.
        /// </param>
        /// <returns>
        /// 변경된 Guid 목록.
        /// </returns>
        private static RenewElementGuidInfoList SetNewGuid(IEnumerable<ElementPropertyInfoGroup> groupList, GuidType type)
        {
            var infoList = new RenewElementGuidInfoList();

            // 새로운 GUID를 생성하고 Element에 다시 적용
            foreach (var group in groupList)
            {
                if (group.InfoList.Count <= 0)
                {
                    continue;
                }

                var propName = group.InfoList[0].Property.Name;

                switch (type)
                {
                    case GuidType.SyncGuid:
                        for (var i = group.InfoList.Count - 1; i >= 0; i--)
                        {
                            if (group.InfoList[i].Property.Name != BaseControl.SyncGUIDProperty.Name)
                            {
                                group.InfoList.RemoveAt(i);
                            }
                        }

                        break;
                    case GuidType.AllExceptEventGuid:
                        for (var i = group.InfoList.Count - 1; i >= 0; i--)
                        {
                            if (group.InfoList[i].Property.Name == BaseControl.MouseClickGUIDProperty.Name)
                            {
                                group.InfoList.RemoveAt(i);
                            }
                        }

                        break;
                    case GuidType.AllExceptSyncGuidEventGuidBookMarkGuid:
                        if (propName == BaseControl.SyncGUIDProperty.Name)
                        {
                            continue;
                        }

                        if (propName == BaseControl.MouseClickGUIDProperty.Name)
                        {
                            continue;
                        }

                        for (var i = group.InfoList.Count - 1; i >= 0; i--)
                        {
                            if (group.InfoList[i].Property.Name == BaseControl.MouseClickGUIDProperty.Name ||
                                group.InfoList[i].Property.Name == BaseControl.SyncGUIDProperty.Name)
                            {
                                group.InfoList.RemoveAt(i);
                            }
                        }

                        break;
                }

                RenewTargetGuid(group, infoList);
            }

            return infoList;
        }

        #endregion
    }
}