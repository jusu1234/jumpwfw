﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Process.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   The process.
//
//   최초작업 : gcnam 
//   날짜 : 2008-09 추석무렵
//  
//   기본원칙 : 
//   1. 중첩구조를 제대로 지원하기 위해 Mouse Event 의 전달을 막는다 ( e.Handle = true )
//   2. LayoutControl 또는 iCanvas 도 Element 로 본다. 
//   3. 모든 이벤트는 Element -> Layout 순서로 전달된다. 
//   4. 되도록 처리 로직은 Layout 에서 처리 한다. 
//   5. CaptureMouse 의 사용을 최소화 하며, 되도록이면 부모클래스 ( Process ) 에서 처리한다.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.DLLs.LayoutControls.Processes
{
    using System.Windows;
    using System.Windows.Input;

    /// <summary>
    /// The process.
    /// </summary>
    public class Process
    {
        /// <summary>
        /// Gets or sets _layoutControl.
        /// </summary>
        protected LayoutControl OwnerLayout { get; set; }

        internal virtual void ElementMouseEnter(object sender, MouseEventArgs e)
        {
        }

        internal virtual void ElementMouseLeave(object sender, MouseEventArgs e)
        {
        }

        //internal virtual void ElementMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        //{
        //    var selectedElement = sender as FrameworkElement;
        //    if (selectedElement == null)
        //    {
        //        return;
        //    }

        //    if (selectedElement.IsMouseCaptured)
        //    {
        //        selectedElement.ReleaseMouseCapture();
        //    }

        //    selectedElement.CaptureMouse();
        //}

        //internal virtual void ElementMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        //{
        //    var selectedElement = sender as FrameworkElement;
        //    if (selectedElement == null)
        //    {
        //        return;
        //    }

        //    if (selectedElement.IsMouseCaptured)
        //    {
        //        selectedElement.ReleaseMouseCapture();
        //    }
        //}

        //internal virtual void ElementMouseMove(object sender, MouseEventArgs e)
        //{
        //}

        internal virtual void LayoutControl_MouseEnter(object sender, MouseEventArgs e)
        {
        }

        internal virtual void LayoutControl_MouseLeave(object sender, MouseEventArgs e)
        {
        }

        //internal virtual void LayoutControl_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        //{
        //    if (this.OwnerLayout.IsMouseCaptured)
        //    {
        //        this.OwnerLayout.ReleaseMouseCapture();
        //    }

        //    this.OwnerLayout.CaptureMouse();
        //}

        //internal virtual void LayoutControl_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        //{
        //    if (this.OwnerLayout.IsMouseCaptured)
        //    {
        //        this.OwnerLayout.ReleaseMouseCapture();
        //    }
        //}

        //internal virtual void LayoutControl_MouseMove(object sender, MouseEventArgs e)
        //{
        //}

        internal virtual void LayoutControl_MouseWheel(object sender, MouseWheelEventArgs e)
        {
        }
    }
}