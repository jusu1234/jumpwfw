﻿// -----------------------------------------------------------------------
// <copyright file="RecordingChangePropertyEventArgs.cs" company="Microsoft">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace Innotive.InnoWatch.DLLs
{
    using System;
    using System.Windows;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class RecordingChangePropertyEventArgs : EventArgs
    {
        public DependencyProperty DependencyProperty { get; protected set; }

        public RecordingChangePropertyEventArgs(DependencyProperty dp)
        {
            this.DependencyProperty = dp;
        }
    }
}
