﻿// -----------------------------------------------------------------------
// <copyright file="BaseControlEditorViewModel.cs" company="Microsoft">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace Innotive.InnoWatch.DLLs.BaseControlEditors
{
    using System.ComponentModel;

    using Innotive.InnoWatch.Commons.BaseControls;
    using Innotive.InnoWatch.CommonUtils.Log;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class BaseControlEditorViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged(string propertyName)
        {
            if (propertyName != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        public ISelectedElements SelectedElements;

        public double? Left
        {
            get
            {
                if (this.SelectedElements == null) return null;

                return this.SelectedElements.Left;
            }
            set
            {
                if (this.SelectedElements == null) return;
                if (value == null || value == this.Left)
                {
                    this.OnPropertyChanged("Left");
                    return;
                }

                this.SelectedElements.Left = value;
                this.OnPropertyChanged("Left");
            }
        }
        
        public double? Top
        {
            get
            {
                if (this.SelectedElements == null) return null;

                return this.SelectedElements.Top;
            }
            set
            {
                if (this.SelectedElements == null) return;
                if (value == null || value == this.Top)
                {
                    this.OnPropertyChanged("Top");
                    return;
                }

                this.SelectedElements.Top = value;
                this.OnPropertyChanged("Top");
            }
        }


        public double? Width
        {
            get
            {
                if (this.SelectedElements == null) return null;

                return this.SelectedElements.Width;
            }
            set
            {
                if (this.SelectedElements == null) return;
                if (value == null || value == this.Width)
                {
                    this.OnPropertyChanged("Width");
                    return;
                }

                this.SelectedElements.Width = value;
                this.OnPropertyChanged("Width");
            }
        }

        public double? Height
        {
            get
            {
                if (this.SelectedElements == null) return null;

                return this.SelectedElements.Height;
            }
            set
            {
                if (this.SelectedElements == null) return;
                if (value == null || value == this.Height)
                {
                    this.OnPropertyChanged("Height");
                    return;
                }

                this.SelectedElements.Height = value;
                this.OnPropertyChanged("Height");
            }
        }        

        public int? PlayVisible
        {
            get
            {
                if (this.SelectedElements == null) return null;
                if (this.SelectedElements.PlayVisible == null) return null;

                if ((bool)this.SelectedElements.PlayVisible) return 0;
                return 1;
            }

            set
            {
                if (this.SelectedElements == null) return;

                if (value == null)
                {
                    this.OnPropertyChanged("PlayVisible");
                    return;
                }

                if (value == 0) this.SelectedElements.PlayVisible = true;
                if (value == 1) this.SelectedElements.PlayVisible = false;

                this.OnPropertyChanged("PlayVisible");
            }
        }

        public int? LockType
        {
            get
            {
                if (this.SelectedElements == null) return null;
                if (this.SelectedElements.LockType == null) return null;

                //if (this.SelectedElements.LockType.Value == LockTypes.None)
                //    return 0;

                InnotiveDebug.Trace(1, "[blackRoot01] Get LockType = {0}", this.SelectedElements.LockType.Value);

                if (this.SelectedElements.LockType.Value == LockTypes.OnlySize)
                    return 1;

                if (this.SelectedElements.LockType.Value == LockTypes.PositionAndSize)
                    return 2;

                return 0;

                //return this.SelectedElements.LockType;
            }
            set
            {
                if (this.SelectedElements == null) return;
                if (value == null)
                {
                    this.OnPropertyChanged("LockType");
                    return;
                }

                if (value == 1)
                    this.SelectedElements.LockType = LockTypes.OnlySize;
                else if (value == 2)
                    this.SelectedElements.LockType = LockTypes.PositionAndSize;
                else
                    this.SelectedElements.LockType = LockTypes.None;

                InnotiveDebug.Trace(1, "[blackRoot01] SetLockType() value = {0}, LockType = {1}", value, this.SelectedElements.LockType);

                //this.SelectedElements.LockType = value;
                this.OnPropertyChanged("LockType");
            }
        }

        public double? Opacity
        {
            get
            {
                if (this.SelectedElements == null) return null;

                return this.SelectedElements.Opacity;
            }
            set
            {
                if (this.SelectedElements == null) return;

                if (value == null)
                {
                    this.OnPropertyChanged("Opacity");
                    return;
                }

                if (value > 1) value = 1;

                this.SelectedElements.Opacity = value;
                this.OnPropertyChanged("Opacity");
            }
        }

        public bool? IsAppearanceInAllLevel
        {
            get
            {
                if (this.SelectedElements == null) return null;

                return this.SelectedElements.IsAppearanceInAllLevel;
            }
            set
            {
                if (this.SelectedElements == null) return;

                if (value == null)
                {
                    this.OnPropertyChanged("IsAppearanceInAllLevel");
                    return;
                }

                this.SelectedElements.IsAppearanceInAllLevel = value;
                this.OnPropertyChanged("IsAppearanceInAllLevel");
            }
        }

        public double? AppearanceMaxLevel 
        {

            get
            {
                if (this.SelectedElements == null) return null;

                return this.SelectedElements.AppearanceMaxLevel;
            }
            set
            {
                if (this.SelectedElements == null) return;

                if (value == null)
                {
                    this.OnPropertyChanged("AppearanceMaxLevel");
                    return;
                }

                this.SelectedElements.AppearanceMaxLevel = value;
                this.OnPropertyChanged("AppearanceMaxLevel");
            }
        }


        public double? AppearanceMinLevel
        {
            get
            {
                if (this.SelectedElements == null) return null;

                return this.SelectedElements.AppearanceMinLevel;
            }
            set
            {
                if (this.SelectedElements == null) return;

                if (value == null)
                {
                    this.OnPropertyChanged("AppearanceMinLevel");
                    return;
                }

                this.SelectedElements.AppearanceMinLevel = value;
                this.OnPropertyChanged("AppearanceMinLevel");
            }
        }

        public double? CurrentZoomRatio
        {
            get
            {
                if (this.SelectedElements == null) return null;

                return this.SelectedElements.CurrentZoomPercentage;
            }
            set
            {
                if (this.SelectedElements == null) return;

                if (value == null)
                {
                    this.OnPropertyChanged("CurrentZoomRatio");
                    return;
                }

                this.SelectedElements.CurrentZoomPercentage = value;
                this.OnPropertyChanged("CurrentZoomRatio");
            }
        }

        public double? LeftGis
        {
            get
            {
                if (this.SelectedElements == null) return null;

                return this.SelectedElements.LeftGis;
            }
            set
            {
                if (this.SelectedElements == null) return;
                if (value == null || value == this.LeftGis)
                {
                    this.OnPropertyChanged("LeftGis");
                    return;
                }

                this.SelectedElements.LeftGis = value;
                this.OnPropertyChanged("LeftGis");
            }
        }

        public double? TopGis
        {
            get
            {
                if (this.SelectedElements == null) return null;

                return this.SelectedElements.TopGis;
            }
            set
            {
                if (this.SelectedElements == null) return;
                if (value == null || value == this.TopGis)
                {
                    this.OnPropertyChanged("TopGis");
                    return;
                }

                this.SelectedElements.TopGis = value;
                this.OnPropertyChanged("TopGis");
            }
        }

        private int? PlayVisibleValueConverter(bool? playVisible)
        {
            if (playVisible == null) return null;
            if (playVisible == true) return 0;
            if (playVisible == false) return 1;
            return 0;
        }

        public void SetBindings(ISelectedElements selected)
        {
            if (selected == null) return;

            this.SelectedElements = selected;
            this.SelectedElements.PropertyChanged += this.SelectedElements_PropertyChanged;
            
            this.Left = this.SelectedElements.Left;
            this.Top = this.SelectedElements.Top;
            this.Width = this.SelectedElements.Width;
            this.Height = this.SelectedElements.Height;
            
            if (this.SelectedElements.LockType == LockTypes.OnlySize)
                this.LockType = 1;
            else if (this.SelectedElements.LockType == LockTypes.PositionAndSize)
                this.LockType = 2;
            else
                this.LockType = 0;

            //this.LockType = this.SelectedElements.LockType;

            
            this.Opacity = this.SelectedElements.Opacity;
            this.PlayVisible = PlayVisibleValueConverter(this.SelectedElements.PlayVisible);
            this.IsAppearanceInAllLevel = this.SelectedElements.IsAppearanceInAllLevel;
            this.CurrentZoomRatio = this.SelectedElements.CurrentZoomPercentage;
            this.AppearanceMinLevel = this.SelectedElements.AppearanceMinLevel;
            this.AppearanceMaxLevel = this.SelectedElements.AppearanceMaxLevel;
            //this.LeftGis = this.SelectedElements.LeftGis;
            //this.TopGis = this.SelectedElements.TopGis;
        }
        
        private void SelectedElements_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            this.OnPropertyChanged(e.PropertyName);
        }
    }
}
