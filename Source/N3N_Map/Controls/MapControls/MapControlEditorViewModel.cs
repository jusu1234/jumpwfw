﻿// -----------------------------------------------------------------------
// <copyright file="MapControlEditorViewModel.cs" company="Microsoft">
// Innotive Inc. Korea
// </copyright>
// -----------------------------------------------------------------------

namespace Innotive.InnoWatch.DLLs.MapControls
{
    using System.ComponentModel;

    using Innotive.InnoWatch.Commons.BaseControls;

    /// <summary>
    /// 맵 콘트롤 뷰 모델.
    /// </summary>
    public class MapControlEditorViewModel: INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged(string propertyName)
        {
            if (propertyName != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        public ISelectedElements SelectedElements;

        public void SetBinding(ISelectedElements selectedElements)
        {
            this.SelectedElements = selectedElements;

            this.SelectedElements.PropertyChanged += new PropertyChangedEventHandler(SelectedElements_PropertyChanged);

            this.MapLatitude = selectedElements.MapLatitude;
            this.MapLongitude = selectedElements.MapLongitude;
            this.MapZoom = selectedElements.MapZoom;
        }

        void SelectedElements_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            this.OnPropertyChanged(e.PropertyName);
        }

        public double? MapLatitude
        {
            get
            {
                if (SelectedElements == null) return null;
                return SelectedElements.MapLatitude;
            }
            set
            {
                if (SelectedElements == null) return;
                this.SelectedElements.MapLatitude = value;
                this.OnPropertyChanged("MapLatitude");
            }
        }

        public double? MapLongitude
        {
            get
            {
                if (SelectedElements == null) return null;
                return SelectedElements.MapLongitude;
            }
            set
            {
                if (SelectedElements == null) return;
                this.SelectedElements.MapLongitude = value;
                this.OnPropertyChanged("MapLongitude");
            }
        }

        public double? MapZoom
        {
            get
            {
                if (SelectedElements == null) return null;
                return SelectedElements.MapZoom;
            }
            set
            {
                if (SelectedElements == null) return;
                this.SelectedElements.MapZoom = value;
                this.OnPropertyChanged("MapZoom");
            }
        }
    }
}
