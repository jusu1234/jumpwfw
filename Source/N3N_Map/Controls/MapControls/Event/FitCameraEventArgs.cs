﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FitCameraEventArgs.cs" company="Innotive Inc. Korea">
//   Copyright (c) 2012 Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   FitCamera 시 발생하는 이벤트
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.DLLs.MapControls.Event
{
    using System;

    /// <summary>
    /// FitCamera 시 발생하는 이벤트
    /// </summary>
    public class FitCameraEventArgs : EventArgs
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FitCameraEventArgs"/> class.
        /// </summary>
        /// <param name="cameraId">
        /// The cameraId.
        /// </param>
        public FitCameraEventArgs(string cameraId)
        {
            this.CameraId = cameraId;
        }

        /// <summary>
        /// Gets or sets CameraId.
        /// </summary>
        public string CameraId { get; set; }
    }
}
