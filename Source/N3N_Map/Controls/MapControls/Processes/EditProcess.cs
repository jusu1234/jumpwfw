﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="EditProcess.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   The edit process.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using Innotive.InnoWatch.DLLs.LayoutControls;

namespace Innotive.InnoWatch.DLLs.MapControls.Processes
{
    using System.Windows;
    using Innotive.InnoWatch.Commons.EventArguments;

    /// <summary>
    /// The edit process.
    /// </summary>
    public class EditProcess : Process
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="EditProcess"/> class.
        /// </summary>
        /// <param name="layoutControl">
        /// The layout control.
        /// </param>
        public EditProcess(MapControl mapControl)
        {
            this.OwnMapControl = mapControl;
        }

        #endregion
    }
}