﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MapControl.cs" company="Innotive Inc. Korea">
//   Copyright (c) 2012 Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   CameraControl class.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using Innotive.InnoWatch.CommonUtils.Models;

namespace Innotive.InnoWatch.DLLs.MapControls
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Linq;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;
    
    using GMap.NET;
    using GMap.NET.MapProviders;
    using GMap.NET.WindowsPresentation;

    using SharpKml;
    using SharpKml.Base;
    using SharpKml.Dom;
    using SharpKml.Engine;

    using Innotive.InnoWatch.Commons.BaseControls;
    using Innotive.InnoWatch.Commons.CameraManagers.InnoVideoImages;
    using Innotive.InnoWatch.Commons.Publics;
    using Innotive.InnoWatch.Commons.Utils;
    using Innotive.InnoWatch.DLLs.CameraControls;
    using Innotive.InnoWatch.DLLs.MapControls.Elements;
    using Innotive.InnoWatch.DLLs.MapControls.Event;
    using Innotive.InnoWatch.DLLs.MapControls.Processes;
    using Innotive.InnoWatch.CommonUtils.Log;
    using Innotive.InnoWatch.DLLs.LayoutControls;
    using System.Windows.Media;

    /// <summary>
    /// MapPositionFitCamera Handler.
    /// </summary>
    /// <param index="sender">
    /// The sender.
    /// </param>
    /// <param index="e">
    /// The e.
    /// </param>
    public delegate void MapPositionFitCameraHandler(object sender, FitCameraEventArgs e);

    /// <summary>
    /// MapFullViewCamera Handler.
    /// </summary>
    /// <param index="sender">
    /// The sender.
    /// </param>
    /// <param index="e">
    /// The e.
    /// </param>
    public delegate void MapFullViewCameraHandler(object sender, FullViewCameraEventArgs e);

    /// <summary>
    /// CameraControl class.
    /// </summary>
    [TemplatePart(Name = "xGrid", Type = typeof(Grid))]
    public class MapControl : BaseLayoutControl, IBaseLayoutControl
    {
        private readonly List<CameraIconUserControl> cameraIconList = new List<CameraIconUserControl>();
        private readonly List<PinObjectUserControl> pinObjectList = new List<PinObjectUserControl>();
        private readonly List<string> mapProviderNameList = new List<string>();

        private static Cursor handPaperCursor = CursorHelper.CreateCursorWithBitmap(Properties.Resources.hand_paper, 5, 5);
        private static Cursor handRockCursor = CursorHelper.CreateCursorWithBitmap(Properties.Resources.hand_rock, 5, 5);
        private static Cursor beforeCursor = Cursors.Arrow;

        private ControlPanelUserControl controlPanel;
        private CenterCrossPen centerCrossPen;

        private bool enableMouseEvent;
        private bool showControlPanel;
        private bool showPinObject;
        private bool showCenterCrossPen;
        private bool cameraFullView;

        private string fullViewCameraId;

        private bool beforeShowControlPanel;
        private bool beforeShowPinObject;

        private string OwnVideoSurfaceId { get; set; }

        /// <summary>
        /// Initializes static members of the <see cref="MapControl"/> class.
        /// </summary>
        static MapControl()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(MapControl), new FrameworkPropertyMetadata(typeof(MapControl)));
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MapControl"/> class.
        /// </summary>
        public MapControl()
        {
            System.Diagnostics.Debug.WriteLine("[jhlee] Create MapControl");

            this.IsFocusedLayout = false;
            this.DoCreate();

            this.InitGMapControl();
        }

        private enum MoveDirection
        {
            Left,
            Up,
            Right,
            Down,
        }

        /// <summary>
        /// MapZoomChanged EventHandler.
        /// </summary>
        public event EventHandler MapZoomChangedEventHandler;

        /// <summary>
        /// Position changed event handler.
        /// </summary>
        public event EventHandler MapPositionChangedEventHandler;

        /// <summary>
        /// Position change End event handler.
        /// </summary>
        public event EventHandler MapPositionChangeEndEventHandler;

        /// <summary>
        /// FitCamera event handler.
        /// </summary>
        public event MapPositionFitCameraHandler MapPositionFitCameraHandler;

        /// <summary>
        /// FullViewCamera event handler.
        /// </summary>
        public event MapFullViewCameraHandler MapFullViewCameraHandler;

        public GMapControl Map;

        public static readonly DependencyProperty NonSelectBorderBrushProperty =
            DependencyProperty.Register(
                "NonSelectBorderBrush", typeof(System.Windows.Media.Brush), typeof(MapControl), new UIPropertyMetadata(System.Windows.Media.Brushes.White));

        public static readonly DependencyProperty NonSelectBorderThicknessProperty =
            DependencyProperty.Register(
                "NonSelectBorderThickness", typeof(double), typeof(MapControl), new UIPropertyMetadata(0.0));

        public static readonly DependencyProperty BorderMarginProperty =
            DependencyProperty.Register(
                "BorderMargin", typeof(Thickness), typeof(MapControl), new FrameworkPropertyMetadata(new Thickness()));

        /// <summary>
        /// MapLatitude Property.
        /// </summary>
        public static readonly DependencyProperty MapLatitudeProperty = DependencyProperty.Register(
            "MapLatitude",
            typeof(double),
            typeof(MapControl),
            new FrameworkPropertyMetadata(0.0, OnChangedMapLatitude));

        /// <summary>
        /// MapLongitude Property.
        /// </summary>
        public static readonly DependencyProperty MapLongitudeProperty = DependencyProperty.Register(
            "MapLongitude",
            typeof(double),
            typeof(MapControl),
            new FrameworkPropertyMetadata(0.0, OnChangedMapLongitude));

        /// <summary>
        /// MapZoom Property.
        /// </summary>
        public static readonly DependencyProperty MapZoomProperty = DependencyProperty.Register(
            "MapZoom",
            typeof(double),
            typeof(MapControl),
            new FrameworkPropertyMetadata(0.0, OnChangedMapZoom));

        /// <summary>
        /// MapType ( MapProvider ) Property.
        /// </summary>
        public static readonly DependencyProperty MapTypeProperty = DependencyProperty.Register(
            "MapType",
            typeof(string),
            typeof(MapControl),
            new FrameworkPropertyMetadata("OpenStreetMap", new PropertyChangedCallback(OnChangedMapType)));

        /// <summary>
        /// Gets ContentCanvas.
        /// </summary>
        public InnoCanvas ContentCanvas { get; internal set; }

        /// <summary>
        /// Gets MainProcess.
        /// </summary>
        public Processes.Process MainProcess { get; private set; }

        /// <summary>
        /// Gets ElementManager.
        /// </summary>
        public MapChildren ElementManager { get; private set; }

        /// <summary>
        /// Gets or sets a value indicating whether IsFocusedLayout.
        /// </summary>
        public bool IsFocusedLayout { get; set; }

        /// <summary>
        /// Gets or sets NonSelectBorderBrush.
        /// </summary>
        public System.Windows.Media.Brush NonSelectBorderBrush
        {
            get { return (System.Windows.Media.Brush)this.GetValue(NonSelectBorderBrushProperty); }

            set { this.SetValue((DependencyProperty)NonSelectBorderBrushProperty, value); }
        }

        /// <summary>
        /// Gets or sets NonSelectBorderThickness.
        /// </summary>
        public double NonSelectBorderThickness
        {
            get { return (double)this.GetValue(NonSelectBorderThicknessProperty); }

            set { this.SetValue((DependencyProperty)NonSelectBorderThicknessProperty, value); }
        }

        /// <summary>
        /// Gets or sets BorderMargin.
        /// </summary>
        public Thickness BorderMargin
        {
            get { return (Thickness)this.GetValue(BorderMarginProperty); }

            set { this.SetValue((DependencyProperty)BorderMarginProperty, value); }
        }

        /// <summary>
        /// Gets or sets MapLatitude.
        /// </summary>
        public double MapLatitude
        {
            get { return (double)this.GetValue(MapLatitudeProperty); }

            set { this.SetValue((DependencyProperty)MapLatitudeProperty, value); }
        }

        /// <summary>
        /// Gets or sets MapLongitude.
        /// </summary>
        public double MapLongitude
        {
            get { return (double)this.GetValue(MapLongitudeProperty); }

            set { this.SetValue((DependencyProperty)MapLongitudeProperty, value); }
        }

        /// <summary>
        /// Gets or sets MapZoom
        /// </summary>
        public double MapZoom
        {
            get
            {
                return (double)this.GetValue(MapZoomProperty);
            }

            set
            {
                this.SetValue((DependencyProperty)MapZoomProperty, value);
            }
        }

        /// <summary>
        /// Gets or sets MapType.
        /// </summary>
        public string MapType
        {
            get
            {
                return (string)this.GetValue(MapTypeProperty);
            }

            set
            {
                this.SetValue(MapTypeProperty, value);
            }
        }

        /// <summary>
        /// Gets MapProviderList.
        /// </summary>
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public List<string> MapTypeList
        {
            get
            {
                return this.mapProviderNameList;
            }
        }


        /// <summary>
        /// Gets or sets a value indicating whether MoveControlEnable.
        /// </summary>
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool ShowControlPanel
        {
            get
            {
                return this.showControlPanel;
            }

            set
            {
                if (this.controlPanel == null)
                {
                    return;
                }

                this.controlPanel.Visibility = value ? Visibility.Visible : Visibility.Collapsed;
                this.ShowCenterCrossPen = value;
                this.showControlPanel = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether ShowPinObject.
        /// </summary>
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool ShowPinObject
        {
            get
            {
                return this.showPinObject;
            }

            set
            {
                var visible = value ? Visibility.Visible : Visibility.Collapsed;
                foreach (var pinObjectUserControl in this.pinObjectList)
                {
                    pinObjectUserControl.Visibility = visible;
                }

                this.showPinObject = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether ShowPinObject.
        /// </summary>
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool ShowCenterCrossPen
        {
            get
            {
                return this.showCenterCrossPen;
            }

            set
            {
                if (this.centerCrossPen != null)
                {
                    this.centerCrossPen.Visibility = value ? Visibility.Visible : Visibility.Collapsed;
                }
                this.showCenterCrossPen = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether EnableMouseEvent.
        /// </summary>
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool EnableMouseEvent
        {
            get
            {
                return this.enableMouseEvent;
            }

            set
            {
                this.enableMouseEvent = value;

                if (this.Map != null)
                {
                    this.Map.CanDragMap = value;
                    this.Map.CanMouseWheelZooming = value;
                }
            }
        }

        /// <summary>
        /// Gets or sets CameraFullView.
        /// </summary>
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool CameraFullView
        {
            get
            {
                return this.cameraFullView;
            }

            set
            {
                if (value != this.cameraFullView)
                {
                    var cameraControl = this.ElementManager.GetCameraControl(this.fullViewCameraId);

                    if (cameraControl == null)
                    {
                        return;
                    }

                    if (value == true)
                    {
                        this.ApplyFullCameraView(cameraControl);
                    }
                    else
                    {
                        this.RestoreFullCameraView(cameraControl);
                    }

                    this.cameraFullView = value;

                    this.OnMapFullViewCameraHandler(new FullViewCameraEventArgs(this.fullViewCameraId, value));

                    //Layout Update (Camera Request) --> 속도가 빠름
                    //BaseLayoutControl layoutControl = LayoutUtils.GetParentBaseLayoutControl(this);
                    //if (layoutControl != null)
                    //    layoutControl.UpdateViewArea(Rect.Empty, null);
                    
                    //MapControl Update (Camera Request) --> 속도가 느림
                    //this.UpdateViewAreaInternal(Rect.Empty, null);
                    this.UpdateViewArea(Rect.Empty, null);
                }
            }
        }

        public void OnMapZoomChangedEventHandler()
        {
            var handler = this.MapZoomChangedEventHandler;
            if (handler != null)
            {
                handler(this, new EventArgs());
            }
        }

        public void OnMapPositionChangedEventHandler()
        {
            var handler = this.MapPositionChangedEventHandler;
            if (handler != null)
            {
                handler(this, new EventArgs());
            }
        }

        /// <summary>
        /// OnMapPositionChangeEnd Event Handler.
        /// </summary>
        public void OnMapPositionChangeEndEventHandler()
        {
            var handler = this.MapPositionChangeEndEventHandler;
            if (handler != null)
            {
                handler(this, new EventArgs());
            }
        }

        /// <summary>
        /// OnMapPositionFitCamera Event Handler.
        /// </summary>
        /// <param index="cameraId">
        /// The camera Id.
        /// </param>
        public void OnMapPositionFitCameraHandler(FitCameraEventArgs e)
        {
            var handler = this.MapPositionFitCameraHandler;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        /// <summary>
        /// OnMapFullViewCamera Event Handler.
        /// </summary>
        /// <param index="e">
        /// The e.
        /// </param>
        public void OnMapFullViewCameraHandler(FullViewCameraEventArgs e)
        {
            var handler = this.MapFullViewCameraHandler;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        /// <summary>
        /// 핀 오브젝트를 생성하고 추가한다.
        /// 카메라보다 상위에 나타나도록 하기 위해 ZINdex를 5000 부터 시작하도록 한다.
        /// </summary>
        public void AddPinObject(string index, string tooltip, double latitude, double longitude)
        {
            var pin = new PinObjectUserControl(this)
                {
                    ToolTip = tooltip,
                    Latitude = latitude,
                    Longitude = longitude,
                    Visibility = this.ShowPinObject ? Visibility.Visible : Visibility.Collapsed
                };
            pin.SetText(index);

            Panel.SetZIndex(pin, 5000 + Convert.ToInt32(index));

            this.ContentCanvas.Children.Add(pin);
            this.pinObjectList.Add(pin);

            this.ChangePinPosition(pin);
        }

        public void RemovePinObject(string name)
        {

        }

        public void ClearPinObject()
        {
            foreach (var pinObjectUserControl in this.pinObjectList)
            {
                this.ContentCanvas.Children.Remove(pinObjectUserControl);
            }

            this.pinObjectList.Clear();
        }

        public List<CameraControl> GetCameraElements()
        {
            var elements = new List<CameraControl>();
            foreach (var child in this.ContentCanvas.Children)
            {
                var camera = child as CameraControl;
                if (camera != null)
                {
                    elements.Add(camera);
                }
            }

            foreach (var child in this.Children)
            {
                var camera = child as CameraControl;
                if (camera != null)
                {
                    elements.Add(camera);
                }
            }

            return elements;
        }

        public Rect ConvertGisRect(System.Windows.Point lt, System.Windows.Point rb)
        {
            var gisLt = this.Map.FromLocalToLatLng(Convert.ToInt32(lt.X), Convert.ToInt32(lt.Y));
            var gisRb = this.Map.FromLocalToLatLng(Convert.ToInt32(rb.X), Convert.ToInt32(rb.Y));

            return new Rect(gisLt.Lat, gisLt.Lng, gisRb.Lat, gisRb.Lng);
        }

        /// <summary>
        /// The set process.
        /// </summary>
        /// <param index="processMode">
        /// The process mode.
        /// </param>
        public void SetProcess(ProcessMode processMode)
        {
            if (this.MainProcess != null)
            {
                this.ElementManager.ClearElementEventHandler();
                this.ElementManager.ClearElementEventHandler(this);

                this.MainProcess = null;
            }

            if (this.IsMouseCaptured)
            {
                this.ReleaseMouseCapture();
            }

            switch (processMode)
            {
                case ProcessMode.Edit:
                    this.MainProcess = new EditProcess(this);
                    break;

                case ProcessMode.Play:
                    this.MainProcess = new PlayProcess(this);
                    break;

                case ProcessMode.Record:
                    this.MainProcess = new RecordProcess(this);
                    break;
            }

            this.ElementManager.AssignElementEventHandler();
            this.ElementManager.AssignElementEventHandler(this);
        }

        public PointLatLng GetMouseGisPosition(System.Windows.Point point)
        {
            return this.Map.FromLocalToLatLng((int)point.X, (int)point.Y);
        }

        /// <summary>
        /// 카메라 위치로 이동.
        /// </summary>
        /// <param index="cameraId">
        /// The camera id.
        /// </param>
        public void GotoCamera(string cameraId)
        {
            foreach (var camera in this.GetCameraElements())
            {
                if (string.Compare(camera.ID, cameraId) == 0)
                {
                    this.MapLatitude = camera.CenterLatitude;
                    this.MapLongitude = camera.CenterLongitude;
                }
            }
        }

        /// <summary>
        /// 카메라 크기에 맞는 최대 줌 상태에서 카메라 위치로 이동.
        /// </summary>
        /// <param index="camraId">
        /// The camra id.
        /// </param>
        public void GotoFitCamera(string camraId)
        {
            if (this.Map == null)
            {
                return;
            }

            foreach (var element in this.GetCameraElements())
            {
                if (element.ID.Equals(camraId))
                {
                    var rect = new RectLatLng(element.TopGis, element.LeftGis, Math.Abs(element.BottomGis - element.TopGis), Math.Abs(element.RightGis - element.LeftGis));
                    var zoom = this.Map.GetMaxZoomToFitRect(rect);

                    var isZoomChanged = false;
                    var isPositionChanged = false;

                    // Event Loop 방지 용
                    if (this.MapZoom != Convert.ToDouble(zoom))
                    {

                        this.MapZoom = zoom;

                        isZoomChanged = true;
                    }

                    // Event Loop 방지 용
                    if (this.MapLatitude != element.CenterLatitude)
                    {
                        this.MapLatitude = element.CenterLatitude;

                        isPositionChanged = true;
                    }

                    // Event Loop 방지 용
                    if (this.MapLongitude != element.CenterLongitude)
                    {
                        this.MapLongitude = element.CenterLongitude;

                        isPositionChanged = true;
                    }

                    if (isZoomChanged)
                    {
                        this.OnMapZoomChangedEventHandler();
                    }

                    if (isPositionChanged)
                    {
                        this.OnMapPositionChangeEndEventHandler();
                    }

                    if (isZoomChanged || isPositionChanged)
                    {
                        this.OnMapPositionFitCameraHandler(new FitCameraEventArgs(camraId));
                    }
                    break;
                }
            }
        }

        /// <summary>
        /// Sync 명령을 통해서 화면을 제어한다. 
        /// </summary>
        /// <param index="zoomLevel">변경할 줌 레벨</param>
        public void MapControlZoomChangedSync(double zoomLevel)
        {
            var action = new Action(() =>
                {
                    if (!double.IsNaN(zoomLevel))
                    {
                        this.MapZoom = zoomLevel;
                    }
                });

            if (this.Dispatcher.CheckAccess())
            {
                action.Invoke();
            }
            else
            {
                this.Dispatcher.BeginInvoke(action);
            }
        }

        /// <summary>
        /// Sync 명령을 통해서 MapControl 의 좌표을 변경한다. 
        /// </summary>
        /// <param index="latitute">위도</param>
        /// <param index="longtitute">경도</param>
        public void MapControlPositionChangedSync(double latitute, double longtitute)
        {
            var action = new Action(() =>
                {
                    if (!double.IsNaN(latitute))
                    {
                        this.MapLatitude = latitute;
                    }

                    if (!double.IsNaN(longtitute))
                    {
                        this.MapLongitude = longtitute;
                    }
                });

            if (this.Dispatcher.CheckAccess())
            {
                action.Invoke();
            }
            else
            {
                this.Dispatcher.BeginInvoke(action);
            }
        }

        /// <summary>
        /// Sync 명령을 통해서 MapControl 의 카메라를 전체화면으로 만든다.
        /// </summary>
        /// <param index="cameraId">
        /// The camera Id.
        /// </param>
        /// <param index="isFull">
        /// The is Full.
        /// </param>
        public void MapControlFullViewCameraSync(string cameraId, bool isFull)
        {
            var action = new Action(() =>
                {
                    if (!string.IsNullOrEmpty(cameraId))
                    {
                        var cameraControl = this.ElementManager.GetCameraControl(cameraId);
                        if (cameraControl != null)
                        {
                            if (isFull == true)
                            {
                                this.fullViewCameraId = cameraId;
                            }
                            this.CameraFullView = isFull;
                        }
                    }
                });

            if (this.Dispatcher.CheckAccess())
            {
                action.Invoke();
            }
            else
            {
                this.Dispatcher.BeginInvoke(action);
            }
        }

        public void AddElement(FrameworkElement element)
        {
            var baseControl = element as BaseControl;

            if (baseControl == null)
            {
                Debug.Assert(false, "엘리먼트가 베이스컨트롤이 아닙니다.");
                return;
            }

            var cameraControl = element as CameraControl;
            if (cameraControl != null)
            {
                cameraControl.MouseDoubleClick += this.camera_MouseDoubleClick;
                cameraControl.VideoSurfaceId = this.OwnVideoSurfaceId;
            }

            this.ElementManager.AddElement(baseControl);
            this.Redraw();
            //this.UpdateViewAreaInternal(Rect.Empty, null);
            this.UpdateViewArea(Rect.Empty, null);
        }

        public void RemoveElement(FrameworkElement element)
        {
            if (element is CameraControl)
            {
                var control = element as CameraControl;
                control.MouseDoubleClick -= this.camera_MouseDoubleClick;
            }

            this.ElementManager.RemoveElement(element);

            if(element is IDisposable)
            {
                var disposable = element as IDisposable;
                disposable.Dispose();
            }

            //this.UpdateViewAreaInternal(Rect.Empty, null);
            this.UpdateViewArea(Rect.Empty, null);
        }

        public void RemoveElements(Type type, bool includeSubLayout)
        {

        }

        public void ArrangeElement(FrameworkElement targetElement, Commons.BaseControls.ElementArrangeType arrangeType)
        {

        }

        public void BringToFront(FrameworkElement element)
        {
            this.ElementManager.BringToFront(element);
        }

        public void BringForward(FrameworkElement element)
        {
            this.ElementManager.BringForward(element);
        }

        public void InsertElement(int index, FrameworkElement element)
        {
            throw new NotImplementedException();
        }

        public bool IsContain(FrameworkElement element)
        {
            throw new NotImplementedException();
        }

        public int GetZIndex(FrameworkElement element)
        {
            throw new NotImplementedException();
        }

        public int GetMaxZIndex()
        {
            throw new NotImplementedException();
        }

        public void SendToBack(FrameworkElement element)
        {
            this.ElementManager.SendToBack(element);
        }

        public void SendBackward(FrameworkElement element)
        {
            this.ElementManager.SendBackward(element);
        }

        public System.Collections.Generic.List<FrameworkElement> GetElements()
        {
            return this.ElementManager.Elements;
        }

        public UIElementCollection GetUnlockedElements()
        {
            throw new NotImplementedException();
        }

        public UIElementCollection GetLockedElements()
        {
            throw new NotImplementedException();
        }

        public void AllocationCanvas(FrameworkElement element)
        {
            //this.ElementManager.AllocationCanvas(element);
        }

        public void SetZIndex(FrameworkElement element, int zIndex)
        {
            this.ElementManager.SetZIndex(element, zIndex);
        }


        public System.Windows.Point GetMousePosition()
        {
            return Mouse.GetPosition(this.ContentCanvas);
        }

        public override void UpdateViewArea(Rect parentCanvasScreenRegion, List<Rect> parentsScreenRegion)
        {
            // base.UpdateViewArea(parentCanvasScreenRegion, parentsScreenRegion);
            AsyncWorker.Instance.UpdateMapChildrenCamera(
                this, 
                new Action(()=> this.UpdateViewAreaInternal(parentCanvasScreenRegion, parentsScreenRegion)));
            //this.UpdateViewAreaInternal(parentCanvasScreenRegion, parentsScreenRegion);
        }

        protected override void OnRenderSizeChanged(SizeChangedInfo sizeInfo)
        {
            base.OnRenderSizeChanged(sizeInfo);

            this.Width = sizeInfo.NewSize.Width;
            this.Height = sizeInfo.NewSize.Height;

            this.ContentCanvas.Width = sizeInfo.NewSize.Width;
            this.ContentCanvas.Height = sizeInfo.NewSize.Height;

            if (this.Map != null)
            {
                this.Map.Width = sizeInfo.NewSize.Width;
                this.Map.Height = sizeInfo.NewSize.Height;
            }

            if (this.centerCrossPen != null)
            {
                Canvas.SetLeft(this.centerCrossPen, (sizeInfo.NewSize.Width / 2) - (this.centerCrossPen.Width / 2));
                Canvas.SetTop(this.centerCrossPen, (sizeInfo.NewSize.Height / 2) - (this.centerCrossPen.Height / 2));
            }
        }

        /// <summary>
        /// The do dispose.
        /// </summary>
        /// <param index="isManage">
        /// The is manage.
        /// </param>
        protected override void DoDispose(bool isManage)
        {
            if (this.Dispatcher.CheckAccess())
            {
                this.Internal_DoDispose();
            }
            else
            {
                this.Dispatcher.Invoke(new Action(this.Internal_DoDispose));
            }

            base.DoDispose(isManage);
        }

        private void Internal_DoDispose()
        {
            this.ClearEvent();

            if (this.ElementManager != null)
            {
                ElementManager.ClearElementEventHandler();
                ElementManager.Clear();
            }

            ElementManager = null;

            foreach (var item in this.ContentCanvas.Children)
            {
                var element = item;

                if (element is IDisposable)
                {
                    (element as IDisposable).Dispose();
                }
            }
            this.ContentCanvas.Children.Clear();

            for (var i = Children.Count - 1; i >= 0; i--)
            {
                var element = Children[i];

                if (element is IDisposable)
                {
                    (element as IDisposable).Dispose();
                }
            }
            Children.Clear();

            this.Map.Dispose();
            this.Map = null;

            // 생성 시 만들어졌던 VideoSurface도 함께 Dispose 한다.
            if (!string.IsNullOrWhiteSpace(this.OwnVideoSurfaceId))
            {
                VideoSurfaceManager.Instance.DisposeVideoSurface(this.OwnVideoSurfaceId);
                this.OwnVideoSurfaceId = string.Empty;
            }

            System.Diagnostics.Debug.WriteLine("[jhlee] Dispose MapControl");
        }

        protected override Size MeasureOverride(Size constraint)
        {
            if (this.ElementManager != null)
            {
                this.ElementManager.AdjustElementLocation();
            }
            return base.MeasureOverride(constraint);
        }

        private static void OnChangedMapLongitude(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            var self = o as MapControl;

            if (self == null)
            {
                return;
            }

            if (self.Map == null)
            {
                return;
            }

            var mapLongitude = (double)e.NewValue;

            var mapLatitude = self.Map.Position.Lat;
            self.Map.Position = new PointLatLng(mapLatitude, mapLongitude);
        }

        private static void OnChangedMapLatitude(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            var self = o as MapControl;

            if (self == null)
            {
                return;
            }

            if (self.Map == null)
            {
                return;
            }

            var mapLatitude = (double)e.NewValue;

            var mapLongitude = self.Map.Position.Lng;
            self.Map.Position = new PointLatLng(mapLatitude, mapLongitude);
        }

        private static void OnChangedMapZoom(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            var self = o as MapControl;

            if (self == null)
            {
                return;
            }

            if (self.Map == null)
            {
                return;
            }

            var mapZoom = (double)e.NewValue;
            self.Map.Zoom = mapZoom;
        }

        private static void OnChangedMapType(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            var self = o as MapControl;

            if (self == null)
            {
                return;
            }

            if (self.Map == null)
            {
                return;
            }

            var mapType = e.NewValue as string;
            self.Map.MapProvider = self.ConvertMapProvider(mapType);
        }

        // Convert : String => GMapProvier
        private GMapProvider ConvertMapProvider(string mapProviderString)
        {
            GMapProvider ret = GMapProviders.EmptyProvider;

            // type convert
            foreach (var mapproviderItem in GMapProviders.List)
            {
                if (String.Compare(mapproviderItem.Name, mapProviderString, true) == 0)
                {
                    ret = mapproviderItem;
                }
            }

            return ret;
        }


        public PointLatLng GetCenterLatitudeAndLongitude()
        {
            System.Windows.Point centerPoint = this.GetCenterPosition();
            return this.Map.FromLocalToLatLng((int)centerPoint.X, (int)centerPoint.Y);
        }

        private void DoCreate()
        {
            this.ClipToBounds = true;
            this.ElementManager = new MapChildren(this);
            this.ContentCanvas = new InnoCanvas { Width = 100, Height = 100 };
            this.Children.Add(this.ContentCanvas);
            this.SetProcess(Public.IsEditingType() ? ProcessMode.Edit : ProcessMode.Play);

            this.Loaded += this.MapControl_Loaded;
        }


        private void ClearEvent()
        {
            this.Loaded -= this.MapControl_Loaded;

            foreach (var camera in this.GetCameraElements())
            {
                camera.MouseDoubleClick -= this.camera_MouseDoubleClick;
            }

            if (this.Map != null)
            {
                this.Map.OnPositionChanged -= this.map_OnPositionChanged;
                this.Map.OnTileLoadComplete -= this.map_OnTileLoadComplete;
                this.Map.OnTileLoadStart -= this.map_OnTileLoadStart;
                this.Map.OnMapTypeChanged -= this.map_OnMapTypeChanged;
                this.Map.MouseMove -= this.map_MouseMove;
                this.Map.MouseLeftButtonDown -= this.map_MouseLeftButtonDown;
                this.Map.MouseLeftButtonUp -= this.map_MouseLeftButtonUp;
                this.Map.Loaded -= this.map_Loaded;
                this.Map.OnMapZoomChanged -= this.map_OnMapZoomChanged;
                this.Map.MouseEnter -= this.map_MouseEnter;
                this.Map.MouseLeave -= this.map_MouseLeave;
                this.Map.ManipulationCompleted -= this.Map_ManipulationCompleted;
            }

            if (this.controlPanel != null)
            {
                this.UnlinkControlPanelEventHandlers();
            }
        }

        private void MapControl_Loaded(object sender, RoutedEventArgs e)
        {
            var layoutControl = sender as MapControl;

            if (layoutControl != null)
            {
                layoutControl.Loaded -= this.MapControl_Loaded;
            }

            // RedrawLayoutFrame();
            this.ElementManager.AdjustElementLocation();

            foreach (var camera in this.GetCameraElements())
            {
                camera.MouseDoubleClick += this.camera_MouseDoubleClick;
            }
        }

        public void SetOwnVideoSurfaceId(string id)
        {
            this.OwnVideoSurfaceId = id;
        }

        private void InitGMapControl()
        {
            this.Map = new GMapControl();

            // Initialize
            if (this.Map != null)
            {
                this.Map.Manager.UseMemoryCache = true;

                this.Map.MapProvider = GMapProviders.EmptyProvider;
                this.Map.Position = new PointLatLng(this.MapLatitude, this.MapLongitude);
                this.Map.MinZoom = 1;
                this.Map.MaxZoom = 17;

                this.MapZoom = 7;
                this.MapLatitude = 37.0902398030721;
                this.MapLongitude = 127.705078125;

                this.Map.Manager.Mode = AccessMode.ServerOnly;
                this.Map.MouseWheelZoomType = MouseWheelZoomType.MousePositionWithoutCenter;
                //this.Map.MouseWheelZoomType = MouseWheelZoomType.MousePositionAndCenter;

                this.Map.DragCursor = handRockCursor;
                this.Map.ShowCenter = false;

                this.Map.OnPositionChanged += this.map_OnPositionChanged;
                this.Map.OnTileLoadComplete += this.map_OnTileLoadComplete;
                this.Map.OnTileLoadStart += this.map_OnTileLoadStart;
                this.Map.OnMapTypeChanged += this.map_OnMapTypeChanged;
                this.Map.MouseMove += this.map_MouseMove;
                this.Map.MouseLeftButtonDown += this.map_MouseLeftButtonDown;
                this.Map.MouseLeftButtonUp += this.map_MouseLeftButtonUp;
                this.Map.Loaded += this.map_Loaded;
                this.Map.OnMapZoomChanged += this.map_OnMapZoomChanged;
                this.Map.MouseEnter += this.map_MouseEnter;
                this.Map.MouseLeave += this.map_MouseLeave;
                this.Map.ManipulationCompleted += this.Map_ManipulationCompleted;
            }

            this.ContentCanvas.Children.Add(this.Map);

            foreach (var provider in GMapProviders.List)
            {
                this.mapProviderNameList.Add(provider.Name);
            }

            // CM이 아니면 ControlPanel과 CenterCross를 생성하지 않는다.
            if(Public.GetProgramType() != ProgramType.iCommand)
                return;

            this.controlPanel = new ControlPanelUserControl();
            this.ContentCanvas.Children.Add(this.controlPanel);
            this.controlPanel.Visibility = Visibility.Collapsed;
            this.controlPanel.Margin = new Thickness(10, 30, 10, 0);
            
            this.LinkControlPanelEventHandlers();
            this.controlPanel.SetSliderPosition(this.MapZoom);

            this.centerCrossPen = new CenterCrossPen();
            this.ContentCanvas.Children.Add(this.centerCrossPen);
            this.ShowCenterCrossPen = true;
        }

        private void LinkControlPanelEventHandlers()
        {
            this.controlPanel.ControlPanelLeftButtonClicked += this.ControlPanel_LeftButtonClicked;
            this.controlPanel.ControlPanelUpButtonClicked += this.ControlPanel_UpButtonClicked;
            this.controlPanel.ControlPanelRightButtonClicked += this.ControlPanel_RightButtonClicked;
            this.controlPanel.ControlPanelDownButtonClicked += this.ControlPanel_DownButtonClicked;
            this.controlPanel.SliderValueChanged += this.ControlPanel_SliderValueChanged;
        }

        private void UnlinkControlPanelEventHandlers()
        {
            this.controlPanel.ControlPanelLeftButtonClicked -= this.ControlPanel_LeftButtonClicked;
            this.controlPanel.ControlPanelUpButtonClicked -= this.ControlPanel_UpButtonClicked;
            this.controlPanel.ControlPanelRightButtonClicked -= this.ControlPanel_RightButtonClicked;
            this.controlPanel.ControlPanelDownButtonClicked -= this.ControlPanel_DownButtonClicked;
            this.controlPanel.SliderValueChanged -= this.ControlPanel_SliderValueChanged;
        }

        /// <summary>
        /// Normal => Full
        /// </summary>
        /// <param index="cameraControl"></param>
        private void ApplyFullCameraView(CameraControl cameraControl)
        {
            foreach (var camera in this.GetCameraElements())
            {
                if (camera.Equals(cameraControl))
                {
                    Canvas.SetLeft(cameraControl, 0);
                    Canvas.SetTop(cameraControl, 0);
                    cameraControl.Width = this.Width;
                    cameraControl.Height = this.Height;

                    this.fullViewCameraId = cameraControl.ID;
                    this.beforeShowControlPanel = this.ShowControlPanel;
                    this.beforeShowPinObject = this.ShowPinObject;
                    this.ShowControlPanel = false;
                    this.ShowPinObject = false;
                }
                else
                {
                    camera.Visibility = Visibility.Hidden;
                }
            }

            //this.UpdateViewAreaInternal(Rect.Empty, null);
        }

        /// <summary>
        /// Full => Normal
        /// </summary>
        private void RestoreFullCameraView()
        {
            if (string.IsNullOrEmpty(this.fullViewCameraId))
            {
                return;
            }

            foreach (var camera in this.GetCameraElements())
            {
                if (string.Compare(camera.ID, this.fullViewCameraId) == 0)
                {
                    this.RestoreFullCameraView(camera);

                    return;
                }
            }
        }

        private void RestoreFullCameraView(CameraControl cameraControl)
        {
            foreach (var camera in this.GetCameraElements())
            {
                camera.Visibility = Visibility.Visible;
            }

            switch (GetLockType(cameraControl))
            {
                case LockTypes.None:
                    this.LockTypeNone(cameraControl);
                    break;
                case LockTypes.OnlySize:
                    this.LockTypeOnlySize(cameraControl);
                    break;
                case LockTypes.PositionAndSize:
                    this.LockTypePositionAndSize(cameraControl);
                    break;
                default:
                    this.LockTypeNone(cameraControl);
                    break;
            }

            //this.UpdateViewAreaInternal(Rect.Empty, null);

            // this.fullViewCameraId = string.Empty;

            this.ShowControlPanel = this.beforeShowControlPanel;
            this.ShowPinObject = this.beforeShowPinObject;
        }

        private void camera_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (!this.EnableMouseEvent)
            {
                return;
            }

            var cameraControl = sender as CameraControl;

            if (cameraControl != null)
            {
                this.GotoFitCamera(cameraControl.ID);
                //this.fullViewCameraId = cameraControl.ID;
                //this.CameraFullView = !this.CameraFullView;
            }
        }

        void Map_ManipulationCompleted(object sender, ManipulationCompletedEventArgs e)
        {

        }

        private void ChangePinPosition(PinObjectUserControl pin)
        {
            if (pin == null)
            {
                return;
            }

            var pt = this.Map.FromLatLngToLocal(new PointLatLng(pin.Latitude, pin.Longitude));

            Canvas.SetLeft(pin, pt.X - (pin.Width / 2));
            Canvas.SetTop(pin, pt.Y - pin.Height);
        }

        private void ControlPanel_SliderValueChanged(object sender, ChangedDoubleValueEventArgs e)
        {
            this.MapZoom = e.DoubleValue;
        }

        private void ControlPanel_LeftButtonClicked(object sender, EventArgs e)
        {
            this.Move(MoveDirection.Left);
        }

        private void ControlPanel_UpButtonClicked(object sender, EventArgs e)
        {
            this.Move(MoveDirection.Up);
        }

        private void ControlPanel_RightButtonClicked(object sender, EventArgs e)
        {
            this.Move(MoveDirection.Right);
        }

        private void ControlPanel_DownButtonClicked(object sender, EventArgs e)
        {
            this.Move(MoveDirection.Down);
        }

        private void Move(MoveDirection direction)
        {
            switch (direction)
            {
                case MoveDirection.Left:
                    this.MapLongitude -= 2 / this.MapZoom;
                    break;

                case MoveDirection.Up:
                    this.MapLatitude += 2 / this.MapZoom;
                    break;

                case MoveDirection.Right:
                    this.MapLongitude += 2 / this.MapZoom;
                    break;

                case MoveDirection.Down:
                    this.MapLatitude -= 2 / this.MapZoom;
                    break;
            }
        }

        void map_MouseEnter(object sender, MouseEventArgs e)
        {
            if (!this.EnableMouseEvent)
            {
                return;
            }

            beforeCursor = this.Map.Cursor;
            this.Map.Cursor = handPaperCursor;
        }

        void map_MouseLeave(object sender, MouseEventArgs e)
        {
            if (!this.EnableMouseEvent)
            {
                return;
            }

            this.Map.Cursor = beforeCursor;
        }

        void map_Loaded(object sender, RoutedEventArgs e)
        {
            this.Redraw();
            this.SuperviseCameraSize();
            if (this.controlPanel != null)
            {
                this.controlPanel.SetSliderPosition(this.MapZoom);
            }

            //this.UpdateViewAreaInternal(Rect.Empty, null);
            this.UpdateViewArea(Rect.Empty, null);

            //KmlFile kmlData = KmlFile.LoadFromKmz(
            //    SharpKml.Engine.KmzFile.Open("D:\\test.kmz"));
            
            //foreach (SharpKml.Dom.Placemark item in kmlData.Root.Flatten().OfType<SharpKml.Dom.Placemark>())
            //{
            //    var pl = new PointLatLng();
            //    pl.Lat = (double)item.CalculateLookAt().Latitude;
            //    pl.Lng = (double)item.CalculateLookAt().Longitude;
                
            //    var marker = new GMapMarker();
            //    marker.Position = pl;
                                                
            //    var text = new TextBlock();
            //    text.Text = item.Name;
            //    text.Width = 100;
            //    text.Height = 25;
            //    text.Background = Brushes.White;
            //    text.ToolTip = item.Address;
            //    text.IsHitTestVisible = false;

            //    marker.Shape = text;

            //    this.Map.Markers.Add(marker);
            //}
        }

        void map_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (!this.EnableMouseEvent)
            {
                return;
            }

            this.Map.Cursor = handRockCursor;
        }

        void map_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (!this.EnableMouseEvent)
            {
                return;
            }

            this.Map.Cursor = handPaperCursor;

            this.OnMapPositionChangeEndEventHandler();
        }

        void map_MouseMove(object sender, MouseEventArgs e)
        {

        }

        void map_OnMapTypeChanged(GMapProvider type)
        {

        }

        void map_OnTileLoadStart()
        {

        }

        void map_OnTileLoadComplete(long ElapsedMilliseconds)
        {

        }

        private void map_OnMapZoomChanged()
        {
            this.MapZoom = this.Map.Zoom;

            if(this.controlPanel != null)
            {
                this.controlPanel.SetSliderPosition(this.MapZoom);
            }

            this.MapLatitude = this.Map.Position.Lat;
            this.MapLongitude = this.Map.Position.Lng;

            foreach (BaseControl element in this.ElementManager.Elements)
            {
                switch (GetLockType(element))
                {
                    case LockTypes.None:
                        this.LockTypeNone(element);
                        break;
                    case LockTypes.OnlySize:
                        this.LockTypeOnlySize(element);
                        break;
                    case LockTypes.PositionAndSize:
                        this.LockTypePositionAndSize(element);
                        break;
                    default:
                        this.LockTypeNone(element);
                        break;
                }
            }

            this.OnMapZoomChangedEventHandler();
            //this.OnMapPositionChangeEndEventHandler();

            foreach (var pinObjectUserControl in this.pinObjectList)
            {
                this.ChangePinPosition(pinObjectUserControl);
            }

            this.SuperviseCameraSize();
            //AsyncWorker.Instance.UpdateMapChildrenCamera(
            //    this,
            //    new Action(() => this.UpdateViewAreaInternal(Rect.Empty, null)));
            this.UpdateViewArea(Rect.Empty, null);
            //this.UpdateViewAreaInternal(Rect.Empty, null);
        }

        private void LockTypeNone(BaseControl element)
        {
            var leftTop = this.Map.FromLatLngToLocal(new PointLatLng(element.TopGis, element.LeftGis));
            var rightBottom = this.Map.FromLatLngToLocal(new PointLatLng(element.BottomGis, element.RightGis));

            element.Left = leftTop.X;
            element.Top = leftTop.Y;
            element.Width = Math.Abs(rightBottom.X - leftTop.X);
            element.Height = Math.Abs(rightBottom.Y - leftTop.Y);
        }

        private void LockTypeOnlySize(BaseControl element)
        {
            var pt = this.Map.FromLatLngToLocal(new PointLatLng(element.CenterLatitude, element.CenterLongitude));

            Canvas.SetLeft(element, pt.X - (element.Width / 2));
            Canvas.SetTop(element, pt.Y - (element.Height / 2));
        }

        private void LockTypePositionAndSize(BaseControl element)
        {
            //throw new NotImplementedException();
        }

        private List<Rect> GetParentRects()
        {
            //// 자신을 포함한 모든 부모의 요소를 가져옴.
            var ancestorList = InnoControlUtil.GetAllParents(this, false);
            if (ancestorList.Any(parent => parent.Visibility != Visibility.Visible))
            {
                ancestorList = null;
            }

            //// 자신을 포함한 모든 부모의 화면 영역을 리스트로 저장.
            //// 아직 자식이 부모와 연결되어 있지 않으면 그 요소는 무시.
            var ancestorScreenRegionList = new List<Rect>();
            if (ancestorList != null)
            {
                foreach (FrameworkElement element in ancestorList)
                {
                    if (PresentationSource.FromVisual(element) != null)
                    {
                        var elementScreenRegion = new Rect(
                            element.PointToScreen(new System.Windows.Point(0, 0)),
                            element.PointToScreen(new System.Windows.Point(element.ActualWidth, element.ActualHeight)));
                        ancestorScreenRegionList.Add(elementScreenRegion);
                    }
                }
            }

            // 부모에 실제 화면 영역 추가.
            var windowRegion = InnoMonitorUtil.GetWindowBounds(Application.Current.MainWindow);
            ancestorScreenRegionList.Add(windowRegion);

            return ancestorScreenRegionList;
        }

        private void UpdateViewAreaInternal(Rect parentCanvasScreenRegion, List<Rect> parentsScreenRegion)
        {
            if (PresentationSource.FromVisual(this) == null)
            {
                return;
            }

            // 자기 자신 화면 영역 계산
            var thisScreenRegion = new Rect(
                this.PointToScreen(new System.Windows.Point(0, 0)),
                this.PointToScreen(new System.Windows.Point(this.ActualWidth, this.ActualHeight)));

            // TODO : 모든 컨트롤에 적용될 수 있도록 수정해야 함. (by jhlee)
            foreach (FrameworkElement element in this.ContentCanvas.Children)
            {
                if (element is BaseControl)
                {
                    if (parentsScreenRegion != null)
                    {
                        if (parentCanvasScreenRegion != Rect.Empty)
                        {
                            parentsScreenRegion.Add(parentCanvasScreenRegion);
                        }

                        (element as BaseControl).UpdateViewArea(thisScreenRegion, parentsScreenRegion);
                    }
                    else
                    {
                        var control = (element as BaseControl) as CameraControl;
                        if (control == null)
                            continue;
                        if (PresentationSource.FromVisual(this) == null)
                        {
                            return;
                        }

                        if (parentsScreenRegion == null)
                        {
                            parentsScreenRegion = this.GetParentRects();
                        }

                        // 자기 자신 화면 영역 계산
                        var ownScreenRegion = new Rect(
                           this.PointToScreen(new System.Windows.Point(0, 0)),
                           this.PointToScreen(new System.Windows.Point(this.ActualWidth, this.ActualHeight)));

                        control.UpdateViewArea(ownScreenRegion, parentsScreenRegion);

                        //AsyncWorker.Instance.UpdateMapChildrenCamera(
                        //    control,
                        //    new Action(() =>
                        //    {
                        //        if (PresentationSource.FromVisual(this) == null)
                        //        {
                        //            return;
                        //        }

                        //        if (parentsScreenRegion == null)
                        //        {
                        //            parentsScreenRegion = this.GetParentRects();
                        //        }

                        //        // 자기 자신 화면 영역 계산
                        //        var ownScreenRegion = new Rect(
                        //           this.PointToScreen(new Point(0, 0)),
                        //           this.PointToScreen(new Point(this.ActualWidth, this.ActualHeight)));
                                
                        //        control.UpdateViewArea(ownScreenRegion, parentsScreenRegion);
                        //    }));
                    }
                }
                else
                {
                    element.InvalidateVisual();
                }
            }
        }

        public void map_OnPositionChanged(PointLatLng point)
        {
            this.MapLatitude = point.Lat;
            this.MapLongitude = point.Lng;

            foreach (BaseControl element in this.ElementManager.Elements)
            {
                switch (GetLockType(element))
                {
                    case LockTypes.None:
                        this.LockTypeNone(element);
                        break;
                    case LockTypes.OnlySize:
                        this.LockTypeOnlySize(element);
                        break;
                    case LockTypes.PositionAndSize:
                        this.LockTypePositionAndSize(element);
                        break;
                    default:
                        this.LockTypeNone(element);
                        break;
                }
            }

            if (!this.Map.IsDragging)
            {
                this.OnMapPositionChangeEndEventHandler();
                InnotiveDebug.Trace(1, "[blackRoot30] OnMapPositionChangeEndEventHandler()");
            }
            else
            {
                this.OnMapPositionChangedEventHandler();
                InnotiveDebug.Trace(1, "[blackRoot30] OnMapPositionChangedEventHandler()");
            }

            foreach (var pinObjectUserControl in this.pinObjectList)
            {
                this.ChangePinPosition(pinObjectUserControl);
            }

            foreach (var cameraIconUserControl in this.cameraIconList)
            {
                this.ChangeCameraIconPosition(cameraIconUserControl);
            }

            //AsyncWorker.Instance.UpdateMapChildrenCamera(
            //    this,
            //    new Action(() => this.UpdateViewAreaInternal(Rect.Empty, null)));
            //this.UpdateViewAreaInternal(Rect.Empty, null);
            this.UpdateViewArea(Rect.Empty, null);
        }

        private void Redraw()
        {
            this.SetLockType();

            var index = this.ContentCanvas.Children.Count;
            foreach (var pinObjectUserControl in this.pinObjectList)
            {
                Panel.SetZIndex(pinObjectUserControl, index);
            }

            if(Public.GetProgramType() != ProgramType.iCommand)
                return;

            Panel.SetZIndex(this.centerCrossPen, index + 1);
            Panel.SetZIndex(this.controlPanel, index + 2);
            Canvas.SetLeft(this.controlPanel, this.ContentCanvas.Width - this.controlPanel.Width - 10);
        }

        private void SetLockType()
        {
            if (this.ElementManager == null)
            {
                return;
            }

            if (this.ElementManager.Elements == null)
            {
                return;
            }

            if (this.ElementManager.Elements.Count == 0)
            {
                return;
            }

            foreach (BaseControl element in this.ElementManager.Elements)
            {
                switch (GetLockType(element))
                {
                    case LockTypes.None:
                        this.LockTypeNone(element);
                        break;
                    case LockTypes.OnlySize:
                        this.LockTypeOnlySize(element);
                        break;
                    case LockTypes.PositionAndSize:
                        this.LockTypePositionAndSize(element);
                        break;
                    default:
                        this.LockTypeNone(element);
                        break;
                }
            }
        }

        private void SuperviseCameraSize()
        {
            var tempIcon = new CameraIconUserControl();
            var refWidth = tempIcon.Width;
            var refHeight = tempIcon.Height;

            foreach (var cameraIcon in this.cameraIconList)
            {
                this.ContentCanvas.Children.Remove(cameraIcon);
            }

            this.cameraIconList.Clear();

            foreach (var camera in this.GetCameraElements())
            {
                if (camera.Width < refWidth || camera.Height < refHeight)
                {
                    camera.Visibility = Visibility.Hidden;
                    var icon = new CameraIconUserControl
                    {
                        ToolTip = camera.CameraName,
                        Latitude = camera.CenterLatitude,
                        Longitude = camera.CenterLongitude,
                    };

                    this.ContentCanvas.Children.Add(icon);
                    this.ChangeCameraIconPosition(icon);
                    this.cameraIconList.Add(icon);
                }
                else
                {
                    camera.Visibility = Visibility.Visible;
                }
            }
        }

        private void ChangeCameraIconPosition(CameraIconUserControl icon)
        {
            if (icon == null)
            {
                return;
            }

            var pt = this.Map.FromLatLngToLocal(new PointLatLng(icon.Latitude, icon.Longitude));

            Canvas.SetLeft(icon, pt.X - (icon.Width / 2));
            Canvas.SetTop(icon, pt.Y - icon.Height);
        }

        public void PinMouseLeftButtonDown(PinObjectUserControl selectedPin)
        {
            foreach (var pin in this.pinObjectList)
            {
                if (selectedPin == pin)
                {
                    pin.xPath.Visibility = Visibility.Collapsed;
                    pin.xSelectedPath.Visibility = Visibility.Visible;
                    Panel.SetZIndex(pin, 5000 + pinObjectList.Count + 1);
                }
                else
                {
                    pin.xPath.Visibility = Visibility.Visible;
                    pin.xSelectedPath.Visibility = Visibility.Collapsed;
                    Panel.SetZIndex(pin, 5000 + Convert.ToInt32(pin.GetText()));
                }
            }
        }

        public void GoToPinLocation(double latitude, double longitude)
        {
            this.MapLatitude = latitude;
            this.MapLongitude = longitude;

            foreach (var pin in this.pinObjectList)
            {
                if ((pin.Longitude == longitude) && (pin.Latitude == latitude))
                {

                    pin.xPath.Visibility = Visibility.Collapsed;
                    pin.xSelectedPath.Visibility = Visibility.Visible;
                    Panel.SetZIndex(pin, 5000 + pinObjectList.Count + 1);
                }
                else
                {
                    pin.xPath.Visibility = Visibility.Visible;
                    pin.xSelectedPath.Visibility = Visibility.Collapsed;
                    Panel.SetZIndex(pin, 5000 + Convert.ToInt32(pin.GetText()));
                }
            }
        }

        /// <summary>
        /// VideoSurface를 생성하여 해당 MapControl에 등록한다.
        /// 생성한 VideoSurface를  MapControl의 자식 CameraControl에도 모두 등록한다.
        /// </summary>
        /// <param name="mapControl">The MapControl.</param>
        public static void AddVideoSurface(MapControl mapControl)
        {
            var videoSurfaceId = VideoSurfaceManager.Instance.AddMapVideoSurface(mapControl.OriginGUID.ToString());

            mapControl.SetOwnVideoSurfaceId(videoSurfaceId);

            var cameraControls = mapControl.GetCameraElements();
            foreach (var cameraControl in cameraControls)
            {
                cameraControl.VideoSurfaceId = videoSurfaceId;
            }
        }
    }
}