﻿
namespace Innotive.InnoWatch.DLLs.MapControls
{
    using System;
    using System.ComponentModel;
    using System.Windows;
    using System.Windows.Input;
    using System.Windows.Media;

    /// <summary>
    /// Interaction logic for PinObjectUserControl.xaml.
    /// </summary>
    public partial class PinObjectUserControl : INotifyPropertyChanged
    {
        //private string name;
        private double latitude;
        private double longitude;

        private MapControl owner;

        /// <summary>
        /// Initializes a new instance of the <see cref="PinObjectUserControl"/> class.
        /// </summary>
        public PinObjectUserControl(MapControl mapControl)
        {
            InitializeComponent();
            this.DataContext = this;
            this.owner = mapControl;

            this.PreviewMouseLeftButtonDown += OnPreviewMouseLeftButtonDown;            
        }

        private void OnPreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs mouseButtonEventArgs)
        {
            this.owner.PinMouseLeftButtonDown(this);
        }
        
        public void SetText(string name)
        {
            this.xText.Text = name;
        }

        public string GetText()
        {
            return this.xText.Text;
        }

        public double Latitude
        {
            get
            {
                return this.latitude;
            }
            
            set
            {
                this.latitude = value;
            }
        }

        public double Longitude
        {
            get
            {
                return this.longitude;
            }
            
            set
            {
                this.longitude = value;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged(string name)
        {
            var handler = this.PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }
    }
}
