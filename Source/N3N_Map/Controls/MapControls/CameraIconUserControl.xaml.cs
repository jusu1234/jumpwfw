﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CameraIconUserControl.xaml.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Interaction logic for CameraIconUserControl.xaml
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.DLLs.MapControls
{
    using System.Windows.Controls;

    /// <summary>
    /// Interaction logic for CameraIconUserControl.xaml.
    /// </summary>
    public partial class CameraIconUserControl : UserControl
    {
        //private string name;
        private double latitude;
        private double longitude;

        /// <summary>
        /// Initializes a new instance of the <see cref="CameraIconUserControl"/> class.
        /// </summary>
        public CameraIconUserControl()
        {
            InitializeComponent();
        }

        // UserControl은 Name 속성을 이미 가지고 있음.
        //public string Name
        //{
        //    get
        //    {
        //        return this.name;
        //    }

        //    set
        //    {
        //        this.name = value;
        //    }
        //}

        public double Latitude
        {
            get
            {
                return this.latitude;
            }

            set
            {
                this.latitude = value;
            }
        }

        public double Longitude
        {
            get
            {
                return this.longitude;
            }

            set
            {
                this.longitude = value;
            }
        }
    }
}
