﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MapMoveControl.xaml.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Interaction logic for MapMoveControl.xaml
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.DLLs.MapControls
{
    using System;
    using System.Windows;
    using System.Windows.Controls;

    using Innotive.InnoWatch.Commons.BaseControls;

    /// <summary>
    /// Interaction logic for MapMoveControl.xaml.
    /// </summary>
    public partial class ControlPanelUserControl : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ControlPanelUserControl"/> class.
        /// </summary>
        public ControlPanelUserControl()
        {
            InitializeComponent();
        }

        public event EventHandler ControlPanelLeftButtonClicked;
        public event EventHandler ControlPanelUpButtonClicked;
        public event EventHandler ControlPanelRightButtonClicked;
        public event EventHandler ControlPanelDownButtonClicked;

        /// <summary>
        /// </summary>
        public event EventHandler<ChangedDoubleValueEventArgs> SliderValueChanged;

        /// <summary>
        /// </summary>
        public void InvokeLeftButtonClicked()
        {
            var handler = this.ControlPanelLeftButtonClicked;
            if (handler != null)
            {
                handler(this, new EventArgs());
            }
        }

        public void InvokeUpButtonClicked()
        {
            var handler = this.ControlPanelUpButtonClicked;
            if (handler != null)
            {
                handler(this, new EventArgs());
            }
        }

        public void InvokeRightButtonClicked()
        {
            var handler = this.ControlPanelRightButtonClicked;
            if (handler != null)
            {
                handler(this, new EventArgs());
            }
        }

        public void InvokeDownButtonClicked()
        {
            var handler = this.ControlPanelDownButtonClicked;
            if (handler != null)
            {
                handler(this, new EventArgs());
            }
        }

        public void InvokeSliderValueChanged()
        {
            var handler = this.SliderValueChanged;
            if (handler != null)
            {
                handler(this, new ChangedDoubleValueEventArgs(xZoomSlider.Value));
            }
        }

        public void SetSliderPosition(double value)
        {
            if (this.xZoomSlider.Value.Equals(value))
            {
                return;
            }

            xZoomSlider.ValueChanged -= this.xZoomSlider_ValueChanged;
            xZoomSlider.Value = value;
            xZoomSlider.ValueChanged += this.xZoomSlider_ValueChanged;
        }

        private void xZoomSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            this.InvokeSliderValueChanged();
        }

        #region Mouse PreviewDown Event
        // ClickEvent가 발생하지 않아 터널링 이벤트를 사용함. 멀티그리드의 드래그/드랍과 충돌로 예상됨.
        private void xControlPanelLeftButton_PreviewMouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            this.InvokeLeftButtonClicked();
        }

        private void xControlPanelRightButton_PreviewMouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            this.InvokeRightButtonClicked();
        }

        private void xControlPanelUpButton_PreviewMouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            this.InvokeUpButtonClicked();
        }

        private void xControlPanelDownButton_PreviewMouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            this.InvokeDownButtonClicked();
        }

        private void xControlPanelZoomInButton_PreviewMouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            this.xZoomSlider.Value += 1;
        }

        private void xControlPanelZoomOutButton_PreviewMouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            this.xZoomSlider.Value -= 1;
        }

        #endregion //Mouse PreviewDown Event

        #region Mouse Click Event

        private void xControlPanelButton_Click(object sender, RoutedEventArgs e)
        {
            this.InvokeLeftButtonClicked();
        }

        private void xControlPanelLeftButton_Click(object sender, RoutedEventArgs e)
        {
            this.InvokeLeftButtonClicked();
        }

        private void xControlPanelUpButton_Click(object sender, RoutedEventArgs e)
        {
            this.InvokeUpButtonClicked();
        }

        private void xControlPanelRightButton_Click(object sender, RoutedEventArgs e)
        {
            this.InvokeRightButtonClicked();
        }

        private void xControlPanelDownButton_Click(object sender, RoutedEventArgs e)
        {
            this.InvokeDownButtonClicked();
        }

        private void xZoomInButton_Click(object sender, RoutedEventArgs e)
        {
            this.xZoomSlider.Value += 1;
        }

        private void xZoomOutButton_Click(object sender, RoutedEventArgs e)
        {
            this.xZoomSlider.Value -= 1;
        }

        #endregion //Mouse Click Event
    }
}
