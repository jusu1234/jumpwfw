﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MapControlRepository.cs" company="Innotive Inc. Korea">
//   Copyright (c) 2012 Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Defines the MapControlRepository type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using Innotive.InnoWatch.CommonUtils.Log;

namespace Innotive.InnoWatch.DLLs.MapControls
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Text;
    using System.Xml;
    using Innotive.InnoWatch.Commons.BaseControls;
    using Innotive.InnoWatch.Commons.Configs;
    using Innotive.InnoWatch.Commons.Files;
    using Innotive.InnoWatch.Commons.Map;
    using Innotive.InnoWatch.Commons.Xamls;
    using Innotive.InnoWatch.Commons.CameraManagers.InnoVideoImages;
    using Innotive.InnoWatch.DLLs.CameraControls;

    /// <summary>
    /// MapControlRepository class.
    /// </summary>
    public class MapControlRepository
    {
        #region Constant and Fields
        
        /// <summary>
        /// 맵 컨트롤 이름.
        /// </summary>
        private const string MapControlName = "DynamicMapControl";
                
        #endregion

        #region Properties

        /// <summary>
        /// 로컬 파일 저장 경로.
        /// </summary>
        public static InnoFilePath FilePath { get; private set; }

        #endregion

        /// <summary>
        /// 로딩할 Map 파일의 경로를 할당한다.
        /// 반드시 MainProcess.Instance.StartManagers() 이전에 호출되어야 정상적으로 Map을 로딩할 수 있다.
        /// </summary>
        /// <param name="folderPath"></param>
        public static void SetMapFilePath(string folderPath)
        {
            try
            {
                if (!Directory.Exists(folderPath))
                {
                    Directory.CreateDirectory(folderPath);
                }

                var fileName = string.Format("{0}.map", CommonsConfig.Instance.ProjectID);
                var fullPath = string.Format(@"{0}\{1}", folderPath, fileName);

                FilePath = new InnoFilePath(fullPath);
            }
            catch (Exception ex)
            {
                InnotiveDebug.Log.Error(string.Format("[Map File Path Setting Error] {0}", ex));
            }
        }

        /// <summary>
        /// 맵 컨트롤 생성하여 반환.
        /// VideoSurface를 생성하여 등록한다.
        /// </summary>
        /// <param name="width">
        /// 맵 컨트롤 가로 크기.
        /// </param>
        /// <param name="height">
        /// 맵 컨트롤 세로 크기.
        /// </param>
        /// <param name="createVideoSurface">MapControl 생성과 함께 VideoSurface를 생성할 것인지 여부.</param>
        /// <returns>
        /// 맵 컨트롤.
        /// </returns>
        public static MapControl CreateMapControl(double width = 800, double height = 600, bool createVideoSurface = true)
        {
            //var mapControl = LoadMapControl();

            //if (mapControl != null)
            //{
            //    mapControl.Width = width;
            //    mapControl.Height = height;
            //}
            //else
            //{
            //    mapControl = new MapControl
            //    {
            //        LockType = LockTypes.None,
            //        Left = 0,
            //        Top = 0,
            //        Width = width,
            //        Height = height,
            //        MapZoom = 7,
            //        MapLatitude = CommonsConfig.Instance.MapLatitude,
            //        MapLongitude = CommonsConfig.Instance.MapLongitude
            //    };

            //    Save(mapControl);
            //}
            
            //if(CommonsConfig.Instance.MaxHighResSourceCount != 0)
            //{
            //    // VideoSurface를 생성하여 등록한다.
            //    if (createVideoSurface)
            //    {
            //        MapControl.AddVideoSurface(mapControl);
            //    }    
            //}

            var mapControl = new MapControl
            {
                LockType = LockTypes.None,
                Left = 0,
                Top = 0,
                Width = width,
                Height = height,
                MapZoom = 7,
                MapLatitude = CommonsConfig.Instance.MapLatitude,
                MapLongitude = CommonsConfig.Instance.MapLongitude
            };
            
            return mapControl;
        }
        
        /// <summary>
        /// 맵 컨트롤을 파일로 저장.
        /// </summary>
        /// <param name="mapControl">
        /// The map control.
        /// </param>
        public static void SaveMapControl(MapControl mapControl)
        {
            Save(mapControl);
        }

        /// <summary>
        /// 저장된 Map Layout File 의 데이터를 반환한다.
        /// </summary>
        /// <returns>
        /// string 형태의 Map Layout 파일 DATA.
        /// </returns>
        public static string ReadXamlString()
        {
            if (!File.Exists(FilePath.Path))
            {
                return string.Empty;
            }

            var mapLayoutFile = new StreamReader(FilePath.Path);
            var mapLayoutStringData = mapLayoutFile.ReadToEnd();
            mapLayoutFile.Close();
            return mapLayoutStringData;
        }

        /// <summary>
        /// 저장된 맵 파일에서 MapControl 의 SyncGuid 를 찾아서 반환.
        /// </summary>
        /// <returns>
        /// 싱크 GUID.
        /// </returns>
        public static Guid GetLocationMapSyncGuid()
        {
            var mapControlData = ReadXamlString();

            if (mapControlData == string.Empty)
            {
                return Guid.Empty;
            }

            var xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(mapControlData);
            var root = xmlDoc.DocumentElement;
            if (root != null)
            {
                var guid = Guid.Parse(root.Attributes["SyncGUID"].Value);

                return guid;
            }

            return Guid.Empty;
        }

        /// <summary>
        /// 저장된 맵 파일에서 카메라 컨트롤의 아이디만 찾아 리스트 형태로 반환.
        /// </summary>
        /// <returns>
        /// 카메라 컨트롤 아이디 리스트.
        /// </returns>
        public static List<string> GetCameraElementIdList()
        {
            var cameraIdList = new List<string>();

            var mapControlData = ReadXamlString();

            if (mapControlData == string.Empty)
            {
                var mapControl = CreateMapControl(800, 600);
                Save(mapControl); // 빈 MapLayoutControl 생성

                // Reload
                mapControlData = ReadXamlString();
            }

            var xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(mapControlData);
            var root = xmlDoc.DocumentElement;
            if (root != null)
            {
                var count = root.ChildNodes.Count;
                for (int i = 0; i < count; i++)
                {
                    var node = root.ChildNodes.Item(i);

                    if (node == null)
                    {
                        continue;
                    }

                    if (String.CompareOrdinal(node.LocalName, "CameraControl") == 0 || String.CompareOrdinal(node.LocalName, "RdsViewerControl")== 0)
                    {
                        if (node.Attributes != null)
                        {
                            cameraIdList.Add(node.Attributes["ID"].Value);
                        }
                    }
                }
            }

            return cameraIdList;
        }

        /// <summary>
        /// 지정된 카메라의 GiocodeData 반환.
        /// </summary>
        /// <param name="cameraId">
        /// The camera id.
        /// </param>
        /// <returns>
        /// GiocodeData of CameraControl.
        /// </returns>
        public static GiocodeData GetGiocodeData(string cameraId)
        {
            var xmlDoc = new XmlDocument();
            xmlDoc.Load(FilePath.Path);
            var root = xmlDoc.DocumentElement;
            if (root != null)
            {
                var count = root.ChildNodes.Count;

                // count가 0이 나오네....
                for (int i = 0; i < count; i++)
                {
                    var node = root.ChildNodes.Item(i);
                    if (node != null && string.Compare(node.LocalName, "CameraControl") == 0)
                    {
                        if (node.Attributes != null && node.Attributes["ID"].Value.Equals(cameraId))
                        {
                            var data = new GiocodeData();

                            var leftGis = Convert.ToDouble(node.Attributes["LeftGis"].Value);
                            var topGis = Convert.ToDouble(node.Attributes["TopGis"].Value);
                            var rightGis = Convert.ToDouble(node.Attributes["RightGis"].Value);
                            var bottomGis = Convert.ToDouble(node.Attributes["BottomGis"].Value);

                            data.Lat = (topGis + bottomGis) / 2;
                            data.Lng = (leftGis + rightGis) / 2;

                            return data;
                        }
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// MapControl 내에서 CameraControl 을 얻어 온다. 
        /// </summary>
        /// <returns>
        /// 카메라 컨트롤 목록.
        /// </returns>
        public static List<CameraControl> GetCameraControlList()
        {
            var mapControl = CreateMapControl(800, 600);

            var cameraControlList = new List<CameraControl>();

            foreach (var element in mapControl.GetElements())
            {
                if (element is CameraControl)
                {
                    cameraControlList.Add(element as CameraControl);
                }
            }

            return cameraControlList;
        }

        /// <summary>
        /// 맵 저장 파일 경로 반환.
        /// </summary>
        /// <returns>
        /// 맵 경로 스트링.
        /// </returns>
        public static string GetFilePath()
        {
            var path = System.Windows.Forms.Application.ExecutablePath;
            var index = path.LastIndexOf(@"\");
            path = path.Substring(0, index + 1) + CommonsConfig.Instance.ProjectID + ".map";
            return path;
        }

        /// <summary>
        /// 저장되어 있는 MapControl 파일을 로드한다. ( {Proejct ID}.map ).
        /// </summary>
        /// <returns>
        /// The load map control.
        /// </returns>
        private static MapControl LoadMapControl()
        {
            if (File.Exists(FilePath.Path))
            {
                var mapControl = InnoXamlReader.FileLoad(FilePath) as MapControl;

                if (mapControl != null && mapControl.Name == MapControlName)
                {
                    return mapControl;
                }
            }

            return null;
        }

        /// <summary>
        /// map control save.
        /// </summary>
        /// <param name="mapControl">
        /// The map control.
        /// </param>
        private static void Save(MapControl mapControl)
        {
            if (mapControl == null)
            {
                return;
            }

            mapControl.Name = MapControlName;

            var settings = new XmlWriterSettings
            {
                Indent = true,
                IndentChars = new string(' ', 4),
                NewLineOnAttributes = true,
                Encoding = Encoding.UTF8
            };

            var xmlWriter = XmlWriter.Create(FilePath.Path, settings);

            try
            {
                InnoXamlWriter.Save(mapControl, xmlWriter);
            }
            finally
            {
                xmlWriter.Flush();
                xmlWriter.Close();
            }
        }
    }
}
