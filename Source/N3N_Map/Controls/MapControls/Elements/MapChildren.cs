﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright company="Innotive Inc. Korea" file="ElementManager.cs">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   Normal : XAML 에 기록되는 Element
//   Foundation : Zoom 기능등을 구현하기 위한 뼈대 Element
//   Unknown : 초기화 되지 않은 Element.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.DLLs.MapControls.Elements
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Media;

    using Innotive.InnoWatch.Commons.BaseControls;
    using Innotive.InnoWatch.DLLs.BookmarkControls;
    using Innotive.InnoWatch.DLLs.CameraControls;

    /// <summary>
    /// The element manager.
    /// </summary>
    public class MapChildren
    {
        private readonly List<FrameworkElement> visualElementList = new List<FrameworkElement>();

        private readonly MapControl owner;

        public MapChildren(MapControl owner)
        {
            this.owner = owner;
        }

        public List<FrameworkElement> Elements
        {
            get { return this.GetElements(); }
        }

        // 엘리먼트를 추가한다.
        public void AddElement(FrameworkElement element)
        {
            this.AllocationCanvas(element);
            this.AssignElementEventHandler(element);
            this.InitControlElementList();
           // this.AddControlElementList(element);
        }

        // 
        public void AdjustElementLocation()
        {
            var tempElement = new List<object>();

            for (int i = 0; i < VisualTreeHelper.GetChildrenCount(this.owner); i++ )
            {
                var element = VisualTreeHelper.GetChild(this.owner, i);

                if (element is BaseLayoutControl || element is BaseControl)
                {
                    tempElement.Add(element);
                }
            }

            foreach (FrameworkElement element in tempElement)
            {
                this.AllocationCanvas(element);
                this.AssignElementEventHandler(element);
            }

            this.InitControlElementList();
        }

        /// <summary>
        /// 엘리먼트에 이벤트들을 할당합니다.
        /// </summary>
        /// <param name="element"></param>
        public void AssignElementEventHandler(FrameworkElement element)
        {
            if (element == null)
            {
                Debug.Assert(false, "엘리먼트가 널입니다.");
                return;
            }

            if (this.owner.MainProcess == null)
            {
                Debug.Assert(false, "메인프로세스가 널입니다.");
                return;
            }

            var baseControl = element as BaseControl;
            if (baseControl == null)
            {
                Debug.Assert(false, "엘리먼트를 베이스컨트롤로 형변환 할 수 없습니다.");
                return;
            }

            lock (baseControl)
            {
                baseControl.MouseEnter += this.owner.MainProcess.ElementMouseEnter;
                baseControl.MouseLeave += this.owner.MainProcess.ElementMouseLeave;
                baseControl.ChangedCenterLatitudeEvent += this.ElementManager_ChangedGisLocationEvent;
                baseControl.ChangedCenterLongitudeEvent += this.ElementManager_ChangedGisLocationEvent;
                baseControl.SizeChanged += this.ElementManager_SizeChanged;
                baseControl.LockTypeChanged += this.ElementManager_ControlLockTypeChanged;

                if (baseControl is BaseLayoutControl)
                {
                    var control = baseControl as BaseLayoutControl;
                    control.Process = this.owner.Process;
                }
            }
        }

        public void AssignElementEventHandler()
        {
            foreach (FrameworkElement element in this.Elements)
            {
                if (element is BaseLayoutControl || element is BaseControl)
                {
                    this.AssignElementEventHandler(element);
                }
            }
        }

        public void BringForward(FrameworkElement element)
        {
            this.InitControlElementList();

            var zIndex = Panel.GetZIndex(element);
            if (zIndex >= this.visualElementList.Count)
            {
                return;
            }

            this.visualElementList.Remove(element);

            if (this.visualElementList.Count > zIndex)
            {
                this.visualElementList.Insert(zIndex + 1, element);
            }
            else
            {
                this.visualElementList.Add(element);
            }

            this.SetControlElementZIndex();
        }

        public void BringToFront(FrameworkElement element)
        {
            this.InitControlElementList();
            this.visualElementList.Remove(element);
            this.visualElementList.Add(element);
            this.SetControlElementZIndex();
        }

        public void ClearElementEventHandler(FrameworkElement element)
        {
            if (element == null)
            {
                return;
            }

            lock (element)
            {
                if (this.owner.MainProcess != null)
                {
                    element.MouseEnter -= this.owner.MainProcess.ElementMouseEnter;
                    element.MouseLeave -= this.owner.MainProcess.ElementMouseLeave;

                    if (element is BaseControl)
                    {
                        var control = element as BaseControl;

                        control.ChangedCenterLatitudeEvent -= this.ElementManager_ChangedGisLocationEvent;
                        control.ChangedCenterLongitudeEvent -= this.ElementManager_ChangedGisLocationEvent;
                        control.SizeChanged += this.ElementManager_SizeChanged;
                        control.LockTypeChanged += this.ElementManager_ControlLockTypeChanged;

                        if (control is BaseLayoutControl)
                        {
                            var layout = element as BaseLayoutControl;
                            if (layout != null)
                            {
                                layout.Process = null;
                            }
                        }
                    }
                }
            }
        }

        public void ClearElementEventHandler()
        {
            foreach (FrameworkElement element in this.Elements)
            {
                if (element is BaseLayoutControl || element is BaseControl)
                {
                    this.ClearElementEventHandler(element);
                }
            }
        }

        public void InsertElement(int index, FrameworkElement element)
        {
            this.AllocationCanvas(index, element);
            this.AssignElementEventHandler(element);
        }

        public bool IsFoundationElement(FrameworkElement element)
        {
            return element is IFoundationElement;
        }

        public void RemoveElement(FrameworkElement element)
        {
            this.ClearElementEventHandler(element);

            var index = this.owner.ContentCanvas.Children.IndexOf(element);

            if (index > -1)
            {
                this.owner.ContentCanvas.Children.Remove(element);
            }

            this.InitControlElementList();
            this.SetControlElementZIndex();
        }

        public void SendBackward(FrameworkElement element)
        {
            this.InitControlElementList();

            var zIndex = Panel.GetZIndex(element);

            if (zIndex < 1)
            {
                return;
            }

            this.visualElementList.Remove(element);
            this.visualElementList.Insert(zIndex - 1, element);
            this.SetControlElementZIndex();
        }

        public void SendToBack(FrameworkElement element)
        {
            this.InitControlElementList();
            this.visualElementList.Remove(element);
            this.visualElementList.Insert(0, element);
            this.SetControlElementZIndex();
        }

        public int GetMaxZIndex()
        {
            return this.Elements.Count - this.Elements.OfType<BookmarkControl>().Count() - 1;
        }

        public int GetZIndex(FrameworkElement element)
        {
            return Panel.GetZIndex(element);
        }

        public void SetZIndex(FrameworkElement element, int zIndex)
        {
            this.InitControlElementList();

            this.visualElementList.Remove(element);
            this.visualElementList.Insert(zIndex, element);
            this.SetControlElementZIndex();
        }

        internal void Clear()
        {
            for (var i = this.visualElementList.Count - 1; i >= 0; i--)
            {
                var element = this.visualElementList[i];
                if (element is IDisposable)
                {
                    (element as IDisposable).Dispose();
                }
            }

            if (this.visualElementList != null)
            {
                this.visualElementList.Clear();
            }
        }

        void ElementManager_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            var baseControl = sender as BaseControl;
            if (baseControl == null)
            {
                return;
            }

            //var elementCenterPosition = baseControl.GetCenterPosition();
            //var positionGIS = this.mapControl.Map.FromLocalToLatLng((int)elementCenterPosition.X, (int)elementCenterPosition.Y);

            //baseControl.CenterLatitude = positionGIS.Lat;
            //baseControl.CenterLongitude = positionGIS.Lng;
        }

        void ElementManager_ChangedGisLocationEvent(object sender, ChangedDoubleValueEventArgs e)
        {
            var baseControl = sender as BaseControl;
            if (baseControl == null)
            {
                return;
            }

            //var point = mapControl.Map.FromLatLngToLocal(new PointLatLng(baseControl.CenterLatitude, baseControl.CenterLongitude));

            //Canvas.SetLeft(baseControl, point.X - baseControl.Width / 2);
            //Canvas.SetTop(baseControl, point.Y - baseControl.Height / 2);
        }

        private List<FrameworkElement> GetElements()
        {
            var elementList = new List<FrameworkElement>();

            foreach (FrameworkElement element in this.owner.ContentCanvas.Children)
            {
                if (this.IsFoundationElement(element) == false && element is BaseControl)
                {
                    elementList.Add(element);
                }
            }

            return elementList;
        }

        private void AddControlElementList(UIElement element)
        {
            var count = this.GetElements().Count;
            Panel.SetZIndex(element, count);
            this.InitControlElementList();
        }

        private void AllocationCanvas(FrameworkElement element)
        {
            RemoveFromParent(element);
            this.owner.ContentCanvas.Children.Add(element);
        }

        private void AllocationCanvas(int index, FrameworkElement element)
        {
            RemoveFromParent(element);
            this.owner.ContentCanvas.Children.Insert(index, element);

            this.InitControlElementList();
        }

        private void ElementManager_ControlLockTypeChanged(object sender, RoutedPropertyChangedEventArgs<LockTypes> e)
        {
            var element = sender as FrameworkElement;
            this.AllocationCanvas(element);
            e.Handled = true;
        }

        private void InitControlElementList()
        {
            var list = this.GetElements();
            this.visualElementList.Clear();
            foreach (var element in list)
            {
                // 북마크 컨트롤을 최상위로 - by Alvin (110127)
                if (element.GetType() == typeof(BookmarkControl))
                {
                    Panel.SetZIndex(element, list.Count + 10);
                }
                
                this.InsertControlElementZIndex(element);
            }

            this.SetControlElementZIndex();
        }

        private void InsertControlElementZIndex(FrameworkElement element)
        {
            var elementZIndex = Panel.GetZIndex(element);

            for (var index = 0; index < this.visualElementList.Count; index++)
            {
                var listZIndex = Panel.GetZIndex(this.visualElementList[index]);
                if (elementZIndex < listZIndex)
                {
                    this.visualElementList.Insert(index, element);
                    return;
                }
            }

            this.visualElementList.Add(element);
        }

        private static void RemoveFromParent(FrameworkElement element)
        {
            if (element == null) return;
            if (element.Parent == null) return;

            if (element.Parent is Canvas)
            {
                (element.Parent as Canvas).Children.Remove(element);
            }
            else if (element.Parent is ListBox)
            {
                (element.Parent as ListBox).Items.Remove(element);
            }
            else if (element.Parent is BaseLayoutControl)
            {
                (element.Parent as BaseLayoutControl).Children.Remove(element);
            }
        }

        private void SetControlElementZIndex()
        {
            for (var index = 0; index < this.visualElementList.Count; index++)
            {
                Panel.SetZIndex(this.visualElementList[index], index);
            }
        }

        /// <summary>
        /// Element 목록에서 CameraControl 를 찾아서 반환한다. 
        /// </summary>
        /// <param name="cameraId">찾을 카메라 ID</param>
        /// <returns>CameraControl</returns>
        public CameraControl GetCameraControl(string cameraId)
        {
            foreach (var element in this.Elements)
            {
                if (element is CameraControl)
                {
                    var cameraControl = element as CameraControl;

                    if (cameraControl.ID == cameraId)
                    {
                        return cameraControl;
                    }
                }                
            }

            return null;
        }

    }
}