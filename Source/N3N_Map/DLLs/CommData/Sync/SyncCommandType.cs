﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SyncCommandType.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   싱크 명령 타입.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.DLLs.CommData.Sync
{
    /// <summary>
    /// 싱크 명령 타입.
    /// </summary>
    public enum SyncCommandType : byte
    {
        None = 0,
        
        ElementMovedData = 1,
        
        ElementMouseOver = 2,
        
        ZoomIn = 3,
        
        ZoomOut = 4,
        
        ZoomEnd = 5,
        
        PanningEnd = 6,
        
        SettingRect = 7,
        
        GoToLocation = 8,
        
        ChangeElementProperty = 9,
        
        OpenLayout = 10,
        
        ChangeLayout = 11,
        
        BringToFront = 12,
        
        GridAddCamera = 13,
        
        GridDelCamera = 14,
        
        GridViewSetting = 15,
        
        GridSetPage = 16,

        GridControl = 17,

        GridSetScreenMode = 18,

        GridSetClear = 19,

        AlarmStroke = 20,

        DisplayMode = 21,

        DisplayAreaInit = 22,

        MultiGridData = 23,

        MultiGridCellData = 24,

        MultiGridFullScreenData = 25,

        XamlControlStoryboard = 26,

        Reserved1 = 27,

        Reserved2 = 28,
    }
}
