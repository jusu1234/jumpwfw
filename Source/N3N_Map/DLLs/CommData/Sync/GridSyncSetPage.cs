﻿
namespace Innotive.InnoWatch.DLLs.CommData.Sync
{
    using System;
    using System.Xml.Serialization;

    [XmlRoot("GridSetPageData")]
    public class GridSetPageData : BaseSync
    {       
        [XmlAttribute("GridSyncGuid")]
        public Guid GridSyncGuid;
        
        [XmlAttribute("PageNo")]
        public int PageNo;

        //public string SaveDataToXML()
        //{
        //    XmlSerializer serializer = new XmlSerializer(typeof(GridSetPageData));
        //    MemoryStream memStream = new MemoryStream();
        //    XmlTextWriter xmlWriter = new XmlTextWriter(memStream, Encoding.UTF8);
        //    serializer.Serialize(xmlWriter, this);
        //    xmlWriter.Close();
        //    memStream.Close();

        //    string xmlData = Encoding.UTF8.GetString(memStream.GetBuffer());
        //    xmlData = xmlData.Substring(xmlData.IndexOf('<'));
        //    xmlData = xmlData.Substring(0, (xmlData.LastIndexOf('>') + 1));

        //    return xmlData;
        //}

        //public static GridSetPageData ReadDataFromXML(string xmlData)
        //{
        //    XmlSerializer serializer = null;

        //    serializer = new XmlSerializer(typeof(GridSetPageData));

        //    StringReader stringReader = new StringReader(xmlData);
        //    XmlTextReader xmlReader = new XmlTextReader(stringReader);

        //    GridSetPageData gridSetPageData = serializer.Deserialize(xmlReader) as GridSetPageData;

        //    xmlReader.Close();
        //    stringReader.Close();

        //    return gridSetPageData;
        //}
    }
}