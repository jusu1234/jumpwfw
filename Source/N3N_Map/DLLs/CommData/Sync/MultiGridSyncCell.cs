﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.IO;
using System.Xml;

namespace Innotive.InnoWatch.DLLs.CommData.Sync
{
    [XmlRoot("MultiGridCellInnerData")]
    public class MultiGridCellInnerData
    {
        [XmlAttribute("SyncGuidCell")]
        public Guid SyncGuidCell;

        [XmlAttribute("ElementSyncGuid")]
        public Guid ElementSyncGuid;

        [XmlAttribute("ElementID")]
        public string ElementID;

        public MultiGridCellInnerData()
        {
        }

        public static MultiGridCellInnerData ReadDataFromXML(string xmlData)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(MultiGridCellInnerData));
            StringReader stringReader = new StringReader(xmlData);
            XmlTextReader xmlReader = new XmlTextReader(stringReader);

            MultiGridCellInnerData data = serializer.Deserialize(xmlReader) as MultiGridCellInnerData;

            xmlReader.Close();
            stringReader.Close();

            return data;
        }

        public static MultiGridCellInnerData ReadDataFromXMLFile(string fileName)
        {
            string xmlData = string.Empty;

            TextReader streamReader = new StreamReader(fileName);
            xmlData = streamReader.ReadToEnd();

            return MultiGridCellInnerData.ReadDataFromXML(xmlData);
        }

        public string SaveDataToXML()
        {
            XmlSerializer serializer = new XmlSerializer(typeof(MultiGridCellInnerData));
            MemoryStream memStream = new MemoryStream();

            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Indent = true;
            settings.IndentChars = new string(' ', 4);
            settings.NewLineOnAttributes = false;
            settings.Encoding = Encoding.UTF8;

            XmlWriter xmlWriter = XmlWriter.Create(memStream, settings);
            serializer.Serialize(xmlWriter, this);
            xmlWriter.Close();
            memStream.Close();

            string xmlData = Encoding.UTF8.GetString(memStream.GetBuffer());
            xmlData = xmlData.Substring(xmlData.IndexOf('<'));
            xmlData = xmlData.Substring(0, (xmlData.LastIndexOf('>') + 1));

            return xmlData;
        }
    }
}
