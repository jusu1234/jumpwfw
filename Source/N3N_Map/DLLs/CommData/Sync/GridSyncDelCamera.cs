﻿
namespace Innotive.InnoWatch.DLLs.CommData.Sync
{
    using System;
    using System.Collections.Generic;
    using System.Xml.Serialization;

    [XmlRoot("GridDelCameraData")]
    public class GridDelCameraData : BaseSync
    {
        private List<string> _cameraList;
       
        [XmlAttribute("GridSyncGuid")]
        public Guid GridSyncGuid;
        
        [XmlArray("CameraList")]
        [XmlArrayItem(typeof(string))]
        public List<string> CameraList
        {
            get { return _cameraList; }
            set { _cameraList = value; }
        }

        public GridDelCameraData()
        {
            _cameraList = new List<string>();
        }

        //public string SaveDataToXML()
        //{
        //    XmlSerializer serializer = new XmlSerializer(typeof(GridDelCameraData));
        //    MemoryStream memStream = new MemoryStream();
        //    XmlTextWriter xmlWriter = new XmlTextWriter(memStream, Encoding.UTF8);
        //    serializer.Serialize(xmlWriter, this);
        //    xmlWriter.Close();
        //    memStream.Close();

        //    string xmlData = Encoding.UTF8.GetString(memStream.GetBuffer());
        //    xmlData = xmlData.Substring(xmlData.IndexOf('<'));
        //    xmlData = xmlData.Substring(0, (xmlData.LastIndexOf('>') + 1));

        //    return xmlData;
        //}

        //public static GridDelCameraData ReadDataFromXML(string xmlData)
        //{
        //    XmlSerializer serializer = null;

        //    serializer = new XmlSerializer(typeof(GridDelCameraData));

        //    StringReader stringReader = new StringReader(xmlData);
        //    XmlTextReader xmlReader = new XmlTextReader(stringReader);

        //    GridDelCameraData gridDelCameraData = serializer.Deserialize(xmlReader) as GridDelCameraData;

        //    xmlReader.Close();
        //    stringReader.Close();

        //    return gridDelCameraData;
        //}
    }
}