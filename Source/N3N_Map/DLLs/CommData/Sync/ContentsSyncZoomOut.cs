﻿
namespace Innotive.InnoWatch.DLLs.CommData.Sync
{
    using System;
    using System.Windows;
    using System.Xml.Serialization;

    [XmlRoot("ZoomOut")]
    public class ZoomOutData : BaseSync
    {
        [XmlAttribute("LayoutGuid")]
        public Guid LayoutGuid;

        [XmlAttribute("DesireRect")]
        public Rect DesireRect;
        
        //public string SaveDataToXML()
        //{
        //    XmlSerializer serializer = new XmlSerializer(typeof(ZoomOutData));
        //    MemoryStream memStream = new MemoryStream();
        //    XmlTextWriter xmlWriter = new XmlTextWriter(memStream, Encoding.UTF8);
        //    serializer.Serialize(xmlWriter, this);
        //    xmlWriter.Close();
        //    memStream.Close();

        //    string xmlData = Encoding.UTF8.GetString(memStream.GetBuffer());
        //    xmlData = xmlData.Substring(xmlData.IndexOf('<'));
        //    xmlData = xmlData.Substring(0, (xmlData.LastIndexOf('>') + 1));

        //    return xmlData;
        //}

        //public static ZoomOutData ReadDataFromXML(string xmlData)
        //{
        //    XmlSerializer serializer = null;

        //    serializer = new XmlSerializer(typeof(ZoomOutData));

        //    StringReader stringReader = new StringReader(xmlData);
        //    XmlTextReader xmlReader = new XmlTextReader(stringReader);

        //    ZoomOutData zoomOutData = serializer.Deserialize(xmlReader) as ZoomOutData;

        //    xmlReader.Close();
        //    stringReader.Close();

        //    return zoomOutData;
        //}
    }
}