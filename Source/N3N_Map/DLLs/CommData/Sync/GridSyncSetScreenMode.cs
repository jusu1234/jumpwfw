﻿
namespace Innotive.InnoWatch.DLLs.CommData.Sync
{
    using System;
    using System.Xml.Serialization;
    
    [XmlRoot("GridSetScreenModeData")]
    public class GridSetScreenModeData : BaseSync
    {       
        [XmlAttribute("GridSyncGuid")]
        public Guid GridSyncGuid;
        
        [XmlAttribute("FullscreenMode")]
        public GridScreenModeType FullscreenMode;
        
        [XmlAttribute("SelectedCameraID")]
        public string SelectedCameraID;

        //public string SaveDataToXML()
        //{
        //    XmlSerializer serializer = new XmlSerializer(typeof(GridSetScreenModeData));
        //    MemoryStream memStream = new MemoryStream();
        //    XmlTextWriter xmlWriter = new XmlTextWriter(memStream, Encoding.UTF8);
        //    serializer.Serialize(xmlWriter, this);
        //    xmlWriter.Close();
        //    memStream.Close();

        //    string xmlData = Encoding.UTF8.GetString(memStream.GetBuffer());
        //    xmlData = xmlData.Substring(xmlData.IndexOf('<'));
        //    xmlData = xmlData.Substring(0, (xmlData.LastIndexOf('>') + 1));

        //    return xmlData;
        //}

        //public static GridSetScreenModeData ReadDataFromXML(string xmlData)
        //{
        //    XmlSerializer serializer = null;

        //    serializer = new XmlSerializer(typeof(GridSetScreenModeData));

        //    StringReader stringReader = new StringReader(xmlData);
        //    XmlTextReader xmlReader = new XmlTextReader(stringReader);

        //    GridSetScreenModeData gridSetScreenModeData = serializer.Deserialize(xmlReader) as GridSetScreenModeData;

        //    xmlReader.Close();
        //    stringReader.Close();

        //    return gridSetScreenModeData;
        //}
    }
}