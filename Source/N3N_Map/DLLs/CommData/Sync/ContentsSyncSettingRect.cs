﻿
namespace Innotive.InnoWatch.DLLs.CommData.Sync
{
    using System;
    using System.Windows;
    using System.Xml.Serialization;

    [XmlRoot("SettingRectData")]
    public class SettingRectData : BaseSync
    {
        [XmlAttribute("LayoutGuid")]
        public Guid LayoutGuid;

        [XmlAttribute("Rect")]
        public Rect Rect;
        
        //public string SaveDataToXML()
        //{
        //    XmlSerializer serializer = new XmlSerializer(typeof(SettingRectData));
        //    MemoryStream memStream = new MemoryStream();
        //    XmlTextWriter xmlWriter = new XmlTextWriter(memStream, Encoding.UTF8);
        //    serializer.Serialize(xmlWriter, this);
        //    xmlWriter.Close();
        //    memStream.Close();

        //    string xmlData = Encoding.UTF8.GetString(memStream.GetBuffer());
        //    xmlData = xmlData.Substring(xmlData.IndexOf('<'));
        //    xmlData = xmlData.Substring(0, (xmlData.LastIndexOf('>') + 1));

        //    return xmlData;
        //}

        //public static SettingRectData ReadDataFromXML(string xmlData)
        //{
        //    XmlSerializer serializer = null;

        //    serializer = new XmlSerializer(typeof(SettingRectData));

        //    StringReader stringReader = new StringReader(xmlData);
        //    XmlTextReader xmlReader = new XmlTextReader(stringReader);

        //    SettingRectData settingRectData = serializer.Deserialize(xmlReader) as SettingRectData;

        //    xmlReader.Close();
        //    stringReader.Close();

        //    return settingRectData;
        //}
    }
}