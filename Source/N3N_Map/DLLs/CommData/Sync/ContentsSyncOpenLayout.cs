﻿
namespace Innotive.InnoWatch.DLLs.CommData.Sync
{
    using System;
    using System.Xml.Serialization;

    [XmlRoot("OpenLayout")]
    public class OpenLayoutData : BaseSync
    {
        [XmlAttribute("DisplayAreaName")]
        public string DisplayAreaName;

        [XmlAttribute("LayoutGuid")]
        public Guid LayoutGuid;

        [XmlAttribute("Effect")]
        public string Effect;

        [XmlAttribute("StartTime")]
        public double StartTime;

        [XmlAttribute("Duration")]
        public double Duration;

        //public string SaveDataToXML()
        //{
        //    XmlSerializer serializer = new XmlSerializer(typeof(OpenLayoutData));
        //    MemoryStream memStream = new MemoryStream();
        //    XmlTextWriter xmlWriter = new XmlTextWriter(memStream, Encoding.UTF8);
        //    serializer.Serialize(xmlWriter, this);
        //    xmlWriter.Close();
        //    memStream.Close();

        //    string xmlData = Encoding.UTF8.GetString(memStream.GetBuffer());
        //    xmlData = xmlData.Substring(xmlData.IndexOf('<'));
        //    xmlData = xmlData.Substring(0, (xmlData.LastIndexOf('>') + 1));

        //    return xmlData;
        //}

        //public static OpenLayoutData ReadDataFromXML(string xmlData)
        //{
        //    XmlSerializer serializer = null;

        //    serializer = new XmlSerializer(typeof(OpenLayoutData));

        //    StringReader stringReader = new StringReader(xmlData);
        //    XmlTextReader xmlReader = new XmlTextReader(stringReader);

        //    OpenLayoutData openLayoutData = serializer.Deserialize(xmlReader) as OpenLayoutData;

        //    xmlReader.Close();
        //    stringReader.Close();

        //    return openLayoutData;
        //}
    }
}