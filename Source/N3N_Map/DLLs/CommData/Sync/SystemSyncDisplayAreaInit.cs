﻿
namespace Innotive.InnoWatch.DLLs.CommData.Sync
{
    using System;
    using System.Collections.Generic;
    using System.Xml.Serialization;

    [XmlRoot("DisplayArea")]
    public class DisplayAreaInfo
    {
        [XmlAttribute("DisplayName")]
        public string DisplayName;

        [XmlAttribute("LayoutGuid")]
        public Guid LayoutGuid;

        public DisplayAreaInfo()
        {
        }

        public DisplayAreaInfo(string DisplayName, Guid LayoutGuid)
        {
            this.DisplayName = DisplayName;
            this.LayoutGuid = LayoutGuid;
        }
    }

    [XmlRoot("DisplayAreaInit")]
    public class DisplayAreaInitData : BaseSync
    {
        private List<DisplayAreaInfo> _displayAreaList;

        [XmlArray("DisplayAreaList")]
        [XmlArrayItem(typeof(DisplayAreaInfo))]
        public List<DisplayAreaInfo> DisplayAreaList
        {
            get { return _displayAreaList; }
            set { _displayAreaList = value; }
        }

        public DisplayAreaInitData()
        {
            _displayAreaList = new List<DisplayAreaInfo>();
        }

        //public string SaveDataToXML()
        //{
        //    XmlSerializer serializer = new XmlSerializer(typeof(DisplayAreaInitData));
        //    MemoryStream memStream = new MemoryStream();
        //    XmlTextWriter xmlWriter = new XmlTextWriter(memStream, Encoding.UTF8);
        //    serializer.Serialize(xmlWriter, this);
        //    xmlWriter.Close();
        //    memStream.Close();

        //    string xmlData = Encoding.UTF8.GetString(memStream.GetBuffer());
        //    xmlData = xmlData.Substring(xmlData.IndexOf('<'));
        //    xmlData = xmlData.Substring(0, (xmlData.LastIndexOf('>') + 1));

        //    return xmlData;
        //}

        //public static DisplayAreaInitData ReadDataFromXML(string xmlData)
        //{
        //    XmlSerializer serializer = null;

        //    serializer = new XmlSerializer(typeof(DisplayAreaInitData));

        //    StringReader stringReader = new StringReader(xmlData);
        //    XmlTextReader xmlReader = new XmlTextReader(stringReader);

        //    DisplayAreaInitData displayAreaInitData = serializer.Deserialize(xmlReader) as DisplayAreaInitData;

        //    xmlReader.Close();
        //    stringReader.Close();

        //    return displayAreaInitData;
        //}
    }
}