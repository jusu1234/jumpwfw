﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="BaseSync.cs" company="Innotive Inc. Korea">
//   Copyright (c) Innotive Corporation.  All rights reserved.
// </copyright>
// <summary>
//   기본 싱크 클래스.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Innotive.InnoWatch.DLLs.CommData.Sync
{
    using System;
    using System.IO;
    using System.Text;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// 기본 싱크 클래스.
    /// </summary>
    public class BaseSync
    {
        [XmlAttribute]
        public string TargetDisplayAreaName { get; set; }

        [XmlAttribute]
        public Guid TargetLayoutGuid { get; set; }

        [XmlAttribute]
        public SyncCommandType SyncCommandType { get; set; }

        /// <summary>
        /// Read Sync Class from Xml String.
        /// </summary>
        /// <param name="xml">Xml String.</param>
        /// <param name="type">Sync Class Type.</param>
        /// <returns>Made Sync Class.</returns>
        public static object ReadDataFromXml(string xml, Type type)
        {
            object sync = null;
            var serializer = new XmlSerializer(type);
            using (var stringReader = new StringReader(xml))
            {
                using (var xmlReader = new XmlTextReader(stringReader))
                {
                    sync = serializer.Deserialize(xmlReader);

                    xmlReader.Close();
                }

                stringReader.Close();
            }

            return sync;
        }

        /// <summary>
        /// Save Sync Class to Xml String.
        /// </summary>
        /// <returns>Made Xml Data.</returns>
        public string SaveDataToXml()
        {
            var xml = string.Empty;
            var serializer = new XmlSerializer(this.GetType());
            using (var memStream = new MemoryStream())
            {
                using (var xmlWriter = new XmlTextWriter(memStream, Encoding.UTF8))
                {
                    serializer.Serialize(xmlWriter, this);

                    xmlWriter.Close();
                }    

                memStream.Close();

                xml = Encoding.UTF8.GetString(memStream.GetBuffer());
                xml = xml.Substring(xml.IndexOf('<'));
                xml = xml.Substring(0, (xml.LastIndexOf('>') + 1));
            }

            return xml;
        }
    }
}
