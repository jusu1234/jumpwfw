﻿
namespace Innotive.InnoWatch.DLLs.CommData.Sync
{
    using System;
    using System.Xml.Serialization;

    [XmlRoot("ChangeLayout")]
    public class ChangeLayoutData : BaseSync
    {
        [XmlAttribute("OriginLayoutGuid")]
        public Guid OriginLayoutGuid;

        [XmlAttribute("NewLayoutGuid")]
        public Guid NewLayoutGuid;

        [XmlAttribute("Effect")]
        public string Effect;

        [XmlAttribute("StartTime")]
        public double StartTime;

        [XmlAttribute("Duration")]
        public double Duration;

        //public string SaveDataToXML()
        //{
        //    XmlSerializer serializer = new XmlSerializer(typeof(ChangeLayoutData));
        //    MemoryStream memStream = new MemoryStream();
        //    XmlTextWriter xmlWriter = new XmlTextWriter(memStream, Encoding.UTF8);
        //    serializer.Serialize(xmlWriter, this);
        //    xmlWriter.Close();
        //    memStream.Close();

        //    string xmlData = Encoding.UTF8.GetString(memStream.GetBuffer());
        //    xmlData = xmlData.Substring(xmlData.IndexOf('<'));
        //    xmlData = xmlData.Substring(0, (xmlData.LastIndexOf('>') + 1));

        //    return xmlData;
        //}

        //public static ChangeLayoutData ReadDataFromXML(string xmlData)
        //{
        //    XmlSerializer serializer = null;

        //    serializer = new XmlSerializer(typeof(ChangeLayoutData));

        //    StringReader stringReader = new StringReader(xmlData);
        //    XmlTextReader xmlReader = new XmlTextReader(stringReader);

        //    ChangeLayoutData changeLayoutData = serializer.Deserialize(xmlReader) as ChangeLayoutData;

        //    xmlReader.Close();
        //    stringReader.Close();

        //    return changeLayoutData;
        //}
    }
}