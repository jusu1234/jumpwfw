﻿
namespace Innotive.InnoWatch.DLLs.CommData.Sync
{
    using System.Xml.Serialization;

    public enum ConsoleDisplayMode 
    { 
        ConsoleMode, 
        ContentsMode, 
        AlarmMode 
    }

    public enum GridViewModeType
    {
        Auto,
        User
    };

    public enum GridScreenModeType
    {
        Fullscreen, // 전체화면        
        Zoomed, // 그리드 위에 Overlay 형태로 2/3 정도 크기로 띄워지는 전체화면        
        Normal  // 일반표출 화면
    };

    [XmlRoot("Camera")]
    public class CameraInfo
    {
        [XmlAttribute("ID")]
        public string ID;

        [XmlAttribute("Name")]
        public string Name;

        public CameraInfo()
        {
        }

        public CameraInfo(string ID, string Name)
        {
            this.ID = ID;
            this.Name = Name;
        }
    }
}
