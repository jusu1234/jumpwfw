using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows;
using System.Diagnostics;
using System.Windows.Documents;
using System.Windows.Media;
using EditBoxControls;

namespace Innotive.InnoWatch.DLLs.EditBoxControls
{
    /// <summary>
    /// EditBox is a customized cotrol that can switch between two modes: editing and normal.
    /// when it is in editing mode, a TextBox will show up to enable editing. When in normal mode, it 
    /// displays content as a TextBlock.
    /// 
    /// This control can only be used in GridView to enable editing.
    /// </summary>
    public class EditBoxForTreeView : Control
    {
        #region Static Constructor

        /// <summary>
        /// Static constructor
        /// </summary>
        static EditBoxForTreeView()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(EditBoxForTreeView), new FrameworkPropertyMetadata(typeof(EditBoxForTreeView)));
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Ocurr event when edit start.
        /// </summary>
        public event EventHandler<TextBoxEditingStartEventArgs> TextBoxEditingStartEvent;

        /// <summary>
        /// 
        /// </summary>
        public static readonly RoutedEvent eSetEditTextboxEvent =
            EventManager.RegisterRoutedEvent(
                "eLostKeyboardFocus", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(EditBoxForTreeView));


        /// <summary>
        /// The eLostKeyboardFocus.
        /// </summary>
        public event RoutedEventHandler eSetEditTextbox
        {
            add
            {
                this.AddHandler(eSetEditTextboxEvent, value);
            }

            remove
            {
                this.RemoveHandler(eSetEditTextboxEvent, value);
            }
        }

        /// <summary>
        /// Called when the Template's tree has been generated
        /// </summary>
        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            TextBlock textBlock = GetTemplateChild("PART_TextBlockPart") as TextBlock;
            Debug.Assert(textBlock != null, "No TextBlock!");

            _textBox = new TextBox();
            _adorner = new EditBoxAdornerForTreeView(textBlock, _textBox);
            AdornerLayer layer = AdornerLayer.GetAdornerLayer(textBlock); ;
            layer.Add(_adorner);

            (_textBox as TextBox).MaxLength = this.MaxTextLength;
            _textBox.KeyDown += new KeyEventHandler(OnTextBoxKeyDown);
            _textBox.KeyUp += new KeyEventHandler(OnTextBoxKeyUp);
            _textBox.LostKeyboardFocus += new KeyboardFocusChangedEventHandler(OnTextBoxLostKeyboardFocus);
            this.TextBoxEditingStartEvent += new EventHandler<TextBoxEditingStartEventArgs>(EditBoxForTreeView_TextBoxEditingStartEvent);

            //hook resize event to handle the the column resize. 
            HookTemplateParentResizeEvent();

            //hook the resize event to  handle TreeView resize cases.
            HookItemsControlEvents();

            _treeViewItem = GetDependencyObjectFromVisualTree(this, typeof(TreeViewItem)) as TreeViewItem;
            Debug.Assert(_treeViewItem != null, "No TreeViewItem found");
        }

        void EditBoxForTreeView_TextBoxEditingStartEvent(object sender, TextBoxEditingStartEventArgs e)
        {
            this.OldValue = e.OldValue;
        }

        #endregion

        private void OnTextBoxKeyUp(object sender, KeyEventArgs e)
        {
            e.Handled = true;
        }

        #region Protected Methods

        /// <summary>
        /// If the TreeView that contains EditBox is selected, when Mouse enters it, it can switch to editale mode now.
        /// </summary>
        protected override void OnMouseEnter(MouseEventArgs e)
        {
            base.OnMouseEnter(e);

            if(!this.CanEdit)
                return;

            if (!IsEditing && IsParentSelected)
            {
                _canBeEdit = true;
            }
        }

        /// <summary>
        /// If mouse leave it, no matter wheher the TreeViewItem that contains it is selected or not, 
        /// it can not switch into Editable mode.
        /// </summary>
        protected override void OnMouseLeave(MouseEventArgs e)
        {
            base.OnMouseLeave(e);

            _isMouseWithinScope = false;
            _canBeEdit = false;
        }

        /// <summary>
        /// When TreeViewItem that contains EditBox is selected and this event happens, if one of the following conditions is satisified, EditBox will be switched EditBox into editable mode.
        /// 1. A MouseEnter happened before this. 
        /// 2. Mouse never move out of it since the TreeViewItem was selected.
        /// </summary>
        /// <param name="e"></param>
        protected override void OnMouseUp(MouseButtonEventArgs e)
        {
            base.OnMouseUp(e);

            if (e.ChangedButton == MouseButton.Right || e.ChangedButton == MouseButton.Middle)
                return;

            if (!this.CanEdit)
                return;

            if (!IsEditing)
            {
                if (!e.Handled && (_canBeEdit || _isMouseWithinScope))
                {
                    IsEditing = true;
                }

                //Handle a specific case: After a TreeViewItem was selected by clicking it,
                // Clicking the EditBox again should switch into Editable mode.
                if (IsParentSelected)
                    _isMouseWithinScope = true;
            }
        }

        #endregion

        #region Public Properties

        #region Value

        /// <summary>
        /// ValueProperty DependencyProperty.
        /// </summary>
        public static readonly DependencyProperty ValueProperty =
                DependencyProperty.Register(
                        "Value",
                        typeof(object),
                        typeof(EditBoxForTreeView),
                        new FrameworkPropertyMetadata(null, ValueChanged));

        public static void ValueChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            //System.Diagnostics.Debug.WriteLine("EditBox ValueChanged! : " + (d as EditBox).Value);
        }

        /// <summary>
        /// The value of the EditBox
        /// </summary>
        public object Value
        {
            get
            {
                return GetValue(ValueProperty);
            }
            set
            {
                SetValue(ValueProperty, value);
                this.EditTextbox.Text = value.ToString();
            }
        }

        #endregion


        #region OldValue
        /// <summary>
        /// The old value of EditTextBox.
        /// </summary>
        public string OldValue { get; set; }

        #endregion


        #region IsEditing

        /// <summary>
        /// IsEditingProperty DependencyProperty
        /// </summary>
        public static DependencyProperty IsEditingProperty =
                DependencyProperty.Register(
                        "IsEditing",
                        typeof(bool),
                        typeof(EditBoxForTreeView),
                        new FrameworkPropertyMetadata(false));


        /// <summary>
        /// True if the control is in editing mode
        /// </summary>
        public bool IsEditing
        {
            get { return (bool)GetValue(IsEditingProperty); }
            private set
            {
                if (value)
                {

                    if (this.TextBoxEditingStartEvent != null)
                    {
                        var TextBoxEditingStartEvent_Clone = this.TextBoxEditingStartEvent;
                        TextBoxEditingStartEvent_Clone(this, new TextBoxEditingStartEventArgs(this.EditTextbox.Text));
                    }

                }
                SetValue(IsEditingProperty, value);
                _adorner.UpdateVisibilty(value);
            }
        }

        #endregion

        #region CanEdit

        public static DependencyProperty CanEditProperty =
            DependencyProperty.Register(
                                "CanEdit",
                                typeof(bool),
                                typeof(EditBoxForTreeView),
                                new FrameworkPropertyMetadata(true));

        public bool CanEdit
        {
            get { return (bool)this.GetValue(CanEditProperty); }
            set { this.SetValue(CanEditProperty, value); }
        }

        #endregion

        #region IsParentSelected

        /// <summary>
        /// Whether the TreeViewItem that contains it is selected.
        /// </summary>
        private bool IsParentSelected
        {
            get
            {
                if (_treeViewItem == null)
                    return false;
                else
                    return _treeViewItem.IsSelected;
            }
        }

        #endregion


        #region MaxTextLength

        /// <summary>
        /// The MaxTextLength of EditTextBox.
        /// </summary>
        public int MaxTextLength { get; set; }

        #endregion


        #region EditTextbox

        /// <summary>
        /// Gets _textBox.
        /// </summary>
        public TextBox EditTextbox
        {
            get { return this._textBox as TextBox; }
        }

        #endregion


        #endregion


        #region Private Methods

        /// <summary>
        /// When in editable mode,Pressing Enter Key and F2 Key make it switch into normal model
        /// </summary>
        private void OnTextBoxKeyDown(object sender, KeyEventArgs e)
        {
            if (!this.CanEdit)
                return;

            if (IsEditing && (e.Key == Key.Enter || e.Key == Key.F2))
            {
                IsEditing = false;
                _canBeEdit = false;

                var text = (this._textBox as TextBox).Text;

                this.isEnterDown = true;
                this.Value = text;
                this.RaiseEvent(new RoutedEventArgs(eSetEditTextboxEvent, null));
            }
        }

        /// <summary>
        /// When in editable mode, losing focus make it switch into normal mode.
        /// </summary>
        private void OnTextBoxLostKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
        {
            IsEditing = false;
            //System.Diagnostics.Debug.WriteLine("ontextboxlostkeyboardofocus");

            if (!this.isEnterDown)
            {
                this.Value = this.OldValue;
                this.EditTextbox.Text = this.OldValue;
            }
            this.isEnterDown = false;
        }

        /// <summary>
        /// Set IsEditing to false when parent has changed its size
        /// </summary>
        private void OnCouldSwitchToNormalMode(object sender, RoutedEventArgs e)
        {
            //IsEditing = false;
            //System.Diagnostics.Debug.WriteLine("OnCouldSwitchToNormalMode");
        }

        /// <summary>
        /// Walk the visual tree to find the ItemsControl and hook its some events on it.
        /// </summar
        private void HookItemsControlEvents()
        {
            _itemsControl = GetDependencyObjectFromVisualTree(this, typeof(ItemsControl)) as ItemsControl;
            if (_itemsControl != null)
            {
                //The reason of hooking Resize/ScrollChange/MouseWheel event is :
                //when one of these event happens, the EditBox should be switched into editable mode.
                _itemsControl.SizeChanged += new SizeChangedEventHandler(OnCouldSwitchToNormalMode);
                _itemsControl.AddHandler(ScrollViewer.ScrollChangedEvent, new RoutedEventHandler(OnScrollViewerChanged));
                _itemsControl.AddHandler(ScrollViewer.MouseWheelEvent, new RoutedEventHandler(OnCouldSwitchToNormalMode), true);
            }
        }

        /// <summary>
        /// If EditBox is in editable mode, scrolling TreeView should switch it into normal mode.
        /// </summary>
        private void OnScrollViewerChanged(object sender, RoutedEventArgs args)
        {
            if (IsEditing && Mouse.PrimaryDevice.LeftButton == MouseButtonState.Pressed)
            {
                IsEditing = false;
            }
        }

        /// <summary>
        /// Walk visual tree to find the first DependencyObject of the specific type.
        /// </summary>
        private DependencyObject GetDependencyObjectFromVisualTree(DependencyObject startObject, Type type)
        {
            //Iterate the visual tree to get the parent(ItemsControl) of this control
            DependencyObject parent = startObject;
            while (parent != null)
            {
                if (type.IsInstanceOfType(parent))
                    break;
                else
                    parent = VisualTreeHelper.GetParent(parent);
            }

            return parent;
        }

        /// <summary>
        /// Get the TemplatedParent and hook its resize event. 
        /// The reason of hooking this event is that when resize a column, the EditBox should switch editable mode.
        /// </summary>
        private void HookTemplateParentResizeEvent()
        {
            FrameworkElement parent = TemplatedParent as FrameworkElement;
            if (parent != null)
            {
                parent.SizeChanged += new SizeChangedEventHandler(OnCouldSwitchToNormalMode);
            }
        }

        #endregion

        #region Private variable

        private EditBoxAdornerForTreeView _adorner; //The AdornerLayer on the TextBlock
        private FrameworkElement _textBox; //TextBox in visual tree.
        private bool _canBeEdit = false;     //Whether EditBox can swithc into Editable mode. If the TreeViewItem that contain the EditBox is selected, when mouse enter the EditBox, it becomes true
        private bool _isMouseWithinScope = false; //whether can swithc into Editable mode. If the TreeViewItem that contain the EditBox is selected, when mouse is over the EditBox, it becomes true.
        private ItemsControl _itemsControl; //The TreeView that contains it.
        private TreeViewItem _treeViewItem; //The TreeViewItem that contains it.
        private bool isEnterDown = false; // Yes or no Enter key Down after Editbox editing.

        #endregion
    }
}
