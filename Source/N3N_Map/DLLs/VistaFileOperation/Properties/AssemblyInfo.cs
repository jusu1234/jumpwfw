// Stephen Toub

using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("Innotive Korea FileOperation")]
[assembly: AssemblyDescription("Innotive Korea FileOperation")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Innotive Korea")]
[assembly: AssemblyProduct("Innotive Korea FileOperation")]
[assembly: AssemblyCopyright("Copyright © Innotive Korea 2008")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
[assembly: ComVisible(false)]
[assembly: Guid("3453bda0-100a-434e-88b7-17c5ba332d2f")]
[assembly: AssemblyVersion("3.0.1.75")]
[assembly: AssemblyFileVersion("3.0.1.75")]
