﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Innotive.InnoWatch.DLLs.TreeViewControls
{
    /// <summary>
    /// StackedTreeViewItem Control 
    /// </summary>
    public class StackedTreeViewItem :  TreeViewItem
    {
        /// <summary>
        /// 클래스 정적 생성자
        /// </summary>
        static StackedTreeViewItem()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(StackedTreeViewItem), new FrameworkPropertyMetadata(typeof(StackedTreeViewItem)));
        }

        /// <summary>
        /// 클래스 동적 생성자
        /// </summary>
        public StackedTreeViewItem()
        {
            this.RequestBringIntoView += new RequestBringIntoViewEventHandler(StackedTreeViewItem_RequestBringIntoView);
        }

        /// <summary>
        /// 클래스 소멸자
        /// </summary>
        ~StackedTreeViewItem()
        {
            this.RequestBringIntoView -= new RequestBringIntoViewEventHandler(StackedTreeViewItem_RequestBringIntoView);
        }

        /// <summary>
        /// TreeViewItem 을 선택했을 때 자동으로 스크롤 되는것을 막기위한 함수.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void StackedTreeViewItem_RequestBringIntoView(object sender, RequestBringIntoViewEventArgs e)
        {
            // 드레그 시 스크롤 따라가도록 하기 위해 이벤트를 사용해야함. - by Alvin (101224)
            //e.Handled = true;
        }

        #region DepthUnit DP

        public static readonly DependencyProperty DepthUnitProperty =
            DependencyProperty.Register("DepthUnit",
            typeof(double), typeof(StackedTreeViewItem), new UIPropertyMetadata(10.0));

        public double DepthUnit
        {
            get { return (double)GetValue(DepthUnitProperty); }
            set { SetValue(DepthUnitProperty, value); }
        }

        #endregion // DepthUnit DP

        #region Depth DP

        public static readonly DependencyProperty DepthProperty =
            DependencyProperty.Register("Depth",
            typeof(int), typeof(StackedTreeViewItem), new UIPropertyMetadata(0, new PropertyChangedCallback(DepthChanged)));

        public int Depth
        {
            get { return (int)GetValue(DepthProperty); }
            protected set { SetValue(DepthProperty, value); }
        }

        public static void DepthChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            StackedTreeViewItem myTreeViewItem = d as StackedTreeViewItem;
            myTreeViewItem.ActualDepth = myTreeViewItem.Depth * myTreeViewItem.DepthUnit;
        }

        #endregion // Depth DP

        #region ActualDepth DP

        public static readonly DependencyProperty ActualDepthProperty =
            DependencyProperty.Register("ActualDepth",
            typeof(double), typeof(StackedTreeViewItem), new UIPropertyMetadata(0.0));

        public double ActualDepth
        {
            get { return (double)GetValue(ActualDepthProperty); }
            protected set { SetValue(ActualDepthProperty, value); }
        }

        #endregion // ActualDepth DP

        #region 사용자 정의 트리 아이템 사용

        protected override DependencyObject GetContainerForItemOverride()
        {
            return new StackedTreeViewItem();
        }

        protected override bool IsItemItsOwnContainerOverride(object item)
        {
            return item is StackedTreeViewItem;
        }

        #endregion // 사용자 정의 트리 아이템 사용
                
        public void ApplyItemDepth(int depth)
        {
            this.Depth = depth;
        }

        public void ApplyItemContanerSource(Style style)
        {
            this.ItemContainerStyle = style;
        }

        
    }
}
