using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("INNOWATCH ActionEffects Library")]
[assembly: AssemblyDescription("INNOWATCH ActionEffects Library")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Innotive inc. Korea")]
[assembly: AssemblyProduct("INNOWATCH ActionEffects Library")]
[assembly: AssemblyCopyright("Copyright ⓒ Innotive inc. Korea 2010")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("d26cc3df-a54d-463d-a9fd-a3766b6a2087")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [-assembly: AssemblyVersion("3.0.1.75")]
[assembly: AssemblyVersion("3.0.1.75")]
[assembly: AssemblyFileVersion("3.0.1.75")]
