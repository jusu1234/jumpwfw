﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media.Animation;
using System.Windows;
using System.Windows.Media;
using System.Windows.Controls;
using System.Windows.Media.Imaging;

namespace Innotive.InnoWatch.DLLs.ActionEffects
{
    public class ActionEffectBaseStrategyForTransition : IActionEffectStrategy
    {
        private FrameworkElement _newElement = null;
        private FrameworkElement _oldElement = null;
        private double _startTime = 0.0;
        private double _duration = 0.0;        

        public ActionEffectBaseStrategyForTransition(FrameworkElement newElement, FrameworkElement oldElement, double startTime, double duration)
        {
            this._newElement = newElement;
            this._oldElement = oldElement;
            this._startTime = startTime;
            this._duration = duration;
        }

        #region IActionEffectStrategy Members

        public TransitionEffects.TransitionEffect Effect { get; set; }

        private Action _endAction = null;
        private DoubleAnimation _da = null;        
        //private Random _rand = new Random();

        virtual public void Begin(Action endAction)
        {
            
            this._endAction = endAction;

            if (Effect != null)
            {
                //TransitionEffects.RandomizedTransitionEffect randEffect = Effect as TransitionEffects.RandomizedTransitionEffect;
                //if (randEffect != null)
                //{
                //    randEffect.RandomSeed = this._rand.NextDouble();
                //}

                _da = new DoubleAnimation(0.0, 1.0, new Duration(TimeSpan.FromSeconds(this._duration)), FillBehavior.HoldEnd);
                _da.BeginTime = TimeSpan.FromSeconds(this._startTime);
                _da.AccelerationRatio = 0.5;
                _da.DecelerationRatio = 0.5;
                _da.Completed += new EventHandler(TransitionCompleted);
                
                Effect.BeginAnimation(TransitionEffects.TransitionEffect.ProgressProperty, _da);

                if (this._oldElement != null)
                {   
                    VisualBrush vb = new VisualBrush(this._oldElement);
                    vb.Viewbox = new Rect(Canvas.GetLeft(this._oldElement), Canvas.GetTop(this._oldElement), this._oldElement.ActualWidth, this._oldElement.ActualHeight);
                    vb.ViewboxUnits = BrushMappingMode.Absolute;

                    Effect.OldImage = vb;
                }

                this._newElement.Effect = Effect;                
            }
        }

        private void TransitionCompleted(object sender, EventArgs e)
        {
            this._newElement.Effect = null;

            this._newElement = null;
            this._oldElement = null;

            if (this._endAction != null)
            {
                this._endAction.Invoke();
            }

            _da.Completed -= new EventHandler(TransitionCompleted);
        }

        #endregion
    }
}
