﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media.Animation;
using System.Windows;
using System.Windows.Media;

namespace Innotive.InnoWatch.DLLs.ActionEffects
{
    public class ActionEffectStrategyForSaturateTransition : ActionEffectBaseStrategyForTransition
    {
        public ActionEffectStrategyForSaturateTransition(FrameworkElement newElement, FrameworkElement oldElement, double startTime, double duration)
            : base(newElement, oldElement, startTime, duration)
        {

        }

        public override void Begin(Action endAction)
        {
            Effect = new TransitionEffects.SaturateTransitionEffect();

            base.Begin(endAction);
        }
    }
}
