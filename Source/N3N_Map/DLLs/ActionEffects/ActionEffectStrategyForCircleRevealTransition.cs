﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media.Animation;
using System.Windows;
using System.Windows.Media;

namespace Innotive.InnoWatch.DLLs.ActionEffects
{
    public class ActionEffectStrategyForCircleRevealTransition : ActionEffectBaseStrategyForTransition
    {
        private double _fuzzyAmount = 0.0;

        public ActionEffectStrategyForCircleRevealTransition(FrameworkElement newElement, FrameworkElement oldElement, double startTime, double duration, double fuzzyAmount)
            : base(newElement, oldElement, startTime, duration)
        {

            this._fuzzyAmount = fuzzyAmount;
        }

        public override void Begin(Action endAction)
        {
            Effect = new TransitionEffects.CircleRevealTransitionEffect() { FuzzyAmount = this._fuzzyAmount };

            base.Begin(endAction);
        }
    }
}
