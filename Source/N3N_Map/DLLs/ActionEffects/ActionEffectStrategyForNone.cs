﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace Innotive.InnoWatch.DLLs.ActionEffects
{
    public class ActionEffectStrategyForNone : IActionEffectStrategy
    {
        private FrameworkElement _newElement = null;
        private FrameworkElement _oldElement = null;
        private double _startTime = 0.0;
        private double _duration = 0.0;

        public ActionEffectStrategyForNone(FrameworkElement newElement, FrameworkElement oldElement, double startTime, double duration)
        {
            this._newElement = newElement;
            this._oldElement = oldElement;
            this._startTime = startTime;
            this._duration = duration;
        }

        #region IActionEffectStrategy Members

        public void Begin(Action endAction)
        {
            _newElement = null;
            _oldElement = null;

            if (endAction != null)
            {
                endAction.Invoke();
            }
        }

        #endregion
    }
}
