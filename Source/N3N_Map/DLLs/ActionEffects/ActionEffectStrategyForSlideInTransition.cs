﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media.Animation;
using System.Windows;
using System.Windows.Media;

namespace Innotive.InnoWatch.DLLs.ActionEffects
{
    public class ActionEffectStrategyForSlideInTransition : ActionEffectBaseStrategyForTransition
    {
        private Vector _slideAmount = new Vector(1.0, 0.0);

        public ActionEffectStrategyForSlideInTransition(FrameworkElement newElement, FrameworkElement oldElement, double startTime, double duration, Vector slideAmount)
            : base(newElement, oldElement, startTime, duration)
        {
            this._slideAmount = slideAmount;
        }

        public override void Begin(Action endAction)
        {
            Effect = new TransitionEffects.SlideInTransitionEffect() { SlideAmount = this._slideAmount };

            base.Begin(endAction);
        }
    }
}
