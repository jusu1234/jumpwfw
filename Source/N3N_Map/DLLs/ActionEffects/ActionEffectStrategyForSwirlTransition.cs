﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media.Animation;
using System.Windows;
using System.Windows.Media;

namespace Innotive.InnoWatch.DLLs.ActionEffects
{
    public class ActionEffectStrategyForSwirlTransition : ActionEffectBaseStrategyForTransition
    {
        private double _param = Math.PI * 4;

        public ActionEffectStrategyForSwirlTransition(FrameworkElement newElement, FrameworkElement oldElement, double startTime, double duration, double param)
            : base(newElement, oldElement, startTime, duration)
        {
            this._param = param;
        }

        public override void Begin(Action endAction)
        {
            Effect = new TransitionEffects.SwirlTransitionEffect(this._param);

            base.Begin(endAction);
        }
    }
}
