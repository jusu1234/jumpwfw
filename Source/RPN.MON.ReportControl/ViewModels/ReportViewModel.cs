﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.Composition;
using System.Collections.ObjectModel;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Controls;

using RPN.Common.Core.ViewModel;
using RPN.Common.Core.Command;
using RPN.Common.Core.Message;
using RPN.Common.Core.Service;
using RPN.Common.Core.Tasks;
using RPN.Common.Core.Collection;

using RPN.MON.Data;
using RPN.MON.Data.Models;
//using RPN.MON.ReportControl.Services;


namespace RPN.MON.ReportControl.ViewModels
{
    [Export(typeof(IViewModel))]
    [View(typeof(RPN.MON.ReportControl.Views.ReportView))]
    public class ReportViewModel : ViewModelBase, IViewModel
    {
         #region IView member
        static string viewName_;
        public string ViewName
        {
            get { return viewName_; }
            set
            {
                viewName_ = value;
                RaisePropertyChanged(() => ViewName);
            }
        }
        #endregion

        public ReportViewModel()
            : base("ReportView", true, false)
        {
            ViewName = "Report";
        }
    }
}
