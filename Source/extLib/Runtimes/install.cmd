@echo off

echo Installing VC Runtime x86 2005 sp1...
echo Just a moment, please
vcredist_x86_2005sp1.exe /q /t:c:

echo Installing VC Runtime x64 2005 sp1...
echo Just a moment, please
vcredist_x64_2005sp1.exe /q /t:c:

echo Installing VC Runtime x86 2008 sp1...
echo Just a moment, please
vcredist_x86_2008sp1.exe /q
echo Installing VC Runtime x64 2008 sp1...
vcredist_x64_2008sp1.exe /q

echo Installing VC Runtime x86 2010...
en_vc++_2010_redist_package_x86_x64_ia64_508951.exe /q
echo Just a moment, please

echo Installing VC Runtime x86 2010 sp1...
echo Just a moment, please
vcredist_x86_2010sp1.exe /q

echo Installing VC Runtime x64 2010 sp1...
echo Just a moment, please
vcredist_x64_2010sp1.exe /q

echo Installing .NET Framework 4.0...
echo Just a moment, please
en_.net_framework_4_full_x86_x64_508940.exe /passive /norestart