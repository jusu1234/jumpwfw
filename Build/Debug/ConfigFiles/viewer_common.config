﻿<?xml version="1.0" encoding="utf-8"?>
<CommandConfig xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <PackageVersion>2.1.1.78</PackageVersion>
  <Common>
    <Infomation>
      <Project>
        <Id Comment="프로그램 실행시 기준값으로 사용하는 Project의 ID 를 지정합니다." Editable="true" Value="" />
      </Project>
    </Infomation>
    <Camera>
      <UI>
        <NameLabelFontColor Comment="카메라 레이블을 표시할 색깔을 설정합니다. 값으로 HEX(예, #FFFFFFFF)와 색상 문자열(예, Red, Blue, ..) 입력 할 수 있습니다. (기본값: 노란색)" Editable="true" Value="" />
        <NameLabelFontSizeRatio Comment="카메라 레이블이 카메라 컨트롤에서 차지할 비율을 설정합니다.  비율은 카메라 컨트롤 높이 기준으로 계산됩니다." Editable="true" Value="10" />
        <NameLabelFormat Comment="카메라 레이블 표시 형식을 설정합니다. 이름은 {NAME}, 아이디는 {ID}를 입력하면 됩니다." Editable="true" Value="{NAME} ({ID})" />
        <NameLabelVisible Comment="카메라 레이블 표시 여부를 설정합니다. (true/false)" Editable="true" Value="true" />
        <NameLabelFontFamily Comment="카메라 레이블을 표시할 폰트를 설정합니다. 폰트는 장비에 설치가 되어 있어야 합니다." Editable="true" Value="" />
        <NameLabelFontStyle Comment="카메라 레이블을 표시할 폰트 스타일을 설정합니다. (기본값: Normal, 설정값: Normal, Italic, Oblique)" Editable="true" Value="Normal" />
        <NameLabelFontWeight Comment="카메라 레이블을 표시할 폰트 무게을 설정합니다. (기본값: Normal, 설정값: Black, Bold, DemiBold, ExtraBlack, ExtraBold, ExtraLight, Heavy, Light, Medium, Normal, Regular, SemiBold, Thin, UltraBlack, UltraBold, UltraLight)" Editable="true" Value="Normal" />
        <NameLabelDropShadowEffect Comment="카메라 레이블을 표시시 그림자를 설정합니다. Use는 사용여부를 설정합니다. Depth는 그림자의 너비를 설정합니다. Direction은 그림자의 방향(0~360)을 설정합니다. Color는 그림자의 색상을 설정합니다. Opacity는 그림자의 투명 정도(0.0~1.0)을 설정합니다. BlurRadius는 그림자의 부드러움 정도를 설정합니다." Editable="true" Use="false" Depth="4" Direction="330" Color="Black" Opacity="0.5" BlurRadius="4" />
        <NameLabelFourWayShadow Comment="카메라 레이블을 표시시 네방향 그림자를 설정합니다. Use는 사용여부를 설정합니다. Distance는 그림자의 거리를 설정합니다(기본값 0.4). Color는 그림자의 색상을 설정합니다. Opacity는 그림자의 투명 정도(0.0~1.0)을 설정합니다." Editable="true" Use="false" Distance="0.4" Color="Black" Opacity="1" />
        <NameLabelAlignment Comment="카메라 레이블의 정렬 기준을 설정합니다.(설정 가능한 값 : TopLeft, BottomLeft, TopRight, BottomRight)" Editable="true" Value="TopLeft" />
      </UI>
    </Camera>
    <Window>
      <Resource>
        <Language Comment="표시 언어를 설정합니다. (en-US와 ko-KR만 지원함.)" Editable="true" Value="en-US" />
      </Resource>
    </Window>
  </Common>
  <DisplayCommon>
    <Camera>
      <Clipping>
        <HighStreamVideo Comment="고화질 영상의 외곽에 여백이 있을 경우 자르는 크기를 설정합니다. (단위: Pixel)" Editable="true" Left="0" Top="0" Right="0" Bottom="0" />
        <Merger Comment="Merger 전체 영상의 외곽에 여백이 있을 경우 자르는 크기를 설정합니다. Bottom 값만 사용합니다. (단위: Pixel)" Editable="true" Left="0" Top="0" Right="0" Bottom="0" />
        <MergerCell Comment="저화질 개별 영상의 외곽에 여백이 있을 경우 자르는 크기를 설정합니다. (단위: Pixel). UseCropMergerCellByIndividualSetting를 true로 설정하면 개별 카메라 설정되어 있는 짜르기 속성값을 사용하여 그 값에 대한 저화질 해상도 비율에 따라 영상을 자릅니다." Editable="true" Left="0" Top="0" Right="0" Bottom="0" UseCropMergerCellByIndividualSetting="false" />
      </Clipping>
      <DisplayRule>
        <ScreenOutsideBlack Comment="화면 바깥 영상을 끌지 여부를 설정합니다. 사용하면 성능 향상이 있습니다. (true/false)" Editable="true" Value="false" />
        <StopScreenOutsideLowLevelVideo Comment="화면 바깥 저해상도 영상을 백그라운드 재생 여부를 설정합니다. 사용하면 성능 향상이 있습니다. (true/false)" Editable="true" Value="false" />
        <PauseCameraWhenMoving Comment="화면 확대/축소 및 드래그시 영상을 정지할지 여부를 설정합니다. 사용하면 성능 향상이 있습니다. (true/false)" Editable="true" Value="true" />
        <ShowLimitWidth Comment="화면에 실제 표시되는 너비가 설정된 값보다 작으면 영상을 표시하지 않음. (단위: Pixel, 0이하 값이면 사용하지 않음.)" Editable="true" Value="10" />
        <ShowLimitHeight Comment="화면에 실제 표시되는 높이가 설정된 값보다 작으면 영상을 표시하지 않음. (단위: Pixel, 0이하 값이면 사용하지 않음.)" Editable="true" Value="10" />
      </DisplayRule>
      <PTZ>
        <Use Comment="카메라 PTZ 제어 사용 여부를 설정합니다. (true/false)" Editable="true" Value="true" />
      </PTZ>
      <Stream>
        <ReceiveEnable Comment="카메라 영상을 수신을 할지 여부를 설정합니다. (true/false)" Editable="true" Value="true" />
      </Stream>
      <SwitchingRule>
        <HighPositionStandard Comment="저화질/고화질로 전환 기준을 설정합니다. (0: 모니터 중앙 중심, 1: DisplayArea 중심, 2: ContentsMode에서 모니터, StageMode에서 DisplayArea 중심)" Editable="true" Sorting="0" />
        <SwitchingSpeed Comment="고화질에서 저화질로 전환되는 속도를 설정합니다. (단위: 초)" Editable="true" Value="0.2" />
      </SwitchingRule>
      <UI>
        <StatusLabelVisible Comment="카메라 컨트롤에 저화질/고화질 여부 라벨을 표시합니다. (true/false)" Editable="true" Value="false" />
      </UI>
    </Camera>
    <Communication>
      <ConnectionStatusIcon>
        <Use Comment="서버 연결 상태 표시 여부를 설정합니다. (true/false)" Editable="true" Value="true" />
      </ConnectionStatusIcon>
      <CameraControlService>
        <IP Comment="카메라 제어 서비스의 IP를 설정합니다. (0.0.0.0이나 빈문자열이 설정되어 있으면 자동으로 사용하지 않게 설정됩니다.)" Editable="true" Value="0.0.0.0" />
      </CameraControlService>
      <ExternalCommandService>
        <IP Comment="외부 명령 서비스의 IP를 설정합니다. (0.0.0.0이나 빈문자열이 설정되어 있으면 자동으로 사용하지 않게 설정됩니다.)" Editable="true" Value="0.0.0.0" />
      </ExternalCommandService>
      <ControlServer>
        <IP Comment="컨트롤 서버 IP를 설정합니다. (0.0.0.0이나 빈문자열이 설정되어 있으면 자동으로 사용하지 않게 설정됩니다.)" Editable="true" Value="0.0.0.0" />
        <DataPort Comment="컨트롤 서버의 Data Service의 포트를 설정합니다. (기본값: 25000)" Editable="true" Value="25000" />
        <MessagePort Comment="컨트롤 서버의 Message Service의 포트를 설정합니다. (기본값: 20000)" Editable="true" Value="20000" />
        <PollingURLPort Comment="XamlViewerControl이 URL Polling시 컨트롤 서버의 Model  참조할 경우 사용할 포트를 설정합니다. (기본값: 31000)" Editable="true" Value="31000" />
      </ControlServer>
      <ControlManager>
        <Path Comment="iControlManager가 설치된 경로를 설정합니다. (경로에 파일이 없으면 자동으로 사용하지 않습니다.)" Editable="true" Value="C:\Program Files\Innowatch\iControl Manager\iControlManager.exe" />
      </ControlManager>
      <Playback>
        <PlaybackPath Comment="Playback CMS가 설치된 경로를 설정합니다. (경로에 파일이 없으면 자동으로 사용하지 않습니다.)" Editable="true" Value="C:\Program Files\Innowatch\Playback\PlayBackDialog.exe" />
      </Playback>
    </Communication>
    <Control>
      <Layout>
        <PanningStartPixel Comment="마우스 드래그 시 최초 이동이 동작하는 거리를 설정합니다. (단위: Pixel)" Editable="true" Value="0" />
        <ZoomAnimationSpeed Comment="확대/축소 동작 속도를 설정합니다. (단위: 초)" Editable="true" Value="0.15" />
        <UseMouseWheel Comment="레이아웃에서 확대 축소시 마우스 휠 사용 여부를 설정 합니다. (true/false)" Editable="true" Value="false" />
      </Layout>
      <Xaml>
        <RefreshTimeout Comment="컨트롤 서버로 부터 받은 ERP Date가 그려지는 Timeout을 설정합니다. (단위: msec)" Editable="true" Value="100" />
        <PollingLimitMinimumWidth Comment="XamlViewerControl이 Polling을 실행할 최소 너비 입니다. (이 크기보다 작으면 Polling 하지 않습니다.)" Editable="true" Value="0" />
        <PollingLimitMinimumHeight Comment="XamlViewerControl이 Polling을 실행할 최소 높이 입니다. (이 크기보다 작으면 Polling 하지 않습니다.)" Editable="true" Value="0" />
      </Xaml>
    </Control>
    <Device>
      <Mouse>
        <CursorVisible Comment="마우스 커서 보이기 여부를 설정합니다.  (true/false)" Editable="true" Value="true" />
      </Mouse>
      <Ethernet>
        <VideoIP Comment="영상 수신에 사용할 랜카드 주소를 설정합니다. (맨뒤에 255를 설정하면 그 대역에 주소를 자동으로 찾습니다.)" Editable="true" Value="0.0.0.255" />
      </Ethernet>
      <Graphic>
        <UseHardwareAcceleration Comment="하드웨어 가속 사용 여부를 결정합니다. 가급적 true로 설정하시기 바랍니다. (true/false)" Editable="true" Value="true" />
      </Graphic>
    </Device>
    <File>
      <Environment>
        <Path Comment="이값은 ReadOnly 속성입니다. 변경되지 않습니다." Editable="true" Value="D:\Sources\src\bin\Debug\iViewer\QuickSettings.xml" />
      </Environment>
    </File>
    <Filter>
      <Decoder>
        <H264 Comment="H264 디코딩에 사용할 디코더를 설정합니다. (0: MicrosoftMpeg2, 1: CoreAVC, 2: InnotiveVideoDecoder(IPP), 3: LG,   4: Innodep,   5: FFDShowH264,   6: DivXH264)" Editable="true" Value="0" />
        <MPEG4 Comment="MPEG4 디코딩에 사용할 디코더를 설정합니다. (0: DMOWrapperFilter(MS),  1: MainConceptMpeg4,   2: XvidDecoder,  3: InnotiveVideoDecoder(IPP),  4: FFDShowMpeg4)" Editable="true" Value="0" />
      </Decoder>
      <Limit>
        <HighMiddle Comment="화면에 동시에 표시할 고화질과 중화질 영상 수를 설정합니다." Editable="true" Value="20" />
        <MaxHigh Comment="화면에 표시할 최대 고화질 영상 수를 설정합니다." Editable="true" Value="10" />
        <Low Comment="내부적으로 가져올 저화질 영상 수를 설정합니다. (-1이면 등록된 전체 저화질 영상 수를 자동 계산)" Editable="true" Value="-1" />
      </Limit>
      <ResizingFilter>
        <Use Comment="고해상도 표출시 그래픽 카드 메모리 사용률을 줄위기 위한 Resizing Filter 사용 여부를 설정합니다. (true/false)" Editable="true" Value="false" />
      </ResizingFilter>
      <RTSP>
        <Use Comment="영상 수신시 RTSP 프로토콜 사용 여부를 설정합니다. (true/false)" Editable="true" Value="true" />
        <Timeout Comment="소스 필터의 타임아웃(재시도) 시간을 설정합니다. (단위: ms. 0이하 값이면 필터의 기본값을 사용합니다.)" Editable="true" Value="-1" />
      </RTSP>
      <Video>
        <Renderer Comment="영상 표시시 비디오 렌더러 형식을 설정합니다. (VMR9/EVR)" Editable="true" Value="EVR" />
      </Video>
      <InnoVideoLib>
        <Use Comment="InnoVideoLib 사용 여부를 설정합니다. true 이면 MediaKit과 FilterNode의 값을 사용하지 않습니다. (true/false)" Editable="true" Value="false" />
      </InnoVideoLib>
    </Filter>
    <GridCell>
      <Line>
        <Visible Comment="Stage Cell 분할 시 Cell 간 경계선을 보일 지 설정합니다. (true/false)" Editable="true" Value="true" />
      </Line>
      <Zoom>
        <AnimationSpeed Comment="Stage Cell Double Click으로 확대/축소 시 속도를 설정합니다. (단위: 초)" Editable="true" Value="0.15" />
        <ManualSizePercentage Comment="Stage Cell Double Click 시 확대될 Cell의 크기를 설정합니다. (단위: Percent)" Editable="true" Left="0" Top="0" Width="100" Height="100" />
        <UseManualSize Comment="ManualSizePercentage를 사용할지 여부를 설정합니다. 사용하지 않으면 Stage 전체에 표시됩니다. (true/false)" Editable="true" Value="true" />
      </Zoom>
    </GridCell>
    <Mode>
      <Contents>
        <Use Comment="컨텐츠 모드 사용 여부를 설정합니다. (true/false)" Editable="true" Value="true" />
      </Contents>
      <Stage>
        <Use Comment="카메라 모드 사용 여부를 설정합니다. (true/false)" Editable="true" Value="true" />
      </Stage>
    </Mode>
  </DisplayCommon>
  <Command>
    <Alarm>
      <Connection>
        <Use Comment="알람 사용 여부를 설정합니다. (true/false)" Editable="true" Value="false" />
      </Connection>
      <Message>
        <SendConfirmMessageToServer Comment="서버에 알람 확인 메시지를 전송할 지 설정합니다. (true/false)" Editable="true" Value="true" />
      </Message>
      <ActivatedList>
        <RowCountNode Comment="알람 목록의 행 개수를 설정합니다." Editable="true" Value="2" />
      </ActivatedList>
      <StatusViewer>
        <Use Comment="우측 상단에 현재 알람 상태를 표시할 지 설정합니다. (true/false)" Editable="true" Value="false" />
      </StatusViewer>
      <AlarmPopupWindow>
        <Use Comment="알람 팝업 윈도우 사용 여부를 설정합니다. (true/false)" Editable="true" Value="true" />
        <Position Comment="알람 팝업 창 위치를 설정합니다. (기본값 Top: -1, Left: -1, 둘중 하나라도 음수값을 가지면 자동으로 설정한다.)" Editable="true" Top="-1" Left="-1" />
      </AlarmPopupWindow>
    </Alarm>
    <Control>
      <RDS>
        <Operating Comment="RDS 제어 사용 여부를 설정합니다. (true/false)" Editable="true" Value="false" />
        <InputProtocolType Comment="RDS 제어 시 사용할 프로토콜을 설정합니다. (0: InnotiveRDS, 1: VNC)" Editable="true" Value="0" />
        <VncPassword Comment="VNC 접속에 사용할 암호를 설정합니다." Editable="true" Value="1111" />
        <VncPort Comment="VNC를 사용할 포트를 설정합니다. (기본값: 5100)" Editable="true" Value="5100" />
      </RDS>
    </Control>
    <SettingWindow>
      <StagePosition>
        <Auto Comment="Stage 설정 시 크기 위치 지정을 자동으로 할 지 설정합니다. 자동으로 사용하지 않으면 사용자가 지정한 위치에 표시합니다. (true/false)" Editable="true" Value="false" />
      </StagePosition>
    </SettingWindow>
    <StageMode>
      <CameraList>
        <NameFormat Comment="카메라 목록 탭에 표시되는 카메라 레이블 형식을 설정합니다. 이름은 {NAME}, 아이디는 {ID}를 입력하면 됩니다." Editable="true" Value="{NAME} ({ID})" />
        <SelectingLimit Comment="카메라 목록 탭에서 선택할 수 있는 최대 카메라 개수를 설정합니다." Editable="true" Value="99" />
        <Use Comment="카메라 목록 탭을 보일 지 여부를 설정합니다. (true/false)" Editable="true" Value="true" />
      </CameraList>
      <Layout>
        <LoadSameLayout Comment="모든 Stage를 통틀어 같은 레이아웃을 추가할 수 있을 지 여부를 설정합니다. (true/false)" Editable="true" Value="false" />
        <Limit Comment="모든 Stage를 통틀어 레이아웃을 추가할 수 있는 최대 개수를 설정합니다." Editable="true" Value="5" />
      </Layout>
      <LayoutList>
        <Use Comment="레이아웃 목록 탭을 보일 지 여부를 설정합니다. (true/false)" Editable="true" Value="true" />
      </LayoutList>
      <Stage>
        <LayoutLimit Comment="Stage당 추가할 수 있는 최대 레이아웃 개수를 설정합니다." Editable="true" Value="3" />
        <PlayOnControlMode Comment="제어 모드일 경우에만 영상을 출력할 지 여부를 설정합니다.(true/false)" Editable="true" Value="false" />
        <MultiGridTemplate Comment="Multigrid Template 사용 여부를 설정합니다.(true/false)" Editable="true" Value="false" />
        <MultiGridCustom Comment="Multigrid Custome 사용 여부를 설정합니다.(true/false)" Editable="true" Value="true" />
        <UseSyncToggleButton Comment="Sync Toggle Button 사용 여부를 설정합니다.(true/false)" Editable="true" Value="false" />
      </Stage>
      <StageCell>
        <AutoClearSelection Comment="Stage의 Cell들을 선택하여 병합시 자동으로 선택이 해제할지 여부를 설정합니다. (true/false)" Editable="true" Value="false" />
        <MouseOverBorderVisible Comment="Stage의 Cell 위에 마우스를 올렸을 때 테두리를 보여줄지 여부를 설정합니다. (true/false)" Editable="true" Value="true" />
        <MouseSelectBorderVisible Comment="Stage의 Cell을 선택했을 때 테두리를 보여줄지 여부를 설정합니다. (true/false)" Editable="true" Value="true" />
      </StageCell>
    </StageMode>
  </Command>
</CommandConfig>